//
//  HWColourScheme.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 28/10/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

@interface HWColourScheme : NSObject

+ (UIColor*)harmonyColour;
+ (UIColor*)passingColour;
+ (UIColor*)suspensionColour;
+ (UIColor*)autoColour;
+ (UIColor*)restColour;
+ (UIColor*)harmonyEventColour;
+ (UIColor*)harmonyEventMelodicColour;
+ (UIColor*)harmonyEventHarmonicColour;
+ (UIColor*)selectedColour;
+ (UIColor*)outOfRangeColour;
+ (UIColor*)lockedAutoColour;
+ (UIColor*)paperColour;
+ (UIColor*)rulerBackgroundColor;
+ (UIColor*)rulerLoopBackgroundColor;
+ (UIColor*)mainBackgroundColour;
+ (UIColor*)horizontalDividerColour;
+ (UIColor*)verticalDividerColour;
+ (UIColor*)selectBoxBorderColour;
+ (UIColor*)selectBoxFillColour;
+ (UIColor*)eraserColour;

@end
