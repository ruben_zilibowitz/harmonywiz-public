//
//  HWAddNewTrackMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 22/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWAddNewTrackMenu.h"
#import "HWCollectionViewController.h"

extern NSString * const HWNewTrackNotification;

@interface HWAddNewTrackMenu ()

@end

@implementation HWAddNewTrackMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([HWCollectionViewController currentlyOpenDocument].song.keySign != nil) {
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc postNotificationName:HWNewTrackNotification object:self userInfo:@{@"row" : @(indexPath.row)}];
    }
    else if ([HWCollectionViewController currentlyOpenDocument].song.songMode != nil) {
        [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Add New Track", @"Alert title") message:LinguiLocalizedString(@"When using modes you must have four tracks. To add a new track, first change to a major or minor key signature.", @"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
    }
}

@end
