//
//  HWTimeSignatureMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWTimeSignatureMenu.h"
#import "NSString+Unichar.h"
#import "HWCollectionViewController.h"

NSString * const HWTimeSignatureChangedNotification = @"HWTimeSignatureChanged";

@interface HWTimeSignatureMenu ()

@end

@implementation HWTimeSignatureMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    self.upperNumeralStepper.value = doc.song.timeSign.upperNumeral;
    self.lowerNumeralStepper.value = doc.song.timeSign.lowerNumeral;
    
    self.upperNumeralLabel.font = [UIFont fontWithName:@"AloisenNew" size:42.0f];
    self.upperNumeralLabel.text = [self numeralStringFromInteger:(NSUInteger)self.upperNumeralStepper.value];
    self.lowerNumeralLabel.font = [UIFont fontWithName:@"AloisenNew" size:42.0f];
    self.lowerNumeralLabel.text = [self numeralStringFromInteger:(NSUInteger)self.lowerNumeralStepper.value];
}

- (NSString*)numeralStringFromInteger:(NSUInteger)value {
    NSString *str = [(@(value)) stringValue];
    const char *strUTF8 = str.UTF8String;
    unichar unistr[32];
    for (UInt16 i = 0; strUTF8[i] != '\0' && i < 32; i++)
        unistr[i] = strUTF8[i] - '0' + 0xF030;
    return [NSString stringWithCharacters:unistr length:str.length];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)upperNumeralStep:(UIStepper*)sender {
    self.upperNumeralLabel.text = [self numeralStringFromInteger:(NSUInteger)sender.value];
}

- (IBAction)lowerNumeralStep:(UIStepper*)sender {
    self.lowerNumeralLabel.text = [self numeralStringFromInteger:(NSUInteger)sender.value];
}

#pragma mark - Table view delegate


- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        
        [doc.hwUndoManager startUndoableChanges];
        HarmonyWizTimeSignEvent *timeSign = [doc.song.timeSign copy];
        timeSign.upperNumeral = (NSUInteger)self.upperNumeralStepper.value;
        timeSign.lowerNumeral = (NSUInteger)self.lowerNumeralStepper.value;
        doc.song.timeSign = timeSign;
        [doc.song roundUpFinalBar];
        [doc.hwUndoManager finishUndoableChangesName:@"Time Signature"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HWTimeSignatureChangedNotification
                                                            object:self];
    }
}

@end
