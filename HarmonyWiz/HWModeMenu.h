//
//  HWModeMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

enum ModeName {
    kMode_QuartalMajor = 0,
//    kMode_QuartalPerfectMajor,
    kMode_HarmonicMinor,
    kMode_Wholetone,
    kMode_Octatonic,
    kMode_Messiaen3,
//    kMode_Messiaen4,
//    kMode_Messiaen5,
//    kMode_Messiaen6,
    kMode_Messiaen7,
    kMode_Nohkan,
    kMode_YoulanChina,
    kMode_NumModes
};

extern NSString * const HWModeChangedNotification;

@interface HWModeMenu : UITableViewController
+ (NSString*)modeNameForID:(enum ModeName)modeID;
+ (NSArray*)modePitchesForID:(enum ModeName)modeID;
//@property (nonatomic,assign) IBOutlet UISegmentedControl *topTones, *bottomTones;
//- (IBAction)topToneSelected:(UISegmentedControl*)sender;
//- (IBAction)bottomToneSelected:(UISegmentedControl*)sender;
@end
