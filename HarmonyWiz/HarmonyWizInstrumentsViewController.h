//
//  HarmonyWizInstrumentsViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 13/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizTrackControl.h"
#import "HarmonyWizPurchaseSamplesViewController.h"
#if (! RELEASE_BUILD)
#import "HarmonyWizDownloadSamplesViewController.h"
#endif
#import "HWInstrumentSelectViewController.h"
#import "HWOctaveTransposeViewController.h"

extern NSString * const HWSelectAllTrackNotification;
extern NSString * const HWRemoveTrackNotification;
extern NSString * const HWFreezeTrackNotification;
extern NSString * const HWClearTrackNotification;

@interface HarmonyWizInstrumentsViewController : UITableViewController

@property (nonatomic,strong) HarmonyWizTrackControl *trackControl;

@property (nonatomic,strong) HarmonyWizPurchaseSamplesViewController *purchaseSamplesViewController;
#if (! RELEASE_BUILD)
@property (nonatomic,strong) HarmonyWizDownloadSamplesViewController *downloadSamplesViewController;
#endif
@property (nonatomic,strong) HWInstrumentSelectViewController *instrumentSelectViewController;
@property (nonatomic,strong) HWOctaveTransposeViewController *octaveViewController;

- (void)updateInstrumentNames;
@property (nonatomic,strong) NSArray *instrumentNames;

@end
