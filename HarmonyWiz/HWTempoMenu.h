//
//  HWTempoMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWTempoMenu : UITableViewController

@property (nonatomic,weak) IBOutlet UILabel *tempoLabel;
@property (nonatomic,weak) IBOutlet UIButton *tapButton, *stepDownButton, *stepUpButton;

- (IBAction)stepDown:(id)sender;
- (IBAction)stepUp:(id)sender;

@end

@interface HWTempoCell : UITableViewCell

@end