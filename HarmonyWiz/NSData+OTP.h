//
//  NSData+OTP.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (OTP)
- (NSData*)oneTimePad:(const int8_t)key;
@end
