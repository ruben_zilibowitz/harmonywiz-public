//
//  UIApplication+Version.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 9/11/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (Version)

+ (NSString *) appVersion;
+ (NSString *) build;
+ (NSString *) versionBuild;

@end
