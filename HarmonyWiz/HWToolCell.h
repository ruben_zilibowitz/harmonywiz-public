
#import <UIKit/UIKit.h>
#import "NSString+HWVerticalAlign.h"

@interface HWToolCell : UICollectionViewCell

@property (nonatomic,assign) BOOL outline;
@property (nonatomic, strong) NSString *contentString;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic,assign) HWVerticalTextAlignment verticalAlignment;
@property (nonatomic, strong) UIColor *interiorColor;
@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) NSString *helpText;

- (void)setup;

@end
