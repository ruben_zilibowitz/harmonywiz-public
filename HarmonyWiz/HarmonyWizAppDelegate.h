//
//  HarmonyWizAppDelegate.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HarmonyWizViewController;

@interface HarmonyWizAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) HarmonyWizViewController *viewController;

- (void)pushIdleTimerDisabled;
- (void)popIdleTimerDisabled;

@end
