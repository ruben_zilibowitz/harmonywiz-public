//
//  NSMutableArray+Sorted.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 6/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "NSMutableArray+Sorted.h"
#import "HWExceptions.h"
#import "HWAppDelegate.h"

@interface NSObject (CompareWithValue)
- (NSComparisonResult)compareWithValue:(NSNumber*)obj;
@end

@implementation NSMutableArray (Sorted)

- (NSUInteger)indexOfInt:(int)number {
    NSUInteger idx = [self indexOfObject:@(number) inSortedRange:NSMakeRange(0, self.count) options:0 usingComparator:^NSComparisonResult(id obj1,id obj2){return [obj1 compareWithValue:obj2];}];
    return idx;
}

- (NSUInteger)insertionIndex:(id)anObject {
    NSUInteger idx = [self indexOfObject:anObject inSortedRange:NSMakeRange(0, self.count) options:NSBinarySearchingInsertionIndex usingComparator:^NSComparisonResult(id obj1,id obj2){return [obj1 compare:obj2];}];
    
    // run some checks
    if (idx > 0) {
        if ([[self objectAtIndex:idx-1] compare:anObject] == NSOrderedSame) {
            @throw [HWPlacedEventDoubledUpException exceptionWithEvents:[self objectAtIndex:idx-1] :anObject];
        }
    }
    if (idx < self.count) {
        if ([[self objectAtIndex:idx] compare:anObject] == NSOrderedSame) {
            @throw [HWPlacedEventDoubledUpException exceptionWithEvents:anObject :[self objectAtIndex:idx]];
        }
    }
    return idx;
}

- (void)noTryPlaceObject:(id)anObject {
    NSUInteger idx = [self insertionIndex:anObject];
    [self insertObject:anObject atIndex:idx];
}

- (void)placeObject:(id)anObject {
    @try {
        [self noTryPlaceObject:anObject];
    }
    @catch (HWPlacedEventDoubledUpException *exception) {
        [self alertSomethingWrong];
    }
    @finally {
    }
}

- (void)placeObjectsFromArray:(NSArray *)otherArray {
    @try {
        for (id obj in otherArray) {
            [self noTryPlaceObject:obj];
        }
    }
    @catch (HWPlacedEventDoubledUpException *exception) {
        [self alertSomethingWrong];
    }
    @finally {
    }
}

- (void)alertSomethingWrong {
    recordError([NSError errorWithDomain:@"HWPlaceObject" code:1 userInfo:nil]);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Something went wrong. The last action could not be completed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
}

@end
