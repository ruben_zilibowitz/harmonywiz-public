//
//  NSString+Concatenation.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Concatenation)

- (NSString *):(NSString *)a;
- (NSString *):(NSString *)a :(NSString *)b;
- (NSString *):(NSString *)a :(NSString *)b :(NSString *)c;
- (NSString *):(NSString *)a :(NSString *)b :(NSString *)c :(NSString *)d;

- (NSString *)concat:(NSString *)strings, ...;

@end
