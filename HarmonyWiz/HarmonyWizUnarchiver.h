//
//  HarmonyWizUnarchiver.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HarmonyWizUnarchiver : NSKeyedUnarchiver
@end
