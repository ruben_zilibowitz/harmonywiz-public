//
//  HWSongMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWSongMenu : UITableViewController
@property (nonatomic,weak) IBOutlet UITableViewCell *addNewTrackCell;
@property (nonatomic,weak) IBOutlet UILabel *reverb, *quantisation;
@property (nonatomic,weak) IBOutlet UILabel *tempo, *key, *time, *measures, *presets;
@property (nonatomic,weak) IBOutlet UISwitch *loopSongSwitch, *masterEffectsSwitch;
- (IBAction)loopSong:(UISwitch*)sender;
- (IBAction)masterEffects:(UISwitch*)sender;
@end
