//
//  HWExceptions.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HWExceptions.h"

@implementation HWNonFatalException

+ (NSString *)theReason { [NSException raise:NSGenericException format:@"Abstract class method must be overriden in subclass."]; return nil; }
+ (NSString *)theExplanation { [NSException raise:NSGenericException format:@"Abstract class method must be overriden in subclass."]; return nil; }

+ (id)exception
{
    return [[self class] exceptionWithName:NSStringFromClass([self class]) reason:[self theReason] userInfo:nil];
}

@end

/////////////////////////////////////////////////////////////////////////
#pragma mark - Specific exception implementations
/////////////////////////////////////////////////////////////////////////


@implementation HWContextException
+ (NSString *)theReason { return nil; }
+ (NSString *)theExplanation { return nil; }
@end

@implementation HWEventsSortedVerifyFailed
+ (NSString *)theReason { return nil; }
+ (NSString *)theExplanation { return nil; }
@end

@implementation HWPlacedEventDoubledUpException
+ (NSString *)theReason { return nil; }
+ (NSString *)theExplanation { return nil; }
+ (id)exceptionWithEvents:(id)ev1 :(id)ev2 {
    HWPlacedEventDoubledUpException *result = [[self class] exception];
    result->_event1 = ev1;
    result->_event2 = ev2;
    return result;
}
@end

@implementation HWSongVersionException
+ (id)exceptionWithVersion:(NSString*)versionString {
    HWSongVersionException *result = [[self class] exception];
    result->_versionString = versionString;
    return result;
}
+ (NSString *)theReason { return nil; }
+ (NSString *)theExplanation { return nil; }
@end
