//
//  NSString+RomanNumerals.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 23/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RomanNumerals)

+ (NSString *)romanNumeralsFromInt:(NSUInteger)input;

@end
