//
//  HWEntry.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HWMetadata.h"

@interface HWEntry : NSObject

@property (atomic,strong) NSURL * fileURL;
@property (atomic,strong) HWMetadata * metadata;
@property (atomic,assign) UIDocumentState state;
@property (atomic,strong) NSFileVersion * version;

- (id)initWithFileURL:(NSURL *)fileURL metadata:(HWMetadata *)metadata state:(UIDocumentState)state version:(NSFileVersion *)version;
- (NSString *) description;

@end
