//
//  HWTimeRulerScrollView.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "HWTimeRulerScrollView.h"
#import "HWDocument.h"
#import "HarmonyWizEvent.h"
#import "HWCollectionViewController.h"
#import "NSString+iOS6.h"
#import "HWColourScheme.h"
#import "NSMutableArray+Sorted.h"
#import "UIView+Recursive/UIView+Recursive.h"
#include <vector>

extern CGFloat const HWRulerHeight;
extern CGFloat const HWScreenWidth;
const CGFloat HWScrollFullScorePaddingDistance = 256.f;

NSInteger const HWRulerTag = 'rulr';
NSString * const HWTouchedRuler = @"HWTouchedRulerNotification";

CGFloat const HWLocatorWidth = 20.f;

using namespace std;

@interface HWBarNumberLabel : UILabel
@property (nonatomic,assign) NSInteger barNumber;
@end

@implementation HWBarNumberLabel
- (void)setBarNumber:(NSInteger)barNumber {
    _barNumber = barNumber;
    self.text = @(barNumber+1).stringValue;
}
- (NSComparisonResult)compare:(HWBarNumberLabel*)obj {
    if (self.barNumber < obj.barNumber)
        return NSOrderedAscending;
    else if (self.barNumber > obj.barNumber)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
- (NSComparisonResult)compareWithValue:(NSNumber*)obj {
    if (self.barNumber < obj.integerValue)
        return NSOrderedAscending;
    else if (self.barNumber > obj.integerValue)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
@end

@interface HWTimeRulerBar : UIView
@property (nonatomic,assign) NSInteger barNumber;
@end

@implementation HWTimeRulerBar
- (NSComparisonResult)compare:(HWTimeRulerBar*)obj {
    if (self.barNumber < obj.barNumber)
        return NSOrderedAscending;
    else if (self.barNumber > obj.barNumber)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
- (NSComparisonResult)compareWithValue:(NSNumber*)obj {
    if (self.barNumber < obj.integerValue)
        return NSOrderedAscending;
    else if (self.barNumber > obj.integerValue)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
- (void)drawRect:(CGRect)rect {
    HWTimeRulerScrollView *tr = (id)[[self superview] superview];
    if (tr) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        
        // draw ruler
        if (doc.song.timeSigns.events.count > 0) {
            const HarmonyWizTimeSignEvent *timeSign = [doc.song.timeSigns.events objectAtIndex:0];
            NSAssert(timeSign.beat == 0 && timeSign.subdivision == 0, @"Time sign not at start");
            NSAssert(timeSign.lowerNumeral == 4 || timeSign.lowerNumeral == 8, @"Time sign lower numeral invalid");
            
            UIColor *fontColor = (tr.internalColor ? tr.internalColor : [UIColor lightTextColor]);
            vector<CGPoint> lines;
            CGFloat xpos;
            
            if (timeSign.lowerNumeral == 4) {
                for (UInt16 beat = 0; beat < doc.song.timeSign.upperNumeral; beat++) {
                    xpos = ((1+beat)*tr.distanceBetweenBeats);
                    lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.7f));
                    lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                    
                    if (doc.metadata.quantization >= kQuantization_Eighth) {
                        xpos += (0.5f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.9f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                    }
                    if (doc.metadata.quantization >= kQuantization_Sixteenth) {
                        xpos -= (0.25f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.95f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                        xpos += (0.5f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.95f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                    }
                    if (doc.metadata.quantization >= kQuatization_Thirtysecond) {
                        xpos += (0.125f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.97f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                        xpos -= (0.25f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.97f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                        xpos -= (0.25f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.97f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                        xpos -= (0.25f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.97f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                    }
                }
            }
            else if (timeSign.lowerNumeral == 8) {
                for (UInt16 beat = 0; beat < doc.song.timeSign.upperNumeral; beat++) {
                    xpos = ((1+beat)*tr.distanceBetweenBeats);
                    lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.7f));
                    lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                    if (doc.metadata.quantization >= kQuantization_Sixteenth) {
                        xpos += (0.5f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.9f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                    }
                    if (doc.metadata.quantization >= kQuatization_Thirtysecond) {
                        xpos -= (0.25f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.95f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                        xpos += (0.5f*tr.distanceBetweenBeats);
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height*0.95f));
                        lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
                    }
                }
            }
            xpos = (tr.distanceBetweenBars);
            lines.push_back(CGPointMake(xpos, 0));
            lines.push_back(CGPointMake(xpos, tr.bounds.size.height));
            
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetLineWidth(context, 1.f);
            CGContextSetShouldAntialias(context, false);
            CGContextSetStrokeColorWithColor(context, fontColor.CGColor);
            CGContextStrokeLineSegments(context, &lines[0], lines.size());
        }
    }
}
@end

@interface HWLocatorHead : UIView
@property (nonatomic,assign) BOOL isPointer;
@end

@implementation HWLocatorHead
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(context, true);
    if (self.isPointer) {
        CGContextSetFillColorWithColor(context, [UIColor orangeColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, 0, 0.f);
        CGContextAddLineToPoint(context, self.bounds.size.width, 0.f);
        CGContextAddLineToPoint(context, self.bounds.size.width, self.bounds.size.height*0.4f);
        CGContextAddLineToPoint(context, self.bounds.size.width*0.5f, self.bounds.size.height);
        CGContextAddLineToPoint(context, 0, self.bounds.size.height*0.4f);
        CGContextFillPath(context);
    }
    else {
        CGContextSetStrokeColorWithColor(context, [UIColor orangeColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, self.bounds.size.width*0.5f, 0.f);
        CGContextAddLineToPoint(context, self.bounds.size.width*0.5f, self.bounds.size.height);
        CGContextSetLineWidth(context, 2.f);
        CGContextStrokePath(context);
    }
}
@end

@interface HWTimeRulerScrollView ()
@property (nonatomic,strong) NSMutableArray *barViews, *barViewQueue;
@property (nonatomic,strong) NSMutableArray *barNumberLabels, *barNumberLabelQueue;
@property (nonatomic,strong) HWLocatorHead *locatorHead;
@property (nonatomic,strong) UILongPressGestureRecognizer *longPress;
@property (nonatomic,strong) UITapGestureRecognizer *doubleTap;
@end

NSString * const HWDidAddBarNotification = @"HWDidAddBarNotification";
NSString * const HWDidRemoveBarNotification = @"HWDidRemoveBarNotification";

@implementation HWTimeRulerScrollView

- (UIView*)locatorHeadView {
    return self.locatorHead;
}

- (void)updateBackgroundColour {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.loopingEnabled)
        self.backgroundColor = [HWColourScheme rulerLoopBackgroundColor];
    else
        self.backgroundColor = [HWColourScheme rulerBackgroundColor];
}

- (void)setContentSize:(CGSize)contentSize {
    self.contentView.frame = CGRectMake(0, 0, contentSize.width, contentSize.height);
    [super setContentSize:contentSize];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // create content view
        self.contentView = [[HWViewKeepingTop alloc] initWithFrame:CGRectMake(0, 0, 100, HWRulerHeight)];
        [self addSubview:self.contentView];
        self.contentSize = CGSizeMake(100, HWRulerHeight);
        
        [self updateBackgroundColour];
        self.xOffset = 8.f;
        self.xZoom = 1.f;
        self.barViews = [NSMutableArray array];
        self.barNumberLabels = [NSMutableArray array];
        self.barViewQueue = [NSMutableArray array];
        self.barNumberLabelQueue = [NSMutableArray array];
        self.locatorHead = [[HWLocatorHead alloc] initWithFrame:CGRectMake(0, 0, HWLocatorWidth, frame.size.height)];
        self.locatorHead.backgroundColor = [UIColor clearColor];
        self.locatorHead.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.locatorHead];
        
        self.contentView.viewsOnTop = @[self.locatorHead];
        
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        self.isPointer = (! doc.metadata.magicMode);
        
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc addObserver:self selector:@selector(addBar:) name:HWDidAddBarNotification object:nil];
        [dnc addObserver:self selector:@selector(removeBar:) name:HWDidRemoveBarNotification object:nil];
        
        self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [self addGestureRecognizer:self.longPress];

        self.doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        self.doubleTap.numberOfTapsRequired = 2;
        [self addGestureRecognizer:self.doubleTap];
        
        self.leftMostVisibleMeasure = -2;
        self.rightMostVisibleMeasure = -2;
}
    return self;
}

- (void)longPress:(UILongPressGestureRecognizer*)gest {
    CGPoint loc = [gest locationInView:self];
    [self touchRuler:loc];
}

- (void)doubleTap:(UITapGestureRecognizer*)gest {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    doc.metadata.loopingEnabled = (! doc.metadata.loopingEnabled);
    [self updateBackgroundColour];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)updateVisible:(BOOL)redraw {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const CGFloat HWScrollStavesPaddingDistance = (doc.metadata.magicMode ? HWScreenWidth : HWScrollFullScorePaddingDistance);
    
    float leftPos = [self playbackPositionFromLocation:self.contentOffset.x - HWScrollStavesPaddingDistance] * (doc.song.timeSign.lowerNumeral / 4);
    float rightPos = [self playbackPositionFromLocation:self.contentOffset.x+self.frame.size.width + HWScrollStavesPaddingDistance] * (doc.song.timeSign.lowerNumeral / 4);
    
    const SInt16 left = MIN(MAX((SInt16)(leftPos / doc.song.timeSign.upperNumeral), 0), doc.song.finalBarNumber-1);
    const SInt16 right = MIN(MAX((SInt16)(rightPos / doc.song.timeSign.upperNumeral), 0), doc.song.finalBarNumber-1);
    
    BOOL needsRedraw = NO;
    if (left != self.leftMostVisibleMeasure) {
        self.leftMostVisibleMeasure = left;
        needsRedraw = YES;
    }
    if (right != self.rightMostVisibleMeasure) {
        self.rightMostVisibleMeasure = right;
        needsRedraw = YES;
    }
    
    if (redraw && needsRedraw) {
        [self updateBarViews];
    }
    
    return needsRedraw;
}

- (void)setIsPointer:(BOOL)pointer {
    self.locatorHead.isPointer = pointer;
//    self.longPress.enabled = pointer;
    [self.locatorHead setNeedsDisplay];
}

- (void)addBar:(NSNotification*)notif {
    [self updateVisible:YES];
//    [self updateBarViews];
//    [self setNeedsLayout];
}

- (void)removeBar:(NSNotification*)notif {
    [self updateVisible:YES];
//    [self updateBarViews];
//    [self setNeedsLayout];
}

- (void)updateBarViews {
    
    // bar views
    {
        // remove invisible ruler bars
        NSMutableArray *removing = [NSMutableArray array];
        for (HWTimeRulerBar *rulerBar in self.barViews) {
            if (rulerBar.barNumber < self.leftMostVisibleMeasure || rulerBar.barNumber > self.rightMostVisibleMeasure) {
                [removing addObject:rulerBar];
//                [rulerBar removeFromSuperview];
            }
        }
        [self.barViews removeObjectsInArray:removing];
        [self.barViewQueue addObjectsFromArray:removing];
        
        // add ruler bars needed
        for (int bar = self.leftMostVisibleMeasure; bar <= self.rightMostVisibleMeasure; bar++) {
            BOOL notFound = ([self.barViews indexOfInt:bar] == NSNotFound);
            if (notFound) {
                HWTimeRulerBar *rulerBar = [self.barViewQueue firstObject];
                if (rulerBar == nil) {
                    rulerBar = [[HWTimeRulerBar alloc] initWithFrame:CGRectZero];
                    rulerBar.userInteractionEnabled = NO;
                    rulerBar.backgroundColor = [UIColor clearColor];
                    rulerBar.opaque = YES;
                }
                else {
                    [self.barViewQueue removeObject:rulerBar];
                }
                rulerBar.barNumber = bar;
                rulerBar.translatesAutoresizingMaskIntoConstraints = NO;
                if (rulerBar.superview == nil)
                    [self.contentView addSubview:rulerBar];
                [self.contentView sendSubviewToBack:rulerBar];
                [rulerBar setNeedsDisplay];
                [self.barViews placeObject:rulerBar];
            }
        }
        
        [self.barViewQueue makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    // bar number labels
    {
        // remove invisible bar number labels
        NSMutableArray *removing = [NSMutableArray array];
        for (HWBarNumberLabel *barNum in self.barNumberLabels) {
            if (barNum.barNumber < self.leftMostVisibleMeasure || barNum.barNumber > self.rightMostVisibleMeasure) {
                [removing addObject:barNum];
            }
        }
        [self.barNumberLabels removeObjectsInArray:removing];
        [self.barNumberLabelQueue addObjectsFromArray:removing];
        
        // add bar number labels needed
        for (int bar = self.leftMostVisibleMeasure; bar <= self.rightMostVisibleMeasure; bar++) {
            BOOL notFound = ([self.barNumberLabels indexOfInt:bar] == NSNotFound);
            if (notFound) {
                HWBarNumberLabel *barNum = [self.barNumberLabelQueue firstObject];
                if (barNum == nil) {
                    barNum = [[HWBarNumberLabel alloc] initWithFrame:CGRectZero];
                    barNum.userInteractionEnabled = NO;
                    barNum.backgroundColor = nil;
                    barNum.opaque = NO;
                    barNum.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.f];
                }
                else {
                    [self.barNumberLabelQueue removeObject:barNum];
                }
                barNum.textColor = self.internalColor;
                barNum.barNumber = bar;
                barNum.translatesAutoresizingMaskIntoConstraints = NO;
                if (barNum.superview == nil)
                    [self.contentView addSubview:barNum];
                [self.contentView bringSubviewToFront:barNum];
                [self.barNumberLabels placeObject:barNum];
            }
        }
        
        [self.barNumberLabelQueue makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [self maintainQueueMaxSize];
}

- (void)maintainQueueMaxSize {
    NSUInteger const kMaxQueueSize = 20;
    
    if (self.barViewQueue.count > kMaxQueueSize) {
        NSLog(@"self.barViewQueue.count is too large at %lu", (unsigned long)self.barViewQueue.count);
        [self.barViewQueue removeObjectsInRange:NSMakeRange(0, self.barViewQueue.count - kMaxQueueSize)];
    }
    
    if (self.barNumberLabelQueue.count > kMaxQueueSize) {
        NSLog(@"self.barNumberLabelQueue.count is too large at %lu", (unsigned long)self.barNumberLabelQueue.count);
        [self.barNumberLabelQueue removeObjectsInRange:NSMakeRange(0, self.barNumberLabelQueue.count - kMaxQueueSize)];
    }
}

- (void)setInternalColor:(UIColor *)internalColor {
    _internalColor = internalColor;
    for (HWBarNumberLabel *label in self.barNumberLabels)
        label.textColor = internalColor;
    [self redisplay];
}

- (CGFloat)distanceBetweenCrotchetBeats {
    return (self.staveLineSpacing * [HWCollectionViewController currentlyOpenDocument].metadata.noteSpacing);
}

- (CGFloat)clefXLoc {
    return (self.staveLineSpacing * 1.f);
}

- (CGFloat)staveLineSpacing {
    return [HWCollectionViewController currentlyOpenDocument].metadata.notesPointSize / 4.f /* * 0.92f */;
}

- (CGFloat)keySignatureXLoc {
    return self.clefXLoc + self.staveLineSpacing*4.f;
}

- (CGFloat)keySignatureWidth:(SInt8)key {
    if (key >= 0) {
        return (key * self.staveLineSpacing * 2);
    }
    else {
        return (-key * self.staveLineSpacing * 0.6f * 2);
    }
}

- (CGFloat)timeSignatureXLoc {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    return [self keySignatureWidth:doc.song.keySign.sharpsFlatsCount] + self.keySignatureXLoc;
}

- (CGFloat)firstBeatXLoc {
    return self.timeSignatureXLoc + self.staveLineSpacing*6.f;
}

- (CGFloat)distanceBetweenBeats {
    return self.distanceBetweenCrotchetBeats / ([HWCollectionViewController currentlyOpenDocument].song.timeSign.lowerNumeral / 4);
}

- (CGFloat)totalSongWidth {
    return [self distanceBetweenBars] * [[HWCollectionViewController currentlyOpenDocument].song finalBarNumber] + self.firstBeatXLoc + self.staveLineSpacing - self.distanceBetweenBeats;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    const CGFloat barWidth = self.distanceBetweenBars;
    const CGFloat kNoOverlapBuffer = 0;//self.distanceBetweenBeats*0.5f;
    
    if (self.barViews) {
        const CGFloat startPos = self.xOffset + self.firstBeatXLoc - self.distanceBetweenBeats;
        const CGFloat height = self.bounds.size.height;
        for (HWTimeRulerBar *bar in self.barViews) {
            CGRect frame = CGRectMake(startPos + bar.barNumber * barWidth, 0, barWidth + self.distanceBetweenBeats - kNoOverlapBuffer, height);
            if (isfinite(frame.origin.x) && isfinite(frame.origin.y) && isfinite(frame.size.width) && isfinite(frame.size.height)) {
                bar.frame = frame;
            }
        }
    }
    
    if (self.barNumberLabels) {
        const CGFloat startPos = self.firstBeatXLoc - self.distanceBetweenBeats;
        for (HWBarNumberLabel *barNum in self.barNumberLabels) {
            [barNum sizeToFit];
            CGRect frame = CGRectMake((barNum.barNumber == 0 ? self.xOffset : startPos) + barNum.barNumber * barWidth + 12.f, 0, barNum.frame.size.width, barNum.frame.size.height);
            if (isfinite(frame.origin.x) && isfinite(frame.origin.y) && isfinite(frame.size.width) && isfinite(frame.size.height)) {
                barNum.frame = frame;
            }
        }
    }
    
    if (self.locatorHead && isfinite(_playbackIndicatorXLoc)) {
        CGPoint center = CGPointMake(self.playbackIndicatorXLoc+self.xOffset, self.bounds.size.height*0.5f);
        self.locatorHead.frame = CGRectMake(center.x - HWLocatorWidth*0.5f, 0, HWLocatorWidth, self.bounds.size.height);
    }
}

- (void)redisplay {
    [self.barViews makeObjectsPerformSelector:@selector(setNeedsDisplay)];
    [self setNeedsLayout];
    [self updatePlaybackIndicatorPosition];
}

- (void)updatePlaybackIndicatorPosition {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const SInt16 barCount = SInt16(doc.song.playbackPosition / doc.song.timeSign.upperNumeral * (doc.song.timeSign.lowerNumeral/4.));
    _playbackIndicatorXLoc = self.firstBeatXLoc + doc.song.playbackPosition * self.distanceBetweenCrotchetBeats + barCount * self.distanceBetweenBeats;
}

- (CGFloat)distanceBetweenBars {
    HarmonyWizTimeSignEvent *timeSign = [[HWCollectionViewController currentlyOpenDocument].song.timeSigns.events objectAtIndex:0];
    NSAssert(timeSign.beat == 0 && timeSign.subdivision == 0, @"Time sign not at start");
    return (self.distanceBetweenBeats * (1+timeSign.upperNumeral));
}

- (CGFloat)xposFromEvent:(HarmonyWizEvent*)event {
    UInt16 bar = [[HWCollectionViewController currentlyOpenDocument].song barNumberFromEvent:event];
    UInt16 beat = [[HWCollectionViewController currentlyOpenDocument].song barBeatFromEvent:event];
    float fbeat = beat + float(event.subdivision) / float([HWCollectionViewController currentlyOpenDocument].song.beatSubdivisions) + 1;
    return self.firstBeatXLoc - self.distanceBetweenBeats + self.distanceBetweenBars*bar + self.distanceBetweenBeats * fbeat;
}

- (float)playbackPositionFromLocation:(CGFloat)xloc {
    float result = 0;
    xloc -= self.xOffset;
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const float distanceBetweenBarLines = (self.distanceBetweenBeats * (doc.song.timeSign.upperNumeral+1));
    const UInt16 barCount = UInt16((xloc - self.firstBeatXLoc + self.distanceBetweenBeats)/distanceBetweenBarLines);
    const float distanceFromPreviousBarLine = (xloc - self.firstBeatXLoc + self.distanceBetweenBeats - barCount*distanceBetweenBarLines);
    const float quantization = 1.f * (powf(2.f,2.f-doc.metadata.quantization));
    const float denominator = (4.f/doc.song.timeSign.lowerNumeral);
    if (distanceFromPreviousBarLine < self.distanceBetweenBeats) {
        result = barCount * doc.song.timeSign.upperNumeral * denominator;
    }
    else {
        result = MAX(0.f,(xloc - self.firstBeatXLoc - barCount * self.distanceBetweenBeats) / self.distanceBetweenCrotchetBeats);
        result = roundf(result / quantization) * quantization;
    }
    
    BOOL const HWStickToPreviousBar = YES;
    if (HWStickToPreviousBar) {
        const float beat = fmodf(result/denominator, doc.song.timeSign.upperNumeral);
        const float bar = result/denominator / doc.song.timeSign.upperNumeral;
        if (beat < 1e-4f && bar > 0.5f) {
            // we are on first beat of bar
            BOOL isOK = (distanceFromPreviousBarLine < self.distanceBetweenBeats*(1+quantization/denominator*0.5f) && distanceFromPreviousBarLine >= 0);
            if (! isOK) {
                result -= quantization;
            }
            
        }
    }
    
    return result;
}

- (void)touchRuler:(CGPoint)loc {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    doc.song.playbackPosition = [self playbackPositionFromLocation:loc.x];
    [self updatePlaybackIndicatorPosition];
    [[NSNotificationCenter defaultCenter] postNotificationName:HWTouchedRuler object:self];
    [self setNeedsDisplay];
}

- (void)purgeQueues {
    [self.barViewQueue removeAllObjects];
    [self.barNumberLabelQueue removeAllObjects];
}

@end
