//
//  HWTimeSignatureMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const HWTimeSignatureChangedNotification;

@interface HWTimeSignatureMenu : UITableViewController
@property (nonatomic,weak) IBOutlet UILabel *upperNumeralLabel, *lowerNumeralLabel;
@property (nonatomic,weak) IBOutlet UIStepper *upperNumeralStepper, *lowerNumeralStepper;
- (IBAction)upperNumeralStep:(UIStepper*)sender;
- (IBAction)lowerNumeralStep:(UIStepper*)sender;
@end
