//
//  NSArray+Map.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import "NSArray+Map.h"

@implementation NSArray (Map)
- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [result addObject:block(obj, idx)];
    }];
    return result;
}
@end
