//
//  HWSettingsMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWSettingsMenu : UITableViewController

@property (nonatomic,weak) IBOutlet UISwitch *expertMode, *autofix, *autoScroll, *recordFromMIDI, *keyboardScrolls;
@property (nonatomic,weak) IBOutlet UISlider *verticalScale, *horizontalScale;
@property (nonatomic,weak) IBOutlet UILabel *languageLabel;

@end
