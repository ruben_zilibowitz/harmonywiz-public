//
//  HarmonyWizSong+AutoColour.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 13/11/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong.h"

@interface HarmonyWizSong (AutoColour)

- (void)autoColour:(NSMutableArray *)events withCadence:(BOOL)candence;
- (void)autoColour:(NSMutableArray*)events withPattern:(NSArray*)pattern withCadence:(BOOL)candence;

@end
