//
//  HWTimeRulerScrollView.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizSong.h"
#import "HWViewKeepingTop.h"

@interface HWTimeRulerScrollView : UIScrollView
@property (nonatomic,strong) HWViewKeepingTop *contentView;
@property (nonatomic,assign,readonly) CGFloat playbackIndicatorXLoc;
@property (nonatomic,assign) CGFloat xOffset;
@property (nonatomic,assign) CGFloat xZoom;
@property (nonatomic,strong) UIColor *internalColor;
@property (nonatomic,assign) SInt16 leftMostVisibleMeasure, rightMostVisibleMeasure;

- (void)setIsPointer:(BOOL)pointer;
- (CGFloat)staveLineSpacing;
- (CGFloat)distanceBetweenCrotchetBeats;
- (CGFloat)clefXLoc;
- (CGFloat)firstBeatXLoc;
- (CGFloat)keySignatureXLoc;
- (CGFloat)timeSignatureXLoc;

- (CGFloat)distanceBetweenBars;
- (CGFloat)distanceBetweenBeats;
- (CGFloat)totalSongWidth;
- (CGFloat)keySignatureWidth:(SInt8)key;
- (CGFloat)xposFromEvent:(HarmonyWizEvent*)event;
- (void)updatePlaybackIndicatorPosition;
- (void)touchRuler:(CGPoint)loc;
- (float)playbackPositionFromLocation:(CGFloat)xloc;
- (void)redisplay;
- (void)updateBarViews;
- (BOOL)updateVisible:(BOOL)redraw;
- (void)purgeQueues;
- (void)updateBackgroundColour;

- (UIView*)locatorHeadView;

@end

extern const NSInteger HWRulerTag;
extern NSString * const HWTouchedRuler;
extern NSString * const HWDidAddBarNotification;
extern NSString * const HWDidRemoveBarNotification;
