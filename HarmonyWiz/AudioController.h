//
//  AudioController.h
//  iPhoneAudio
//
//  Created by Ruben Zilibowitz on 4/06/11.
//  Copyright 2011 N.A. All rights reserved.
//

#import <TheAmazingAudioEngine.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#if AUDIOBUS_SUPPORT
#import <Audiobus.h>
#endif

// settings for reverb unit
struct Reverb2Parameters {
    AudioUnitParameterValue dryWetMix;              // (CrossFade, 0->100, 100)
    AudioUnitParameterValue gain;                   // (Decibels, -20->20, 0)
    AudioUnitParameterValue minDelayTime;           // (Secs, 0.0001->1.0, 0.008)
    AudioUnitParameterValue maxDelayTime;           // (Secs, 0.0001->1.0, 0.050)
    AudioUnitParameterValue decayTimeAt0Hz;         // (Secs, 0.001->20.0, 1.0)
    AudioUnitParameterValue decayTimeAtNyquist;     // (Secs, 0.001->20.0, 0.5)
    AudioUnitParameterValue randomizeReflections;   // (Integer, 1->1000)
};

// settings for dynamics processor unit
struct DynamicsProcessorParameters {
    AudioUnitParameterValue threshold;          // Global, dB, -40->20, -20
    AudioUnitParameterValue headRoom;           // Global, dB, 0.1->40.0, 5
    AudioUnitParameterValue expansionRatio;     // Global, rate, 1->50.0, 2
    AudioUnitParameterValue expansionThreshold; // Global, dB
    AudioUnitParameterValue attackTime;         // Global, secs, 0.0001->0.2, 0.001
    AudioUnitParameterValue releaseTime;        // Global, secs, 0.01->3, 0.05
    AudioUnitParameterValue masterGain;         // Global, dB, -40->40, 0
};

#define HWAudioControllerSamplerCount   5
#define kAlwaysRecreateSamplers     0

// used for bouncing audio
typedef void (^progress_block_t)(float);

@interface AudioController : NSObject {
}

+ (AudioController*)sharedAudioController;

#if AUDIOBUS_SUPPORT
@property (nonatomic, strong) ABAudiobusController *audiobusController;
#endif

@property (nonatomic, strong) AEAudioUnitChannel *metronome;
@property (nonatomic, strong) AEAudioUnitFilter *reverb, *compressor;
@property (nonatomic,strong) AEAudioFilePlayer *filePlayer;
@property (nonatomic,strong) NSIndexPath *demoPlayingIndexPath;

@property (nonatomic, strong) AEAudioController *audioController;
@property (atomic,assign,readonly) UInt8 samplerCount;

@property (atomic,assign) BOOL audioGraphStarted;
@property (atomic,assign) BOOL compressionBeforeReverb;

@property (nonatomic, strong) NSMutableArray *samplers;

- (id)initWithSamplerCount:(UInt8)count;

- (void) setMixerInput: (UInt32) inputBus gain: (AudioUnitParameterValue) newGain;
- (void) setMixerInput: (UInt32) inputBus pan: (AudioUnitParameterValue) newPan;
- (void) setChannel: (UInt32) inputBus muted: (BOOL) newEnabled;
- (BOOL) getChannelMuted: (UInt32) inputBus;
- (void) setChannel: (UInt32) inputBus enabled: (BOOL) newEnabled;
- (BOOL) getChannelEnabled: (UInt32) inputBus;
- (AudioUnitParameterValue) getMixerGain: (UInt32) inputBus;
- (AudioUnitParameterValue) getMixerPan: (UInt32) inputBus;
- (void) startProcessingGraph;
- (void) stopProcessingGraph;
- (void) setReverbParams:(struct Reverb2Parameters)params;
- (void) setDynamicsProcessorParams:(struct DynamicsProcessorParameters)params;
- (void) applyReverb:(SInt32)reverbType;

- (AUNode) metronomeSamplerNode;
- (AudioUnit) metronomeSamplerUnit;
- (AUNode) samplerNodeForVoice:(UInt8)voiceNum;
- (AudioUnit) samplerUnitForVoice:(UInt8)voiceNum;
- (NSString*) instrumentNameForVoice:(UInt16)voiceNum;
- (CGPoint)levels;

- (NSArray*)getPresetNamesFor:(AudioUnit)unit;
- (void) setUnit:(AudioUnit)unit preset:(SInt32)presetNumber;

- (void)updatePresetCountTo:(UInt16)voiceCount;
- (NSUInteger)numberOfActiveSamplers;
- (void) releasePresetForVoice:(UInt16)voiceNum;
- (void) releaseAllPresets;
- (void) createPresetForVoice:(UInt16)voiceNum;
- (void) createPresetsForVoices:(UInt16)voiceCount;
- (BOOL) createPresetFromSamplesIn: (NSString*)subpath withExtension:(NSString*)ext forVoice:(UInt16)voiceNum;
- (void) loadFromPresetURL: (NSURL *)presetURL forVoice:(UInt16)voiceNum;

- (void) setChannel:(UInt32) inputBus pitchBend:(AudioUnitParameterValue)bend;
- (void) setChannel:(UInt32) inputBus modWheel:(AudioUnitParameterValue)mod;
- (BOOL)isVoiceOn:(UInt16)voiceNum;
- (void)voiceOn:(UInt16)voiceNum pitch:(NoteInstanceID)pitch velocity:(UInt8)velocity;
- (void)voiceOff:(UInt16)voiceNum note:(NoteInstanceID)note;
- (void)voiceOff:(unsigned short)voiceNum;
- (void)voicePitchChange:(unsigned short)voiceNum pitch:(short)pitch velocity:(short)velocity;

- (void)sendAllNotesOff;

- (void)enableReverb;
- (void)disableReverb;
- (void)enableDynamicsProcessor;
- (void)disableDynamicsProcessor;

- (void)bouncePlayer:(MusicPlayer)player duration:(MusicTimeStamp)sequenceLength outputPath:(NSString*)outputFilePath progress:(progress_block_t)block;

@end
