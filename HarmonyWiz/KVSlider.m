//
//  KVSlider.m
//  Accompanist
//
//  Created by Ruben Zilibowitz on 15/10/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import "KVSlider.h"

@implementation KVSlider

-(id) initWithFrame: (CGRect) frame {
    if (self = [super initWithFrame: frame]) {
        
        [self addTarget: self
                 action: @selector(valueChanged:)
       forControlEvents: UIControlEventValueChanged];
    }
    
    return self;
}

- (void)valueChanged: (UISlider *) control {
    self.kvValue = control.value;
}

- (float)kvValue {
    return self.value;
}

- (void)setKvValue : (float) value {
    self.value = value;
}

@end
