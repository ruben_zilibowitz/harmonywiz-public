//
//  HarmonyWizPurchaseSamplesViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "CargoManager/CargoManager.h"

@interface HarmonyWizPurchaseSamplesViewController : UITableViewController <CargoManagerUIDelegate>

@end
