//
//  NSString+HWVerticalAlign.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/03/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    HWVerticalTextAlignmentTop,
    HWVerticalTextAlignmentMiddle,
    HWVerticalTextAlignmentMiddleWithTail,
    HWVerticalTextAlignmentBottom
} HWVerticalTextAlignment;

@interface NSString (HWVerticalAlign)
- (void)drawInRect:(CGRect)rect withFont:(UIFont *)font color:(UIColor*)color verticalAlignment:(HWVerticalTextAlignment)vAlign;
- (void)drawInRect:(CGRect)rect withFont:(UIFont *)font color:(UIColor*)color lineBreakMode:(NSLineBreakMode)lineBreakMode verticalAlignment:(HWVerticalTextAlignment)vAlign;
- (void)drawInRect:(CGRect)rect withFont:(UIFont *)font color:(UIColor*)color lineBreakMode:(NSLineBreakMode)lineBreakMode alignment:(NSTextAlignment)alignment verticalAlignment:(HWVerticalTextAlignment)vAlign;
@end
