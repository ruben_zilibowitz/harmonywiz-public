//
//  HWData.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 12/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HarmonyWizSong.h"

@interface HWData : NSObject <NSCoding>
@property (nonatomic,strong) HarmonyWizSong *song;
@property (nonatomic,strong) NSError *openingSongError;
@end
