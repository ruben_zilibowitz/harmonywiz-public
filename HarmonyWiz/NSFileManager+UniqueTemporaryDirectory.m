//
//  NSFileManager+UniqueTemporaryDirectory.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/08/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "NSFileManager+UniqueTemporaryDirectory.h"

@implementation NSFileManager (UniqueTemporaryDirectory)

- (NSString*)createUniqueTemporaryDirectory {
    NSString* templateStr = [NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.XXXXXXXX"];
    char temp[templateStr.length + 1];
    
    strcpy(temp, [templateStr cStringUsingEncoding:NSASCIIStringEncoding]);
    
    int fd = mkstemp(temp);
    char filename[PATH_MAX];
    if (fcntl(fd, F_GETPATH, filename) == -1) {
        NSLog(@"Could not create temporary file");
        return nil;
    }
    
    NSString *result = [NSString stringWithCString:filename encoding:NSASCIIStringEncoding];
    NSError *error;
    if (![self removeItemAtPath:result error:&error]) {
        NSLog(@"Remove temporary file error: %@", error.localizedDescription);
        return nil;
    }
    if (![self createDirectoryAtPath:result
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error]) {
        NSLog(@"Create directory error: %@", error.localizedDescription);
        return nil;
    }
    return result;
}

@end
