//
//  AudioController.m
//  iPhoneAudio
//
//  Created by Ruben Zilibowitz on 4/06/11.
//  Copyright 2011 N.A. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "AudioController.h"
#import "NSString+Paths.h"
#import "HarmonyWizTrack.h"
#import "HWExceptions.h"
#import "HWAppDelegate.h"
#import "AERecorder.h"

#include <AssertMacros.h>
#include <vector>

// instrument settings
#define kCorrectEndLoopPoint    false
#define kSaveAUPreset           NO

enum ZoneSelectType {
    kZoneSelect_All = 0,
    kZoneSelect_Random,
    kZoneSelect_RoundRobin
};

NSString * const HWAudiobusAPIKey = @"hiddenkey";

extern NSString * const HWCancelAudioBounce;

unsigned long const HWLoopEndPointFramesToCorrectBy = 2;
float const HWDefaultReleaseTime = 1.f;
float const HWDefaultAttackTime = 0.f;
ZoneSelectType const HWDefaultZoneSelectType = kZoneSelect_RoundRobin;
AudioUnitParameterValue const HWDynamicsGain = 4.f;

using namespace std;

@interface AudioController () {
    vector<NoteInstanceID> noteID;
    vector<UInt32> samplerNote;
    vector<UInt32> samplerVelocity;
    vector<BOOL> samplerPlaying;
    AEChannelGroupRef instrumentsGroup;
}

@property (nonatomic, strong) NSMutableArray *loadedInstrumentNames;
@property (nonatomic, assign) BOOL bounceCancelled;
//@property (nonatomic, assign) BOOL connectedToIAA;
@end

@implementation AudioController

- (id)initWithSamplerCount:(UInt8)count {
    self = [super init];
    if (self) {
        self.audioController = [[AEAudioController alloc]
                                initWithAudioDescription:[AEAudioController nonInterleavedFloatStereoAudioDescription]  // need to use nonInterleavedFloatStereoAudioDescription otherwise filters crash in TAAE 1.5
                                inputEnabled:NO];
        
        _samplerCount = count;
        noteID.resize(self.samplerCount);
        samplerNote.resize(self.samplerCount);
        samplerVelocity.resize(self.samplerCount);
        samplerPlaying.resize(self.samplerCount);
        
        self.loadedInstrumentNames = [NSMutableArray arrayWithCapacity:count];
        for (NSUInteger i = 0; i < count; i++)
            [self.loadedInstrumentNames addObject:LinguiLocalizedString(@"None", @"No instrument selected")];
        
        {
            self.samplers = [NSMutableArray arrayWithCapacity:self.samplerCount];
            for (NSUInteger idx = 0; idx < self.samplerCount; idx++) {
                [self.samplers addObject:[NSNull null]];
            }
            instrumentsGroup = [self.audioController createChannelGroup];
            
            // add samplers
#if 0
            {
                AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_MusicDevice, kAudioUnitSubType_Sampler);
                for (NSUInteger idx = 0; idx < self.samplerCount; idx++) {
                    NSError *error = NULL;
                    AEAudioUnitChannel *sampler = [[AEAudioUnitChannel alloc]
                                initWithComponentDescription:component
                                audioController:_audioController
                                error:&error];
                    if ( !sampler ) {
                        NSLog(@"%@", error);
                        recordError(error)
                    }
                    else {
                        sampler.volume = 1.0;
                        sampler.channelIsMuted = NO;
                        [array addObject:sampler];
                    }
                }
                self.samplers = [NSArray arrayWithArray:array];
                [self.audioController addChannels:self.samplers toChannelGroup:instrumentsGroup];
            }
#endif
            
            // metronome
            {
                AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_MusicDevice, kAudioUnitSubType_Sampler);
                NSError *error = NULL;
                self.metronome = [[AEAudioUnitChannel alloc] initWithComponentDescription:component];
                if ( !self.metronome ) {
                    NSLog(@"%@", error);
                    recordError(error)
                }
                else {
                    self.metronome.volume = 0.25f;
                    self.metronome.channelIsMuted = NO;
                    [self.audioController addChannels:@[self.metronome]];
                    [self loadMetronome];
                }
            }
            
            self.audioController.masterOutputVolume = 1.f;
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelBounce:) name:HWCancelAudioBounce object:nil];
        }
    }
    return self;
}

- (void) applyReverb:(SInt32)reverbType {
    if (self.reverb == nil) {
        return;
    }
    
    if (SYSTEM_VERSION_LESS_THAN(@"7")) {
        switch (reverbType) {
            case 0: {
                Reverb2Parameters params = { 50.f,1.f,0.002f,0.014f,0.3f,0.08f,1.f };
                [self setReverbParams:params];
            } break;
            case 1: {
                Reverb2Parameters params = { 50.f,1.f,0.005f,0.045f,0.85f,0.3f,1.f };
                [self setReverbParams:params];
            } break;
            case 2: {
                Reverb2Parameters params = { 50.f,1.f,0.01f,0.07f,1.f,0.6f,1.f };
                [self setReverbParams:params];
            } break;
            case 3: {
                Reverb2Parameters params = { 50.f,1.f,0.01f,0.07f,2.1f,1.1f,1.f };
                [self setReverbParams:params];
            } break;
            case 4: {
                Reverb2Parameters params = { 50.f,1.f,0.018f,0.1f,2.9f,2.f,1.f };
                [self setReverbParams:params];
            } break;
            case 5: {
                Reverb2Parameters params = { 50.f,1.f,0.005f,0.045f,7.f,2.5f,1.f };
                [self setReverbParams:params];
            } break;
            case 6: {
                Reverb2Parameters params = { 50.f,1.f,0.01f,0.028f,1.2f,0.37f,1.f };
                [self setReverbParams:params];
            } break;
            case 7: {
                Reverb2Parameters params = { 50.f,1.f,0.01f,0.028f,2.f,0.67f,1.f };
                [self setReverbParams:params];
            } break;
            case 8: {
                Reverb2Parameters params = { 50.f,1.f,0.03f,0.2f,9.f,3.f,1.f };
                [self setReverbParams:params];
            } break;
            case 9: {
                Reverb2Parameters params = { 50.f,1.f,0.0065f,0.08f,1.f,0.6f,1.f };
                [self setReverbParams:params];
            } break;
            case 10: {
                Reverb2Parameters params = { 50.f,1.f,0.006f,0.04f,2.1f,1.1f,1.f };
                [self setReverbParams:params];
            } break;
            case 11: {
                Reverb2Parameters params = { 50.f,1.f,0.008f,0.055f,2.1f,1.1f,1.f };
                [self setReverbParams:params];
            } break;
            case 12: {
                Reverb2Parameters params = { 50.f,1.f,0.006f,0.04f,2.9f,2.f,1. };
                [self setReverbParams:params];
            } break;
            default:
                [NSException raise:@"HarmonyWiz" format:@"unkown reverb type"];
                break;
        }
    }
    else {
        [self setUnit:self.reverb.audioUnit preset:reverbType];
    }
}

- (void)enableReverb {
    if (self.reverb == nil) {
        AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_Effect, kAudioUnitSubType_Reverb2);
        NSError *error = NULL;
        self.reverb = [[AEAudioUnitFilter alloc] initWithComponentDescription:component];
        if ( !_reverb ) {
            NSLog(@"%@", error);
            recordError(error)
        }
        else {
            [self.audioController addFilter:self.reverb toChannelGroup:instrumentsGroup];
            //                    [self printReverbPresets];
        }
        
        // nb: for some reason, cannot set a preset here
        // first must set params directly. Then can set preset.
        struct Reverb2Parameters params = { 50.f,1.f,0.01f,0.07f,1.f,0.6f,1.f };
        [self setReverbParams:params];
    }
}

- (void)disableReverb {
    if (self.reverb) {
        [self.audioController removeFilter:self.reverb fromChannelGroup:instrumentsGroup];
        self.reverb = nil;
    }
}

- (void)enableDynamicsProcessor {
    if (self.compressor == nil) {
        AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_Effect, kAudioUnitSubType_DynamicsProcessor);
        NSError *error = NULL;
        self.compressor = [[AEAudioUnitFilter alloc] initWithComponentDescription:component];
        if ( !_compressor ) {
            NSLog(@"%@", error);
            recordError(error)
        }
        else {
            [self.audioController addFilter:self.compressor toChannelGroup:instrumentsGroup];
            //                    [self.audioController addFilter:self.compressor];
        }
        
        [self setCompressorPreset:2 masterGain:HWDynamicsGain];
    }
}

- (void)disableDynamicsProcessor {
    if (self.compressor) {
        [self.audioController removeFilter:self.compressor fromChannelGroup:instrumentsGroup];
        self.compressor = nil;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setMixerInput: (UInt32) inputBus gain: (AudioUnitParameterValue) newGain {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        [sampler setVolume:newGain];
    }
}

- (void) setMixerInput: (UInt32) inputBus pan: (AudioUnitParameterValue) newPan {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        [sampler setPan:newPan];
    }
}

- (void) setChannel: (UInt32) inputBus enabled: (BOOL) newEnabled {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        [sampler setChannelIsPlaying:newEnabled];
    }
}

- (BOOL) getChannelEnabled: (UInt32) inputBus {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        return [sampler channelIsPlaying];
    }
    return NO;
}

- (void) setChannel:(UInt32) inputBus pitchBend:(AudioUnitParameterValue)bend {
    // bend value -8192 - 8191
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        HWThrowIfAudioError(AudioUnitSetParameter([sampler audioUnit],kAUGroupParameterID_PitchBend,kAudioUnitScope_Group, 0, bend, 0));
    }
}

- (void) setChannel:(UInt32) inputBus modWheel:(AudioUnitParameterValue)mod {
    // mod value 0 < 128
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        HWThrowIfAudioError(AudioUnitSetParameter([sampler audioUnit],kAUGroupParameterID_ModWheel,kAudioUnitScope_Group, 0, mod, 0));
    }
}

- (void) setChannel: (UInt32) inputBus muted: (BOOL) newMuted {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        [sampler setChannelIsMuted:newMuted];
    }
}

- (BOOL) getChannelMuted: (UInt32) inputBus {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        return [sampler channelIsMuted];
    }
    return NO;
}

- (CGPoint)levels {
    Float32 power, peak;
//    [self.audioController averagePowerLevel:&power peakHoldLevel:&peak forGroup:instrumentsGroup];
    [self.audioController outputAveragePowerLevel:&power peakHoldLevel:&peak];
    return CGPointMake(power, peak);
}

- (AudioUnitParameterValue) getMixerGain: (UInt32) inputBus {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        return [sampler volume];
    }
    return 0;
}

- (AudioUnitParameterValue) getMixerPan: (UInt32) inputBus {
    id sampler = [self.samplers objectAtIndex:inputBus];
    if (sampler != nil && sampler != [NSNull null]) {
        return [sampler pan];
    }
    return 0;
}

- (void) printAUFactoryPresets:(AudioUnit)unit {
    CFArrayRef presets = nil;
    UInt32 sz = sizeof (presets);
    
    HWThrowIfAudioError(AudioUnitGetProperty (unit,
                                              kAudioUnitProperty_FactoryPresets,
                                              kAudioUnitScope_Global,
                                              0, &presets, &sz));
    
    {
        CFIndex idx = CFArrayGetCount(presets);
        NSLog(@"presets counts = %ld", idx);
        for (CFIndex i = 0; i < idx; i++) {
            AUPreset *preset = (AUPreset*)CFArrayGetValueAtIndex(presets, i);
            NSNumber *presetNumber = [NSNumber numberWithInt:preset->presetNumber];
            NSString *presetName = [(__bridge NSString *)preset->presetName copy];
            NSLog(@"%ld %@", (long)presetNumber.integerValue, presetName);
        }
        CFRelease(presets);
    }
}

- (NSArray*)getPresetNamesFor:(AudioUnit)unit {
    NSMutableArray *result = [NSMutableArray array];
    
    CFArrayRef presets = nil;
    UInt32 sz = sizeof (presets);
    
    HWThrowIfAudioError(AudioUnitGetProperty (unit,
                                              kAudioUnitProperty_FactoryPresets,
                                              kAudioUnitScope_Global,
                                              0, &presets, &sz));
    
    {
        CFIndex idx = CFArrayGetCount(presets);
        for (CFIndex i = 0; i < idx; i++) {
            AUPreset *preset = (AUPreset*)CFArrayGetValueAtIndex(presets, i);
//            NSNumber *presetNumber = [NSNumber numberWithInt:preset->presetNumber];
            NSString *presetName = [(__bridge NSString *)preset->presetName copy];
            
            [result addObject:presetName];
        }
        CFRelease(presets);
    }
    
    return result;
}

- (void)printReverbPresets {
    for (SInt32 presetNumber = 0; presetNumber < 13; presetNumber++) {
        AUPreset preset = {presetNumber, NULL};
        HWThrowIfAudioError(AudioUnitSetProperty(self.reverb.audioUnit, kAudioUnitProperty_PresentPreset, kAudioUnitScope_Global, 0, &preset, sizeof(preset)));
        
        AudioUnitParameterValue value[7];
        
        AudioUnitGetParameter(self.reverb.audioUnit, kReverb2Param_DryWetMix, kAudioUnitScope_Global, 0, &value[0]);
        AudioUnitGetParameter(self.reverb.audioUnit, kReverb2Param_Gain, kAudioUnitScope_Global, 0, &value[1]);
        AudioUnitGetParameter(self.reverb.audioUnit, kReverb2Param_MinDelayTime, kAudioUnitScope_Global, 0, &value[2]);
        AudioUnitGetParameter(self.reverb.audioUnit, kReverb2Param_MaxDelayTime, kAudioUnitScope_Global, 0, &value[3]);
        AudioUnitGetParameter(self.reverb.audioUnit, kReverb2Param_DecayTimeAt0Hz, kAudioUnitScope_Global, 0, &value[4]);
        AudioUnitGetParameter(self.reverb.audioUnit, kReverb2Param_DecayTimeAtNyquist, kAudioUnitScope_Global, 0, &value[5]);
        AudioUnitGetParameter(self.reverb.audioUnit, kReverb2Param_RandomizeReflections, kAudioUnitScope_Global, 0, &value[6]);
        
        NSLog(@"%f %f %f %f %f %f %f", value[0], value[1], value[2], value[3], value[4], value[5], value[6]);
    }
}

- (void)printDynamicsProcessorPresets {
    for (SInt32 presetNumber = 0; presetNumber < 5; presetNumber++) {
        AUPreset preset = {presetNumber, NULL};
        HWThrowIfAudioError(AudioUnitSetProperty(self.compressor.audioUnit, kAudioUnitProperty_PresentPreset, kAudioUnitScope_Global, 0, &preset, sizeof(preset)));
        
        AudioUnitParameterValue value[10];
        
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_Threshold, kAudioUnitScope_Global, 0, &value[0]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_HeadRoom, kAudioUnitScope_Global, 0, &value[1]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_ExpansionRatio, kAudioUnitScope_Global, 0, &value[2]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_ExpansionThreshold, kAudioUnitScope_Global, 0, &value[3]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_AttackTime, kAudioUnitScope_Global, 0, &value[4]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_ReleaseTime, kAudioUnitScope_Global, 0, &value[5]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_MasterGain, kAudioUnitScope_Global, 0, &value[6]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_CompressionAmount, kAudioUnitScope_Global, 0, &value[7]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_InputAmplitude, kAudioUnitScope_Global, 0, &value[8]);
        AudioUnitGetParameter(self.compressor.audioUnit, kDynamicsProcessorParam_OutputAmplitude, kAudioUnitScope_Global, 0, &value[9]);
        
        NSLog(@"%f %f %f %f %f %f %f %f %f %f", value[0], value[1], value[2], value[3], value[4], value[5], value[6], value[7], value[8], value[9]);
    }
}

- (void) setUnit:(AudioUnit)unit preset:(SInt32)presetNumber {
    AUPreset preset = {presetNumber, NULL};
    HWThrowIfAudioError(AudioUnitSetProperty(unit, kAudioUnitProperty_PresentPreset, kAudioUnitScope_Global, 0, &preset, sizeof(preset)));
}

- (void) setCompressorPreset:(SInt32)presetNumber masterGain:(AudioUnitParameterValue)masterGain {
    
//    [self printAUFactoryPresets:self.compressorUnit];
    /*
     presets counts = 6
     0 Fast and Smooth
     1 Hard
     2 Light
     3 Light Gate
     4 Medium Gate
     5 Hard Gate
     */
    
    AUPreset preset = {presetNumber, NULL};
    HWThrowIfAudioError(AudioUnitSetProperty(self.compressor.audioUnit, kAudioUnitProperty_PresentPreset, kAudioUnitScope_Global, 0, &preset, sizeof(preset)));
    
    HWThrowIfAudioError(AudioUnitSetParameter(self.compressor.audioUnit,kDynamicsProcessorParam_MasterGain,kAudioUnitScope_Global, 0, masterGain, 0));
}

- (void) setReverbParams:(struct Reverb2Parameters)params {
    
    if (self.reverb == nil) {
        return;
    }
    
    HWThrowIfAudioError(AudioUnitSetParameter(self.reverb.audioUnit,kReverb2Param_DryWetMix,kAudioUnitScope_Global, 0, params.dryWetMix, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.reverb.audioUnit,kReverb2Param_Gain,kAudioUnitScope_Global, 0, params.gain, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.reverb.audioUnit,kReverb2Param_MinDelayTime,kAudioUnitScope_Global, 0, params.minDelayTime, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.reverb.audioUnit,kReverb2Param_MaxDelayTime,kAudioUnitScope_Global, 0, params.maxDelayTime, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.reverb.audioUnit,kReverb2Param_DecayTimeAt0Hz,kAudioUnitScope_Global, 0, params.decayTimeAt0Hz, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.reverb.audioUnit,kReverb2Param_DecayTimeAtNyquist,kAudioUnitScope_Global, 0, params.decayTimeAtNyquist, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.reverb.audioUnit,kReverb2Param_RandomizeReflections,kAudioUnitScope_Global, 0, params.randomizeReflections, 0));
}

- (void) setDynamicsProcessorParams:(struct DynamicsProcessorParameters)params {
    
    if (self.compressor == nil) {
        return;
    }
    
    HWThrowIfAudioError(AudioUnitSetParameter(self.compressor.audioUnit,kDynamicsProcessorParam_Threshold,kAudioUnitScope_Global, 0, params.threshold, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.compressor.audioUnit,kDynamicsProcessorParam_HeadRoom,kAudioUnitScope_Global, 0, params.headRoom, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.compressor.audioUnit,kDynamicsProcessorParam_ExpansionRatio,kAudioUnitScope_Global, 0, params.expansionRatio, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.compressor.audioUnit,kDynamicsProcessorParam_ExpansionThreshold,kAudioUnitScope_Global, 0, params.expansionThreshold, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.compressor.audioUnit,kDynamicsProcessorParam_AttackTime,kAudioUnitScope_Global, 0, params.attackTime, 0));
    HWThrowIfAudioError(AudioUnitSetParameter(self.compressor.audioUnit,kDynamicsProcessorParam_MasterGain,kAudioUnitScope_Global, 0, params.masterGain, 0));
}

#if AUDIOBUS_SUPPORT
- (void)setupAudiobus {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
        if (! self.audiobusController) {
            self.audioController.allowMixingWithOtherApps = YES;
            self.audiobusController = [[ABAudiobusController alloc] initWithApiKey:HWAudiobusAPIKey];
            
            ABSenderPort *sender = [[ABSenderPort alloc] initWithName:@"Audio Output" title:@"HarmonyWiz Output"
                                            audioComponentDescription:(AudioComponentDescription) {
                                                .componentType = kAudioUnitType_RemoteGenerator,
                                                .componentSubType = 'aout',
                                                .componentManufacturer = 'HaWz' }];
            [self.audiobusController addSenderPort:sender];
            self.audioController.audiobusSenderPort = sender;
        }
    }
}
#endif

- (void) startProcessingGraph {
    if (! self.audioGraphStarted) {
        
        NSError *error = NULL;
        BOOL result = [self.audioController start:&error];
        if ( !result ) {
            NSLog(@"%@", error);
            recordError(error)
        }
        
        self.audioGraphStarted = YES;
        
#if AUDIOBUS_SUPPORT
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
            // Audiobus
            if (! self.audiobusController) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setupAudiobus];
                });
            }
        }
#endif
    }
}

- (void) stopProcessingGraph {
    // nb: need to check if audiobus is connected. Should not stop audio graph if
    // it is connected.
#if AUDIOBUS_SUPPORT
    if (self.audioGraphStarted && ! self.audiobusController.connected)
#else
        if (self.audioGraphStarted)
#endif
    {
        [self.audioController stop];
        self.audioGraphStarted = NO;
    }
}

- (AUNode) metronomeSamplerNode {
    return self.metronome.audioGraphNode;
}

- (AudioUnit) metronomeSamplerUnit {
    return self.metronome.audioUnit;
}

- (AudioUnit) samplerUnitForVoice:(UInt8)voiceNum {
    NSAssert(voiceNum < self.samplerCount, @"Illegal voice number");
    
    id sampler = [self.samplers objectAtIndex:voiceNum];
    if (sampler != nil && sampler != [NSNull null]) {
        return [sampler audioUnit];
    }
    return nil;
}

- (AUNode) samplerNodeForVoice:(UInt8)voiceNum {
    NSAssert(voiceNum < self.samplerCount, @"Illegal voice number");
    
    id sampler = [self.samplers objectAtIndex:voiceNum];
    if (sampler != nil && sampler != [NSNull null]) {
        return [sampler audioGraphNode];
    }
    else {
        // should not get here
        [NSException raise:@"HarmonyWiz" format:@"sampler not valid"];
    }
    return 0;
}

- (NSString*) instrumentNameForVoice:(UInt16)voiceNum {
    NSAssert(voiceNum < self.samplerCount, @"Illegal voice number");
    return [self.loadedInstrumentNames objectAtIndex:voiceNum];
}

- (BOOL) loadMetronome {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Metronome" ofType:nil];
    NSError *error;
    
    NSArray *filenames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path
                                                                             error:&error];
    if (error) {
        return NO;
    }
    
    NSMutableArray *filesArrayMutable = [NSMutableArray array];
    for (NSString *name in filenames) {
        if ([name hasSuffix:@"aif"]) {
            NSURL *url = [NSURL fileURLWithPath:[path stringByAppendingPathComponent:name]];
            [filesArrayMutable addObject:url];
        }
    }
    NSArray *filesArray = [NSArray arrayWithArray:filesArrayMutable];
    CFArrayRef urlArrayRef = (__bridge CFArrayRef)filesArray;
    
    HWThrowIfAudioError(AudioUnitSetProperty(self.metronome.audioUnit,
                                             kAUSamplerProperty_LoadAudioFiles,
                                             kAudioUnitScope_Global,
                                             0,
                                             &urlArrayRef,
                                             sizeof(urlArrayRef)));
    
    return YES;
}

- (void)releaseAllPresets {
    NSArray *nonNullSamplers = [self.samplers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self != %@", [NSNull null]]];
    [self.audioController removeChannels:nonNullSamplers fromChannelGroup:instrumentsGroup];
    for (int i = 0; i < HWAudioControllerSamplerCount; i++) {
        [self.samplers replaceObjectAtIndex:i withObject:[NSNull null]];
        [self.loadedInstrumentNames replaceObjectAtIndex:i withObject:LinguiLocalizedString(@"None", @"No instrument selected")];
    }
}

- (void) releasePresetForVoice:(UInt16)voiceNum {
    if ([self.samplers objectAtIndex:voiceNum] != [NSNull null]) {
        [self.audioController removeChannels:@[[self.samplers objectAtIndex:voiceNum]] fromChannelGroup:instrumentsGroup];
        [self.samplers replaceObjectAtIndex:voiceNum withObject:[NSNull null]];
        
        [self.loadedInstrumentNames replaceObjectAtIndex:voiceNum withObject:LinguiLocalizedString(@"None", @"No instrument selected")];
    }
}

- (void)createPresetForVoice:(UInt16)voiceNum {
    AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_MusicDevice, kAudioUnitSubType_Sampler);
    NSError *error = NULL;
    AEAudioUnitChannel *sampler = [[AEAudioUnitChannel alloc] initWithComponentDescription:component];
    if ( !sampler ) {
        NSLog(@"%@", error.localizedDescription);
        recordError(error)
    }
    else {
        sampler.volume = 1.0;
        sampler.channelIsMuted = NO;
        [self.samplers replaceObjectAtIndex:voiceNum withObject:sampler];
        [self.audioController addChannels:@[sampler] toChannelGroup:instrumentsGroup];
    }
}

- (NSUInteger)numberOfActiveSamplers {
    return [[self.samplers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self != nil"]] count];
}

- (void)updatePresetCountTo:(UInt16)voiceCount {
#if kAlwaysRecreateSamplers
        [self releaseAllPresets];
        [self createPresetsForVoices:voiceCount];
#else
        const NSUInteger active = self.numberOfActiveSamplers;
        if (active > voiceCount) {
            [self.audioController removeChannels:[self.samplers subarrayWithRange:NSMakeRange(voiceCount, active-voiceCount)] fromChannelGroup:instrumentsGroup];
            for (UInt16 idx = voiceCount; idx < active; idx++) {
                [self.samplers replaceObjectAtIndex:idx withObject:[NSNull null]];
                [self.loadedInstrumentNames replaceObjectAtIndex:idx withObject:LinguiLocalizedString(@"None", @"No instrument selected")];
            }
        }
        else if (active < voiceCount) {
            for (UInt16 idx = active; idx < voiceCount; idx++) {
                AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_MusicDevice, kAudioUnitSubType_Sampler);
                NSError *error = NULL;
                AEAudioUnitChannel *sampler = [[AEAudioUnitChannel alloc] initWithComponentDescription:component];
                if ( !sampler ) {
                    NSLog(@"%@", error.localizedDescription);
                    recordError(error)
                }
                else {
                    sampler.volume = 1.0;
                    sampler.channelIsMuted = NO;
                    [self.samplers replaceObjectAtIndex:idx withObject:sampler];
                }
            }
            [self.audioController addChannels:[self.samplers subarrayWithRange:NSMakeRange(active, voiceCount-active)] toChannelGroup:instrumentsGroup];
        }
        else {
            // nothing to do here
        }
#endif
}

- (void)createPresetsForVoices:(UInt16)voiceCount {
    AudioComponentDescription component = AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_MusicDevice, kAudioUnitSubType_Sampler);
    
    NSMutableArray *newSamplers = [NSMutableArray arrayWithCapacity:voiceCount];
    for (UInt16 voiceNum = 0; voiceNum < voiceCount; voiceNum++) {
        NSError *error = NULL;
        AEAudioUnitChannel *sampler = [[AEAudioUnitChannel alloc] initWithComponentDescription:component];
        if ( !sampler ) {
            NSLog(@"%@", error.localizedDescription);
            recordError(error)
        }
        else {
            sampler.volume = 1.0;
            sampler.channelIsMuted = NO;
            [self.samplers replaceObjectAtIndex:voiceNum withObject:sampler];
            [newSamplers addObject:sampler];
        }
    }
    [self.audioController addChannels:newSamplers toChannelGroup:instrumentsGroup];
}

- (BOOL) createPresetFromSamplesIn:(NSString*)subpath withExtension:(NSString*)ext forVoice:(UInt16)voiceNum {
    
    if (subpath == nil || subpath.length == 0) {
        recordError([NSError errorWithDomain:@"createPresetFromSamplesIn subpath empty" code:1 userInfo:nil])
        return NO;
    }
    
    AudioUnit auSampler = [self samplerUnitForVoice:voiceNum];
    
    // default settings
    float releaseTime = HWDefaultReleaseTime;
    float attackTime = HWDefaultAttackTime;
    ZoneSelectType zoneSelect = HWDefaultZoneSelectType;
    
    // get the path
    NSString *path = [[NSString samplerFilesPath] stringByAppendingPathComponent:subpath];
    if (! [[NSFileManager defaultManager] fileExistsAtPath:path]) {
        path = [[NSBundle mainBundle] pathForResource:subpath ofType:nil inDirectory:@"Sampler Files"];
    }
    
    // check for preset info
    {
        NSString *hwzpresetPath = [[path stringByAppendingPathComponent:subpath.lastPathComponent] stringByAppendingPathExtension:@"hwzpresetsettings"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:hwzpresetPath]) {
            NSDictionary *hwzpreset = [NSDictionary dictionaryWithContentsOfFile:hwzpresetPath];
            for (id key in hwzpreset) {
                if ([key isEqualToString:@"zone select"]) {
                    zoneSelect = (ZoneSelectType)[[hwzpreset objectForKey:key] intValue];
                }
                else if ([key isEqualToString:@"release time"]) {
                    releaseTime = [[hwzpreset objectForKey:key] floatValue];
                }
                else if ([key isEqualToString:@"attack time"]) {
                    attackTime = [[hwzpreset objectForKey:key] floatValue];
                }
            }
        }
    }
    
    NSError *error;
    NSArray *filenames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:&error];
    if (error) {
        recordError(error)
        return NO;
    }
    NSMutableArray *filesArrayMutable = [NSMutableArray array];
    for (NSString *name in filenames) {
        if ([name hasSuffix:ext]) {
            NSURL *url = [NSURL fileURLWithPath:[path stringByAppendingPathComponent:name]];
            [filesArrayMutable addObject:url];
        }
    }
    NSArray *filesArray = [NSArray arrayWithArray:filesArrayMutable];
    CFArrayRef urlArrayRef = (__bridge CFArrayRef)filesArray;
    
    {
        OSStatus status = AudioUnitSetProperty(auSampler,
                                               kAUSamplerProperty_LoadAudioFiles,
                                               kAudioUnitScope_Global,
                                               0,
                                               &urlArrayRef,
                                               sizeof(urlArrayRef));
        if (status != noErr) {
            NSLog(@"Failed to load samples. OSStatus was %ld", (long)status);
            
//            NSError *error = nil;
//            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
//            if (error) {
//                NSLog(@"%@", error.localizedDescription);
//            }
            
            recordError([NSError errorWithDomain:@"AudioUnitSetProperty" code:status userInfo:nil])
            return NO;
        }
    }
    
    [self.loadedInstrumentNames replaceObjectAtIndex:voiceNum withObject:[subpath lastPathComponent]];
    
    {
        CFPropertyListRef classInfoPList = 0;
        UInt32 size = sizeof(CFPropertyListRef);
        
        HWThrowIfAudioError(AudioUnitGetProperty(
                                      auSampler,
                                      kAudioUnitProperty_ClassInfo,
                                      kAudioUnitScope_Global,
                                      0,
                                      &classInfoPList,
                                      &size
                                      ));
        
        if (classInfoPList != 0) {
            NSMutableDictionary *mutableInfo = (NSMutableDictionary *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFDictionaryRef)classInfoPList, kCFPropertyListMutableContainers));
            CFRelease(classInfoPList);
            
            NSDictionary *instrument = [mutableInfo objectForKey:@"Instrument"];
            NSArray *layers = [instrument objectForKey:@"Layers"];
            NSMutableDictionary *layer0 = [layers objectAtIndex:0];
            NSArray *envelopes = [layer0 objectForKey:@"Envelopes"];
            NSArray *zones = [layer0 objectForKey:@"Zones"];
            
            for (NSMutableDictionary *stage in [[envelopes objectAtIndex:0] objectForKey:@"Stages"]) {
                NSNumber *curve = [stage objectForKey:@"curve"];
                NSNumber *stageNumber = [stage objectForKey:@"stage"];
                
                // this is the release time
                if (curve.integerValue == 20 && stageNumber.integerValue == 5) {
                    [stage setObject:[NSNumber numberWithDouble:releaseTime] forKey:@"time"];
                }
                
                // this is the attack time
                if (curve.integerValue == 22 && stageNumber.integerValue == 1) {
                    [stage setObject:[NSNumber numberWithDouble:attackTime] forKey:@"time"];
                }
            }
            
            for (NSMutableDictionary *zone in zones) {
                NSNumber *loopEnabled = [zone objectForKey:@"loop enabled"];
                NSNumber *loopEnd = [zone objectForKey:@"loop end"];
                NSNumber *loopStart = [zone objectForKey:@"loop start"];
                if (loopEnabled.boolValue && loopEnd.unsignedLongValue > loopStart.unsignedLongValue + HWLoopEndPointFramesToCorrectBy + 1) {
                    if (kCorrectEndLoopPoint) {
                        const unsigned long loopEndVal = loopEnd.unsignedLongValue - HWLoopEndPointFramesToCorrectBy;
                        NSNumber *newLoopEnd = [NSNumber numberWithUnsignedLong:loopEndVal];
                        [zone setObject:newLoopEnd forKey:@"loop end"];
                    }
                }
                else {
                    [zone setObject:@(NO) forKey:@"loop enabled"];
                }
            }
            
            // zone selection method for duplicate notes
            switch (zoneSelect) {
                case kZoneSelect_Random:
                    [layer0 setObject:[NSNumber numberWithInteger:32] forKey:@"zone select"];
                    break;
                    
                case kZoneSelect_RoundRobin:
                    [layer0 setObject:[NSNumber numberWithInteger:31] forKey:@"zone select"];
                    break;
                    
                default:
                    break;
            }
            
            CFPropertyListRef inClassInfo = (__bridge CFPropertyListRef)mutableInfo;
            AudioUnitSetProperty(auSampler,
                                 kAudioUnitProperty_ClassInfo,
                                 kAudioUnitScope_Global,
                                 0,
                                 &inClassInfo,
                                 sizeof(inClassInfo));
        }
    }
    
    if (kSaveAUPreset)
    {
        AUPreset myPreset;
        myPreset.presetNumber = -1; //should be less than zero ,(user preset)
        myPreset.presetName = (__bridge CFStringRef)subpath.lastPathComponent;
        UInt32 size = sizeof(AUPreset);
        
        AudioUnitSetProperty(auSampler,
                             kAudioUnitProperty_PresentPreset,
                             kAudioUnitScope_Global,
                             0,
                             &myPreset,
                             size);
        
        CFPropertyListRef presetPropertyList = 0;
        size = sizeof(CFPropertyListRef);
        HWThrowIfAudioError(AudioUnitGetProperty(
                                      auSampler,
                                      kAudioUnitProperty_ClassInfo,
                                      kAudioUnitScope_Global,
                                      0,
                                      &presetPropertyList,
                                      &size
                                      ));
        
        if (presetPropertyList != 0) {
            CFDataRef xmlData = CFPropertyListCreateXMLData(kCFAllocatorDefault, presetPropertyList);
            NSString *documentsDirectory = [NSString documentsDirectory];
            NSString *appFile = [documentsDirectory stringByAppendingPathComponent:[subpath stringByAppendingPathExtension:@"aupreset"]];
            NSData *data = [NSData dataWithData:(__bridge NSData*)xmlData];
            [data writeToFile:appFile atomically:YES];
            CFRelease(xmlData);
            
            CFRelease(presetPropertyList);
        }
    }
    
    return YES;
}

-(void) loadFromPresetURL: (NSURL *)presetURL forVoice:(UInt16)voiceNum {
    
    AudioUnit sampler = [self samplerUnitForVoice:voiceNum];
    
    CFDataRef propertyResourceData = nil;
//    Boolean status;
    SInt32 errorCode = 0;
    
    propertyResourceData = (__bridge CFDataRef)[NSData dataWithContentsOfURL:presetURL];
    
    // read from an .aupreset file specified in 'presetURL'
    // and convert into a CFData chunk
//    status = CFURLCreateDataAndPropertiesFromResource(kCFAllocatorDefault,
//                                                      (__bridge CFURLRef) presetURL,
//                                                      &propertyResourceData,
//                                                      NULL,
//                                                      NULL,
//                                                      &errorCode);
    
    // check errors
    if (propertyResourceData == nil)
        HWThrowIfAudioError(errorCode);
    
    // convert the CFData chunk into a property list
    CFPropertyListRef presetPropertyList = 0;
    CFPropertyListFormat dataFormat;
    CFErrorRef errorRef = 0;
    presetPropertyList = CFPropertyListCreateWithData(kCFAllocatorDefault,
                                                      propertyResourceData,
                                                      kCFPropertyListImmutable,
                                                      &dataFormat,
                                                      &errorRef);
    
    if (errorRef != nil)
        HWThrowIfAudioError((OSStatus)CFErrorGetCode(errorRef));
    
    if (presetPropertyList != 0) {
        // set the kAudioUnitProperty_ClassInfo property for the Sampler
        // using the property list as the value
        HWThrowIfAudioError(AudioUnitSetProperty(sampler,
                                      kAudioUnitProperty_ClassInfo,
                                      kAudioUnitScope_Global,
                                      0,
                                      &presetPropertyList,
                                      sizeof(CFPropertyListRef)));
        
        CFRelease(presetPropertyList);
    }
    
    // release the errorRef if there was one, and the CFDataRef
    if (errorRef) CFRelease(errorRef);
//    if (propertyResourceData != NULL)
//        CFRelease (propertyResourceData);
}

/*
#pragma mark - Inter-App Audio

static void IsInterAppConnectedCallback(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement) {
    NSLog(@"host connection property changed");
    [[AudioController sharedAudioController] checkHostConnected];
}

static void HostTransportStateCallback(void *inRefCon, AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement) {
    NSLog(@"host transport property changed");
    [[AudioController sharedAudioController] updateStatefromTransportCallBack];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AudioEngineHostTransportState" object:nil];
    });
}

-(void) checkStartStopGraph {
    if (self.connectedToIAA || inForeground ) {
        [self setAudioSessionActive];
        //Initialize the graph if it hasn't been already
        if (synthGraph) {
            Boolean initialized = YES;
            Check(AUGraphIsInitialized(synthGraph, &initialized));
            if (!initialized)
                Check (AUGraphInitialize (synthGraph));
        }
        [self startGraph];
    } else if(!inForeground){
        [self stopGraph];
        [self setAudioSessionInActive];
    }
}

UIImage *scaleImageToSize(UIImage *image, CGSize newSize) {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newImage;
}

-(UIImage *) getAudioUnitIcon {
    UIImage *result = nil;
    AudioUnit outputUnit = self.audioController.audioUnit;
    if (outputUnit)
        result = scaleImageToSize(AudioOutputUnitGetHostIcon(outputUnit, 114), CGSizeMake(41, 41));
    
	return result;
}

- (BOOL)checkHostConnected {
    AudioUnit outputUnit = self.audioController.audioUnit;
    if (outputUnit) {
        UInt32 connect;
        UInt32 dataSize = sizeof(UInt32);
        HWThrowIfAudioError(AudioUnitGetProperty(outputUnit, kAudioUnitProperty_IsInterAppConnected, kAudioUnitScope_Global, 0, &connect, &dataSize));
        if (connect != self.connectedToIAA) {
            self.connectedToIAA = connect;
            //Transition is from not connected to connected
            if (self.connectedToIAA) {
                [self checkStartStopGraph];
                //Get the appropriate callback info
                [self getHostCallBackInfo];
                [self getAudioUnitIcon];
            }
            //Transition is from connected to not connected;
            else {
                //If the graph is started stop it.
                if ([self isGraphStarted])
                    [self stopGraph];
                //Attempt to restart the graph
                [self checkStartStopGraph];
            }
        }
    }
    return self.connectedToIAA;
}

- (void) getHostCallBackInfo {
//    if (connectedToHost) {
//        if (callBackInfo)
//            free(callBackInfo);
//        UInt32 dataSize = sizeof(HostCallbackInfo);
//        callBackInfo = (HostCallbackInfo*) malloc(dataSize);
//        OSStatus result = AudioUnitGetProperty(audioController.audioUnit, kAudioUnitProperty_HostCallbacks, kAudioUnitScope_Global, 0, callBackInfo, &dataSize);
//        if (result != noErr) {
//            free(callBackInfo);
//            callBackInfo = NULL;
//        }
//    }
}

- (void) publishOutputAudioUnit:(AudioUnit)outputUnit {
    
	AudioComponentDescription desc = { kAudioUnitType_RemoteInstrument,'iasp','appl',0,0 };
	OSStatus result = AudioOutputUnitPublish(&desc, CFSTR("IAA Sampler Demo"), 0, outputUnit);
    if (result != noErr)
        NSLog(@"AudioOutputUnitPublish instrument result: %d", (int)result);
    
    desc = { kAudioUnitType_RemoteGenerator,'iasp','appl',0,0 };
    result = AudioOutputUnitPublish(&desc, CFSTR("IAA Sampler Demo"), 0, outputUnit);
    if (result != noErr)
        NSLog(@"AudioOutputUnitPublish generator result: %d", (int)result);
//    [self setupMidiCallBacks:&outputUnit userData:self];
}

- (void) iaaSetup {
    AudioUnit unit = self.audioController.audioUnit;
    NSLog(@"Setting up IAA");
    
    AudioUnitAddPropertyListener(unit,
                                 kAudioUnitProperty_IsInterAppConnected,
                                 IsInterAppConnectedCallback,
                                 (__bridge void*)self);
    AudioUnitAddPropertyListener(unit,
                                 kAudioOutputUnitProperty_HostTransportState,
                                 HostTransportStateCallback,
                                 (__bridge void*)self);
    
    [self publishOutputAudioUnit:unit];
}

- (void) updateStatefromTransportCallBack {
//    if (connectedToHost) {
//        if (callBackInfo) {
//            Boolean isPlaying   = hostPlaying;
//            Boolean isRecording = hostRecording;
//            Float64 outCurrentSampleInTimeLine = hostCurrentTime;
//            void * hostUserData = callBackInfo->hostUserData;
//            OSStatus result =  callBackInfo->transportStateProc2(hostUserData,
//                                                                 &isPlaying,
//                                                                 &isRecording, NULL,
//                                                                 &outCurrentSampleInTimeLine,
//                                                                 NULL, NULL, NULL);
//            if (result == noErr) {
//                hostPlaying   = isPlaying;
//                hostRecording = isRecording;
//                hostCurrentTime  = outCurrentSampleInTimeLine;
//            }
//        }
//    }
}
*/
#pragma mark - Note ons and offs

- (BOOL)isVoiceOn:(UInt16)voiceNum {
    return samplerPlaying.at(voiceNum);
}

- (void)voiceOn:(UInt16)voiceNum pitch:(NoteInstanceID)pitch velocity:(UInt8)velocity {
    NSAssert(voiceNum < self.samplerCount, @"Illegal voice number");
    
    NoteInstanceID noteNum = pitch;
    UInt32 onVelocity = velocity;
    
    UInt32 offsetSampleFrame = 0;
    MusicDeviceGroupID groupID = 0;
    MusicDeviceNoteParams noteParams = {2,static_cast<Float32>(noteNum),static_cast<Float32>(onVelocity),0};
    AudioUnit audioUnit = [self samplerUnitForVoice:voiceNum];
    HWThrowIfAudioError(MusicDeviceStartNote(audioUnit,
                                           kMusicNoteEvent_Unused,
                                           groupID,
                                           &noteID.at(voiceNum),
                                           offsetSampleFrame,
                                           &noteParams));
    
    samplerNote.at(voiceNum) = noteNum;
    samplerVelocity.at(voiceNum) = onVelocity;
    samplerPlaying.at(voiceNum) = YES;
    
    // send midi
//    [self midiNoteOn:pitch velocity:velocity channel:voiceNum];
}

- (void)voiceOff:(UInt16)voiceNum note:(NoteInstanceID)note {
    NSAssert(voiceNum < self.samplerCount, @"Illegal voice number");
    
    if (samplerPlaying.at(voiceNum)) {
        UInt32 offsetSampleFrame = 0;
        MusicDeviceGroupID groupID = 0;
        AudioUnit audioUnit = [self samplerUnitForVoice:voiceNum];
        HWThrowIfAudioError(MusicDeviceStopNote(audioUnit, groupID, note, offsetSampleFrame));
        
        samplerPlaying.at(voiceNum) = NO;
        
        // send midi
//        [self midiNoteOff:samplerNote.at(voiceNum) channel:voiceNum];
    }
}

- (void)sendAllNotesOff {
    for (UInt16 idx = 0; idx < self.samplerCount; ++idx) {
        id channel = [self.samplers objectAtIndex:idx];
        if (channel != nil && channel != [NSNull null]) {
            HWThrowIfAudioError(AudioUnitSetParameter([channel audioUnit],kAUGroupParameterID_AllNotesOff,kAudioUnitScope_Group, 0, 0, 0));
            
            HWThrowIfAudioError(AudioUnitSetParameter([channel audioUnit],kAUGroupParameterID_AllSoundOff,kAudioUnitScope_Group, 0, 0, 0));
            
            HWThrowIfAudioError(AudioUnitSetParameter([channel audioUnit],kAUGroupParameterID_ResetAllControllers,kAudioUnitScope_Group, 0, 0, 0));
        }
    }
}

- (void)voiceOff:(UInt16)voiceNum {
    [self voiceOff:voiceNum note:noteID.at(voiceNum)];
}

- (void)voicePitchChange:(unsigned short)voiceNum pitch:(short)pitch velocity:(short)velocity {
    NSAssert(voiceNum < self.samplerCount, @"Illegal voice number");
    
    if (samplerPlaying.at(voiceNum) && samplerNote.at(voiceNum) != pitch) {
        [self voiceOff:voiceNum];
        [self voiceOn:voiceNum pitch:pitch velocity:velocity];
    }
    else if (!samplerPlaying.at(voiceNum)) {
        [self voiceOn:voiceNum pitch:pitch velocity:velocity];
    }
}

#pragma mark -
#pragma mark Audio Session Notifications

- (void) interruption:(NSNotification*)notif {
    NSNumber *type = [notif.userInfo objectForKey:AVAudioSessionInterruptionTypeKey];
    NSNumber *options = [notif.userInfo objectForKey:AVAudioSessionInterruptionOptionKey];
    
    switch (type.unsignedIntegerValue) {
        case AVAudioSessionInterruptionTypeBegan:
            NSLog (@"Audio session was interrupted.");
            break;
        case AVAudioSessionInterruptionTypeEnded:
            if (options.unsignedIntegerValue == AVAudioSessionInterruptionOptionShouldResume)
                NSLog (@"Audio session resumed.");
            break;
    }
}

#pragma mark -
#pragma mark Utility methods

// You can use this method during development and debugging to look at the
//    fields of an AudioStreamBasicDescription struct.
- (void) printASBD: (AudioStreamBasicDescription) asbd {
    
    char formatIDString[5];
    UInt32 formatID = CFSwapInt32HostToBig (asbd.mFormatID);
    bcopy (&formatID, formatIDString, 4);
    formatIDString[4] = '\0';
    
    NSLog (@"  Sample Rate:         %10.0f",  asbd.mSampleRate);
    NSLog (@"  Format ID:           %10s",    formatIDString);
    NSLog (@"  Format Flags:        %10X",    (unsigned int)asbd.mFormatFlags);
    NSLog (@"  Bytes per Packet:    %10u",    (unsigned int)asbd.mBytesPerPacket);
    NSLog (@"  Frames per Packet:   %10u",    (unsigned int)asbd.mFramesPerPacket);
    NSLog (@"  Bytes per Frame:     %10u",    (unsigned int)asbd.mBytesPerFrame);
    NSLog (@"  Channels per Frame:  %10u",    (unsigned int)asbd.mChannelsPerFrame);
    NSLog (@"  Bits per Channel:    %10u",    (unsigned int)asbd.mBitsPerChannel);
}


- (void) printErrorMessage: (NSString *) errorString withStatus: (OSStatus) result {
    
    char resultString[5];
    UInt32 swappedResult = CFSwapInt32HostToBig (result);
    bcopy (&swappedResult, resultString, 4);
    resultString[4] = '\0';
    
    NSLog (
           @"*** %@ error: %d %08X %4.4s\n",
           errorString,
           (int)result,
           (int)result,
           (char*) &resultString
           );
}

- (void)cancelBounce:(NSNotification*)notif {
    self.bounceCancelled = YES;
}

- (void)bouncePlayer:(MusicPlayer)player duration:(MusicTimeStamp)sequenceLength outputPath:(NSString*)outputFilePath progress:(progress_block_t)block {
    
    self.bounceCancelled = NO;
    
    UInt32 originalFPS = 4096;
    UInt32 originalFPS_size = sizeof(originalFPS);
    AudioUnitGetProperty(self.audioController.audioUnit, kAudioUnitProperty_MaximumFramesPerSlice, kAudioUnitScope_Global, 0, &originalFPS, &originalFPS_size);
    
    AUGraph graph = self.audioController.audioGraph;
    
    // set processing graph
    MusicSequence musicSequence;
    UInt32 numTracks;
    HWThrowIfAudioError(MusicPlayerGetSequence(player, &musicSequence));
    HWThrowIfAudioError(MusicSequenceGetTrackCount(musicSequence, &numTracks));
    HWThrowIfAudioError(MusicSequenceSetAUGraph(musicSequence, graph));
    
    // set track nodes
    for (UInt32 i = 0; i < numTracks; i++) {
        MusicTrack musicTrack;
        HWThrowIfAudioError(MusicSequenceGetIndTrack(musicSequence, i, &musicTrack));
        HWThrowIfAudioError(MusicTrackSetDestNode(musicTrack, [self samplerNodeForVoice:i]));
    }
    
    // create file
    __block BOOL beginRecording = NO;
    __block AERecorder *recorder = nil;
    dispatch_sync(dispatch_get_main_queue(), ^{
        recorder = [[AERecorder alloc] initWithAudioController:self.audioController];
        NSError *error = nil;
        if ( ![recorder beginRecordingToFileAtPath:outputFilePath fileType:kAudioFileAIFFType error:&error] ) {
            recordError(error)
            beginRecording = NO;
        }
        else {
            beginRecording = YES;
            [self.audioController addOutputReceiver:recorder];
        }
    });
    
    if (beginRecording) {
        // slight delay at start
        [NSThread sleepForTimeInterval:0.5];
        
        // start player and record
        HWThrowIfAudioError(MusicPlayerStart(player));
        {
            MusicTimeStamp currentTime;
            const Float64 progressPeriod = 0.25;
            do {
                [NSThread sleepForTimeInterval:progressPeriod];
                
                HWThrowIfAudioError(MusicPlayerGetTime (player, &currentTime));
                
                if (block) {
                    dispatch_async(dispatch_get_main_queue(), ^{block(currentTime / sequenceLength);});
                }
            } while (currentTime < sequenceLength && !self.bounceCancelled);
        }
        HWThrowIfAudioError(MusicPlayerStop(player));
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self.audioController removeOutputReceiver:recorder];
            [recorder finishRecording];
        });
    }
}

#pragma mark - shared audio controller

+ (AudioController*)sharedAudioController {
    static dispatch_once_t once;
    static AudioController *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[super allocWithZone:NULL] initWithSamplerCount:HWAudioControllerSamplerCount];
    });
    return sharedInstance;
}

@end
