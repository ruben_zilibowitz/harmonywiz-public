//
//  Figures.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 23/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#ifndef __HarmonyWiz__Figures__
#define __HarmonyWiz__Figures__

#include <iostream>
#include <bitset>
#include <tuple>

namespace HarmonyWiz {
    
    using namespace std;
    
    class Figures {
    private:
        bitset<8> bits;
    public:
        enum ChordType {
            kTriad = 0,
            kSeven,
            kAddTwo,
            kAddFour,
            kAddSix,
            kNine,
            kEleven,
            kThirteen,
            kNotValidChord,
            kNumTypes
        };
        
        enum ChordInversion {
            kRootPosition = 0,
            kFirstInversion,
            kSecondInversion,
            kThirdInversion,
            kFourthInversion,
            kNumInversions
        };
        
        enum ChordTypeWithInversion {
            kTriad_Root = 0,
            kTriad_First,
            kTriad_Second,
            
            kSeven_Root,
            kSeven_First,
            kSeven_Second,
            kSeven_Third,
            
            kAddTwo_Root,
            kAddTwo_First,
            kAddTwo_Second,
            kAddTwo_Third,
            
            kAddFour_Root,
            kAddFour_First,
            kAddFour_Second,
            kAddFour_Third,
            
            kAddSix_Root,
            kAddSix_First,
            kAddSix_Second,
            kAddSix_Third,
            
            kNine_Root,
            kNine_First,
            kNine_Second,
            kNine_Third,
            kNine_Fourth,
            
            kEleven_Root,
            kEleven_First,
            kEleven_Second,
            kEleven_Third,
            kEleven_Fourth,
            
            kThirteen_Root,
            kThirteen_First,
            kThirteen_Second,
            kThirteen_Third,
            kThirteen_Fourth,
            
            kNoValidChordWithInversion,
            
            kNumChordsWithInversion
        };
        
        static ChordTypeWithInversion ChordTypeNext(ChordTypeWithInversion target) {
            if (target == kNumChordsWithInversion)
                throw "ChordTypeNext: target == kNumChordsWithInversion";
            target = static_cast<ChordTypeWithInversion>( static_cast<long>(target)+1 );
            return target;
        }
        
        static ChordTypeWithInversion ChordTypePrev(ChordTypeWithInversion target) {
            if (target == 0)
                throw "ChordTypePrev: target == 0";
            target = static_cast<ChordTypeWithInversion>( static_cast<long>(target)-1 );
            return target;
        }
        
        static ChordInversion ChordInversionNext(ChordInversion target) {
            if (target == kNumInversions)
                throw "ChordInversionNext: target == kNumInversions";
            target = static_cast<ChordInversion>( static_cast<long>(target)+1 );
            return target;
        }
        
        static ChordInversion ChordInversionPrev(ChordInversion target) {
            if (target == 0)
                throw "ChordInversionPrev: target == 0";
            target = static_cast<ChordInversion>( static_cast<long>(target)-1 );
            return target;
        }
        
        static tuple<ChordType,ChordInversion> unpackChord(ChordTypeWithInversion ch) {
            switch (ch) {
                case kTriad_Root: return make_tuple(kTriad,kRootPosition); break;
                case kTriad_First: return make_tuple(kTriad,kFirstInversion); break;
                case kTriad_Second: return make_tuple(kTriad,kSecondInversion); break;
                    
                case kSeven_Root: return make_tuple(kSeven,kRootPosition); break;
                case kSeven_First: return make_tuple(kSeven,kFirstInversion); break;
                case kSeven_Second: return make_tuple(kSeven,kSecondInversion); break;
                case kSeven_Third: return make_tuple(kSeven,kThirdInversion); break;
                    
                case kAddTwo_Root: return make_tuple(kAddTwo,kRootPosition); break;
                case kAddTwo_First: return make_tuple(kAddTwo,kFirstInversion); break;
                case kAddTwo_Second: return make_tuple(kAddTwo,kSecondInversion); break;
                case kAddTwo_Third: return make_tuple(kAddTwo,kThirdInversion); break;
                    
                case kAddFour_Root: return make_tuple(kAddFour,kRootPosition); break;
                case kAddFour_First: return make_tuple(kAddFour,kFirstInversion); break;
                case kAddFour_Second: return make_tuple(kAddFour,kSecondInversion); break;
                case kAddFour_Third: return make_tuple(kAddFour,kThirdInversion); break;
                    
                case kAddSix_Root: return make_tuple(kAddSix,kRootPosition); break;
                case kAddSix_First: return make_tuple(kAddSix,kFirstInversion); break;
                case kAddSix_Second: return make_tuple(kAddSix,kSecondInversion); break;
                case kAddSix_Third: return make_tuple(kAddSix,kThirdInversion); break;
                    
                case kNine_Root: return make_tuple(kNine,kRootPosition); break;
                case kNine_First: return make_tuple(kNine,kFirstInversion); break;
                case kNine_Second: return make_tuple(kNine,kSecondInversion); break;
                case kNine_Third: return make_tuple(kNine,kThirdInversion); break;
                case kNine_Fourth: return make_tuple(kNine,kFourthInversion); break;
                    
                case kEleven_Root: return make_tuple(kEleven,kRootPosition); break;
                case kEleven_First: return make_tuple(kEleven,kFirstInversion); break;
                case kEleven_Second: return make_tuple(kEleven,kSecondInversion); break;
                case kEleven_Third: return make_tuple(kEleven,kThirdInversion); break;
                case kEleven_Fourth: return make_tuple(kEleven,kFourthInversion); break;
                    
                case kThirteen_Root: return make_tuple(kThirteen,kRootPosition); break;
                case kThirteen_First: return make_tuple(kThirteen,kFirstInversion); break;
                case kThirteen_Second: return make_tuple(kThirteen,kSecondInversion); break;
                case kThirteen_Third: return make_tuple(kThirteen,kThirdInversion); break;
                case kThirteen_Fourth: return make_tuple(kThirteen,kFourthInversion); break;
                    
                default: return make_tuple(kNotValidChord,kRootPosition); break;
            }
        }
        
        static ChordTypeWithInversion packChord(ChordType type, ChordInversion inv) {
            ChordTypeWithInversion result;
            switch (type) {
                case kTriad: result = kTriad_Root; break;
                case kSeven: result = kSeven_Root; break;
                case kAddTwo: result = kAddTwo_Root; break;
                case kAddFour: result = kAddFour_Root; break;
                case kAddSix: result = kAddSix_Root; break;
                case kNine: result = kNine_Root; break;
                case kEleven: result = kEleven_Root; break;
                case kThirteen: result = kThirteen_Root; break;
                default: return kNoValidChordWithInversion; break;
            }
            
            result = (ChordTypeWithInversion)((uint16_t)result + (uint16_t)inv);
            
            return result;
        }
    private:
        
        bitset< kNumChordsWithInversion > allowedChords;
        bitset< kNumChordsWithInversion > possibleChords;
        
    public:
        
        Figures() {
            reset();
            allowAll();
        }
        
        void reset() {
            bits.reset();
        }
        
        void permission(ChordTypeWithInversion ch, bool val) {
            allowedChords[ch] = val;
        }
        
        void allow(ChordTypeWithInversion ch) {
            allowedChords[ch] = true;
        }
        
        void disallow(ChordTypeWithInversion ch) {
            allowedChords[ch] = false;
        }
        
        void disallowAll() {
            allowedChords.reset();
        }
        
        void allowAll() {
            allowedChords.set();
            allowedChords[kNoValidChordWithInversion] = false;  // this should never be on
        }
        
        //        void PrintDebugInfo() const {
        //            cout << "DBG: " << allowedChords << endl;
        //        }
        
        // must run check_chord before calling this method
        ChordTypeWithInversion what() const {
            const bitset< kNumChordsWithInversion > wh = whats();
            for (ChordTypeWithInversion ch = kTriad_Root; ch < kNumChordsWithInversion; ch=ChordTypeNext(ch)) {
                if (wh.test(ch))
                    return ch;
            }
            return kNoValidChordWithInversion;
        }
        
        // must run check_chord before calling this method
        bitset< kNumChordsWithInversion > whats() const {
            return (allowedChords & possibleChords);
        }
        
        // computes possible chord types and inversions
        bool check_chord() {
            possibleChords.reset();
            
            check_root_position();
            check_first_inversion();
            check_second_inversion();
            check_third_inversion();
            check_fourth_inversion();
            
            return possibleChords.any();
        }
        
        void check_root_position() {
            // triad
            if (only(1,3,5))
                possibleChords.set(kTriad_Root);
            // seven
            if (only(1,3,5,7))
                possibleChords.set(kSeven_Root);
            // add two
            if (only(1,3,5,2))
                possibleChords.set(kAddTwo_Root);
            // add four
            if (only(1,3,5,4))
                possibleChords.set(kAddFour_Root);
            // add six
            if (only(1,3,5,6))
                possibleChords.set(kAddSix_Root);
            // nine
            if (only(1,3,5,7,2))
                possibleChords.set(kNine_Root);
            // eleven
            if (only(1,3,5,7,4))
                possibleChords.set(kEleven_Root);
            // thirteen
            if (only(1,3,5,7,6))
                possibleChords.set(kThirteen_Root);
        }
        
        void check_first_inversion() {
            // triad
            if (only(1,3,6))
                possibleChords.set(kTriad_First);
            // seven
            if (only(1,3,6,5))
                possibleChords.set(kSeven_First);
            // add two
            if (only(1,3,6,7))
                possibleChords.set(kAddTwo_First);
            // add four
            if (only(1,3,6,2))
                possibleChords.set(kAddFour_First);
            // add six
            if (only(1,3,6,4))
                possibleChords.set(kAddSix_First);
            // nine
            if (only(1,3,6,5,7))
                possibleChords.set(kNine_First);
            // eleven
            if (only(1,3,6,5,2))
                possibleChords.set(kEleven_First);
            // thirteen
            if (only(1,3,6,5,4))
                possibleChords.set(kThirteen_First);
        }
        
        void check_second_inversion() {
            // triad
            if (only(1,4,6))
                possibleChords.set(kTriad_Second);
            // seven
            if (only(1,4,6,3))
                possibleChords.set(kSeven_Second);
            // add two
            if (only(1,4,6,5))
                possibleChords.set(kAddTwo_Second);
            // add four
            if (only(1,4,6,7))
                possibleChords.set(kAddFour_Second);
            // add six
            if (only(1,4,6,2))
                possibleChords.set(kAddSix_Second);
            // nine
            if (only(1,4,6,3,5))
                possibleChords.set(kNine_Second);
            // eleven
            if (only(1,4,6,3,7))
                possibleChords.set(kEleven_Second);
            // thirteen
            if (only(1,4,6,3,2))
                possibleChords.set(kThirteen_Second);
        }
        
        void check_third_inversion() {
            // seven
            if (only(1,2,4,6))
                possibleChords.set(kSeven_Third);
            // add two
            if (only(1,2,4,7))
                possibleChords.set(kAddTwo_Third);
            // add four
            if (only(1,2,5,7))
                possibleChords.set(kAddFour_Third);
            // add six
            if (only(1,3,5,7))
                possibleChords.set(kAddSix_Third);
            // nine
            if (only(1,2,3,4,6))
                possibleChords.set(kNine_Third);
            // eleven
            if (only(1,2,4,5,6))
                possibleChords.set(kEleven_Third);
            // thirteen
            if (only(1,2,4,6,7))
                possibleChords.set(kThirteen_Third);
        }
        
        void check_fourth_inversion() {
            // nine
            if (only(1,2,4,6,7))
                possibleChords.set(kNine_Fourth);
            // eleven
            if (only(1,2,4,5,7))
                possibleChords.set(kEleven_Fourth);
            // thirteen
            if (only(1,2,3,5,7))
                possibleChords.set(kThirteen_Fourth);
        }
        
        bool at ( size_t pos ) const {
            return bits[pos];
        }
        
        bitset<8>::reference at ( size_t pos ) {
            return bits[pos];
        }
        
    private:
        
        bool only(uint8_t a, uint8_t b, uint8_t c) {
            bool result = true;
            for (uint8_t it = 0; it < 8; it++) {
                if (it == a || it == b || it == c)
                    result = result && at(it);
                else
                    result = result && ! at(it);
            }
            return result;
        }
        
        bool only(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {
            bool result = true;
            for (uint8_t it = 0; it < 8; it++) {
                if (it == a || it == b || it == c || it == d)
                    result = result && at(it);
                else
                    result = result && ! at(it);
            }
            return result;
        }
        
        bool only(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e) {
            bool result = true;
            for (uint8_t it = 0; it < 8; it++) {
                if (it == a || it == b || it == c || it == d || it == e)
                    result = result && at(it);
                else
                    result = result && ! at(it);
            }
            return result;
        }
    };
}

#endif /* defined(__HarmonyWiz__Figures__) */
