//
//  HWSongLengthMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 13/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWSongLengthMenu.h"
#import "HWCollectionViewController.h"
#import "HarmonyWizSong+Splice.h"

NSInteger const HWMaxMeasures = 32;

@interface HWSongLengthMenu ()

@end

@implementation HWSongLengthMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSString *singularMeasure = LinguiLocalizedString(@"%ld measure", @"singular for 'measure'");
    NSString *pluralMeasure = LinguiLocalizedString(@"%ld measures", @"plural for 'measures'");
    
    cell.textLabel.text = [NSString stringWithFormat:(indexPath.row == 0 ? singularMeasure : pluralMeasure), (long)(indexPath.row + 1)];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    NSUInteger measures = doc.song.finalBarNumber;
    
    cell.accessoryType = indexPath.row+1 == measures ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return HWMaxMeasures;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const NSUInteger measures = indexPath.row + 1;
    const NSUInteger oldMeasures = doc.song.finalBarNumber;
    if (oldMeasures < measures) {
        [doc.hwUndoManager startUndoableChanges];
        NSUInteger count = measures - oldMeasures;
        [doc.song addBarsToEnd:count];
        [doc.hwUndoManager finishUndoableChangesName:@"Add bars"];
        [[NSNotificationCenter defaultCenter] postNotificationName:HWDidAddBarNotification object:self];
    }
    else if (oldMeasures > measures) {
        [doc.hwUndoManager startUndoableChanges];
        NSUInteger count = oldMeasures - measures;
        [doc.song removeBarsFromEnd:count];
        [doc.song spliceHarmony];
        [doc.hwUndoManager finishUndoableChangesName:@"Remove bars"];
        [[NSNotificationCenter defaultCenter] postNotificationName:HWDidRemoveBarNotification object:self];
    }
    
    if (measures != oldMeasures) {
        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:oldMeasures-1 inSection:0], indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

@end
