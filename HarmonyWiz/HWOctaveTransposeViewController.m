//
//  HWOctaveTransposeViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWOctaveTransposeViewController.h"

NSString * const HWChangedOctaveTransposeNotification = @"HWChangedOctaveTransposeNotification";

@interface HWOctaveTransposeViewController ()
@property (nonatomic,assign) NSInteger selectedRow;
@end

@implementation HWOctaveTransposeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.selectedRow = -1;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = LinguiLocalizedString(@"Octave transpose", @"Menu title");
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *DefaultCellIdentifier = @"DefaultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DefaultCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DefaultCellIdentifier];
    }
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = LinguiLocalizedString(@"Up one octave", @"Table cell text");
            break;
        case 1:
            cell.textLabel.text = LinguiLocalizedString(@"No transpose", @"Table cell text");
            break;
        case 2:
            cell.textLabel.text = LinguiLocalizedString(@"Down one octave", @"Table cell text");
            break;
        default:
            cell.textLabel.text = nil;
            break;
    }
    
    {
        long chromaticTranspose = indexPath.row;
        chromaticTranspose = (1 - chromaticTranspose)*12;
        cell.accessoryType = (self.trackControl.transposition == chromaticTranspose ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            self.selectedRow = indexPath.row;
        }
    }
    
    return cell;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

#pragma mark -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section == 0 && self.selectedRow != indexPath.row) {
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        long transposition = indexPath.row;
        transposition = (1 - transposition)*12;
        [dnc postNotificationName:HWChangedOctaveTransposeNotification object:self userInfo:@{ @"transposition" : @(transposition) }];
        
        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.selectedRow inSection:0], indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

@end
