//
//  HWAppDelegate.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <MessageUI/MessageUI.h>

extern NSString * const HWOpenDocumentFromURL;

@interface HWAppDelegate : UIResponder <UIApplicationDelegate, CrashlyticsDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)pushIdleTimerDisabled;
- (void)popIdleTimerDisabled;
- (void)resetIdleTimerDisabled;

+ (void)recordEvent:(NSString*)event;
+ (void)recordEvent:(NSString*)event segmentation:(NSDictionary*)segmentation;
+ (void)recordEvent:(NSString*)event segmentation:(NSDictionary*)segmentation sum:(double)sum;

+ (void)recordError_:(NSError*)error func:(const char*)func line:(int)line;

+ (void)setBackgroundAudioEnabled:(BOOL)enabled;
+ (BOOL)backgroundAudioEnabled;
+ (NSInteger)newFileStartMode;

@end

#define recordError(error)  {[HWAppDelegate recordError_:error func:__func__ line:__LINE__];}
