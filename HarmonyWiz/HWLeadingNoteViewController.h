//
//  HWLeadingNoteViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 17/03/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import "HWChordAttributeSubmenuViewController.h"

@interface HWLeadingNoteViewController : HWChordAttributeSubmenuViewController

@end
