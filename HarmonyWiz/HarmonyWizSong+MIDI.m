//
//  HarmonyWizSong+MIDI.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizSong+MIDI.h"
#import "NSMutableArray+Sorted.h"
#import "HarmonyWizSong+AutoColour.h"
#import "HarmonyWizSong+Splice.h"

const UInt8 kChangeInstrumentCode = 0xC0;
const UInt8 kControlChange = 0xB0;
const UInt8 kControlChangeVolume = 0x07;
const UInt8 kControlChangePan = 0x0A;
UInt8 const kMIDIMetaEventType_TimeSign = 0x58;
UInt8 const kMIDIMetaEventType_KeySign = 0x59;
UInt8 const kMIDIMetaEventType_SMPTEOffset = 0x54;
UInt8 const kMIDIMetaEventType_TrackName = 0x03;
UInt8 const kMIDIMetaEventType_InstrumentName = 0x04;

extern NSInteger const HWMaxMeasures;

const Float32 kNearlyZero = 0.01;

@implementation HarmonyWizSong (MIDI)

- (void)checkError:(OSStatus)err name:(NSString*)name {
    if (err != noErr)
        [NSException raise:@"HarmonyWiz" format:@"%@ result is %d", name, (int)err];
}

- (MusicSequence)convertToMusicSequence {
    return [self convertToMusicSequence:0 includeControlMessages:YES transposing:NO];
}

MIDIMetaEvent *CreateMIDIMetaEventWithDataSize(UInt16 x) {
    const size_t length = (sizeof(MIDIMetaEvent) + (sizeof(char) * ((x) - 1)));
    MIDIMetaEvent *result = (MIDIMetaEvent *)malloc(length);
    memset(result, 0, length);
    return result;
}

- (MusicSequence)convertToMusicSequence:(UInt16)precount includeControlMessages:(BOOL)includeControlMessages transposing:(BOOL)transposing {
    OSStatus result;
    MusicSequence musicSequence;
    
    result = NewMusicSequence(&musicSequence);
    [self checkError:result name:@"NewMusicSequence"];
    
    MusicTrack tempoTrack;
    result = MusicSequenceGetTempoTrack(musicSequence, &tempoTrack);
    [self checkError:result name:@"MusicSequenceGetTempoTrack"];
    
    result = MusicTrackNewExtendedTempoEvent(tempoTrack, 0, self.tempo);
    [self checkError:result name:@"MusicTrackNewExtendedTempoEvent"];

    // add time signature
    {
        const int elements = 4;
        MIDIMetaEvent *timeEvent = CreateMIDIMetaEventWithDataSize(elements);
        
        UInt8 nn = self.timeSign.upperNumeral;
        UInt8 dd = self.timeSign.lowerNumeral == 4 ? 2 : 3;
        UInt8 cc = 18;
        UInt8 bb = 8;
        
        timeEvent->metaEventType = kMIDIMetaEventType_TimeSign;
        timeEvent->dataLength = elements;
        timeEvent->data[0] = nn;
        timeEvent->data[1] = dd;
        timeEvent->data[2] = cc;
        timeEvent->data[3] = bb;
        result = MusicTrackNewMetaEvent(tempoTrack, 0, timeEvent);
        [self checkError:result name:@"MusicTrackNewMetaEvent"];
        free(timeEvent);
    }
    
    UInt8 channel = 0;
//    const UInt8 instrument = 0;
    const UInt8 velocity = 100;
    
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        MusicTrack musicTrack;
        result = MusicSequenceNewTrack(musicSequence, &musicTrack);
        [self checkError:result name:@"MusicSequenceNewTrack"];
        
        // add key sign
        {
            const int elements = 2;
            MIDIMetaEvent *keyEvent = CreateMIDIMetaEventWithDataSize(elements);
            
            UInt8 sf = self.keySign.sharpsFlatsCount;
            UInt8 mi = (self.keySign.tonality == kTonality_Major ? 0 : 1);
            keyEvent->metaEventType = kMIDIMetaEventType_KeySign;
            keyEvent->dataLength = elements;
            keyEvent->data[0] = sf;
            keyEvent->data[1] = mi;
            result = MusicTrackNewMetaEvent(musicTrack, 0, keyEvent);
            [self checkError:result name:@"MusicTrackNewMetaEvent"];
            free(keyEvent);
        }
        
        // track name
        {
            const int elements = (int)track.title.length;
            MIDIMetaEvent *trackNameEvent = CreateMIDIMetaEventWithDataSize(elements);
            
            trackNameEvent->metaEventType = kMIDIMetaEventType_TrackName;
            trackNameEvent->dataLength = elements;
            const char *name = [track.title cStringUsingEncoding:NSUTF8StringEncoding];
            for (int idx = 0; idx < trackNameEvent->dataLength; idx++) {
                trackNameEvent->data[idx] = name[idx];
            }
            result = MusicTrackNewMetaEvent(musicTrack, 0, trackNameEvent);
            [self checkError:result name:@"MusicTrackNewMetaEvent"];
            free(trackNameEvent);
        }
        
        // instrument name
        {
            const int elements = (int)track.instrument.length;
            MIDIMetaEvent *instrumentNameEvent = CreateMIDIMetaEventWithDataSize(elements);
            
            instrumentNameEvent->metaEventType = kMIDIMetaEventType_InstrumentName;
            instrumentNameEvent->dataLength = elements;
            const char *name = [track.instrument cStringUsingEncoding:NSUTF8StringEncoding];
            for (int idx = 0; idx < instrumentNameEvent->dataLength; idx++) {
                instrumentNameEvent->data[idx] = name[idx];
            }
            result = MusicTrackNewMetaEvent(musicTrack, 0, instrumentNameEvent);
            [self checkError:result name:@"MusicTrackNewMetaEvent"];
            free(instrumentNameEvent);
        }
        
        if (includeControlMessages) {
            MIDIChannelMessage volumeMsg = {kControlChange + channel, kControlChangeVolume, (UInt8)(track.volume*127.f+0.5f), 0};
            result = MusicTrackNewMIDIChannelEvent(musicTrack, 0, &volumeMsg);
            [self checkError:result name:@"MusicTrackNewMIDIChannelEvent"];
            
            MIDIChannelMessage panMsg = {kControlChange + channel, kControlChangePan, (UInt8)((track.pan+1.f)*0.5f*127.f+0.5f), 0};
            result = MusicTrackNewMIDIChannelEvent(musicTrack, 0, &panMsg);
            [self checkError:result name:@"MusicTrackNewMIDIChannelEvent"];
        }
        
        for (HarmonyWizMusicEvent *event in track.events) {
            if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)event;
                
                const MusicTimeStamp modifier = (4.f / self.timeSign.lowerNumeral);
                
                MusicTimeStamp timeStamp = [self eventStartSubdivision:ne];
                timeStamp /= self.beatSubdivisions;
                timeStamp += precount * self.timeSign.upperNumeral;
                timeStamp *= modifier;
                
                MusicTimeStamp duration = [self eventDuration:ne];
                duration /= self.beatSubdivisions;
                duration *= modifier;
                
                MIDINoteMessage noteMessage = {
                    channel,                // channel
                    ne.noteValue+12+track.transposition,                // note
                    velocity,               // velocity
                    0,                      // release velocity (Use 0 if you don’t want to specify a particular value)
                    duration      // duration
                };
                
                result = MusicTrackNewMIDINoteEvent(musicTrack, timeStamp, &noteMessage);
                [self checkError:result name:@"MusicTrackNewMIDINoteEvent"];
            }
        }
        channel++;
    }
    
    return musicSequence;
}

- (NSData*)convertToMIDIData {
    CFDataRef smfRef;

    MusicSequence seq = [self convertToMusicSequence];
    const SInt16 resolution = 120;
    OSStatus result = MusicSequenceFileCreateData(seq, kMusicSequenceFile_MIDIType, 0, resolution, &smfRef);
    [self checkError:result name:@"MusicSequenceFileCreateData"];
    NSData *smf = (NSData*)CFBridgingRelease(smfRef);
    DisposeMusicSequence(seq);
    
    return smf;
}

- (void)checkResult:(OSStatus)result {
    if (result != noErr) {
        [NSException raise:@"HarmonyWiz" format:@"result = %ld", (long)result];
    }
}

- (BOOL)hasCurrentEvent:(MusicEventIterator)iterator {
	Boolean hasCurrent = false;
	OSStatus result = MusicEventIteratorHasCurrentEvent(iterator, &hasCurrent);
    [self checkResult:result];
    
	return (BOOL) hasCurrent;
}

- (void)handleMetaEvent:(const MIDIMetaEvent *)metaEvent forTrack:(HarmonyWizMusicTrack*)track {
//    printf("type %d\n", metaEvent->metaEventType);
//    for (int idx = 0; idx < metaEvent->dataLength; idx++) {
//        printf("%d ", metaEvent->data[idx]);
//    }
//    printf("\n");
//    
    if (metaEvent->metaEventType == kMIDIMetaEventType_TimeSign) {
        // time sign
        char numerator = metaEvent->data[0];
        char denominatorExp = metaEvent->data[1];
        
        // only supports 4 and 8 in denominator for now
        if (denominatorExp > 3)
            denominatorExp = 3;
        if (denominatorExp < 2)
            denominatorExp = 2;
        
        char denominator = 1;
        for (int i = 0; i < denominatorExp; i++) {
            denominator *= 2;
        }
        
        const UInt16 finalBarNumber = self.finalBarNumber;
        
        self.timeSign.upperNumeral = numerator;
        self.timeSign.lowerNumeral = denominator;
        
        // reset the finalBarNumber
        self.finalBarNumber = finalBarNumber;
    }
    else if (metaEvent->metaEventType == kMIDIMetaEventType_KeySign) {
        // key sign
        char key = metaEvent->data[0];
        char tonality = metaEvent->data[1];
        
        char root = 0;
        if (key > 0) {
            for (int i = 0; i < key; i++) {
                root = (root + 7)%12;
            }
        }
        else if (key < 0) {
            key = -key;
            for (int i = 0; i < key; i++) {
                root = (root + 5)%12;
            }
        }
        self.keySign.root = root;
        self.keySign.tonality = (tonality == 0 ? kTonality_Major : kTonality_Minor);
    }
    else if (metaEvent->metaEventType == kMIDIMetaEventType_SMPTEOffset) {
        // what to do here?
    }
    else if (metaEvent->metaEventType == kMIDIMetaEventType_TrackName) {
        UInt32 length = MIN(255,metaEvent->dataLength);
        char name[256];
        for (int idx = 0; idx < length; idx++) {
            name[idx] = metaEvent->data[idx];
        }
        name[length] = '\0';
        NSLog(@"MIDI Meta: track name is '%s'", name);
    }
    else if (metaEvent->metaEventType == kMIDIMetaEventType_InstrumentName) {
        UInt32 length = MIN(255,metaEvent->dataLength);
        char name[256];
        for (int idx = 0; idx < length; idx++) {
            name[idx] = metaEvent->data[idx];
        }
        name[length] = '\0';
        NSLog(@"MIDI Meta: instrument name is '%s'", name);
        
        if (track) {
            track.instrument = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
        }
    }
}

- (BOOL)importFromMIDIWithURL:(NSURL*)url {
    const int kMinNumberOfTracks = 4;
    const int kMaxNumberOfTracks = 5;
    
    OSStatus result;
    MusicSequence sequence = NULL;
    
    result = NewMusicSequence(&sequence);
    [self checkResult:result];
    
    result = MusicSequenceFileLoad(sequence, (__bridge CFURLRef)(url), kMusicSequenceFile_MIDIType, 0);
    
    // report an error if we could not open the MIDI file
    if (result != noErr) {
        result = DisposeMusicSequence(sequence);
        [self checkResult:result];
        return NO;
    }
    
    MusicTrack tempoTrack = NULL;
    result = MusicSequenceGetTempoTrack(sequence, &tempoTrack);
    [self checkResult:result];
    
    BOOL midiFileHasKeySign = NO;
    
    // iterate through tempo track events and process them
    {
        MusicEventIterator iter = NULL;
        result = NewMusicEventIterator(tempoTrack, &iter);
        [self checkResult:result];
        
        while ([self hasCurrentEvent:iter]) {
            MusicEventType eventType = 0;
            const void *eventData = NULL;
            UInt32 eventDataSize = 0;
            MusicTimeStamp outTimeStamp;
            result = MusicEventIteratorGetEventInfo(iter, &outTimeStamp, &eventType, &eventData, &eventDataSize);
            [self checkResult:result];
            
            if (eventType == kMusicEventType_ExtendedTempo) {
                const ExtendedTempoEvent *tempoEvent = (const ExtendedTempoEvent *)eventData;
                self.tempo = (UInt16)(tempoEvent->bpm + 0.5);
            }
            else if (eventType == kMusicEventType_Meta) {
                const MIDIMetaEvent *metaEvent = (const MIDIMetaEvent *)eventData;
                [self handleMetaEvent:metaEvent forTrack:nil];
                if (metaEvent->metaEventType == kMIDIMetaEventType_KeySign) {
                    midiFileHasKeySign = YES;
                }
            }
            
            result = MusicEventIteratorNextEvent(iter);
            [self checkResult:result];
        }
        
        result = DisposeMusicEventIterator(iter);
        [self checkResult:result];
    }
    
    UInt32 trackCount;
    result = MusicSequenceGetTrackCount(sequence, &trackCount);
    [self checkResult:result];
    
    // remove empty tracks
    {
        NSMutableArray *emptyTracks = [NSMutableArray array];
        for (UInt32 trackNum = 0; trackNum < trackCount; trackNum++) {
            MusicTrack musicTrack = NULL;
            result = MusicSequenceGetIndTrack(sequence, trackNum, &musicTrack);
            [self checkResult:result];
            
            MusicEventIterator iter = NULL;
            result = NewMusicEventIterator(musicTrack, &iter);
            [self checkResult:result];
            
            UInt16 noteCount = 0;
            while ([self hasCurrentEvent:iter]) {
                MusicEventType eventType = 0;
                MusicTimeStamp outTimeStamp;
                UInt32 outEventDataSize;
                result = MusicEventIteratorGetEventInfo(iter, &outTimeStamp, &eventType, NULL, &outEventDataSize);
                [self checkResult:result];
                
                if (eventType == kMusicEventType_MIDINoteMessage) {
                    noteCount ++;
                    break;
                }
                
                result = MusicEventIteratorNextEvent(iter);
                [self checkResult:result];
            }
            if (noteCount == 0) {
                [emptyTracks addObject:[NSValue valueWithPointer:musicTrack]];
            }
            
            result = DisposeMusicEventIterator(iter);
            [self checkResult:result];
        }
        for (id trackNum in emptyTracks) {
            MusicTrack track = [trackNum pointerValue];
            result = MusicSequenceDisposeTrack(sequence, track);
            [self checkResult:result];
        }
    }
    
    // get new track count
    result = MusicSequenceGetTrackCount(sequence, &trackCount);
    [self checkResult:result];
    
    // limit to 5 tracks max
    if (trackCount > kMaxNumberOfTracks) {
        trackCount = kMaxNumberOfTracks;
    }
    
    // remove tracks from song
    while (self.musicTracks.count > MAX(trackCount,kMinNumberOfTracks)) {
        [self removeMusicTrack:self.musicTracks.lastObject];
    }
    
    NSMutableArray *orderedSongTracks = [NSMutableArray array];
    
    // add tracks to song
    while (self.musicTracks.count < trackCount) {
        
        // auto track type computation
        enum TrackType trackType;
        
        {
            MusicTrack musicTrack = NULL;
            result = MusicSequenceGetIndTrack(sequence, (UInt32)self.musicTracks.count, &musicTrack);
            [self checkResult:result];
            
            MusicEventIterator iter = NULL;
            result = NewMusicEventIterator(musicTrack, &iter);
            [self checkResult:result];
            
            NSMutableArray *noteValues = [NSMutableArray array];
            while ([self hasCurrentEvent:iter]) {
                MusicEventType eventType = 0;
                const void *eventData = NULL;
                UInt32 eventDataSize = 0;
                result = MusicEventIteratorGetEventInfo(iter, NULL, &eventType, &eventData, &eventDataSize);
                [self checkResult:result];
                
                if (eventType == kMusicEventType_MIDINoteMessage) {
                    const MIDINoteMessage *noteMessage = (const MIDINoteMessage *)eventData;
                    const int noteValue = noteMessage->note - 12;
                    [noteValues addObject:@(noteValue)];
                }
                
                result = MusicEventIteratorNextEvent(iter);
                [self checkResult:result];
            }
            
            result = DisposeMusicEventIterator(iter);
            [self checkResult:result];
            
            trackType = kTrackType_Soprano;
            UInt16 lowestScore = 10000;
            for (enum TrackType testTrackType = kTrackType_Soprano; testTrackType < kNumTrackTypes; testTrackType++) {
                UInt16 score = 0;
                UInt16 high = [HarmonyWizSong highestNoteForTrackType:testTrackType];
                UInt16 low = [HarmonyWizSong lowestNoteForTrackType:testTrackType];
                for (id num in noteValues) {
                    const int note = [num intValue];
                    if (note > high || note < low) {
                        score++;
                    }
                }
                if (score < lowestScore) {
                    lowestScore = score;
                    trackType = testTrackType;
                }
            }
        }
        
        HarmonyWizMusicTrack *track = [self addMusicTrackOfType:trackType];
        track.volume = 0.8f;
        [orderedSongTracks addObject:track];
    }
    
    // make sure there are enough tracks
    // guess what track types to insert
    {
        enum TrackType trackType;
        while (self.musicTracks.count < kMinNumberOfTracks) {
            trackType = kTrackType_Bass;
            UInt16 lowestScore = 10000;
            for (enum TrackType testTrackType = kTrackType_Soprano; testTrackType < kNumTrackTypes; testTrackType++) {
                UInt16 score = 0;
                UInt16 high = [HarmonyWizSong highestNoteForTrackType:testTrackType];
                UInt16 low = [HarmonyWizSong lowestNoteForTrackType:testTrackType];
                for (HarmonyWizMusicTrack *musicTrack in self.musicTracks) {
                    SInt16 high2 = musicTrack.highestNoteAllowed;
                    SInt16 low2 = musicTrack.lowestNoteAllowed;
                    SInt16 overlap = MIN(high,high2) - MAX(low,low2);
                    if (overlap > 0) {
                        score += overlap;
                    }
                }
                if (score <= lowestScore) {
                    lowestScore = score;
                    trackType = testTrackType;
                }
            }
            [self addMusicTrackOfType:trackType];
        }
    }
    
    const MusicTimeStamp modifier = ((self.timeSign.lowerNumeral * self.beatSubdivisions) / 4.f);
    
    for (UInt32 idx = 0; idx < orderedSongTracks.count; idx++) {
        HarmonyWizMusicTrack *songMusicTrack = [orderedSongTracks objectAtIndex:idx];
        [songMusicTrack.events removeAllObjects];
        
        MusicTimeStamp trackLength;
        MusicTrack musicTrack = NULL;
        result = MusicSequenceGetIndTrack(sequence, idx, &musicTrack);
        [self checkResult:result];
        
        {
            UInt32 dataLength = sizeof(trackLength);
            result = MusicTrackGetProperty(musicTrack, kSequenceTrackProperty_TrackLength, &trackLength, &dataLength);
            [self checkResult:result];
            
            UInt16 bars = (UInt16)(ceil(trackLength / self.timeSign.upperNumeral * (self.timeSign.lowerNumeral / 4.f)) + 0.5f);
            if (bars > HWMaxMeasures)
                bars = HWMaxMeasures;
            if (bars > self.finalBarNumber) {
                self.finalBarNumber = bars;
            }
        }
        
        MusicEventIterator iter = NULL;
        result = NewMusicEventIterator(musicTrack, &iter);
        [self checkResult:result];
        
        NSMutableArray *floodEvents = [NSMutableArray array];
        
        while ([self hasCurrentEvent:iter]) {
            MusicTimeStamp timestamp = 0;
            MusicEventType eventType = 0;
            const void *eventData = NULL;
            UInt32 eventDataSize = 0;
            result = MusicEventIteratorGetEventInfo(iter, &timestamp, &eventType, &eventData, &eventDataSize);
            [self checkResult:result];
            
            UInt16 barNum = (UInt16)(timestamp / self.timeSign.upperNumeral * (self.timeSign.lowerNumeral / 4.f) + 0.5f);
            if (barNum >= HWMaxMeasures) {
                // skip events that are past the max number of measures
                result = MusicEventIteratorNextEvent(iter);
                [self checkResult:result];
                continue;
            }
            
            if (eventType == kMusicEventType_MIDINoteMessage) {
                const Float32 quantization = self.beatSubdivisions / 4;
                
                const MIDINoteMessage *noteMessage = (const MIDINoteMessage *)eventData;
                HarmonyWizNoteEvent *ne = [[HarmonyWizNoteEvent alloc] init];
                ne.type = kNoteEventType_Auto;
                ne.noteValue = noteMessage->note - 12;
                const UInt32 dur = (UInt32)(roundf((noteMessage->duration * modifier) / quantization) * quantization + 0.5f);
                const UInt32 pos = (UInt32)(roundf((timestamp * modifier) / quantization) * quantization + 0.5f);
                [self setEventPosition:ne to:pos];
                
                if (noteMessage->duration > kNearlyZero) {
                    if (dur > 0) {
                        [self setEventDuration:ne to:dur];
                    }
                    else {
                        [self setEventDuration:ne to:(UInt32)(quantization+0.5f)];
                    }
                }
                else {
                    // If the duration of the event is zero, it means
                    // the pitch is the same as the next event and
                    // the duration lasts until the start of the next event.
                    // This seems to be a small bug in the midi importer.
                    
                    [self setEventDuration:ne to:1];
                    [floodEvents addObject:ne];
                }
                
                // kill overlaps
                {
                    NSIndexSet *overlaps = [self overlappingEvents:songMusicTrack.events forEvent:ne];
                    NSArray *removedEvents = [songMusicTrack.events objectsAtIndexes:overlaps];
                    [songMusicTrack.events removeObjectsAtIndexes:overlaps];
                    [floodEvents removeObjectsInArray:removedEvents];
                }
                
                // place the event
                [songMusicTrack.events placeObject:ne];
            }
            else if (eventType == kMusicEventType_Meta) {
                const MIDIMetaEvent *metaEvent = (const MIDIMetaEvent *)eventData;
                [self handleMetaEvent:metaEvent forTrack:songMusicTrack];
                if (metaEvent->metaEventType == kMIDIMetaEventType_KeySign) {
                    midiFileHasKeySign = YES;
                }
            }
            else if (eventType == kMusicEventType_MIDIChannelMessage) {
                const MIDIChannelMessage *channelEvent = (const MIDIChannelMessage *)eventData;
                
                const UInt8 type = channelEvent->status & 0xF0;
                
                if (type == kControlChange) {
                    // should we take the channel of the control change into account??
                    // const UInt8 channel = channelEvent->status & 0x0F;
                    switch (channelEvent->data1) {
                        case kControlChangeVolume:
                        {
                            const float vol = channelEvent->data2 / 127.f;
                            songMusicTrack.volume = vol;
                        }
                            break;
                        case kControlChangePan:
                        {
                            const float pan = channelEvent->data2 / 127.f * 2.f - 1.f;
                            songMusicTrack.pan = pan;
                        }
                            break;
                            
                        default:
                            break;
                    }
                }
            }
            
            result = MusicEventIteratorNextEvent(iter);
            [self checkResult:result];
        }
        
        result = DisposeMusicEventIterator(iter);
        [self checkResult:result];
        
        // handle flood events
        for (id obj in floodEvents) {
            NSUInteger idx = [songMusicTrack.events indexOfObject:obj];
            if (idx+1 < songMusicTrack.events.count) {
                id nextObj = [songMusicTrack.events objectAtIndex:idx+1];
                UInt16 dur = [self eventStartSubdivision:nextObj] - [self eventStartSubdivision:obj];
                [self setEventDuration:obj to:dur];
            }
            else {
                [songMusicTrack.events removeObject:obj];
            }
        }
    }
    
    result = DisposeMusicSequence(sequence);
    [self checkResult:result];
    
    // splice tracks and colour first track
    HarmonyWizMusicTrack *firstTrack = self.musicTracks.firstObject;
    [self autoColour:firstTrack.events withCadence:YES];
    for (id songMusicTrack in orderedSongTracks) {
        // add filler rests
        [self addFillerRestsTo:[songMusicTrack events]];
    }
    [self spliceHarmony];
    
    // auto-detect key if not specified in midi file
    if (! midiFileHasKeySign) {
        [self autoDetectKey];
    }
    
    return YES;
}

- (void)autoDetectKey {
    // nb: the key finding algorithm is copyright CCRMA Stanford University
    // and supposedly needs to be licensed from them.
#if 0
    NSMutableArray *allNotes = [NSMutableArray array];
    for (id track in self.musicTracks) {
        for (id event in [track events]) {
            if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                [allNotes addObject:event];
            }
        }
    }
    
    int *pitchesArr = calloc(sizeof(int), allNotes.count);
    double *durationsArr = calloc(sizeof(double), allNotes.count);
    
    for (NSUInteger idx = 0; idx < allNotes.count; idx++) {
        HarmonyWizNoteEvent *ne = [allNotes objectAtIndex:idx];
        pitchesArr[idx] = ne.noteValue;
        durationsArr[idx] = (double)[self eventDuration:ne] / (double)self.beatSubdivisions;
    }
    
    double scores[24];
    double distribution[12];
    
    int theKey = analyzeKeyKS2(scores, distribution, pitchesArr, durationsArr, allNotes.count, 1, majorKeyBellman, minorKeyBellman);
    
    free(pitchesArr);
    free(durationsArr);
    
    if (theKey < 12) {
        self.keySign.root = theKey;
        self.keySign.tonality = kTonality_Major;
    }
    else if (theKey < 24) {
        self.keySign.root = theKey - 12;
        self.keySign.tonality = kTonality_Minor;
    }
    else {
        // should not get here
        [NSException raise:@"HarmonyWiz" format:@"Bad answer from analyzeKeyKS2 %d", theKey];
    }
#endif
}

@end
