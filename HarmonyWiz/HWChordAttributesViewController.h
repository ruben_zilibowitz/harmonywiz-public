//
//  HWChordAttributesViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizEvent.h"

@interface HWChordAttributesViewController : UITableViewController
@property (nonatomic,weak) IBOutlet UILabel *root, *type, *inversion, *leading;
@property (nonatomic,weak) IBOutlet UITableViewCell *leadingNoteCell;
@property (atomic,strong) HarmonyWizHarmonyEvent *harmonyEvent;
@end
