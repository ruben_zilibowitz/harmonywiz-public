//
//  PSPDFUIKitMainThreadGuard.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/08/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#ifndef HarmonyWiz_PSPDFUIKitMainThreadGuard_h
#define HarmonyWiz_PSPDFUIKitMainThreadGuard_h

__attribute__((constructor)) void PSPDFUIKitMainThreadGuard(void);

#endif
