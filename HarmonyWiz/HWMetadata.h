//
//  HWMetadata.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HarmonyWizEvent.h"

@interface HWMetadata : NSObject <NSCoding>

@property (atomic,strong) UIImage * thumbnail;

@property (atomic,assign) enum NoteColour { kNoteColour_Harmony = 0, kNoteColour_Passing, kNoteColour_Suspension, kNoteColour_Auto, kNoteColour_Rest, kNumNoteTypes } currentNoteColour;
@property (atomic,assign) enum ToolType { kToolType_Hand = 0, kToolType_Pen, kToolType_Eraser, kToolType_Select, kNumToolTypes } currentToolType;
@property (atomic,assign) NSUInteger noteDots;
@property (atomic,assign) enum NotePlainDuration { /*kDuration_Breve, */kDuration_Semibreve, kDuration_Minim, kDuration_Crotchet, kDuration_Quaver, kDuration_Semiquaver, kDuration_Demisemiquaver, kNumNoteDurations } notePlainDuration;
@property (atomic,assign) enum Quantization { kQuantization_Whole, kQuantization_Half, kQuantization_Quarter, kQuantization_Eighth, kQuantization_Sixteenth, kQuatization_Thirtysecond, kNumQuantizations } quantization;
@property (atomic,strong) HarmonyWizMusicEvent *currentToolEvent;

@property (atomic,assign) NSInteger currentlySelectedTrackNumber;
@property (atomic,assign) CGFloat notesPointSize, noteSpacing;
@property (atomic,strong) NSMutableArray *harmonyRules;
//@property (atomic,assign) BOOL expertMode, midiRecordNoteOnEvents, keyboardWizdomScrollEnabled;
@property (atomic,strong) NSNumber *harmonyScore;
@property (atomic,strong) NSString *instrumentsPresets;
@property (atomic,assign) float autoMelody;
@property (atomic,assign) BOOL stepRecord, metronome, precount;
@property (atomic,assign) BOOL isNewFile;
@property (atomic,assign) CGPoint scoreScroll;
@property (atomic,assign) BOOL magicMode;
@property (atomic,assign) BOOL enableTranspose;

@property (atomic,assign) enum HarmonyStyle { kStyle_Baroque = 0, kStyle_Contemporary, kStyle_Rudess1, kStyle_Rudess2, kStyle_Rudess3, kNumStyles } harmonyStyle;
@property (atomic,assign) SInt32 masterReverb;
@property (atomic,assign) BOOL masterEffectsEnabled;

@property (atomic,assign) enum KeyboardType { kKeyboardType_Chromatic = 0, kKeyboardType_Major, kKeyboardType_NaturalMinor, kKeyboardType_Modal, kNumKeyboardTypes } keyboardType;

@property (atomic,assign) BOOL loopingEnabled;
@property (atomic,assign) BOOL pianoOpened;
@property (atomic,assign) CGFloat pianoScrollOffsetX;

@end
