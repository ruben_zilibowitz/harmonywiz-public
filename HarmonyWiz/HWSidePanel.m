//
//  HWSidePanel.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWSidePanel.h"

@implementation HWSidePanel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if ([self.magicView pointInside:[self.magicView convertPoint:point fromView:self] withEvent:event]) {
        return NO;
    }
    return [super pointInside:point withEvent:event];
}

@end
