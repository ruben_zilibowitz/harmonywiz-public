//
// PPCollageEmitterView.h
// Created by Particle Playground on 23/01/2014
//

#import "PPCollageEmitterView.h"

@implementation PPCollageEmitterView

-(id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
	if (self) {
		self.backgroundColor = [UIColor clearColor];
	}
	
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		self.backgroundColor = [UIColor clearColor];
	}
	
	return self;
}

+ (Class) layerClass {
    //configure the UIView to have emitter layer
    return [CAEmitterLayer class];
}


- (UIImage *)flippedVerticalImage:(UIImage*)image {
    UIGraphicsBeginImageContext(image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *flippedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return flippedImage;
}

- (UIImage*)noteImage {
    if (SYSTEM_VERSION_LESS_THAN(@"7"))
        return [self flippedVerticalImage:[UIImage imageNamed:@"images/eighthnote@2x"]];
    else
        return [UIImage imageNamed:@"images/eighthnote@2x"];
}

-(void)start {
    CAEmitterLayer *emitterLayer = (CAEmitterLayer*)self.layer;
    
	emitterLayer.name = @"emitterLayer";
	emitterLayer.emitterPosition = CGPointMake(512.f, 384.f);
	emitterLayer.emitterZPosition = 0;
    
	emitterLayer.emitterSize = CGSizeMake(824.f, 568.f);
	emitterLayer.emitterDepth = 0.00;
    
	emitterLayer.renderMode = kCAEmitterLayerOldestLast;//kCAEmitterLayerAdditive;
    
	emitterLayer.emitterShape = kCAEmitterLayerRectangle;
    
	emitterLayer.seed = (UInt32)time(NULL);
    
	// Create the emitter Cell
	CAEmitterCell *emitterCell = [CAEmitterCell emitterCell];
	
	emitterCell.name = @"Note";
	emitterCell.enabled = YES;
    
	emitterCell.contents = (id)[[self noteImage] CGImage];
	emitterCell.contentsRect = CGRectMake(0.00, 0.00, 1.00, 1.00);
    
	emitterCell.magnificationFilter = kCAFilterLinear;
	emitterCell.minificationFilter = kCAFilterLinear;
	emitterCell.minificationFilterBias = 0.00;
    
	emitterCell.scale = 0.75;
	emitterCell.scaleRange = 0.20;
	emitterCell.scaleSpeed = 0.00;
    
	emitterCell.color = [[UIColorFromRGB(0xFFEEEE) colorWithAlphaComponent:8] CGColor];
	emitterCell.redRange = 0.00;
	emitterCell.greenRange = 0.00;
	emitterCell.blueRange = 0.00;
	emitterCell.alphaRange = 0.00;
    
	emitterCell.redSpeed = 1;//0.20;
	emitterCell.greenSpeed = -0.5;//0.00;
	emitterCell.blueSpeed = 1;//0.20;
	emitterCell.alphaSpeed = -0.10;
    
	emitterCell.lifetime = 10.00;
	emitterCell.lifetimeRange = 0.00;
	emitterCell.birthRate = 0.5;
	emitterCell.velocity = 1.00;
	emitterCell.velocityRange = 50.00;
	emitterCell.xAcceleration = 0.00;
	emitterCell.yAcceleration = 0.00;
	emitterCell.zAcceleration = 0.00;
    
	emitterCell.spin = 0.017;
	emitterCell.spinRange = 0.125;
	emitterCell.emissionLatitude = 0.000;
	emitterCell.emissionLongitude = 0.000;
	emitterCell.emissionRange = 6.283;
    
	emitterLayer.emitterCells = @[emitterCell];
}

@end
