//
//  HWTutorialViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWTutorialViewController : UIPageViewController
@property (nonatomic,strong) NSString *tutorialName;
@end
