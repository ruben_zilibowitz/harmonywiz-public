//
//  HarmonyWizPiano.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 28/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizPiano.h"
#import "NSString+Concatenation.h"
#import "HWCollectionViewController.h"
#import "HWKeyboardTypeMenu.h"
#import "HarmonyWizTrackControl.h"
#import "HarmonyWizMusicEvent+CPP.h"
#include "divmod.h"

int16_t roundDownChromatic(int16_t chromatic, int16_t key, bool isMinor);
int16_t roundUpChromatic(int16_t chromatic, int16_t key, bool isMinor);

CGFloat const kTestTimerInterval = 0.1;
CGFloat const kShiftStartThreshold = 42;
int const kTotalOctaves = 8;

const CGFloat kChromaticWhiteKeyWidth = 147.f * 0.5f;
const CGFloat kChromaticWhiteKeyHeight = 658.f * 0.5f;
const CGFloat kChromaticBlackKeyWidth = 90.f * 0.5f;
const CGFloat kChromaticBlackKeyHeight = 411.f * 0.5f;
//const CGFloat kOctaveWidth = kChromaticWhiteKeyWidth * 7;

const CGFloat kHarmonicKeyWidth = 147.f * 0.5f;
const CGFloat kHarmonicKeyShortHeight = 523.f * 0.5f;
const CGFloat kHarmonicKeyNormalHeight = 658.f * 0.5f;

const int kNumberOfPreloadedOctaves = 3;

const BOOL HWHarmonicKeyboardPlayingSurfaceEntire = YES;

CGFloat const HWPanelHeight = 40;
extern CGFloat const kKeyboardWizdomHeight;

extern NSString * const HWStateChangedNotification;
NSString * const HWStepRecordToggledNotification = @"HWStepRecordToggledNotification";
extern NSString * const HWKeyboardScrolls;
extern NSString * const HWKeyboardTypeChangedNotification;
extern NSString * const HWKeySignatureChangedNotification;
extern NSString * const HWModeChangedNotification;
extern NSString * const HarmonyWizDidSelectTrack;
extern NSString * const HWKeyboardScrolls;

@interface HWPianoTouch : NSObject
@property (nonatomic,strong) UITouch *touch;
@property (nonatomic,assign) long key;
@property (nonatomic,assign,readonly) CGFloat scrollOffset;
+ (HWPianoTouch*)pianoTouch:(UITouch*)touch withKey:(NSInteger)key scrollOffset:(CGFloat)offset;
@end

@implementation HWPianoTouch
+ (HWPianoTouch*)pianoTouch:(UITouch*)touch withKey:(NSInteger)key scrollOffset:(CGFloat)offset {
    HWPianoTouch *result = [[HWPianoTouch alloc] init];
    result.touch = touch;
    result.key = key;
    result->_scrollOffset = offset;
    return result;
}
@end

@interface HarmonyWizPiano () {
    BOOL keyStates[128];
    UIImageView *keyImageViews[128];
    UIImageView *normalKeyImageViews[36];
    UIImageView *silverKeyImageViews[36];
    UILabel *keyLabels[36];
}

@property (nonatomic,assign) CGFloat shiftStart;
@property (nonatomic,strong) NSMutableSet *activeTouches;
@property (nonatomic,assign) int testState;
@property (nonatomic,strong) UITouch *currentTestTouch;
@property (nonatomic,strong) UIView *overlayPanel;
@property (nonatomic,assign) int contentScrollIsAnimating;
@property (nonatomic,assign) CGFloat contentScrollDestination;
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UIView *scrollViewContentView;
@property (nonatomic,strong) UIButton *stepRecordSwitch, *metronomeSwitch, *precountSwitch, *octaveDownButton, *octaveUpButton, *keyboardTypeButton;
@property (nonatomic,assign) BOOL trackingOctaveDownButton, trackingOctaveUpButton;
@property (nonatomic,assign) UInt32 normalKeyLowest, normalKeyHighest;
@property (nonatomic,strong) UIPopoverController *keyboardTypePopover;

@property (nonatomic,strong) NSArray *keyNames;

@end

NSString * const HWNotificationVirtualPianoNoteOn = @"HWVirtualPianoNoteOn";
NSString * const HWNotificationVirtualPianoNoteOff = @"HWVirtualPianoNoteOff";

@implementation HarmonyWizPiano

static CGPoint CGPointMake2(CGFloat x, CGFloat y) {
    return CGPointMake(x * 0.5f, y * 0.5f);
}

- (CGFloat)octaveWidth {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    switch (doc.metadata.keyboardType) {
        case kKeyboardType_Chromatic:
            return kChromaticWhiteKeyWidth * 7;
            break;
        case kKeyboardType_Major:
        case kKeyboardType_NaturalMinor:
        case kKeyboardType_Modal:
            return kHarmonicKeyWidth * self.numberOfKeysInOctave;
            break;
        default:
            break;
    }
    
    return -1;
}

- (UIImage*)chromaticKeyImagePathForName:(NSString*)name {
    NSString *path = [@"images/KeyboardWizdomKeys/ChromaticKeys" stringByAppendingPathComponent:name];
    return [UIImage imageNamed:path];
}

- (UIImage*)harmonicKeyImagePathForName:(NSString*)name {
    NSString *path = [@"images/KeyboardWizdomKeys/HarmonicKeys" stringByAppendingPathComponent:name];
    return [UIImage imageNamed:path];
}

- (void)loadImageViews {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    switch (doc.metadata.keyboardType) {
        case kKeyboardType_Chromatic:
            [self loadImageViewsChromatic];
            break;
        case kKeyboardType_Major:
        case kKeyboardType_NaturalMinor:
        case kKeyboardType_Modal:
            [self loadImageViewsHarmonic];
            break;
        default:
            break;
    }
}

- (void)loadImageViewsHarmonic {
    const BOOL keyColours[12] = {YES,NO,YES,NO,YES,YES,NO,YES,NO,YES,NO,YES};
    
    NSMutableArray *normalUpKeyImages = [NSMutableArray array];
    NSMutableArray *normalDownKeyImages = [NSMutableArray array];
    NSMutableArray *silverUpKeyImages = [NSMutableArray array];
    NSMutableArray *silverDownKeyImages = [NSMutableArray array];
    
    // semitone intervals in the scale
    NSArray *scale = nil;
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    switch (doc.metadata.keyboardType) {
        case kKeyboardType_Major:
            scale = @[@(2),@(2),@(1),@(2),@(2),@(2),@(1)];
            break;
        case kKeyboardType_NaturalMinor:
            scale = @[@(2),@(1),@(2),@(2),@(1),@(2),@(2)];
            break;
        case kKeyboardType_Modal:
        {
            // compute first differences of mode pitches array
            NSMutableArray *pitches = [NSMutableArray arrayWithArray:[HWCollectionViewController currentlyOpenDocument].song.songMode.modePitches];
            [pitches addObject:@(12)];
            NSMutableArray *pitchesDiffs = [NSMutableArray arrayWithCapacity:pitches.count-1];
            for (int i = 1; i < pitches.count; i++) {
                const NSUInteger diff = [[pitches objectAtIndex:i] intValue] - [[pitches objectAtIndex:i-1] intValue];
                [pitchesDiffs addObject:@(diff)];
            }
            scale = [NSArray arrayWithArray:pitchesDiffs];
        }
            break;
            
        default:
            [NSException raise:@"HarmonyWiz" format:@"Unsupported keyboard layout %d", doc.metadata.keyboardType];
            break;
    }
    
    {
        int idx = (doc.song.isModal ? doc.song.songMode.transposition % 12 : doc.song.keySign.root % 12);
        for (NSNumber *interval in scale) {
            const BOOL isWhiteKey = keyColours[idx];
            [normalUpKeyImages addObject:(isWhiteKey ? [self harmonicKeyImagePathForName:@"WhiteNotPressed"] : [self harmonicKeyImagePathForName:@"BlackShortNotPressed"])];
            [normalDownKeyImages addObject:(isWhiteKey ? [self harmonicKeyImagePathForName:@"WhitePressed"] : [self harmonicKeyImagePathForName:@"BlackShortPressed"])];
            [silverUpKeyImages addObject:(isWhiteKey ? [self harmonicKeyImagePathForName:@"SilverNotPressed"] : [self harmonicKeyImagePathForName:@"SilverShortNotPressed"])];
            [silverDownKeyImages addObject:(isWhiteKey ? [self harmonicKeyImagePathForName:@"SilverPressed"] : [self harmonicKeyImagePathForName:@"SilverShortPressed"])];
            
            int intervalValue = interval.intValue;
            idx = (idx + intervalValue) % 12;
        }
    }
    
    const UInt16 kKeysInOctave = normalUpKeyImages.count;
    const UInt16 kNumPreloaded = self.numberOfKeysInOctave * kNumberOfPreloadedOctaves;
    NSAssert(kNumPreloaded <= NELEMS(normalKeyImageViews), @"array size too small");
    NSAssert(kNumPreloaded <= NELEMS(silverKeyImageViews), @"array size too small");
    NSAssert(kNumPreloaded <= NELEMS(keyLabels), @"array size too small");
    for (UInt16 idx = 0; idx < kNumPreloaded; idx++) {
        UIImage *keyUp = [normalUpKeyImages objectAtIndex:idx % kKeysInOctave];
        UIImage *keyDown = [normalDownKeyImages objectAtIndex:idx % kKeysInOctave];
        UIImage *silverUp = [silverUpKeyImages objectAtIndex:idx % kKeysInOctave];
        UIImage *silverDown = [silverDownKeyImages objectAtIndex:idx % kKeysInOctave];
        
        normalKeyImageViews[idx] = [[UIImageView alloc] initWithImage:keyUp highlightedImage:keyDown];
        silverKeyImageViews[idx] = [[UIImageView alloc] initWithImage:silverUp highlightedImage:silverDown];
        
        normalKeyImageViews[idx].hidden = YES;
        silverKeyImageViews[idx].hidden = YES;
        
        normalKeyImageViews[idx].translatesAutoresizingMaskIntoConstraints = NO;
        silverKeyImageViews[idx].translatesAutoresizingMaskIntoConstraints = NO;
        [self.scrollViewContentView addSubview:normalKeyImageViews[idx]];
        [self.scrollViewContentView addSubview:silverKeyImageViews[idx]];
    }
    
    // create key labels
    for (NSUInteger idx = 0; idx < kNumPreloaded; idx++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
//        label.font = [UIFont systemFontOfSize:18.f];
        label.font = [UIFont fontWithName:@"Apple Symbols" size:24.f];
        label.textAlignment = NSTextAlignmentCenter;
        keyLabels[idx] = label;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        [self.scrollViewContentView addSubview:label];
    }
}

- (void)loadImageViewsChromatic {
    const NSArray *normalUpKeyImages =
    @[ [self chromaticKeyImagePathForName:@"C_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Up"],
       [self chromaticKeyImagePathForName:@"D_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Up"],
       [self chromaticKeyImagePathForName:@"E_Up"],
       [self chromaticKeyImagePathForName:@"C_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Up"],
       [self chromaticKeyImagePathForName:@"D_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Up"],
       [self chromaticKeyImagePathForName:@"D_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Up"],
       [self chromaticKeyImagePathForName:@"E_Up"] ];
    
    const NSArray *normalDownKeyImages =
    @[ [self chromaticKeyImagePathForName:@"C_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Down"],
       [self chromaticKeyImagePathForName:@"D_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Down"],
       [self chromaticKeyImagePathForName:@"E_Down"],
       [self chromaticKeyImagePathForName:@"C_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Down"],
       [self chromaticKeyImagePathForName:@"D_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Down"],
       [self chromaticKeyImagePathForName:@"D_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Down"],
       [self chromaticKeyImagePathForName:@"E_Down"] ];
    
    const NSArray *silverUpKeyImages =
    @[ [self chromaticKeyImagePathForName:@"C_Silver_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Up"],
       [self chromaticKeyImagePathForName:@"D_Silver_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Up"],
       [self chromaticKeyImagePathForName:@"E_Silver_Up"],
       [self chromaticKeyImagePathForName:@"C_Silver_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Up"],
       [self chromaticKeyImagePathForName:@"D_Silver_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Up"],
       [self chromaticKeyImagePathForName:@"D_Silver_Up"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Up"],
       [self chromaticKeyImagePathForName:@"E_Silver_Up"] ];
    
    const NSArray *silverDownKeyImages =
    @[ [self chromaticKeyImagePathForName:@"C_Silver_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Down"],
       [self chromaticKeyImagePathForName:@"D_Silver_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Down"],
       [self chromaticKeyImagePathForName:@"E_Silver_Down"],
       [self chromaticKeyImagePathForName:@"C_Silver_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Down"],
       [self chromaticKeyImagePathForName:@"D_Silver_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Down"],
       [self chromaticKeyImagePathForName:@"D_Silver_Down"],
       [self chromaticKeyImagePathForName:@"Sharp_Silver_Down"],
       [self chromaticKeyImagePathForName:@"E_Silver_Down"] ];
    
    const UInt16 kNumPreloaded = self.numberOfKeysInOctave * kNumberOfPreloadedOctaves;
    NSAssert(kNumPreloaded <= NELEMS(normalKeyImageViews), @"array size too small");
    NSAssert(kNumPreloaded <= NELEMS(silverKeyImageViews), @"array size too small");
    NSAssert(kNumPreloaded <= NELEMS(keyLabels), @"array size too small");
    for (UInt16 idx = 0; idx < kNumPreloaded; idx++) {
        UIImage *keyUp = [normalUpKeyImages objectAtIndex:idx%12];
        UIImage *keyDown = [normalDownKeyImages objectAtIndex:idx%12];
        UIImage *silverUp = [silverUpKeyImages objectAtIndex:idx%12];
        UIImage *silverDown = [silverDownKeyImages objectAtIndex:idx%12];
        
        normalKeyImageViews[idx] = [[UIImageView alloc] initWithImage:keyUp highlightedImage:keyDown];
        silverKeyImageViews[idx] = [[UIImageView alloc] initWithImage:silverUp highlightedImage:silverDown];
        
        normalKeyImageViews[idx].hidden = YES;
        silverKeyImageViews[idx].hidden = YES;
        
        normalKeyImageViews[idx].translatesAutoresizingMaskIntoConstraints = NO;
        silverKeyImageViews[idx].translatesAutoresizingMaskIntoConstraints = NO;
        [self.scrollViewContentView addSubview:normalKeyImageViews[idx]];
        [self.scrollViewContentView addSubview:silverKeyImageViews[idx]];
    }
    
    // bring black keys to front
    const int whiteKeyNumbers[] = {0,2,4,5,7,9,11};
    for (NSUInteger idx = 0; idx < kNumPreloaded; idx++) {
        if (searchArray(idx%12,whiteKeyNumbers,NELEMS(whiteKeyNumbers)) < 0) {
            [self.scrollViewContentView bringSubviewToFront:normalKeyImageViews[idx]];
            [self.scrollViewContentView bringSubviewToFront:silverKeyImageViews[idx]];
        }
    }
    
    // key labels
    for (int oct = 0; oct < 4; oct++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.opaque = NO;
        label.backgroundColor = [UIColor clearColor];
//        label.font = [UIFont systemFontOfSize:18.f];
        label.font = [UIFont fontWithName:@"Apple Symbols" size:24.f];
        label.textColor = [UIColor darkGrayColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [@"C" : @(oct).stringValue];
        keyLabels[oct] = label;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        [self.scrollViewContentView addSubview:label];
    }
}

- (void)removeKeysAndLabels {
    for (int idx = 0; idx < NELEMS(normalKeyImageViews); idx++) {
        [normalKeyImageViews[idx] removeFromSuperview];
        normalKeyImageViews[idx] = nil;
        [silverKeyImageViews[idx] removeFromSuperview];
        silverKeyImageViews[idx] = nil;
        [keyLabels[idx] removeFromSuperview];
        keyLabels[idx] = nil;
    }
}

- (void)zeroKeys {
    for (int idx = 0; idx < NELEMS(keyImageViews); idx++) {
        keyImageViews[idx] = nil;
    }
}

- (void)positionChromaticKeysInViewport {
    [self zeroKeys];
    const BOOL kWhiteKeysWithBlackToLeft[] = {0,1,1,0,1,1,1};
    const int whiteKeyNumbersOctave[] = {0,2,4,5,7,9,11};
    const UInt16 kNumPreloadedWhiteKeys = (NELEMS(normalKeyImageViews) / 12) * 7;
    int whiteKeyNumbers[kNumPreloadedWhiteKeys];
    for (int idx = 0; idx < kNumPreloadedWhiteKeys; idx++) {
        whiteKeyNumbers[idx] = whiteKeyNumbersOctave[idx%7] + (idx/7)*12;
    }
    const CGRect viewport = CGRectMake(self.scrollView.contentOffset.x, 0.f, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    int leftWhiteKey = MAX((int)floorf(CGRectGetMinX(viewport) / kChromaticWhiteKeyWidth) - 1, 0);
    int rightWhiteKey = MIN((int)floorf(CGRectGetMaxX(viewport) / kChromaticWhiteKeyWidth) + 2, kTotalOctaves*7);
    
    int keyLabelCount = 0;
    
    for (int key = leftWhiteKey; key < rightWhiteKey; key++) {
        const int fullKeyNumber = whiteKeyNumbersOctave[key%7] + (key/7)*12;
        const BOOL inRange = self.normalKeyLowest <= fullKeyNumber && fullKeyNumber <= self.normalKeyHighest;
        int keyNumber = whiteKeyNumbers[key % kNumPreloadedWhiteKeys];
        CGPoint offset = CGPointMake(key * kChromaticWhiteKeyWidth, -1.f);
        UIImageView *keyImageView = (inRange ? normalKeyImageViews : silverKeyImageViews)[keyNumber];
        [(inRange ? silverKeyImageViews : normalKeyImageViews)[keyNumber] setHidden:YES];
        keyImageView.hidden = NO;
        keyImageView.frame = CGRectMake(offset.x, offset.y, kChromaticWhiteKeyWidth, kChromaticWhiteKeyHeight);
        
        keyImageView.highlighted = [self isKeyPressed:fullKeyNumber];
        keyImageViews[ fullKeyNumber ] = keyImageView;
        
        if (kWhiteKeysWithBlackToLeft[key%7]) {
            keyImageView = (inRange ? normalKeyImageViews : silverKeyImageViews)[keyNumber-1];
            [(inRange ? silverKeyImageViews : normalKeyImageViews)[keyNumber-1] setHidden:YES];
            keyImageView.hidden = NO;
            keyImageView.frame = CGRectMake(offset.x - kChromaticBlackKeyWidth*0.5f, offset.y, kChromaticBlackKeyWidth, kChromaticBlackKeyHeight);
            keyImageView.highlighted = [self isKeyPressed:fullKeyNumber-1];
            keyImageViews[ fullKeyNumber-1 ] = keyImageView;
        }
        
        // position key label
        if (fullKeyNumber % 12 == 0) {
            NSAssert1(keyLabels[keyLabelCount] != nil, @"Key label at index %d does not exist", keyLabelCount);
            int octave = fullKeyNumber / 12;
            CGPoint offset = CGPointMake(octave * self.octaveWidth, 0);
            keyLabels[keyLabelCount].frame = CGRectMake(offset.x, 250, kChromaticWhiteKeyWidth, 50);
            keyLabels[keyLabelCount].text = [NSString stringWithFormat:@"C%d", octave];
            [keyLabels[keyLabelCount] setHidden:NO];
            keyLabelCount ++;
        }
    }
    
    for (int idx = keyLabelCount; idx < NELEMS(keyLabels) && keyLabels[idx] != nil; idx++) {
        [keyLabels[idx] setHidden:YES];
    }
}

- (UInt16)chromaticKeyFromHarmonicKey:(UInt16)harmonicKey {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    NSArray *scale = nil;
    switch (doc.metadata.keyboardType) {
        case kKeyboardType_Major:
            scale = @[@(0),@(2),@(4),@(5),@(7),@(9),@(11)];
            break;
        case kKeyboardType_NaturalMinor:
            scale = @[@(0),@(2),@(3),@(5),@(7),@(8),@(10)];
            break;
        case kKeyboardType_Modal:
        {
            HarmonyWizModeEvent *mode = [HWCollectionViewController currentlyOpenDocument].song.songMode;
            scale = [mode.modePitches copy];
        }
            break;
            
        default:
            [NSException raise:@"HarmonyWiz" format:@"Unsupported keyboard type %d", doc.metadata.keyboardType];
            break;
    }
    
    const int octave = harmonicKey / scale.count;
    const int scaleNote = [[scale objectAtIndex:(harmonicKey % (scale.count))] intValue];
    
    return (scaleNote + octave * 12 + (doc.song.isModal ? doc.song.songMode.transposition : doc.song.keySign.root));
}

+ (NSArray*)keyNamesSharps {
    return @[@"C",@"C#",@"D",@"D#",@"E",@"F",@"F#",@"G",@"G#",@"A",@"A#",@"B"];
}

+ (NSArray*)keyNamesFlats {
    return @[@"C",@"D♭",@"D",@"E♭",@"E",@"F",@"G♭",@"G",@"A♭",@"A",@"B♭",@"B"];
}

+ (NSArray*)modeKeyNames {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const NSArray *sharps = @[@"B#",@"C#",@"C𝄪",@"D#",@"D𝄪",@"E#",@"F#",@"F𝄪",@"G#",@"G𝄪",@"A#",@"A𝄪"];
    const NSArray *flats = @[@"D𝄫",@"D♭",@"E𝄫",@"E♭",@"F♭",@"G𝄫",@"G♭",@"A𝄫",@"A♭",@"B𝄫",@"B♭",@"C♭"];
    NSMutableArray *keyNames = [[HarmonyWizPiano keyNamesFlats] mutableCopy];
    
    NSArray *shifts = [doc.song.songMode modeAccidentalTypes];
    
    for (int i = 0; i < shifts.count; i++) {
        switch ([shifts[i] intValue]) {
                // use a flat or double flat
            case -1:
            case -2:
                keyNames[i] = flats[i];
                break;
                // use a natural
            case 0:
                break;
                // use a sharp or double sharp
            case 1:
            case 2:
                keyNames[i] = sharps[i];
                break;
                
            default:
                NSLog(@"Error: should not get here. shift = %d", [shifts[i] intValue]);
                break;
        }
    }
    
    return keyNames;
}

- (void)updateKeyNames {
    const NSArray *keyNamesSharps = [HarmonyWizPiano keyNamesSharps];
    const NSArray *keyNamesFlats = [HarmonyWizPiano keyNamesFlats];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.keyboardType == kKeyboardType_Modal) {
        self.keyNames = [HarmonyWizPiano modeKeyNames];
    }
    else if (doc.metadata.keyboardType == kKeyboardType_Major) {
        switch (doc.song.keySign.root) {
            case 0:
            case 2:
            case 4:
            case 5:
            case 7:
            case 9:
            case 11:
                self.keyNames = keyNamesSharps.copy;
                break;
            default:
                self.keyNames = keyNamesFlats.copy;
                break;
        }
    }
    else {
        switch (doc.song.keySign.root) {
            case 1:
            case 4:
            case 6:
            case 8:
            case 11:
                self.keyNames = keyNamesSharps.copy;
                break;
            default:
                self.keyNames = keyNamesFlats.copy;
                break;
        }
    }
}

- (void)positionHarmonicKeysInViewport {
    const BOOL keyColours[12] = {YES,NO,YES,NO,YES,YES,NO,YES,NO,YES,NO,YES};
    
    [self zeroKeys];
    const CGRect viewport = CGRectMake(self.scrollView.contentOffset.x, 0.f, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
    int leftWhiteKey = MAX((int)floorf(CGRectGetMinX(viewport) / kHarmonicKeyWidth) - 1, 0);
    int rightWhiteKey = MIN((int)floorf(CGRectGetMaxX(viewport) / kHarmonicKeyWidth) + 2, kTotalOctaves * self.numberOfKeysInOctave);
    
    int keyLabelCount = 0;
    
    for (int key = leftWhiteKey; key < rightWhiteKey; key++) {
        const int fullKeyNumber = [self chromaticKeyFromHarmonicKey:key];
        const int keyNumber = (key % (self.numberOfKeysInOctave * kNumberOfPreloadedOctaves));
        const BOOL inRange = self.normalKeyLowest <= fullKeyNumber && fullKeyNumber <= self.normalKeyHighest;
        CGPoint offset = CGPointMake(key * kHarmonicKeyWidth, -1.f);
        UIImageView *keyImageView = (inRange ? normalKeyImageViews : silverKeyImageViews)[keyNumber];
        [(inRange ? silverKeyImageViews : normalKeyImageViews)[keyNumber] setHidden:YES];
        keyImageView.hidden = NO;
        keyImageView.frame = CGRectMake(offset.x, offset.y, kHarmonicKeyWidth, keyImageView.image.size.height);
        
        keyImageView.highlighted = [self isKeyPressed:fullKeyNumber];
        keyImageViews[ fullKeyNumber ] = keyImageView;
        
        // position key label
        {
            NSAssert1(keyLabels[keyLabelCount] != nil, @"Key label at index %d does not exist", keyLabelCount);
            int octave = fullKeyNumber / 12;
            int degree = fullKeyNumber % 12;
            CGRect frame = keyImageView.frame;
            frame.size.height = 100.f;
            keyLabels[keyLabelCount].frame = frame;
            keyLabels[keyLabelCount].textColor = (keyColours[degree] || !inRange ? [UIColor darkGrayColor] : UIColorFromRGB(0xEDEDED));
            keyLabels[keyLabelCount].text = [NSString stringWithFormat:@"%@%d", [self.keyNames objectAtIndex:degree], octave];
            [keyLabels[keyLabelCount] setHidden:NO];
            keyLabelCount ++;
        }
    }
    
    for (int idx = keyLabelCount; idx < NELEMS(keyLabels) && keyLabels[idx] != nil; idx++) {
        [keyLabels[idx] setHidden:YES];
    }
}

- (void)positionKeysInViewport {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    switch (doc.metadata.keyboardType) {
        case kKeyboardType_Chromatic:
            [self positionChromaticKeysInViewport];
            break;
        case kKeyboardType_Major:
        case kKeyboardType_NaturalMinor:
        case kKeyboardType_Modal:
            [self positionHarmonicKeysInViewport];
            break;
        default:
            break;
    }
}

- (int)numberOfKeysInOctave {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    switch (doc.metadata.keyboardType) {
        case kKeyboardType_Chromatic:
            return 12;
            break;
        case kKeyboardType_Major:
        case kKeyboardType_NaturalMinor:
            return 7;
            break;
        case kKeyboardType_Modal:
            return (int)[HWCollectionViewController currentlyOpenDocument].song.songMode.modePitches.count;
            break;
        default:
            break;
    }
    
    return -1;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // create key buttons
        
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, HWPanelHeight, frame.size.width, kKeyboardWizdomHeight - HWPanelHeight)];
        self.scrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"images/BrushedSeamless"]];
        self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.scrollView];
        
        const CGFloat contentWidth = self.octaveWidth*kTotalOctaves;
        const CGFloat contentHeight = kKeyboardWizdomHeight - HWPanelHeight;
        self.scrollViewContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, contentWidth, contentHeight)];
        [self.scrollView addSubview:self.scrollViewContentView];
        
        self.backgroundColor = nil;
        
        self.scrollView.delaysContentTouches = NO;
        self.scrollView.scrollEnabled = NO;
        self.scrollView.contentSize = CGSizeMake(contentWidth, contentHeight);
        
        self.scrollView.userInteractionEnabled = NO;
        
        self.activeTouches = [NSMutableSet setWithCapacity:12];
        self.multipleTouchEnabled = YES;
        
        self.scrollView.delegate = self;
        
        // load keys
        
        for (int idx = 0; idx < NELEMS(keyStates); idx++) {
            keyStates[idx] = NO;
        }
        [self zeroKeys];
        [self updateKeyNames];
        [self loadImageViews];
        
        // overlay
        
        self.overlayPanel = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, HWPanelHeight)];
        self.overlayPanel.backgroundColor = [UIColor blackColor];
        self.overlayPanel.userInteractionEnabled = YES;
        self.overlayPanel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.overlayPanel];
        
        self.octaveDownButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.octaveDownButton.frame = CGRectMake(24, 4, 153, 30);
        [self.octaveDownButton setImage:[UIImage imageNamed:@"images/Controls/K_OctaveDown_01"] forState:UIControlStateNormal];
//        [self.octaveDownButton setImage:[UIImage imageNamed:@"images/Controls/K_OctaveDown_02"] forState:UIControlStateHighlighted];
        [self.octaveDownButton addTarget:self action:@selector(octaveDown:) forControlEvents:UIControlEventTouchUpInside];
        self.octaveDownButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.overlayPanel addSubview:self.octaveDownButton];
        
        self.octaveUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.octaveUpButton.frame = CGRectMake(24 + 153 + 4, 4, 153, 30);
        [self.octaveUpButton setImage:[UIImage imageNamed:@"images/Controls/K_OctaveUp_01"] forState:UIControlStateNormal];
//        [self.octaveUpButton setImage:[UIImage imageNamed:@"images/Controls/K_OctaveUp_02"] forState:UIControlStateHighlighted];
        [self.octaveUpButton addTarget:self action:@selector(octaveUp:) forControlEvents:UIControlEventTouchUpInside];
        self.octaveUpButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.overlayPanel addSubview:self.octaveUpButton];
        
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        
        self.stepRecordSwitch = [UIButton buttonWithType:UIButtonTypeCustom];
        self.stepRecordSwitch.selected = doc.metadata.stepRecord;
        self.stepRecordSwitch.frame = CGRectMake(350, 4, 161, 30);
        [self.stepRecordSwitch setImage:[UIImage imageNamed:[@"images/Controls/K_StepRecord_0" : (self.stepRecordSwitch.selected?@"2":@"1")]] forState:UIControlStateNormal];
        [self.stepRecordSwitch setImage:[UIImage imageNamed:@"images/Controls/K_StepRecord_02"] forState:UIControlStateSelected];
        [self.stepRecordSwitch addTarget:self action:@selector(toggleStepRecord:) forControlEvents:UIControlEventTouchUpInside];
        self.stepRecordSwitch.translatesAutoresizingMaskIntoConstraints = NO;
        [self.overlayPanel addSubview:self.stepRecordSwitch];
        
        self.metronomeSwitch = [UIButton buttonWithType:UIButtonTypeCustom];
        self.metronomeSwitch.selected = doc.metadata.metronome;
        self.metronomeSwitch.frame = CGRectMake(350+165, 4, 163, 30);
        [self.metronomeSwitch setImage:[UIImage imageNamed:[@"images/Controls/K_Metronome_0" : (self.metronomeSwitch.selected?@"2":@"1")]] forState:UIControlStateNormal];
        [self.metronomeSwitch setImage:[UIImage imageNamed:@"images/Controls/K_Metronome_02"] forState:UIControlStateSelected];
        [self.metronomeSwitch addTarget:self action:@selector(toggleMetronome:) forControlEvents:UIControlEventTouchUpInside];
        self.metronomeSwitch.translatesAutoresizingMaskIntoConstraints = NO;
        [self.overlayPanel addSubview:self.metronomeSwitch];
        
        self.precountSwitch = [UIButton buttonWithType:UIButtonTypeCustom];
        self.precountSwitch.selected = doc.metadata.precount;
        self.precountSwitch.frame = CGRectMake(350+165*2, 4, 163, 30);
        [self.precountSwitch setImage:[UIImage imageNamed:[@"images/Controls/K_Precount_0" : (self.precountSwitch.selected?@"2":@"1")]] forState:UIControlStateNormal];
        [self.precountSwitch setImage:[UIImage imageNamed:@"images/Controls/K_Precount_02"] forState:UIControlStateSelected];
        [self.precountSwitch addTarget:self action:@selector(togglePrecount:) forControlEvents:UIControlEventTouchUpInside];
        self.precountSwitch.translatesAutoresizingMaskIntoConstraints = NO;
        [self.overlayPanel addSubview:self.precountSwitch];
        
        self.keyboardTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.keyboardTypeButton.frame = CGRectMake(350+165*3, 4, 161, 30);
        [self.keyboardTypeButton setImage:[UIImage imageNamed:@"images/Controls/K_KeyboardType"] forState:UIControlStateNormal];
        [self.keyboardTypeButton addTarget:self action:@selector(keyboardType:) forControlEvents:UIControlEventTouchUpInside];
        self.keyboardTypeButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self.overlayPanel addSubview:self.keyboardTypeButton];
        
        HWKeyboardTypeMenu *keyboardTypeMenu = [[HWKeyboardTypeMenu alloc] initWithStyle:UITableViewStyleGrouped];
        self.keyboardTypePopover = [[UIPopoverController alloc] initWithContentViewController:keyboardTypeMenu];
//        self.keyboardTypePopover.delegate = self;
        self.keyboardTypePopover.popoverContentSize = CGSizeMake(300, 240);
        
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        
        [dnc addObserver:self selector:@selector(changedState:) name:HWStateChangedNotification object:nil];
        
        [dnc addObserver:self selector:@selector(keyboardTypeChanged:) name:HWKeyboardTypeChangedNotification object:nil];
        
        [dnc addObserver:self selector:@selector(keyChanged:) name:HWKeySignatureChangedNotification object:nil];
        [dnc addObserver:self selector:@selector(modeChanged:) name:HWModeChangedNotification object:nil];
        
        [dnc addObserver:self selector:@selector(didSelectTrack:) name:HarmonyWizDidSelectTrack object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateScroll {
    [self.scrollView setContentOffset:CGPointMake([HWCollectionViewController currentlyOpenDocument].metadata.pianoScrollOffsetX, 0) animated:NO];
}

- (void)toggleStepRecord:(UIButton*)sender {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    sender.selected = ! sender.selected;
    [sender setImage:[UIImage imageNamed:[@"images/Controls/K_StepRecord_0" : (sender.selected?@"2":@"1")]] forState:UIControlStateNormal];
    doc.metadata.stepRecord = sender.selected;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HWStepRecordToggledNotification object:self];
}

- (void)toggleMetronome:(UIButton*)sender {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    sender.selected = ! sender.selected;
    [sender setImage:[UIImage imageNamed:[@"images/Controls/K_Metronome_0" : (sender.selected?@"2":@"1")]] forState:UIControlStateNormal];
    doc.metadata.metronome = sender.selected;
}

- (void)togglePrecount:(UIButton*)sender {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    sender.selected = ! sender.selected;
    [sender setImage:[UIImage imageNamed:[@"images/Controls/K_Precount_0" : (sender.selected?@"2":@"1")]] forState:UIControlStateNormal];
    doc.metadata.precount = sender.selected;
}

static long searchArray(int val, const int *arr, size_t size) {
    for (size_t idx = 0; idx < size; idx++) {
        if (val == arr[idx]) {
            return idx;
        }
    }
    return -1;
}

- (BOOL)isKeyPressed:(int)key {
    if (key >= 0 && key < NELEMS(keyImageViews)) {
        return keyStates[key];
    }
    return NO;
}

- (void)setKeyPressed:(int)key {
    if (key >= 0 && key < NELEMS(keyImageViews)) {
        keyImageViews[key].highlighted = YES;
        keyStates[key] = YES;
    }
}

- (void)clearKeyPressed:(long)key {
    if (key >= 0 && key < NELEMS(keyImageViews)) {
        keyImageViews[key].highlighted = NO;
        keyStates[key] = NO;
    }
}

- (int)keyFromPoint:(CGPoint)pt {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (doc.metadata.keyboardType == kKeyboardType_Chromatic) {
        int whiteKey = (pt.x / kChromaticWhiteKeyWidth);
        int blackKey = (pt.x / kChromaticWhiteKeyWidth) + 0.5f;
        
        CGRect blackKeyRect = CGRectMake(kChromaticWhiteKeyWidth * blackKey - kChromaticBlackKeyWidth*0.5f, 0, kChromaticBlackKeyWidth, kChromaticBlackKeyHeight);
        
        // check if black key
        if (CGRectContainsPoint(blackKeyRect, pt)) {
            int blackKeyMod = imod(blackKey, 7);
            if (blackKeyMod==1||blackKeyMod==2||blackKeyMod==4||blackKeyMod==5||blackKeyMod==6) {
                const int nothing = INT_MAX;
                int blackKeyNumbers[] = {nothing,1,3,nothing,6,8,10};
                int result = (blackKeyNumbers[blackKeyMod] + idiv(blackKey, 7)*12);
                return result;
            }
        }
        
        // must be white key
        {
            int whiteKeyMod = imod(whiteKey, 7);
            int whiteKeyNumbers[] = {0,2,4,5,7,9,11};
            int result = (whiteKeyNumbers[whiteKeyMod] + idiv(whiteKey, 7)*12);
            return result;
        }
    }
    else {
        const int key = (pt.x / kHarmonicKeyWidth);
        const int chromaticKey = [self chromaticKeyFromHarmonicKey:key];
        const int blackKeyNumbers[] = {1,3,6,8,10};
        if (searchArray(chromaticKey % 12, blackKeyNumbers, NELEMS(blackKeyNumbers)) >= 0) {
            if (pt.y > kHarmonicKeyShortHeight && !HWHarmonicKeyboardPlayingSurfaceEntire) {
                return -1;
            }
        }
        return chromaticKey;
    }
}

- (void)showRangeForTrack:(HarmonyWizTrack*)track {
    if ([track isKindOfClass:[HarmonyWizMusicTrack class]]) {
        HarmonyWizMusicTrack *mt = (id)track;
        
        self.normalKeyLowest = mt.lowestNoteAllowed;
        self.normalKeyHighest = mt.highestNoteAllowed;
    }
    else {
        // load all silver keys
        self.normalKeyLowest = NELEMS(keyImageViews);
        self.normalKeyHighest = NELEMS(keyImageViews);
    }
    [self positionKeysInViewport];
}

- (void)releaseAllNotes {
    for (HWPianoTouch *touch in self.activeTouches) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWNotificationVirtualPianoNoteOff object:self userInfo:@{ @"key" : @(touch.key) }];
    }
    [self.activeTouches removeAllObjects];
    for (int key = 0; key < NELEMS(keyImageViews); key++)
        [self clearKeyPressed:key];
}

- (CGPoint)cleanTouchLocation:(UITouch*)touch {
    CGPoint pt = [touch locationInView:self.scrollView];
    if (self.isKeyboardScrollAnimationInProgress) {
        pt.x += (self.contentScrollDestination - self.scrollView.contentOffset.x);
    }
    return pt;
}

- (void)changedState:(NSNotification*)note {
    NSUInteger state = [[note.userInfo objectForKey:@"state"] unsignedIntegerValue];
    switch (state) {
        case 0: // stopped
            self.stepRecordSwitch.enabled = YES;
            self.metronomeSwitch.enabled = YES;
            self.precountSwitch.enabled = YES;
            break;
        case 1: // playing
        case 2: // recording
            self.stepRecordSwitch.enabled = NO;
            self.metronomeSwitch.enabled = NO;
            self.precountSwitch.enabled = NO;
            break;
    }
}

- (void)didSelectTrack:(NSNotification*)notif {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:HWKeyboardScrolls] == NO || doc.metadata.stepRecord) {
        HarmonyWizTrackControl *tc = [notif.userInfo valueForKey:@"track"];
        [self scrollKeyboardToRangeForTrack:tc.track];
    }
}

- (void)keyChanged:(NSNotification*)note {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.keyboardType != kKeyboardType_Chromatic) {
        if (doc.song.keySign.tonality == kTonality_Minor)
            doc.metadata.keyboardType = kKeyboardType_NaturalMinor;
        else if (doc.song.keySign.tonality == kTonality_Major)
            doc.metadata.keyboardType = kKeyboardType_Major;
    }
    [self keyboardTypeChanged:nil];
}

- (void)modeChanged:(NSNotification*)note {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
//    if (doc.metadata.keyboardType != kKeyboardType_Chromatic) {
        doc.metadata.keyboardType = kKeyboardType_Modal;
//    }
    [self keyboardTypeChanged:nil];
}

- (void)scrollKeyboardToRangeForTrack:(HarmonyWizTrack*)track {
    if ([track isKindOfClass:[HarmonyWizMusicTrack class]]) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        HarmonyWizMusicTrack *mt = (id)track;
        SInt16 low = mt.lowestNoteAllowed;
        SInt16 high = mt.highestNoteAllowed;
        
        if (doc.metadata.keyboardType == kKeyboardType_Major || doc.metadata.keyboardType == kKeyboardType_NaturalMinor) {
            // round up low
            low = roundUpChromatic(low, doc.song.keySign.root, doc.song.keySign.tonality == kTonality_Minor);
            
            // round down high
            high = roundDownChromatic(high, doc.song.keySign.root, doc.song.keySign.tonality == kTonality_Minor);
        }
        else if (doc.metadata.keyboardType == kKeyboardType_Modal) {
            HarmonyWizNoteEvent *ne = [[HarmonyWizNoteEvent alloc] init];
            
            ne.noteValue = low - 1;
            [ne roundPitchModal:doc.song.songMode];
            low = ne.noteValue;
            
            ne.noteValue = high + 1;
            [ne roundPitchModal:doc.song.songMode];
            high = ne.noteValue;
        }
        
        const CGFloat normalKeyWidth = (doc.metadata.keyboardType == kKeyboardType_Chromatic ? kChromaticWhiteKeyWidth : kHarmonicKeyWidth);
        
        CGFloat xLow = 0, xHigh = 0;
        
        const int keysPerOctave = (doc.metadata.keyboardType == kKeyboardType_Chromatic ? 7 : [self numberOfKeysInOctave]);
        
        for (SInt16 idx = imod(low, 12); idx < NELEMS(keyImageViews); idx += 12) {
            if (idx >= 0 && keyImageViews[idx] != nil) {
                xLow = CGRectGetMinX(keyImageViews[idx].frame);
                int octaveOffset = idiv(low - idx, 12);
                xLow += (octaveOffset * (normalKeyWidth * keysPerOctave));
                break;
            }
        }
        
        for (SInt16 idx = imod(high, 12); idx < NELEMS(keyImageViews); idx += 12) {
            if (idx >= 0 && keyImageViews[idx] != nil) {
                xHigh = CGRectGetMaxX(keyImageViews[idx].frame);
                int octaveOffset = idiv(high - idx, 12);
                xHigh += (octaveOffset * (normalKeyWidth * keysPerOctave));
                break;
            }
        }
        
        CGFloat xAverage = (xLow + xHigh) * 0.5f - self.scrollView.frame.size.width * 0.5f;
        [self scrollAnimatedTo:xAverage];
    }
}

- (void)scrollAnimatedTo:(CGFloat)offset {
    if (ABS(offset - self.scrollView.contentOffset.x) > 1) {
        self.contentScrollIsAnimating ++;
        [self.scrollView setContentOffset:CGPointMake(offset, 0) animated:YES];
    }
    else {
        [self.scrollView setContentOffset:CGPointMake(offset, 0) animated:NO];
    }
}

- (void)keyboardTypeChanged:(NSNotification*)note {
    [self removeKeysAndLabels];
    [self updateKeyNames];
    [self loadImageViews];
    [self positionKeysInViewport];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.currentlySelectedTrackNumber < doc.song.musicTracks.count) {
        HarmonyWizMusicTrack *track = [doc.song.musicTracks objectAtIndex:doc.metadata.currentlySelectedTrackNumber];
        [self scrollKeyboardToRangeForTrack:track];
    }
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if (self.activeTouches.count > 0 && [self pointInside:point withEvent:event]) {
        return self;
    }
    
    return [super hitTest:point withEvent:event];
}

- (void)handleTouchesBegan:(NSSet*)touches forButton:(UIButton*)button tracking:(BOOL*)tracking {
    for (UITouch *touch in touches) {
        if (CGRectContainsPoint(button.bounds, [touch locationInView:button])) {
            [button sendActionsForControlEvents:UIControlEventTouchDown];
            button.highlighted = YES;
            *tracking = YES;
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouchesBegan:touches forButton:self.octaveDownButton tracking:&_trackingOctaveDownButton];
    [self handleTouchesBegan:touches forButton:self.octaveUpButton tracking:&_trackingOctaveUpButton];
    
    for (UITouch *touch in touches) {
        CGPoint pt = [self cleanTouchLocation:touch];
        if (pt.y > 0) {
            int key = [self keyFromPoint:pt];
            if (key >= 0) {
                [self setKeyPressed:key];
                [self.activeTouches addObject:[HWPianoTouch pianoTouch:touch withKey:key scrollOffset:self.scrollView.contentOffset.x]];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:HWNotificationVirtualPianoNoteOn object:self userInfo:@{ @"key" : @(key) }];
            }
        }
    }
    self.shiftStart = 0;
}

- (void)handleTouchesCancelled:(NSSet*)touches forButton:(UIButton*)button tracking:(BOOL*)tracking {
    if (*tracking) {
        [button sendActionsForControlEvents:UIControlEventTouchUpOutside];
        button.highlighted = NO;
        *tracking = NO;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouchesCancelled:touches forButton:self.octaveDownButton tracking:&_trackingOctaveDownButton];
    [self handleTouchesCancelled:touches forButton:self.octaveUpButton tracking:&_trackingOctaveUpButton];
    
    [self releaseAllNotes];
}

- (BOOL)isKeyActive:(long)key withDifferentTouchFrom:(UITouch*)touch {
    for (HWPianoTouch *pianoTouch in self.activeTouches) {
        if (pianoTouch.touch != touch && pianoTouch.key == key) {
            return YES;
        }
    }
    return NO;
}

- (void)handleTouchesMoved:(NSSet*)touches forButton:(UIButton*)button tracking:(BOOL*)tracking {
    for (UITouch *touch in touches) {
        if (*tracking) {
            if (CGRectContainsPoint(button.bounds, [touch previousLocationInView:button]) &&
                ! CGRectContainsPoint(button.bounds, [touch locationInView:button])) {
                [button sendActionsForControlEvents:UIControlEventTouchDragExit];
                button.highlighted = NO;
            }
            else if (! CGRectContainsPoint(button.bounds, [touch previousLocationInView:button]) &&
                     CGRectContainsPoint(button.bounds, [touch locationInView:button])) {
                [button sendActionsForControlEvents:UIControlEventTouchDragEnter];
                button.highlighted = YES;
            }
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouchesMoved:touches forButton:self.octaveDownButton tracking:&_trackingOctaveDownButton];
    [self handleTouchesMoved:touches forButton:self.octaveUpButton tracking:&_trackingOctaveUpButton];
    
    BOOL keyboardScrolls = [[NSUserDefaults standardUserDefaults] boolForKey:HWKeyboardScrolls];
    
    if (keyboardScrolls) {
        CGFloat dx = 0;
        int activeTouchCount = 0;
        for (UITouch *touch in touches) {
            NSSet *touchesMoving = [self.activeTouches objectsPassingTest:^BOOL(HWPianoTouch *obj, BOOL *stop){return (obj.touch == touch);}];
            if (touchesMoving.count > 1) {
                [NSException raise:@"HarmonyWiz" format:@"Too many active touches"];
            }
            else if (touchesMoving.count == 1) {
                CGPoint pt0 = [touch previousLocationInView:self.scrollView];
                CGPoint pt1 = [touch locationInView:self.scrollView];
                dx += (pt1.x - pt0.x);
                activeTouchCount ++;
            }
        }
        if (activeTouchCount > 0)
            dx /= activeTouchCount;
        if (self.shiftStart > kShiftStartThreshold) {
            CGFloat newX = self.scrollView.contentOffset.x - dx;
            if (newX < 0)
                newX = 0;
            else if (newX + self.scrollView.frame.size.width > kTotalOctaves*self.octaveWidth)
                newX = kTotalOctaves*self.octaveWidth - self.scrollView.frame.size.width;
            self.scrollView.contentOffset = CGPointMake(newX, 0);
        }
        else {
            self.shiftStart += fabsf(dx);
        }
    }
    else {
        for (UITouch *touch in touches) {
            NSSet *touchesMoving = [self.activeTouches objectsPassingTest:^BOOL(HWPianoTouch *obj, BOOL *stop){return (obj.touch == touch);}];
            
            if (touchesMoving.count > 1) {
                [NSException raise:@"HarmonyWiz" format:@"Too many active touches"];
            }
            else if (touchesMoving.count == 1) {
                HWPianoTouch *pianoTouch = touchesMoving.anyObject;
                CGPoint pt = [touch locationInView:self.scrollView];
                
                int newKeyTransposed = [self keyFromPoint:CGPointMake(pt.x + pianoTouch.scrollOffset - self.scrollView.contentOffset.x, pt.y)];
                if (newKeyTransposed >= 0) {
                    if (pianoTouch.key != newKeyTransposed) {
                        if (! [self isKeyActive:pianoTouch.key withDifferentTouchFrom:pianoTouch.touch]) {
                            [self clearKeyPressed:pianoTouch.key];
                            [[NSNotificationCenter defaultCenter] postNotificationName:HWNotificationVirtualPianoNoteOff object:self userInfo:@{ @"key" : @(pianoTouch.key) }];
                        }
                        
                        if (! [self isKeyActive:newKeyTransposed withDifferentTouchFrom:pianoTouch.touch]) {
                            [self setKeyPressed:newKeyTransposed];
                            [[NSNotificationCenter defaultCenter] postNotificationName:HWNotificationVirtualPianoNoteOn object:self userInfo:@{ @"key" : @(newKeyTransposed) }];
                        }
                        
                        pianoTouch.key = newKeyTransposed;
                    }
                }
                else {
                    if (pianoTouch.key >= 0) {
                        [self clearKeyPressed:pianoTouch.key];
                        [[NSNotificationCenter defaultCenter] postNotificationName:HWNotificationVirtualPianoNoteOff object:self userInfo:@{ @"key" : @(pianoTouch.key) }];
                        pianoTouch.key = -1;
                    }
                }
            }
        }
    }
}

- (void)handleTouchesEnded:(NSSet*)touches forButton:(UIButton*)button tracking:(BOOL*)tracking {
    for (UITouch *touch in touches) {
        if (*tracking) {
            if (CGRectContainsPoint(button.bounds, [touch locationInView:button])) {
                [button sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
            else {
                [button sendActionsForControlEvents:UIControlEventTouchUpOutside];
            }
            button.highlighted = NO;
            *tracking = NO;
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleTouchesEnded:touches forButton:self.octaveDownButton tracking:&_trackingOctaveDownButton];
    [self handleTouchesEnded:touches forButton:self.octaveUpButton tracking:&_trackingOctaveUpButton];
    
    for (UITouch *touch in touches) {
        NSSet *touchesEnding = [self.activeTouches objectsPassingTest:^BOOL(HWPianoTouch *obj, BOOL *stop){return (obj.touch == touch);}];
        for (HWPianoTouch *pt in touchesEnding) {
            long key = pt.key;
            [self clearKeyPressed:key];
            [[NSNotificationCenter defaultCenter] postNotificationName:HWNotificationVirtualPianoNoteOff object:self userInfo:@{ @"key" : @(key) }];
        }
        [self.activeTouches minusSet:touchesEnding];
    }
}

- (BOOL)isKeyboardScrollAnimationInProgress {
    return (self.contentScrollIsAnimating != 0);
}

#pragma mark - actions

- (void)keyboardType:(UIButton*)sender {
    [self.keyboardTypePopover presentPopoverFromRect:sender.bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)octaveDown:(UIButton*)sender {
    if (self.isKeyboardScrollAnimationInProgress) {
        self.contentScrollDestination = MAX(self.contentScrollDestination - self.octaveWidth, 0);
    }
    else {
        self.contentScrollDestination = MAX(self.scrollView.contentOffset.x - self.octaveWidth, 0);
    }
    
    [self scrollAnimatedTo:self.contentScrollDestination];
}

- (void)octaveUp:(UIButton*)sender {
    if (self.isKeyboardScrollAnimationInProgress) {
        self.contentScrollDestination = MIN(self.contentScrollDestination + self.octaveWidth,kTotalOctaves*self.octaveWidth - self.scrollView.frame.size.width);
    }
    else {
        self.contentScrollDestination = MIN(self.scrollView.contentOffset.x + self.octaveWidth,kTotalOctaves*self.octaveWidth - self.scrollView.frame.size.width);
    }
    
    [self scrollAnimatedTo:self.contentScrollDestination];
}

#pragma mark - UISrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    doc.metadata.pianoScrollOffsetX = scrollView.contentOffset.x;
    
    [self positionKeysInViewport];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    self.contentScrollIsAnimating --;
    if (self.contentScrollIsAnimating < 0) {
        self.contentScrollIsAnimating = 0;
    }
}

@end
