//
//  HarmonyWizPiano.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 28/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HarmonyWizTrack;

@interface HarmonyWizPiano : UIView <UIScrollViewDelegate>
- (void)releaseAllNotes;
- (void)showRangeForTrack:(HarmonyWizTrack*)track;
- (void)scrollKeyboardToRangeForTrack:(HarmonyWizTrack*)track;
- (void)updateScroll;
@end

extern NSString * const HWNotificationVirtualPianoNoteOn;
extern NSString * const HWNotificationVirtualPianoNoteOff;
extern NSString * const HWStepRecordToggledNotification;
