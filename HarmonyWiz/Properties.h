//
//  Properties.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#ifndef HarmonyWiz_Properties_h
#define HarmonyWiz_Properties_h

namespace HarmonyWiz {
    enum Property {
        kCommonTone = 0,
        kBassAlwaysMoves,
        kPrepareVIIDiminished,
        kResolveVIIDiminished,
        kPrepareIIIAugmented,
        kResolveIIIAugmented,
        kPrepareIIDiminished,
        kResolveIIDiminished,
        kPrepareVIDiminished,
        kResolveVIDiminished,
        kAllowSevenths,
        kPrepareSevenths,
        kResolveSevenths,
        kAllowNinths,
        kPrepareNinths,
        kResolveNinths,
        kAllowElevenths,
        kPrepareElevenths,
        kResolveElevenths,
        kAllowThirteenths,
        kPrepareThirteenths,
        kResolveThirteenths,
        kNumProperties
    };
}

#endif
