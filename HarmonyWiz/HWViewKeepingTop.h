//
//  HWViewKeepingTop.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWViewKeepingTop : UIView
@property (nonatomic,strong) NSArray *viewsOnTop;
@end
