//
//  HarmonyWizAppDelegate.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizAppDelegate.h"
#import "HarmonyWizViewController.h"
#import "HarmonyWizSharedObjects.h"
#import "AudioController.h"
#import "HarmonyWizUnarchiver.h"
#import "HarmonyWizUndoManager.h"
#import "NSString+Paths.h"
#import "HarmonyWizArchiver.h"

#import "SCUI.h"

////
// Apple ID: 629564112
////

//#define kTestFlight_WizdomMusic_TeamToken   @"6ad596c119f15eb9d316b24d477bd16a_MTUxMTA4MjAxMi0xMS0wNCAxNzozNDozNC40NjUwMTI"

#define kTestFlight_HarmonyWiz_AppToken    @"d0b776cf-80df-4189-9c4f-0c36dfc55b0a"

#define kFacebook_AppID     @"484163101678102"
#define kFacebook_AppSecret @"d73155f0bd489260df176665536e938a"

#define kSoundCloud_WizdomMusic_ClientID    @"013755eaad5cebb8ebb792cbac4026b9"
#define kSoundCloud_WizdomMusic_Secret      @"50262a1bfc89eab6732f6eb955c76400"
#define kSoundCloud_WizdomMusic_RedirectURI @"HarmonyWiz.soundcloud://"

#define kRefreshSamplesDirectory    0

@interface HarmonyWizAppDelegate ()
@property (nonatomic) NSUInteger idleTimerDisabledCounter;
@end

@implementation HarmonyWizAppDelegate

+ (void)initialize;
{
    [SCSoundCloud  setClientID:kSoundCloud_WizdomMusic_ClientID
                        secret:kSoundCloud_WizdomMusic_Secret
                   redirectURL:[NSURL URLWithString:kSoundCloud_WizdomMusic_RedirectURI]];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // install crash handlers
    {
        NSSetUncaughtExceptionHandler(&HandleException);
        
        struct sigaction signalAction;
        memset(&signalAction, 0, sizeof(signalAction));
        signalAction.sa_handler = &HandleSignal;
        
        sigaction(SIGABRT, &signalAction, NULL);
        sigaction(SIGILL, &signalAction, NULL);
        sigaction(SIGBUS, &signalAction, NULL);
    }
    
#if TESTFLIGHTSDK
    [TestFlight setDeviceIdentifier:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    [TestFlight takeOff:kTestFlight_HarmonyWiz_AppToken];
#endif
    
	// Tell the application to support shake-to-edit.
    application.applicationSupportsShakeToEdit = YES;
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    {
        NSError *error;
        NSString *samplesFilePath = [NSString samplerFilesPath];
        
        // remove samples directory and recreate
        BOOL samplesExist = [fileManager fileExistsAtPath:samplesFilePath];
#if kRefreshSamplesDirectory
        if (samplesExist) {
            [fileManager removeItemAtPath:samplesFilePath error:&error];
            if (error) {
                NSLog(@"%@", error);
            }
            samplesExist = NO;
        }
#endif
        if (!samplesExist) {
            [fileManager createDirectoryAtPath:samplesFilePath
                   withIntermediateDirectories:YES
                                    attributes:nil
                                         error:&error];
            if (error) {
                NSLog(@"%@", error);
            }
        }
    }
    
    self.viewController = [[HarmonyWizViewController alloc] initWithNibName:@"HarmonyWizViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    if ( ![[NSUserDefaults standardUserDefaults] boolForKey:@"play_in_background"]) {
        
        if (self.viewController.state != kStateStopped)
            [self.viewController stopRewind:nil];
        
        HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
        [so.audioController stopProcessingGraph];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
    [so.audioController startProcessingGraph];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([url.scheme isEqualToString:@"HarmonyWiz.soundcloud"]) {
        NSLog(@"SoundCloud callback handled");
        return YES;
    }
    else {
        NSError *error;
        HarmonyWizSong *song = [HarmonyWizSong songWithURL:url error:&error];
        if (error) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else {
            [self.viewController applySong:song];
            
            return YES;
        }
    }
    
    return NO;
}

- (void)pushIdleTimerDisabled {
    self.idleTimerDisabledCounter = self.idleTimerDisabledCounter + 1;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)popIdleTimerDisabled {
    if (self.idleTimerDisabledCounter > 0) {
        self.idleTimerDisabledCounter = self.idleTimerDisabledCounter - 1;
    }
    if (self.idleTimerDisabledCounter == 0) {
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    }
}

void saveBackupSong() {
    NSString *savedSongPath = [NSString songBackupPath];
    NSURL *songURL = [NSURL fileURLWithPath:savedSongPath];
    NSData *songData = [HarmonyWizArchiver archivedDataWithRootObject:[HarmonyWizSharedObjects sharedManager].song];
    NSError *err = nil;
    [songData writeToURL:songURL options:NSDataWritingAtomic error:&err];
    if (err) {
        NSLog(@"%@", err);
    }
}

void HandleException(NSException *exception) {
    NSString *path = [NSString terminationTokenPath];
    [[[NSString stringWithFormat:@"Exception: %@", exception] dataUsingEncoding:NSUTF8StringEncoding] writeToFile:path atomically:YES];
    saveBackupSong();
}

void HandleSignal(int signal) {
    NSString *path = [NSString terminationTokenPath];
    [[[NSString stringWithFormat:@"Signal: %d", signal] dataUsingEncoding:NSUTF8StringEncoding] writeToFile:path atomically:YES];
    saveBackupSong();
}

@end
