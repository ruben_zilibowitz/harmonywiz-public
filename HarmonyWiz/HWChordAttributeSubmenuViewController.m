//
//  HWChordAttributeSubmenuViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWChordAttributeSubmenuViewController.h"
#import "HWCollectionViewController.h"
#import "HWChordAttributesViewController.h"

NSString * const HWChordAttributeModifiedNotification = @"HWChordAttributeModifiedNotification";

@interface HWChordAttributeSubmenuViewController ()

@end

@implementation HWChordAttributeSubmenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.selectedRow != indexPath.row) {
        NSUInteger oldRow = self.selectedRow;
        self.selectedRow = indexPath.row;
        HWChordAttributesViewController *cavc = [self.navigationController.viewControllers objectAtIndex:0];
        [self updateConstraintsForHarmonyEvent:cavc.harmonyEvent];
        UITableViewCell *cell1 = [tableView cellForRowAtIndexPath:indexPath];
        UITableViewCell *cell0 = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:oldRow inSection:0]];
        cell0.accessoryType = UITableViewCellAccessoryNone;
        cell1.accessoryType = UITableViewCellAccessoryCheckmark;
        [[NSNotificationCenter defaultCenter] postNotificationName:HWChordAttributeModifiedNotification object:self];
    }
}

- (void)updateConstraintsForHarmonyEvent:(HarmonyWizHarmonyEvent *)harmonyEvent {
}

@end
