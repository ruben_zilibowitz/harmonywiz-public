//
//  HarmonyWizSong+Splice.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizSong.h"

@interface HarmonyWizSong (Splice)

- (HarmonyWizMusicTrack*)spliceTracks:(NSArray*)tracks;
- (void)spliceHarmony;
- (void)applyDefaultConstraints;

@end
