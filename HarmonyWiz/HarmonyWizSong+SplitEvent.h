//
//  HarmonyWizSong+SplitEvent.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizSong.h"

@interface HarmonyWizSong (SplitEvent)

- (NSArray*) splitEventOnBeats:(HarmonyWizMusicEvent*)event;
- (NSArray*) splitEventAcrossBeats:(HarmonyWizMusicEvent*)event withGradation:(UInt16)gradation;
- (NSArray*) splitEventAcrossBarlines:(HarmonyWizMusicEvent*)event;
- (NSArray*) splitEventWithOddBeats:(HarmonyWizMusicEvent*)event;
- (NSArray*)recursivelySplitEvent:(HarmonyWizMusicEvent*)event;

@end
