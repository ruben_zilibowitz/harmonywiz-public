//
//  HWClearMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 20/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWClearMenu.h"
#import "HWCollectionViewController.h"

extern NSString * const HWClearSongNotification;
extern NSString * const HWClearArrangementNotification;
extern NSString * const HWClearMagicPadNotification;

@interface HWClearMenu ()

@end

@implementation HWClearMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    return (doc.metadata.magicMode ? 2 : 1);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.magicMode) {
        if (section == 0) {
            return 1;
        }
        else if (section == 1) {
            return 2;
        }
    }
    else {
        return 2;
    }
    
    return -1;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.magicMode) {
        if (section == 0) {
            return LinguiLocalizedString(@"Magic Paint", @"Clear menu magic paint header");
        }
        else if (section == 1) {
            return LinguiLocalizedString(@"Song Edit", @"Clear menu song edit header");
        }
    }
    else {
        return LinguiLocalizedString(@"Song Edit", @"Clear menu song edit header");
    }
    
    return nil;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:17.f];
    }
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.magicMode) {
        if (indexPath.section == 0) {
            cell.textLabel.text = LinguiLocalizedString(@"Clear pad", @"Clear menu clear pad item");
        }
        else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                cell.textLabel.text = LinguiLocalizedString(@"Clear arrangement", @"Clear menu clear arrangement item");
            }
            else if (indexPath.row == 1) {
                cell.textLabel.text = LinguiLocalizedString(@"Clear song", @"Clear menu clear song item");
                
            }
        }
    }
    else {
        if (indexPath.row == 0) {
            cell.textLabel.text = LinguiLocalizedString(@"Clear arrangement", @"Clear menu clear arrangement item");
        }
        else if (indexPath.row == 1) {
            cell.textLabel.text = LinguiLocalizedString(@"Clear song", @"Clear menu clear song item");
            
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.magicMode) {
        if (indexPath.section == 0) {
            [dnc postNotificationName:HWClearMagicPadNotification object:self];
        }
        else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                [dnc postNotificationName:HWClearArrangementNotification object:self];
            }
            else if (indexPath.row == 1) {
                [dnc postNotificationName:HWClearSongNotification object:self];
            }
        }
    }
    else {
        if (indexPath.row == 0) {
            [dnc postNotificationName:HWClearArrangementNotification object:self];
        }
        else if (indexPath.row == 1) {
            [dnc postNotificationName:HWClearSongNotification object:self];
        }
    }
}

@end
