#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


typedef enum
{
    BorderTypeDashed,
    BorderTypeSolid
} BorderType;


@interface LBorderView : UIView

@property (assign, nonatomic) BorderType borderType;
@property (assign, nonatomic) CGFloat cornerRadius;
@property (assign, nonatomic) CGFloat borderWidth;
@property (assign, nonatomic) NSUInteger dashPattern;
@property (assign, nonatomic) NSUInteger spacePattern;
@property (strong, nonatomic) UIColor *borderColor;

@property (assign, nonatomic) CGRect virtualFrame;

@end
