//
//  HWPainter.m
//  ProceduralDrawing
//
//  Created by Ruben Zilibowitz on 11/02/2014.
//  Copyright (c) 2014 Zilibowitz Productions. All rights reserved.
//

#import "HWPainter.h"
#include <vector>

NSUInteger const HWNumPainters = 37;

extern "C" NSUInteger const HWMaxIterationsToBrushEnd = 8;

extern "C" struct HWPoint HWPointMake(float x, float y) {
    struct HWPoint pt;
    pt.x = x;
    pt.y = y;
    return pt;
}

CGPoint CGPointFromHWPoint(struct HWPoint pt) {
    return CGPointMake(pt.x, pt.y);
}

static struct HWPoint HWPointZero = {0,0};

@interface HWPainter ()
@end

@implementation HWPainter
{
    struct Painter painters[HWNumPainters];
    CGFloat color0[4];
    CGFloat color1[4];
    CGColorSpaceRef baseSpace;
    
    std::vector<struct HWPoint> rawInput;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        
        // painters
        const int32_t numPainters = [aDecoder decodeInt32ForKey:@"numPainters"];
        {
            for (NSUInteger idx = 0; idx < numPainters && idx < HWNumPainters; idx++) {
                [aDecoder decodeValueOfObjCType:@encode(struct Painter) at:&painters[idx]];
            }
            
            // in case there are less painters now than were saved, read off the rest
            for (NSUInteger i = HWNumPainters; i < numPainters; i++) {
                struct Painter temp;
                [aDecoder decodeValueOfObjCType:@encode(struct Painter) at:&temp];
            }
        }
        
        // raw input
        {
            const int32_t rawInputSize = [aDecoder decodeInt32ForKey:@"rawInputSize"];
            rawInput.resize(rawInputSize);
            for (NSUInteger idx = 0; idx < rawInputSize; idx++) {
                [aDecoder decodeValueOfObjCType:@encode(struct HWPoint) at:&rawInput.at(idx)];
            }
        }
        
        self.div = [aDecoder decodeFloatForKey:@"div"];
        self.speed = [aDecoder decodeFloatForKey:@"speed"];
        self.width = [aDecoder decodeFloatForKey:@"width"];
        self.peturbAmount = [aDecoder decodeFloatForKey:@"peturbAmount"];
        _bounds = [aDecoder decodeCGRectForKey:@"bounds"];
        
        color0[0] = [aDecoder decodeFloatForKey:@"color0[0]"];
        color0[1] = [aDecoder decodeFloatForKey:@"color0[1]"];
        color0[2] = [aDecoder decodeFloatForKey:@"color0[2]"];
        color0[3] = [aDecoder decodeFloatForKey:@"color0[3]"];
        
        color1[0] = [aDecoder decodeFloatForKey:@"color1[0]"];
        color1[1] = [aDecoder decodeFloatForKey:@"color1[1]"];
        color1[2] = [aDecoder decodeFloatForKey:@"color1[2]"];
        color1[3] = [aDecoder decodeFloatForKey:@"color1[3]"];
        
        baseSpace = CGColorSpaceCreateDeviceRGB();
        
        // in case there are more painters now than were saved, initialise the rest
        for (NSUInteger i = numPainters; i < HWNumPainters; i++) {
            const float arg = drand48() * 2 * M_PI;
            const float mod = drand48() * self.peturbAmount;
            painters[i].peturb = HWPointMake(sinf(arg)*mod, cosf(arg)*mod);
            painters[i].div = self.div + 0.02*(drand48()-0.5f);
            painters[i].ease = self.speed + self.width*(drand48()-0.5f);
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    // painters
    {
        [aCoder encodeInt32:HWNumPainters forKey:@"numPainters"];
        for (int i = 0; i < HWNumPainters; i++) {
            [aCoder encodeValueOfObjCType:@encode(struct Painter) at:&painters[i]];
        }
    }
    
    // raw input
    {
        [aCoder encodeInt32:(int32_t)rawInput.size() forKey:@"rawInputSize"];
        for (NSUInteger idx = 0; idx < rawInput.size(); idx++) {
            [aCoder encodeValueOfObjCType:@encode(struct HWPoint) at:&rawInput.at(idx)];
        }
    }
    
    [aCoder encodeFloat:self.div forKey:@"div"];
    [aCoder encodeFloat:self.speed forKey:@"speed"];
    [aCoder encodeFloat:self.width forKey:@"width"];
    [aCoder encodeFloat:self.peturbAmount forKey:@"peturbAmount"];
    [aCoder encodeCGRect:self.bounds forKey:@"bounds"];
    
    [aCoder encodeFloat:color0[0] forKey:@"color0[0]"];
    [aCoder encodeFloat:color0[1] forKey:@"color0[1]"];
    [aCoder encodeFloat:color0[2] forKey:@"color0[2]"];
    [aCoder encodeFloat:color0[3] forKey:@"color0[3]"];
    
    [aCoder encodeFloat:color1[0] forKey:@"color1[0]"];
    [aCoder encodeFloat:color1[1] forKey:@"color1[1]"];
    [aCoder encodeFloat:color1[2] forKey:@"color1[2]"];
    [aCoder encodeFloat:color1[3] forKey:@"color1[3]"];
}


- (id)initWithOffscreenBuffer:(BOOL)buffer contentOffset:(CGPoint)contentOffset size:(CGSize)size
{
    self = [super init];
    if (self) {
        for (int i = 0; i < HWNumPainters; i++) {
            painters[i].dx = 0;
            painters[i].dy = 0;
            painters[i].ax = 0;
            painters[i].ay = 0;
            self.div = 0.15f;
            self.speed = 0.65f;
            self.width = 0.08f;
            self.peturbAmount = 25;
        }
        
        baseSpace = CGColorSpaceCreateDeviceRGB();
        
        float hue = drand48();
        UIColor *uicolor0 = [UIColor colorWithHue:hue saturation:1 brightness:1 alpha:0.25f];
        UIColor *uicolor1 = [UIColor colorWithHue:fmodf(hue+0.3333f,1) saturation:1 brightness:1 alpha:0.25f];
        [uicolor0 getRed:&color0[0] green:&color0[1] blue:&color0[2] alpha:&color0[3]];
        [uicolor1 getRed:&color1[0] green:&color1[1] blue:&color1[2] alpha:&color1[3]];
        
        if (buffer) {
            _offscreenSize = size;
            _offscreenCtx = [self createOffscreenContext:CGSizeMake(size.width, size.height)];
        }
        else {
            _offscreenCtx = nil;
        }
        
        _contentOffset = contentOffset;
    }
    return self;
}

- (void)dealloc {
    if (_offscreenCtx != nil) {
        CGContextRelease(_offscreenCtx);
        _offscreenCtx = nil;
    }
    if (baseSpace != nil) {
        CGColorSpaceRelease(baseSpace);
        baseSpace = nil;
    }
}

- (void)killOffscreenBuffer {
    CGContextRelease(_offscreenCtx);
    _offscreenCtx = nil;
    _contentOffset = CGPointZero;
}

- (CGContextRef) createOffscreenContext: (CGSize) size  {
    CGFloat scale = [[UIScreen mainScreen] scale];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, size.width*scale, size.height*scale, 8, size.width*scale*4, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);
    CGContextScaleCTM(context, scale, scale);
    CGColorSpaceRelease(colorSpace);
    return context;
}

- (void)startWithPoint:(struct HWPoint)pt0 {
    for (int i = 0; i < HWNumPainters; i++) {
        const float arg = drand48() * 2 * M_PI;
        const float mod = drand48() * self.peturbAmount;
        
        if (i == 0) {
            painters[0].peturb = HWPointZero;
            painters[0].div = self.div;
            painters[0].ease = self.speed;
        }
        else {
            painters[i].peturb = HWPointMake(sinf(arg)*mod, cosf(arg)*mod);
            painters[i].div = self.div + 0.02*(drand48()-0.5f);
            painters[i].ease = self.speed + self.width*(drand48()-0.5f);
        }
        
        struct HWPoint pt = pt0;//{pt0.x + painters[i].peturb.x, pt0.y + painters[i].peturb.y};
        painters[i].dx = pt.x;
        painters[i].dy = pt.y;
        painters[i].ax = 0;
        painters[i].ay = 0;
        painters[i].ctr = 0;
        painters[i].pts[0] = HWPointMake(painters[i].dx, painters[i].dy);
        
        for (int j = 0; j < 5; j++) {
            painters[i].pts[j] = painters[i].pts[0];
            painters[i].as[j] = HWPointZero;
        }
    }
    _bounds = CGRectMake(pt0.x, pt0.y, 0, 0);
    
    self.midPoints = @[].mutableCopy;
    [self.midPoints addObject:[NSValue valueWithCGPoint:CGPointFromHWPoint(painters[0].pts[0])]];
    self.midSpeeds = @[].mutableCopy;
    [self.midSpeeds addObject:[NSValue valueWithCGPoint:CGPointZero]];
    
    rawInput.push_back(pt0);
}

- (BOOL)startEndWithPoint:(struct HWPoint)pt {
    return [self appendPoint:pt recording:YES fastDraw:YES peturb:YES context:self.offscreenCtx];
}

- (BOOL)continueEnd {
    struct HWPoint pt = HWPointMake(painters[0].dx, painters[0].dy);
    return [self appendPoint:pt recording:NO fastDraw:YES peturb:NO context:self.offscreenCtx];
}

- (BOOL)appendPoint:(struct HWPoint)pt {
    return [self appendPoint:pt recording:YES fastDraw:YES peturb:YES context:self.offscreenCtx];
}

- (BOOL)appendPoint:(struct HWPoint)pt0 recording:(BOOL)recording fastDraw:(BOOL)fastDraw peturb:(BOOL)peturb context:(CGContextRef)ctx {
    BOOL shouldRedraw = NO;
    
    for (int i = 0; i < HWNumPainters; i++) {
        struct HWPoint pt = pt0;
        if (peturb) {
            pt.x += painters[i].peturb.x;
            pt.y += painters[i].peturb.y;
        }
        painters[i].dx -= painters[i].ax = (painters[i].ax + (painters[i].dx - pt.x) * painters[i].div) * painters[i].ease;
        painters[i].dy -= painters[i].ay = (painters[i].ay + (painters[i].dy - pt.y) * painters[i].div) * painters[i].ease;
        
        painters[i].ctr++;
        painters[i].pts[painters[i].ctr] = HWPointMake(painters[i].dx, painters[i].dy);
        painters[i].as[painters[i].ctr] = HWPointMake(painters[i].ax, painters[i].ay);
        if (painters[i].ctr == 3)
        {
            shouldRedraw = YES;
            
            painters[i].pts[2] = HWPointMake((painters[i].pts[1].x + painters[i].pts[3].x)/2.0, (painters[i].pts[1].y + painters[i].pts[3].y)/2.0);
        }
    }
    
    if (shouldRedraw) {
        _redrawRect = CGRectNull;
        
        if (ctx != nil) {
            UIGraphicsPushContext(ctx);
            CGContextSetFillColorSpace(ctx, baseSpace);
            CGContextSaveGState(ctx);
            CGContextTranslateCTM(ctx, -self.contentOffset.x, -self.contentOffset.y);
        }
        for (int i = 1; i < HWNumPainters; i+=2) {
            
            // create path
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointFromHWPoint(painters[i].pts[0])];
            [path addQuadCurveToPoint:CGPointFromHWPoint(painters[i].pts[2]) controlPoint:CGPointFromHWPoint(painters[i].pts[1])];
            [path addLineToPoint:CGPointFromHWPoint(painters[i+1].pts[2])];
            [path addQuadCurveToPoint:CGPointFromHWPoint(painters[i+1].pts[0]) controlPoint:CGPointFromHWPoint(painters[i+1].pts[1])];
            [path closePath];
            
            // expand the redraw rect
            _redrawRect = CGRectUnion(_redrawRect, path.bounds);
            
            // expand bounds
            _bounds = CGRectUnion(path.bounds, _bounds);
            
            // fill path with gradient
            const float speed0 =
            (sqrtf(painters[i].as[0].x*painters[i].as[0].x + painters[i].as[0].y*painters[i].as[0].y) +
             sqrtf(painters[i+1].as[0].x*painters[i+1].as[0].x + painters[i+1].as[0].y*painters[i+1].as[0].y))*0.5f;
            const float speed1 =
            (sqrtf(painters[i].as[2].x*painters[i].as[2].x + painters[i].as[2].y*painters[i].as[2].y) +
             sqrtf(painters[i+1].as[2].x*painters[i+1].as[2].x + painters[i+1].as[2].y*painters[i+1].as[2].y))*0.5f;
            
            CGFloat const colorVelocity = 0.04f;
            CGFloat param0 = speed0 * colorVelocity;
            CGFloat param1 = speed1 * colorVelocity;
            CGFloat colors [] = {
                color0[0]*(1-param0)+color1[0]*param0, color0[1]*(1-param0)+color1[1]*param0, color0[2]*(1-param0)+color1[2]*param0, color0[3]*(1-param0)+color1[3]*param0,
                color0[0]*(1-param1)+color1[0]*param1, color0[1]*(1-param1)+color1[1]*param1, color0[2]*(1-param1)+color1[2]*param1, color0[3]*(1-param1)+color1[3]*param1
            };
            
            CGPoint startPoint = CGPointMake((painters[i].pts[0].x + painters[i+1].pts[0].x)/2, (painters[i].pts[0].y + painters[i+1].pts[0].y)/2);
            CGPoint endPoint = CGPointMake((painters[i].pts[2].x + painters[i+1].pts[2].x)/2, (painters[i].pts[2].y + painters[i+1].pts[2].y)/2);
            
            // render to offscreen buffer
            if (ctx != nil) {
                if (fastDraw) {
                    // faster
                    CGFloat color[4] = {0.5f*(colors[0]+colors[4]),0.5f*(colors[1]+colors[5]),0.5f*(colors[2]+colors[6]),0.5f*(colors[3]+colors[7])};
                    CGContextSetFillColor(ctx, color);
                    CGContextBeginPath(ctx);
                    CGContextAddPath(ctx, path.CGPath);
                    CGContextFillPath(ctx);
                }
                else {
                    // slower but more smooth color gradient
                    CGContextSaveGState(ctx);
                    [path addClip];
                    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
                    CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
                    CGGradientRelease(gradient);
                    CGContextRestoreGState(ctx);
                }
            }
        }
        if (ctx != nil) {
            CGContextRestoreGState(ctx);
            UIGraphicsPopContext();
        }
        
        // replace points and get ready to handle the next segment
        for (int i = 0; i < HWNumPainters; i++) {
            painters[i].as[0] = painters[i].as[2];
            painters[i].as[1] = painters[i].as[3];
            
            painters[i].pts[0] = painters[i].pts[2];
            painters[i].pts[1] = painters[i].pts[3];
            
            painters[i].ctr = 1;
        }
        
        // store mid points
        if (recording) {
            [self.midPoints addObject:[NSValue valueWithCGPoint:CGPointFromHWPoint(painters[0].pts[1])]];
            [self.midPoints addObject:[NSValue valueWithCGPoint:CGPointFromHWPoint(painters[0].pts[2])]];
            [self.midSpeeds addObject:[NSValue valueWithCGPoint:CGPointFromHWPoint(painters[0].as[1])]];
            [self.midSpeeds addObject:[NSValue valueWithCGPoint:CGPointFromHWPoint(painters[0].as[2])]];
        }
    }
    
    if (recording)
        rawInput.push_back(pt0);
    
    return shouldRedraw;
}

/*
- (void)printPainter:(struct Painter)painter {
    printf("dx,dy: %f,%f\n", painter.dx, painter.dy);
    printf("ax,ay: %f,%f\n", painter.ax, painter.ay);
    printf("div: %f\n", painter.div);
    printf("ease: %f\n", painter.ease);
    for (int i = 0; i < 5; i++)
        printf("pts[%d]: %f,%f\n", i,painter.pts[i].x,painter.pts[i].y);
    for (int i = 0; i < 5; i++)
        printf("as[%d]: %f,%f\n", i,painter.as[i].x,painter.as[i].y);
    printf("ctr: %d\n", painter.ctr);
}
*/

- (void)draw {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    if (rawInput.size() > 0) {
        struct HWPoint pt = rawInput.at(0);
        for (int i = 0; i < HWNumPainters; i++) {
            painters[i].dx = pt.x;
            painters[i].dy = pt.y;
            painters[i].ax = 0;
            painters[i].ay = 0;
            painters[i].ctr = 0;
            painters[i].pts[0] = HWPointMake(painters[i].dx, painters[i].dy);
            
            for (int j = 0; j < 5; j++) {
                painters[i].pts[j] = painters[i].pts[0];
                painters[i].as[j] = HWPointZero;
            }
        }
        
        for (unsigned long idx = 1; idx < rawInput.size(); idx++) {
            [self appendPoint:rawInput.at(idx) recording:NO fastDraw:NO peturb:YES context:ctx];
        }
    }
    
    for (int i = 0; i < HWMaxIterationsToBrushEnd;) {
        struct HWPoint pt = HWPointMake(painters[0].dx, painters[0].dy);
        if ([self appendPoint:pt recording:NO fastDraw:NO peturb:NO context:ctx]) {
            i++;
        }
    }
}

@end
