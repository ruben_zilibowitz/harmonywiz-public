//
//  HarmonyWizAddController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizCreateTrackViewController.h"

@interface HarmonyWizAddController : UITableViewController
@property (nonatomic,strong) IBOutlet HarmonyWizCreateTrackViewController *createTrackController;
@end

extern NSString * const HarmonyWizAddBarNotification;
extern NSString * const HarmonyWizRemoveBarNotification;
extern NSString * const HarmonyWizClearSongNotification;
extern NSString * const HarmonyWizClearArrangementNotification;
extern NSString * const HarmonyWizRemoveTrackNotification;
extern NSString * const HarmonyWizFreezeTrackNotification;
extern NSString * const HarmonyWizClearTrackNotification;
