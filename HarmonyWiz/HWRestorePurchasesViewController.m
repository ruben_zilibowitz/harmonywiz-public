//
//  HWRestorePurchasesViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWRestorePurchasesViewController.h"
#import "NSString+Paths.h"
#import "HWIAPContentDelegate.h"
#import "UIAlertView+Blocks.h"
#import "HWAppDelegate.h"

enum {
    kSection_RestoreAll = 0,
    kSection_RestoreSingleItems,
    kNumSections
};

@interface HWRestorePurchasesViewController ()
@property (nonatomic,strong) NSArray *purchasedProducts;
@property (nonatomic,strong) NSMutableSet *contentViews;
@end

NSInteger const HWPurchaseButtonTag = -1;
NSInteger const HWProgressBarTag = -2;

@implementation HWRestorePurchasesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.contentViews == nil)
        self.contentViews = [NSMutableSet set];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = LinguiLocalizedString(@"Restore Purchases", @"Menu heading");
    [self updatePurchasedProducts];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[CargoManager sharedManager] setUIDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[CargoManager sharedManager] setUIDelegate:nil];
}

- (void)updatePurchasedProducts {
    NSArray *allProductIDs = [[HWIAPContentDelegate sharedManager] productIdentifiers];
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:allProductIDs.count];
    for (NSString *productID in allProductIDs) {
        if ([[HWIAPContentDelegate sharedManager] productPurchased:productID]) {
            SKProduct *prod = [[CargoManager sharedManager] productForIdentifier:productID];
            if (prod) {
                [result addObject:prod];
            }
        }
    }
    if (![self.purchasedProducts isEqualToArray:result]) {
        self.purchasedProducts = result;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSection_RestoreSingleItems] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (IBAction)restore:(UIButton*)sender {
    SKProduct *prod = [self.purchasedProducts objectAtIndex:sender.superview.tag];
    SKDownload *thisDownload = [[HWIAPContentDelegate sharedManager] downloadForProductID:prod.productIdentifier];
    if (thisDownload) {
        // should not get here
        [[SKPaymentQueue defaultQueue] cancelDownloads:@[thisDownload]];
        [self.tableView reloadData];
    }
    else {
        [[CargoManager sharedManager] buyProduct:prod];
        [sender setEnabled:NO];
        [sender setTitle:LinguiLocalizedString(@"Connecting", @"Button title") forState:UIControlStateDisabled];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == kSection_RestoreAll) {
        return 1;
    }
    else if (self.purchasedProducts.count > 0) {
        return self.purchasedProducts.count;
    }
    else {
        return 1;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == kSection_RestoreAll) {
        return nil;
    }
    else {
        return LinguiLocalizedString(@"Restore single packs", @"Menu header");
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == kSection_RestoreAll) {
        [cell setBackgroundColor:UIColorFromRGB(0x007AFF)];
    }
    else {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
}

- (void)setupContentView:(UIView*)contentView forDownload:(SKDownload*)download {
    UIButton *purchase = (id)[contentView viewWithTag:HWPurchaseButtonTag];
    UIProgressView *progress = (id)[contentView viewWithTag:HWProgressBarTag];
    
    purchase.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    if (download && purchase && progress) {
        switch (download.downloadState) {
            case SKDownloadStateActive: {
                purchase.hidden = YES;
                progress.hidden = NO;
                progress.progress = download.progress;
            }
                break;
            case SKDownloadStateWaiting: {
                [purchase setEnabled:NO];
                purchase.hidden = NO;
                progress.hidden = YES;
                [purchase setTitle:LinguiLocalizedString(@"Waiting", @"Button title") forState:UIControlStateDisabled];
            }
                break;
            case SKDownloadStateFinished: {
                [purchase setEnabled:NO];
                purchase.hidden = NO;
                progress.hidden = YES;
                [purchase setTitle:LinguiLocalizedString(@"Finished", @"Button title") forState:UIControlStateDisabled];
            }
                break;
            case SKDownloadStatePaused: {
                [purchase setEnabled:YES];
                purchase.hidden = NO;
                progress.hidden = YES;
                [purchase setTitle:LinguiLocalizedString(@"Paused", @"Button title") forState:UIControlStateNormal];
            }
                break;
            case SKDownloadStateCancelled: {
                [purchase setEnabled:YES];
                purchase.hidden = NO;
                progress.hidden = YES;
                [purchase setTitle:LinguiLocalizedString(@"Cancelled", @"Button title") forState:UIControlStateNormal];
            }
                break;
            case SKDownloadStateFailed: {
                [purchase setEnabled:YES];
                purchase.hidden = NO;
                progress.hidden = YES;
                [purchase setTitle:LinguiLocalizedString(@"Failed", @"Button title") forState:UIControlStateNormal];
            }
                break;
        }
    }
    else if (purchase && progress) {
        SKProduct *product = [self.purchasedProducts objectAtIndex:contentView.tag];
        SKPaymentTransaction *transaction = [[HWIAPContentDelegate sharedManager] transactionForProductID:product.productIdentifier];
        purchase.enabled = (transaction == nil);
        purchase.hidden = NO;
        progress.hidden = YES;
        [purchase setTitle:LinguiLocalizedString(@"Restore", @"Button title") forState:UIControlStateNormal];
        [purchase setTitle:LinguiLocalizedString(@"Connecting", @"Button title") forState:UIControlStateDisabled];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == kSection_RestoreAll) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"RestoreAllCell" forIndexPath:indexPath];
    }
    else if (self.purchasedProducts.count > 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        cell.contentView.tag = indexPath.row;
        
        [self.contentViews filterUsingPredicate:[NSPredicate predicateWithFormat:@"self.tag != %d", indexPath.row]];
        [self.contentViews addObject:cell.contentView];
        
        if (self.purchasedProducts) {
            SKProduct *prod = [self.purchasedProducts objectAtIndex:indexPath.row];
            cell.textLabel.text = prod.localizedTitle;
            
            SKDownload *thisDownload = [[HWIAPContentDelegate sharedManager] downloadForProductID:prod.productIdentifier];
            
            [self setupContentView:cell.contentView forDownload:thisDownload];
        }
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        cell.textLabel.text = LinguiLocalizedString(@"Not available", @"Table cell text");
        cell.userInteractionEnabled = NO;
    }
    
    return cell;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == kSection_RestoreSingleItems) {
        return LinguiLocalizedString(@"Please note that downloads will be cancelled if HarmonyWiz enters the background whilst they are in progress.", @"Menu footer");
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == kSection_RestoreAll) {
        [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Restore all", @"Alert title") message:LinguiLocalizedString(@"This will download all HarmonyWiz sample packs that have been previously purchased on your iTunes Account. Are you sure you want to continue?", @"Alert message") cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Continue", @"Continue button") action:^{
            [[CargoManager sharedManager] restorePurchasedProducts];
            [HWAppDelegate recordEvent:@"restore_all_purchases"];
        }], nil] show];
    }
}

#pragma mark - cargo manager UI delegate

- (void)transactionDidFinishWithSuccess:(BOOL)success {
    [self.tableView reloadData];
}

- (void)restoredTransactionsDidFinishWithSuccess:(BOOL)success {
    [self.tableView reloadData];
}

- (void)downloadUpdated:(SKDownload *)download {
    if (![[self.purchasedProducts valueForKey:@"productIdentifier"] containsObject:download.transaction.payment.productIdentifier]) {
        [self updatePurchasedProducts];
    }
    
    const NSUInteger idx = [[self.purchasedProducts valueForKey:@"productIdentifier"] indexOfObject:download.transaction.payment.productIdentifier];
    if (idx == NSNotFound) {
        NSLog(@"product not found %@", download.transaction.payment.productIdentifier);
    }
    else {
        UIView *contentView = [[self.contentViews filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.tag == %d", idx]] anyObject];
        [self setupContentView:contentView forDownload:download];
    }
}

@end
