//
//  HWZoomNavigationController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 17/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWZoomNavigationController : UINavigationController

@end
