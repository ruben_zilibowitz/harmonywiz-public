//
//  HWZoomTransitionSegue.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 17/3/14.
//  Copyright (c) 2014 Ruben Zilibowitz. All rights reserved.
//  Based on ImageTransition by Joris Kluivers
//

#import <UIKit/UIKit.h>

@interface HWZoomTransitionSegue : UIStoryboardSegue

@property (atomic,assign) BOOL unwinding;

@property (atomic,assign) CGRect sourceRect;
@property (atomic,assign) CGRect destinationRect;

@property (atomic,strong) UIImage *transitionImage;

@end
