//
//  HWDocument.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWDocument.h"
#import "HWData.h"
#import "AudioController.h"
#import "HarmonyWizSong+MIDI.h"
#include "Properties.h"

#import "HarmonyWizArchiver.h"
#import "GZIP/GZIP.h"
#import "NSData+OTP.h"
#import "HWAppDelegate.h"

#define DATA_FILENAME       @"song.data"
#define METADATA_FILENAME   @"song.metadata"

#define kLevelsOfUndo   50

NSTimeInterval const HWBouncePaddingTime = 2;
NSTimeInterval const HWSampleLoadDelayApply = 0.1;

extern "C" int8_t const HWOTPKey;

int8_t const HWOTPKey = 0x2e;

@interface HWDocument ()
@property (nonatomic, strong) HWData * data;
@property (nonatomic, strong) NSFileWrapper * fileWrapper;
@end

@implementation HWDocument

- (id)initWithFileURL:(NSURL *)url {
    self = [super initWithFileURL:url];
    if (self) {
        // undo manager
        self.hwUndoManager = [[HarmonyWizUndoManager alloc] init];
        [self.hwUndoManager setLevelsOfUndo:kLevelsOfUndo];
        self.undoManager = self.hwUndoManager;
        
        // new song
        self.song = [[HarmonyWizSong alloc] init];
        [self.song setupSATB];
        
        // default note size
        self.metadata.notesPointSize = 42.f;
        self.metadata.noteSpacing = 8.f;
        
        // start in magic mode
        self.metadata.magicMode = ([HWAppDelegate newFileStartMode] == 0);
        
        // setup properties
        uint16_t numbits = HarmonyWiz::kNumProperties;
        for (uint16_t i = 0; i < numbits; i++)
            [self.metadata.harmonyRules addObject:@(YES)];
//        [self.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kBassAlwaysMoves withObject:@(NO)];
    }
    return self;
}

- (void)encodeObject:(id<NSCoding>)object toWrappers:(NSMutableDictionary *)wrappers preferredFilename:(NSString *)preferredFilename {
    @autoreleasepool {
        NSMutableData * data = [NSMutableData data];
        NSKeyedArchiver * archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        [archiver encodeObject:object forKey:@"data"];
        [archiver finishEncoding];
        NSFileWrapper * wrapper = [[NSFileWrapper alloc] initRegularFileWithContents:data];
        [wrappers setObject:wrapper forKey:preferredFilename];
    }
}

- (id)decodeObjectFromWrapperWithPreferredFilename:(NSString *)preferredFilename {
    @autoreleasepool {
        NSFileWrapper * fileWrapper = [self.fileWrapper.fileWrappers objectForKey:preferredFilename];
        if (!fileWrapper) {
            NSLog(@"Unexpected error: Couldn't find %@ in file wrapper!", preferredFilename);
            return nil;
        }
        
        NSData * data = [fileWrapper regularFileContents];
        NSKeyedUnarchiver * unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        
        return [unarchiver decodeObjectForKey:@"data"];
    }
}

- (id)contentsForType:(NSString *)typeName error:(NSError *__autoreleasing *)outError {
    NSMutableDictionary * wrappers = [NSMutableDictionary dictionary];
    [self encodeObject:self.data toWrappers:wrappers preferredFilename:DATA_FILENAME];
    [self encodeObject:self.metadata toWrappers:wrappers preferredFilename:METADATA_FILENAME];
    NSFileWrapper * fileWrapper = [[NSFileWrapper alloc] initDirectoryWithFileWrappers:wrappers];
    
    return fileWrapper;
}

- (HWMetadata *)metadata {
    if (_metadata == nil) {
        if (self.fileWrapper != nil) {
            self.metadata = [self decodeObjectFromWrapperWithPreferredFilename:METADATA_FILENAME];
        } else {
            self.metadata = [[HWMetadata alloc] init];
        }
    }
    return _metadata;
}

- (HWData *)data {
    if (_data == nil) {
        if (self.fileWrapper != nil) {
            //NSLog(@"Loading song for %@...", self.fileURL);
            self.data = [self decodeObjectFromWrapperWithPreferredFilename:DATA_FILENAME];
        } else {
            self.data = [[HWData alloc] init];
        }
    }
    return _data;
}

- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError *__autoreleasing *)outError {
    
    self.fileWrapper = (NSFileWrapper *) contents;
    
    // The rest will be lazy loaded...
    self.data = nil;
    self.metadata = nil;
    
    return YES;
}

- (NSString *) description {
    return [[self.fileURL lastPathComponent] stringByDeletingPathExtension];
}

- (void)autosaveWithCompletionHandler:(void (^)(BOOL success))completionHandler {
    if (self.disableAutosave)
        completionHandler(NO);
    else
        [super autosaveWithCompletionHandler:completionHandler];
}

#pragma mark Accessors

- (HarmonyWizSong *)song {
    return self.data.song;
}

- (void)setSong:(HarmonyWizSong *)song {
    
    if ([self.data.song isEqual:song]) return;
    
    self.data.song = song;
}

- (NSError*)openingSongError {
    return self.data.openingSongError;
}

#pragma mark - audio

- (void) bounceDocumentToPath:(NSString*)bouncePath withHUD:(MBProgressHUD *)progressHUD {
    AudioController *ac = [AudioController sharedAudioController];
    
    // apply audio settings
    [self applyAllAudioSettings];
    
    // create sequence and player
    MusicPlayer bouncePlayer;
    MusicSequence musicSequence = [self.song convertToMusicSequence:0 includeControlMessages:NO transposing:YES];
    NewMusicPlayer(&bouncePlayer);
    MusicPlayerSetSequence(bouncePlayer, musicSequence);
    
    // calculate number of crotchets per bar
    const float crotchetBeatsPerBeat = (4.f / self.song.timeSign.lowerNumeral);
    const float crotchetBeatsPerBar = crotchetBeatsPerBeat * self.song.timeSign.upperNumeral;
    
    // calculate padding time to allow reverb to fade out
    NSTimeInterval paddingTime = [self.song countEmptyBeatsAtEnd] * crotchetBeatsPerBeat * (60.f/self.song.tempo);
    if (paddingTime < HWBouncePaddingTime) {
        paddingTime = HWBouncePaddingTime;
    }
    
    // bounce to file
    const MusicTimeStamp endTime = (self.song.finalBarNumber * crotchetBeatsPerBar) + paddingTime*(self.song.tempo/60.f);
    
    [ac startProcessingGraph];
    [ac bouncePlayer:bouncePlayer duration:endTime outputPath:bouncePath progress:^(float x){progressHUD.progress = x;}];
    [ac stopProcessingGraph];
    
    DisposeMusicPlayer(bouncePlayer);
    DisposeMusicSequence(musicSequence);
}

- (void) applyAllAudioSettings {
    // reverb
    // track volumes
    // track instruments
    // mute, solo
    
    AudioController *ac = [AudioController sharedAudioController];
    NSArray *instrumentNames = [self.song.musicTracks valueForKey:@"instrument"];
    
    if (self.metadata.masterEffectsEnabled) {
        [ac enableReverb];
        [ac enableDynamicsProcessor];
        [ac applyReverb:self.metadata.masterReverb];
    }
    else {
        [ac disableReverb];
        [ac disableDynamicsProcessor];
    }
    
    [ac stopProcessingGraph];
    [ac updatePresetCountTo:instrumentNames.count];
    
    for (UInt8 i = 0; i < instrumentNames.count; i++) {
        NSString *instr = [instrumentNames objectAtIndex:i];
        if (!(instr == (id)[NSNull null] || instr.length == 0)) {
            BOOL result = [ac createPresetFromSamplesIn:instr withExtension:@"aif" forVoice:i];
            if (!result) {
                // create preset failed so revert instrument name to previous
                [[self.song.musicTracks objectAtIndex:i] setInstrument:[ac instrumentNameForVoice:i]];
            }
        }
        float gain = [[self.song.musicTracks objectAtIndex:i] volume];
        float pan = [[self.song.musicTracks objectAtIndex:i] pan];
        [ac setMixerInput:i gain:gain];
        [ac setMixerInput:i pan:pan];
        if (i+1 < instrumentNames.count) {
            [NSThread sleepForTimeInterval:HWSampleLoadDelayApply];
        }
    }
    [ac applyReverb:self.metadata.masterReverb];
    [self applyMuteAndSolo];
    
    [ac startProcessingGraph];
}

- (NSData*)dataForExport {
    NSString *filename = [self.fileURL.lastPathComponent stringByDeletingPathExtension];
    NSDictionary *saveData = @{ @"song" : self.song, @"metadata" : self.metadata, @"filename" : filename };
    NSData *data = [HarmonyWizArchiver archivedDataWithRootObject:saveData];
    data = [[data gzippedData] oneTimePad:HWOTPKey];
    
    return data;
}

- (NSSet*)mutedTracks {
    NSMutableSet *soloTracks = [NSMutableSet set];
    NSMutableSet *muteTracks = [NSMutableSet set];
    
    for (HarmonyWizMusicTrack *mt in self.song.musicTracks) {
        if (mt.solo)
            [soloTracks addObject:mt];
        if (mt.mute)
            [muteTracks addObject:mt];
    }
    if (soloTracks.count > 0) {
        for (HarmonyWizMusicTrack *mt in self.song.musicTracks) {
            if (! [soloTracks member:mt]) {
                [muteTracks addObject:mt];
            }
        }
    }
    
    return muteTracks;
}

- (void)applyMuteAndSolo {
    AudioController *ac = [AudioController sharedAudioController];
    
    NSSet *muteTracks = [self mutedTracks];
    
    for (HarmonyWizMusicTrack *mt in muteTracks) {
        [ac setChannel:(UInt32)mt.trackNumber muted:YES];
    }
    for (HarmonyWizMusicTrack *mt in self.song.musicTracks) {
        if (! [muteTracks member:mt]) {
            [ac setChannel:(UInt32)mt.trackNumber muted:NO];
        }
    }
}

@end
