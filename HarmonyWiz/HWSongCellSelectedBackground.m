//
//  HWSongCellSelectedBackground.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWSongCellSelectedBackground.h"

CGFloat const HWSelectFrameBorderSize = 4;

@interface HWSongCellSelectedBackground ()
@end

@implementation HWSongCellSelectedBackground

- (id)initWithFrame:(CGRect)frame imageView:(UIView*)aImageView
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = nil;
        self.opaque = NO;
        self.songImageView = aImageView;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [UIColorFromRGB(0x1994FB) set];
    [[UIBezierPath bezierPathWithRoundedRect:CGRectInset(self.songImageView.frame, -HWSelectFrameBorderSize, -HWSelectFrameBorderSize) cornerRadius:4] fill];
}

@end
