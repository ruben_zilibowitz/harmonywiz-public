//
//  HarmonyWizStaveView.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizSong.h"
#import "HarmonyWizTrack.h"

@class HWClefLabel;
@class HWTimeSignature;
@class HWKeySignature;
@class HWHarmonyLabel;
@class HWStavesScrollView;

@interface HarmonyWizStaveView : NSObject

@property (nonatomic,weak) HWStavesScrollView *parentScrollView;

@property (nonatomic,assign) UInt8 numberOfStaveLines;
@property (nonatomic,assign) NSUInteger trackNumber;
@property (nonatomic,assign) CGRect touchableFrame;

@property (nonatomic,strong) NSDate *touchStartDate;
@property (nonatomic,strong) NSDate *currentNoteStartDate;
@property (nonatomic,strong) HarmonyWizNoteEvent *noteBeforeSelection;
@property (nonatomic,strong) HarmonyWizMusicEvent *createdEvent;
@property (nonatomic,assign) SInt16 heldNoteValue;
@property (nonatomic,assign) CGFloat clefOriginY;
@property (nonatomic,strong) HWClefLabel *clefLabel;
@property (nonatomic,strong) HWTimeSignature *timeSignatureView;
@property (nonatomic,strong) HWKeySignature *keySignatureView;

- (NSArray*)allViews;
- (void)removeAllViewsFromSuperview;
- (void)layoutStaveViews;

- (UIFont*)notesFont;
- (UInt16)noteValueFromYLoc:(CGFloat)yloc;
- (UInt16)noteValueFromYLoc:(CGFloat)yloc raised:(BOOL)raised;
- (HarmonyWizMusicEvent*)closestEventToLoc:(CGPoint)loc;
- (HarmonyWizTrack*)track;
- (CGPoint)noteHeadLoc:(HarmonyWizMusicEvent*)event;

- (void)updateAllAccidentals;
- (void)removeUnusedEvents;
- (void)removeInvisibleEvents;
- (void)generateStaveEvents;
- (void)generateEverything;
- (void)updateStaveEventsIn:(NSArray *)staveEvents;
- (void)updateStave;
- (void)updateKeySignatureOrMode;
- (void)updateTimeSignature;
- (void)flushStave;
- (void)updateStaveLines;
- (void)generateBarLines;
- (void)layoutHarmonyLabel:(HWHarmonyLabel*)harmonyLabel;
- (NSArray*)staveEventsFromMusicEvents:(NSArray*)events;

@end
