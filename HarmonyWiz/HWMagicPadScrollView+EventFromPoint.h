//
//  HWMagicPadScrollView+EventFromPoint.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWMagicPadScrollView.h"

@interface HWMagicPadScrollView (EventFromPoint)

- (HarmonyWizNoteEvent*)musicEventFromPoint:(CGPoint)pt pos:(NSUInteger)pos dur:(UInt16)dur;
- (int16_t)stepsFromMusicNote:(HarmonyWizNoteEvent*)firstNote to:(HarmonyWizNoteEvent*)lastNote;

@end
