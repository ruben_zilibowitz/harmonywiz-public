//
//  HWIAPContentDelegate.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWIAPContentDelegate.h"
#import "NSString+Paths.h"
#import "HWAppDelegate.h"

@interface HWIAPContentDelegate ()
@end

@implementation HWIAPContentDelegate

+ (id)sharedManager {
    static HWIAPContentDelegate *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (NSArray *)productIdentifiers {
    return @[@"com.wizdommusic.guitar1",
             @"com.wizdommusic.synth1",
             @"com.wizdommusic.synth2",
             @"com.wizdommusic.thumbjam_pack_1",
             @"com.wizdommusic.thumbjam_pack_2"];
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction {
    if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
        [HWAppDelegate recordEvent:@"harmonywiz-payment-event" segmentation:@{ @"identifier" : transaction.payment.productIdentifier } sum:1.];
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:transaction.payment.productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)productPurchased:(NSString*)productID {
    return [[NSUserDefaults standardUserDefaults] boolForKey:productID];
}

- (void)showInstallationFailedAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Installation failed"
                                                    message:@"The installation of your downloaded samples failed. Please try again later."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)showDownloadFailedAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download failed"
                                                    message:@"The download of your samples failed. Please try again later."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)resetIdleTimerDisabled {
    [((HWAppDelegate*)[[UIApplication sharedApplication] delegate]) resetIdleTimerDisabled];
}

- (void)downloadUpdated:(SKDownload *)download {
    switch (download.downloadState) {
        case SKDownloadStateFinished: {
            [self resetIdleTimerDisabled];
            BOOL result = [self handleCompletedDownload:download];
            if (!result) {
                [self showInstallationFailedAlert];
            }
        }
            break;
        case SKDownloadStateCancelled:
            [self resetIdleTimerDisabled];
            NSLog(@"Download cancelled");
            break;
        case SKDownloadStateFailed:
            [self resetIdleTimerDisabled];
            [self showDownloadFailedAlert];
            NSLog(@"Download of %@ failed with error \"%@\"", download.transaction.payment.productIdentifier, download.error.localizedDescription);
            recordError(download.error);
            break;
            
        default:
            if (![UIApplication sharedApplication].idleTimerDisabled) {
                [UIApplication sharedApplication].idleTimerDisabled = YES;
            }
            break;
    }
}

- (SKPaymentTransaction*)transactionForProductID:(NSString*)productID {
    for (SKPaymentTransaction *transaction in [[SKPaymentQueue defaultQueue] transactions]) {
        if ([transaction.payment.productIdentifier isEqualToString:productID]) {
            return transaction;
        }
    }
    return nil;
}

- (SKDownload*)downloadForProductID:(NSString*)productID {
    for (SKPaymentTransaction *transaction in [[SKPaymentQueue defaultQueue] transactions]) {
        for (SKDownload *download in transaction.downloads) {
            if ([download.transaction.payment.productIdentifier isEqualToString:productID]) {
                return download;
            }
        }
    }
    return nil;
}

- (BOOL)handleCompletedDownload:(SKDownload*)download {
    const BOOL kLogDownloadContents = NO;
    NSLog(@"Download finished");
    
    BOOL isDirectory, fileExists;
    NSError *err;
    NSString *installLocation = [NSString samplerFilesPath];
    err = nil;
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:download.contentURL.path error:&err];
    if (err) {
        NSLog(@"%@", err);
        [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:nil];
        return NO;
    }
    if (contents) {
        for (id item in contents) {
            if (kLogDownloadContents)   NSLog(@"* %@", item);
            NSString *itemPath = [download.contentURL.path stringByAppendingPathComponent:item];
            fileExists = [[NSFileManager defaultManager] fileExistsAtPath:itemPath isDirectory:&isDirectory];
            if (!fileExists) {
                NSLog(@"Download corrupted");
                [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:nil];
                return NO;
            }
            if (isDirectory) {
                err = nil;
                NSArray *itemContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:itemPath error:&err];
                if (err) {
                    NSLog(@"%@", err);
                    [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:nil];
                    return NO;
                }
                if (itemContents != nil) {
                    for (id subitem in itemContents) {
                        if (kLogDownloadContents)   NSLog(@"** %@", subitem);
                        if ([item isEqualToString:@"Contents"]) {
                            NSString *subitemPath = [itemPath stringByAppendingPathComponent:subitem];
                            NSString *installedItemPath = [installLocation stringByAppendingPathComponent:subitem];
                            
                            // if file exists at intsall path, delete it
                            if ([[NSFileManager defaultManager] fileExistsAtPath:installedItemPath]) {
                                err = nil;
                                [[NSFileManager defaultManager] removeItemAtPath:installedItemPath error:&err];
                                if (err) {
                                    NSLog(@"%@", err);
                                    [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:nil];
                                    return NO;
                                }
                            }
                            
                            // move the downloaded item to the install path
                            err = nil;
                            [[NSFileManager defaultManager] moveItemAtPath:subitemPath toPath:installedItemPath error:&err];
                            if (err) {
                                NSLog(@"%@", err);
                                [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:nil];
                                return NO;
                            }
                        }
                    }
                }
                else {
                    [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:nil];
                    return NO;
                }
            }
        }
    }
    else {
        [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:nil];
        return NO;
    }
    err = nil;
    [[NSFileManager defaultManager] removeItemAtURL:download.contentURL error:&err];
    if (err) {
        return NO;
    }
    return YES;
}

- (void)pauseAllDownloads {
    for (SKPaymentTransaction *transaction in [[SKPaymentQueue defaultQueue] transactions]) {
        [[SKPaymentQueue defaultQueue] pauseDownloads:transaction.downloads];
    }
}

- (void)resumeAllDownloads {
    for (SKPaymentTransaction *transaction in [[SKPaymentQueue defaultQueue] transactions]) {
        [[SKPaymentQueue defaultQueue] resumeDownloads:transaction.downloads];
    }
}

- (BOOL)cancelAllDownloads {
    BOOL anythingCancelled = NO;
    for (SKPaymentTransaction *transaction in [[SKPaymentQueue defaultQueue] transactions]) {
        if (transaction.downloads.count > 0) {
            [[SKPaymentQueue defaultQueue] cancelDownloads:transaction.downloads];
            anythingCancelled = YES;
        }
    }
    return anythingCancelled;
}

@end
