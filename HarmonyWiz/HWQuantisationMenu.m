//
//  HWQuantisationMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 20/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWQuantisationMenu.h"
#import "HWCollectionViewController.h"

extern NSString * const HWQuantizationNotification;

@interface HWQuantisationMenu ()

@end

@implementation HWQuantisationMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    {
        self.lastSelectedCell = [NSIndexPath indexPathForRow:[HWCollectionViewController currentlyOpenDocument].metadata.quantization inSection:0];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.lastSelectedCell];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    [dnc postNotificationName:HWQuantizationNotification object:self userInfo:@{ @"quantization" : @(indexPath.row) }];
    [tableView cellForRowAtIndexPath:self.lastSelectedCell].accessoryType = UITableViewCellAccessoryNone;
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    self.lastSelectedCell = indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
