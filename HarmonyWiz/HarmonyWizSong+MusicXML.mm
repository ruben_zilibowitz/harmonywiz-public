//
//  HarmonyWizSong+MusicXML.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/08/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong+MusicXML.h"
#import "HarmonyWizSong+SplitEvent.h"
#import "HarmonyWizSong+ScaleNote.h"
#import "HarmonyWizSong+Accidentals.h"
#import "NSFileManager+UniqueTemporaryDirectory.h"
#import "XMLWriter.h"
#import "SSZipArchive/SSZipArchive.h"

#import "HWAppDelegate.h"   // for recordError

#include <vector>
#include <map>
#include <sstream>
extern "C" {
    #include "divmod.h"
}

#include "ScaleNote.h"

#include "elements.h"
#include "factory.h"
#include "xml.h"
#include "xmlfile.h"

using namespace HarmonyWiz;
using namespace MusicXML2;
using namespace std;

@implementation HarmonyWizSong (MusicXML)

//------------------------------------------------------------------------
static Sxmlattribute newAttribute(const string& name, const string& value)
{
	Sxmlattribute attribute = xmlattribute::create();
	attribute->setName(name);
	attribute->setValue(value);
	return attribute;
}

//------------------------------------------------------------------------
static Sxmlattribute newAttributeI(const string& name, int value)
{
	Sxmlattribute attribute = xmlattribute::create();
	attribute->setName(name);
	attribute->setValue(value);
	return attribute;
}

//------------------------------------------------------------------------
static Sxmlelement newElement(int type, const string& value)
{
	Sxmlelement elt = factory::instance().create(type);
	elt->setValue (value);
	return elt;
}

//------------------------------------------------------------------------
static Sxmlelement newElementI(int type, int value)
{
	Sxmlelement elt = factory::instance().create(type);
	elt->setValue (value);
	return elt;
}

static Sxmlelement clefElement(int clefType) {
	Sxmlelement clef = factory::instance().create(k_clef);
    switch (clefType) {
        case kClef_Treble:
            clef->push (newElement(k_sign, "G"));
            clef->push (newElement(k_line, "2"));
            break;
        case kClef_Alto:
            clef->push (newElement(k_sign, "C"));
            clef->push (newElement(k_line, "3"));
            break;
        case kClef_Tenor:
            clef->push (newElement(k_sign, "C"));
            clef->push (newElement(k_line, "4"));
            break;
        case kClef_Bass:
            clef->push (newElement(k_sign, "F"));
            clef->push (newElement(k_line, "4"));
            break;
            
        default:
            break;
    }
    return clef;
}

static Sxmlelement timeSignElement(HarmonyWizTimeSignEvent *timeSign) {
	Sxmlelement time = factory::instance().create(k_time);
	time->push (newElement(k_beats, [[NSString stringWithFormat:@"%d", timeSign.upperNumeral] UTF8String]));
	time->push (newElement(k_beat_type, [[NSString stringWithFormat:@"%d", timeSign.lowerNumeral] UTF8String]));
    return time;
}

static Sxmlelement keySignElement(HarmonyWizKeySignEvent *keySign) {
	Sxmlelement time = factory::instance().create(k_key);
    time->push(newElement(k_fifths, [[NSString stringWithFormat:@"%d", keySign.sharpsFlatsCount] UTF8String]));
    time->push(newElement(k_mode, keySign.tonality == kTonality_Major ? "major" : "minor"));
    return time;
}

static Sxmlelement makeAttributes(HarmonyWizMusicTrack *track, HarmonyWizSong *song) {
	Sxmlelement attributes = factory::instance().create(k_attributes);
	attributes->push (newElementI(k_divisions, song.beatSubdivisions));
    
	Sxmlelement key = keySignElement(song.keySign);
	attributes->push (key);
	Sxmlelement time = timeSignElement(song.timeSign);
	attributes->push (time);
	Sxmlelement clef = clefElement(track.clef);
	attributes->push (clef);
	
	return attributes;
}

static NSString *partIDFromTrack(HarmonyWizMusicTrack *track) {
    return [NSString stringWithFormat:@"P%ld", (long)track.trackNumber];
}

static NSString *instrumentIDFromTrack(HarmonyWizMusicTrack *track) {
    return [NSString stringWithFormat:@"I%ld", (long)track.trackNumber];
}

static void addDurationGraphic(Sxmlelement element, HarmonyWizMusicEvent *event, HarmonyWizSong *song) {
    const NSArray* durationStrings = @[
        @"_", @"16th", @"eighth", @"dotted eighth",
        @"quarter", @"_", @"dotted quarter", @"doubledotted quarter",
        @"half", @"_", @"_", @"_",
        @"dotted half", @"_", @"doubledotted half", @"_",
        @"whole", @"_", @"_", @"_",
        @"_", @"_", @"_", @"_",
        @"dotted whole", @"_", @"_", @"_",
        @"_", @"_", @"_", @"_",
        @"double", @"_", @"_", @"_",
        @"_", @"_", @"_", @"_",
        @"_", @"_", @"_", @"_",
        @"_", @"_", @"_", @"_",
        @"dotted double"];
    
    UInt16 semiquaversDur = ([song eventDuration:event] / (song.beatSubdivisions/4) * (4.f/song.timeSign.lowerNumeral));
    const NSString *durString = durationStrings[semiquaversDur];
    if ([durString isEqualToString:@"_"]) {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"Unkown note duration type" userInfo:nil];
    }
    NSArray *components = [durString componentsSeparatedByString:@" "];
    element->push (newElement(k_type, [components.lastObject UTF8String]));					// creates the graphic elements of the note
    if ([components.firstObject isEqualToString:@"dotted"]) {
        element->push (factory::instance().create(k_dot));
    }
    else if ([components.firstObject isEqualToString:@"doubledotted"]) {
        element->push (factory::instance().create(k_dot));
        element->push (factory::instance().create(k_dot));
    }
}

static Sxmlelement makePart(int measureCount, HarmonyWizMusicTrack *track, HarmonyWizSong *song) {
    const NSArray* majorScales = @[
                                   @[@"C",@"D",@"E",@"F",@"G",@"A",@"B"],
                                   @[@"Db",@"Eb",@"F",@"Gb",@"Ab",@"Bb",@"C"],
                                   @[@"D",@"E",@"F#",@"G",@"A",@"B",@"C#"],
                                   @[@"Eb",@"F",@"G",@"Ab",@"Bb",@"C",@"D"],
                                   @[@"E",@"F#",@"G#",@"A",@"B",@"C#",@"D#"],
                                   @[@"F",@"G",@"A",@"Bb",@"C",@"D",@"E"],
                                   @[@"Gb",@"Ab",@"Bb",@"Cb",@"Db",@"Eb",@"F"],
                                   @[@"G",@"A",@"B",@"C",@"D",@"E",@"F#"],
                                   @[@"Ab",@"Bb",@"C",@"Db",@"Eb",@"F",@"G"],
                                   @[@"A",@"B",@"C#",@"D",@"E",@"F#",@"G#"],
                                   @[@"Bb",@"C",@"D",@"Eb",@"F",@"G",@"A"],
                                   @[@"B",@"C#",@"D#",@"E",@"F#",@"G#",@"A#"]
                                   ];
    const NSArray* minorScales = @[
                                   @[@"C",@"D",@"Eb",@"F",@"G",@"Ab",@"Bb"],
                                   @[@"C#",@"D#",@"E",@"F#",@"G#",@"A",@"B"],
                                   @[@"D",@"E",@"F",@"G",@"A",@"Bb",@"C"],
                                   @[@"Eb",@"F",@"Gb",@"Ab",@"Bb",@"Cb",@"Db"],
                                   @[@"E",@"F#",@"G",@"A",@"B",@"C",@"D"],
                                   @[@"F",@"G",@"Ab",@"Bb",@"C",@"Db",@"Eb"],
                                   @[@"F#",@"G#",@"A",@"B",@"C#",@"D",@"E"],
                                   @[@"G",@"A",@"Bb",@"C",@"D",@"Eb",@"F"],
                                   @[@"G#",@"A#",@"B",@"C#",@"D#",@"E",@"F#"],
                                   @[@"A",@"B",@"C",@"D",@"E",@"F",@"G"],
                                   @[@"Bb",@"C",@"Db",@"Eb",@"F",@"Gb",@"Ab"],
                                   @[@"B",@"C#",@"D",@"E",@"F#",@"G",@"A"]
                                   ];
    const NSArray* scales = (song.keySign.tonality == kTonality_Major ? majorScales : minorScales);
    const NSArray* scale = [scales objectAtIndex:song.keySign.root];
    
	Sxmlelement part = factory::instance().create(k_part);
	part->add (newAttribute("id", [partIDFromTrack(track) UTF8String]));
	for (int num = 1; num <= measureCount; num++) {
        map<SInt16,ScaleNote::ScaleNoteAlteration> effectiveAccidental;
        
        Sxmlelement measure = factory::instance().create(k_measure);
        measure->add (newAttributeI("number", num));
        if (num==1) {					//  creates specific elements of the first measure
            measure->push(makeAttributes(track, song));		// division, time, clef...
        }
        
        for (HarmonyWizMusicEvent *originalEvent in track.events) {
            if ([song barNumberFromEvent:originalEvent]+1 <= num && num <= [song barNumberFromEventEnd:originalEvent]+1) {
                NSArray *splittedEvents = [song recursivelySplitEvent:originalEvent];
                for (HarmonyWizMusicEvent *splittedEvent in splittedEvents) {
                    if ([song barNumberFromEvent:splittedEvent]+1 == num) {
                        Sxmlelement note = factory::instance().create(k_note);		// creates the note
                        
                        NSMutableArray *accidentals = [NSMutableArray array];
                        bool noteShouldNeutraliseAccidental = NO;
                        AccidentalType noteAccidentalType = kAccidental_None;
                        
                        if ([splittedEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            HarmonyWizNoteEvent *ne = (id)splittedEvent;
                            ScaleNote scaleNote = [song HWNoteToScaleNote:ne];
                            
                            // keep track of accidentals
                            if (effectiveAccidental[scaleNote.note] != scaleNote.alteration) {
                                const AccidentalType accidental = [song accidentalForScaleNote:scaleNote];
                                
                                // check if should neutralise
                                const AccidentalType prevAccidental = [song accidentalForScaleNote:
                                                                       ScaleNote(scaleNote.note,effectiveAccidental[scaleNote.note])];
                                const BOOL shouldNeutraliseAccidental = ((prevAccidental == kAccidental_DoubleSharp && accidental == kAccidental_Sharp) || (prevAccidental == kAccidental_DoubleFlat && accidental == kAccidental_Flat));
                                
                                noteShouldNeutraliseAccidental = shouldNeutraliseAccidental;
                                noteAccidentalType = accidental;
                                
                                // save alteration
                                effectiveAccidental[scaleNote.note] = scaleNote.alteration;
                            }
                            else {
                                noteShouldNeutraliseAccidental = NO;
                                noteAccidentalType = kAccidental_None;
                            }
                            
                            if (noteShouldNeutraliseAccidental) {
                                [accidentals addObject:@"natural"];
                            }
                            
                            if (noteAccidentalType == kAccidental_Natural) {
                                [accidentals addObject:@"natural"];
                            }
                            else if (noteAccidentalType == kAccidental_Sharp) {
                                [accidentals addObject:@"sharp"];
                            }
                            else if (noteAccidentalType == kAccidental_Flat) {
                                [accidentals addObject:@"flat"];
                            }
                            else if (noteAccidentalType == kAccidental_DoubleSharp) {
                                [accidentals addObject:@"double sharp"];
                            }
                            else if (noteAccidentalType == kAccidental_DoubleFlat) {
                                [accidentals addObject:@"double flat"];
                            }
                            
                            const NSString *pitchName = [[scale objectAtIndex:imod(scaleNote.note, 7)] substringToIndex:1];
                            NSString *alterationString = [[scale objectAtIndex:imod(scaleNote.note, 7)] substringFromIndex:1];
                            int alteration = 0;
                            if ([alterationString isEqualToString:@"#"]) {
                                alteration++;
                            }
                            else if ([alterationString isEqualToString:@"b"]) {
                                alteration--;
                            }
                            Sxmlelement pitch = factory::instance().create(k_pitch);	// creates a pitch
                            pitch->push (newElement(k_step, pitchName.UTF8String));				// sets the pitch
                            if (scaleNote.alteration == ScaleNote::kAlteration_Sharpen) {
                                alteration++;
                            }
                            else if (scaleNote.alteration == ScaleNote::kAlteration_Flatten) {
                                alteration--;
                            }
                            pitch->push (newElement(k_alter, [[NSString stringWithFormat:@"%d", alteration] UTF8String]));
                            pitch->push (newElementI(k_octave, idiv(ne.noteValue, 12)));		// sets the octave
                            note->push (pitch);											// adds the pitch to the note
                        }
                        else if ([splittedEvent isKindOfClass:[HarmonyWizRestEvent class]]) {
                            note->push (factory::instance().create(k_rest));
                        }
                        else {
                            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"Unkown event type" userInfo:nil];
                        }
                        note->push (newElementI(k_duration, [song eventDuration:splittedEvent] / (song.timeSign.lowerNumeral/4)));				// sets the note duration to a quarter note
                        
                        // add ties if necessary
                        if (splittedEvents.count > 1) {
                            if (splittedEvent != splittedEvents.firstObject) {
                                Sxmlelement tie = factory::instance().create(k_tie);
                                tie->add(newAttribute("type", "stop"));
                                note->push(tie);
                            }
                            if (splittedEvent != splittedEvents.lastObject) {
                                Sxmlelement tie = factory::instance().create(k_tie);
                                tie->add(newAttribute("type", "start"));
                                note->push(tie);
                            }
                        }
                        
                        addDurationGraphic(note, splittedEvent, song);
                        
                        for (NSString *str in accidentals) {
                            note->push(newElement(k_accidental, str.UTF8String));
                        }
                        
                        measure->push (note);		// and finally adds the note to the measure
                    }
                }
            }
        }
        
        if (num == song.finalBarNumber) {
            Sxmlelement barline = factory::instance().create(k_barline);
            barline->add(newAttribute("location", "right"));
            barline->push(newElement(k_bar_style, "light-heavy"));
            measure->push(barline);
        }
        
		part->push (measure);     // adds a new measure to the part
    }
	return part;
}

static Sxmlelement makePartList(HarmonyWizSong *song) {
	Sxmlelement partlist = factory::instance().create(k_part_list);
    for (HarmonyWizMusicTrack *track in song.musicTracks) {
        Sxmlelement scorepart = factory::instance().create(k_score_part);
        scorepart->add (newAttribute("id", [partIDFromTrack(track) UTF8String]));
        scorepart->push (newElement(k_part_name, track.title.UTF8String));
        Sxmlelement scoreinstr = factory::instance().create(k_score_instrument);
        scoreinstr->add (newAttribute("id", [instrumentIDFromTrack(track) UTF8String]));
        scoreinstr->push (newElement(k_instrument_name, (track.instrument ? track.instrument.UTF8String : "?")));
        scorepart->push (scoreinstr);
        partlist->push(scorepart);
    }
	return partlist;
}

static Sxmlelement makeIdentification(const char *creatorString) {
	Sxmlelement id = factory::instance().create(k_identification);
	Sxmlelement encoding = factory::instance().create(k_encoding);
    
	Sxmlelement creator = newElement(k_creator, creatorString);
	creator->add(newAttribute("type", "Composer"));
	id->push (creator);
	
	encoding->push (newElement(k_software, "MusicXML Library v2"));
	id->push (encoding);
	return id;
}

static Sxmlelement theMusic(int measuresCount, const char *title, const char *creatorString, HarmonyWizSong *song) {
	Sxmlelement score = factory::instance().create(k_score_partwise);
	score->push (newElement(k_movement_title, title));
	score->push (makeIdentification(creatorString));
    score->push(makePartList(song));
    for (HarmonyWizMusicTrack *track in song.musicTracks) {
        score->push(makePart(measuresCount, track, song));
    }
	return score;
}

- (NSString*)musicXML {
    ostringstream stream;
    
	SXMLFile f = TXMLFile::create();
	f->set( new TXMLDecl("1.0", "", TXMLDecl::kNo));
	f->set( new TDocType("score-partwise"));
	f->set( theMusic([self finalBarNumber], self.title.UTF8String, [[[UIDevice currentDevice] name] UTF8String], self) );
	f->print(stream);
	
    std::string text = stream.str();
    
    return [NSString stringWithUTF8String:text.c_str()];
}

- (NSData*)MXL_data:(NSString*)filename {
    NSError *error = nil;
    
    NSString *xml = [self musicXML];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *tempDir = [fm createUniqueTemporaryDirectory];
    if (tempDir == nil) {
        recordError([NSError errorWithDomain:@"Failed to create temporary directory" code:1 userInfo:nil])
        return nil;
    }
    NSString *mainDirPath = [tempDir stringByAppendingPathComponent:filename];
    NSString *metaInfDirPath = [mainDirPath stringByAppendingPathComponent:@"META-INF"];
    
    // create the directory structure
    [fm createDirectoryAtPath:metaInfDirPath withIntermediateDirectories:YES attributes:nil error:&error];
    if (error) {
        recordError(error)
        return nil;
    }
    
    // write the main MusicXML file
    [xml writeToFile:[[mainDirPath stringByAppendingPathComponent:filename] stringByAppendingPathExtension:@"xml"] atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        recordError(error)
        return nil;
    }
    
    // create the container.xml file
    XMLWriter *xmlWriter = [[XMLWriter alloc] init];
    [xmlWriter writeStartDocumentWithEncodingAndVersion:@"UTF-8" version:@"1.0"];
    [xmlWriter writeStartElement:@"container"];
    {
        [xmlWriter writeStartElement:@"rootfiles"];
        {
            [xmlWriter writeStartElement:@"rootfile"];
            [xmlWriter writeAttribute:@"full-path" value:[filename stringByAppendingPathExtension:@"xml"]];
            [xmlWriter writeAttribute:@"media-type" value:@"application/vnd.recordare.musicxml+xml"];
            [xmlWriter writeEndElement];
        }
        [xmlWriter writeEndElement];
    }
    [xmlWriter writeEndElement];
    
    // now write the container.xml file to disk
    [[xmlWriter toString] writeToFile:[[metaInfDirPath stringByAppendingPathComponent:@"container"] stringByAppendingPathExtension:@"xml"] atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        recordError(error)
        return nil;
    }
    
    NSString *MXL_path = [mainDirPath stringByAppendingPathExtension:@"mxl"];
    [SSZipArchive createZipFileAtPath:MXL_path withContentsOfDirectory:mainDirPath];
    
    NSData *data = [NSData dataWithContentsOfFile:MXL_path options:0 error:&error];
    if (error) {
        recordError(error)
        return nil;
    }
    
    [fm removeItemAtPath:tempDir error:&error];
    if (error) {
        recordError(error)
    }
    
    return data;
}

@end
