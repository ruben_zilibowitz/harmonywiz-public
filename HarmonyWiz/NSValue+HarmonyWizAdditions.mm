//
//  NSValue+HarmonyWizAdditions.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "NSValue+HarmonyWizAdditions.h"

using namespace HarmonyWiz;

@implementation NSValue (ScaleNote)

+ (NSValue*)valueWithHWScaleNote:(ScaleNote)sn {
    NSValue *val = [NSValue valueWithBytes:&sn objCType:@encode(ScaleNote)];
    return val;
}

- (ScaleNote)HWScaleNoteValue {
    ScaleNote sn;
    [self getValue:&sn];
    return sn;
}

+ (NSValue*)valueWithHWChordType:(HarmonyWiz::Figures::ChordType)ch {
    NSValue *val = [NSValue valueWithBytes:&ch objCType:@encode(HarmonyWiz::Figures::ChordType)];
    return val;
}

- (HarmonyWiz::Figures::ChordType)HWChordTypeValue {
    HarmonyWiz::Figures::ChordType ch;
    [self getValue:&ch];
    return ch;
}

+ (NSValue*)valueWithHWChordInversion:(HarmonyWiz::Figures::ChordInversion)ch {
    NSValue *val = [NSValue valueWithBytes:&ch objCType:@encode(HarmonyWiz::Figures::ChordInversion)];
    return val;
}

- (HarmonyWiz::Figures::ChordInversion)HWChordInversionValue {
    HarmonyWiz::Figures::ChordInversion ch;
    [self getValue:&ch];
    return ch;
}

+ (NSValue*)valueWithHWChordTypeWithInversion:(HarmonyWiz::Figures::ChordTypeWithInversion)ch {
    NSValue *val = [NSValue valueWithBytes:&ch objCType:@encode(HarmonyWiz::Figures::ChordTypeWithInversion)];
    return val;
}

- (HarmonyWiz::Figures::ChordTypeWithInversion)HWChordTypeWithInversionValue {
    HarmonyWiz::Figures::ChordTypeWithInversion ch;
    [self getValue:&ch];
    return ch;
}

@end
