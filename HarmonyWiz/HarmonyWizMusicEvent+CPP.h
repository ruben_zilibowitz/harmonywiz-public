//
//  HarmonyWizMusicEvent+CPP.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizEvent.h"

@interface HarmonyWizMusicEvent (CPP)

- (void)roundPitch:(HarmonyWizKeySignEvent*)keySign;
- (void)roundPitchModal:(HarmonyWizModeEvent*)mode;
- (void)makeRaised:(BOOL)raised forKey:(HarmonyWizKeySignEvent*)keySign;

@end
