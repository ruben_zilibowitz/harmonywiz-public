//
//  NSString+Unichar.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Unichar)
+ (NSString*)stringWithUnichar:(unichar)ch;
@end
