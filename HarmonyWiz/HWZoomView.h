//
//  HWZoomView.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWZoomView : UIView
@property (nonatomic,assign) CGFloat scale;
@property (nonatomic,assign) CGFloat spacing;
@end
