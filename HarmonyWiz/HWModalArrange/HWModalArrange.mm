//
//  HWModalArrange.m
//  HWModalArrange
//
//  Created by Ruben Zilibowitz on 15/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import "HWModalArrange.h"
#import "../HarmonyWizSong.h"
#import "../HWModeMenu.h"
#import "../NSMutableArray+Sorted.h"
#import "../HWCollectionViewController.h"
#import "../HarmonyWizPiano.h"

#include "Reharmonise/reharmonise.h"
#include "Reharmonise/prettyprint.hpp"

#include <array>
#include <iostream>

using namespace Reharmonise;
using namespace std;

@implementation HWModalArrange

static vector< Maybe<Line> > convertTrack(HarmonyWizSong *song, HarmonyWizMusicTrack* track) {
    NSIndexSet *idxSet;
    bool found;
    vector< Maybe<Line> > outVec;
    
    outVec.clear();
    
    NSArray *unifiedEvents = [song unifiedEvents:track.events];
    
    NSEnumerator *en = song.harmony.events.objectEnumerator;
    HarmonyWizHarmonyEvent *he;
    while ((he = en.nextObject) != nil) {
        if (! he.noChord) {
            idxSet = [song overlappingEvents:unifiedEvents forEvent:he];
            found = false;
            if (idxSet.count > 0) {
                Pitch pitch = 0;
                vector<Pitch> suspension, passing;
                NSUInteger currentIndex = idxSet.firstIndex;
                while (currentIndex != NSNotFound) {
                    id ev = [unifiedEvents objectAtIndex:currentIndex];
                    if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                        HarmonyWizNoteEvent *ne = ev;
                        const BOOL noteIsConstraint = (ne.type == kNoteEventType_Harmony || (ne.type == kNoteEventType_Auto && track.locked));
                        if (!found && noteIsConstraint) {
                            pitch = ne.noteValue - [HarmonyWizMusicEvent middleC];
                            found = true;
                        }
                        else {
                            if (found) {
                                suspension.push_back(ne.noteValue);
                            }
                            else {
                                passing.push_back(ne.noteValue);
                            }
                        }
                    }
                    
                    currentIndex = [idxSet indexGreaterThanIndex:currentIndex];
                }
                if (found) {
                    outVec.push_back(Maybe<Line>(Line(pitch,suspension,passing)));
                }
            }
            if (!found) {
                outVec.push_back(Maybe<Line>());
            }
        }
    }
    
    return outVec;
}

bool belongsToTonicTriad(const Maybe<Line> &mline, const HarmonyWizModeEvent *mode) {
    bool result = true;
    const unsigned char transposition = mode.transposition;
    maybe_if(mline, [&result,transposition](const Line &line){
        const Pitch modPitch = imod(line.pitch - transposition, 12);
        result = (modPitch == 0 || modPitch == 3 || modPitch == 4 || modPitch == 7);
    });
    return result;
}

- (BOOL)modalArrange:(HarmonyWizSong*)song callback:(std::function<void(float)>)updateUI {
    NSAssert(song.keySign == nil, @"Song has a key selected");
    NSAssert(song.songMode != nil, @"Song does not have a mode selected");
    NSAssert(song.musicTracks.count == 4, @"Song must have four music tracks for modal arrange");
    
    const NSArray *modeAccidentalTypes = [song.songMode modeAccidentalTypes];
    
    Pitch transpose = song.songMode.transposition;
    Mode mode;
    vector<pair<SingleRule,string> > singleRules = SingleRules::kAll;
    vector<pair<DoubleRule,string> > doubleRules = DoubleRules::kAll;
    vector<pair<TripleRule,string> > tripleRules = TripleRules::kAll;
    PartialSong input;
    const TriadIntervalType intervalType = (TriadIntervalType)song.triadsType;
    
    // get mode
    for (NSNumber *pitch in song.songMode.modePitches) {
        mode.push_back(pitch.intValue);
    }
    
    // get song input
    {
        const Maybe<Line> anyLine;
        const PartialChord anyChord;
        PartialChord rootPositionTriad;
        rootPositionTriad.inversion = 0;
        rootPositionTriad.extensions = vector<Pitch>();
        if (intervalType == Tertian && song.songMode.modeID != kMode_Wholetone) {
            rootPositionTriad.allowedTriadTypes.insert(Reharmonise::MajorTriad);
            rootPositionTriad.allowedTriadTypes.insert(Reharmonise::MinorTriad);
        }
        PartialChord tonicRootPositionTriad = rootPositionTriad;
        tonicRootPositionTriad.triad.root = transpose;
        PartialChord startChord = rootPositionTriad, endChord = rootPositionTriad;
        
        auto track0 = convertTrack(song, [song.musicTracks objectAtIndex:0]);
        auto track1 = convertTrack(song, [song.musicTracks objectAtIndex:1]);
        auto track2 = convertTrack(song, [song.musicTracks objectAtIndex:2]);
        auto track3 = convertTrack(song, [song.musicTracks objectAtIndex:3]);
        
        const size_t length = track0.size();
        
        // if in any mode except Nohkan or Octatonic
        // and first and last chords can be tonic
        // then force them to be tonic.
        if (intervalType == Tertian && song.songMode.modeID != kMode_Nohkan && song.songMode.modeID != kMode_Octatonic) {
            if (belongsToTonicTriad(track0[0],song.songMode) && belongsToTonicTriad(track1[0],song.songMode) && belongsToTonicTriad(track2[0],song.songMode) && belongsToTonicTriad(track3[0],song.songMode)) {
                startChord = tonicRootPositionTriad;
            }
            if (belongsToTonicTriad(track0[length-1],song.songMode) && belongsToTonicTriad(track1[length-1],song.songMode) && belongsToTonicTriad(track2[length-1],song.songMode) && belongsToTonicTriad(track3[length-1],song.songMode)) {
                endChord = tonicRootPositionTriad;
            }
        }
        
        for (size_t i = 0; i < length; i++) {
            PartialChord chd = anyChord;
            if (i == 0) {
                chd = startChord;
            }
            else if (i == length - 1) {
                chd = endChord;
            }
            input.push_back( make_pair(chd, make_tuple(track0[i],track1[i],track2[i],track3[i])));
        }
    }
    
    if (transpose != 0) {
        transform(mode.begin(), mode.end(), mode.begin(), [transpose](Pitch x){ return x + transpose; });
    }
    
    if (song.songMode.modeID == kMode_Wholetone) {
        // for whole tone mode, use a limited rule set
        singleRules = {SingleRules::kAll[0],SingleRules::kAll[1],SingleRules::kAll[2],SingleRules::kAll[5]};
        tripleRules = {TripleRules::kAll[2],TripleRules::kAll[3]};
    }
    
    Reharmonise::Style style = defaultStyle(mode,intervalType,singleRules,doubleRules,tripleRules);
    Reharmonise::Solver solver(style, updateUI, &_cancelArrange);
    
    Maybe<Song> result = solver.startSolveOnce(input.cbegin(), input.cend());
    
    if (self.cancelArrange) {
        // arrangement cancelled
        return NO;
    }
    
    // reset guard fail number
    self.lastGuardNumberToFail = 0;
    
    BOOL solutionFound;
    maybe_if(result, [&solutionFound,style,input,solver,song,modeAccidentalTypes](Song& solution){
        // optimise it (twice)
        optimiseSongVector(style, input, solution);
        optimiseSongVector(style, input, solution);
        // print it out
        cout << "guard calls " << solver.getGuardCalls() << endl;
        cout << "solution" << endl;
        cout << solution << endl;
        // insert back to song
        if (1) {
            array<vector<Pitch>, 4> tracks;
            for (auto x : solution) {
                tracks[0].push_back(get<0>(x.second));
                tracks[1].push_back(get<1>(x.second));
                tracks[2].push_back(get<2>(x.second));
                tracks[3].push_back(get<3>(x.second));
            }
            [song clearArrangement];
            {
                size_t row = 0;
                NSEnumerator *en = song.musicTracks.objectEnumerator;
                HarmonyWizMusicTrack *track;
                size_t startCol = 0;    // doing whole song for now, instead of selection
                size_t endCol = song.harmonyEventCount;
                while ((track = en.nextObject) != nil) {
                    size_t col = 0;
                    
                    NSMutableArray *newEvents = [NSMutableArray arrayWithCapacity:track.events.count];
                    NSEnumerator *en1 = track.events.objectEnumerator;
                    HarmonyWizMusicEvent *ev;
                    while ((ev = en1.nextObject) != nil) {
                        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && (((HarmonyWizNoteEvent*)ev).wasCreatedByUser || track.locked || col < startCol || col >= endCol)) {
                            [newEvents placeObject:ev];
                        }
                    }
                    
                    NSEnumerator *en2 = song.harmony.events.objectEnumerator;
                    HarmonyWizHarmonyEvent *he;
                    while ((he = en2.nextObject) != nil) {
                        if (! he.noChord) {
                            NSIndexSet *idxs = [song overlappingEvents:track.events
                                                              forEvent:he];
                            BOOL userEvent = NO;
                            if (idxs.count > 0) {
                                
                                NSUInteger currentIndex = idxs.firstIndex;
                                while (currentIndex != NSNotFound) {
                                    id ev = [track.events objectAtIndex:currentIndex];
                                    if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                                        HarmonyWizNoteEvent *ne = ev;
                                        if (ne.wasCreatedByUser || track.locked || col < startCol || col >= endCol) {
                                            userEvent = YES;
                                            if (![newEvents containsObject:ne]) {
                                                [newEvents placeObject:ne];
                                            }
                                        }
                                    }
                                    
                                    currentIndex = [idxs indexGreaterThanIndex:currentIndex];
                                }
                            }
                            if (!userEvent && col >= startCol && col < endCol) {
                                const Pitch pitch = tracks[row][col]; //this->at(row,col);
                                HarmonyWizNoteEvent *ne = [HarmonyWizNoteEvent new];
                                ne.beat = he.beat;
                                ne.subdivision = he.subdivision;
                                ne.durationBeats = he.durationBeats;
                                ne.durationSubdivisions = he.durationSubdivisions;
                                ne.type = kNoteEventType_Auto;
                                ne.noteValue = pitch + [HarmonyWizMusicEvent middleC];
                                const int shift = [modeAccidentalTypes[(ne.noteValue%12+12)%12] intValue];
                                switch (shift) {
                                    case -2:
                                        ne.enharmonicShift = kForceDoubleFlat;
                                        break;
                                    case -1:
                                        ne.enharmonicShift = kSharpsToFlats;
                                        break;
                                    case 0:
                                        ne.enharmonicShift = kDefaultAccidental;
                                        break;
                                    case 1:
                                        ne.enharmonicShift = kFlatsToSharps;
                                        break;
                                    case 2:
                                        ne.enharmonicShift = kForceDoubleSharp;
                                        break;
                                        
                                    default:
                                        break;
                                }
                                [newEvents placeObject:ne];
                            }
                            col++;
                            if (col >= endCol)
                                break;
                        }
                    }
                    track.events = newEvents;
                    [song addFillerRestsTo:track.events];
                    row++;
                }
            }
            [song tieAdjacentAutoNotesPreservingSelection:nil];
        }
        
        // a solution was found!
        solutionFound = YES;
    }).otherwise([&solutionFound,solver,self](){
        // no solutions found
        solutionFound = NO;
        
        // get the guard number that failed
        self.lastGuardNumberToFail = solver.getGuardNumber();
        self.lastGuardNameToFail = [NSString stringWithUTF8String:solver.getGuardName().c_str()];
        
        // get furthest column
        self.furthestColumnReached = solver.getFurthestColumnReached();
    });
    
    return solutionFound;
}

@end
