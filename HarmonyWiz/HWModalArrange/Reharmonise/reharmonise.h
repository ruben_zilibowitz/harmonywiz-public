// Reharmonise
// Functions and types for solving four part harmonisation using modes
// Author: Ruben Zilibowitz

#include <vector>
#include <map>
#include <set>
#include <tuple>
#include <functional>
#include <algorithm>
#include <utility>
#include <string>
#include <sstream>
#include <cassert>

#include "maybe.hpp"

namespace Reharmonise {

#error Public Repository - The code from this file is currently closed source
    
}   // namespace Reharmonise
