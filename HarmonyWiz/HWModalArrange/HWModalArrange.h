//
//  HWModalArrange.h
//  HWModalArrange
//
//  Created by Ruben Zilibowitz on 15/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <functional>

@interface HWModalArrange : NSObject
@property (atomic) bool cancelArrange;
@property (atomic) uint64_t lastGuardNumberToFail;
@property (atomic) NSString *lastGuardNameToFail;
@property (atomic) size_t furthestColumnReached;
- (BOOL)modalArrange:(id)song callback:(std::function<void(float)>)updateUI;
@end
