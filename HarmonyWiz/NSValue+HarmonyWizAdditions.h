//
//  NSValue+HarmonyWizAdditions.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "ScaleNote.h"
#include "Figures.h"

@interface NSValue (ScaleNote)

+ (NSValue*)valueWithHWScaleNote:(HarmonyWiz::ScaleNote)sn;
- (HarmonyWiz::ScaleNote)HWScaleNoteValue;

+ (NSValue*)valueWithHWChordType:(HarmonyWiz::Figures::ChordType)ch;
- (HarmonyWiz::Figures::ChordType)HWChordTypeValue;

+ (NSValue*)valueWithHWChordInversion:(HarmonyWiz::Figures::ChordInversion)ch;
- (HarmonyWiz::Figures::ChordInversion)HWChordInversionValue;

+ (NSValue*)valueWithHWChordTypeWithInversion:(HarmonyWiz::Figures::ChordTypeWithInversion)ch;
- (HarmonyWiz::Figures::ChordTypeWithInversion)HWChordTypeWithInversionValue;

@end
