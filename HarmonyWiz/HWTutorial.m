//
//  HWTutorial.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWTutorial.h"
#import "HWTutorialPage.h"
#import "NSString+Concatenation.h"

@interface HWTutorial ()
@property (nonatomic,strong,readonly) NSString *name;
@property (nonatomic,assign,readonly) NSUInteger pageCount;
@end

@implementation HWTutorial

- (id)initWithName:(NSString*)name {
    self = [super init];
    if (self) {
        _name = name;
        NSString *path = [[NSBundle mainBundle] pathForResource:[@"images/Tutorial/" : name] ofType:nil];
        NSArray *tutorialPages = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
        _pageCount = tutorialPages.count / 2;
    }
    return self;
}

- (HWTutorialPage *)viewControllerAtIndex:(NSUInteger)index {
    
    HWTutorialPage *childViewController = [[HWTutorialPage alloc] initWithName:self.name];
    childViewController.index = index;
    
    return childViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(HWTutorialPage *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    else {
        index--;
    }
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(HWTutorialPage *)viewController index];
    
    index++;
    
    if (index == self.pageCount) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return self.pageCount;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

@end
