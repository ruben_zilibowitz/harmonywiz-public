//
//  HWInstalledInstrumentsViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWInstalledInstrumentsViewController : UITableViewController

@end
