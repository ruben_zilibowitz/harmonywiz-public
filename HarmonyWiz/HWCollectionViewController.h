//
//  HWCollectionViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "HWSongViewController.h"
#import "MBProgressHUD/MBProgressHUD.h"

@class HWSongViewController;

@interface HWCollectionViewController : UICollectionViewController <UITextFieldDelegate, MFMailComposeViewControllerDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate, MBProgressHUDDelegate>

+ (HWDocument*)currentlyOpenDocument;
- (IBAction) unwindAction:(UIStoryboardSegue *)segue;

- (void)shareSongWithMethod:(NSString*)method indexPath:(NSIndexPath*)indexPath;

@property (nonatomic,weak) IBOutlet UIBarButtonItem *actionButton;
@property (nonatomic,assign) CGRect zoomTransitionSourceRect;
@property (nonatomic,weak) HWSongViewController *songVC;

@end
