//
//  HarmonyWizToolboxController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 28/06/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HarmonyWizEvent.h"

@interface HarmonyWizToolboxController : NSObject <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIPopoverControllerDelegate>

- (void)setup;
- (void)updateHighlights;
- (void)updateEnabled;

@property (nonatomic,weak) IBOutlet UICollectionView* toolboxView;
@property (nonatomic,weak) UIView *keyboardWizdom;

@end

extern NSString * const HWToolChangeNotification;
