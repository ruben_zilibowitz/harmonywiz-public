//
//  HWTracksScrollView.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWViewKeepingTop.h"

@interface HWTracksContentsView : HWViewKeepingTop
- (NSArray*)tracks;
@end

@interface HWTracksScrollView : UIScrollView
@property (nonatomic,strong) HWTracksContentsView *contentView;
- (NSArray*)tracks;
@end
