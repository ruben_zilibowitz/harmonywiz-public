//
//  NSData+Hex.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 20/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Hex)

- (NSString *)hexadecimalString;

@end
