//
//  HarmonyWizSong+MIDI.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizSong.h"
#import <AudioToolbox/MusicPlayer.h>

@interface HarmonyWizSong (MIDI)

- (MusicSequence)convertToMusicSequence:(UInt16)precount includeControlMessages:(BOOL)includeControlMessages transposing:(BOOL)transposing;
- (NSData*)convertToMIDIData;

- (BOOL)importFromMIDIWithURL:(NSURL*)url;

@end
