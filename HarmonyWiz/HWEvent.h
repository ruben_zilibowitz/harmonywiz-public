//
//  HWEvent.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HarmonyWizEvent.h"

@interface HWEvent : NSObject
@property (nonatomic,strong,readonly) HarmonyWizMusicEvent *event;
@property (nonatomic,strong) NSArray *labels;
@property (nonatomic,strong) NSArray *ties;
@property (nonatomic,strong) NSArray *harmonyInfoLabels;
- (NSComparisonResult)compare:(HWEvent*)obj;
+ (HWEvent*)eventFor:(HarmonyWizMusicEvent*)event;
- (void)setAlpha:(CGFloat)alpha;
- (void)removeViewsFromSuperview;
@end
