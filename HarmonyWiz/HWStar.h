//
//  HWStar.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 31/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWStar : UIView
@property (nonatomic,assign) CGPoint destination;
@property (nonatomic,strong) UIColor *color;
@end
