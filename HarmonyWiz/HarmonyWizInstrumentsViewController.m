//
//  HarmonyWizInstrumentsViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 13/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HarmonyWizInstrumentsViewController.h"
#import "HWCollectionViewController.h"
#import "NSString+Paths.h"
#import "NSString+Concatenation.h"
#import <StoreKit/StoreKit.h>

NSString * const HWSelectAllTrackNotification = @"HWSelectAllTrack";
NSString * const HWRemoveTrackNotification = @"HWRemoveTrack";
NSString * const HWFreezeTrackNotification = @"HWFreezeTrack";
NSString * const HWClearTrackNotification = @"HWClearTrack";
extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;

enum {
    kSection_Instruments = 0,
    kSection_TrackActions,
    kNumSections
};

enum {
    kRow_SelectAll = 0,
    kRow_Clear,
    kRow_Freeze,
    kRow_Remove,
    kNumTrackActionRows
};

@interface HarmonyWizInstrumentsViewController ()
@end

@implementation HarmonyWizInstrumentsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.octaveViewController = [[HWOctaveTransposeViewController alloc] initWithStyle:UITableViewStyleGrouped];
        self.purchaseSamplesViewController = [[HarmonyWizPurchaseSamplesViewController alloc] initWithStyle:UITableViewStyleGrouped];
#if (! RELEASE_BUILD)
        self.downloadSamplesViewController = [[HarmonyWizDownloadSamplesViewController alloc] initWithStyle:UITableViewStyleGrouped];
#endif
        self.instrumentSelectViewController = [[HWInstrumentSelectViewController alloc] initWithStyle:UITableViewStyleGrouped];
#if (! RELEASE_BUILD)
        self.downloadSamplesViewController.currentInstrumentNames = @[];
#endif
        self.instrumentSelectViewController.currentInstrumentNames = @[];
        self.instrumentSelectViewController.purchaseSamplesViewController = self.purchaseSamplesViewController;
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setTrackControl:(HarmonyWizTrackControl *)trackControl {
    _trackControl = trackControl;
    self.instrumentSelectViewController.trackControl = trackControl;
    self.octaveViewController.trackControl = trackControl;
}

- (void)updateInstrumentNames {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [NSURL fileURLWithPath:[NSString samplerFilesPath]];
    NSError *error = nil;
    NSArray *installed = [fileManager contentsOfDirectoryAtURL:url includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    if (error) {
        [NSException raise:@"HarmonyWizError" format:@"contentsOfDirectoryAtURL: %@", error.localizedDescription];
    }
    
    NSURL *mainBundleSamples = [[NSBundle mainBundle] URLForResource:@"Sampler Files" withExtension:nil];
    NSArray *shipped = [fileManager contentsOfDirectoryAtURL:mainBundleSamples includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    if (error) {
        [NSException raise:@"HarmonyWizError" format:@"contentsOfDirectoryAtURL: %@", error.localizedDescription];
    }
    
    NSArray *results = [[installed arrayByAddingObjectsFromArray:shipped] valueForKey:@"lastPathComponent"];
    
    // sort results
    results = [results sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
    
    self.instrumentNames = results;
#if (! RELEASE_BUILD)
    self.downloadSamplesViewController.currentInstrumentNames = results;
#endif
    self.instrumentSelectViewController.currentInstrumentNames = results;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.preferredContentSize = CGSizeMake(320, 500);
    self.navigationItem.title = [self.trackControl.track.title : @" " : LinguiLocalizedString(@"Instruments", @"Instruments menu title")];
//    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self updateInstrumentNames];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case kSection_TrackActions:
            return nil;//return @"Track actions";
            break;
        case kSection_Instruments:
            return LinguiLocalizedString(@"Instruments", @"Instruments menu title");
            
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == kSection_TrackActions)
        return kNumTrackActionRows;
    else if (section == kSection_Instruments)
#if RELEASE_BUILD
        return 3;
#else
        return 4;
#endif
    else
        return -1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == kSection_TrackActions) {
        
        // hide separator lines
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, self.tableView.bounds.size.width, 0, 0)];
            [cell setIndentationWidth:-self.tableView.bounds.size.width];
            [cell setIndentationLevel:1];
        }
        else {
            cell.backgroundView = [UIView new];
        }
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.accessoryView = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = (indexPath.row == kRow_Remove ? [UIColor redColor] : [UIColor blueColor]);
    }
    else if (indexPath.section == kSection_Instruments) {
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.accessoryView = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == kSection_Instruments) {
        return [NSString stringWithFormat:LinguiLocalizedString(@"MIDI output is on channel #%d", @"Table view footer"), self.trackControl.trackNumber+1];
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    static NSString *DefaultCellIdentifier = @"DefaultCell";
    static NSString *Value1CellIdentifier = @"Value1Cell";
    
    if (indexPath.section == kSection_TrackActions) {
        cell = [tableView dequeueReusableCellWithIdentifier:DefaultCellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DefaultCellIdentifier];
        }
        switch (indexPath.row) {
            case kRow_SelectAll:
                cell.textLabel.text = LinguiLocalizedString(@"Select all", @"Button text");
                break;
            case kRow_Remove:
                cell.textLabel.text = LinguiLocalizedString(@"Remove", @"Button text");
                break;
            case kRow_Freeze:
                cell.textLabel.text = LinguiLocalizedString(@"Freeze", @"Button text");
                break;
            case kRow_Clear:
                cell.textLabel.text = LinguiLocalizedString(@"Clear", @"Button text");
                break;
        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        BOOL enabled = (self.trackControl.trackNumber == 0 && indexPath.row != kRow_Remove) || expertMode;
        cell.textLabel.enabled = enabled;
        cell.userInteractionEnabled = enabled;
    }
    else if (indexPath.section == kSection_Instruments) {
        if (indexPath.row == 0 || indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:Value1CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:Value1CellIdentifier];
            }
        }
        else {
            cell = [tableView dequeueReusableCellWithIdentifier:DefaultCellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DefaultCellIdentifier];
            }
        }
        
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = LinguiLocalizedString(@"Select", @"Button text");
                cell.detailTextLabel.text = self.trackControl.instrumentName;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 1:
                cell.textLabel.text = LinguiLocalizedString(@"Octave transpose", @"Button text");
                if (self.trackControl.transposition == -12)
                    cell.detailTextLabel.text = @"- 1";
                else if (self.trackControl.transposition == 0)
                    cell.detailTextLabel.text = @"0";
                else if (self.trackControl.transposition == 12)
                    cell.detailTextLabel.text = @"+ 1";
                else
                    cell.detailTextLabel.text = @"?";
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 2:
                cell.textLabel.text = LinguiLocalizedString(@"Purchase Instruments", @"Button text");
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 3:
                cell.textLabel.text = LinguiLocalizedString(@"Download Instruments", @"Button text");
                cell.textLabel.textColor = [UIColor blueColor];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            default:
                [NSException raise:@"HarmonyWiz" format:@"Unkown cell row"];
                break;
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    
    if (indexPath.section == kSection_TrackActions) {
        switch (indexPath.row) {
            case kRow_SelectAll:
                [dnc postNotificationName:HWSelectAllTrackNotification object:self userInfo:@{ @"trackControl" : self.trackControl }];
                break;
            case kRow_Remove:
                if ([HWCollectionViewController currentlyOpenDocument].song.keySign != nil) {
                    [dnc postNotificationName:HWRemoveTrackNotification object:self userInfo:@{ @"trackControl" : self.trackControl }];
                }
                else if ([HWCollectionViewController currentlyOpenDocument].song.songMode != nil) {
                    [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Remove Track", @"Alert title") message:LinguiLocalizedString(@"When using modes you must have four tracks. To remove this track, first change to a major or minor key signature.", @"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
                }
                break;
            case kRow_Freeze:
                [dnc postNotificationName:HWFreezeTrackNotification object:self userInfo:@{ @"trackControl" : self.trackControl }];
                break;
            case kRow_Clear:
                [dnc postNotificationName:HWClearTrackNotification object:self userInfo:@{ @"trackControl" : self.trackControl }];
                break;
        }
    }
    else if (indexPath.section == kSection_Instruments) {
        switch (indexPath.row) {
            case 0:
                [self.navigationController pushViewController:self.instrumentSelectViewController animated:YES];
                break;
            case 1:
                [self.navigationController pushViewController:self.octaveViewController animated:YES];
                break;
            case 2:
                if ([SKPaymentQueue canMakePayments]) {
                    [self.navigationController pushViewController:self.purchaseSamplesViewController animated:YES];
                    
                }
                else {
                    [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Purchase Instruments", @"Alert title") message:LinguiLocalizedString(@"In-app purchase is not available on this device.", @"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK Button") otherButtonTitles:nil] show];
                }
                break;
#if (! RELEASE_BUILD)
            case 3:
                [self.navigationController pushViewController:self.downloadSamplesViewController animated:YES];
                break;
#endif
        }
    }
}

@end
