//
//  HarmonyWizClefCell.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HarmonyWizClefCell : UITableViewCell
@property (nonatomic, assign) enum CellClefType {
    kClef_Treble = 0,
    kClef_Alto,
    kClef_Tenor,
    kClef_Bass
    } clef;
@end
