//
//  HWChordRootViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWChordRootViewController.h"
#import "HWCollectionViewController.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "HWChordAttributesViewController.h"

@interface HWChordRootViewController ()

@end

@implementation HWChordRootViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    HWChordAttributesViewController *cavc = [self.navigationController.viewControllers objectAtIndex:0];
    NSValue *deg = [cavc.harmonyEvent.constraints objectForKey:HarmonyWizDegreeProperty];
    if (deg) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        HarmonyWiz::ScaleNote sn = deg.HWScaleNoteValue;
        if (doc.song.keySign.tonality == kTonality_Major) {
            self.selectedRow = sn.note + 1;
        }
        else if (sn.note < 5) {
            self.selectedRow = sn.note + 1;
        }
        else if (sn.note == 5 && sn.alteration == HarmonyWiz::ScaleNote::kAlteration_None) {
            self.selectedRow = 6;
        }
        else if (sn.note == 5 && sn.alteration == HarmonyWiz::ScaleNote::kAlteration_Sharpen) {
            self.selectedRow = 7;
        }
        else if (sn.note == 6 && sn.alteration == HarmonyWiz::ScaleNote::kAlteration_None) {
            self.selectedRow = 8;
        }
        else if (sn.note == 6 && sn.alteration == HarmonyWiz::ScaleNote::kAlteration_Sharpen) {
            self.selectedRow = 9;
        }
    }
    else {
        self.selectedRow = 0;
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:self.selectedRow inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    return (doc.song.keySign.tonality == kTonality_Major ? 8 : 10);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    BOOL isMajor = (doc.song.keySign.tonality == kTonality_Major);
    
    if (isMajor) {
        cell.textLabel.text = [@[LinguiLocalizedString(@"Any", @"Cell text"),@"I",@"II",@"III",@"IV",@"V",@"VI",@"VII"] objectAtIndex:indexPath.row];
    }
    else {
        cell.textLabel.text = [@[LinguiLocalizedString(@"Any", @"Cell text"),@"I",@"II",@"III",@"IV",@"V",@"VI",@"#VI",@"VII",@"#VII"] objectAtIndex:indexPath.row];
    }
    
    cell.accessoryType = (indexPath.row == self.selectedRow ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    
    return cell;
}

- (void)updateConstraintsForHarmonyEvent:(HarmonyWizHarmonyEvent*)harmonyEvent {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [doc.hwUndoManager startUndoableChanges];
    [harmonyEvent.properties removeAllObjects];
    HarmonyWiz::ScaleNote sn;
    if (self.selectedRow == 0) {
        [harmonyEvent.constraints removeObjectForKey:HarmonyWizDegreeProperty];
    }
    else if (doc.song.keySign.tonality == kTonality_Major) {
        sn = HarmonyWiz::ScaleNote(self.selectedRow - 1);
        [harmonyEvent.constraints setObject:[NSValue valueWithHWScaleNote:sn] forKey:HarmonyWizDegreeProperty];
    }
    else {
        switch (self.selectedRow) {
            case 9:
                sn = HarmonyWiz::ScaleNote(6,HarmonyWiz::ScaleNote::kAlteration_Sharpen);
                break;
            case 8:
                sn = HarmonyWiz::ScaleNote(6);
                break;
            case 7:
                sn = HarmonyWiz::ScaleNote(5,HarmonyWiz::ScaleNote::kAlteration_Sharpen);
                break;
            default:
                sn = HarmonyWiz::ScaleNote(self.selectedRow - 1);
                break;
        }
        [harmonyEvent.constraints setObject:[NSValue valueWithHWScaleNote:sn] forKey:HarmonyWizDegreeProperty];
    }
    [doc.hwUndoManager finishUndoableChangesName:@"Chord root"];
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return LinguiLocalizedString(@"Setting this attribute will force the root of the chord to have a certain degree.", @"Table view footer");
}

@end
