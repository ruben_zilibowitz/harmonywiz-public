//
//  divmod.c
//  Tonalis
//
//  Created by Ruben Zilibowitz on 8/11/11.
//  Copyright (c) 2011 N.A. All rights reserved.
//

#include "divmod.h"

short imod(short x, short y) {
    return (x%y+y)%y;
}

short idiv(short x, short y) {
    if (x >= 0) {
        return x/y;
    }
    else {
        return (x+1)/y-1;
    }
}
