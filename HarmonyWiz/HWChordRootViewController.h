//
//  HWChordRootViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWChordAttributeSubmenuViewController.h"

@interface HWChordRootViewController : HWChordAttributeSubmenuViewController

@end
