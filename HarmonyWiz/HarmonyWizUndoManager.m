//
//  HarmonyWizUndoManager.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizUndoManager.h"
#import "HWDocument.h"
#import "HWCollectionViewController.h"
#import "NSString+Paths.h"
#import "HarmonyWizArchiver.h"
#import "HarmonyWizUnarchiver.h"
#import "HWAppDelegate.h"

NSString * const HarmonyWizRevertUndoNotification = @"RevertUndo";

@interface HarmonyWizUndoManager ()
@property (nonatomic,strong) NSString *lastUndoPointPath;
@end

@implementation HarmonyWizUndoManager

- (id)init {
    self = [super init];
    if (self) {
        NSError *error = nil;
        
        BOOL isDirectory;
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[NSString undoPointsPath] isDirectory:&isDirectory];
        
        if (fileExists) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString undoPointsPath] error:&error];
            if (error) {
                NSLog(@"%@", error.localizedDescription);
                recordError(error)
            }
        }
        
        error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:[NSString undoPointsPath] withIntermediateDirectories:YES attributes:nil error:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            recordError(error)
        }
    }
    return self;
}

- (BOOL)canStartUndoableChanges {
    return (self.lastUndoPointPath == nil);
}

- (NSString*)uniqueFileName {
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
    NSString *uniqueFileName = [[NSString stringWithFormat:@"UndoPoint_%@", guid] stringByAppendingPathExtension:HW_EXTENSION];
    
    return [[NSString undoPointsPath] stringByAppendingPathComponent:uniqueFileName];
}

- (void)startUndoableChanges {
    NSAssert([NSThread isMainThread], @"Must start undo block on main thread");
    NSAssert(self.lastUndoPointPath == nil, @"Cannot start undo when already inside an undo block");
    self.lastUndoPointPath = [self uniqueFileName];
    BOOL result = [HarmonyWizArchiver archiveRootObject:[HWCollectionViewController currentlyOpenDocument].song toFile:self.lastUndoPointPath];
    if (! result) {
        [NSException raise:@"HarmonyWiz" format:@"Unable to save undo point to disk"];
    }
}

- (void)cancelUndoableChanges {
    NSAssert([NSThread isMainThread], @"Must cancel undo block on main thread");
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:self.lastUndoPointPath error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        recordError(error)
    }
    self.lastUndoPointPath = nil;
}

- (void)cancelAndRevertUndoableChanges {
    NSError *error = nil;
    NSAssert([NSThread isMainThread], @"Must revert undo block on main thread");
    HarmonyWizSong *lastUndoSong = [HarmonyWizUnarchiver unarchiveObjectWithFile:self.lastUndoPointPath];
    [[NSFileManager defaultManager] removeItemAtPath:self.lastUndoPointPath error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        recordError(error)
    }
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    doc.song = lastUndoSong;
    self.lastUndoPointPath = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizRevertUndoNotification object:self];
}

- (void)finishUndoableChangesName:(NSString*)name {
    NSAssert([NSThread isMainThread], @"Must finish undo block on main thread");
    NSAssert(self.lastUndoPointPath != nil, @"Cannot finish undo when not inside an undo block");
    [self registerUndoWithTarget:self selector:@selector(undoChanges:) object:self.lastUndoPointPath];
    [self setActionName:name];
    self.lastUndoPointPath = nil;
    
    [[Crashlytics sharedInstance] setObjectValue:name forKey:@"lastUndoName"];
}

- (void)undoChanges:(NSString*)pathPoint {
    NSError *error = nil;
    HarmonyWizSong *lastUndoSong = [HarmonyWizUnarchiver unarchiveObjectWithFile:pathPoint];
    [[NSFileManager defaultManager] removeItemAtPath:pathPoint error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        recordError(error)
    }
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    {
        // set redo point
        NSString *redoPath = [self uniqueFileName];
        BOOL result = [HarmonyWizArchiver archiveRootObject:doc.song toFile:redoPath];
        if (! result) {
            [NSException raise:@"HarmonyWiz" format:@"Unable to save redo point to disk"];
        }
        [self registerUndoWithTarget:self selector:@selector(undoChanges:) object:redoPath];
    }
    doc.song = lastUndoSong;
    self.shouldRedisplay = YES;
    self.lastActionName = self.undoActionName;
}

@end
