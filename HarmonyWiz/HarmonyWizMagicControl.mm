//
//  HarmonyWizMagicControl.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 8/07/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizMagicControl.h"
#import "HarmonyWizSharedObjects.h"
#import "HarmonyWizSong.h"
#import "HarmonyWizSong+MIDI.h"
#import "AudioController.h"
#import "UIImage+Tint.h"
#include "ScaleNote.h"

CGFloat const kStarRadius = 30;
CGFloat const kNoteRadius = 10;
CGFloat const kLineWidth = 4;

@interface HarmonyWizMagicControl ()
@property (nonatomic,strong) NSMutableArray *brushPoints;
@property (nonatomic) NSTimeInterval trackingStartTime;
@property (nonatomic,strong) NSMutableArray *notePoints;
@property (nonatomic) MusicPlayer musicPlayer;
@property (nonatomic,weak) NSValue *highlightedPoint;
@end

@implementation HarmonyWizMagicControl

{
    UIBezierPath *path;
    UIImage *incrementalImage;
    CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
    uint ctr;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.multipleTouchEnabled = NO;
        self.opaque = YES;
    }
    return self;
}

- (void)reset {
    incrementalImage = nil;
    ctr = 0;
    path = [UIBezierPath bezierPath];
    [path setLineWidth:kLineWidth];
    self.notePoints = [NSMutableArray arrayWithCapacity:1];
    self.notes = [NSMutableArray arrayWithCapacity:1];
    self.numLines = 0;
}

- (void)drawStarAt:(CGPoint)pt radius:(CGFloat)r {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    float theta = 2.0 * M_PI * (2.0 / 5.0); // 144 degrees
    float flip = -1.0;
    
    CGContextMoveToPoint(ctx, pt.x, r*flip+pt.y);
    
    for (NSUInteger k=1; k<5; k++)
    {
        float x = r * sinf(k * theta);
        float y = r * cosf(k * theta);
        CGContextAddLineToPoint(ctx, x+pt.x, y*flip+pt.y);
    }
    
    CGContextFillPath(ctx);
}

- (void)drawRect:(CGRect)rect {
    [incrementalImage drawInRect:self.bounds];
    [[self paintColour] setStroke];
    [path stroke];
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    for (NSValue *notePoint in self.notePoints) {
        CGPoint pt = notePoint.CGPointValue;
        
        if (notePoint == self.highlightedPoint) {
            [[UIColor redColor] set];
            [self drawStarAt:pt radius:kStarRadius];
        }
        [[UIColor yellowColor] set];
        CGContextFillEllipseInRect(ctx, CGRectMake(pt.x-kNoteRadius, pt.y-kNoteRadius, 2*kNoteRadius, 2*kNoteRadius));
    }
}

- (UIColor *)paintColour {
    HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
    UIColor *color = nil;
    switch (so.currentNoteType) {
        case kNoteType_Harmony:
            color = [UIColor blueColor];
            break;
        case kNoteType_Passing:
            color = [UIColor cyanColor];
            break;
        case kNoteType_Suspension:
            color = [UIColor magentaColor];
            break;
        default:
            break;
    }
    return color;
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    ctr = 0;
    pts[0] = [touch locationInView:self];
    
    // initialise brush points
    self.brushPoints = [NSMutableArray arrayWithCapacity:1];
    self.trackingStartTime = touch.timestamp;
    
    return YES;
}

- (void)appendPointToPath:(CGPoint)p {
    ctr++;
    pts[ctr] = p;
    if (ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
        [path moveToPoint:pts[0]];
        [path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]]; // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
        
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[0]]];
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[1]]];
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[2]]];
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[3]]];
        
        // replace points and get ready to handle the next segment
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }
}

- (void)endPath {
    [self drawBitmap];
    [self setNeedsDisplay];
    [path removeAllPoints];
    ctr = 0;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint pt = [touch locationInView:self];
    [self appendPointToPath:pt];
    [self setNeedsDisplayInRect:CGRectInset(path.bounds, -kLineWidth, -kLineWidth)];
    
    return YES;
}

- (void)cancelTrackingWithEvent:(UIEvent *)event {
    self.brushPoints = nil;
    [self endPath];
}

- (HarmonyWizMusicEvent*)musicEventFromPoint:(CGPoint)pt idx:(UInt16)idx dur:(UInt16)dur {
    HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
    HarmonyWizMusicEvent *toolEvent = so.currentToolEvent;
    const UInt16 subdivsPerBar = so.song.timeSign.upperNumeral * so.song.beatSubdivisions;
    HarmonyWizMusicEvent *ev = [toolEvent copy];
    [so.song setEventPosition:ev to:(idx * dur + subdivsPerBar * self.numLines)];
    [so.song setEventDuration:ev to:dur];
    if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
        HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)ev;
        UInt16 noteVal = UInt16((1.f - pt.y / CGRectGetHeight(self.frame)) * (self.highestNote - self.lowestNote) + 0.5f) + self.lowestNote;
        bool isMinor = so.song.keySign.tonality == kTonality_Minor;
        HarmonyWiz::ScaleNote sn = HarmonyWiz::ScaleNote(noteVal, so.song.keySign.root, isMinor).roundUp(isMinor);
        ne.noteValue = sn.chromatic(so.song.keySign.root, isMinor);
        
        // insert passing notes into semiquaver passages
        if (dur == (so.song.beatSubdivisions * (so.song.timeSign.lowerNumeral / 4) / 4) && idx%2==1 && ne.type == kNoteEventType_Harmony) {
            ne.type = kNoteEventType_Passing;
        }
    }
    return ev;
}

- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.f);
    if (!incrementalImage) // first time; paint background white
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [[UIColor whiteColor] setFill];
        [rectpath fill];
    }
    [incrementalImage drawAtPoint:CGPointZero];
    [[self paintColour] setStroke];
    [path stroke];
    incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

static CGFloat bezierInterpolation(CGFloat t, CGFloat a, CGFloat b, CGFloat c, CGFloat d) {
    CGFloat t2 = t * t;
    CGFloat t3 = t2 * t;
    return a + (-a * 3 + t * (3 * a - a * t)) * t
    + (3 * b + t * (-6 * b + b * 3 * t)) * t
    + (c * 3 - c * 3 * t) * t2
    + d * t3;
}

- (void)generateNotes:(UITouch*)touch {
    
    // total length
    CGFloat totalLength = 0.f;
    for (NSUInteger brushIdx = 3; brushIdx < self.brushPoints.count; brushIdx += 4) {
        CGPoint p1 = [[self.brushPoints objectAtIndex:brushIdx-3] CGPointValue];
        CGPoint p2 = [[self.brushPoints objectAtIndex:brushIdx-2] CGPointValue];
        CGPoint p3 = [[self.brushPoints objectAtIndex:brushIdx-1] CGPointValue];
        CGPoint p4 = [[self.brushPoints objectAtIndex:brushIdx] CGPointValue];
        CGPoint point0 = p1;
        for (CGFloat t = 0.0; t <= 1.00001; t += 0.05) {
            CGPoint point = CGPointMake(bezierInterpolation(t, p1.x, p2.x, p3.x, p4.x), bezierInterpolation(t, p1.y, p2.y, p3.y, p4.y));
            totalLength += distance(point0, point);
            point0 = point;
        }
    }
    
    // total time
    NSTimeInterval totalTime = touch.timestamp - self.trackingStartTime;
    
    // average velocity
    float averageVelocity = totalLength / totalTime;
    
    HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
    
    UInt16 subdivsPerNote = so.song.beatSubdivisions * (so.song.timeSign.lowerNumeral / 4);
    
    // number of notes
    const CGFloat width = CGRectGetWidth(self.frame);
    if (averageVelocity > width) {
        subdivsPerNote /= 4;
    }
    else if (averageVelocity > 0.75f*width) {
        subdivsPerNote /= 2;
    }
    else if (averageVelocity > 0.5f*width) {
    }
    else if (averageVelocity > 0.25f*width) {
        subdivsPerNote *= 2;
    }
    else if (averageVelocity > 1.875f*width) {
        subdivsPerNote *= 3;
    }
    else {
        subdivsPerNote *= 4;
    }
    
    UInt16 subdivsPerBar = so.song.timeSign.upperNumeral * so.song.beatSubdivisions;
    UInt16 numNotes = subdivsPerBar / subdivsPerNote;
    
    // visualise notes
    if (numNotes > 1) {
        for (UInt16 curNote = 0; curNote < numNotes-1; curNote++) {
            const CGFloat visualNoteLength = totalLength / (numNotes-1) * curNote;
            
            CGFloat currentLength = 0.f;
            for (NSUInteger brushIdx = 3; brushIdx < self.brushPoints.count; brushIdx += 4) {
                CGPoint p1 = [[self.brushPoints objectAtIndex:brushIdx-3] CGPointValue];
                CGPoint p2 = [[self.brushPoints objectAtIndex:brushIdx-2] CGPointValue];
                CGPoint p3 = [[self.brushPoints objectAtIndex:brushIdx-1] CGPointValue];
                CGPoint p4 = [[self.brushPoints objectAtIndex:brushIdx] CGPointValue];
                CGPoint point0 = p1;
                for (CGFloat t = 0.0; t <= 1.00001; t += 0.05) {
                    CGPoint point = CGPointMake(bezierInterpolation(t, p1.x, p2.x, p3.x, p4.x), bezierInterpolation(t, p1.y, p2.y, p3.y, p4.y));
                    CGFloat dist = distance(point0, point);
                    currentLength += dist;
                    if (currentLength > visualNoteLength) {
                        float lambda = (visualNoteLength - currentLength + dist) / dist;
                        CGPoint notePt = plus(scalarMultiply(lambda, point), scalarMultiply(1 - lambda, point0));
                        [self.notePoints addObject:[NSValue valueWithCGPoint:notePt]];
                        HarmonyWizMusicEvent *ev = [self musicEventFromPoint:notePt idx:curNote dur:subdivsPerNote];
                        [self.notes addObject:ev];
                        goto end_loop;
                    }
                    point0 = point;
                }
            }
        end_loop: ;
        }
    }
    if (numNotes > 0) {
        [self.notePoints addObject:self.brushPoints.lastObject];
        [self.notes addObject:[self musicEventFromPoint:[self.brushPoints.lastObject CGPointValue] idx:numNotes-1 dur:subdivsPerNote]];
    }
    
    self.numLines = (self.numLines + 1);
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint pt = [touch locationInView:self];
    
    // append final point
    [self appendPointToPath:pt];
    [self endPath];
    
    // generate notes on line
    [self generateNotes:touch];
    
    // start playback for new sequence
    [self startPlayback];
}

const struct EndUserData {
    UInt32  length;
    UInt32  code;
} defaultMusicSequenceEndData = {4,'end.'};

const struct NoteUserData {
    UInt32  length;
    UInt32  code;
    UInt16  idx;
} defaultMusicSequenceNoteData = {6,'note'};

static void TheMusicSequenceUserCallback (
                                          void                      *inClientData,
                                          MusicSequence             inSequence,
                                          MusicTrack                inTrack,
                                          MusicTimeStamp            inEventTime,
                                          const MusicEventUserData  *inEventData,
                                          MusicTimeStamp            inStartSliceBeat,
                                          MusicTimeStamp            inEndSliceBeat
                                          ) {
    HarmonyWizMagicControl *aSelf = (__bridge HarmonyWizMagicControl*)inClientData;
    const struct EndUserData *endData = (const struct EndUserData*)inEventData;
    const struct NoteUserData *noteData = (const struct NoteUserData*)inEventData;
    const BOOL isEnd = (endData->length == defaultMusicSequenceEndData.length &&
                        endData->code == defaultMusicSequenceEndData.code);
    const BOOL isNote = (noteData->length == defaultMusicSequenceNoteData.length &&
                         noteData->code == defaultMusicSequenceNoteData.code);
    if (isEnd) {
        [aSelf performSelectorOnMainThread:@selector(restartPlayback) withObject:nil waitUntilDone:NO];
    }
    else if (isNote) {
        [aSelf performSelectorOnMainThread:@selector(blinkNote:) withObject:[NSNumber numberWithUnsignedShort:noteData->idx] waitUntilDone:NO];
    }
}

- (void)restartPlayback {
    MusicPlayerSetTime(self.musicPlayer, 0);
}

- (void)blinkNote:(NSNumber*)num {
    UInt16 idx = num.unsignedShortValue;
    self.highlightedPoint = [self.notePoints objectAtIndex:idx];
    [self setNeedsDisplay];
}

- (void)stopPlayback {
    if (self.musicPlayer) {
        OSStatus result;
        MusicSequence seq;
        
        result = MusicPlayerGetSequence(self.musicPlayer, &seq);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerGetSequence result is %ld", result];
        
        result = MusicPlayerStop(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStop result is %ld", result];
        
        result = DisposeMusicPlayer(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"DisposeMusicPlayer result is %ld", result];
        
        result = DisposeMusicSequence(seq);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"DisposeMusicSequence result is %ld", result];
        
        self.musicPlayer = nil;
    }
}

- (void)startPlayback {
    MusicTimeStamp time = -1;
    if (self.musicPlayer) {
        OSStatus result;
        result = MusicPlayerGetTime(self.musicPlayer, &time);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerGetTime result is %ld", result];
        [self stopPlayback];
    }
    
    HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
    
    // create song
    HarmonyWizSong *magicSong = [so.song copy];
    for (HarmonyWizMusicTrack *tr in magicSong.musicTracks) {
        if (tr.trackNumber != so.currentlySelectedTrackNumber) {
            [tr.events removeAllObjects];
        }
    }
    
    [[magicSong.musicTracks objectAtIndex:so.currentlySelectedTrackNumber] setEvents:self.notes];
    
    OSStatus result;
    
    {
        // Create music sequence
        MusicSequence musicSequence = [magicSong convertToMusicSequence];
        
        // create user track and set callback
        MusicTrack userTrack;
        result = MusicSequenceNewTrack(musicSequence, &userTrack);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicSequenceNewTrack result is %ld", result];
        
        const MusicTimeStamp endTime = self.numLines * magicSong.timeSign.upperNumeral * (4. / magicSong.timeSign.lowerNumeral);
        result = MusicTrackNewUserEvent(userTrack, endTime,
                                        (MusicEventUserData*)&defaultMusicSequenceEndData);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicTrackNewUserEvent result is %ld", result];
        
        UInt16 idx = 0;
        for (HarmonyWizMusicEvent *ev in self.notes) {
            const MusicTimeStamp noteTime = (MusicTimeStamp)[magicSong eventStartSubdivision:ev] / (MusicTimeStamp)magicSong.beatSubdivisions * (4. / magicSong.timeSign.lowerNumeral);
            struct NoteUserData noteData = defaultMusicSequenceNoteData;
            noteData.idx = idx;
            idx++;
            result = MusicTrackNewUserEvent(userTrack, noteTime, (MusicEventUserData*)&noteData);
            if (result != noErr)
                [NSException raise:@"HarmonyWizError" format:@"MusicTrackNewUserEvent result is %ld", result];
        }
        
        result = MusicSequenceSetUserCallback(musicSequence,
                                              TheMusicSequenceUserCallback,
                                              (__bridge void*)self);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicSequenceSetUserCallback result is %ld", result];
        
        if (so.midiDestination) {
            
            // set MIDI endpoint
            result = MusicSequenceSetMIDIEndpoint(musicSequence, so.midiDestination.endpoint);
            if (result != noErr)
                [NSException raise:@"HarmonyWizError" format:@"MusicSequenceSetMIDIEndpoint result is %ld", result];
        }
        else {
            
            // set processing graph
            result = MusicSequenceSetAUGraph(musicSequence, so.audioController.processingGraph);
            if (result != noErr)
                [NSException raise:@"HarmonyWizError" format:@"MusicSequenceSetAUGraph result is %ld", result];
            
            // set track nodes
            for (UInt32 i = 0; i < magicSong.musicTracks.count; i++) {
                MusicTrack musicTrack;
                result = MusicSequenceGetIndTrack(musicSequence, i, &musicTrack);
                if (result != noErr)
                    [NSException raise:@"HarmonyWizError" format:@"MusicSequenceGetIndTrack result is %ld", result];
                result = MusicTrackSetDestNode(musicTrack, [so.audioController samplerNodeForVoice:i]);
                if (result != noErr)
                    [NSException raise:@"HarmonyWizError" format:@"MusicTrackSetDestNode result is %ld", result];
            }
        }
        
        // create music player
        result = NewMusicPlayer(&_musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"NewMusicPlayer result is %ld", result];
        result = MusicPlayerSetSequence(self.musicPlayer, musicSequence);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerSetSequence result is %ld", result];
        if (time > 0) {
            result = MusicPlayerSetTime(self.musicPlayer, time);
            if (result != noErr)
                [NSException raise:@"HarmonyWizError" format:@"MusicPlayerSetTime result is %ld", result];
        }
        result = MusicPlayerStart(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStart result is %ld", result];
    }
}

#pragma mark - 2D vector operations

static CGPoint midpoint(CGPoint p, CGPoint q) {
    return CGPointMake(0.5f*(p.x+q.x), 0.5f*(p.y+q.y));
}

static CGFloat length_squared(CGPoint p, CGPoint q) {
    CGFloat dx = p.x - q.x;
    CGFloat dy = p.y - q.y;
    return (dx*dx + dy*dy);
}

static CGFloat distance(CGPoint p, CGPoint q) {
    return sqrtf(length_squared(p, q));
}

static CGPoint minus(CGPoint p, CGPoint q) {
    return CGPointMake(p.x - q.x, p.y - q.y);
}

static CGPoint plus(CGPoint p, CGPoint q) {
    return CGPointMake(p.x + q.x, p.y + q.y);
}

static CGFloat dot(CGPoint p, CGPoint q) {
    return (p.x * q.x + p.y * q.y);
}

static CGPoint scalarMultiply(CGFloat s, CGPoint p) {
    return CGPointMake(s * p.x, s * p.y);
}

// Return minimum distance between line segment vw and point p
static CGFloat minimum_distance_sqr(CGPoint v, CGPoint w, CGPoint p) {
    const CGFloat eps = 1e-6f;
    const CGFloat l2 = length_squared(v, w);  // i.e. |w-v|^2 -  avoid a sqrt
    if (l2 < eps) return length_squared(p, v);   // v == w case
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    const CGFloat t = dot(minus(p,v), minus(w,v)) / l2;
    if (t < 0.0) return length_squared(p, v);       // Beyond the 'v' end of the segment
    else if (t > 1.0) return length_squared(p, w);  // Beyond the 'w' end of the segment
    const CGPoint projection = plus(v, scalarMultiply(t, minus(w,v)));  // Projection falls on the segment
    return length_squared(p, projection);
}

@end
