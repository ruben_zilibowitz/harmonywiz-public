//
//  HarmonyWizSong+Lilypond.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong.h"

@interface HarmonyWizSong (Lilypond)

- (NSString*)lilypondScript;

@end
