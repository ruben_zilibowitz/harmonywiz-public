//
//  HWTriadsMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/02/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import "HWTriadsMenu.h"
#import "HWCollectionViewController.h"

@interface HWTriadsMenu ()

@end

@implementation HWTriadsMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:song.triadsType inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    if (indexPath.row != song.triadsType) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:song.triadsType inSection:0]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        song.triadsType = (int)indexPath.row;
        cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:song.triadsType inSection:0]];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

@end
