//
//  HWExceptions.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

// see http://club15cc.com/code/objective-c/dispelling-nsexception-myths-in-ios-can-we-use-try-catch-finally

#import <Foundation/Foundation.h>

/**
 Base class for exceptions resulting from runtime errors.  These should be caught and handled whereas NSExceptions are coder errors and instantly fatal.
 
 We use exception typing to indicate the specific error.  This also allows error messages to be centralised.
 */
@interface HWNonFatalException : NSException

/// @abstract
/// Defined by subclasses to save them from having to override
/// -init methods to set instance properties
+ (NSString *)theReason;

/// @abstract
+ (NSString *)theExplanation;

/** Longer explanation.  Equivalent to failureReason in NSError */
@property (nonatomic, strong, readonly) NSString *explanation;

+ (id)exception;

@end

/////////////////////////////////////////////////////////////////////////
// HWNonFatalException Sub-Classes - All in once nice place :)
/////////////////////////////////////////////////////////////////////////

@interface HWContextException : HWNonFatalException @end
@interface HWEventsSortedVerifyFailed : HWNonFatalException @end
@interface HWPlacedEventDoubledUpException : HWNonFatalException
@property (nonatomic,strong) id event1, event2;
+ (id)exceptionWithEvents:(id)ev1 :(id)ev2;
@end
@interface HWSongVersionException : HWNonFatalException
@property (nonatomic,strong,readonly) NSString *versionString;
+ (id)exceptionWithVersion:(NSString*)versionString;
@end

/////////////////////////////////////////////////////////////////////////
// Special handler for audio errors
/////////////////////////////////////////////////////////////////////////

#define HWThrowIfAudioError(x) {                                                \
    OSStatus st = x;                                                            \
    if (st == kAUGraphErr_CannotDoInCurrentContext)                             \
        [[HWContextException exception] raise];                                 \
    else if (st != noErr)                                                       \
        [NSException raise:@"HarmonyWizError" format:@"error code %ld", (long)st];    \
}

#define HWThrowIfError(x) {                                                     \
    OSStatus st = x;                                                            \
    if (st != noErr)                                                            \
        [NSException raise:@"HarmonyWizError" format:@"error code %ld", (long)st];    \
}
