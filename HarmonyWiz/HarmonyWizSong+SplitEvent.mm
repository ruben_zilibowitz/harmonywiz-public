//
//  HarmonyWizSong+SplitEvent.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizSong+SplitEvent.h"
#import "NSMutableArray+Sorted.h"

@implementation HarmonyWizSong (SplitEvent)

- (NSArray*) splitEventOnBeats:(HarmonyWizMusicEvent*)event {
    
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:1];
    
    {
        UInt16 eventStart = [self eventStartSubdivision:event];
        UInt16 eventEnd = [self eventEndSubdivision:event];
        UInt16 startBeat = eventStart / self.beatSubdivisions;
        UInt16 endBeat = eventEnd / self.beatSubdivisions + 1;
        for (UInt16 beat = startBeat; beat < endBeat; beat++) {
            UInt16 newEventStart = beat * self.beatSubdivisions;
            UInt16 newEventEnd = (beat+1) * self.beatSubdivisions;
            newEventStart = MAX(newEventStart, eventStart);
            newEventEnd = MIN(newEventEnd, eventEnd);
            if (newEventStart < newEventEnd) {
                HarmonyWizMusicEvent *ev = [event copy];
                [self setEventPosition:ev to:newEventStart];
                [self setEventDuration:ev to:newEventEnd - newEventStart];
                [result placeObject:ev];
            }
        }
    }
    
    if (result.count > 0 && [self eventStartSubdivision:[result objectAtIndex:0]] != [self eventStartSubdivision:event]) {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"split check 2" userInfo:nil];
    }
    if (result.count > 0 && [self eventEndSubdivision:result.lastObject] != [self eventEndSubdivision:event]) {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"split check 3" userInfo:nil];
    }
    for (NSUInteger idx = 1; idx < result.count; idx++) {
        if ([self eventEndSubdivision:[result objectAtIndex:idx-1]] != [self eventStartSubdivision:[result objectAtIndex:idx]]) {
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"split check 4" userInfo:nil];
        }
    }
    
    return result;
}

- (NSArray*) splitEventAcrossBeats:(HarmonyWizMusicEvent*)event withGradation:(UInt16)gradation {
    UInt16 start = [self eventStartSubdivision:event];
    UInt16 end = [self eventEndSubdivision:event];
    
    SInt16 startGradation = start / gradation;
    SInt16 endGradation = end / gradation;
    
    if (end % gradation == 0)
        endGradation--;
    
    NSAssert(startGradation <= endGradation, @"illegal event");
    
    if (startGradation == endGradation) {
        NSAssert(gradation > 1, @"cannot split at finer gradation");
        return [self splitEventAcrossBeats:event withGradation:gradation/2];
    }
    
    UInt16 splitPoint = (startGradation+1) * gradation;
    
    HarmonyWizMusicEvent * ev1 = [event copy];
    [self setEventDuration:ev1 to:splitPoint - start];
    
    HarmonyWizMusicEvent * ev2 = [event copy];
    [self setEventPosition:ev2 to:splitPoint];
    [self setEventDuration:ev2 to:end - splitPoint];
    
    return @[ev1, ev2];
}
 
- (NSArray*) splitEventAcrossBarlines:(HarmonyWizMusicEvent*)event {
    UInt16 startSubdiv = event.beat * self.beatSubdivisions + event.subdivision;
    UInt16 endSubdiv = startSubdiv + event.durationBeats * self.beatSubdivisions + event.durationSubdivisions;
    UInt16 subdivsPerBar = self.timeSign.upperNumeral * self.beatSubdivisions;
    UInt16 firstBar = (startSubdiv / subdivsPerBar + 1) * subdivsPerBar;
    
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:1];
    
    if (endSubdiv > firstBar) {
        {
            HarmonyWizMusicEvent *firstEvent = [event copy];
            UInt16 firstEventDuration = firstBar - startSubdiv;
            firstEvent.durationBeats = firstEventDuration / self.beatSubdivisions;
            firstEvent.durationSubdivisions = firstEventDuration % self.beatSubdivisions;
            [result placeObject:firstEvent];
        }
        
        UInt16 lastBar = (endSubdiv / subdivsPerBar) * subdivsPerBar;
        UInt16 lastEventDuration = endSubdiv - lastBar;
        if (lastEventDuration > 0) {
            HarmonyWizMusicEvent *lastEvent = [event copy];
            lastEvent.beat = lastBar / self.beatSubdivisions;
            lastEvent.subdivision = 0;
            lastEvent.durationBeats = lastEventDuration / self.beatSubdivisions;
            lastEvent.durationSubdivisions = lastEventDuration % self.beatSubdivisions;
            [result placeObject:lastEvent];
        }
        
        while (lastBar > firstBar) {
            HarmonyWizMusicEvent *newEvent = [event copy];
            newEvent.beat = firstBar / self.beatSubdivisions;
            newEvent.subdivision = 0;
            newEvent.durationBeats = self.timeSign.upperNumeral;
            newEvent.durationSubdivisions = 0;
            [result placeObject:newEvent];
            firstBar += subdivsPerBar;
        }
    }
    else {
        [result placeObject:event];
    }
    
    return result;
}


- (NSArray*) splitEventWithOddBeats:(HarmonyWizMusicEvent*)event {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:1];
    if (event.durationSubdivisions == 0) {
        switch (event.durationBeats) {
            case 5:
            {
                HarmonyWizMusicEvent *e1 = [event copy];
                e1.durationBeats = 3;
                e1.durationSubdivisions = 0;
                HarmonyWizMusicEvent *e2 = [event copy];
                e2.beat += 3;
                e2.durationBeats = 2;
                e2.durationSubdivisions = 0;
                [result placeObject:e1];
                [result placeObject:e2];
            }
                break;
                
            case 9:
            {
                HarmonyWizMusicEvent *e1 = [event copy];
                e1.durationBeats = 6;
                e1.durationSubdivisions = 0;
                HarmonyWizMusicEvent *e2 = [event copy];
                e2.beat += 6;
                e2.durationBeats = 3;
                e2.durationSubdivisions = 0;
                [result placeObject:e1];
                [result placeObject:e2];
            }
                break;
                
            case 10:
            {
                HarmonyWizMusicEvent *e1 = [event copy];
                e1.durationBeats = 8;
                e1.durationSubdivisions = 0;
                HarmonyWizMusicEvent *e2 = [event copy];
                e2.beat += 8;
                e2.durationBeats = 2;
                e2.durationSubdivisions = 0;
                [result placeObject:e1];
                [result placeObject:e2];
            }
                break;
                
            case 11:
            {
                HarmonyWizMusicEvent *e1 = [event copy];
                e1.durationBeats = 8;
                e1.durationSubdivisions = 0;
                HarmonyWizMusicEvent *e2 = [event copy];
                e2.beat += 8;
                e2.durationBeats = 3;
                e2.durationSubdivisions = 0;
                [result placeObject:e1];
                [result placeObject:e2];
            }
                break;
                
            case 13:
            {
                HarmonyWizMusicEvent *e1 = [event copy];
                e1.durationBeats = 8;
                e1.durationSubdivisions = 0;
                HarmonyWizMusicEvent *e2 = [event copy];
                e2.beat += 8;
                e2.durationBeats = 3;
                e2.durationSubdivisions = 0;
                HarmonyWizMusicEvent *e3 = [event copy];
                e3.beat += 11;
                e3.durationBeats = 2;
                e3.durationSubdivisions = 0;
                [result placeObject:e1];
                [result placeObject:e2];
                [result placeObject:e3];
            }
                break;
                
            default:
                [result placeObject:event];
                break;
        }
    }
    else {
        [result placeObject:event];
    }
    return [NSArray arrayWithArray:result];
}

- (NSArray*)splitEvent:(HarmonyWizMusicEvent*)event {
    
    NSAssert(event.durationSubdivisions != 0 || event.durationBeats != 0, @"Zero length event");
    
    // split across bar lines
    NSArray *mainSplitEvents = [self splitEventAcrossBarlines:event];
    
    // split notes with beat lengths that can't be handled
    NSMutableArray *lengthSplitEvents = [NSMutableArray arrayWithCapacity:mainSplitEvents.count];
    for (HarmonyWizMusicEvent *event in mainSplitEvents)
        [lengthSplitEvents placeObjectsFromArray:[self splitEventWithOddBeats:event]];
    
    // split notes across beats if there is no other way to handle them
    NSMutableArray *beatSplitEvents = [NSMutableArray arrayWithCapacity:lengthSplitEvents.count];
    for (HarmonyWizMusicEvent *event in lengthSplitEvents) {
        const NoteType noteType = [self noteTypeFromMusicEvent:event];
        if (noteType.noteHead == NoteType::kNote_Unsupported) {
            [beatSplitEvents placeObjectsFromArray:[self splitEventAcrossBeats:event withGradation:self.beatSubdivisions]];
        }
        else {
            [beatSplitEvents placeObject:event];
        }
    }
    
    return beatSplitEvents;
}

- (NSArray*)recursivelySplitEvent:(HarmonyWizMusicEvent*)event maxDepth:(NSUInteger)depth {
    if (depth == 0) {
        [NSException raise:@"HarmonyWiz" format:@"Maximum recursion depth exceeded"];
    }
    NSArray *result = [self splitEvent:event];
    if (result.count > 1) {
        NSMutableArray *subs = [NSMutableArray arrayWithCapacity:result.count];
        for (HarmonyWizMusicEvent *subEvent in result)
            [subs placeObjectsFromArray:[self recursivelySplitEvent:subEvent maxDepth:depth-1]];
        result = [NSArray arrayWithArray:subs];
    }
    else {
        //        NSLog(@"recursion depth remaining is %u", depth);
    }
    return result;
}

- (NSArray*)recursivelySplitEvent:(HarmonyWizMusicEvent*)event {
    if (event.durationBeats == 0) {
        ;
    }
    return [self recursivelySplitEvent:event maxDepth:8];
}

@end
