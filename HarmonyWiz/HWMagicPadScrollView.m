//
//  HWMagicPadScrollView.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWMagicPadScrollView.h"
#import "HWCollectionViewController.h"
#import "HWDocument.h"
#import "HarmonyWizTrack.h"
#import "HarmonyWizSong+AutoColour.h"
#import "HarmonyWizSong+Splice.h"
#import "NSMutableArray+Sorted.h"
#import "PPSparkleEmitterView.h"
#import "HWStar.h"
#import "HWEvent.h"
#import "HWPainter.h"
#import "HWMagicPadScrollView+EventFromPoint.h"
#import "PPStarsEmitterView.h"
#import "HWAppDelegate.h"
#import "UIView+Recursive/UIView+Recursive.h"
#import "HarmonyWizPiano.h"

#include "divmod.h"

NSString *const HWPaintedNotification = @"HWPaintedNotification";
NSTimeInterval const kStarTravelDuration = 1;
NSTimeInterval const kStarFadeOutDuration = 1;
NSTimeInterval const kStarTravelDelay = 0.05;
UInt32 const kMaxPainterTotalArea = 500 * 1024 * 2;
NSUInteger const kMaxNumberOfShapeViews = 64;
NSTimeInterval const HWAutoScrollDuration = 2;
const CGFloat kMinimumShapeViewArea = 400.f;

extern NSUInteger const HWMaxIterationsToBrushEnd;
extern CGFloat const HWTracksViewClosedWidth;
extern CGFloat const HWTracksViewOpenedWidth;
extern CGFloat const HWScreenWidth;
extern CGFloat const HWScreenHeight;
extern CGFloat const HWPanelWidth;
//extern NSInteger const HWScrollButtons;

extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;

extern struct HWPoint HWPointMake(float x, float y);

// checks if (x0,x1) intersects (y0,y1)
static char intervalsIntersect(float x0, float x1, float y0, float y1) {
    return (MAX(x0,y0) < MIN(x1,y1));
}

@interface HWShapeViewInfo : NSObject <NSCoding>
@property (nonatomic,assign) CGRect frame;
@property (nonatomic,strong) HWPainter *painter;
@end

@implementation HWShapeViewInfo
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.frame = [aDecoder decodeCGRectForKey:@"frame"];
        self.painter = [aDecoder decodeObjectForKey:@"painter"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeCGRect:self.frame forKey:@"frame"];
    [aCoder encodeObject:self.painter forKey:@"painter"];
}
@end

@interface HWShapeView : UIView
@property (nonatomic,strong) HWPainter *painter;
@property (nonatomic,assign) CGRect drawnRect;
@property (atomic,assign) NSUInteger iterationsToEndBrush;
@property (nonatomic,weak) NSArray *allShapeViews;
@end
@implementation HWShapeView
- (id)initWithInfo:(HWShapeViewInfo *)info {
    if (self = [super initWithFrame:info.frame]) {
        self.clipsToBounds = YES;
        self.userInteractionEnabled = NO;
        self.contentMode = UIViewContentModeScaleToFill;
        self.opaque = YES;
        self.backgroundColor = [UIColor whiteColor];
        self.painter = info.painter;
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextClipToRect(context, rect);
    
    if (self.painter.offscreenCtx == nil) {
        [[UIColor whiteColor] setFill];
        [[UIBezierPath bezierPathWithRect:CGRectInset(self.bounds, -4, -4)] fill];
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, -self.frame.origin.x, -self.frame.origin.y);
//        [self.painter drawInRect:CGRectOffset(rect, self.frame.origin.x, self.frame.origin.y)];
        [self.painter draw];
        CGContextRestoreGState(context);
        
        // paint out regions covered by higher up views
        BOOL foundSelf = NO;
        for (HWShapeView *sv in self.allShapeViews) {
            if (sv == self) {
                foundSelf = YES;
            }
            else if (foundSelf && intervalsIntersect(CGRectGetMinX(sv.frame),CGRectGetMaxX(sv.frame),CGRectGetMinX(self.frame),CGRectGetMaxX(self.frame))) {
                CGRect whiteRect = CGRectMake(MAX(CGRectGetMinX(sv.frame),CGRectGetMinX(self.frame)), 0, MIN(CGRectGetMaxX(sv.frame),CGRectGetMaxX(self.frame)) - MAX(CGRectGetMinX(sv.frame),CGRectGetMinX(self.frame)), self.bounds.size.height);
                whiteRect.origin.x -= self.frame.origin.x;
                [[UIColor whiteColor] setFill];
                [[UIBezierPath bezierPathWithRect:whiteRect] fill];
            }
        }
    }
    else {
        CGRect r = self.painter.bounds;
        r.origin.y = 0;
        r.size.height = self.bounds.size.height;
        [[UIColor whiteColor] setFill];
        [[UIBezierPath bezierPathWithRect:CGRectOffset(r, -self.painter.contentOffset.x, -self.painter.contentOffset.y)] fill];
        CGImageRef cgImage = CGBitmapContextCreateImage(self.painter.offscreenCtx);
        CGContextDrawImage(context, CGRectMake(self.painter.contentOffset.x-self.frame.origin.x, self.painter.contentOffset.y-self.frame.origin.y, self.painter.offscreenSize.width, self.painter.offscreenSize.height), cgImage);
        CGImageRelease(cgImage);
    }
}
@end

@interface HWMagicPadScrollView ()
@property (nonatomic,strong) NSMutableArray *notePoints;
@property (nonatomic,strong) UIImageView *paintHere;
@property (nonatomic,strong) PPStarsEmitterView *starsEmitter;
@property (nonatomic,strong) PPSparkleEmitterView *sparkles;
@property (nonatomic,strong) NSMutableArray *stars, *shapeViews;
@property (nonatomic,strong) HWShapeView *currentShapeView;
@property (nonatomic,strong) CADisplayLink *brushDLink;
@property (nonatomic,assign) enum ShouldScroll {kShouldScroll_None, kShouldScroll_Right, kShouldScroll_Left} shouldScroll;
@end

CGFloat const kStarRadius = 15;

@implementation HWMagicPadScrollView
{
    CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
    NSTimeInterval times[5];
    uint ctr;
    
    dispatch_queue_t _drawingQueue;
}

- (void)setContentSize:(CGSize)contentSize {
    if (isfinite(contentSize.width) && isfinite(contentSize.height)) {
        self.contentView.frame = CGRectMake(0, 0, contentSize.width, contentSize.height);
        [super setContentSize:contentSize];
    }
}

- (BOOL)paintHereUsesFlatWithStars {
    return NO;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView = [[HWViewKeepingTop alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:self.contentView];
        self.contentSize = CGSizeMake(frame.size.width, frame.size.height);
        
        self.backgroundColor = [UIColor whiteColor];
        self.opaque = YES;
        
        if ([self paintHereUsesFlatWithStars]) {
            self.paintHere = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/PaintHere"]];
            self.paintHere.contentMode = UIViewContentModeCenter;
            
            self.starsEmitter = [[PPStarsEmitterView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
            self.starsEmitter.backgroundColor = nil;
            self.starsEmitter.opaque = NO;
            self.starsEmitter.clipsToBounds = YES;
            [self.starsEmitter setup];
            self.starsEmitter.translatesAutoresizingMaskIntoConstraints = NO;
            [self.contentView addSubview:self.starsEmitter];
        }
        else {
            self.paintHere = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/PaintHere_3DStyle"]];
            self.paintHere.contentMode = UIViewContentModeScaleAspectFill;
        }
        
        self.paintHere.clipsToBounds = YES;
        self.paintHere.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.paintHere];
        
        self.sparkles = [[PPSparkleEmitterView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.sparkles.backgroundColor = nil;
        self.sparkles.opaque = NO;
        self.sparkles.clipsToBounds = YES;
        [self.sparkles setup];
        self.sparkles.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.sparkles];
        
        self.contentView.viewsOnTop = @[self.paintHere,self.sparkles];
        if (self.starsEmitter) {
            self.contentView.viewsOnTop = [self.contentView.viewsOnTop arrayByAddingObject:self.starsEmitter];
        }
        
        self.shapeViews = [NSMutableArray array];
        
        _drawingQueue = dispatch_queue_create("com.wizdom-music.harmonywiz.drawingQueue", NULL);
        
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        [dnc addObserver:self selector:@selector(undoManagerDidUndo:) name:NSUndoManagerDidUndoChangeNotification object:doc.hwUndoManager];
        [dnc addObserver:self selector:@selector(undoManagerDidRedo:) name:NSUndoManagerDidRedoChangeNotification object:doc.hwUndoManager];
        [dnc addObserver:self selector:@selector(undoManagerDidRevert:) name:HarmonyWizRevertUndoNotification object:doc.hwUndoManager];
    }
    return self;
}

- (void)dealloc {
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    [dnc removeObserver:self];
    
    if (self.brushDLink) {
        [self.brushDLink invalidate];
        self.brushDLink = nil;
    }
}

- (void)removeShapeViews {
    [self.shapeViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.shapeViews removeAllObjects];
}

- (void)updateShapeViewsFromSong {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [self removeShapeViews];
    NSArray *shapeViewInfos = [NSMutableArray arrayWithArray:[doc.song.misc objectForKey:@"shapeViews"]];
    for (HWShapeViewInfo *info in shapeViewInfos) {
        HWShapeView *sv = [[HWShapeView alloc] initWithInfo:info];
        [self.shapeViews addObject:sv];
        sv.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:sv];
        sv.allShapeViews = self.shapeViews;
    }
    [self updateLabel];
}

- (void)undoManagerDidUndo:(NSNotification*)notif {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const BOOL magicMode = doc.metadata.magicMode;
    
    if (magicMode) {
        if ([doc.hwUndoManager.lastActionName isEqualToString:@"Paint notes"] ||
            [doc.hwUndoManager.lastActionName isEqualToString:@"Clear Song"] ||
            [doc.hwUndoManager.lastActionName isEqualToString:@"Clear Magic Pad"]) {
            [self updateShapeViewsFromSong];
        }
        [self updateLabel];
    }
}

- (void)undoManagerDidRedo:(NSNotification*)notif {
    [self undoManagerDidUndo:notif];
}

- (void)undoManagerDidRevert:(NSNotification*)notif {
    // ?
}

- (void)updateLabel {
    if ([self paintHereUsesFlatWithStars]) {
        self.paintHere.center = CGPointMake(self.bounds.size.width*0.5f + self.contentOffset.x, self.bounds.size.height*0.5f + self.contentOffset.y);
        self.starsEmitter.frame = CGRectMake(self.contentOffset.x, self.contentOffset.y, self.bounds.size.width, self.bounds.size.height);
    }
    else {
        self.paintHere.frame = CGRectMake(self.contentOffset.x, self.contentOffset.y, self.bounds.size.width, self.bounds.size.height);
    }
    
    if (self.paintHere.layer.animationKeys == nil) {
        self.paintHere.alpha = 1.f;
        self.paintHere.hidden = (self.currentShapeView != nil || self.shapeViews.count > 0);
        
        self.starsEmitter.hidden = self.paintHere.hidden;
        if (self.starsEmitter.hidden || ![HWCollectionViewController currentlyOpenDocument].metadata.magicMode) {
            [self.starsEmitter pause];
        }
        else {
            [self.starsEmitter resume];
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updateLabel];
    
    for (HWShapeView *shapeView in self.shapeViews) {
        CGRect frame = shapeView.frame;
        if (CGRectGetMinY(frame) < 0)
            frame.origin.y = 0;
        if (CGRectGetMaxY(frame) > self.bounds.size.height)
            frame.size.height = self.bounds.size.height - frame.origin.y;
        shapeView.frame = frame;
    }
    
    self.sparkles.frame = CGRectMake(self.contentOffset.x, self.contentOffset.y, self.bounds.size.width, self.bounds.size.height);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        UITouch *touch = touches.anyObject;
        
        if (self.brushDLink) {
            return;
        }
        
        self.currentShapeView = [[HWShapeView alloc] initWithFrame:CGRectMake(self.contentOffset.x, 0, self.bounds.size.width, self.bounds.size.height)];
        self.currentShapeView.allShapeViews = self.shapeViews;
        self.currentShapeView.backgroundColor = [UIColor clearColor];
        self.currentShapeView.opaque = NO;
        self.currentShapeView.clipsToBounds = YES;
        self.currentShapeView.userInteractionEnabled = NO;
        self.currentShapeView.contentMode = UIViewContentModeScaleToFill;
//        self.currentShapeView.layer.borderWidth = 2;
//        self.currentShapeView.layer.borderColor = [UIColor blackColor].CGColor;
        self.currentShapeView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.currentShapeView];
        
        CGPoint loc = [touch locationInView:self];
        
        self.currentShapeView.painter = [[HWPainter alloc] initWithOffscreenBuffer:YES contentOffset:self.contentOffset size:CGSizeMake(self.bounds.size.width, self.bounds.size.height)];
        [self.currentShapeView.painter startWithPoint:HWPointMake(loc.x, loc.y)];
        self.currentShapeView.drawnRect = CGRectNull;
        
        self.notePoints = [NSMutableArray arrayWithCapacity:1];
        
        // initialise brush points array
        ctr = 0;
        pts[0] = loc;
        times[0] = touch.timestamp;
        
        [self.sparkles resume];
        ((CAEmitterLayer*)self.sparkles.layer).emitterPosition = CGPointMake(pts[0].x - self.contentOffset.x, pts[0].y - self.contentOffset.y);
        
        if (self.stars) {
            [self.stars makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }
        
        //    [self updateLabel];
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:0.6 animations:^{
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.paintHere.alpha = 0.f;
        } completion:^(BOOL finished){
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf updateLabel];
        }];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        UITouch *touch = touches.anyObject;
        
        CGPoint loc = [touch locationInView:self];
        
        // render to offscreen buffer asynchronously
        dispatch_async(_drawingQueue, ^(void) {
            @autoreleasepool {
                const BOOL redraw = [self.currentShapeView.painter appendPoint:HWPointMake(loc.x, loc.y)];
                if (redraw) {
                    const CGRect redrawRect = self.currentShapeView.painter.redrawRect;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        CGRect r = CGRectOffset(redrawRect, -self.currentShapeView.frame.origin.x, -self.currentShapeView.frame.origin.y);
                        if (! CGRectContainsRect(self.currentShapeView.drawnRect, r)) {
                            r.origin.y = 0;
                            r.size.height = self.currentShapeView.frame.size.height;
                            self.currentShapeView.drawnRect = CGRectUnion(self.currentShapeView.drawnRect, r);
                        }
                        [self.currentShapeView setNeedsDisplayInRect:r];
                    });
                }
            }
        });
        
//        NSLog(@"frame is now %@", NSStringFromCGRect(weakSelf.currentShapeView.frame));
        
        ((CAEmitterLayer*)self.sparkles.layer).emitterPosition = CGPointMake(loc.x - self.contentOffset.x, loc.y - self.contentOffset.y);
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        // synchronize
        dispatch_sync(_drawingQueue, ^(void) {
            @autoreleasepool {
                [self.currentShapeView.painter killOffscreenBuffer];
                [self.currentShapeView removeFromSuperview];
                self.currentShapeView = nil;
            }
        });
        [self updateLabel];
        ctr = 0;
        
        [self.sparkles pause];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        UITouch *touch = touches.anyObject;
        CGPoint pt = [touch locationInView:self];
        
        // pause sparkles
        ((CAEmitterLayer*)self.sparkles.layer).emitterPosition = CGPointMake(pt.x - self.contentOffset.x, pt.y - self.contentOffset.y);
        [self.sparkles pause];
        
        self.currentShapeView.iterationsToEndBrush = 0;
        
        // render to offscreen buffer asynchronously
        dispatch_async(_drawingQueue, ^(void) {
            @autoreleasepool {
                BOOL redraw = [self.currentShapeView.painter startEndWithPoint:HWPointMake(pt.x, pt.y)];
                self.brushDLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(endBrush:)];
                [self.brushDLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
                if (redraw) {
                    const CGRect redrawRect = self.currentShapeView.painter.redrawRect;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        CGRect r = CGRectOffset(redrawRect, -self.currentShapeView.frame.origin.x, -self.currentShapeView.frame.origin.y);
                        if (! CGRectContainsRect(self.currentShapeView.drawnRect, r)) {
                            r.origin.y = 0;
                            r.size.height = self.currentShapeView.frame.size.height;
                            self.currentShapeView.drawnRect = CGRectUnion(self.currentShapeView.drawnRect, r);
                        }
                        [self.currentShapeView setNeedsDisplayInRect:r];
                    });
                }
            }
        });
        
        // auto scroll if touch ends in right or left panels
        {
            const CGPoint rightPanelPoint = [touch locationInView:self.mainView.rightPanel];
            const CGPoint leftPanelPoint = [touch locationInView:self.mainView.leftPanel];
            if (rightPanelPoint.x >= CGRectGetMinX(self.mainView.rightPanel.bounds)) {
                self.shouldScroll = kShouldScroll_Right;
            }
            else if (leftPanelPoint.x <= CGRectGetMaxX(self.mainView.leftPanel.bounds)) {
                self.shouldScroll = kShouldScroll_Left;
            }
            else {
                self.shouldScroll = kShouldScroll_None;
            }
        }
    }
}

- (void)clearAll {
    if (self.stars) {
        [self.stars makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.stars removeAllObjects];
    }
    if (self.shapeViews) {
        [self.shapeViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.shapeViews removeAllObjects];
    }
    if (self.currentShapeView) {
        [self.currentShapeView removeFromSuperview];
        self.currentShapeView = nil;
    }
    
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[HWShapeView class]]) {
            NSLog(@"Removing a stray shape view");
            [view removeFromSuperview];
        }
    }
    
    [self updateLabel];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [doc.song.misc setObject:[self shapeViewInfos] forKey:@"shapeViews"];
}

- (NSArray*)shapeViewInfos {
    NSMutableArray *result = [NSMutableArray array];
    for (HWShapeView *sv in self.shapeViews) {
        HWShapeViewInfo *info = [HWShapeViewInfo new];
        info.frame = sv.frame;
        info.painter = sv.painter;
        [result addObject:info];
    }
    return result;
}

- (UInt32)totalPainterArea {
    UInt32 result = 0;
    for (HWShapeView *shapeView in self.shapeViews) {
        result += (shapeView.bounds.size.width * shapeView.bounds.size.height);
    }
    return result;
}

- (void)shapeViewFinalise:(HWShapeView*)shapeView {
    
    shapeView.backgroundColor = self.backgroundColor;
    shapeView.opaque = YES;
    
    // adjust frame size
    {
        CGRect frame = CGRectInset(shapeView.painter.bounds, 0.f, 0.f);
        if (CGRectGetMinY(frame) < 0)
            frame.origin.y = 0;
        if (CGRectGetMaxY(frame) > self.bounds.size.height)
            frame.size.height = self.bounds.size.height - frame.origin.y;
//        frame.origin.y = 0.f;
//        frame.size.height = self.contentSize.height;
        shapeView.frame = frame;
    }
    
//    [shapeView.painter.midPoints addObject:nil];
    
    // generate notes
    if (shapeView.painter.midPoints.count > 0) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        [doc.hwUndoManager startUndoableChanges];
        [self generateNotes];
        
        const CGFloat area = CGRectGetWidth(shapeView.frame)*CGRectGetHeight(shapeView.frame);
        if (self.notePoints.count > 0 && area > kMinimumShapeViewArea) {
            {
                // remove views covered by new one (optimisation)
                NSMutableArray *toRemove = [NSMutableArray array];
                for (HWShapeView *view in self.shapeViews) {
                    if (CGRectContainsRect(shapeView.frame, view.frame)) {
                        [toRemove addObject:view];
                    }
                }
                [toRemove makeObjectsPerformSelector:@selector(removeFromSuperview)];
                [self.shapeViews removeObjectsInArray:toRemove];
            }
            
            // redraw overlapping shape views
            for (HWShapeView *sv in self.shapeViews) {
                if (intervalsIntersect(CGRectGetMinX(sv.frame),CGRectGetMaxX(sv.frame),CGRectGetMinX(shapeView.frame),CGRectGetMaxX(shapeView.frame))) {
                    [sv setNeedsDisplay];
                }
            }
            
            // add new view and if count is over limit then remove oldest one (optimisation)
            [self.shapeViews addObject:shapeView];
            [shapeView setNeedsDisplay];
            {
                // fade out and remove earlier views
                NSMutableArray *removing = [NSMutableArray array];
                if (self.shapeViews.count > kMaxNumberOfShapeViews) {
                    const NSUInteger removeCount = self.shapeViews.count - kMaxNumberOfShapeViews;
                    [removing addObjectsFromArray:[self.shapeViews subarrayWithRange:NSMakeRange(0, removeCount)]];
                    [self.shapeViews removeObjectsInRange:NSMakeRange(0, removeCount)];
                }
                while ([self totalPainterArea] > kMaxPainterTotalArea) {
                    UIView *fadeView = [self.shapeViews objectAtIndex:0];
                    [self.shapeViews removeObject:fadeView];
                    [removing addObject:fadeView];
                }
                if (removing.count > 0) {
                    [UIView animateWithDuration:0.5 animations:^{
                        for (UIView *fadeView in removing) {
                            [fadeView setAlpha:0.f];
                        }
                    } completion:^(BOOL finished){
                        [removing makeObjectsPerformSelector:@selector(removeFromSuperview)];
                    }];
                }
            }
        }
        else {
            [shapeView removeFromSuperview];
        }
        [doc.song.misc setObject:[self shapeViewInfos] forKey:@"shapeViews"];
        [doc.hwUndoManager finishUndoableChangesName:@"Paint notes"];
        shapeView = nil;
        [self updateLabel];
    }
    
    // make all the stave events invisible so we can fade them in
    NSArray *staveEvents = [self.staveView staveEventsFromMusicEvents:[self.notePoints valueForKey:@"note"]];
    for (HWEvent *event in staveEvents) {
        [event setAlpha:0.f];
    }
    
    self.stars = [NSMutableArray arrayWithCapacity:self.notePoints.count];
    NSTimeInterval delay = 0;
    for (NSDictionary *dic in self.notePoints) {
        CGPoint pt = [[dic objectForKey:@"point"] CGPointValue];
        HarmonyWizNoteEvent *ne = [dic objectForKey:@"note"];
        if (ne != nil) {
            CGPoint noteHeadLoc = [self.staveView noteHeadLoc:ne];
            noteHeadLoc.x += self.rulerControl.xOffset; // NB: weird bug. Why do I need to add this? The noteHeadLoc value seems to be slightly to the left.
            noteHeadLoc = [self convertPoint:noteHeadLoc fromView:self.staveView.parentScrollView.contentView];
            HWStar *star = [[HWStar alloc] initWithFrame:CGRectMake(pt.x-kStarRadius, pt.y-kStarRadius, kStarRadius*2, kStarRadius*2)];
            star.userInteractionEnabled = NO;
            star.destination = noteHeadLoc;
            [self.stars addObject:star];
            star.translatesAutoresizingMaskIntoConstraints = NO;
            [self.contentView addSubview:star];
            
            // Move star up to note. Then fade out star and fade in note simultaneously.
            __weak typeof(self) weakSelf = self;
            [UIView animateWithDuration:kStarTravelDuration delay:0.1+delay options:0 animations:^{
                star.center = star.destination;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:kStarFadeOutDuration delay:0 options:0 animations:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    star.alpha = 0.f;
                    for (HWEvent *event in [strongSelf.staveView staveEventsFromMusicEvents:@[ne]]) {
                        [event setAlpha:1.f];
                    }
                } completion:nil];
            }];
            
            delay += kStarTravelDelay;
        }
    }
}

- (void)endBrush:(CADisplayLink*)dlink {
    HWShapeView *shapeView = self.currentShapeView;
    
    if (shapeView.superview == nil) {
        [dlink invalidate];
        self.brushDLink = nil;
        return;
    }
    
    if (shapeView.iterationsToEndBrush < HWMaxIterationsToBrushEnd) {
        // render to offscreen buffer asynchronously
        dispatch_async(_drawingQueue, ^(void) {
            @autoreleasepool {
                const BOOL redraw = [shapeView.painter continueEnd];
                if (redraw) {
                    shapeView.iterationsToEndBrush ++;
                    const CGRect redrawRect = shapeView.painter.redrawRect;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        CGRect r = CGRectOffset(redrawRect, -shapeView.frame.origin.x, -shapeView.frame.origin.y);
                        if (! CGRectContainsRect(shapeView.drawnRect, r)) {
                            r.origin.y = 0;
                            r.size.height = shapeView.frame.size.height;
                            shapeView.drawnRect = CGRectUnion(shapeView.drawnRect, r);
                        }
                        [shapeView setNeedsDisplayInRect:r];
                    });
                }
            }
        });
    }
    else {
        [dlink invalidate];
        self.brushDLink = nil;
        [shapeView.painter killOffscreenBuffer];
        [self shapeViewFinalise:shapeView];
        self.currentShapeView = nil;
    }
}

- (void)scrollOnePageRightWithDuration:(NSTimeInterval)duration {
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration animations:^{
        __strong typeof(self) strongSelf = weakSelf;
        CGPoint pt = strongSelf.mainView.stavesView.contentOffset;
        pt.x += (HWScreenWidth - HWPanelWidth * 2.f);
        if (pt.x > strongSelf.mainView.stavesView.contentSize.width - HWScreenWidth) {
            pt.x = strongSelf.mainView.stavesView.contentSize.width - HWScreenWidth;
        }
        [strongSelf.mainView.stavesView setContentOffset:pt];
    }];
}

- (void)scrollOnePageLeftWithDuration:(NSTimeInterval)duration {
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration animations:^{
        __strong typeof(self) strongSelf = weakSelf;
        CGPoint pt = strongSelf.mainView.stavesView.contentOffset;
        pt.x -= (HWScreenWidth - HWPanelWidth * 2.f);
        if (pt.x < 0) {
            pt.x = 0.f;
        }
        [strongSelf.mainView.stavesView setContentOffset:pt];
    }];
}

- (void)generateNotes {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    HarmonyWizMusicTrack *track = [doc.song.musicTracks objectAtIndex:doc.metadata.currentlySelectedTrackNumber];
    
//    CGPoint firstNotePoint = [self.currentShapeView.painter.midPoints.firstObject CGPointValue];
//    CGPoint lastNotePoint = [self.currentShapeView.painter.midPoints.lastObject CGPointValue];
    CGPoint pt0 = self.currentShapeView.frame.origin;
    CGPoint pt1 = pt0;
    pt1.x += CGRectGetWidth(self.currentShapeView.frame);
    
    pt0 = [self.rulerControl convertPoint:pt0 fromView:self];
    pt1 = [self.rulerControl convertPoint:pt1 fromView:self];
    
    // use eighth note quantization for finding ruler positions
    const enum Quantization savedQuant = doc.metadata.quantization;
    doc.metadata.quantization = kQuantization_Eighth;
    const UInt16 crotchetSubdivs = doc.song.beatSubdivisions * doc.song.timeSign.lowerNumeral / 4;
    const UInt16 pos0 = MIN([self.rulerControl playbackPositionFromLocation:pt0.x] * crotchetSubdivs, doc.song.finalBarSubdivision);
    const UInt16 pos1 = MIN([self.rulerControl playbackPositionFromLocation:pt1.x] * crotchetSubdivs, doc.song.finalBarSubdivision);
    doc.metadata.quantization = savedQuant;
    
    if (pos1 < pos0) {
        return;
    }
    
    int16_t maxSubdivsPerStep;
    {
        CGPoint pt, bottomLeft, topRight;
        bottomLeft = [self.currentShapeView.painter.midPoints.firstObject CGPointValue];
        topRight = bottomLeft;
        for (NSValue *ptval in self.currentShapeView.painter.midPoints) {
            pt = ptval.CGPointValue;
            if (pt.x < bottomLeft.x)
                bottomLeft.x = pt.x;
            if (pt.x > topRight.x)
                topRight.x = pt.x;
            if (pt.y < topRight.y)
                topRight.y = pt.y;
            if (pt.y > bottomLeft.y)
                bottomLeft.y = pt.y;
        }
        HarmonyWizNoteEvent *firstNote = [self musicEventFromPoint:bottomLeft pos:pos0 dur:crotchetSubdivs*0.25f];
        HarmonyWizNoteEvent *lastNote = [self musicEventFromPoint:topRight pos:pos1 dur:crotchetSubdivs*0.25f];
        int16_t steps = [self stepsFromMusicNote:firstNote to:lastNote];
        int16_t subdivDist = [doc.song eventStartSubdivision:lastNote] - [doc.song eventStartSubdivision:firstNote];
        maxSubdivsPerStep = (steps == 0 ? crotchetSubdivs*4 : subdivDist / steps);
        if (maxSubdivsPerStep >= crotchetSubdivs*4)         maxSubdivsPerStep = crotchetSubdivs*4;
        else if (maxSubdivsPerStep >= crotchetSubdivs*3)    maxSubdivsPerStep = crotchetSubdivs*3;
        else if (maxSubdivsPerStep >= crotchetSubdivs*2)    maxSubdivsPerStep = crotchetSubdivs*2;
        else if (maxSubdivsPerStep >= crotchetSubdivs*1.5f) maxSubdivsPerStep = crotchetSubdivs*1.5f;
        else if (maxSubdivsPerStep >= crotchetSubdivs)      maxSubdivsPerStep = crotchetSubdivs;
        else  maxSubdivsPerStep = crotchetSubdivs*0.5f;
//        else                                                maxSubdivsPerStep = crotchetSubdivs*0.25f;
    }
    
    NSMutableArray *notes = [NSMutableArray array];
    
    // total path time
//    const float totalTime = MAX([self.brushTimestamps.lastObject floatValue] - [[self.brushTimestamps objectAtIndex:0] floatValue], 0.01f);
    
    // total path length
    CGFloat totalLength = 0.f;
    for (NSUInteger idx = 2; idx < self.currentShapeView.painter.midPoints.count; idx += 2) {
        totalLength += bezierArcLength([[self.currentShapeView.painter.midPoints objectAtIndex:idx-2] CGPointValue],
                                  [[self.currentShapeView.painter.midPoints objectAtIndex:idx-1] CGPointValue],
                                  [[self.currentShapeView.painter.midPoints objectAtIndex:idx] CGPointValue]);
    }
    
    const UInt16 maxNoteCount = (pos1 - pos0) / (crotchetSubdivs * 0.25f);
    
    UInt16 pos = pos0;
    
    CGFloat visualNoteLength = totalLength/maxNoteCount * 2;
    
    // create notes
    if (maxNoteCount > 1) {
        for (UInt16 curNote = 0; curNote < maxNoteCount-1; curNote++) {
//            const CGFloat visualNoteLength = totalLength / (maxNoteCount-1) * curNote;
            
            if (pos > pos1 || visualNoteLength > totalLength) {
                break;
            }
            
            CGFloat currentLength = 0.f;
            for (NSUInteger brushIdx = 2; brushIdx < self.currentShapeView.painter.midPoints.count; brushIdx += 2) {
                CGPoint p1 = [[self.currentShapeView.painter.midPoints objectAtIndex:brushIdx-2] CGPointValue];
                CGPoint p2 = [[self.currentShapeView.painter.midPoints objectAtIndex:brushIdx-1] CGPointValue];
                CGPoint p3 = [[self.currentShapeView.painter.midPoints objectAtIndex:brushIdx-0] CGPointValue];
                
                CGPoint a1 = [[self.currentShapeView.painter.midSpeeds objectAtIndex:brushIdx-2] CGPointValue];
                CGPoint a2 = [[self.currentShapeView.painter.midSpeeds objectAtIndex:brushIdx-1] CGPointValue];
                CGPoint a3 = [[self.currentShapeView.painter.midSpeeds objectAtIndex:brushIdx-0] CGPointValue];
                
                CGPoint point0 = p1;
                for (CGFloat t = 0.0; t <= 1.00001; t += 0.05) {
                    CGPoint point = CGPointMake(bezierQudraticInterpolation(t, p1.x, p2.x, p3.x), bezierQudraticInterpolation(t, p1.y, p2.y, p3.y));
                    
                    const CGFloat dist = distance(point0, point);
                    currentLength += dist;
                    if (currentLength > visualNoteLength) {
                        CGPoint velocity = CGPointMake(bezierQudraticInterpolation(t, a1.x, a2.x, a3.x), bezierQudraticInterpolation(t, a1.y, a2.y, a3.y));
                        const CGFloat speed = sqrtf(velocity.x*velocity.x + velocity.y*velocity.y);
                        float noteSpeedFactor;
                        if (speed > 20)         noteSpeedFactor = 0.25f;
                        else if (speed > 10)    noteSpeedFactor = 0.5f;
                        else if (speed > 5)     noteSpeedFactor = 1.f;
                        else if (speed > 3.33)  noteSpeedFactor = 1.5f;
                        else if (speed > 2.5)   noteSpeedFactor = 2.f;
                        else if (speed > 1.66)  noteSpeedFactor = 3.f;
                        else                    noteSpeedFactor = 4.f;
//                        printf("speed %f factor %f\n", speed, noteSpeedFactor);
                        UInt16 dur = MIN((crotchetSubdivs * noteSpeedFactor), maxSubdivsPerStep);
                        
                        // truncate note length if it goes past right boundary
                        if (pos+dur > pos1 + doc.song.beatSubdivisions) {
                            dur = MAX(pos1 + doc.song.beatSubdivisions - pos, crotchetSubdivs*0.25f);
                        }
                        
                        float lambda = (visualNoteLength - currentLength + dist) / dist;
                        CGPoint notePt = plus(scalarMultiply(lambda, point), scalarMultiply(1 - lambda, point0));
                        HarmonyWizMusicEvent *ev = [self musicEventFromPoint:notePt pos:pos dur:dur];
                        pos += dur;
                        visualNoteLength += (totalLength / (maxNoteCount-1))*dur/(crotchetSubdivs*0.25f);
                        if (ev != nil) {
                            [self.notePoints addObject:@{@"point" : [NSValue valueWithCGPoint:notePt], @"note" : ev}.mutableCopy];
                            [notes addObject:ev];
                        }
                        goto end_loop;
                    }
                    point0 = point;
                }
            }
        end_loop: ;
        }
    }
    
    // make sure at least one note is created
    if (notes.count == 0) {
        CGPoint notePt = [self.currentShapeView.painter.midPoints.firstObject CGPointValue];
        HarmonyWizNoteEvent *ev = [self musicEventFromPoint:notePt pos:pos dur:doc.song.beatSubdivisions/4];
        if (ev) {
            if (doc.song.isModal) {
                const int shift = [[doc.song.songMode modeAccidentalTypes][imod(ev.noteValue,12)] intValue];
                [ev applyNoteShift:shift];
            }
            [self.notePoints addObject:@{@"point" : [NSValue valueWithCGPoint:notePt], @"note" : ev}.mutableCopy];
            [notes addObject:ev];
        }
    }
    
    if (notes.count >= 2) {
        // auto colour
        [doc.song autoColour:notes withCadence:NO];
    }
    
    // handle long passages without harmony changes
    const UInt16 maxSubdivsWithoutHarmonyChange = crotchetSubdivs*2;
    {
        for (short idx = 0; idx < notes.count; idx++) {
            HarmonyWizNoteEvent *ne0 = [notes objectAtIndex:idx];
            if (ne0.type == kNoteEventType_Harmony) {
                for (short jdx = idx+1; jdx < notes.count; jdx++) {
                    HarmonyWizNoteEvent *ne1 = [notes objectAtIndex:jdx];
                    if (ne1.type == kNoteEventType_Harmony) {
                        idx = jdx-1;
                        break;
                    }
                    else {
                        const UInt16 dist = [doc.song eventStartSubdivision:ne1] - [doc.song eventStartSubdivision:ne0];
                        if (dist >= maxSubdivsWithoutHarmonyChange) {
                            ne1.type = kNoteEventType_Harmony;
                            idx = jdx-1;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    // handle large leaps
    {
        const int kMaxLeap = 5;
        for (short idx = 0; idx < notes.count; idx++) {
            HarmonyWizNoteEvent *ne0 = [notes objectAtIndex:idx];
            if (ne0.type == kNoteEventType_Harmony) {
                for (short jdx = idx+1; jdx < notes.count; jdx++) {
                    HarmonyWizNoteEvent *ne1 = [notes objectAtIndex:jdx];
                    if (ne1.type == kNoteEventType_Harmony) {
                        if (ABS(ne1.noteValue - ne0.noteValue) > kMaxLeap) {
                            if (jdx - idx > 1) {
                                const short halfWay = (jdx+idx)/2;
                                HarmonyWizNoteEvent *ne2 = [notes objectAtIndex:halfWay];
                                ne2.type = kNoteEventType_Harmony;
                                idx = halfWay-1;
                                if (idx < 0)
                                    idx = 0;
                                break;
                            }
                        }
                        else {
                            idx = jdx-1;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    BOOL paintedThroughToEndOfSong = NO;
    
    // if last note is a passing note and is in last measure of piece and after all other notes, then make it a harmony note
    HarmonyWizNoteEvent *lastNote = notes.lastObject;
    if ([doc.song barNumberFromEvent:lastNote]+1 == doc.song.finalBarNumber) {
        BOOL isLastNoteInStave = YES;
        const UInt16 lastNoteEndSubdiv = [doc.song eventEndSubdivision:lastNote];
        for (id event in self.staveView.track.events) {
            if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                if ([doc.song eventEndSubdivision:event] > lastNoteEndSubdiv) {
                    isLastNoteInStave = NO;
                    break;
                }
            }
        }
        if (isLastNoteInStave) {
            paintedThroughToEndOfSong = YES;
            
            lastNote.type = kNoteEventType_Harmony;
            
            // if there is a harmony note before it that is a sixteenth note
            // then make it a suspension note
            if (notes.count >= 2) {
                HarmonyWizNoteEvent *secondLastNote = [notes objectAtIndex:notes.count-2];
                if (secondLastNote.type == kNoteEventType_Harmony && [doc.song eventStartSubdivision:lastNote] == [doc.song eventEndSubdivision:secondLastNote] && [doc.song eventStartSubdivision:lastNote] - [doc.song eventStartSubdivision:secondLastNote] <= crotchetSubdivs/4) {
                    secondLastNote.type = kNoteEventType_Suspension;
                }
            }
        }
    }
    
    // compute final notes associated with each note point
    for (NSMutableDictionary *dic in self.notePoints) {
        HarmonyWizMusicEvent *ev = [dic objectForKey:@"note"];
        NSIndexSet *overlaps = [doc.song overlappingEvents:notes forEvent:ev];
        if (overlaps.count > 0) {
            NSAssert(overlaps.count == 1, @"multiple overlaps");
            ev = [notes objectAtIndex:overlaps.lastIndex];
            [dic setObject:ev forKey:@"note"];
        }
        else {
            [dic removeObjectForKey:@"note"];
        }
    }
    
    // non-expert mode and splice
    {
        for (id ev in notes)
            [track.events removeObjectsAtIndexes:[doc.song overlappingEvents:track.events forEvent:ev]];
        [track.events placeObjectsFromArray:notes];
        [doc.song addFillerRestsTo:track.events];
        
        // handle non-expert mode requirements
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        if (! expertMode) {
            [doc.song removeOtherEventsOverlappingWith:notes];
        }
        
        doc.metadata.harmonyScore = nil;
        [doc.song spliceHarmony];
    }
    
    if (notes.count > 0) {
        if (paintedThroughToEndOfSong) {
            doc.song.playbackPosition = 0.f;
//            [self handlePaintedThroughToLastMeasure];
        }
        else {
            doc.song.playbackPosition = (float)[doc.song eventStartSubdivision:notes.firstObject] / crotchetSubdivs;
            
            if (self.shouldScroll == kShouldScroll_Right) {
                [self scrollOnePageRightWithDuration:HWAutoScrollDuration];
            }
            else if (self.shouldScroll == kShouldScroll_Left) {
                [self scrollOnePageLeftWithDuration:HWAutoScrollDuration];
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HWPaintedNotification object:self];
    }
}

/*
- (void)handlePaintedThroughToLastMeasure {
    if (self.rulerControl.contentOffset.x > 0.5f) {
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:HWAutoScrollDuration animations:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.rulerControl setContentOffset:CGPointZero];
        }];
    }
}
*/

#pragma mark - Bezier functions

static CGFloat bezierQudraticInterpolation(CGFloat t, CGFloat a, CGFloat b, CGFloat c) {
    return (1 - t) * (1 - t) * a + 2 * (1 - t) * t * b + t * t * c;
}

static CGFloat bezierArcLength(CGPoint a, CGPoint b, CGPoint c) {
    // fairly naive algorithm for approximating arc length
    CGFloat totalLength = 0.f;
    CGPoint point0 = a;
    for (CGFloat t = 0.0f; t <= 1.00001f; t += 0.001f) {
        CGPoint point = CGPointMake(bezierQudraticInterpolation(t, a.x, b.x, c.x), bezierQudraticInterpolation(t, a.y, b.y, c.y));
        totalLength += distance(point0, point);
        point0 = point;
    }
    return totalLength;
}

#pragma mark - 2D vector operations

static CGPoint midpoint(CGPoint p, CGPoint q) {
    return CGPointMake(0.5f*(p.x+q.x), 0.5f*(p.y+q.y));
}

static CGFloat length_squared(CGPoint p, CGPoint q) {
    CGFloat dx = p.x - q.x;
    CGFloat dy = p.y - q.y;
    return (dx*dx + dy*dy);
}

static CGFloat distance(CGPoint p, CGPoint q) {
    return sqrtf(length_squared(p, q));
}

static CGPoint minus(CGPoint p, CGPoint q) {
    return CGPointMake(p.x - q.x, p.y - q.y);
}

static CGPoint plus(CGPoint p, CGPoint q) {
    return CGPointMake(p.x + q.x, p.y + q.y);
}

static CGFloat dot(CGPoint p, CGPoint q) {
    return (p.x * q.x + p.y * q.y);
}

static CGPoint scalarMultiply(CGFloat s, CGPoint p) {
    return CGPointMake(s * p.x, s * p.y);
}

// Return minimum distance between line segment vw and point p
static CGFloat minimum_distance_sqr(CGPoint v, CGPoint w, CGPoint p) {
    const CGFloat eps = 1e-6f;
    const CGFloat l2 = length_squared(v, w);  // i.e. |w-v|^2 -  avoid a sqrt
    if (l2 < eps) return length_squared(p, v);   // v == w case
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    const CGFloat t = dot(minus(p,v), minus(w,v)) / l2;
    if (t < 0.0) return length_squared(p, v);       // Beyond the 'v' end of the segment
    else if (t > 1.0) return length_squared(p, w);  // Beyond the 'w' end of the segment
    const CGPoint projection = plus(v, scalarMultiply(t, minus(w,v)));  // Projection falls on the segment
    return length_squared(p, projection);
}

@end
