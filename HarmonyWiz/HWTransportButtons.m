//
//  HWTransportButtons.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWTransportButtons.h"

extern NSString * const HWStateChangedNotification;

@interface HWTransportButtons ()
@property (nonatomic,strong) UIImageView *backgroundFrame;
@end

@implementation HWTransportButtons

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        
        self.backgroundFrame = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/frame"]];
        self.backgroundFrame.bounds = CGRectMake(0, 0, frame.size.width, frame.size.height);
        self.backgroundFrame.opaque = NO;
        self.backgroundFrame.backgroundColor = [UIColor clearColor];
        [self addSubview:self.backgroundFrame];
        
        self.leftbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftbutton.frame = CGRectMake(0, 0, frame.size.width / 3, frame.size.height);
        self.leftbutton.opaque = NO;
        self.leftbutton.backgroundColor = [UIColor clearColor];
        [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/rewind_black"] forState:UIControlStateNormal];
        [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/rewind_grey"] forState:UIControlStateHighlighted];
        [self addSubview:self.leftbutton];
        
        self.midButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.midButton.frame = CGRectMake(frame.size.width / 3, 0, frame.size.width / 3, frame.size.height);
        self.midButton.opaque = NO;
        self.midButton.backgroundColor = [UIColor clearColor];
        [self.midButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/play_black"] forState:UIControlStateNormal];
        [self.midButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/play_grey"] forState:UIControlStateHighlighted];
        [self addSubview:self.midButton];
        
        self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rightButton.frame = CGRectMake(2*frame.size.width / 3, 0, frame.size.width / 3, frame.size.height);
        self.rightButton.opaque = NO;
        self.rightButton.backgroundColor = [UIColor clearColor];
        [self.rightButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/record_red"] forState:UIControlStateNormal];
        [self.rightButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/record_grey"] forState:UIControlStateHighlighted];
        [self addSubview:self.rightButton];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stateChanged:) name:HWStateChangedNotification object:nil];
    }
    return self;
}

- (void)stateChanged:(NSNotification*)notification {
    NSNumber *num = [notification.userInfo objectForKey:@"state"];
    switch (num.intValue) {
        case 0: // stopped
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/rewind_black"] forState:UIControlStateNormal];
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/rewind_grey"] forState:UIControlStateHighlighted];
            [self.midButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/play_black"] forState:UIControlStateNormal];
            [self.rightButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/record_red"] forState:UIControlStateNormal];
            break;
        case 1: // playing
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/stop_black"] forState:UIControlStateNormal];
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/stop_grey"] forState:UIControlStateHighlighted];
            [self.midButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/play_active"] forState:UIControlStateNormal];
            [self.rightButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/record_red"] forState:UIControlStateNormal];
            break;
        case 2: // recording
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/stop_black"] forState:UIControlStateNormal];
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/stop_grey"] forState:UIControlStateHighlighted];
            [self.midButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/play_active"] forState:UIControlStateNormal];
            [self.rightButton setImage:[UIImage imageNamed:@"images/TopIcon/RewPlayRec/record_active"] forState:UIControlStateNormal];
            break;
        default:    // should not be here
            [NSException raise:@"HarmonyWiz" format:@"Unkown state"];
            break;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
