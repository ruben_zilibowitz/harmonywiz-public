//
//  HWTempoMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWTempoMenu.h"
#import "HWCollectionViewController.h"
#import "UIView+Recursive/UIView+Recursive.h"
#import "HWAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface HWTempoMenu ()
@property (nonatomic,strong) NSMutableArray *taps;
@property (nonatomic,assign) float tempoValue;
@property (nonatomic,strong) NSTimer *timer;
@end

@implementation HWTempoMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tempoValue = [HWCollectionViewController currentlyOpenDocument].song.tempo;
    self.tempoLabel.text = [NSString stringWithFormat:@"%.0f", self.tempoValue];
    [self.tapButton addTarget:self action:@selector(tapped:) forControlEvents:UIControlEventTouchDown];
    
    self.taps = [NSMutableArray array];
    
    UILongPressGestureRecognizer *longPressDown = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressDown:)];
    [self.stepDownButton addGestureRecognizer:longPressDown];
    
    UILongPressGestureRecognizer *longPressUp = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressUp:)];
    [self.stepUpButton addGestureRecognizer:longPressUp];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)tapped:(id)sender {
    if (self.taps.count > 5) {
        [self.taps removeLastObject];
    }
    [self.taps insertObject:[NSDate date] atIndex:0];
    if (self.taps.count >= 2) {
        NSTimeInterval last_dt = [[self.taps objectAtIndex:0] timeIntervalSinceDate:[self.taps objectAtIndex:1]];
        if (last_dt > 2) {
            [self.taps removeObjectsInRange:NSMakeRange(1, self.taps.count-1)];
        }
    }
    if (self.taps.count > 2) {
        NSTimeInterval dt = 0.;
        for (NSUInteger idx = 1; idx < self.taps.count; idx++) {
            dt += [[self.taps objectAtIndex:idx-1] timeIntervalSinceDate:[self.taps objectAtIndex:idx]];
        }
        dt /= (self.taps.count-1);
        self.tempoValue = (UInt16)(60. / dt + 0.5);
        self.tempoLabel.text = [NSString stringWithFormat:@"%.0f", self.tempoValue];
    }
}

- (IBAction)stepDown:(id)sender {
    if (self.tempoValue > 40.5f) {
        self.tempoValue --;
        self.tempoLabel.text = [NSString stringWithFormat:@"%.0f", self.tempoValue];
    }
}

- (IBAction)stepUp:(id)sender {
    if (self.tempoValue < 239.5f) {
        self.tempoValue ++;
        self.tempoLabel.text = [NSString stringWithFormat:@"%.0f", self.tempoValue];
    }
}

- (void)longPressDown:(UILongPressGestureRecognizer*)gest {
    if (self.timer == nil && gest.state == UIGestureRecognizerStateBegan) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(stepDown:) userInfo:nil repeats:YES];
//        self.stepDownButton.selected = YES;
    }
    else if (self.timer != nil && gest.state == UIGestureRecognizerStateEnded) {
        [self.timer invalidate];
        self.timer = nil;
//        self.stepDownButton.selected = NO;
    }
}

- (void)longPressUp:(UILongPressGestureRecognizer*)gest {
    if (self.timer == nil && gest.state == UIGestureRecognizerStateBegan) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(stepUp:) userInfo:nil repeats:YES];
//        self.stepUpButton.selected = YES;
    }
    else if (self.timer != nil && gest.state == UIGestureRecognizerStateEnded) {
        [self.timer invalidate];
        self.timer = nil;
//        self.stepUpButton.selected = NO;
    }
}

#pragma mark - Table view delegate

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 2) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        UInt16 newTempo = self.tempoValue;
        if (newTempo != doc.song.tempo) {
            [doc.hwUndoManager startUndoableChanges];
            doc.song.tempo = newTempo;
            [doc.hwUndoManager finishUndoableChangesName:@"Tempo"];
            
            [HWAppDelegate recordEvent:@"tempo_changed" segmentation:@{ @"tempo" : @(newTempo) }];
        }
    }
}

@end

@implementation HWTempoCell

- (void)layoutSubviews {
    [super layoutSubviews];

    UIView *label = (id)[self viewWithTag:1];
    label.frame = CGRectMake(label.superview.center.x-100, label.superview.center.y-30, 200, 60);
    
    UIView *leftButton = (id)[self viewWithTag:2];
    leftButton.frame = CGRectMake(4, leftButton.superview.center.y-30, 60, 60);
    
    UIView *rightButton = (id)[self viewWithTag:3];
    rightButton.frame = CGRectMake(rightButton.superview.bounds.size.width - 4 - 60, rightButton.superview.center.y-30, 60, 60);
}

@end