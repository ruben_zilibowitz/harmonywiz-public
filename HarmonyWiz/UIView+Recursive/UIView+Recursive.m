//
//  UIView+Recursive.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "UIView+Recursive.h"

@implementation UIView (Recursive)

- (void)recursivelyRemoveConstraints {
    for (UIView *view in self.subviews) {
        [view recursivelyRemoveConstraints];
    }
    self.translatesAutoresizingMaskIntoConstraints = NO;
    if (self.constraints.count > 0) {
        NSLog(@"removing %d constraint(s) from view %@", self.constraints.count, self);
        NSArray *constraints = [NSArray arrayWithArray:self.constraints];
        for (id constraint in constraints) {
            [self removeConstraint:constraint];
        }
    }
}

@end
