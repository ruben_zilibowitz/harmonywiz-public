//
//  UIView+Recursive.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Recursive)

- (void)recursivelyRemoveConstraints;

@end
