//
//  HWCollectionViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HWCollectionViewController.h"
#import "HWSongCellBackground.h"
#import "HWSongCellSelectedBackground.h"
#import "HWDocument.h"
#import "HWEntry.h"
#import "NSDate+FormattedStrings.h"
#import "NSString+Concatenation.h"
#import "AudioController.h"
#import "HWExceptions.h"
#import "HarmonyWizArchiver.h"
#import "HarmonyWizUnarchiver.h"
#import "HWAppDelegate.h"
#import "HarmonyWizSong+SplitEvent.h"
#include "divmod.h"
#import "SCUI.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "PPCollageEmitterView.h"
#import "HarmonyWizSong+MIDI.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import "NSString+Paths.h"
#import "GZIP/GZIP.h"
#import "NSData+OTP.h"
#import "HWZoomTransitionSegue.h"
#import "UIApplication+Version.h"
#import "AudioShareSDK.h"
#import "HWShareSongMenu.h"
#import "HarmonyWizSong+Lilypond.h"
#import "HarmonyWizSong+MusicXML.h"
#import "UIApplication+Version.h"
#import "NSFileManager+UniqueTemporaryDirectory.h"

NSString * const HWCancelAudioBounce = @"HWCancelAudioBounce";
NSString * const HWOrderedDocumentsFilename = @"documents.plist";
CGFloat const HWScrollSpeed = 800.f;
NSUInteger const HWMaxAttachmentSize = 1024*256;    // 256 Kb
NSUInteger const kMaxSelected = 30;

extern NSString * const HWOpenAnySongNotification;

extern int8_t const HWOTPKey;

@interface HWCollectionViewController () {
    NSMutableArray *_objects;
    NSURL * _localRoot;
    HWDocument * _selDocument;
    UITextField * _activeTextField;
}
@property (nonatomic,assign) enum LayoutMode {kLayoutHorizontal=0, kLayoutVertical} layoutMode;
@property (nonatomic,strong) NSMutableArray *orderedDocuments;
@property (nonatomic,strong) NSMutableSet *selectedEntries;
@property (atomic,strong) NSMutableArray *bouncedPaths;
@property (atomic,strong) MBProgressHUD *progressHUD;
@property (nonatomic,strong) UIPopoverController *visiblePopover;
@property (nonatomic,strong) MBProgressHUD *loadingHUD;
@property (nonatomic,strong) UIBarButtonItem *layoutButton;
@property (nonatomic,strong) UIAlertView *nameAlert;
@end

@implementation HWCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)updateLayoutButton {
    UIImage *img = (self.layoutMode == kLayoutHorizontal ? [UIImage imageNamed:@"images/SmallPreview"] : [UIImage imageNamed:@"images/BigPreview"]);
    [self.layoutButton setImage:img];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.editButtonItem.title = LinguiLocalizedString(@"Select",@"Select button");
    
    self.layoutButton = [[UIBarButtonItem alloc] initWithImage:nil style:UIBarButtonItemStylePlain target:self action:@selector(toggleLayout:)];
    self.layoutMode = kLayoutVertical;
    [self updateLayoutButton];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewSong:)];
    self.navigationItem.rightBarButtonItems = @[self.editButtonItem, self.layoutButton];
    
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    collectionViewLayout.sectionInset = UIEdgeInsetsMake(20, 10, 20, 10);
    
    _objects = [[NSMutableArray alloc] init];
    self.selectedEntries = [NSMutableSet set];
    
    [self refresh];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openDocumentFromURL:) name:HWOpenDocumentFromURL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAnySong:) name:HWOpenAnySongNotification object:nil];
    
    self.collectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    self.collectionView.backgroundView.backgroundColor = UIColorFromRGB(0xFFEEEE);//UIColorFromRGB(0xEEEEEE);
    
    [(LXReorderableCollectionViewFlowLayout*)self.collectionView.collectionViewLayout setScrollingSpeed:HWScrollSpeed];
}

- (void)setLayoutMode:(enum LayoutMode)layoutMode {
    _layoutMode = layoutMode;
    
    UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    collectionViewLayout.scrollDirection = (layoutMode == kLayoutHorizontal ? UICollectionViewScrollDirectionHorizontal : UICollectionViewScrollDirectionVertical);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CGRect frame = CGRectMake(0, 0, self.collectionView.backgroundView.bounds.size.height, self.collectionView.backgroundView.bounds.size.width - 44.f);
    PPCollageEmitterView *collageEmitter = [[PPCollageEmitterView alloc] initWithFrame:frame];
    collageEmitter.tag = 'emit';
    [self.collectionView.backgroundView addSubview:collageEmitter];
    [collageEmitter start];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    PPCollageEmitterView *collageEmitter = (id)[self.collectionView.backgroundView viewWithTag:'emit'];
    [collageEmitter removeFromSuperview];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // if no documents exist on startup then create one
    NSArray *docs = [self localHWDocs];
    if (docs.count == 0) {
        [self insertNewSongWithName:@"My First Song"];
    }
    
    // check for any new documents to import
    [self importDocuments];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)duplicateSelectedSongs {
    if (self.editing) {
        if (self.selectedEntries.count > kMaxSelected) {
            [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Too many selected", @"Too many selected alert title") message:[NSString stringWithFormat:LinguiLocalizedString(@"Cannot duplicate more than %d songs at a time.", @"Too many selected alert message"), kMaxSelected] delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
        }
        else {
            for (HWEntry *entry in self.selectedEntries) {
                NSAssert([_objects containsObject:entry], @"entry does not exist");
                [self duplicateEntry:entry];
            }
        }
    }
}

- (void)deleteSelectedSongs
{
    if (self.selectedEntries.count > kMaxSelected) {
        [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Too many selected", @"Too many selected alert title") message:[NSString stringWithFormat:LinguiLocalizedString(@"Cannot delete more than %d songs at a time.", @"Too many selected alert message"), kMaxSelected] delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
    }
    else {
        __weak typeof(self) weakSelf = self;
        RIButtonItem *OK = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"OK", @"OK button") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            if (strongSelf.editing) {
                for (HWEntry *entry in strongSelf.selectedEntries) {
                    NSAssert([_objects containsObject:entry], @"entry does not exist");
                    [strongSelf deleteEntry:entry];
                }
                
                [strongSelf.selectedEntries removeAllObjects];
                for (UIBarButtonItem *btn in strongSelf.navigationItem.leftBarButtonItems) {
                    btn.enabled = NO;
                }
                [strongSelf updateNavigationTitle];
            }
        }];
        
        [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Delete", @"Delete alert title") message:(self.selectedEntries.count > 1 ? LinguiLocalizedString(@"Are you sure you want to delete the selected songs? This cannot be undone.", @"Delete many alert message") : LinguiLocalizedString(@"Are you sure you want to delete the selected song? This cannot be undone.", @"Dlete one alert message")) cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:OK, nil] show];
    }
}

- (void)actionButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"shareSong" sender:sender];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0)
        return _objects.count;
    else
        return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"HWCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *songImageView = (UIImageView *)[cell viewWithTag:100];
    UITextField *songField = (UITextField*)[cell viewWithTag:101];
    UILabel *dateLabel = (UILabel*)[cell viewWithTag:102];
    
    // bug fix for iOS 7 and earlier
    if (SYSTEM_VERSION_LESS_THAN(@"8")) {
        songImageView.frame = (self.layoutMode == kLayoutHorizontal ? CGRectMake(10, 10, 460, 390) : CGRectMake(10, 10, 300, 230));
        songField.frame = (self.layoutMode == kLayoutHorizontal ? CGRectMake(10, 408, 460, 32) : CGRectMake(10, 259.5, 300, 20.5));
        dateLabel.frame = (self.layoutMode == kLayoutHorizontal ? CGRectMake(10, 440, 460, 30.5) : CGRectMake(10, 290, 300, 20.5));
        cell.backgroundView.frame = (self.layoutMode == kLayoutHorizontal ? CGRectMake(0, 0, 480, 480) : CGRectMake(0, 0, 320, 320));
        cell.selectedBackgroundView.frame = (self.layoutMode == kLayoutHorizontal ? CGRectMake(0, 0, 480, 480) : CGRectMake(0, 0, 320, 320));
        [cell.selectedBackgroundView setNeedsDisplay];
    }
    
    songImageView.alpha = 0.95f;
    songField.backgroundColor = [UIColor clearColor];
    dateLabel.backgroundColor = [UIColor clearColor];
    
    if (cell.backgroundView == nil) {
        cell.backgroundView = [[HWSongCellBackground alloc] initWithFrame:cell.bounds];
    }
    else {
        cell.backgroundView.frame = cell.bounds;
    }
    if (cell.selectedBackgroundView == nil) {
        cell.selectedBackgroundView = [[HWSongCellSelectedBackground alloc] initWithFrame:cell.bounds imageView:songImageView];
        cell.selectedBackgroundView.contentMode = UIViewContentModeRedraw;
    }
    else {
        cell.selectedBackgroundView.frame = cell.bounds;
    }
    
    if (indexPath.section == 0) {
        HWEntry *entry = [_objects objectAtIndex:indexPath.row];
        songImageView.image = (entry.metadata.thumbnail == nil ? [UIImage imageNamed:@"images/NoThumbnail"] : entry.metadata.thumbnail);
        songField.text = entry.description;
        dateLabel.text = [entry.version.modificationDate mediumString];
    }
    else {
        songImageView.image = [UIImage imageNamed:@"images/tutorial_icon.jpg"];
        songField.text = @"Start Tutorial";
        dateLabel.text = @"";
    }
    
    return cell;
}

- (void)updateNavigationTitle {
    if (self.editing) {
        switch (self.selectedEntries.count) {
            case 0:
                self.navigationItem.title = LinguiLocalizedString(@"Select a song", @"Select song title");
                break;
            case 1:
                self.navigationItem.title = LinguiLocalizedString(@"1 song selected", @"One song selected title");
                break;
            default:
                self.navigationItem.title = [NSString stringWithFormat:LinguiLocalizedString(@"%u songs selected", @"Many songs selected title"), self.selectedEntries.count];
                break;
        }
    }
    else {
        self.navigationItem.title = LinguiLocalizedString(@"HarmonyWiz", @"HarmonyWiz title");
    }
}

- (void)showUnableToOpenSongAlertFor:(NSString*)name error:(NSError*)error {
    [[[UIAlertView alloc] initWithTitle:@"Unable to open song" message:error.localizedFailureReason delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

/*
- (BOOL)versionCheck {
    NSString *minimumVersion = [[NSUserDefaults standardUserDefaults] stringForKey:@"minimumVersion"];
    if (minimumVersion != nil) {
        NSString *currentVersion = [UIApplication appVersion];
        if ([minimumVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
            [[[UIAlertView alloc] initWithTitle:@"Please update HarmonyWiz" message:[NSString stringWithFormat:@"You are using HarmonyWiz version %@. Please update to version %@ to continue.", currentVersion, minimumVersion] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            return NO;
        }
    }
    return YES;
}
 */

- (void)openSongForFileURL:(NSURL*)fileURL {
    _selDocument = [[HWDocument alloc] initWithFileURL:fileURL];
    __weak typeof(self) weakSelf = self;
    [_selDocument openWithCompletionHandler:^(BOOL success) {
        if (!success) {
            NSLog(@"Failed to open %@", fileURL);
            return;
        }
        __strong typeof(self) strongSelf = weakSelf;
        if (_selDocument.song != nil) {
            [strongSelf performSegueWithIdentifier:@"openSong" sender:self];
        }
        else {
            [self showUnableToOpenSongAlertFor:fileURL.lastPathComponent error:_selDocument.openingSongError];
            [self.progressHUD hide:YES];
            self.progressHUD = nil;
            [_selDocument closeWithCompletionHandler:nil];
            _selDocument = nil;
        }
    }];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HWEntry * entry = [_objects objectAtIndex:indexPath.row];
    
    if (self.editing) {
        [self.selectedEntries addObject:entry];
        [self updateNavigationTitle];
    }
    else if (self.progressHUD == nil) {
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
        
        HWEntry * entry = [_objects objectAtIndex:indexPath.row];
        
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
        UIView *songView = [cell viewWithTag:100];
        
        self.progressHUD = [[MBProgressHUD alloc] initWithView:songView];
        self.progressHUD.mode = MBProgressHUDModeIndeterminate;
        self.progressHUD.dimBackground = YES;
        self.progressHUD.labelText = LinguiLocalizedString(@"Opening", @"Opening label text");
        self.progressHUD.removeFromSuperViewOnHide = YES;
        [songView addSubview:self.progressHUD];
        [self.progressHUD show:YES];
        
        [self openSongForFileURL:entry.fileURL];
    }
    
    if (self.selectedEntries.count > 0 && self.isEditing) {
        for (UIBarButtonItem *btn in self.navigationItem.leftBarButtonItems) {
            btn.enabled = YES;
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    HWEntry * entry = [_objects objectAtIndex:indexPath.row];
    
    [self.selectedEntries removeObject:entry];
    [self updateNavigationTitle];
    
    if (self.selectedEntries.count == 0 && self.isEditing) {
        for (UIBarButtonItem *btn in self.navigationItem.leftBarButtonItems) {
            btn.enabled = NO;
        }
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    self.layoutButton.enabled = (!editing);
    [self.selectedEntries removeAllObjects];
    if (self.visiblePopover) {
        [self.visiblePopover dismissPopoverAnimated:YES];
        self.visiblePopover = nil;
    }
    if (editing) {
        UIImage *duplicateIcon = nil;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
            duplicateIcon = [UIImage imageNamed:@"images/DuplicateIcon"];
        }
        else {
            duplicateIcon = [UIImage imageNamed:@"images/DuplicateIconiOS6"];
        }
        self.navigationItem.leftBarButtonItems =
        @[ self.actionButton,
           [[UIBarButtonItem alloc] initWithImage:duplicateIcon style:UIBarButtonItemStylePlain target:self action:@selector(duplicateSelectedSongs)],
           [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteSelectedSongs)]
           ];
        self.collectionView.allowsMultipleSelection = YES;
        for (UIBarButtonItem *btn in self.navigationItem.leftBarButtonItems) {
            btn.enabled = NO;
        }
    }
    else {
        self.navigationItem.leftBarButtonItems =
        @[ [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewSong:)] ];
        for(NSIndexPath *indexPath in self.collectionView.indexPathsForSelectedItems) {
            [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
        }
        self.collectionView.allowsMultipleSelection = NO;
        self.editButtonItem.title = LinguiLocalizedString(@"Select",@"Select button");
    }
    [self updateNavigationTitle];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    extern CGFloat const HWTracksViewClosedWidth;
    extern const CGFloat HWRulerHeight;
    extern const CGFloat HWRulerHeightMagicMode;
    extern CGFloat const HWToolboxWidth;
    
    if ([segue.identifier isEqualToString:@"openSong"]) {
        UIImageView *imageView = (id)self.progressHUD.superview;
        [self.progressHUD hide:NO];
        self.progressHUD = nil;
        
        // configure segue
        HWZoomTransitionSegue *imageSegue = (HWZoomTransitionSegue *)segue;
        
        if (_selDocument.metadata.magicMode) {
            imageSegue.destinationRect = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - HWRulerHeightMagicMode);
        }
        else {
            imageSegue.destinationRect = CGRectMake(HWTracksViewClosedWidth, HWRulerHeight, CGRectGetWidth(self.view.frame) - HWTracksViewClosedWidth - HWToolboxWidth, CGRectGetHeight(self.view.frame) - HWRulerHeight);
        }
        imageSegue.sourceRect = [imageView convertRect:imageView.bounds toView:self.view];
        
        if (imageView == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
                UIImageView *imageView = (id)[cell viewWithTag:100];
                self.zoomTransitionSourceRect = [imageView convertRect:imageView.bounds toView:self.view];
            });
        }
        else {
            self.zoomTransitionSourceRect = imageSegue.sourceRect;
            imageSegue.transitionImage = imageView.image;
        }
        
        // setup destination controller
        [[segue destinationViewController] setDoc:_selDocument];
        currentlyOpenDocumentData = _selDocument;
        self.songVC = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:@"shareSong"]) {
        UIStoryboardPopoverSegue *pseg = (id)segue;
        HWShareSongMenu *shareSongMenu = pseg.destinationViewController;
        shareSongMenu.parentPopover = pseg.popoverController;
        shareSongMenu.songSelectView = self;
        self.visiblePopover = pseg.popoverController;
        self.visiblePopover.delegate = self;
    }
}

- (IBAction) unwindAction:(UIStoryboardSegue *)segue {
    HWSongViewController *songViewController = segue.sourceViewController;
    NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:songViewController.doc.fileURL];
    [self addOrUpdateEntryWithURL:songViewController.doc.fileURL metadata:songViewController.doc.metadata state:songViewController.doc.documentState version:version];
    currentlyOpenDocumentData = nil;
    self.songVC = nil;
    _selDocument = nil;
}

- (void)writeOrderedDocumentsFile {
    [self.orderedDocuments writeToURL:[self.localRoot URLByAppendingPathComponent:HWOrderedDocumentsFilename] atomically:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return (self.layoutMode == kLayoutHorizontal ? CGSizeMake(480, 480) : CGSizeMake(320, 320));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return (self.layoutMode == kLayoutHorizontal ? 24.f : 2.f);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return (self.layoutMode == kLayoutHorizontal ? 24.f : 2.f);
}

#pragma mark - LXReorderableCollectionViewDataSource methods

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath didMoveToIndexPath:(NSIndexPath *)toIndexPath {
    
    [self writeOrderedDocumentsFile];
}

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath willMoveToIndexPath:(NSIndexPath *)toIndexPath {
    
    HWEntry *entry = [_objects objectAtIndex:fromIndexPath.item];
    
    [_objects removeObjectAtIndex:fromIndexPath.item];
    [_objects insertObject:entry atIndex:toIndexPath.item];
    
    NSString *doc = [self.orderedDocuments objectAtIndex:fromIndexPath.item];
    
    [self.orderedDocuments removeObjectAtIndex:fromIndexPath.item];
    [self.orderedDocuments insertObject:doc atIndex:toIndexPath.item];
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath {
    return YES;
}

#pragma mark - actions

- (void)toggleLayout:(id)sender {
    [self setLayoutMode:(self.layoutMode == kLayoutHorizontal ? kLayoutVertical : kLayoutHorizontal)];
    [self updateLayoutButton];
}

- (void)insertNewSongWithName:(NSString*)name {
    
    // Determine a unique filename to create
    NSURL * fileURL = [self getDocURL:[self getDocFilename:name uniqueInObjects:YES]];
    NSLog(@"Want to create file at %@", fileURL);
    
    // Create new document and save to the filename
    HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
    __weak typeof(self) weakSelf = self;
    [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
        
        if (!success) {
            NSLog(@"Failed to create file at %@", fileURL);
            return;
        }
        
        NSLog(@"File created at %@", fileURL);
        HWMetadata * metadata = doc.metadata;
        NSURL * fileURL = doc.fileURL;
        UIDocumentState state = doc.documentState;
        NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
        
        // Add on the main thread and perform the segue
        _selDocument = doc;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.collectionView.contentOffset = CGPointZero;
            [strongSelf addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
            [strongSelf performSegueWithIdentifier:@"openSong" sender:self];
        });
    }];
}

- (IBAction)insertNewSong:(id)sender {
    self.nameAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Enter a name for your song", @"Alert title") message:nil delegate:self cancelButtonTitle:LinguiLocalizedString(@"Cancel", @"Cancel button") otherButtonTitles:LinguiLocalizedString(@"OK", @"OK button"), nil];
    self.nameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *textField = [self.nameAlert textFieldAtIndex:0];
    [textField setPlaceholder:LinguiLocalizedString(@"Song name", @"Field placeholder")];
    [textField setText:LinguiLocalizedString(@"My Song", @"Default song name")];
    textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    if (1) {
        [textField setClearButtonMode:UITextFieldViewModeUnlessEditing];  // this is buggy in iOS 6 and 7 !!! The clear button is not centered in the view
    }
    else
    {
        // work around for clear button
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 19, 19)];
        [button setImage:[UIImage imageNamed:@"images/clearButton"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"images/clearButtonHighlighted"] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(clearText) forControlEvents:UIControlEventTouchUpInside];
        
        [textField setRightView:button];
        textField.rightViewMode = UITextFieldViewModeAlways;
    }
    
    [self.nameAlert show];
}

//- (void)clearText {
//    UITextField *textField = [self.nameAlert textFieldAtIndex:0];
//    [textField setText:nil];
//}

#pragma mark Helpers

- (BOOL)iCloudOn {
    return NO;
}

- (NSURL *)localRoot {
    if (_localRoot != nil) {
        return _localRoot;
    }
    
    _localRoot = [NSURL fileURLWithPath:[NSString saveFilesDirectory]];
    
    // create the directory if it does not exist
    if (! [[NSFileManager defaultManager] fileExistsAtPath:_localRoot.path]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] createDirectoryAtURL:_localRoot withIntermediateDirectories:YES attributes:nil error:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            recordError(error)
        }
    }
    
    return _localRoot;
}

- (NSURL *)getDocURL:(NSString *)filename {
    if ([self iCloudOn]) {
        // TODO
        return nil;
    } else {
        return [self.localRoot URLByAppendingPathComponent:filename];
    }
}

- (BOOL)docNameExistsInObjects:(NSString *)docName {
    BOOL nameExists = NO;
    for (HWEntry * entry in _objects) {
        if ([[entry.fileURL lastPathComponent] isEqualToString:docName]) {
            nameExists = YES;
            break;
        }
    }
    return nameExists;
}

- (NSString*)getDocFilename:(NSString *)prefix uniqueInObjects:(BOOL)uniqueInObjects {
    NSInteger docCount = 0;
    NSString* newDocName = nil;
    
    // At this point, the document list should be up-to-date.
    BOOL done = NO;
    BOOL first = YES;
    while (!done) {
        if (first) {
            first = NO;
            newDocName = [NSString stringWithFormat:@"%@.%@",
                          prefix, HW_EXTENSION];
        } else {
            newDocName = [NSString stringWithFormat:@"%@ %ld.%@",
                          prefix, (long)docCount, HW_EXTENSION];
        }
        
        // Look for an existing document with the same name. If one is
        // found, increment the docCount value and try again.
        BOOL nameExists;
        if (uniqueInObjects) {
            nameExists = [self docNameExistsInObjects:newDocName];
        } else {
            // TODO
            return nil;
        }
        if (!nameExists) {
            break;
        } else {
            docCount++;
        }
        
    }
    
    return newDocName;
}

#pragma mark Entry management methods

- (NSUInteger)indexOfEntryWithFileURL:(NSURL *)fileURL {
    return [_objects indexOfObjectPassingTest:^BOOL(HWEntry * entry, NSUInteger idx, BOOL *stop) {
        return [entry.fileURL isEqual:fileURL];
    }];
}

- (void)addOrUpdateEntryWithURL:(NSURL *)fileURL metadata:(HWMetadata *)metadata state:(UIDocumentState)state version:(NSFileVersion *)version {
    
    NSUInteger index = [self indexOfEntryWithFileURL:fileURL];
    
    if (self.loadingHUD) {
        [self.loadingHUD hide:YES];
        self.loadingHUD = nil;
    }
    
    // Not found, so add
    if (index == NSNotFound) {
        
        HWEntry * entry = [[HWEntry alloc] initWithFileURL:fileURL metadata:metadata state:state version:version];
        
        // Fix up ordered documents file
        index = [self.orderedDocuments indexOfObject:fileURL.lastPathComponent];
        if (index == NSNotFound) {
            [self.orderedDocuments insertObject:fileURL.lastPathComponent atIndex:0];
            [self writeOrderedDocumentsFile];
            
            [_objects insertObject:entry atIndex:0];
            
            [self.collectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]]];
        }
        else {
            index = [_objects indexOfObject:entry inSortedRange:NSMakeRange(0, _objects.count) options:NSBinarySearchingInsertionIndex usingComparator:^NSComparisonResult(HWEntry *obj1,HWEntry *obj2) {
                NSUInteger idx1 = [self.orderedDocuments indexOfObject:obj1.fileURL.lastPathComponent];
                NSUInteger idx2 = [self.orderedDocuments indexOfObject:obj2.fileURL.lastPathComponent];
                if (idx1 == idx2)   return NSOrderedSame;
                else if (idx1 == NSNotFound)    return NSOrderedAscending;
                else if (idx2 == NSNotFound)    return NSOrderedDescending;
                else if (idx1 < idx2)   return NSOrderedAscending;
                else    return NSOrderedDescending;
            }];
            
            [_objects insertObject:entry atIndex:index];
            
            [self.collectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]]];
        }
    }
    
    // Found, so edit
    else {
        
        HWEntry * entry = [_objects objectAtIndex:index];
        entry.metadata = metadata;
        entry.state = state;
        entry.version = version;
        
        [self.collectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]]];
    }
    
}

- (BOOL)renameEntry:(HWEntry *)entry to:(NSString *)filename {
    
    // remove forward slash characters since it
    // is a path delimiter and confuses iOS
    filename = [filename stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    
    // Bail if not actually renaming
    if ([entry.description isEqualToString:filename]) {
        return YES;
    }
    
    // Check if can rename file
    NSString * newDocFilename = [NSString stringWithFormat:@"%@.%@", filename, HW_EXTENSION];
    if ([self docNameExistsInObjects:newDocFilename]) {
        NSString * message = [NSString stringWithFormat:LinguiLocalizedString(@"\"%@\" is already taken. Please choose a different name.", @"Filename already taken message"), filename];
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:LinguiLocalizedString(@"OK", @"OK button"), nil];
        [alertView show];
        return NO;
    }
    
    NSURL * newDocURL = [self getDocURL:newDocFilename];
    NSLog(@"Moving %@ to %@", entry.fileURL, newDocURL);
    
    // Move file
    NSURL* fileURL = entry.fileURL;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        @autoreleasepool {
            NSFileCoordinator* fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
            [fileCoordinator coordinateWritingItemAtURL:fileURL options:NSFileCoordinatorWritingForMoving
                                                  error:nil byAccessor:^(NSURL* writingURL) {
                                                      NSFileManager* fileManager = [NSFileManager defaultManager];
                                                      BOOL result = [fileManager moveItemAtURL:fileURL toURL:newDocURL error:nil];
                                                      NSAssert(result, @"moveItemAtURL failed");
                                                  }];
            
            // rename the corresponding shared files, if they exist
            for (NSString *extension in @[HW_MIDI_EXTENSION,HW_LILYPOND_EXTENSION,HW_MUSICXML_EXTENSION,HW_GZIPPED_EXTENSION,HW_AUDIO_EXTENSION]) {
                NSString *sharedPath = [[NSString documentsDirectory] stringByAppendingPathComponent:[fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:extension]];
                if ([[NSFileManager defaultManager] fileExistsAtPath:sharedPath]) {
                    NSError *error;
                    [[NSFileManager defaultManager] moveItemAtPath:sharedPath toPath:[[sharedPath.stringByDeletingLastPathComponent stringByAppendingPathComponent:filename] stringByAppendingPathExtension:extension] error:&error];
                    if (error) {
                        NSLog(@"%@", error.localizedDescription);
                        recordError(error)
                    }
                }
            }
        }
    });
    
    // Fix up entry
    entry.fileURL = newDocURL;
    NSUInteger index = [self indexOfEntryWithFileURL:entry.fileURL];
    [self.collectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]]];
    
    // Fix up ordered documents file
    NSUInteger idx = [self.orderedDocuments indexOfObject:fileURL.lastPathComponent];
    [self.orderedDocuments replaceObjectAtIndex:idx withObject:entry.fileURL.lastPathComponent];
    [self writeOrderedDocumentsFile];
    
    return YES;
}

- (void)removeEntryWithURL:(NSURL *)fileURL {
    // Fix up ordered documents file
    [self.orderedDocuments removeObject:fileURL.lastPathComponent];
    [self writeOrderedDocumentsFile];
    
    NSUInteger index = [self indexOfEntryWithFileURL:fileURL];
    [_objects removeObjectAtIndex:index];
    [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]]];
}

#pragma mark File management methods

- (void)loadDocAtURL:(NSURL *)fileURL {
    
    // Open doc so we can read metadata
    HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
    __weak typeof(self) weakSelf = self;
    [doc openWithCompletionHandler:^(BOOL success) {
        
        // Check status
        if (!success) {
            NSLog(@"Failed to open %@", fileURL);
            return;
        }
        
//        if (doc.song == nil) {
//            [self showUnableToOpenSongAlertFor:fileURL.lastPathComponent];
//            [doc closeWithCompletionHandler:nil];
//            return;
//        }
        
        // Preload metadata on background thread
        HWMetadata * metadata = doc.metadata;
        NSURL * fileURL = doc.fileURL;
        UIDocumentState state = doc.documentState;
        NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
        NSLog(@"Loaded File URL: %@", [doc.fileURL lastPathComponent]);
        
        // Close since we're done with it
        [doc closeWithCompletionHandler:^(BOOL success) {
            
            // Check status
            if (!success) {
                NSLog(@"Failed to close %@", fileURL);
                // Continue anyway...
            }
            
            // Add to the list of files on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong typeof(self) strongSelf = weakSelf;
                [strongSelf addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
            });
        }];
    }];
}

- (void)duplicateEntry:(HWEntry*)entry {
    
    NSURL *fileURL = entry.fileURL;
    
    // Open doc so we can read metadata
    HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
    __weak typeof(self) weakSelf = self;
    [doc openWithCompletionHandler:^(BOOL success) {
        
        // Check status
        if (!success) {
            NSLog(@"Failed to open %@", fileURL);
            return;
        }
        
//        if (doc.song == nil) {
//            [self showUnableToOpenSongAlertFor:fileURL.lastPathComponent];
//            [doc closeWithCompletionHandler:nil];
//            return;
//        }
        
        // Preload metadata and song on background thread
        HarmonyWizSong *theSong = doc.song;
        HWMetadata * theMetadata = doc.metadata;
        NSURL * fileURL = doc.fileURL;
        NSLog(@"Loaded File URL: %@", [doc.fileURL lastPathComponent]);
        
        // Close since we're done with it
        [doc closeWithCompletionHandler:^(BOOL success) {
            
            // Check status
            if (!success) {
                NSLog(@"Failed to close %@", fileURL);
                // Continue anyway...
            }
            
            // Add to the list of files on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong typeof(self) strongSelf = weakSelf;
                // Determine a unique filename to create
                NSString *filename  = entry.fileURL.lastPathComponent.stringByDeletingPathExtension;
                filename = [strongSelf getUniqueFilenameFrom:filename];
                NSURL * fileURL = [strongSelf getDocURL:[strongSelf getDocFilename:filename uniqueInObjects:YES]];
                NSLog(@"Want to create file at %@", fileURL);
                
                // Create new document and save to the filename
                HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
                doc.metadata = theMetadata;
                doc.song = theSong;
                
                [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
                    
                    if (!success) {
                        NSLog(@"Failed to create file at %@", fileURL);
                        return;
                    }
                    
                    NSLog(@"File created at %@", fileURL);
                    HWMetadata * metadata = doc.metadata;
                    NSURL * fileURL = doc.fileURL;
                    UIDocumentState state = doc.documentState;
                    NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
                    
                    // Add on the main thread and perform the segue
                    _selDocument = doc;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        __strong typeof(self) strongSelf = weakSelf;
                        [strongSelf addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
                    });
                }];
            });
        }];
    }];
}

- (void)deleteEntry:(HWEntry *)entry {
    
    NSURL* fileURL = entry.fileURL;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        @autoreleasepool {
            NSFileCoordinator* fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
            [fileCoordinator coordinateWritingItemAtURL:fileURL options:NSFileCoordinatorWritingForDeleting
                                                  error:nil byAccessor:^(NSURL* writingURL) {
                                                      NSFileManager* fileManager = [NSFileManager defaultManager];
                                                      BOOL result = [fileManager removeItemAtURL:writingURL error:nil];
                                                      NSAssert(result, @"removeItemAtURL failed");
                                                  }];
            
            // remove the corresponding shared files, if they exists
            for (NSString *extension in @[HW_MIDI_EXTENSION,HW_LILYPOND_EXTENSION,HW_MUSICXML_EXTENSION,HW_GZIPPED_EXTENSION,HW_AUDIO_EXTENSION]) {
                NSString *sharedPath = [[NSString documentsDirectory] stringByAppendingPathComponent:[fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:extension]];
                if ([[NSFileManager defaultManager] fileExistsAtPath:sharedPath]) {
                    NSError *error;
                    [[NSFileManager defaultManager] removeItemAtPath:sharedPath error:&error];
                    if (error) {
                        NSLog(@"%@", error.localizedDescription);
                        recordError(error)
                    }
                }
            }
        }
    });
    
    // Fixup view
    [self removeEntryWithURL:fileURL];
}

#pragma mark Refresh Methods

- (NSMutableArray*)orderedDocuments {
    if (_orderedDocuments == nil) {
        NSURL *docfile = [self.localRoot URLByAppendingPathComponent:HWOrderedDocumentsFilename];
        _orderedDocuments = [NSMutableArray arrayWithContentsOfURL:docfile];
        if (_orderedDocuments == nil) {
            _orderedDocuments = [NSMutableArray arrayWithArray:[self.localHWDocs valueForKey:@"lastPathComponent"]];
            [_orderedDocuments writeToURL:docfile atomically:YES];
        }
    }
    return _orderedDocuments;
}

- (NSArray*)localHWDocs {
    return [[[NSFileManager defaultManager] contentsOfDirectoryAtURL:self.localRoot includingPropertiesForKeys:nil options:0 error:nil] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension == %@", HW_EXTENSION]];
}

- (void)loadLocal {
    
    NSArray * localDocuments = [self localHWDocs];
    NSLog(@"Found %lu local files.", (unsigned long)localDocuments.count);
    for (NSURL * fileURL in localDocuments) {
        NSLog(@"Found local file: %@", fileURL);
        [self loadDocAtURL:fileURL];
    }
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)refresh {
    
    self.loadingHUD = [[MBProgressHUD alloc] initWithView:self.view];
    self.loadingHUD.mode = MBProgressHUDModeIndeterminate;
    self.loadingHUD.labelText = LinguiLocalizedString(@"Loading", @"Loading label text");
    self.loadingHUD.dimBackground = NO;
    [self.view addSubview:self.loadingHUD];
    self.loadingHUD.removeFromSuperViewOnHide = YES;
    [self.loadingHUD show:YES];
    
    [_objects removeAllObjects];
    [self.collectionView reloadData];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    if (![self iCloudOn]) {
        [self loadLocal];
    }        
}

/*
-(void) keyboardWillShow:(NSNotification *)note
{
    self.collectionView.scrollEnabled = NO;
    
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = self.collectionView.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height -= keyboardBounds.size.height;
    else
        frame.size.height -= keyboardBounds.size.width;
    
    // Apply new size of table view
    self.collectionView.frame = frame;
    
    // Scroll the table view to see the TextField just above the keyboard
    if (_activeTextField)
    {
        CGRect textFieldRect = [self.collectionView convertRect:_activeTextField.bounds fromView:_activeTextField];
        [self.collectionView scrollRectToVisible:textFieldRect animated:YES];
    }
    
    [UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    self.collectionView.scrollEnabled = YES;
    
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = self.collectionView.frame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height += keyboardBounds.size.height;
    else
        frame.size.height += keyboardBounds.size.width;
    
    // Apply new size of table view
    self.collectionView.frame = frame;
    
    [UIView commitAnimations];
}
*/

- (void)readDocumentsWithURLs:(NSSet*)urls withCompletion:(void(^)(NSArray*results))completionBlock {
    BOOL __block completion = NO;
    NSMutableArray *results = [NSMutableArray arrayWithCapacity:urls.count];
    for (NSURL *fileURL in urls) {
        HWDocument *doc = [[HWDocument alloc] initWithFileURL:fileURL];
        [doc openWithCompletionHandler:^(BOOL success) {
            if (!success) {
                NSLog(@"Failed to open %@", fileURL);
                return;
            }
            
            if (doc.song != nil) {
                [results addObject:doc];
            }
            else {
                NSLog(@"Could not open document '%@'", doc.fileURL.lastPathComponent);
            }
            
            // Close since we're done with it
            [doc closeWithCompletionHandler:^(BOOL success) {
                if (!success) {
                    NSLog(@"Failed to close %@", fileURL);
                }
                if (results.count == urls.count && !completion) {
                    completion = YES;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionBlock(results);
                    });
                }
            }];
        }];
    }
}

- (void)hudWasCancelled:(MBProgressHUD *)hud {
    if (hud == self.progressHUD) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWCancelAudioBounce object:self];
    }
}

- (void)bounceSelectedDocuments:(void (^)(NSArray*))completion {
    self.progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    self.progressHUD.dimBackground = YES;
    self.progressHUD.removeFromSuperViewOnHide = YES;
    self.progressHUD.labelText = LinguiLocalizedString(@"Bouncing audio", @"Bouncing audio label text");
    self.progressHUD.detailsLabelText = LinguiLocalizedString(@"Initializing", @"Initialising detail label text");
    self.progressHUD.cancelButtonTitle = LinguiLocalizedString(@"Stop", @"Stop button");
    self.progressHUD.showCancelButton = YES;
    self.progressHUD.delegate = self;
    [self.view.window addSubview:self.progressHUD];
    
    self.bouncedPaths = [NSMutableArray arrayWithCapacity:self.selectedEntries.count];
    
    __weak typeof(self.progressHUD) weakProgressHUD = self.progressHUD;
    __weak typeof(self) weakSelf = self;
    [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
        [weakProgressHUD showAnimated:YES whileExecutingBlock:^{
            @autoreleasepool {
                __strong typeof(self) strongSelf = weakSelf;
                NSEnumerator *enumerator = result.objectEnumerator;
                HWDocument *doc = nil;
                while ((doc = enumerator.nextObject)) {
                    NSURL *fileURL = doc.fileURL;
                    NSString *bouncePath = [[[[[NSFileManager defaultManager] createUniqueTemporaryDirectory] stringByAppendingPathComponent:fileURL.lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_AUDIO_EXTENSION];
                    if (!bouncePath) {
                        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"Failed to create temporary directory" userInfo:nil];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        weakProgressHUD.mode = MBProgressHUDModeDeterminate;
                        weakProgressHUD.progress = 0;
                        weakProgressHUD.detailsLabelText = fileURL.lastPathComponent.stringByDeletingPathExtension;
                    });
                    [doc bounceDocumentToPath:bouncePath withHUD:weakProgressHUD];
                    [strongSelf.bouncedPaths addObject:bouncePath];
                }
            }
        } completionBlock:^{
            __strong typeof(self) strongSelf = weakSelf;
            completion(strongSelf.bouncedPaths);
            strongSelf.bouncedPaths = nil;
            strongSelf.progressHUD = nil;
        }];
    }];
}

void copyFilesToDocumentsDirectory(NSArray* results) {
    for (NSString *path in results) {
        NSError *error = nil;
        NSString *dest = [[NSString documentsDirectory] stringByAppendingPathComponent:path.pathComponents.lastObject];
        NSLog(@"copying %@ to %@", path, dest);
        [[NSFileManager defaultManager] copyItemAtPath:path toPath:dest error:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            recordError(error)
        }
    }
}

- (void)cannotSendMailAlert {
    [[[UIAlertView alloc] initWithTitle:@"Cannot send mail" message:@"The device is currently not able to send mail." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)mailBouncedFiles:(NSArray*)results {
    if (![MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    BOOL plural = (self.selectedEntries.count > 1);
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    [mailVC setSubject:(plural ? LinguiLocalizedString(@"Shared HarmonyWiz Songs", @"Many songs shared mail subject") : LinguiLocalizedString(@"Shared HarmonyWiz Song", @"One song shared mail subject"))];
    [mailVC setMessageBody:(plural ? LinguiLocalizedString(@"I would like to share these HarmonyWiz songs with you.", @"Like to share many songs mail body") : LinguiLocalizedString(@"I would like to share this HarmonyWiz song with you.", @"Like to share one song mail body")) isHTML:NO];
    for (NSString *path in results) {
        NSData *data = [NSData dataWithContentsOfFile:path];
        [mailVC addAttachmentData:data mimeType:@"audio/aif" fileName:path.lastPathComponent];
    }
    [mailVC setMailComposeDelegate:self];
    [self presentViewController:mailVC animated:YES completion:nil];
}

- (void)addSongAttachment:(NSArray*)result to:(MFMailComposeViewController*)mailVC {
    for (HWDocument *doc in result) {
        NSData *exportData = doc.dataForExport;
        [mailVC addAttachmentData:exportData mimeType:@"song/x-hwgz" fileName:[doc.fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:HW_GZIPPED_EXTENSION]];
    }
}

- (void)shareSongViaITunes:(NSArray*)result {
    for (HWDocument *doc in result) {
        NSData *data = doc.dataForExport;
        
        NSString *path = [[[[NSString documentsDirectory] stringByAppendingPathComponent:[doc.fileURL lastPathComponent]] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_GZIPPED_EXTENSION];
        [data writeToFile:path atomically:YES];
    }
}

- (void)addSongAttachmentNoDrawings:(NSArray*)result to:(MFMailComposeViewController*)mailVC {
    for (HWDocument *doc in result) {
        [doc.song.misc removeAllObjects];
        NSData *exportData = doc.dataForExport;
        [mailVC addAttachmentData:exportData mimeType:@"song/x-hwgz" fileName:[doc.fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:HW_GZIPPED_EXTENSION]];
    }
}

- (NSUInteger)songsWithoutDrawingsSize:(NSArray*)result {
    NSUInteger length = 0;
    for (HWDocument *doc in result) {
        [doc.song.misc removeAllObjects];
        NSData *exportData = doc.dataForExport;
        length += exportData.length;
    }
    return length;
}

- (NSUInteger)songsWithDrawingsSize:(NSArray*)result {
    NSUInteger length = 0;
    for (HWDocument *doc in result) {
        NSData *exportData = doc.dataForExport;
        length += exportData.length;
    }
    return length;
}

- (void)shareSongDataByMail {
    if (![MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    BOOL plural = (self.selectedEntries.count > 1);
    [mailVC setSubject:(plural ? LinguiLocalizedString(@"Shared HarmonyWiz Songs", @"Many songs shared mail subject") : LinguiLocalizedString(@"Shared HarmonyWiz Song", @"One song shared mail subject"))];
    [mailVC setMessageBody:(plural ? LinguiLocalizedString(@"I would like to share these HarmonyWiz songs with you.", @"Like to share many songs mail body") : LinguiLocalizedString(@"I would like to share this HarmonyWiz song with you.", @"Like to share one song mail body")) isHTML:NO];
    
    MBProgressHUD *shareHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    shareHUD.mode = MBProgressHUDModeIndeterminate;
    shareHUD.labelText = LinguiLocalizedString(@"Preparing songs", @"Preparing songs label text");
    shareHUD.removeFromSuperViewOnHide = YES;
    [self.view.window addSubview:shareHUD];
    [shareHUD show:YES];
    
    __weak typeof(self) weakSelf = self;
    [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                __strong typeof(self) strongSelf = weakSelf;
                NSUInteger length = [strongSelf songsWithDrawingsSize:result];
                if (length > HWMaxAttachmentSize) {
                    NSUInteger lengthWithoutDrawings = [strongSelf songsWithoutDrawingsSize:result];
                    NSString *includeDrawingsString = [NSString stringWithFormat:@"Include drawings (%@)", [NSByteCountFormatter stringFromByteCount:length countStyle:NSByteCountFormatterCountStyleFile]];
                    NSString *excludeDrawingsString = [NSString stringWithFormat:@"Share only songs (%@)", [NSByteCountFormatter stringFromByteCount:lengthWithoutDrawings countStyle:NSByteCountFormatterCountStyleFile]];
                    RIButtonItem *drawings = [RIButtonItem itemWithLabel:includeDrawingsString action:^{
                        __strong typeof(self) strongSelf = weakSelf;
                        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:strongSelf.view.window];
                        hud.mode = MBProgressHUDModeIndeterminate;
                        hud.labelText = LinguiLocalizedStringForBlocks(@"Adding attachments", @"Adding attachments label text");
                        hud.removeFromSuperViewOnHide = YES;
                        [strongSelf.view.window addSubview:hud];
                        [hud showAnimated:YES whileExecutingBlock:^{
                            @autoreleasepool {
                                __strong typeof(self) strongSelf = weakSelf;
                                [strongSelf addSongAttachment:result to:mailVC];
                            }
                        } completionBlock:^{
                            __strong typeof(self) strongSelf = weakSelf;
                            [mailVC setMailComposeDelegate:strongSelf];
                            [strongSelf presentViewController:mailVC animated:YES completion:nil];
                        }];
                    }];
                    RIButtonItem *songOnly = [RIButtonItem itemWithLabel:excludeDrawingsString action:^{
                        __strong typeof(self) strongSelf = weakSelf;
                        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:strongSelf.view.window];
                        hud.mode = MBProgressHUDModeIndeterminate;
                        hud.labelText = LinguiLocalizedStringForBlocks(@"Adding attachments", @"Adding attachments label text");
                        hud.removeFromSuperViewOnHide = YES;
                        [strongSelf.view.window addSubview:hud];
                        [hud showAnimated:YES whileExecutingBlock:^{
                            @autoreleasepool {
                                __strong typeof(self) strongSelf = weakSelf;
                                [strongSelf addSongAttachmentNoDrawings:result to:mailVC];
                            }
                        } completionBlock:^{
                            __strong typeof(self) strongSelf = weakSelf;
                            [mailVC setMailComposeDelegate:strongSelf];
                            [strongSelf presentViewController:mailVC animated:YES completion:nil];
                        }];
                    }];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [shareHUD hide:YES];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"What would you like to do?" message:nil cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedStringForBlocks(@"Cancel", @"Cancel button")] otherButtonItems:drawings, songOnly, nil];
                        [alert show];
                    });
                }
                else {
                    [strongSelf addSongAttachment:result to:mailVC];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        __strong typeof(self) strongSelf = weakSelf;
                        [shareHUD hide:YES];
                        [mailVC setMailComposeDelegate:strongSelf];
                        [strongSelf presentViewController:mailVC animated:YES completion:nil];
                    });
                }
            }
        });
    }];
}

- (void)shareSongMIDIByMail {
    if (![MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    BOOL plural = (self.selectedEntries.count > 1);
    [mailVC setSubject:(plural ? LinguiLocalizedString(@"Shared HarmonyWiz Songs", @"Many songs shared mail subject") : LinguiLocalizedString(@"Shared HarmonyWiz Song", @"One song shared mail subject"))];
    [mailVC setMessageBody:(plural ? LinguiLocalizedString(@"I would like to share these HarmonyWiz songs with you.", @"Like to share many songs mail body") : LinguiLocalizedString(@"I would like to share this HarmonyWiz song with you.", @"Like to share one song mail body")) isHTML:NO];
    
    __weak typeof(self) weakSelf = self;
    [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
        __strong typeof(self) strongSelf = weakSelf;
        for (HWDocument *doc in result) {
            HarmonyWizSong *song = doc.song;
            NSData *data = [song convertToMIDIData];
            NSString *filename = [[doc.fileURL lastPathComponent].stringByDeletingPathExtension stringByAppendingPathExtension:HW_MIDI_EXTENSION];
            [mailVC addAttachmentData:data mimeType:@"audio/midi" fileName:filename];
        }
        
        [mailVC setMailComposeDelegate:strongSelf];
        [strongSelf presentViewController:mailVC animated:YES completion:nil];
    }];
}

- (void)shareSongLilypondScriptByMail {
    if (![MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    BOOL plural = (self.selectedEntries.count > 1);
    [mailVC setSubject:(plural ? LinguiLocalizedString(@"Shared HarmonyWiz Songs", @"Many songs shared mail subject") : LinguiLocalizedString(@"Shared HarmonyWiz Song", @"One song shared mail subject"))];
    [mailVC setMessageBody:[(plural ? LinguiLocalizedString(@"I would like to share these HarmonyWiz songs with you.", @"Like to share many songs mail body") : LinguiLocalizedString(@"I would like to share this HarmonyWiz song with you.", @"Like to share one song mail body")) : @"\n" : LinguiLocalizedString(@"Open .ly files with Lilypond (http://www.lilypond.org) to convert them into scores.", @"Open LilyPond files mail body")] isHTML:NO];
    
    __weak typeof(self) weakSelf = self;
    [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
        __strong typeof(self) strongSelf = weakSelf;
        for (HWDocument *doc in result) {
            NSString *lilypond = [doc.song lilypondScript];
            NSData *data = [lilypond dataUsingEncoding:NSUTF8StringEncoding];
            NSString *filename = [[doc.fileURL lastPathComponent].stringByDeletingPathExtension stringByAppendingPathExtension:HW_LILYPOND_EXTENSION];
            [mailVC addAttachmentData:data mimeType:@"text/x-lilypond" fileName:filename];
        }
        
        [mailVC setMailComposeDelegate:strongSelf];
        [strongSelf presentViewController:mailVC animated:YES completion:nil];
    }];
}


- (void)shareSongMusicXMLByMail {
    if (![MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    BOOL plural = (self.selectedEntries.count > 1);
    [mailVC setSubject:(plural ? LinguiLocalizedString(@"Shared HarmonyWiz Songs", @"Many songs shared mail subject") : LinguiLocalizedString(@"Shared HarmonyWiz Song", @"One song shared mail subject"))];
    [mailVC setMessageBody:[(plural ? LinguiLocalizedString(@"I would like to share these HarmonyWiz songs with you.", @"Like to share many songs mail body") : LinguiLocalizedString(@"I would like to share this HarmonyWiz song with you.", @"Like to share one song mail body")) : @"\n" : LinguiLocalizedString(@"You can open .mxl files in Finale, as well as several other popular music programs.", @"Open MusicXML files mail body")] isHTML:NO];
    
    __weak typeof(self) weakSelf = self;
    [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
        __strong typeof(self) strongSelf = weakSelf;
        for (HWDocument *doc in result) {
            NSData *mxl_data = [doc.song MXL_data:doc.fileURL.lastPathComponent.stringByDeletingPathExtension];
            NSString *filename = [[doc.fileURL lastPathComponent].stringByDeletingPathExtension stringByAppendingPathExtension:HW_MUSICXML_EXTENSION];
            [mailVC addAttachmentData:mxl_data mimeType:@"application/vnd.recordare.musicxml" fileName:filename];
        }
        
        [mailVC setMailComposeDelegate:strongSelf];
        [strongSelf presentViewController:mailVC animated:YES completion:nil];
    }];
}

- (void)postToSoundCloud:(NSURL*)trackURL {
    SCShareViewController *shareViewController;
    shareViewController = [SCShareViewController shareViewControllerWithFileURL:trackURL
                                                              completionHandler:^(NSDictionary *trackInfo, NSError *error) {
                                                                  if (SC_CANCELED(error)) {
                                                                      NSLog(@"Canceled!");
                                                                  } else if (error) {
                                                                      NSLog(@"Ooops, something went wrong: %@", [error localizedDescription]);
                                                                  } else {
                                                                      // If you want to do something with the uploaded
                                                                      // track this is the right place for that.
                                                                      NSLog(@"Uploaded track: %@", trackInfo);
                                                                  }
                                                                  clearAifFilesFromTempDir();
                                                              }];
    
    // If your app is a registered foursquare app, you can set the client id and secret.
    // The user will then see a place picker where a location can be selected.
    // If you don't set them, the user sees a plain plain text filed for the place.
//    [shareViewController setFoursquareClientID:@"<foursquare client id>"
//                                  clientSecret:@"<foursquare client secret>"];
    
    // We can preset the title ...
    [shareViewController setTitle:LinguiLocalizedString(@"My HarmonyWiz Song", @"Share by SoundCloud title")];
    
    // ... and other options like the private flag.
    [shareViewController setPrivate:NO];
    
    // Now present the share view controller.
    [self presentViewController:shareViewController animated:YES completion:nil];
}

//- (void)NYIAlert {
//    [[[UIAlertView alloc] initWithTitle:@"Not Yet Implemented" message:@"Sorry this feature is not done yet." delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
//}

- (NSString*)getUniqueFilenameFrom:(NSString*)filename {
    NSArray * localDocuments = [self localHWDocs];
    NSInteger count = 0;
    NSString *testFilename = filename;
    BOOL unique;
    do {
        unique = YES;
        for (NSURL *url in localDocuments) {
            NSString *theName = url.lastPathComponent.stringByDeletingPathExtension;
            if ([theName isEqualToString:testFilename]) {
                unique = NO;
                count++;
                testFilename = [filename : @" " : @(count).stringValue];
                break;
            }
        }
    } while (! unique);
    
    return testFilename;
}


- (void)importDocuments {
    NSError *error;
    NSArray *docsDir = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString documentsDirectory] error:&error];
    NSArray *savesDir = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString saveFilesDirectory] error:&error];
    NSMutableArray *paths = [NSMutableArray array];
    for (NSString *filename in docsDir) {
        NSString *unzippedFilename = [filename.stringByDeletingPathExtension stringByAppendingPathExtension:HW_EXTENSION];
        if (([filename.pathExtension isEqualToString:HW_GZIPPED_EXTENSION] || [filename.pathExtension isEqualToString:HW_MIDI_EXTENSION] || [filename.pathExtension isEqualToString:HW_LONG_MIDI_EXTENSION]) && ![savesDir containsObject:unzippedFilename]) {
            NSString *path = [[NSString documentsDirectory] stringByAppendingPathComponent:filename];
            [paths addObject:path];
            NSLog(@"Will try to import %@", path.lastPathComponent);
        }
    }
    if (paths.count > 0) {
        [self importDocumentsWithPaths:paths];
    }
}

- (void)importDocumentsWithPaths:(NSArray*)paths {
    MBProgressHUD *importHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    importHUD.mode = MBProgressHUDModeIndeterminate;
    importHUD.labelText = LinguiLocalizedString(@"Importing documents", @"Importing documents label text");
    importHUD.removeFromSuperViewOnHide = YES;
    [self.view.window addSubview:importHUD];
    __weak typeof(self) weakSelf = self;
    [importHUD showAnimated:YES whileExecutingBlock:^{
        @autoreleasepool {
            for (NSString *path in paths) {
                if ([path.pathExtension isEqualToString:HW_GZIPPED_EXTENSION]) {
                    NSData *data = [NSData dataWithContentsOfFile:path];
                    data = [[data oneTimePad:HWOTPKey] gunzippedData];
                    if (data == nil) {
                        NSLog(@"Not able to unzip data");
                        [self showNotAbleToUnzipDocument];
                        return;
                    }
                    
                    NSDictionary *documentData = [HarmonyWizUnarchiver unarchiveObjectWithData:data];
                    HarmonyWizSong *song = [documentData objectForKey:@"song"];
                    HWMetadata *metadata = [documentData objectForKey:@"metadata"];
                    NSString *filename = [documentData objectForKey:@"filename"];
                    
                    // Determine a unique filename to create
                    filename = [self getUniqueFilenameFrom:filename];
                    NSURL * fileURL = [self getDocURL:[self getDocFilename:filename uniqueInObjects:YES]];
                    NSLog(@"Want to create file at %@", fileURL);
                    
                    // Create new document and save to the filename
                    HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
                    doc.song = song;
                    doc.metadata = metadata;
                    
                    [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
                        if (!success) {
                            NSLog(@"Failed to create file at %@", fileURL);
                            return;
                        }
                        
                        NSLog(@"File created at %@", fileURL);
                        HWMetadata * metadata = doc.metadata;
                        NSURL * fileURL = doc.fileURL;
                        UIDocumentState state = doc.documentState;
                        NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            __strong typeof(self) strongSelf = weakSelf;
                            [strongSelf addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
                        });
                    }];
                }
                else if ([path.pathExtension hasPrefix:HW_MIDI_EXTENSION]) {
                    // Determine a unique filename to create
                    NSString *filename = [self getUniqueFilenameFrom:path.lastPathComponent.stringByDeletingPathExtension];
                    NSURL * fileURL = [self getDocURL:[self getDocFilename:filename uniqueInObjects:YES]];
                    NSLog(@"Want to create file at %@", fileURL);
                    
                    // Create new document and save to the filename
                    HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
                    
                    // remove all music tracks
                    while (doc.song.musicTracks.count > 0) {
                        [doc.song removeMusicTrack:doc.song.musicTracks.lastObject];
                    }
                    
                    // don't start in magic mode
                    doc.metadata.magicMode = NO;
                    
                    // import MIDI file
                    BOOL midiSuccess = [doc.song importFromMIDIWithURL:[NSURL fileURLWithPath:path]];
                    
                    if (midiSuccess) {
                        // save file
                        [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
                            if (!success) {
                                NSLog(@"Failed to create file at %@", fileURL);
                                return;
                            }
                            
                            NSLog(@"File created at %@", fileURL);
                            HWMetadata * metadata = doc.metadata;
                            NSURL * fileURL = doc.fileURL;
                            UIDocumentState state = doc.documentState;
                            NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                __strong typeof(self) strongSelf = weakSelf;
                                [strongSelf addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
                            });
                        }];
                    }
                    else {
                        [doc.song setupSATB];
                    }
                }
            }
        }
    } completionBlock:^{
        
        // remove all source files that were imported
        NSError *error;
        for (NSString *path in paths) {
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
            if (error) {
                NSLog(@"%@", error.localizedDescription);
                recordError(error)
            }
        }
    }];
}

- (void)openAnySong:(NSNotification*)note {
    NSArray *docs = [self localHWDocs];
    if (docs.count > 0) {
        [self openSongForFileURL:docs.firstObject];
    }
    else {
        [self insertNewSongWithName:@"My First Song"];
    }
}

- (void)openDocumentFromURL:(NSNotification*)note {
    NSURL *url = [note.userInfo objectForKey:@"url"];
    
    UIWindow *window = nil;
    if (self.songVC) {
        window = self.songVC.view.window;
    }
    else {
        window = self.view.window;
    }
    
    MBProgressHUD *openHUD = [[MBProgressHUD alloc] initWithWindow:window];
    openHUD.mode = MBProgressHUDModeIndeterminate;
    openHUD.labelText = LinguiLocalizedString(@"Opening document", @"Opening document label text");
    openHUD.removeFromSuperViewOnHide = YES;
    [window addSubview:openHUD];
    __weak typeof(self) weakSelf = self;
    [openHUD showAnimated:YES whileExecutingBlock:^{
        @autoreleasepool {
            __strong typeof(self) strongSelf = weakSelf;
            if ([url.pathExtension isEqualToString:HW_GZIPPED_EXTENSION]) {
                [strongSelf importHWSongWithURL:url];
            }
            else if ([url.pathExtension hasPrefix:HW_MIDI_EXTENSION]) {
                [strongSelf importMidiFileWithURL:url];
            }
        }
    }];
}

// import MIDI file code.

#define CheckResult(result) if ((result) != noErr) { [NSException raise:@"HarmonyWiz" format:@"result = %ld", (result)]; }

- (BOOL)hasCurrentEvent:(MusicEventIterator)iterator {
	Boolean hasCurrent = false;
	OSStatus result = MusicEventIteratorHasCurrentEvent(iterator, &hasCurrent);
    CheckResult(result)
    
	return (BOOL) hasCurrent;
}

- (void)updateZoomSourceRectFromDocument:(HWDocument*)doc {
    NSUInteger index = [self indexOfEntryWithFileURL:doc.fileURL];
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
    UIImageView *imageView = (id)[cell viewWithTag:100];
    self.zoomTransitionSourceRect = [imageView convertRect:imageView.bounds toView:self.view];
}

- (void)importMidiFileWithURL:(NSURL*)url {
    NSString *filename = url.lastPathComponent.stringByDeletingPathExtension;
    
    // Determine a unique filename to create
    NSURL * fileURL = [self getDocURL:[self getDocFilename:filename uniqueInObjects:YES]];
    
    // Create new document and save to the filename
    HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
    
    // remove all music tracks
    while (doc.song.musicTracks.count > 0) {
        [doc.song removeMusicTrack:doc.song.musicTracks.lastObject];
    }
    
    // don't start in magic mode
    doc.metadata.magicMode = NO;
    
    // create the song
    BOOL midiSuccess = [doc.song importFromMIDIWithURL:url];
    
    if (midiSuccess) {
        __weak typeof(self) weakSelf = self;
        [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            
            if (!success) {
                NSLog(@"Failed to create file at %@", fileURL);
                return;
            }
            
            NSLog(@"File created at %@", fileURL);
            HWMetadata * metadata = doc.metadata;
            NSURL * fileURL = doc.fileURL;
            UIDocumentState state = doc.documentState;
            NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
            
            // Add on the main thread and perform the segue
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong typeof(self) strongSelf = weakSelf;
                strongSelf.collectionView.contentOffset = CGPointZero;
                [strongSelf addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
                if (strongSelf.songVC) {
                    [[[UIAlertView alloc] initWithTitle:@"Opened MIDI File" message:[NSString stringWithFormat:LinguiLocalizedStringForBlocks(@"New song has been saved as \'%@\'", @"New song saved as alert message"), filename] delegate:nil cancelButtonTitle:LinguiLocalizedStringForBlocks(@"OK", @"OK button") otherButtonTitles:nil] show];
                    
                    [strongSelf updateZoomSourceRectFromDocument:_selDocument];
                }
                else {
                    _selDocument = doc;
                    [strongSelf performSegueWithIdentifier:@"openSong" sender:self];
                }
            });
        }];
    }
    else {
        [doc.song setupSATB];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"MIDI File Error" message:[NSString stringWithFormat:@"Sorry, I could not open the MIDI file \"%@\". This may not be a valid MIDI file.", url.lastPathComponent] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        });
    }
}

- (void)showNotAbleToUnzipDocument {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:@"Document file not valid" message:@"The document file does not appear to be a valid HarmonyWiz document" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    });
}

- (void)importHWSongWithURL:(NSURL*)url {
    
    NSError *error;
    NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error];
    if (error != nil) {
        NSLog(@"%@", error);
        recordError(error)
        return;
    }
    data = [[data oneTimePad:HWOTPKey] gunzippedData];
    if (data == nil) {
        NSLog(@"Not able to unzip data");
        [self showNotAbleToUnzipDocument];
        return;
    }
    
    NSDictionary *documentData = [HarmonyWizUnarchiver unarchiveObjectWithData:data];
    HarmonyWizSong *song = [documentData objectForKey:@"song"];
    HWMetadata *metadata = [documentData objectForKey:@"metadata"];
    NSString *filename = [documentData objectForKey:@"filename"];
    
    // Determine a unique filename to create
    filename = [self getUniqueFilenameFrom:filename];
    NSURL * fileURL = [self getDocURL:[self getDocFilename:filename uniqueInObjects:YES]];
    NSLog(@"Want to create file at %@", fileURL);
    
    // Create new document and save to the filename
    HWDocument * doc = [[HWDocument alloc] initWithFileURL:fileURL];
    doc.song = song;
    doc.metadata = metadata;
    
    __weak typeof(self) weakSelf = self;
    [doc saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
        
        NSError *error = nil;
        
        // remove file from Inbox folder
        [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            recordError(error)
        }
        
        if (!success) {
            NSLog(@"Failed to create file at %@", fileURL);
            return;
        }
        
        NSLog(@"File created at %@", fileURL);
        HWMetadata * metadata = doc.metadata;
        NSURL * fileURL = doc.fileURL;
        UIDocumentState state = doc.documentState;
        NSFileVersion * version = [NSFileVersion currentVersionOfItemAtURL:fileURL];
        
        // Add on the main thread and perform the segue
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf addOrUpdateEntryWithURL:fileURL metadata:metadata state:state version:version];
            if (strongSelf.songVC) {
                [[[UIAlertView alloc] initWithTitle:LinguiLocalizedStringForBlocks(@"Opened song", @"Opened song alert title") message:[NSString stringWithFormat:LinguiLocalizedStringForBlocks(@"New song has been saved as \'%@\'", @"New song saved as alert message"), filename] delegate:nil cancelButtonTitle:LinguiLocalizedStringForBlocks(@"OK", @"OK button") otherButtonTitles:nil] show];
                
                [strongSelf updateZoomSourceRectFromDocument:_selDocument];
            }
            else {
                _selDocument = doc;
                [strongSelf performSegueWithIdentifier:@"openSong" sender:self];
            }
        });
    }];
}

- (void)showSavedAlert {
    [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Saved", @"Saved alert title") message:LinguiLocalizedString(@"Your files have been saved to the documents directory and can be accessed from iTunes.", @"Saved alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
}

- (void)showTooManySelectedAlert:(NSString*)message {
    [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Too many selected", @"Too many selected songs alert title") message:message delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
}

- (void)shareSongWithMethod:(NSString*)method indexPath:(NSIndexPath*)indexPath {
    if (self.visiblePopover) {
        [self.visiblePopover dismissPopoverAnimated:YES];
        self.visiblePopover = nil;
    }
    
    [HWAppDelegate recordEvent:@"share_songs_from_select_view" segmentation:@{ @"method" : method, @"section" : @(indexPath.section), @"row" : @(indexPath.row), @"indexPath" : @[@(indexPath.section),@(indexPath.row)], @"count" : @(self.selectedEntries.count) }];
    
    enum ShareVia shareVia = (enum ShareVia)[indexPath section];
    
    if (ShareMethodIsSoundCloud(indexPath)) {
        if (self.selectedEntries.count == 1) {
            __weak typeof(self) weakSelf = self;
            [self bounceSelectedDocuments:^(NSArray* paths){
                __strong typeof(self) strongSelf = weakSelf;
                [strongSelf postToSoundCloud:[NSURL fileURLWithPath:paths.lastObject]];
            }];
        }
        else {
            [self showTooManySelectedAlert:LinguiLocalizedString(@"You may only post one song at a time to SoundCloud.", @"Too many songs selected for SoundCloud alert message")];
        }
    }
    else if (AudioShareAsAudio(indexPath)) {
        if (self.selectedEntries.count == 1) {
            [self bounceSelectedDocuments:^(NSArray* paths){
                NSString *name = [[paths.firstObject lastPathComponent] stringByDeletingPathExtension];
                [[AudioShare sharedInstance] addSoundFromURL:[NSURL fileURLWithPath:paths.firstObject] withName:name];
            }];
        }
        else {
            [self showTooManySelectedAlert:@"You may only share one song at a time."];
        }
    }
    else if (AudioShareAsMIDI(indexPath)) {
        if (self.selectedEntries.count == 1) {
            [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
                for (HWDocument *doc in result) {
                    HarmonyWizSong *song = doc.song;
                    NSData *data = [song convertToMIDIData];
                    NSString *filename = [[doc.fileURL lastPathComponent] stringByDeletingPathExtension];
                    [[AudioShare sharedInstance] addSoundFromData:data withName:filename];
                }
            }];
        }
        else {
            [self showTooManySelectedAlert:@"You may only share one song at a time."];
        }
    }
    else if (EMailAudioFile(indexPath) || SaveAudioFile(indexPath)) {
        __weak typeof(self) weakSelf = self;
        [self bounceSelectedDocuments:^(NSArray* paths){
            __strong typeof(self) strongSelf = weakSelf;
            if (shareVia == kShareVia_Email) {
                [strongSelf mailBouncedFiles:paths];
            }
            else if (shareVia == kShareVia_iTunes) {
                copyFilesToDocumentsDirectory(paths);
                [strongSelf showSavedAlert];
            }
        }];
    }
    else if (EMailSongFile(indexPath) || SaveSongFile(indexPath)) {
        if (shareVia == kShareVia_Email) {
            [self shareSongDataByMail];
        }
        else if (shareVia == kShareVia_iTunes) {
            __weak typeof(self) weakSelf = self;
            [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
                __strong typeof(self) strongSelf = weakSelf;
                MBProgressHUD *shareHUD = [[MBProgressHUD alloc] initWithWindow:strongSelf.view.window];
                shareHUD.mode = MBProgressHUDModeIndeterminate;
                shareHUD.labelText = LinguiLocalizedString(@"Preparing songs", @"Preparing songs label text");
                shareHUD.removeFromSuperViewOnHide = YES;
                [strongSelf.view.window addSubview:shareHUD];
                [shareHUD showAnimated:YES whileExecutingBlock:^{
                    @autoreleasepool {
                        __strong typeof(self) strongSelf = weakSelf;
                        [strongSelf shareSongViaITunes:result];
                    }
                } completionBlock:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    [strongSelf showSavedAlert];
                }];
            }];
        }
    }
    else if (EMailLilypondFile(indexPath) || SaveLilypondFile(indexPath)) {
        if (shareVia == kShareVia_Email) {
            [self shareSongLilypondScriptByMail];
        }
        else if (shareVia == kShareVia_iTunes) {
            __weak typeof(self) weakSelf = self;
            [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
                __strong typeof(self) strongSelf = weakSelf;
                for (HWDocument *doc in result) {
                    NSString *filename = [[[doc.fileURL lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_LILYPOND_EXTENSION];
                    NSString *lilypond = [doc.song lilypondScript];
                    NSString *path = [[NSString documentsDirectory] stringByAppendingPathComponent:filename];
                    NSError *error = nil;
                    [lilypond writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
                    if (error) {
                        NSLog(@"%@", error);
                        recordError(error)
                        return;
                    }
                }
                [strongSelf showSavedAlert];
            }];
        }
    }
    else if (EMailMusicXMLFile(indexPath) || SaveMusicXMLFile(indexPath)) {
        if (shareVia == kShareVia_Email) {
            [self shareSongMusicXMLByMail];
        }
        else if (shareVia == kShareVia_iTunes) {
            __weak typeof(self) weakSelf = self;
            [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
                __strong typeof(self) strongSelf = weakSelf;
                for (HWDocument *doc in result) {
                    NSString *filename = [doc.fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:HW_MUSICXML_EXTENSION];
                    NSData *mxl_data = [doc.song MXL_data:doc.fileURL.lastPathComponent.stringByDeletingPathExtension];
                    NSString *path = [[NSString documentsDirectory] stringByAppendingPathComponent:filename];
                    BOOL success = [mxl_data writeToFile:path atomically:YES];
                    if (! success) {
                        recordError([NSError errorWithDomain:@"Failed to write MXL data" code:1 userInfo:nil])
                    }
                }
                [strongSelf showSavedAlert];
            }];
        }
    }
    else if (EMailMIDIFile(indexPath) || SaveMIDIFile(indexPath)) {
        if (shareVia == kShareVia_Email) {
            [self shareSongMIDIByMail];
        }
        else if (shareVia == kShareVia_iTunes) {
            __weak typeof(self) weakSelf = self;
            [self readDocumentsWithURLs:[self.selectedEntries valueForKey:@"fileURL"] withCompletion:^(NSArray *result) {
                __strong typeof(self) strongSelf = weakSelf;
                for (HWDocument *doc in result) {
                    HarmonyWizSong *song = doc.song;
                    NSData *data = [song convertToMIDIData];
                    NSString *filename = [[[doc.fileURL lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_MIDI_EXTENSION];
                    NSString *path = [[NSString documentsDirectory] stringByAppendingPathComponent:filename];
                    [data writeToFile:path atomically:YES];
                }
                [strongSelf showSavedAlert];
            }];
        }
    }
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField
{
    _activeTextField = textField;
}

- (IBAction)textFieldDidEndEditing:(UITextField *)textField
{
    _activeTextField = nil;
}

- (void)textChanged:(UITextField *)textField {
    UIView * view = textField.superview;
    while( ![view isKindOfClass: [UICollectionViewCell class]]){
        view = view.superview;
    }
    UICollectionViewCell *cell = (UICollectionViewCell *) view;
    NSIndexPath * indexPath = [self.collectionView indexPathForCell:cell];
    HWEntry * entry = [_objects objectAtIndex:indexPath.row];
    NSLog(@"Want to rename %@ to %@", entry.description, textField.text);
    [self renameEntry:entry to:textField.text];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
    [self textChanged:textField];
	return YES;
}

#pragma mark - global open document

static HWDocument * currentlyOpenDocumentData = nil;

+ (HWDocument*)currentlyOpenDocument {
    return currentlyOpenDocumentData;
}

#pragma mark - MFMailComposeViewControllerDelegate

void clearAifFilesFromTempDir() {
    NSError *myerror;
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:NSTemporaryDirectory()];
    
    for (NSString *path in enumerator) {
        if ([path hasSuffix:@"aif"] || [path hasSuffix:@"aiff"]) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSTemporaryDirectory() stringByAppendingPathComponent:path] error:&myerror];
            if (myerror) {
                NSLog(@"%@", myerror);
            }
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    if (error) {
        NSLog(@"%@", error);
        recordError(error)
    }
    
    // clear out temp directory
    clearAifFilesFromTempDir();
    
    // dismiss controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == self.nameAlert) {
        NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
        if ([buttonTitle isEqualToString:@"OK"]) {
            NSString *name = [[self.nameAlert textFieldAtIndex:0] text];
            if ([name length] > 0) {
                [self insertNewSongWithName:name];
            }
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView == self.nameAlert) {
        self.nameAlert = nil;
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    if (alertView == self.nameAlert) {
        return ([[[alertView textFieldAtIndex:0] text] length] > 0);
    }
    return YES;
}

#pragma mark - popover controller delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.visiblePopover = nil;
}

@end
