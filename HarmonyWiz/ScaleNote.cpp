//
//  ScaleNote.cpp
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#include <iostream>
#include "ScaleNote.h"

extern "C" int16_t roundDownChromatic(int16_t chromatic, int16_t key, bool isMinor);
extern "C" int16_t roundUpChromatic(int16_t chromatic, int16_t key, bool isMinor);

using namespace HarmonyWiz;
using namespace std;

ostream& operator<<(ostream& stream, const ScaleNote& note) {
    stream << note.note;
    if (note.isSharpened())
        stream << "#";
    if (note.isFlattened())
        stream << "♭";
    return stream;
}

int16_t roundDownChromatic(int16_t chromatic, int16_t key, bool isMinor) {
    ScaleNote sn(chromatic, key, isMinor);
    return sn.roundDown(false).chromatic(key, isMinor);
}

int16_t roundUpChromatic(int16_t chromatic, int16_t key, bool isMinor) {
    ScaleNote sn(chromatic, key, isMinor);
    return sn.roundUp(false).chromatic(key, isMinor);
}
