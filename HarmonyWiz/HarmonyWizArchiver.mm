//
//  HarmonyWizArchiver.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizArchiver.h"
#import "HarmonyWizUnarchiver.h"
#import "HWPainter.h"
#include "ScaleNote.h"

using namespace HarmonyWiz;

@implementation HarmonyWizArchiver

- (void)encodeValueOfObjCType:(const char *)type at:(const void *)addr {
    if (strcmp(type, @encode(ScaleNote)) == 0) {
        ScaleNote sn;
        memcpy(&sn, addr, sizeof(ScaleNote));
        [self encodeValueOfObjCType:@encode(int16_t) at:&sn.note];
        [self encodeValueOfObjCType:@encode(ScaleNote::ScaleNoteAlteration) at:&sn.alteration];
    }
    else if (strcmp(type, @encode(struct HWPoint)) == 0) {
        struct HWPoint point;
        memcpy(&point, addr, sizeof(struct HWPoint));
        [self encodeValueOfObjCType:@encode(float) at:&point.x];
        [self encodeValueOfObjCType:@encode(float) at:&point.y];
    }
    else if (strcmp(type, @encode(struct Painter)) == 0) {
        struct Painter painter;
        memcpy(&painter, addr, sizeof(struct Painter));
        [self encodeValueOfObjCType:@encode(float) at:&painter.dx];
        [self encodeValueOfObjCType:@encode(float) at:&painter.dy];
        [self encodeValueOfObjCType:@encode(float) at:&painter.ax];
        [self encodeValueOfObjCType:@encode(float) at:&painter.ay];
        [self encodeValueOfObjCType:@encode(float) at:&painter.div];
        [self encodeValueOfObjCType:@encode(float) at:&painter.ease];
        for (int i = 0; i < 5; i++) {
            [self encodeValueOfObjCType:@encode(float) at:&painter.pts[i].x];
            [self encodeValueOfObjCType:@encode(float) at:&painter.pts[i].y];
        }
        for (int i = 0; i < 5; i++) {
            [self encodeValueOfObjCType:@encode(float) at:&painter.as[i].x];
            [self encodeValueOfObjCType:@encode(float) at:&painter.as[i].y];
        }
        {
            [self encodeValueOfObjCType:@encode(float) at:&(painter.peturb.x)];
            [self encodeValueOfObjCType:@encode(float) at:&(painter.peturb.y)];
        }
        [self encodeValueOfObjCType:@encode(ushort) at:&painter.ctr];
    }
    else {
        [super encodeValueOfObjCType:type at:addr];
    }
}

- (unsigned)systemVersion {
    // get version string
    NSString *harmonyWizVersion = [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    // convert version string to 32 byte int
    NSArray *components = [harmonyWizVersion componentsSeparatedByString:@"."];
    uint8_t n[4] = {0,0,0,0};
    for (NSUInteger idx = 0; idx < components.count; idx++) {
        n[idx] = static_cast<UInt8>([[components objectAtIndex:idx] intValue]);
    }
    int32_t result = 0;
    memcpy(&result, n, sizeof(result));
    
    return result;
}

@end
