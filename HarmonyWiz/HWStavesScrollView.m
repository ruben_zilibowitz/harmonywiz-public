//
//  HWStavesScrollView.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HWStavesScrollView.h"
#import "HarmonyWizEvent.h"
#import "AudioController.h"
#import "HarmonyWizUndoManager.h"
#import "HarmonyWizSong+Splice.h"
#import "NSMutableArray+Sorted.h"
#import "HarmonyWizMusicEvent+CPP.h"
#import "UIImage+Tint.h"
#import "HWDocument.h"
#import "HWCollectionViewController.h"
#import "HWColourScheme.h"
#import "HarmonyWizSong+AutoColour.h"
#import "UIView+Recursive/UIView+Recursive.h"
#import "HarmonyWizSong+Verify.h"

//#include "CAHostTimeBase.h"
#include <mach/mach_time.h>

NSString * const HarmonyWizClefMenuNotification = @"ClefMenu";
NSString * const HarmonyWizTimeSignatureMenuNotification = @"TimeSigMenu";
NSString * const HarmonyWizKeySignatureMenuNotification = @"KeySigMenu";
NSString * const HarmonyWizEditConstraintsUserInfoKey = @"Event";
NSString * const HarmonyWizEditConstraintsNotification = @"EditConstraints";
NSString * const HarmonyWizPlacedEventNotification = @"PlacedEvent";
NSString * const HWSelectTrackNotification = @"HWSelectTrackNumber";
NSString * const HWDraggedLocatorTriangle = @"HWDraggedLocatorTriangle";
NSString * const HWStartedStaveEventNotification = @"HWStartedStaveEventNotification";
NSString * const HWEndedStaveEventNotification = @"HWEndedStaveEventNotification";
NSString * const HWCancelledStaveEventNotification = @"HWCancelledStaveEventNotification";
NSString * const HWEraseNotesFinishedNotification = @"HWEraseNotesFinishedNotification";

extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;
extern CGFloat const HarmonyWizStaveVerticalScale;

extern CGFloat const HWScreenWidth;

CFTimeInterval const HWBrushFadeTime = 0.5;
CFTimeInterval const HWSelectBoxFadeTime = 0.15;
CGFloat const HWAutoScrollInset = 300;
CGFloat const HWBrushSize = 16;
CGFloat const HWEraserSize = 24;
CGFloat const HWBrushSpeed = 2.;
CGFloat const HWPositionIndicatorWidth = 2;
//CGFloat const kStaveOverlap = 50;
CGFloat const HWLocatorTriangleSize = 20.f;

#define kBrushLineWidth  4

@implementation HWLocatorTriangle
- (void)setup {
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self addGestureRecognizer:pan];
}
- (void)drawRect:(CGRect)rect {
    [[UIColor orangeColor] set];
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(self.bounds.size.width, 0)];
    [path addLineToPoint:CGPointMake(0.5f*self.bounds.size.width, self.bounds.size.height)];
    [path closePath];
    [path fill];
}
- (void)pan:(UIPanGestureRecognizer*)gest {
    CGPoint loc = [gest locationInView:self.superview];
    [[NSNotificationCenter defaultCenter] postNotificationName:HWDraggedLocatorTriangle object:self userInfo:@{@"loc" : [NSValue valueWithCGPoint:loc]}];
}
@end

@interface UITouch (Stave)
- (CGPoint)locationInStave:(HarmonyWizStaveView*)stave;
- (CGPoint)previousLocationInStave:(HarmonyWizStaveView*)stave;
@end

@implementation UITouch (Stave)
- (CGPoint)locationInStave:(HarmonyWizStaveView*)stave {
    NSAssert(stave.parentScrollView != nil, @"no container view");
    NSAssert(isfinite(stave.touchableFrame.origin.x) && isfinite(stave.touchableFrame.origin.y), @"non finite touchable frame");
    CGPoint pt = [self locationInView:stave.parentScrollView.contentView];
    pt.x -= stave.touchableFrame.origin.x;
    pt.y -= stave.touchableFrame.origin.y;
    return pt;
}
- (CGPoint)previousLocationInStave:(HarmonyWizStaveView*)stave {
    NSAssert(stave.parentScrollView != nil, @"no container view");
    NSAssert(isfinite(stave.touchableFrame.origin.x) && isfinite(stave.touchableFrame.origin.y), @"non finite touchable frame");
    CGPoint pt = [self previousLocationInView:stave.parentScrollView.contentView];
    pt.x -= stave.touchableFrame.origin.x;
    pt.y -= stave.touchableFrame.origin.y;
    return pt;
}
@end

@interface HWBrushView : UIView
@end
@implementation HWBrushView
+ (Class)layerClass {
    return [CAShapeLayer class];
}
@end

@interface HWStavesScrollView ()
@property (nonatomic,strong) HarmonyWizStaveView *trackingStaveView;
@property (nonatomic,assign) CGFloat brushUpperLimit, brushLowerLimit;
@property (nonatomic,assign) CGPoint selectStart, selectEnd;
@property (nonatomic,strong) NSMutableArray *brushPoints, *brushTimestamps;
@property (nonatomic,assign) CGPoint pointNow, pointBefore;
@property (nonatomic,strong) HarmonyWizMusicEvent *touchedEvent;
@property (nonatomic,assign) CGPoint pencilLocation;
@property (nonatomic,assign) CGFloat lastStaveHeight;
@property (nonatomic,strong) UILongPressGestureRecognizer *longPress;
@property (nonatomic,strong) NSMutableArray *stavesNeedingUpdate;
@property (nonatomic,assign) BOOL needsStaveLayout;
@property (nonatomic,strong) UIView *penHorizontalPosition, *penVerticalPosition;
@property (nonatomic,strong) HWBrushView *brushView;
@end

static CGPoint midpoint(CGPoint p, CGPoint q);
static CGFloat length_squared(CGPoint p, CGPoint q);
static CGFloat distance(CGPoint p, CGPoint q);
static CGPoint minus(CGPoint p, CGPoint q);
static CGPoint plus(CGPoint p, CGPoint q);
static CGFloat dot(CGPoint p, CGPoint q);
static CGPoint scalarMultiply(CGFloat s, CGPoint p);
static CGFloat minimum_distance_sqr(CGPoint v, CGPoint w, CGPoint p);

@implementation HWStavesScrollView
{
    UIBezierPath *path;
    //    UIImage *incrementalImage;
    CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
    NSTimeInterval times[5];
    uint ctr;
}

- (void)endPath {
    //    [self drawBitmap];
    //    [self setNeedsDisplay];
    [path removeAllPoints];
    ctr = 0;
}

- (void)appendPointToPath:(CGPoint)p timestamp:(NSTimeInterval)t {
    ctr++;
    pts[ctr] = p;
    times[ctr] = t;
    if (self.brushTimestamps.count == 0) {
        [self.brushTimestamps addObject:@(t)];
    }
    if (ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
        [path moveToPoint:pts[0]];
        [path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]]; // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
        
        [self.brushTimestamps addObject:@(times[3])];
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[0]]];
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[1]]];
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[2]]];
        [self.brushPoints addObject:[NSValue valueWithCGPoint:pts[3]]];
        
        // replace points and get ready to handle the next segment
        pts[0] = pts[3];
        pts[1] = pts[4];
        ctr = 1;
    }
}

- (void)flushAllStaves {
    [self.musicStaveViews makeObjectsPerformSelector:@selector(flushStave)];
    [self.harmonyStaveView flushStave];
}

- (void)updateAllStavesNow {
    if ([HWCollectionViewController currentlyOpenDocument].song == nil)
        return;
    
    [self updateVisible:NO];
    [self.musicStaveViews makeObjectsPerformSelector:@selector(updateStave)];
    if (self.harmonyStaveView) {
        [self.harmonyStaveView updateStave];
    }
}

- (void)createAllStaveViews {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    // remove existing stave views
    if (self.musicStaveViews) {
        for (HarmonyWizStaveView *sv in self.musicStaveViews) {
            [sv removeAllViewsFromSuperview];
        }
        self.musicStaveViews = nil;
    }
    if (self.harmonyStaveView) {
        [self.harmonyStaveView removeAllViewsFromSuperview];
        self.harmonyStaveView = nil;
    }
    
    // music views
    NSMutableArray *views = [NSMutableArray arrayWithCapacity:doc.song.musicTracks.count];
    for (HarmonyWizMusicTrack *mt in doc.song.musicTracks) {
        if (!doc.metadata.magicMode || doc.metadata.currentlySelectedTrackNumber == mt.trackNumber) {
            HarmonyWizStaveView *sv = [[HarmonyWizStaveView alloc] init];
            sv.trackNumber = mt.trackNumber;
            sv.numberOfStaveLines = 5;
            sv.parentScrollView = self;
            [views addObject:sv];
        }
    }
    self.musicStaveViews = views;
    
    // harmony view
    if (!doc.metadata.magicMode) {
        self.harmonyStaveView = [[HarmonyWizStaveView alloc] init];
        self.harmonyStaveView.trackNumber = doc.song.harmony.trackNumber;
        self.harmonyStaveView.numberOfStaveLines = 1;
        self.harmonyStaveView.track.clef = kClef_Neutral;
        self.harmonyStaveView.parentScrollView = self;
    }
    
    [self updateTouchableFrames];
    [self updateAllStavesNow];
}

- (void)purgeQueues {
    [self.staveLineViewQueue removeAllObjects];
    [self.staveEventLabelQueue removeAllObjects];
    [self.staveTieQueue removeAllObjects];
    [self.staveHarmonyInfoLabelQueue removeAllObjects];
    [self.staveBarLineQueue removeAllObjects];
    [self.tailLabelQueue removeAllObjects];
    [self.rhythmNoteHeadQueue removeAllObjects];
    [self.legerLinesQueue removeAllObjects];
}

- (void)setContentSize:(CGSize)contentSize {
    self.contentView.frame = CGRectMake(0, 0, contentSize.width, contentSize.height);
    [super setContentSize:contentSize];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView = [[HWViewKeepingTop alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:self.contentView];
        self.contentSize = CGSizeMake(frame.size.width, frame.size.height);
        
        self.backgroundColor = [HWColourScheme paperColour];
        self.stavesNeedingUpdate = [NSMutableArray arrayWithCapacity:6];
        if (self.positionIndicator == nil) {
            self.positionIndicator = [[UIView alloc] initWithFrame:CGRectZero];
            self.positionIndicator.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.8f];
            self.positionIndicator.opaque = NO;
            self.positionIndicator.userInteractionEnabled = NO;
            self.positionIndicator.translatesAutoresizingMaskIntoConstraints = NO;
            [self.contentView addSubview:self.positionIndicator];
            
            self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
            self.longPress.delegate = self;
            [self addGestureRecognizer:self.longPress];
        }
        
        if (self.locatorTriangle == nil) {
            self.locatorTriangle = [[HWLocatorTriangle alloc] initWithFrame:CGRectMake(0, 0, HWLocatorTriangleSize, HWLocatorTriangleSize)];
            [self.locatorTriangle setup];
            HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
            self.locatorTriangle.hidden = (! doc.metadata.magicMode);
            self.locatorTriangle.translatesAutoresizingMaskIntoConstraints = NO;
            [self.contentView addSubview:self.locatorTriangle];
        }
        
        self.contentView.viewsOnTop = @[self.positionIndicator, self.locatorTriangle];
        
        [self createQueues];
    }
    return self;
}

- (void)createQueues {
    // allocate queues
    if (self.staveLineViewQueue == nil) {
        self.staveLineViewQueue = [NSMutableArray array];
    }
    if (self.singleStaveLineViewQueue == nil) {
        self.singleStaveLineViewQueue = [NSMutableArray array];
    }
    if (self.staveBarLineQueue == nil) {
        self.staveBarLineQueue = [NSMutableArray array];
    }
    if (self.staveEventLabelQueue == nil) {
        self.staveEventLabelQueue = [NSMutableArray array];
    }
    if (self.staveTieQueue == nil) {
        self.staveTieQueue = [NSMutableArray array];
    }
    if (self.staveHarmonyInfoLabelQueue == nil) {
        self.staveHarmonyInfoLabelQueue = [NSMutableArray array];
    }
    if (self.tailLabelQueue == nil) {
        self.tailLabelQueue = [NSMutableArray array];
    }
    if (self.rhythmNoteHeadQueue == nil) {
        self.rhythmNoteHeadQueue = [NSMutableArray array];
    }
    if (self.legerLinesQueue == nil) {
        self.legerLinesQueue = [NSMutableArray array];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setLocatorTriangleHidden:(BOOL)hidden {
    self.locatorTriangle.hidden = hidden;
}

- (void)updatePositionIndicator {
    if (self.positionIndicator) {
        CGRect frame = CGRectMake(self.rulerControl.playbackIndicatorXLoc+self.rulerControl.xOffset-0.5f*HWPositionIndicatorWidth, 0, HWPositionIndicatorWidth, self.contentSize.height);
        if (isfinite(frame.origin.x) && isfinite(frame.origin.y) && isfinite(frame.size.height) && isfinite(frame.size.width) && self.contentSize.height > 10 && self.contentSize.height < 10000) {
            self.positionIndicator.frame = frame;
        }
    }
    if (self.locatorTriangle) {
        CGPoint center = CGPointMake(self.positionIndicator.center.x, HWLocatorTriangleSize*0.5f);
        if (isfinite(center.x) && isfinite(center.y)) {
            self.locatorTriangle.center = center;
        }
    }
    if (self.rulerControl.locatorHeadView) {
        CGPoint center = CGPointMake(self.positionIndicator.center.x, self.rulerControl.bounds.size.height*0.5f);
        if (isfinite(center.x) && isfinite(center.y)) {
            self.rulerControl.locatorHeadView.center = center;
        }
    }
}

- (void)updateTouchableFrames {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const CGFloat xOffset = 8.f;
    const CGFloat staveWidth = self.contentSize.width - xOffset;
    if (doc.metadata.magicMode) {
        HarmonyWizStaveView *stave = [self.musicStaveViews objectAtIndex:0];
        stave.touchableFrame = CGRectMake(xOffset, 0, staveWidth, CGRectGetHeight(self.frame));
    }
    else {
        //        const CGFloat staveHeight = CGRectGetHeight(self.frame) / (self.musicStaveViews.count+1);
        const CGFloat staveHeight = HarmonyWizStaveVerticalScale * doc.metadata.notesPointSize;
        self.lastStaveHeight = staveHeight;
        for (NSUInteger idx = 0; idx < self.musicStaveViews.count; idx++) {
            HarmonyWizStaveView *stave = [self.musicStaveViews objectAtIndex:idx];
            stave.touchableFrame = CGRectMake(xOffset, idx * staveHeight, staveWidth, staveHeight);
        }
        self.harmonyStaveView.touchableFrame = CGRectMake(xOffset, self.musicStaveViews.count * staveHeight, staveWidth, staveHeight);
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    [self updateTouchableFrames];
    
    if (self.needsStaveLayout) {
        self.needsStaveLayout = NO;
        [self layoutAllStaves];
    }
    
    [self updatePositionIndicator];
    
}

- (SInt16)leftMostVisibleMeasure {
    return self.rulerControl.leftMostVisibleMeasure;
}

- (SInt16)rightMostVisibleMeasure {
    return self.rulerControl.rightMostVisibleMeasure;
}

/*
- (void)updateStaveVisible:(NSTimer*)timer {
    NSAssert(timer == self.updateStaveTimer, @"timer should be self.updateStaveTimer");
    NSMutableSet *staves = timer.userInfo;
    if (staves.count > 0) {
        HarmonyWizStaveView *sv = staves.anyObject;
        [sv removeUnusedEvents];
        [sv removeInvisibleEvents];
        [sv generateStaveEvents];
        [sv updateStaveLines];
        [sv generateBarLines];
        for (UIView *view in self.staveEventLabelQueue)
            [view removeFromSuperview];
        for (UIView *view in self.staveHarmonyInfoLabelQueue)
            [view removeFromSuperview];
        for (UIView *view in self.staveTieQueue)
            [view removeFromSuperview];
        [staves removeObject:sv];
    }
    if (staves.count == 0) {
        [timer invalidate];
        self.updateStaveTimer = nil;
        
        [self maintainQueueMaxSize];
    }
}
*/

/*
// stagger redrawing of staves
- (void)updateVisible:(BOOL)redraw {
    const NSTimeInterval HWRedrawDelay = 0.01;
    const BOOL needsRedraw = [self.rulerControl updateVisible:redraw];
    
    if (redraw && needsRedraw) {
        // timer should be nil but just make sure
        if (self.updateStaveTimer != nil) {
            [self.updateStaveTimer invalidate];
        }
        
        NSMutableSet *staves = [NSMutableSet setWithArray:self.musicStaveViews];
        if (self.harmonyStaveView) {
            [staves addObject:self.harmonyStaveView];
        }
        
        self.updateStaveTimer = [NSTimer timerWithTimeInterval:HWRedrawDelay target:self selector:@selector(updateStaveVisible:) userInfo:staves repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.updateStaveTimer forMode:NSRunLoopCommonModes];
    }
}
 */
- (void)updateStavesRecursively:(NSArray*)staves {
    if (staves.count == 0) {
        for (UIView *view in self.staveEventLabelQueue)
            [view removeFromSuperview];
        for (UIView *view in self.staveHarmonyInfoLabelQueue)
            [view removeFromSuperview];
        for (UIView *view in self.staveTieQueue)
            [view removeFromSuperview];
        [self maintainQueueMaxSize];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            HarmonyWizStaveView *sv = staves.firstObject;
            [sv removeUnusedEvents];
            [sv removeInvisibleEvents];
            [sv generateStaveEvents];
            [sv updateStaveLines];
            [sv generateBarLines];
//            [sv updateAllAccidentals];
            [self updateStavesRecursively:[staves subarrayWithRange:NSMakeRange(1, staves.count-1)]];
        });
    }
}

- (void)updateVisible:(BOOL)redraw {
    if ([HWCollectionViewController currentlyOpenDocument].song == nil)
        return;

    const BOOL needsRedraw = [self.rulerControl updateVisible:redraw];
    
    if (redraw && needsRedraw) {
        NSMutableArray *staves = [NSMutableArray arrayWithArray:self.musicStaveViews];
        if (self.harmonyStaveView) {
            [staves addObject:self.harmonyStaveView];
        }
        [self updateStavesRecursively:staves];
    }
}

/*
// redraw all staves in one go
- (void)updateVisible:(BOOL)redraw {
    const BOOL needsRedraw = [self.rulerControl updateVisible:redraw];
    
    if (redraw && needsRedraw) {
        NSMutableSet *staves = [NSMutableSet setWithArray:self.musicStaveViews];
        if (self.harmonyStaveView) {
            [staves addObject:self.harmonyStaveView];
        }
        for (HarmonyWizStaveView *sv in staves) {
            [sv removeUnusedEvents];
            [sv removeInvisibleEvents];
            [sv generateStaveEvents];
            [sv updateStaveLines];
            [sv generateBarLines];
        }
        for (UIView *view in self.staveEventLabelQueue)
            [view removeFromSuperview];
        for (UIView *view in self.staveHarmonyInfoLabelQueue)
            [view removeFromSuperview];
        for (UIView *view in self.staveTieQueue)
            [view removeFromSuperview];
        
        [self maintainQueueMaxSize];
    }
}
*/

- (void)maintainQueueMaxSize {
    NSUInteger const kMaxQueueSize = 256;
    NSUInteger const kMaxQueueSizeStaveEvents = 1024;
    
    if (self.staveLineViewQueue.count > kMaxQueueSize) {
        NSLog(@"self.staveLineViewQueue.count is too large at %lu", (unsigned long)self.staveLineViewQueue.count);
        [self.staveLineViewQueue removeObjectsInRange:NSMakeRange(0, self.staveLineViewQueue.count - kMaxQueueSize)];
    }
    
    if (self.staveEventLabelQueue.count > kMaxQueueSizeStaveEvents) {
        NSLog(@"self.staveEventLabelQueue.count is too large at %lu", (unsigned long)self.staveEventLabelQueue.count);
        [self.staveEventLabelQueue removeObjectsInRange:NSMakeRange(0, self.staveEventLabelQueue.count - kMaxQueueSize)];
    }
    
    if (self.staveTieQueue.count > kMaxQueueSize) {
        NSLog(@"self.staveTieQueue.count is too large at %lu", (unsigned long)self.staveTieQueue.count);
        [self.staveTieQueue removeObjectsInRange:NSMakeRange(0, self.staveTieQueue.count - kMaxQueueSize)];
    }
    
    if (self.staveHarmonyInfoLabelQueue.count > kMaxQueueSize) {
        NSLog(@"self.staveHarmonyInfoLabelQueue.count is too large at %lu", (unsigned long)self.staveHarmonyInfoLabelQueue.count);
        [self.staveHarmonyInfoLabelQueue removeObjectsInRange:NSMakeRange(0, self.staveHarmonyInfoLabelQueue.count - kMaxQueueSize)];
    }
    
    if (self.staveBarLineQueue.count > kMaxQueueSize) {
        NSLog(@"self.staveBarLineQueue.count is too large at %lu", (unsigned long)self.staveBarLineQueue.count);
        [self.staveBarLineQueue removeObjectsInRange:NSMakeRange(0, self.staveBarLineQueue.count - kMaxQueueSize)];
    }
    
    if (self.singleStaveLineViewQueue.count > kMaxQueueSize) {
        NSLog(@"self.singleStaveLineViewQueue.count is too large at %lu", (unsigned long)self.singleStaveLineViewQueue.count);
        [self.singleStaveLineViewQueue removeObjectsInRange:NSMakeRange(0, self.singleStaveLineViewQueue.count - kMaxQueueSize)];
    }
    
    if (self.tailLabelQueue.count > kMaxQueueSize) {
        NSLog(@"self.tailLabelQueue.count is too large at %lu", (unsigned long)self.tailLabelQueue.count);
        [self.tailLabelQueue removeObjectsInRange:NSMakeRange(0, self.tailLabelQueue.count - kMaxQueueSize)];
    }
    
    if (self.rhythmNoteHeadQueue.count > kMaxQueueSize) {
        NSLog(@"self.rhythmNoteHeadQueue.count is too large at %lu", (unsigned long)self.rhythmNoteHeadQueue.count);
        [self.rhythmNoteHeadQueue removeObjectsInRange:NSMakeRange(0, self.rhythmNoteHeadQueue.count - kMaxQueueSize)];
    }
    
    if (self.legerLinesQueue.count > kMaxQueueSize) {
        NSLog(@"self.legerLinesQueue.count is too large at %lu", (unsigned long)self.legerLinesQueue.count);
        [self.legerLinesQueue removeObjectsInRange:NSMakeRange(0, self.legerLinesQueue.count - kMaxQueueSize)];
    }
    
    NSAssert(self.staveLineViewQueue.count <= kMaxQueueSize, @"Queue too large");
    NSAssert(self.staveEventLabelQueue.count <= kMaxQueueSizeStaveEvents, @"Queue too large");
    NSAssert(self.staveTieQueue.count <= kMaxQueueSize, @"Queue too large");
    NSAssert(self.staveHarmonyInfoLabelQueue.count <= kMaxQueueSize, @"Queue too large");
    NSAssert(self.staveBarLineQueue.count <= kMaxQueueSize, @"Queue too large");
    NSAssert(self.singleStaveLineViewQueue.count <= kMaxQueueSize, @"Queue too large");
    NSAssert(self.tailLabelQueue.count <= kMaxQueueSize, @"Queue too large");
    NSAssert(self.rhythmNoteHeadQueue.count <= kMaxQueueSize, @"Queue too large");
    NSAssert(self.legerLinesQueue.count <= kMaxQueueSize, @"Queue too large");
}

- (void)layoutAllStaves {
    if ([HWCollectionViewController currentlyOpenDocument].song == nil)
        return;
    
    for (HarmonyWizStaveView *stave in self.musicStaveViews) {
        [stave layoutStaveViews];
    }
    [self.harmonyStaveView layoutStaveViews];
}

- (void)setNeedsLayoutStaves {
    self.needsStaveLayout = YES;
    [self setNeedsLayout];
}

- (HarmonyWizStaveView*)staveAtPoint:(CGPoint)loc {
    for (HarmonyWizStaveView *st in self.musicStaveViews) {
        if (CGRectContainsPoint(st.touchableFrame,loc)) {
            return st;
        }
    }
    if (CGRectContainsPoint(self.harmonyStaveView.touchableFrame,loc)) {
        return self.harmonyStaveView;
    }
    return nil;
}

- (CGFloat)brushSize {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.currentToolEvent && [doc.metadata.currentToolEvent isKindOfClass:[HarmonyWizRestEvent class]]) {
        UInt16 dur = [doc.song eventDuration:doc.metadata.currentToolEvent];
        return (HWBrushSize * dur) / doc.song.beatSubdivisions;
    }
    return HWBrushSize;
}

#pragma mark - tracking touches

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    // always make this the target view of any touches
    // or the locator triangle if it is visible
    if (! self.locatorTriangle.hidden) {
        CGPoint ltPt = [self.locatorTriangle convertPoint:point fromView:self];
        if ([self.locatorTriangle pointInside:ltPt withEvent:event]) {
            return self.locatorTriangle;
        }
    }
    if ([self pointInside:point withEvent:event]) {
        return self;
    }
    
    return [super hitTest:point withEvent:event];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        UITouch *touch = touches.anyObject;
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        
        {
            CGPoint loc = [touch locationInView:self];
            HarmonyWizStaveView *staveTouched = [self staveAtPoint:loc];
            // make sure ruler control is loaded in here
            [self rulerControl];
            
            if (staveTouched != nil) {
                CGPoint staveLoc = [touch locationInStave:staveTouched];
                self.trackingStaveView = staveTouched;
                
                staveTouched.createdEvent = nil;
                staveTouched.currentNoteStartDate = nil;
                staveTouched.touchStartDate = nil;
                
                // check if loc is on clef
                bool isClef = CGRectContainsPoint([(UILabel*)self.trackingStaveView.clefLabel frame], loc);
                
                // check if loc is on an event (note or rest)
                self.touchedEvent = [staveTouched closestEventToLoc:staveLoc];
                
                // respond to event
                NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
                
                // harmony event touched
                BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
                if ([self.touchedEvent isKindOfClass:[HarmonyWizHarmonyEvent class]] && expertMode && !doc.song.isModal) {
                    if (! [(HarmonyWizHarmonyEvent*)self.touchedEvent noChord]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizEditConstraintsNotification object:staveTouched userInfo:[NSDictionary dictionaryWithObject:self.touchedEvent forKey:HarmonyWizEditConstraintsUserInfoKey]];
                    }
                }
                
                else if (isClef) {
                    if (staveTouched.track.clef != kClef_Neutral && expertMode) {
                        [notifCenter postNotificationName:HarmonyWizClefMenuNotification object:staveTouched userInfo:@{ @"location" : [NSValue valueWithCGPoint:((UILabel*)staveTouched.clefLabel).center] }];
                    }
                }
                else if (self.touchedEvent) {
                    self.pointNow = [touch locationInView:self];
                    self.pointBefore = self.pointNow;
                    BOOL result = NO;
                    switch (doc.metadata.currentToolType) {
                        case kToolType_Hand:
                            self.isEditing = NO;
                            result = [self beginHandWithTouch:touch withEvent:event];
                            break;
                        case kToolType_Eraser:
                            result = [self beginEraserWithTouch:touch withEvent:event];
                            self.isEditing = result;
                            break;
                        case kToolType_Pen:
                            result = [self beginPencilWithTouch:touch withEvent:event];
                            self.isEditing = result;
                            break;
                        case kToolType_Select:
                            result = [self beginSelectWithTouch:touch withEvent:event];
                            self.isEditing = result;
                            break;
                        default:
                            break;
                    }
                    if (result) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:HWStartedStaveEventNotification object:self];
                    }
                }
                else {
                    [staveTouched updateStave];
                }
            }
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        UITouch *touch = touches.anyObject;
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        self.pointBefore = self.pointNow;
        self.pointNow = [touch locationInView:self];
        switch (doc.metadata.currentToolType) {
            case kToolType_Hand:
                [self continueHandWithTouch:touch withEvent:event];
                break;
            case kToolType_Eraser:
                [self continueEraserWithTouch:touch withEvent:event];
                break;
            case kToolType_Pen:
                [self continuePencilWithTouch:touch withEvent:event];
                break;
            case kToolType_Select:
                [self continueSelectWithTouch:touch withEvent:event];
                break;
            default:
                break;
        }
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        switch (doc.metadata.currentToolType) {
            case kToolType_Hand:
                [self cancelHandWithEvent:event];
                break;
            case kToolType_Eraser:
                [self cancelEraserWithEvent:event];
                break;
            case kToolType_Pen:
                [self cancelPencilWithEvent:event];
                break;
            case kToolType_Select:
                [self cancelSelectWithEvent:event];
                break;
            default:
                break;
        }
        self.isEditing = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:HWCancelledStaveEventNotification object:self];
//        self.userInteractionEnabled = YES;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (touches.count == 1) {
        UITouch *touch = touches.anyObject;
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        switch (doc.metadata.currentToolType) {
            case kToolType_Hand:
                [self endHandWithTouch:touch withEvent:event];
                break;
            case kToolType_Eraser:
                [self endEraserWithTouch:touch withEvent:event];
                break;
            case kToolType_Pen:
                [self endPencilWithTouch:touch withEvent:event];
//                self.userInteractionEnabled = YES;
                break;
            case kToolType_Select:
                [self endSelectWithTouch:touch withEvent:event];
                break;
            default:
                break;
        }
        self.isEditing = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:HWEndedStaveEventNotification object:self];
        [doc.song checkAllEventsSorted];
    }
}

#pragma mark - eraser tool

- (BOOL)beginEraserWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    if (self.brushView == nil) {
//        self.userInteractionEnabled = NO;
        
        // create brush layer
        self.brushView = [HWBrushView new];
        self.brushView.backgroundColor = [UIColor clearColor];
        self.brushView.opaque = NO;
        ((CAShapeLayer*)self.brushView.layer).strokeColor = [HWColourScheme eraserColour].CGColor;
        ((CAShapeLayer*)self.brushView.layer).fillColor = nil;
        ((CAShapeLayer*)self.brushView.layer).lineWidth = HWEraserSize;
        self.brushView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.brushView];
        
        // initialise brush points
        path = [UIBezierPath bezierPath];
        [path setLineWidth:HWEraserSize];
        self.brushPoints = [NSMutableArray arrayWithCapacity:1];
        self.brushTimestamps = [NSMutableArray arrayWithObject:[NSNumber numberWithDouble:touch.timestamp]];
        
        // initialise brush points array
        ctr = 0;
        pts[0] = [touch locationInView:self];
        times[0] = touch.timestamp;
        
        return YES;
    }
    return NO;
}

- (BOOL)continueEraserWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint pt = [touch locationInView:self];
    [self appendPointToPath:pt timestamp:touch.timestamp];
    if (self.brushPoints.count > 0) {
        self.brushView.frame = CGRectInset(path.bounds, -HWEraserSize, -HWEraserSize);
        UIBezierPath *shiftedPath = path.copy;
        [shiftedPath applyTransform:CGAffineTransformMakeTranslation(-self.brushView.frame.origin.x, -self.brushView.frame.origin.y)];
        ((CAShapeLayer*)self.brushView.layer).path = shiftedPath.CGPath;
    }
    [self scrollForTouch:touch];
    
    return YES;
}

- (void)cancelEraserWithEvent:(UIEvent *)event {
    self.brushPoints = nil;
    self.brushTimestamps = nil;
    [self endPath];
    if (self.brushView) {
        [self.brushView removeFromSuperview];
        self.brushView = nil;
    }
}

- (CGPoint)convertPoint:(CGPoint)point fromStave:(HarmonyWizStaveView *)stave {
    point.x += stave.touchableFrame.origin.x;
    point.y += stave.touchableFrame.origin.y;
    return point;
}

- (CGRect)convertRect:(CGRect)rect fromStave:(HarmonyWizStaveView*)stave {
    rect.origin.x += stave.touchableFrame.origin.x;
    rect.origin.y += stave.touchableFrame.origin.y;
    return rect;
}

- (void)eraseNotes {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    for (HarmonyWizStaveView *stave in self.musicStaveViews) {
        
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        if (! expertMode && stave.trackNumber != 0) {
            continue;
        }
        
        // erase notes
        NSMutableArray *notes = [NSMutableArray arrayWithCapacity:1];
        
        for (HarmonyWizMusicEvent *event in stave.track.events) {
            if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                CGPoint notePt = [self convertPoint:[stave noteHeadLoc:event] fromStave:stave];
                
                for (NSUInteger brushIdx = 3; brushIdx < self.brushPoints.count; brushIdx += 4) {
                    CGPoint p1 = [[self.brushPoints objectAtIndex:brushIdx-3] CGPointValue];
                    CGPoint p2 = [[self.brushPoints objectAtIndex:brushIdx-2] CGPointValue];
                    CGPoint p3 = [[self.brushPoints objectAtIndex:brushIdx-1] CGPointValue];
                    CGPoint p4 = [[self.brushPoints objectAtIndex:brushIdx] CGPointValue];
                    
                    CGFloat t = bezierClosestPosition(notePt, p1, p2, p3, p4);
                    CGPoint bezierPt = CGPointMake(bezierInterpolation(t, p1.x, p2.x, p3.x, p4.x), bezierInterpolation(t, p1.y, p2.y, p3.y, p4.y));
                    CGFloat dist_sqr = length_squared(notePt, bezierPt);
                    if (dist_sqr < HWEraserSize*HWEraserSize*0.25f) {
                        [notes addObject:event];
                    }
                }
            }
        }
        
        // apply undoable changes to music
        {
            // remove them from track
            if (notes.count > 0) {
                [stave.track.events removeObjectsInArray:notes];
                [doc.song addFillerRestsTo:stave.track.events];
            }
        }
    }
    
    // splice harmony
    {
        doc.metadata.harmonyScore = nil;
        [doc.song spliceHarmony];
    }
}

- (void)endEraserWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint pt = [touch locationInView:self];
    
    // append final point
    [self appendPointToPath:pt timestamp:touch.timestamp];
    
    // erase notes in background
    MBProgressHUD *eraseHUD = [[MBProgressHUD alloc] initWithWindow:self.window];
    eraseHUD.mode = MBProgressHUDModeIndeterminate;
    eraseHUD.labelText = @"Erasing";
    eraseHUD.removeFromSuperViewOnHide = YES;
    [self.window addSubview:eraseHUD];
    [[HWCollectionViewController currentlyOpenDocument].hwUndoManager startUndoableChanges];
    __weak typeof(self) weakSelf = self;
    [eraseHUD showAnimated:YES whileExecutingBlock:^{
        @autoreleasepool {
            __strong typeof(self) strongSelf = weakSelf;
            if (self.brushPoints.count > 3) {
                [strongSelf eraseNotes];
            }
            else {
                [strongSelf.brushPoints addObject:[NSValue valueWithCGPoint:pt]];
                [strongSelf.brushPoints addObject:[NSValue valueWithCGPoint:pt]];
                [strongSelf.brushPoints addObject:[NSValue valueWithCGPoint:pt]];
                [strongSelf.brushPoints addObject:[NSValue valueWithCGPoint:pt]];
                [strongSelf eraseNotes];
            }
        }
    } completionBlock:^{
        __strong typeof(self) strongSelf = weakSelf;
        // fade out layer
        [strongSelf.brushView removeFromSuperview];
        strongSelf.brushView = nil;
        [strongSelf updateAllStavesNow];
        [[HWCollectionViewController currentlyOpenDocument].hwUndoManager finishUndoableChangesName:@"Erase notes"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HWEraseNotesFinishedNotification object:strongSelf];
    }];
}

#pragma mark - hand tool

- (BOOL)beginHandWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    return NO;
}

- (BOOL)continueHandWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    //    UIScrollView *sv = (id)self.superview;
    //    CGPoint pt1 = [touch locationInView:self];
    //    CGPoint pt2 = [touch previousLocationInView:self];
    //    sv.contentOffset =
    //        CGPointMake(
    //                    MAX(0,MIN(self.bounds.size.width - sv.bounds.size.width,
    //                              sv.contentOffset.x + pt2.x - pt1.x)),
    //                    MAX(0,MIN(self.bounds.size.height - sv.bounds.size.height,
    //                              sv.contentOffset.y + pt2.y - pt1.y))
    //                    );
    //    return YES;
    return NO;
}

- (void)cancelHandWithEvent:(UIEvent *)event {
    
}

- (void)endHandWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
}

#pragma mark - pencil tool

UInt8 const kNoteVelocity = 100;
NSTimeInterval const kMinNoteHoldTime = 0.1;            // in seconds
NSTimeInterval const kNoteOffDelayInterval = 0.2;       // in seconds

- (void)voiceOffFor:(HarmonyWizStaveView*)stave {
    [[AudioController sharedAudioController] voiceOff:stave.trackNumber];
}

- (void)playTouchedNote:(UITouch*)touch {
    // if note then play it
    AudioController *audioController = [AudioController sharedAudioController];
    CGPoint staveLoc = [touch locationInStave:self.trackingStaveView];
    if ([self.trackingStaveView.createdEvent isKindOfClass:[HarmonyWizNoteEvent class]] && ![audioController isVoiceOn:self.trackingStaveView.trackNumber]) {
        const UInt16 val = [self.trackingStaveView noteValueFromYLoc:staveLoc.y];
        [audioController voiceOn:self.trackingStaveView.trackNumber pitch:val+12 velocity:kNoteVelocity];
        ((HarmonyWizNoteEvent*)self.trackingStaveView.createdEvent).noteValue = val;
        self.trackingStaveView.heldNoteValue = val;
        self.trackingStaveView.currentNoteStartDate = [NSDate date];
        self.trackingStaveView.touchStartDate = [NSDate date];
    }
}

- (BOOL)beginPencilWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (![doc.hwUndoManager canStartUndoableChanges]) {
        [self cancelPencilWithEvent:event];
        return NO;
    }
    
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    if (! expertMode && self.trackingStaveView.trackNumber != 0) {
        return NO;
    }
    
//    CGPoint loc = [touch locationInView:self];
//    HarmonyWizStaveView *stave = [self staveAtPoint:loc];
    
    if (self.trackingStaveView) {
        [doc.song selectNone];
        
        HarmonyWizMusicEvent *toolEvent = doc.metadata.currentToolEvent;
        
        // check if note exists at pencil point
        if ([self.touchedEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
            [doc.hwUndoManager startUndoableChanges];
            
            self.trackingStaveView.createdEvent = self.touchedEvent;
            
            [self playTouchedNote:touch];
            
            [self.trackingStaveView.track selectEvent:self.trackingStaveView.createdEvent];
            [self createPencilLayerForStave:self.trackingStaveView event:self.trackingStaveView.createdEvent];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{[self.trackingStaveView updateStave];}];
            
            return YES;
        }
        
        // place a new note or rest event
        else if ([doc.song canPlaceEvent:toolEvent atEvent:self.touchedEvent] && self.trackingStaveView != self.harmonyStaveView) {
            
            [doc.hwUndoManager startUndoableChanges];
            
            // create the new event
            self.trackingStaveView.createdEvent = [toolEvent copy];
            self.trackingStaveView.createdEvent.beat = self.touchedEvent.beat;
            self.trackingStaveView.createdEvent.subdivision = self.touchedEvent.subdivision;
            
            // apply granularity
            const UInt16 granularity = doc.song.beatSubdivisions / 8;
            UInt16 dur = [doc.song eventDuration:self.trackingStaveView.createdEvent];
            dur = (dur / granularity) * granularity;
            dur = MAX(dur, granularity);
            [doc.song setEventDuration:self.trackingStaveView.createdEvent to:dur];
            
            // truncate to end of song
            [doc.song truncateEventToEndOfSong:self.trackingStaveView.createdEvent];
            
            // play note
            [self playTouchedNote:touch];
            
            // remove overlapping events
            NSIndexSet *overlaps = [doc.song overlappingEvents:self.trackingStaveView.track.events forEvent:self.trackingStaveView.createdEvent];
            NSArray *removedEvents = [self.trackingStaveView.track.events objectsAtIndexes:overlaps];
            [self.trackingStaveView.track.events removeObjectsAtIndexes:overlaps];
            
            // add new event and filler rests
            [self.trackingStaveView.track.events placeObject:self.trackingStaveView.createdEvent];
            NSArray *fillerRests = [doc.song fillerRestsForEvents:removedEvents replacedBy:self.trackingStaveView.createdEvent];
            [self.trackingStaveView.track.events placeObjectsFromArray:fillerRests];
            
            // handle non-expert mode requirements
            BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
            if (! expertMode && [self.trackingStaveView.createdEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
                [doc.song removeOtherEventsOverlappingWith:[NSArray arrayWithObject:self.trackingStaveView.createdEvent]];
            }
            
            // select created note
            [self.trackingStaveView.track selectEvent:self.trackingStaveView.createdEvent];
            [self createPencilLayerForStave:self.trackingStaveView event:self.trackingStaveView.createdEvent];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{[self.trackingStaveView updateStave];}];
            
//            self.userInteractionEnabled = NO;
            
            return YES;
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Error", @"Error alert title") message:@"Cannot place this note or rest here." delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
            });
        }
    }
    
    return NO;
}

- (void)createPencilLayerForStave:(HarmonyWizStaveView*)stave event:(HarmonyWizMusicEvent*)event {
    
    // set pencil location
    self.pencilLocation = [self convertPoint:[stave noteHeadLoc:event] fromStave:stave];
    
    self.penHorizontalPosition = [UIView new];
    self.penHorizontalPosition.backgroundColor = [[UIColor cyanColor] colorWithAlphaComponent:0.5f];
    self.penHorizontalPosition.opaque = NO;
    self.penHorizontalPosition.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.penHorizontalPosition];
    
    self.penVerticalPosition = [UIView new];
    self.penVerticalPosition.backgroundColor = [[UIColor cyanColor] colorWithAlphaComponent:0.5f];
    self.penVerticalPosition.opaque = NO;
    self.penVerticalPosition.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.penVerticalPosition];
    
    [self updatePencilLayerForStave:stave event:event];
}

- (void)updatePencilLayerForStave:(HarmonyWizStaveView*)stave event:(HarmonyWizMusicEvent*)event {
    self.pencilLocation = [self convertPoint:[stave noteHeadLoc:event] fromStave:stave];
    
    self.penHorizontalPosition.frame = CGRectMake(self.contentOffset.x, self.pencilLocation.y-self.rulerControl.staveLineSpacing*0.5f, self.frame.size.width, self.rulerControl.staveLineSpacing);
    
    self.penVerticalPosition.frame = CGRectMake(self.pencilLocation.x-self.rulerControl.staveLineSpacing, self.contentOffset.y, self.rulerControl.staveLineSpacing*2, self.frame.size.height);
}

- (void)removePencilLayer {
    [self.penHorizontalPosition removeFromSuperview];
    [self.penVerticalPosition removeFromSuperview];
    self.penVerticalPosition = nil;
    self.penHorizontalPosition = nil;
}

- (BOOL)continuePencilWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    // track new event pitch
    if (self.trackingStaveView.createdEvent) {
        AudioController *audioController = [AudioController sharedAudioController];
        if ([self.trackingStaveView.createdEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)self.trackingStaveView.createdEvent;
            const UInt16 val = [self.trackingStaveView noteValueFromYLoc:[touch locationInStave:self.trackingStaveView].y];
            if (val != ne.noteValue) {
                if (-[self.trackingStaveView.currentNoteStartDate timeIntervalSinceNow] > kMinNoteHoldTime)
                    self.trackingStaveView.heldNoteValue = ne.noteValue;
                self.trackingStaveView.currentNoteStartDate = [NSDate date];
                ne.noteValue = val;
                [audioController voicePitchChange:self.trackingStaveView.trackNumber pitch:val+12 velocity:kNoteVelocity];
                
                [self updatePencilLayerForStave:self.trackingStaveView event:self.trackingStaveView.createdEvent];
                
                [self.trackingStaveView updateStaveEventsIn:[self.trackingStaveView staveEventsFromMusicEvents:@[self.trackingStaveView.createdEvent]]];
                [self.trackingStaveView updateAllAccidentals];
                [self.trackingStaveView layoutStaveViews];
            }
        }
    }
    return YES;
}

- (void)cancelPencilWithEvent:(UIEvent *)event {
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    if (self.trackingStaveView.trackNumber > 0 && ! expertMode) {
        // nothing to do here
    }
    else {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        
        [self removePencilLayer];
        
        // cancel all changes
        [self voiceOffFor:self.trackingStaveView];
        [doc.song selectNone];
        [doc.hwUndoManager cancelAndRevertUndoableChanges];
        self.trackingStaveView.currentNoteStartDate = nil;
        self.trackingStaveView.createdEvent = nil;
        self.trackingStaveView = nil;
//        [self.trackingStaveView updateAllAccidentals];
//        [self.trackingStaveView layoutStaveViews];
        [self updateAllStavesNow];
    }
}

- (void)endPencilWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    if (self.isEditing == NO) {
        return;
    }
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    [self removePencilLayer];
    
    const NSTimeInterval timeSinceNoteOn = -[self.trackingStaveView.touchStartDate timeIntervalSinceNow];
    
    [doc.song selectNone];
    
    // play note on tap or voice off
    if (timeSinceNoteOn < kNoteOffDelayInterval)
        [self performSelector:@selector(voiceOffFor:) withObject:self.trackingStaveView afterDelay:kNoteOffDelayInterval-timeSinceNoteOn];
    else
        [self voiceOffFor:self.trackingStaveView];
    self.trackingStaveView.touchStartDate = nil;
    
    // place note on final pitch
    if (self.trackingStaveView.createdEvent) {
        if ([self.trackingStaveView.createdEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)self.trackingStaveView.createdEvent;
            const NSTimeInterval currentNoteTime = -[self.trackingStaveView.currentNoteStartDate timeIntervalSinceNow];
            if (currentNoteTime > kMinNoteHoldTime)
                self.trackingStaveView.heldNoteValue = ne.noteValue;
            self.trackingStaveView.currentNoteStartDate = nil;
            ne.noteValue = self.trackingStaveView.heldNoteValue;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizPlacedEventNotification object:self.trackingStaveView];
        
        [self.trackingStaveView updateStaveEventsIn:[self.trackingStaveView staveEventsFromMusicEvents:@[self.trackingStaveView.createdEvent]]];
        [self.trackingStaveView updateAllAccidentals];
        [self.trackingStaveView layoutStaveViews];
        self.trackingStaveView.createdEvent = nil;
    }
    
    // redraw
    self.trackingStaveView = nil;
    
    [self updateAllStavesNow];
}

#pragma mark - brush functions

- (UIColor *)paintColour {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    UIColor *color = nil;
    switch (doc.metadata.currentNoteColour) {
        case kNoteColour_Harmony:
            color = [HWColourScheme harmonyColour];
            break;
        case kNoteColour_Passing:
            color = [HWColourScheme passingColour];
            break;
        case kNoteColour_Suspension:
            color = [HWColourScheme suspensionColour];
            break;
        case kNoteColour_Rest:
            color = [HWColourScheme restColour];
            break;
        default:
            break;
    }
    return color;
}

- (void)scrollForTouch:(UITouch*)touch {
    const float scaleMax = 0.5f;
    CGPoint loc = [touch locationInView:self];
    loc.x -= self.contentOffset.x;
    loc.y -= self.contentOffset.y;
    CGRect scrollViewBounds = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    BOOL shouldScroll = !CGRectContainsPoint(CGRectInset(scrollViewBounds, HWAutoScrollInset, HWAutoScrollInset), loc);
    if (shouldScroll) {
        CGFloat dxScale, dyScale;
        CGFloat dx, dy;
        if (loc.x < CGRectGetMidX(scrollViewBounds)) {
            dxScale = MIN(MAX(2.f * (HWAutoScrollInset - loc.x) / HWAutoScrollInset, 0.f), scaleMax);
            dx = (self.pointNow.x-self.pointBefore.x) * dxScale;
            dx = MIN(dx,0.f);
        }
        else {
            dxScale = MIN(MAX(2.f * (loc.x - CGRectGetWidth(scrollViewBounds) + HWAutoScrollInset) / HWAutoScrollInset, 0.f), scaleMax);
            dx = (self.pointNow.x-self.pointBefore.x) * dxScale;
            dx = MAX(dx,0.f);
        }
        if (loc.y < CGRectGetMidY(scrollViewBounds)) {
            dyScale = MIN(MAX(2.f * (HWAutoScrollInset - loc.y) / HWAutoScrollInset, 0.f), scaleMax);
            dy = (self.pointNow.y-self.pointBefore.y) * dyScale;
            dy = MIN(dy,0.f);
        }
        else {
            dyScale = MIN(MAX(2.f * (loc.y - CGRectGetHeight(scrollViewBounds) + HWAutoScrollInset) / HWAutoScrollInset, 0.f), scaleMax);
            dy = (self.pointNow.y-self.pointBefore.y) * dyScale;
            dy = MAX(dy,0.f);
        }
        CGPoint offset = CGPointMake(self.contentOffset.x+dx, self.contentOffset.y+dy);
        offset.x = MAX(0,offset.x);
        offset.y = MAX(0,offset.y);
        offset.x = MIN(MAX(0,self.contentSize.width-CGRectGetWidth(scrollViewBounds)),offset.x);
        offset.y = MIN(MAX(0,self.contentSize.height-CGRectGetHeight(scrollViewBounds)),offset.y);
        [self setContentOffset:offset];
    }
}

static CGFloat bezierInterpolation(CGFloat t, CGFloat a, CGFloat b, CGFloat c, CGFloat d) {
    const CGFloat t2 = t * t;
    const CGFloat t3 = t2 * t;
    return a + (-a * 3 + t * (3 * a - a * t)) * t
    + (3 * b + t * (-6 * b + b * 3 * t)) * t
    + (c * 3 - c * 3 * t) * t2
    + d * t3;
}

static CGFloat bezierDiff(CGFloat t, CGFloat a, CGFloat b, CGFloat c, CGFloat d) {
    const CGFloat t2 = t * t;
    return -3*c*t2 + 3*d*t2 - 6*(c*t - c)*t + 3*(b*t - 2*b)*t + 6*(b*t - b)*t
    - (a*t - 3*a)*t - (2*a*t - 3*a)*t - 3*a + 3*b;
}

static CGFloat bezierArcLength(CGPoint a, CGPoint b, CGPoint c, CGPoint d) {
    // fairly naive algorithm for approximating arc length
    CGFloat totalLength = 0.f;
    CGPoint point0 = a;
    for (CGFloat t = 0.0f; t <= 1.00001f; t += 0.001f) {
        CGPoint point = CGPointMake(bezierInterpolation(t, a.x, b.x, c.x, d.x), bezierInterpolation(t, a.y, b.y, c.y, d.y));
        totalLength += distance(point0, point);
        point0 = point;
    }
    return totalLength;
}

/*
 static float sqr(float x) {
 return x*x;
 }
 
 static float cuberoot(float x) {
 return powf(x,1.f/3.f);
 }
 */

static CGFloat bezierClosestPositionCoord(CGFloat x, CGFloat a, CGFloat b, CGFloat c, CGFloat d) {
    
    CGFloat bestT = 0;
    CGFloat bestDist = 10000;
    for (CGFloat t = 0.0; t <= 1.00001; t += 0.001) {
        CGFloat pos = bezierInterpolation(t, a,b,c,d);
        CGFloat dist = ABS(pos - x);
        if (dist < bestDist) {
            bestDist = dist;
            bestT = t;
        }
    }
    return bestT;
}

static CGFloat bezierClosestPosition(CGPoint x, CGPoint a, CGPoint b, CGPoint c, CGPoint d) {
    
    CGFloat bestT = 0;
    CGFloat bestDist = 1000000;
    for (CGFloat t = 0.0; t <= 1.00001; t += 0.001) {
        CGPoint pos = CGPointMake(bezierInterpolation(t, a.x,b.x,c.x,d.x),bezierInterpolation(t, a.y,b.y,c.y,d.y));
        CGFloat dist = length_squared(pos, x);
        if (dist < bestDist) {
            bestDist = dist;
            bestT = t;
        }
    }
    return bestT;
}

#pragma mark - select tool

- (void)longPress:(UILongPressGestureRecognizer*)gest {
    if (gest.state == UIGestureRecognizerStateBegan) {
        CGPoint pt = [gest locationInView:self];
        NSUInteger num = [[self staveAtPoint:[gest locationInView:self]] trackNumber];
        [[NSNotificationCenter defaultCenter] postNotificationName:HWSelectTrackNotification object:self userInfo:@{ @"trackNumber" : @(num) }];
        [self.rulerControl touchRuler:CGPointMake([gest locationInView:self.rulerControl].x, 0)];
        [self showSelectMenuIn:CGRectMake(pt.x-1, pt.y-1, 2, 2)];
        
        // show selected notes
        [self updateAllStavesNow];
    }
}

- (BOOL)beginSelectWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    if (self.selectBoxView == nil) {
        self.selectStart = [touch locationInView:self];
        self.selectBoxView = [LBorderView new];
        self.selectBoxView.backgroundColor = [HWColourScheme selectBoxFillColour];
        self.selectBoxView.opaque = NO;
        self.selectBoxView.borderWidth = 2;
        self.selectBoxView.borderColor = [HWColourScheme selectBoxBorderColour];
        self.selectBoxView.borderType = BorderTypeDashed;
        self.selectBoxView.cornerRadius = 8;
        self.selectBoxView.dashPattern = 4;
        self.selectBoxView.spacePattern = 4;
        CGPoint loc = [touch locationInView:self];
        self.selectBoxView.virtualFrame = CGRectMake(loc.x,loc.y,1,1);
        self.selectBoxView.frame = self.selectBoxView.virtualFrame;
        self.selectBoxView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.selectBoxView];
    }
//    self.userInteractionEnabled = NO;
    return YES;
}

- (BOOL)continueSelectWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    self.selectEnd = [touch locationInView:self];
    CGRect frame;
    frame.size.width = MAX(ABS(self.selectStart.x - self.selectEnd.x), 1);
    frame.size.height = MAX(ABS(self.selectStart.y - self.selectEnd.y), 1);
    frame.origin.x = MIN(self.selectStart.x, self.selectEnd.x);
    frame.origin.y = MIN(self.selectStart.y, self.selectEnd.y);
    self.selectBoxView.virtualFrame = frame;
    
    // reduce frame size if frame goes offscreen
    {
        const CGFloat HWSelectFrameBufferSize = 100;
        
        if (self.contentOffset.x - frame.origin.x > HWSelectFrameBufferSize) {
            const CGFloat offset = self.contentOffset.x - frame.origin.x - HWSelectFrameBufferSize;
            frame.origin.x += offset;
            frame.size.width -= offset;
        }
        
        if (CGRectGetMaxX(frame) - (self.contentOffset.x + self.frame.size.width) > HWSelectFrameBufferSize) {
            const CGFloat offset = CGRectGetMaxX(frame) - (self.contentOffset.x + self.frame.size.width) - HWSelectFrameBufferSize;
            frame.size.width -= offset;
        }
    }
    
    self.selectBoxView.frame = frame;
    
    [self scrollForTouch:touch];
    return YES;
}

- (void)cancelSelectWithEvent:(UIEvent *)event {
    [self.selectBoxView removeFromSuperview];
    self.selectBoxView = nil;
}

- (BOOL)selectNotesIn:(CGRect)box {
    BOOL anythingSelected = NO;
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    for (HarmonyWizStaveView *sv in self.musicStaveViews) {
        if (! expertMode && sv.trackNumber != 0) {
            continue;
        }
        NSMutableSet *selection = [NSMutableSet set];
        for (HarmonyWizMusicEvent *ev in sv.track.events) {
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                CGPoint headLoc = [self convertPoint:[sv noteHeadLoc:ev] fromStave:sv];
                if (CGRectContainsPoint(box, headLoc)) {
                    [selection addObject:ev];
                    anythingSelected = YES;
                }
            }
        }
        [sv.track selectEvents:selection];
//        [sv updateStave];
    }
    
    NSMutableSet *selection = [NSMutableSet set];
    for (HarmonyWizHarmonyEvent *ev in self.harmonyStaveView.track.events) {
        if (! ev.noChord) {
            CGPoint headLoc = [self convertPoint:[self.harmonyStaveView noteHeadLoc:ev] fromStave:self.harmonyStaveView];
            if (CGRectContainsPoint(box, headLoc)) {
                [selection addObject:ev];
                anythingSelected = YES;
            }
        }
    }
    [self.harmonyStaveView.track selectEvents:selection];
//    [self.harmonyStaveView updateStave];
    
    return anythingSelected;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)showSelectMenuIn:(CGRect)targetRect {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideMenu:) name:UIMenuControllerDidHideMenuNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willHideMenu:) name:UIMenuControllerWillHideMenuNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didShowMenu:) name:UIMenuControllerDidShowMenuNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowMenu:) name:UIMenuControllerWillShowMenuNotification object:nil];
    
    UIMenuItem *noteType = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Note type", @"Menu item") action:@selector(noteType:)];
    UIMenuItem *arrange = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Arrange", @"Menu item") action:@selector(arrangeSelection:)];
    UIMenuItem *transposeOctaveUp = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Octave up", @"Menu item") action:@selector(transposeOctaveUp:)];
    UIMenuItem *transposeOctaveDown = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Octave down", @"Menu item") action:@selector(transposeOctaveDown:)];
    UIMenuItem *tieNotes = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Tie", @"Menu item") action:@selector(tieNotes:)];
    UIMenuItem *autoTieNotes = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Auto-tie", @"Menu item") action:@selector(autoTieNotes:)];
    UIMenuItem *togglePause = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Pause", @"Menu item") action:@selector(togglePause:)];
    UIMenuItem *leadingNotes = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Leading notes", @"Menu item") action:@selector(leadingNotes:)];
    UIMenuItem *enharmonicShift = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Enharmonic shift", @"Menu item") action:@selector(enharmonicShift:)];
    
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setMenuItems:@[noteType,arrange,transposeOctaveUp,transposeOctaveDown,tieNotes,autoTieNotes,togglePause,leadingNotes,enharmonicShift]];
    [menu setTargetRect:targetRect inView:self];
    [menu setMenuVisible:YES animated:YES];
    
    // check if menu has no items and hence did not show up
    if (! menu.menuVisible) {
        [self.selectBoxView removeFromSuperview];
        self.selectBoxView = nil;
        [[HWCollectionViewController currentlyOpenDocument].song selectNone];
        self.longPress.enabled = YES;
    }
}

- (void)endSelectWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [doc.hwUndoManager startUndoableChanges];
    BOOL anythingSelected = [self selectNotesIn:self.selectBoxView.virtualFrame];
    if (anythingSelected) {
        [doc.hwUndoManager finishUndoableChangesName:@"Selection"];
        [self showSelectMenuIn:CGRectInset(self.selectBoxView.virtualFrame, 6 ,6)];
    }
    else {
        [doc.hwUndoManager cancelUndoableChanges];
        [self.selectBoxView removeFromSuperview];
        self.selectBoxView = nil;
//        self.userInteractionEnabled = YES;
//        [self.selectBoxLayer addAnimation:[self fadeOut:HWSelectBoxFadeTime delay:0] forKey:@"opacityOUT"];
    }
    
    // show selected notes
    [self updateAllStavesNow];
}

#pragma mark - notifications

- (void)willShowMenu:(NSNotification*)notif {
    //            [self becomeFirstResponder];
//    self.userInteractionEnabled = NO;
    self.longPress.enabled = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMenuControllerWillShowMenuNotification object:nil];
}

- (void)didShowMenu:(NSNotification*)notif {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMenuControllerDidShowMenuNotification object:nil];
}

- (void)willHideMenu:(NSNotification*)notif {
    //    [self resignFirstResponder];
    [self.selectBoxView removeFromSuperview];
    self.selectBoxView = nil;
//    [self.selectBoxLayer addAnimation:[self fadeOut:HWSelectBoxFadeTime delay:0] forKey:@"opacityOUT"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMenuControllerWillHideMenuNotification object:nil];
}

- (void)didHideMenu:(NSNotification*)notif {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMenuControllerDidHideMenuNotification object:nil];
    [[HWCollectionViewController currentlyOpenDocument].song selectNone];
    [self updateAllStavesNow];
    self.longPress.enabled = YES;
//    self.userInteractionEnabled = YES;
}

#pragma mark - gesture recogniser delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer == self.longPress) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        return (doc.metadata.currentToolType == kToolType_Select);
    }
    return [super gestureRecognizerShouldBegin:gestureRecognizer];
}

@end

#pragma mark - 2D vector operations

static CGPoint midpoint(CGPoint p, CGPoint q) {
    return CGPointMake(0.5f*(p.x+q.x), 0.5f*(p.y+q.y));
}

static CGFloat length_squared(CGPoint p, CGPoint q) {
    CGFloat dx = p.x - q.x;
    CGFloat dy = p.y - q.y;
    return (dx*dx + dy*dy);
}

static CGFloat distance(CGPoint p, CGPoint q) {
    return sqrtf(length_squared(p, q));
}

static CGPoint minus(CGPoint p, CGPoint q) {
    return CGPointMake(p.x - q.x, p.y - q.y);
}

static CGPoint plus(CGPoint p, CGPoint q) {
    return CGPointMake(p.x + q.x, p.y + q.y);
}

static CGFloat dot(CGPoint p, CGPoint q) {
    return (p.x * q.x + p.y * q.y);
}

static CGPoint scalarMultiply(CGFloat s, CGPoint p) {
    return CGPointMake(s * p.x, s * p.y);
}

// Return minimum distance between line segment vw and point p
static CGFloat minimum_distance_sqr(CGPoint v, CGPoint w, CGPoint p) {
    const CGFloat eps = 1e-6f;
    const CGFloat l2 = length_squared(v, w);  // i.e. |w-v|^2 -  avoid a sqrt
    if (l2 < eps) return length_squared(p, v);   // v == w case
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    const CGFloat t = dot(minus(p,v), minus(w,v)) / l2;
    if (t < 0.0) return length_squared(p, v);       // Beyond the 'v' end of the segment
    else if (t > 1.0) return length_squared(p, w);  // Beyond the 'w' end of the segment
    const CGPoint projection = plus(v, scalarMultiply(t, minus(w,v)));  // Projection falls on the segment
    return length_squared(p, projection);
}
