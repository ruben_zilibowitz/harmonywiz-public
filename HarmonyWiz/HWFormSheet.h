//
//  HWFormSheet.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 31/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWFormSheet : UIViewController <UIGestureRecognizerDelegate, UIWebViewDelegate>
@property (nonatomic,strong) NSString *name;
@end
