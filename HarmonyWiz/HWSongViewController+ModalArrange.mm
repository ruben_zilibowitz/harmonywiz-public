//
//  HWSongViewController+ModalArrange.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWSongViewController+ModalArrange.h"
#import "HWCollectionViewController.h"
#import "HarmonyWizSong.h"
#import "HWModalArrange/HWModalArrange.h"

static HWModalArrange *_modalArrange;

@implementation HWSongViewController (ModalArrange)
- (void)cancelModalArrangement {
    _modalArrange.cancelArrange = YES;
}

- (BOOL)arrangeModalMusic {
    _modalArrange = [[HWModalArrange alloc] init];
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    BOOL result = [_modalArrange modalArrange:song callback:[self](float x){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.arrangeHUD.progress = x;
        });
    }];
    if (result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self redrawStaves:YES];
        });
    }
    else {
        self.arrangeGuardColumn = _modalArrange.furthestColumnReached;
        NSArray *chordEvents = [song.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord == NO"]];
        HarmonyWizHarmonyEvent *he = [chordEvents objectAtIndex:self.arrangeGuardColumn];
        [song placePlaybackIndicatorAtEvent:he];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self scrollToPlaybackPosition];
            [self.rulerControl redisplay];
            [self.allStavesControl updatePositionIndicator];
        });
        self.harmonyFailureReason = LinguiLocalizedString(_modalArrange.lastGuardNameToFail,@"Modal harmonisation failure reason");
    }
    _modalArrange = nil;
    return result;
}
@end
