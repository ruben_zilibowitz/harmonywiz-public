//
//  HarmonyWizSong+EnharmonicShift.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 1/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong+EnharmonicShift.h"
#import "HarmonyWizSong+ScaleNote.h"

@implementation HarmonyWizSong (EnharmonicShift)

- (BOOL)canNote:(HarmonyWizNoteEvent*)ne performEnharmonicShiftTo:(enum EnharmonicShift)shift {
    if (ne.enharmonicShift != shift) {
        if (shift == kDefaultAccidental) {
            return YES;
        }
        else {
            HarmonyWiz::ScaleNote sn = [self HWNoteToScaleNote:ne];
            if (sn.isSharpened() && shift == kSharpsToFlats) {
                return YES;
            }
            if (sn.isFlattened() && shift == kFlatsToSharps) {
                return YES;
            }
        }
    }
    return NO;
}

@end
