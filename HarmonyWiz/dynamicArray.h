//
//  dynamicArray.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

/*
 The purpose of this class is to provide a container that is
 like an STL vector but has a fixed size.
 */

#ifndef __HarmonyWiz__dynamicArray__
#define __HarmonyWiz__dynamicArray__

#include <vector>

template <typename value_type, typename Allocator = std::allocator<value_type> >
class dynamicArray {
public:
    typedef typename std::vector<value_type,Allocator>::iterator iterator;
    typedef typename std::vector<value_type,Allocator>::const_iterator const_iterator;
    typedef typename std::vector<value_type,Allocator>::size_type size_type;
    typedef typename std::vector<value_type,Allocator>::reference reference;
    typedef typename std::vector<value_type,Allocator>::const_reference const_reference;
    
    ////////
    // constructors
    ////////
    // fill
    explicit dynamicArray (size_type n) : vector_(n) {}
    dynamicArray (size_type n, const value_type& val,
                  const Allocator& alloc = Allocator()) : vector_(n, val, alloc) {}
    
    // range
    template <class InputIterator>
    dynamicArray (InputIterator first, InputIterator last,
                  const Allocator& alloc = Allocator()) : vector_(first,last,alloc) {}
    
    // copy
    dynamicArray (const dynamicArray& x) : vector_(x.vector_) {}
    dynamicArray (const dynamicArray& x, const Allocator& alloc) : vector_(x,alloc) {}
    
    // move
    dynamicArray (dynamicArray&& x) : vector_(x) {}
    dynamicArray (dynamicArray&& x, const Allocator& alloc) : vector_(x,alloc) {}
    ////////
    // end constructors
    ////////
    
    iterator begin() { return vector_.begin(); }
    const_iterator begin() const { return vector_.begin(); }
    
    iterator end() { return vector_.end(); }
    const_iterator end() const { return vector_.end(); }
    
    size_type size() const { return vector_.size(); }
    bool empty() const { return vector_.empty(); }
    
    reference operator[] (size_type n) { return vector_[n]; }
    const_reference operator[] (size_type n) const { return vector_[n]; }
    
    reference at (size_type n) { return vector_.at(n); }
    const_reference at (size_type n) const { return vector_.at(n); }
    
    reference front() { return vector_.front(); }
    const_reference front() const { return vector_.front(); }
    
    reference back() { return vector_.back(); }
    const_reference back() const { return vector_.back(); }
    
private:
    std::vector<value_type,Allocator> vector_;
};

#endif /* defined(__HarmonyWiz__dynamicArray__) */
