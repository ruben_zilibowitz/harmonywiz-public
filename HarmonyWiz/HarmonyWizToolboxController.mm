//
//  HarmonyWizToolboxController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 28/06/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizToolboxController.h"
#import "HWToolCell.h"
#import "NSString+Unichar.h"
#import "HarmonyWizSong.h"
#import "HarmonyWizEvent.h"
#import "NSString+Concatenation.h"
#import "NSString+Unichar.h"
#import "HWDocument.h"
#import "HWCollectionViewController.h"
#import "HWToolCustomCellBackground.h"
#import "HWStavesScrollView.h"
#include <vector>

extern NSString * const HWNoteColourNotification;
extern NSString * const HWNoteDotsNotification;
NSString * const HWToolChangeNotification = @"HWToolChange";

extern NSInteger const HWAllStavesControlTag;

extern NSString * const HWStartedStaveEventNotification;
extern NSString * const HWEndedStaveEventNotification;
extern NSString * const HWCancelledStaveEventNotification;

extern NSString * const HWExpertMode;

CGFloat const kCellSize = 50;
CGFloat const kMinLineSpacing = 0;
CGFloat const kHeaderSize = 4;
NSInteger const HWIconImageTag = 1;
NSInteger const HWSelectionImageTag = 2;
NSInteger const HWGrayOverlay = 3;

@interface HarmonyWizToolboxController ()
@property (nonatomic,strong) UIPopoverController *popover;
@end

@implementation HarmonyWizToolboxController

- (void)setup {
    self.toolboxView.backgroundColor = [UIColor whiteColor];
    self.toolboxView.showsVerticalScrollIndicator = NO;
    //        view.bounces = NO;
    self.toolboxView.dataSource = self;
    self.toolboxView.delegate = self;
    
    [self updateToolNote];
    
    // nb: this call seems to only work if delayed here. why??
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self updateEnabled];
        [self updateNoteDots];
        [self updateNoteColour];
    }];
    
    NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    
    [dnc addObserver:self selector:@selector(noteColourChanged:) name:HWNoteColourNotification object:nil];
    [dnc addObserver:self selector:@selector(noteDotsChanged:) name:HWNoteDotsNotification object:nil];
    [dnc addObserver:self selector:@selector(toolChanged:) name:HWToolChangeNotification object:nil];
    
    [dnc addObserver:self selector:@selector(staveEvent:) name:HWStartedStaveEventNotification object:nil];
    [dnc addObserver:self selector:@selector(staveEvent:) name:HWEndedStaveEventNotification object:nil];
    [dnc addObserver:self selector:@selector(staveEvent:) name:HWCancelledStaveEventNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateHighlights {
    for (NSUInteger section = 0; section < 2; section++) {
        [self collectionView:self.toolboxView updateCellHighlightForSection:section];
    }
    [self updateNoteColour];
    [self updateNoteDots];
}

- (void)noteColourChanged:(NSNotification*)notif {
    [self updateNoteColour];
    [self updateToolNote];
}

- (void)noteDotsChanged:(NSNotification*)notif {
    [self updateNoteDots];
    [self updateToolNote];
}

#pragma mark - UICollectionView Datasource

// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return kNumToolTypes;
        case 1:
            return kNumNoteDurations+1;
    }
    return -1;
}

// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 2;
}

- (BOOL)enabledNoteDurations {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    return (doc.metadata.currentToolType == kToolType_Pen || (!self.keyboardWizdom.hidden && doc.metadata.stepRecord));
}

- (BOOL)enabledAll {
    HWStavesScrollView *staves = (id)[self.toolboxView.window viewWithTag:HWAllStavesControlTag];
    return !staves.isEditing;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID = [@"cell" : @(indexPath.section).stringValue : @"," : @(indexPath.row).stringValue];
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    if (cell.selectedBackgroundView == nil) {
        cell.selectedBackgroundView = [[HWToolCustomCellBackground alloc] initWithFrame:CGRectZero];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    }
    
    const BOOL enabled = (!(indexPath.section == 1) || self.enabledNoteDurations) && self.enabledAll;
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (indexPath.section == 0) {
        BOOL highlighted = (doc.metadata.currentToolType == indexPath.row);
        [[cell viewWithTag:HWSelectionImageTag] setHidden:!highlighted];
    }
    
    if (indexPath.section == 1 && indexPath.row >= 2) {
        UILabel *label = (id)[cell.contentView viewWithTag:HWIconImageTag];
        label.font = [UIFont fontWithName:@"AloisenNew" size:36];
        if (doc.metadata.currentNoteColour == kNoteColour_Rest) {
            label.text = [NSString stringWithUnichar:0xF061 + indexPath.row - 2];
            label.frame = CGRectMake(0, 0, 50, 50);
        }
        else {
            label.text = [NSString stringWithUnichar:0xF041 + indexPath.row - 2];
            if (indexPath.row > 2)
                label.frame = CGRectMake(0, 0, 50, 80);
        }
        
        BOOL highlighted = (indexPath.row-2 == doc.metadata.notePlainDuration);
        [[cell viewWithTag:HWSelectionImageTag] setHidden:!highlighted || !enabled];
    }
    
    // handle grey out for disabled/enabled
    {
        cell.userInteractionEnabled = enabled;
        [[cell viewWithTag:HWGrayOverlay] setHidden:enabled];
        if (!enabled)
            [[cell viewWithTag:HWSelectionImageTag] setHidden:YES];
    }
    
    return cell;
}

// 3

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView updateCellHighlightForSection:(NSUInteger)section {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (section == 0) {
        for (NSUInteger row = 0; row < 5; row++) {
            NSIndexPath *ip = [NSIndexPath indexPathForItem:row inSection:section];
            UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:ip];
            BOOL highlighted = (doc.metadata.currentToolType == row);
            [[cell viewWithTag:HWSelectionImageTag] setHidden:!highlighted];
        }
    }
    
    {
        const BOOL enabled = (section != 1 || self.enabledNoteDurations) && self.enabledAll;
        if (!enabled) {
            for (NSUInteger row = 0; row < 2+kNumNoteDurations; row++) {
                NSIndexPath *ip = [NSIndexPath indexPathForItem:row inSection:section];
                UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:ip];
                [[cell viewWithTag:HWSelectionImageTag] setHidden:YES];
            }
        }
    }
}

- (void)updateNoteDots {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.metadata.notePlainDuration == kDuration_Semiquaver && doc.metadata.noteDots > 1)
        doc.metadata.noteDots = 1;
    if (doc.metadata.notePlainDuration == kDuration_Demisemiquaver && doc.metadata.noteDots > 0)
        doc.metadata.noteDots = 0;
    NSIndexPath *ip = [NSIndexPath indexPathForItem:1 inSection:1];
    UICollectionViewCell *cell = [self.toolboxView cellForItemAtIndexPath:ip];
    UIImageView *iv = (id)[cell.contentView viewWithTag:HWIconImageTag];
    switch (doc.metadata.noteDots) {
        case 0:
            iv.image = [UIImage imageNamed:@"images/ToolboxIcons/NoDot"];
            break;
        case 1:
            iv.image = [UIImage imageNamed:@"images/ToolboxIcons/SingleDot"];
            break;
        case 2:
            iv.image = [UIImage imageNamed:@"images/ToolboxIcons/DoubleDot"];
            break;
    }
}

- (void)updateNotesRests {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];

    NSIndexPath *ip;
    UICollectionViewCell *cell;
    
    for (NSUInteger row = 2; row < 2+kNumNoteDurations; row++) {
        ip = [NSIndexPath indexPathForItem:row inSection:1];
        cell = [self.toolboxView cellForItemAtIndexPath:ip];
        UILabel *label = (id)[cell.contentView viewWithTag:HWIconImageTag];
        if (row >= 2) {
            if (doc.metadata.currentNoteColour == kNoteColour_Rest) {
                label.text = [NSString stringWithUnichar:0xF061 + row - 2];
                label.frame = CGRectMake(0, 0, 50, 50);
            }
            else {
                label.text = [NSString stringWithUnichar:0xF041 + row - 2];
                if (row > 2)
                    label.frame = CGRectMake(0, 0, 50, 80);
                else
                    label.frame = CGRectMake(0, 0, 50, 50);
            }
        }
        const BOOL enabled = self.enabledNoteDurations && self.enabledAll;
        BOOL highlighted = (row-2 == doc.metadata.notePlainDuration);
        [[cell viewWithTag:HWSelectionImageTag] setHidden:!highlighted || !enabled];
    }
}

- (void)updateNoteColour {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    NSIndexPath *ip = [NSIndexPath indexPathForItem:0 inSection:1];
    UICollectionViewCell *cell = [self.toolboxView cellForItemAtIndexPath:ip];
    UIImageView *iv = (id)[cell.contentView viewWithTag:HWIconImageTag];
    {
        switch (doc.metadata.currentNoteColour) {
            case kNoteColour_Harmony:
                iv.image = [UIImage imageNamed:@"images/ToolboxIcons/Note_Harmony"];
                break;
            case kNoteColour_Passing:
                iv.image = [UIImage imageNamed:@"images/ToolboxIcons/Note_Passing"];
                break;
            case kNoteColour_Suspension:
                iv.image = [UIImage imageNamed:@"images/ToolboxIcons/Note_Suspension"];
                break;
            case kNoteColour_Auto:
                iv.image = [UIImage imageNamed:@"images/ToolboxIcons/Note_Auto"];
                break;
            case kNoteColour_Rest:
                iv.image = [UIImage imageNamed:@"images/ToolboxIcons/Rest"];
                break;
            default:
                break;
        }
        [self updateNotesRests];
    }
}

- (void)setPopover:(UIPopoverController *)popover {
    if (self.popover != nil) {
        [self.popover dismissPopoverAnimated:YES];
    }
    _popover = popover;
}

- (void)updateEnabled {
    for (NSUInteger section = 0; section < 2; section++) {
        const NSUInteger count = [self collectionView:self.toolboxView numberOfItemsInSection:section];
        for (NSUInteger item = 0; item < count; item++) {
            const BOOL enabled = (!(section == 1) || self.enabledNoteDurations) && self.enabledAll;
            UICollectionViewCell *cell = [self.toolboxView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:item inSection:section]];
            cell.userInteractionEnabled = enabled;
            [[cell viewWithTag:HWGrayOverlay] setHidden:enabled];
            if (!enabled)
                [[cell viewWithTag:HWSelectionImageTag] setHidden:YES];
        }
    }
}

- (void)toolChanged:(NSNotification*)notif {
    [self updateNoteColour];
    [self updateEnabled];
    [self collectionView:self.toolboxView updateCellHighlightForSection:0];
    [self updateToolNote];
}

- (void)staveEvent:(NSNotification*)notif {
    [self updateNoteColour];
    [self updateEnabled];
    [self collectionView:self.toolboxView updateCellHighlightForSection:0];
    [self updateToolNote];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (indexPath.section == 0) {
        doc.metadata.currentToolType = (ToolType)indexPath.row;
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:HWToolChangeNotification object:self userInfo:@{ @"tool" : @(doc.metadata.currentToolType) }];
        return;
    }
    
    // Colours menu
    else if (indexPath.section == 1 && indexPath.row == 0) {
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        
        id coloursMenu = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:(expertMode ? @"coloursMenu" : @"coloursMenuEasy")];
        self.popover = [[UIPopoverController alloc] initWithContentViewController:coloursMenu];
        self.popover.delegate = self;
        [self.popover presentPopoverFromRect:CGRectMake(0, 4 * (kCellSize+kMinLineSpacing) + kHeaderSize*2, kCellSize, kCellSize) inView:self.toolboxView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    // Dots menu
    else if (indexPath.section == 1 && indexPath.row == 1) {
        id dotsMenu = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"dotsMenu"];
        self.popover = [[UIPopoverController alloc] initWithContentViewController:dotsMenu];
        self.popover.delegate = self;
        [self.popover presentPopoverFromRect:CGRectMake(0, 5 * (kCellSize+kMinLineSpacing) + kHeaderSize*2, kCellSize, kCellSize) inView:self.toolboxView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    // Notes and Rests durations
    else if (indexPath.section == 1 && indexPath.row >= 2) {
        doc.metadata.notePlainDuration = (enum NotePlainDuration)(indexPath.row - 2);
        [self updateNotesRests];
        if (doc.metadata.notePlainDuration >= kDuration_Semiquaver)
            [self updateNoteDots];
    }
    
    [self collectionView:collectionView updateCellHighlightForSection:indexPath.section];
    [self updateToolNote];
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kCellSize, kCellSize);
}

// 2
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(2, 1, 2, 1);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(kCellSize, kHeaderSize);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return kMinLineSpacing;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return kMinLineSpacing;
}

- (void)updateToolNote {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    HarmonyWizMusicEvent *event = nil;
    
    if (doc.metadata.currentNoteColour == kNoteColour_Rest) {
        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
        event = re;
    }
    else {
        HarmonyWizNoteEvent *ne = [[HarmonyWizNoteEvent alloc] init];
        ne.type = (enum NoteEventType)doc.metadata.currentNoteColour;
        event = ne;
    }
    
    UInt16 subdivs = doc.song.beatSubdivisions;
    switch (doc.metadata.notePlainDuration) {
//        case kDuration_Breve:
//            subdivs *= 8;
//            break;
        case kDuration_Semibreve:
            subdivs *= 4;
            break;
        case kDuration_Minim:
            subdivs *= 2;
            break;
        case kDuration_Crotchet:
            break;
        case kDuration_Quaver:
            subdivs /= 2;
            break;
        case kDuration_Semiquaver:
            subdivs /= 4;
            break;
        case kDuration_Demisemiquaver:
            subdivs /= 8;
            break;
        default:
            break;
    }
    
    if (doc.song.timeSign.lowerNumeral == 8) {
        subdivs *= 2;
    }
    
    switch (doc.metadata.noteDots) {
        case 0:
            break;
        case 1:
            subdivs = ((subdivs * 3) / 2);
            break;
        case 2:
            subdivs = ((subdivs * 7) / 4);
            break;
    }
    
    [doc.song setEventDuration:event to:subdivs];
    
    doc.metadata.currentToolEvent = event;
}

#pragma mark - popover delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if (self.popover == popoverController) {
        self.popover = nil;
    }
}

@end
