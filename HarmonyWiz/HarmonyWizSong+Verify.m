//
//  HarmonyWizSong+Verify.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 14/07/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong+Verify.h"
#import "HWAppDelegate.h"
#import "UIAlertView+Blocks.h"
#import "HWExceptions.h"
#import "HWCollectionViewController.h"

@implementation HarmonyWizSong (Verify)

- (void)assertEventOrder:(HarmonyWizEvent*)ev1 withRespectTo:(HarmonyWizEvent*)ev2 {
    
    switch ([ev1 compare:ev2]) {
        case NSOrderedAscending:
            break;
        case NSOrderedDescending:
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"events not sorted" userInfo:nil];
            break;
        case NSOrderedSame:
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"events doubled up" userInfo:nil];
            break;
    }
}

- (void)checkAllEventsSorted {
    @try {
        for (HarmonyWizMusicTrack *track in [self.musicTracks arrayByAddingObject:self.harmony]) {
            for (NSUInteger idx = 1; idx < track.events.count; idx++) {
                [self assertEventOrder:[track.events objectAtIndex:idx-1] withRespectTo:[track.events objectAtIndex:idx]];
                if ([self eventEndSubdivision:[track.events objectAtIndex:idx-1]] > [self eventStartSubdivision:[track.events objectAtIndex:idx]]) {
                    @throw [HWEventsSortedVerifyFailed exception];
                }
            }
        }
    }
    @catch (HWEventsSortedVerifyFailed *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            recordError([NSError errorWithDomain:@"events overlapping: failed verify" code:1 userInfo:nil])
            
            [[[UIAlertView alloc] initWithTitle:@"Oops" message:@"I'm sorry but somehow a problem has occurred. If you press continue the app might crash." cancelButtonItem:[RIButtonItem itemWithLabel:@"Continue"] otherButtonItems:[RIButtonItem itemWithLabel:@"Revert" action:^{
                HarmonyWizUndoManager *undoManager = [[HWCollectionViewController currentlyOpenDocument] hwUndoManager];
                if (undoManager.canStartUndoableChanges && undoManager.canUndo) {
                    [undoManager undo];
                }
                else {
                }
            }], nil] show];
        });
    }
    @finally {
        
        if ([self eventStartSubdivision:self.finalBarLine] % (self.beatSubdivisions * self.timeSign.upperNumeral) != 0) {
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"end bar line not aligned with start of a bar" userInfo:nil];
        }
    }
}


@end
