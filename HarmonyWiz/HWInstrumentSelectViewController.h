//
//  HWInstrumentSelectViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 20/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HarmonyWizTrackControl;
@class HarmonyWizPurchaseSamplesViewController;

@interface HWInstrumentSelectViewController : UITableViewController

@property (nonatomic,weak) HarmonyWizTrackControl *trackControl;
@property (nonatomic,strong)  NSArray *currentInstrumentNames;
@property (nonatomic,strong) HarmonyWizPurchaseSamplesViewController *purchaseSamplesViewController;

@end

extern NSString * const HarmonyWizChangedInstrumentNotification;
