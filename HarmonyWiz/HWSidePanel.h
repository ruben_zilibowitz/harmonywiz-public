//
//  HWSidePanel.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWSidePanel : UIView
@property (nonatomic,weak) UIView *magicView;
@end
