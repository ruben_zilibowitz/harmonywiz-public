//
//  NSString+iOS6.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/10/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (iOS6)

- (void)iOS6_drawAtPoint:(CGPoint)point withAttributes:(NSDictionary *)attrs;
- (void)iOS6_drawInRect:(CGRect)rect withAttributes:(NSDictionary *)attrs;
- (CGRect)iOS6_boundingRectWithSize:(CGSize)size options:(NSStringDrawingOptions)options attributes:(NSDictionary *)attributes context:(NSStringDrawingContext *)context;

@end
