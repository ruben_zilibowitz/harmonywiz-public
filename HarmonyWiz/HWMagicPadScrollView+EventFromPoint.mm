//
//  HWMagicPadScrollView+EventFromPoint.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWMagicPadScrollView+EventFromPoint.h"
#import "HWCollectionViewController.h"
#import "ScaleNote.h"
#import "HarmonyWizMusicEvent+CPP.h"

@implementation HWMagicPadScrollView (EventFromPoint)

- (HarmonyWizNoteEvent*)musicEventFromPoint:(CGPoint)pt pos:(NSUInteger)pos dur:(UInt16)dur {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    HarmonyWizMusicTrack *track = [doc.song.musicTracks objectAtIndex:doc.metadata.currentlySelectedTrackNumber];
    
    if (pos >= doc.song.finalBarSubdivision) {
        return nil;
    }
    if (pos+dur > doc.song.finalBarSubdivision) {
        dur = (doc.song.finalBarSubdivision - pos);
    }
    
    const float padding = 40.f;
    const float lambda = MAX(MIN((pt.y-padding) / (CGRectGetHeight(self.bounds)-2*padding), 1.f), 0.f);
    const float pitch = (1.f - lambda) * (track.highestNoteAllowed) + lambda * track.lowestNoteAllowed;
    
    HarmonyWizNoteEvent *ne = [[HarmonyWizNoteEvent alloc] init];
    ne.noteValue = (UInt16)(pitch+0.5f);
    ne.type = kNoteEventType_Harmony;
    [doc.song setEventPosition:ne to:pos];
    [doc.song setEventDuration:ne to:dur];
    
    if (doc.song.isModal) {
        [ne roundPitchModal:doc.song.songMode];
    }
    else {
        const UInt8 root = doc.song.keySign.relativeMajorRoot;
        HarmonyWiz::ScaleNote sn = HarmonyWiz::ScaleNote(ne.noteValue, root, false).roundUp(false);
        ne.noteValue = sn.chromatic(root, false);
    }
    
    return ne;
}

- (int16_t)stepsFromMusicNote:(HarmonyWizNoteEvent*)firstNote to:(HarmonyWizNoteEvent*)lastNote {
    // NB: This should be adjusted to handle modes correctly
    // but it will do for now.
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const UInt8 root = doc.song.keySign.relativeMajorRoot;
    HarmonyWiz::ScaleNote firstSN = HarmonyWiz::ScaleNote(firstNote.noteValue, root, false).roundUp(false);
    HarmonyWiz::ScaleNote lastSN = HarmonyWiz::ScaleNote(lastNote.noteValue, root, false).roundUp(false);
    return (ABS(lastSN.note - firstSN.note)+2)/3;
}

//- (NSDictionary*)eventsAndStepsFromBottomLeft:(CGPoint)bottomLeft topRight:(CGPoint)topRight {
//    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
//    const UInt8 root = doc.song.keySign.relativeMajorRoot;
//    HarmonyWiz::ScaleNote firstSN = HarmonyWiz::ScaleNote(firstNote.noteValue, root, false).roundUp(false);
//    HarmonyWiz::ScaleNote lastSN = HarmonyWiz::ScaleNote(lastNote.noteValue, root, false).roundUp(false);
//    steps = (ABS(lastSN.note - firstSN.note)+2)/3;
//}

@end
