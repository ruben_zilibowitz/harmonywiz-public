//
//  HarmonyWizTrackControl.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizSong.h"

extern NSString * const HarmonyWizSoloMuteStatusChanged;
extern NSString * const HarmonyWizLockStatusChanged;
extern NSString * const HarmonyWizSetInstrumentNotification;
extern NSString * const HarmonyWizPressedStyle;
extern NSString * const HarmonyWizWillSelectTrack;
extern NSString * const HarmonyWizDidSelectTrack;
extern NSString * const HWTrackVolumeChanged;
extern NSString * const HWTrackPanChanged;
extern NSString * const HWBeginDraggingTrackControlNotification;
extern NSString * const HWEndDraggingTrackControlNotification;

@interface HarmonyWizTrackControl : UIControl <UIGestureRecognizerDelegate>
@property (nonatomic,assign) CGFloat height;
@property (nonatomic,strong,readonly) UILabel *instrumentLabel, *trackLabel;
@property (nonatomic,strong,readonly) UIImageView *instrumentIcon;
@property (nonatomic,assign) float volume, pan;
@property (nonatomic,assign) NSInteger trackNumber;

- (HarmonyWizTrack*)track;
- (BOOL)isHarmonyTrack;
- (BOOL)solo;
- (BOOL)mute;
- (BOOL)locked;
- (NSString*)instrumentName;
- (int)transposition;
- (NSComparisonResult)compare:(HarmonyWizTrackControl*)control;
- (BOOL)trackIsSelected;
- (void)updateSelectionVisuals;
- (void)selectThisTrack;

- (id)initWithFrame:(CGRect)frame trackNumber:(NSUInteger)aTrackNumber;

- (void)updateSolo;
- (void)updateMute;
- (void)updateLocked;
- (void)updateInstrument;

- (void)loadSamples:(NSString*)name;

@end
