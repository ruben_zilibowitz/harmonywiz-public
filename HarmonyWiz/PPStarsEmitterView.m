//
// PPStarsEmitterView.h
// Created by Particle Playground on 8/05/2014
//

#import "PPStarsEmitterView.h"


@interface PPStarsEmitterView ()
@property (nonatomic,strong) CAEmitterCell *starCell;
@end

@implementation PPStarsEmitterView

-(id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
	if (self) {
		self.backgroundColor = [UIColor clearColor];
	}
	
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		self.backgroundColor = [UIColor clearColor];
	}
	
	return self;
}

+ (Class) layerClass {
    //configure the UIView to have emitter layer
    return [CAEmitterLayer class];
}

- (UIImage *)flippedVerticalImage:(UIImage*)image {
    UIGraphicsBeginImageContext(image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *flippedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return flippedImage;
}

- (UIImage*)sparkleImage {
    if (SYSTEM_VERSION_LESS_THAN(@"7"))
        return [self flippedVerticalImage:[UIImage imageNamed:@"images/sparkle"]];
    else
        return [UIImage imageNamed:@"images/sparkle"];
}

- (void)pause {
    CAEmitterLayer *emitterLayer = (CAEmitterLayer*)self.layer;
    [emitterLayer setValue:@(0.f) forKeyPath:@"emitterCells.stars.birthRate"];
}

- (void)resume {
    CAEmitterLayer *emitterLayer = (CAEmitterLayer*)self.layer;
	emitterLayer.emitterPosition = CGPointMake(self.center.x, self.center.y + 5.f);
    [emitterLayer setValue:@(5.f) forKeyPath:@"emitterCells.stars.birthRate"];
}

- (void)setup {
    CAEmitterLayer *emitterLayer = (CAEmitterLayer*)self.layer;
    
	emitterLayer.name = @"emitterLayer";
	emitterLayer.emitterPosition = self.center;
	emitterLayer.emitterZPosition = 0;
    
	emitterLayer.emitterSize = CGSizeMake(750.f, 50.f);
	emitterLayer.emitterDepth = 0.00;
    
	emitterLayer.emitterShape = kCAEmitterLayerRectangle;
    
	emitterLayer.renderMode = kCAEmitterLayerAdditive;
    
	emitterLayer.seed = 123;
    
    
    
	
	// Create the emitter Cell
	CAEmitterCell *emitterCell = [CAEmitterCell emitterCell];
	
	emitterCell.name = @"stars";
	emitterCell.enabled = YES;
    
	emitterCell.contents = (id)[self.sparkleImage CGImage];
	emitterCell.contentsRect = CGRectMake(0.00, 0.00, 1.00, 1.00);
    
	emitterCell.magnificationFilter = kCAFilterLinear;
	emitterCell.minificationFilter = kCAFilterLinear;
	emitterCell.minificationFilterBias = 0.00;
    
	emitterCell.scale = 0.70;
	emitterCell.scaleRange = 0.00;
	emitterCell.scaleSpeed = 0.10;
    
	emitterCell.color = [[UIColor colorWithRed:1.00 green:1.00 blue:0.00 alpha:1.00] CGColor];
	emitterCell.redRange = 0.20;
	emitterCell.greenRange = 0.19;
	emitterCell.blueRange = 0.16;
	emitterCell.alphaRange = 0.00;
    
	emitterCell.redSpeed = 0.00;
	emitterCell.greenSpeed = 0.00;
	emitterCell.blueSpeed = 0.00;
	emitterCell.alphaSpeed = -0.36;
    
	emitterCell.lifetime = 8.00;
	emitterCell.lifetimeRange = 0.50;
	emitterCell.birthRate = 0;
	emitterCell.velocity = 1.00;
	emitterCell.velocityRange = 2.00;
	emitterCell.xAcceleration = 0.00;
	emitterCell.yAcceleration = 0.00;
	emitterCell.zAcceleration = 0.00;
    
	// these values are in radians, in the UI they are in degrees
	emitterCell.spin = 0.000;
	emitterCell.spinRange = 12.566;
	emitterCell.emissionLatitude = 0.000;
	emitterCell.emissionLongitude = 0.000;
	emitterCell.emissionRange = 6.283;
    
    self.starCell = emitterCell;
	emitterLayer.emitterCells = @[emitterCell];
}

@end
