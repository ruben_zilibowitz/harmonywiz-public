//
//  HWColoursController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/09/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWColoursController : UICollectionViewController
@end

extern NSString * const HWNoteColourNotification;
