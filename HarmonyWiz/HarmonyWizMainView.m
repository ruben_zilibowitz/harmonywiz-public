//
//  HarmonyWizMainView.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 8/07/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "HarmonyWizMainView.h"
#import "HWCollectionViewController.h"
#import "HWDocument.h"
#import "HWStavesScrollView.h"
#import "UIView+Recursive/UIView+Recursive.h"

@implementation HarmonyWizMainView

extern CGFloat const HWTracksViewClosedWidth;
extern CGFloat const HWTracksViewOpenedWidth;
extern CGFloat const kKeyboardWizdomHeight;
extern CGFloat const HarmonyWizStaveVerticalScale;

const CGFloat HWToolboxWidth = 54.f;
const CGFloat HWRulerHeight = 40.f;
const CGFloat HWRulerHeightMagicMode = 64.f;
const CGFloat HWToolbarHeight = 44.f;

CGFloat const HWStatusBarHeight = 20.f;
CGFloat const HWScreenWidth = 1024.f;
CGFloat const HWScreenHeight = 768.f;
CGFloat const HWPanelWidth = 96.f;
CGFloat const HWDividerWidth = 2.f;
CGFloat const HWZoomViewWidth = 500.f;
CGFloat const HWZoomViewMagicModeHeight = 80.f;
CGFloat const HWZoomViewHeight = 200.f;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)updateAllFrames {
    
    const BOOL isAtLeastIOS7 = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7");
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const CGFloat trackHeight = HarmonyWizStaveVerticalScale * doc.metadata.notesPointSize;
    const BOOL magicMode = doc.metadata.magicMode;
    const NSUInteger numTracks = (magicMode ? 1 : doc.song.musicTracks.count+1);
    
    // toolbar
    if (isAtLeastIOS7)
        self.toolbar.frame = CGRectMake(0, 0, self.bounds.size.width, HWToolbarHeight+HWStatusBarHeight);
    else
        self.toolbar.frame = CGRectMake(0, HWStatusBarHeight, self.bounds.size.width, HWToolbarHeight);
    
    if (magicMode) {
        // tracks view
        self.tracksView.frame = CGRectMake(-HWTracksViewOpenedWidth, HWToolbarHeight+HWStatusBarHeight, HWTracksViewOpenedWidth, trackHeight);
        
        // staves view
        self.stavesView.frame = CGRectMake(0.f,HWToolbarHeight+HWStatusBarHeight,HWScreenWidth,trackHeight);
        
        // ruler view
        self.rulerView.frame = CGRectMake(0.f, HWScreenHeight - HWRulerHeightMagicMode, HWScreenWidth, HWRulerHeightMagicMode);
        
        // toolbox view
        self.toolbox.frame = CGRectMake(HWScreenWidth, HWToolbarHeight+HWStatusBarHeight, HWToolboxWidth, HWScreenHeight-(HWToolbarHeight+HWStatusBarHeight));
        
        // piano view
        self.piano.frame = CGRectMake(0, HWScreenHeight, HWScreenWidth, kKeyboardWizdomHeight);
        
        // magic pad view
        self.magicView.frame = CGRectMake(0, HWToolbarHeight+HWStatusBarHeight+trackHeight, HWScreenWidth, HWScreenHeight - HWRulerHeightMagicMode - (HWToolbarHeight+HWStatusBarHeight+trackHeight));
        
        // left panel
        self.leftPanel.frame = CGRectMake(0, HWToolbarHeight+HWStatusBarHeight, HWPanelWidth, HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight));
        
        // right panel
        self.rightPanel.frame = CGRectMake(HWScreenWidth-HWPanelWidth, HWToolbarHeight+HWStatusBarHeight, HWPanelWidth, HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight));
        
        // vertical border view
        self.borderVerticalView.frame = CGRectMake(-HWDividerWidth, HWToolbarHeight+HWStatusBarHeight, HWDividerWidth, trackHeight);
        
        // horizontal border view
        self.borderHorizontalView.frame = CGRectMake(0, HWToolbarHeight+HWStatusBarHeight, HWScreenWidth, HWDividerWidth);
        
        // zoom view
        self.zoomView.frame = CGRectMake(CGRectGetMidX(self.stavesView.frame) - HWZoomViewWidth*0.5f, CGRectGetMidY(self.stavesView.frame) - HWZoomViewMagicModeHeight*0.5f, HWZoomViewWidth, HWZoomViewMagicModeHeight);
    }
    else {
        // magic pad view
        self.magicView.frame = CGRectMake(0, HWScreenHeight, HWScreenWidth, HWScreenHeight - HWRulerHeightMagicMode - (HWToolbarHeight+HWStatusBarHeight+trackHeight));
        
        // left panel
        self.leftPanel.frame = CGRectMake(-HWPanelWidth, HWToolbarHeight+HWStatusBarHeight, HWPanelWidth, HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight));
        
        // right panel
        self.rightPanel.frame = CGRectMake(HWScreenWidth, HWToolbarHeight+HWStatusBarHeight, HWPanelWidth, HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight));
        
        if (doc.metadata.pianoOpened) {
            // piano view
            self.piano.frame = CGRectMake(0, HWScreenHeight - kKeyboardWizdomHeight, HWScreenWidth, kKeyboardWizdomHeight);
        }
        else {
            // piano view
            self.piano.frame = CGRectMake(0, HWScreenHeight, HWScreenWidth, kKeyboardWizdomHeight);
        }
        
        // toolbox view
        self.toolbox.frame = CGRectMake(HWScreenWidth - HWToolboxWidth, HWToolbarHeight+HWStatusBarHeight, HWToolboxWidth, CGRectGetMinY(self.piano.frame)-(HWToolbarHeight+HWStatusBarHeight));
        
        if (self.drawerOpened) {
            // ruler view
            self.rulerView.frame = CGRectMake(HWTracksViewOpenedWidth, HWToolbarHeight+HWStatusBarHeight, HWScreenWidth-HWTracksViewOpenedWidth-HWToolboxWidth, HWRulerHeight);
            
            // tracks view
            self.tracksView.frame = CGRectMake(0.f, HWToolbarHeight+HWStatusBarHeight+HWRulerHeight, HWTracksViewOpenedWidth, CGRectGetMinY(self.piano.frame) - (HWToolbarHeight+HWStatusBarHeight+HWRulerHeight));
            
            // staves view
            self.stavesView.frame = CGRectMake(HWTracksViewOpenedWidth,HWToolbarHeight+HWStatusBarHeight+HWRulerHeight,HWScreenWidth-HWTracksViewOpenedWidth-HWToolboxWidth,self.tracksView.frame.size.height);
            
            // vertical border view
            self.borderVerticalView.frame = CGRectMake(HWTracksViewOpenedWidth, HWToolbarHeight+HWStatusBarHeight+HWRulerHeight, HWDividerWidth, (doc.metadata.pianoOpened ? MIN(HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight+HWRulerHeight) - kKeyboardWizdomHeight, trackHeight*numTracks) : trackHeight*numTracks));
            
            // horizontal border view
            self.borderHorizontalView.frame = CGRectMake(HWTracksViewOpenedWidth, HWToolbarHeight+HWStatusBarHeight+HWRulerHeight, HWScreenWidth-HWTracksViewOpenedWidth, HWDividerWidth);
        }
        else {
            // ruler view
            self.rulerView.frame = CGRectMake(HWTracksViewClosedWidth, HWToolbarHeight+HWStatusBarHeight, HWScreenWidth-HWTracksViewClosedWidth-HWToolboxWidth, HWRulerHeight);
            
            // tracks view
            self.tracksView.frame = CGRectMake(HWTracksViewClosedWidth-HWTracksViewOpenedWidth, HWToolbarHeight+HWStatusBarHeight+HWRulerHeight, HWTracksViewOpenedWidth, CGRectGetMinY(self.piano.frame) - (HWToolbarHeight+HWStatusBarHeight+HWRulerHeight));
            
            // staves view
            self.stavesView.frame = CGRectMake(HWTracksViewClosedWidth,HWToolbarHeight+HWStatusBarHeight+HWRulerHeight,HWScreenWidth-HWTracksViewClosedWidth-HWToolboxWidth,self.tracksView.frame.size.height);
            
            // vertical border view
            self.borderVerticalView.frame = CGRectMake(HWTracksViewClosedWidth, HWToolbarHeight+HWStatusBarHeight+HWRulerHeight, HWDividerWidth, (doc.metadata.pianoOpened ? MIN(HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight+HWRulerHeight) - kKeyboardWizdomHeight, trackHeight*numTracks) : trackHeight*numTracks));
            
            // horizontal border view
            self.borderHorizontalView.frame = CGRectMake(HWTracksViewClosedWidth, HWToolbarHeight+HWStatusBarHeight+HWRulerHeight, HWScreenWidth-HWTracksViewClosedWidth, HWDividerWidth);
        }
        
        // zoom view
        self.zoomView.frame = CGRectMake(CGRectGetMidX(self.stavesView.frame) - HWZoomViewWidth*0.5f, CGRectGetMidY(self.stavesView.frame) - HWZoomViewHeight*0.5f, HWZoomViewWidth, HWZoomViewHeight);
    }
}

- (void)disableConstraintsForAllSubviews {
    self.toolbar.translatesAutoresizingMaskIntoConstraints = NO;
    self.tracksView.translatesAutoresizingMaskIntoConstraints = NO;
    self.rulerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.stavesView.translatesAutoresizingMaskIntoConstraints = NO;
    self.magicView.translatesAutoresizingMaskIntoConstraints = NO;
    self.toolbox.translatesAutoresizingMaskIntoConstraints = NO;
    self.borderHorizontalView.translatesAutoresizingMaskIntoConstraints = NO;
    self.borderVerticalView.translatesAutoresizingMaskIntoConstraints = NO;
    self.piano.translatesAutoresizingMaskIntoConstraints = NO;
    self.zoomView.translatesAutoresizingMaskIntoConstraints = NO;
    self.leftPanel.translatesAutoresizingMaskIntoConstraints = NO;
    self.rightPanel.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self updateAllFrames];
}

@end
