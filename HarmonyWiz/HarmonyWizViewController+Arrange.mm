//
//  HarmonyWizViewController+Arrange.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizViewController+Arrange.h"
#import "HarmonyWizSharedObjects.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "HarmonyWizTimeRulerControl.h"
#import "NSMutableArray+Sorted.h"
#include "Solver.h"

using namespace HarmonyWiz;

// This is a global shift to translate between Solver chromatic space
// and HarmonyWiz chromatic space.
// Solver chromatic note + kChromaticShift = HarmonyWiz chromatic note
const SInt16 kChromaticShift = 4*12;

@implementation HarmonyWizViewController (Arrange)

static bool *sCancelPointer = nullptr;

template <size_t rows>
class Arranger : public Solver<rows> {
public:
    Arranger(HarmonyWizViewController *vc)
    : Solver<rows>([HarmonyWizSharedObjects sharedManager].song.harmonyEventCount,
                   [HarmonyWizSharedObjects sharedManager].song.keySign.root,
                   [HarmonyWizSharedObjects sharedManager].song.keySign.tonality == kTonality_Minor),
    viewController(vc),
    preservedSong([[HarmonyWizSharedObjects sharedManager].song copy]),
    anythingSelected([HarmonyWizSharedObjects sharedManager].song.anythingSelected)
    {}
    
    void applyConstraints(NSDictionary *constraints, size_t col, const bitset< kNumProperties > &properties) {
        id obj;
        if ((obj = [constraints objectForKey:HarmonyWizDegreeProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedRoot(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizThirdProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedThird(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizFifthProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedFifth(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizSevenProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedSeventh(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizNineProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedNinth(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizElevenProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedEleventh(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizThirteenProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedThirteenth(col, val);
        }
        id obj1 = [constraints objectForKey:HarmonyWizChordTypeProperty];
        id obj2 = [constraints objectForKey:HarmonyWizInversionProperty];
        if (obj1 && obj2) {
            Figures::ChordType ch = [obj1 HWChordTypeValue];
            Figures::ChordInversion inv = [obj2 HWChordInversionValue];
            Figures::ChordTypeWithInversion chinv = Figures::packChord(ch, inv);
            this->getFigures(col).disallowAll();
            this->getFigures(col).allow(chinv);
        }
        else {
            if (obj1) {
                Figures::ChordType ch = [obj1 HWChordTypeValue];
                Figures::ChordInversion lastInversion = Figures::kThirdInversion;
                if (ch <= Figures::kAddSix)
                    lastInversion--;
                this->getFigures(col).disallowAll();
                for (Figures::ChordInversion inv = Figures::kRootPosition; inv <= lastInversion; inv++) {
                    this->getFigures(col).allow(Figures::packChord(ch, inv));
                }
            }
            if (obj2) {
                Figures::ChordInversion inv = [obj2 HWChordInversionValue];
                this->getFigures(col).disallowAll();
                if (inv <= Figures::kSecondInversion)
                    this->getFigures(col).allow(Figures::packChord(Figures::kTriad, inv));
                if (inv <= Figures::kThirdInversion) {
                    this->getFigures(col).permission(Figures::packChord(Figures::kSeven, inv),
                                                     properties[kAllowSevenths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kAddTwo, inv),
                                                     properties[kAllowNinths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kAddFour, inv),
                                                     properties[kAllowElevenths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kAddSix, inv),
                                                     properties[kAllowThirteenths]);
                }
                if (inv <= Figures::kFourthInversion) {
                    this->getFigures(col).permission(Figures::packChord(Figures::kNine, inv),
                                                     properties[kAllowNinths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kEleven, inv),
                                                     properties[kAllowElevenths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kThirteen, inv),
                                                     properties[kAllowThirteenths]);
                }
            }
        }
    }
    
    void setup() {
        HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
        
        array< vector<tuple<bool,ScaleNote>>, rows > tracks;
        auto tr = tracks.begin();
        
        NSEnumerator *en = so.song.musicTracks.objectEnumerator;
        HarmonyWizMusicTrack *track;
        while ((track = en.nextObject) != nil) {
            vector<tuple<bool,ScaleNote>> vec = convertTrack(track);
            *tr = vec;
            tr++;
        }
        
        // setup solver parameters
        for (UInt16 i = 0; i < rows; i++) {
            HarmonyWizMusicTrack *track = [so.song.musicTracks objectAtIndex:i];
            this->setRange(i, std::make_tuple(track.lowestNoteAllowed - kChromaticShift,
                                                 track.highestNoteAllowed - kChromaticShift));
        }
        for (UInt16 i = 0; i < rows-2; i++) {
            this->setMaxPartDistance(i, 12);
        }
        this->setMaxPartDistance(rows-2, 19);
        
        // global properties
        bitset< kNumProperties > properties;
        for (UInt16 i = kCommonTone; i < kNumProperties; i++)
            properties[i] = [[so.harmonyRules objectAtIndex:i] boolValue];
        this->setAllGlobalProperties(properties);
        
        // prevent dissonances occurring on harmonies just after a rest
        // for some styles
        if (so.song.harmonyStyle < kStyle_Rudess2) {
            HarmonyWizHarmonyEvent *lastEvent = nil;
            size_t col = 0;
            NSEnumerator *en = so.song.harmony.events.objectEnumerator;
            HarmonyWizHarmonyEvent *he;
            while ((he = en.nextObject) != nil) {
                if (lastEvent) {
                    if (lastEvent.noChord && ! he.noChord) {
                        this->getFigures(col).disallowAll();
                        this->getFigures(col).allow(Figures::kTriad_Root);
                        this->getFigures(col).allow(Figures::kTriad_First);
                        this->getFigures(col).allow(Figures::kTriad_Second);
                    }
                }
                if (! he.noChord)
                    col++;
                lastEvent = he;
            }
        }
        
        // apply local constraints
        // nb: must be done after setting global properties
        {
            size_t col = 0;
            NSEnumerator *en = so.song.harmony.events.objectEnumerator;
            HarmonyWizHarmonyEvent *he;
            while ((he = en.nextObject) != nil) {
                if (! he.noChord) {
                    if (he.constraints && he.constraints.count > 0)
                        applyConstraints(he.constraints, col, properties);
                    else if (he.temporaryConstraints && he.temporaryConstraints.count > 0)
                        applyConstraints(he.temporaryConstraints, col, properties);
                    col++;
                }
            }
        }
        
        // set fuzzy rules
        this->setFuzzyCommonTones(0.0001);
        this->setFuzzyMinimumScore(0.005);
        switch (so.song.harmonyStyle) {
            case kStyle_Baroque:
                break;
            case kStyle_Contemporary:
                this->setFuzzyPrepareAndOrResolveSevenths(0.01);
                this->setFuzzyPrepareAndOrResolveNinths(0.01);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.01);
                break;
            case kStyle_Rudess1:
                this->setFuzzyDoubledThirds(0.05);
                this->setFuzzyTritoneLeaps(0.01);
                this->setFuzzyParallelFifths(0.01);
                this->setFuzzyPrepareAndOrResolveSevenths(0.005);
                this->setFuzzyPrepareAndOrResolveNinths(0.005);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.005);
                break;
            case kStyle_Rudess2:
                this->setFuzzyParallelFifths(0.5);
                this->setFuzzyPrepareAndOrResolveSevenths(0.01);
                this->setFuzzyPrepareAndOrResolveNinths(0.01);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.01);
                break;
            case kStyle_Rudess3:
                this->setFuzzyParallelFifths(0.7);
                this->setFuzzyPrepareAndOrResolveSevenths(0.01);
                this->setFuzzyPrepareAndOrResolveNinths(0.01);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.01);
                break;
            case kStyle_Rudess4:
                this->setFuzzyParallelFifths(0.9);
                this->setFuzzyPrepareAndOrResolveSevenths(0.01);
                this->setFuzzyPrepareAndOrResolveNinths(0.01);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.01);
                break;
            default:
                break;
        }
        
        // set the minimum score
        if (so.harmonyScore && !so.song.anythingSelected) {
            this->setMinimumAllowedScore(so.harmonyScore.floatValue);
        }
        
        // set start and end points
        if (anythingSelected) {
            HarmonyWizMusicEvent *leftMostEvent = [so.song findFirstSelectedEvent];
            HarmonyWizMusicEvent *rightMostEvent = [so.song findLastSelectedEvent];
            this->startColumn = make_tuple(true,[so.song findFirstHarmonyColumnForEvent:leftMostEvent]);
            this->endColumn = make_tuple(true,[so.song findLastHarmonyColumnForEvent:rightMostEvent]+1);
        }
        
        // set notes
        size_t col;
        for (UInt16 i = 0; i < rows; i++) {
            col = 0;
            for (auto nt : tracks.at(i)) {
                if (get<0>(nt))
                    this->setNote(i, col, get<1>(nt));
                col++;
            }
        }
        
        // set non harmonic tones
        {
            size_t row = 0;
            NSEnumerator *en = so.song.musicTracks.objectEnumerator;
            HarmonyWizMusicTrack *track;
            while ((track = en.nextObject) != nil) {
                size_t col = 0;
                
                NSEnumerator *en1 = so.song.harmony.events.objectEnumerator;
                HarmonyWizHarmonyEvent *he;
                while ((he = en1.nextObject) != nil) {
                    if (! he.noChord) {
                        NSIndexSet *idxs = [so.song overlappingEvents:track.events
                                                                    forEvent:he];
                        if (idxs.count > 0) {
                            
                            NSUInteger currentIndex = idxs.firstIndex;
                            while (currentIndex != NSNotFound) {
                                id ev = [track.events objectAtIndex:currentIndex];
                                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                                    HarmonyWizNoteEvent *ne = ev;
                                    if (ne.isNonHarmonicTone) {
                                        ScaleNote nht(ne.noteValue,this->keyRoot,this->keyIsMinor);
                                        this->addNonHarmonicTone(nht,col);
                                    }
                                }
                                
                                currentIndex = [idxs indexGreaterThanIndex:currentIndex];
                            }
                        }
                        
                        col++;
                    }
                }
                row++;
            }
        }
    }
    
protected:
    const HarmonyWizViewController *viewController;
    const HarmonyWizSong *preservedSong;
    const bool anythingSelected;
    
    void clearArrangementBetweenStartAndEnd() const {
        HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
        NSArray *chordEvents = [so.song.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord = NO"]];
        if (get<0>(this->startColumn) || get<0>(this->endColumn)) {
            [so.song clearArrangementFrom:[chordEvents objectAtIndex:get<1>(this->startColumn)] to:[chordEvents objectAtIndex:get<1>(this->endColumn)-1]];
        }
        else {
            [so.song clearArrangement];
        }
    }
    
    virtual void madeProgress() const {
        HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
        [so.songLock lockWhenCondition:0];
        Solver<rows>::madeProgress();
        
        viewController.arrangeGuardColumn = this->getProgress();
        viewController.arrangeGuardCoverage = this->getLastGuardCoverage();
        
        size_t pos = this->getProgress();
        NSArray *chordEvents = [so.song.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord = NO"]];
        HarmonyWizHarmonyEvent *he = [chordEvents objectAtIndex:pos];
        
        if (pos > 0) {
            [so.song placePlaybackIndicatorAtEvent:he];
            clearArrangementBetweenStartAndEnd();
            plugBackProgress();
            
            size_t progress = this->getProgress() - this->getDovetailedStartColumn();
            float percentage = float(progress) / float(this->getDovetailedEndColumn() - this->getDovetailedStartColumn());
            dispatch_sync(dispatch_get_main_queue(), ^{
                viewController.arrangeHUD.progress = percentage;
                [viewController scrollToPlaybackPosition];
            });
        }
        else {
            so.song.playbackPosition = 0;
        }
        
        so.viewsNeedingAccessToSong = rows+2;
        [so.songLock unlockWithCondition:so.viewsNeedingAccessToSong];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [so.rulerControl setNeedsDisplay];
            [viewController redrawStaves];
        });
    }
    
    virtual void solutionFound() {
        Solver<rows>::solutionFound();
        
        // move progress to end of arrangement
        if (get<0>(this->endColumn))
            get<1>(this->lastGuardColumn) = get<1>(this->endColumn)-1;
        else
            get<1>(this->lastGuardColumn) = this->getCols()-1;
        
        HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
        if (so.harmonyScore)
            so.harmonyScore = [NSNumber numberWithFloat:MAX(this->score(), so.harmonyScore.floatValue)];
        else
            so.harmonyScore = [NSNumber numberWithFloat:this->score()];
        cout << *this;
        cout << "score: " << so.harmonyScore.floatValue << endl;
        
        NSLog(@"Solution found with score: %f", so.harmonyScore.floatValue);
        
        [so.songLock lockWhenCondition:0];
        
        clearArrangementBetweenStartAndEnd();
        plugBackProgress();
        so.song.playbackPosition = 0;
        
        so.viewsNeedingAccessToSong = rows+2;
        [so.songLock unlockWithCondition:so.viewsNeedingAccessToSong];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            viewController.arrangeHUD.progress = 1;
            [so.rulerControl setNeedsDisplay];
            [viewController redrawStaves];
        });
    }
    
    void plugBackProgress() const {
        HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
        
        {
            // plug solution back into the song
            size_t row = 0;
            NSEnumerator *en = so.song.musicTracks.objectEnumerator;
            HarmonyWizMusicTrack *track;
            size_t startCol = (get<0>(this->startColumn) ? get<1>(this->startColumn) : 0);
            size_t endCol = (get<0>(this->endColumn) ? MIN(get<1>(this->endColumn),this->getProgress()+1) : this->getProgress()+1);
            while ((track = en.nextObject) != nil) {
                size_t col = 0;
                
                NSMutableArray *newEvents = [NSMutableArray arrayWithCapacity:track.events.count];
                NSEnumerator *en1 = track.events.objectEnumerator;
                HarmonyWizMusicEvent *ev;
                while ((ev = en1.nextObject) != nil) {
                    if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && (((HarmonyWizNoteEvent*)ev).wasCreatedByUser || track.locked || col < startCol || col >= endCol)) {
                        [newEvents placeObject:ev];
                    }
                }
                
                NSEnumerator *en2 = so.song.harmony.events.objectEnumerator;
                HarmonyWizHarmonyEvent *he;
                while ((he = en2.nextObject) != nil) {
                    if (! he.noChord) {
                        NSIndexSet *idxs = [so.song overlappingEvents:track.events
                                                                    forEvent:he];
                        BOOL userEvent = NO;
                        if (idxs.count > 0) {
                            
                            NSUInteger currentIndex = idxs.firstIndex;
                            while (currentIndex != NSNotFound) {
                                id ev = [track.events objectAtIndex:currentIndex];
                                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                                    HarmonyWizNoteEvent *ne = ev;
                                    if (ne.wasCreatedByUser || track.locked || col < startCol || col >= endCol) {
                                        userEvent = YES;
                                        if (![newEvents containsObject:ne]) {
                                            [newEvents placeObject:ne];
                                        }
                                    }
                                }
                                
                                currentIndex = [idxs indexGreaterThanIndex:currentIndex];
                            }
                        }
                        if (!userEvent && col >= startCol && col < endCol) {
                            HarmonyWizNoteEvent *ne = [[HarmonyWizNoteEvent alloc] init];
                            ne.beat = he.beat;
                            ne.subdivision = he.subdivision;
                            ne.durationBeats = he.durationBeats;
                            ne.durationSubdivisions = he.durationSubdivisions;
                            ne.type = kNoteEventType_Auto;
                            const ScaleNote &sn = this->at(row,col);
                            ne.noteValue = sn.chromatic(so.song.keySign.root, so.song.keySign.tonality == kTonality_Minor) + kChromaticShift;
                            [newEvents placeObject:ne];
                        }
                        col++;
                        if (col >= this->getCols())
                            break;
                    }
                }
                track.events = newEvents;
                [so.song addFillerRestsToTrack:track];
                row++;
            }
        }
        
        // tie adjacent notes of same value if created by solver
        NSEnumerator *en = so.song.musicTracks.objectEnumerator;
        HarmonyWizMusicTrack *track;
        size_t row = 0;
        while ((track = en.nextObject) != nil) {
            NSMutableArray *newEvents = [NSMutableArray arrayWithCapacity:track.events.count];
            NSUInteger i, j;
            for (i = 0; i < track.events.count; i++) {
                HarmonyWizMusicEvent *evi = [track.events objectAtIndex:i];
                UInt16 dur = [so.song eventDuration:evi];
                for (j = i + 1; j < track.events.count; j++) {
                    HarmonyWizMusicEvent *evj = [track.events objectAtIndex:j];
                    BOOL tied = NO;
                    if ([evi isKindOfClass:[HarmonyWizNoteEvent class]] &&
                        [evj isKindOfClass:[HarmonyWizNoteEvent class]]) {
                        
                        HarmonyWizNoteEvent *nei = (HarmonyWizNoteEvent*)evi;
                        HarmonyWizNoteEvent *nej = (HarmonyWizNoteEvent*)evj;
                        if (nei.noteValue == nej.noteValue && !nei.wasCreatedByUser && !nej.wasCreatedByUser) {
                            dur += [so.song eventDuration:evj];
                            tied = YES;
                        }
                    }
                    if (! tied)
                        break;
                }
                i = j-1;
                HarmonyWizMusicEvent *newEvent = [evi copy];
                newEvent.durationBeats = dur / so.song.beatSubdivisions;
                newEvent.durationSubdivisions = dur % so.song.beatSubdivisions;
                
                // preserve selection
                if (anythingSelected) {
                    HarmonyWizTrack *pt = [preservedSong.musicTracks objectAtIndex:row];
                    NSIndexSet *preservedSelectedIndices = [preservedSong overlappingEvents:pt.events forEvent:newEvent];
                    NSArray *preservedSelectedObjects = [pt.events objectsAtIndexes:preservedSelectedIndices];
                    NSUInteger selectedCount = [[preservedSelectedObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected = YES"]] count];
                    newEvent.selected = (selectedCount > 0);
                }
                
                [newEvents placeObject:newEvent];
            }
            track.events = newEvents;
            row++;
        }
        
        // set harmony properties
        {
            NSUInteger col = 0;
            NSEnumerator *en = so.song.harmony.events.objectEnumerator;
            HarmonyWizHarmonyEvent *he;
            while ((he = en.nextObject) != nil) {
                if (! he.noChord) {
                    const Figures::ChordTypeWithInversion ch = this->getChord(col);
                    Figures::ChordType type;
                    Figures::ChordInversion inversion;
                    tie(type,inversion) = Figures::unpackChord(ch);
                    
                    // clear the properties
                    he.properties = [NSMutableDictionary dictionary];
                    
                    // root
                    [he.properties setObject:[NSValue valueWithHWScaleNote:this->getRoot(col).octaveMod()]
                                      forKey:HarmonyWizDegreeProperty];
                    
                    // third
                    if (this->getThirdPtr(col))
                        [he.properties setObject:[NSValue valueWithHWScaleNote:this->getThirdPtr(col)->octaveMod()] forKey:HarmonyWizThirdProperty];
                    
                    // fifth
                    if (this->getFifthPtr(col))
                        [he.properties setObject:[NSValue valueWithHWScaleNote:this->getFifthPtr(col)->octaveMod()] forKey:HarmonyWizFifthProperty];
                    
                    // seventh
                    if (this->getSeventhPtr(col))
                        [he.properties setObject:[NSValue valueWithHWScaleNote:this->getSeventhPtr(col)->octaveMod()] forKey:HarmonyWizSevenProperty];
                    
                    // ninth
                    if (this->getNinthPtr(col))
                        [he.properties setObject:[NSValue valueWithHWScaleNote:this->getNinthPtr(col)->octaveMod()] forKey:HarmonyWizNineProperty];
                    
                    // eleventh
                    if (this->getEleventhPtr(col))
                        [he.properties setObject:[NSValue valueWithHWScaleNote:this->getEleventhPtr(col)->octaveMod()] forKey:HarmonyWizElevenProperty];
                    
                    // thirteenth
                    if (this->getThirteenthPtr(col))
                        [he.properties setObject:[NSValue valueWithHWScaleNote:this->getThirteenthPtr(col)->octaveMod()] forKey:HarmonyWizThirteenProperty];
                    
                    // chord type
                    [he.properties setObject:[NSValue valueWithHWChordType:type]
                                      forKey:HarmonyWizChordTypeProperty];
                    
                    // inversion
                    [he.properties setObject:[NSValue valueWithHWChordInversion:inversion]
                                      forKey:HarmonyWizInversionProperty];
                    
                    col++;
                    if (col > this->getProgress())
                        break;
                }
            }
        }
    }
    
    vector<tuple<bool,ScaleNote>> convertTrack(HarmonyWizMusicTrack* track) {
        NSIndexSet *idxSet;
        HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
        bool found;
        vector<tuple<bool,ScaleNote>> outVec;
        
        outVec.clear();
        
        NSArray *unifiedEvents = [so.song unifiedEvents:track];
        
        NSEnumerator *en = so.song.harmony.events.objectEnumerator;
        HarmonyWizHarmonyEvent *he;
        while ((he = en.nextObject) != nil) {
            if (! he.noChord) {
                idxSet = [so.song overlappingEvents:unifiedEvents forEvent:he];
                found = false;
                if (idxSet.count > 0) {
                    NSUInteger currentIndex = idxSet.firstIndex;
                    while (currentIndex != NSNotFound) {
                        id ev = [unifiedEvents objectAtIndex:currentIndex];
                        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            HarmonyWizNoteEvent *ne = ev;
                            BOOL noteIsConstraint = (ne.type == kNoteEventType_Harmony || (ne.type == kNoteEventType_Auto && track.locked) || (anythingSelected && !he.selected && !ne.selected && ne.type == kNoteEventType_Auto));
                            if (noteIsConstraint) {
                                ScaleNote sn(ne.noteValue - kChromaticShift, this->keyRoot, this->keyIsMinor);
                                outVec.push_back(make_tuple(true,sn));
                                found = true;
                                break;
                            }
                        }
                        
                        currentIndex = [idxSet indexGreaterThanIndex:currentIndex];
                    }
                }
                if (!found) {
                    outVec.push_back(make_tuple(false,0));
                }
            }
        }
        
        return outVec;
    }
};

- (void)cancelArrangement {
    self.didCancelArrange = YES;
    if (sCancelPointer != nullptr) {
        *sCancelPointer = true;
    }
}

- (void)applyRulesForStyle:(enum HarmonyStyle)style {
    HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
    const uint16_t numbits = HarmonyWiz::kNumProperties;
    for (uint16_t i = 0; i < numbits; i++)
        [so.harmonyRules replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
    switch (style) {
        case kStyle_Rudess4:
        case kStyle_Rudess3:
            [so.harmonyRules replaceObjectAtIndex:HarmonyWiz::kPrepareNinths withObject:[NSNumber numberWithBool:NO]];
            [so.harmonyRules replaceObjectAtIndex:HarmonyWiz::kResolveNinths withObject:[NSNumber numberWithBool:NO]];
            [so.harmonyRules replaceObjectAtIndex:HarmonyWiz::kResolveSevenths withObject:[NSNumber numberWithBool:NO]];
        case kStyle_Rudess2:
            [so.harmonyRules replaceObjectAtIndex:HarmonyWiz::kPrepareSevenths withObject:[NSNumber numberWithBool:NO]];
        case kStyle_Rudess1:
            [so.harmonyRules replaceObjectAtIndex:HarmonyWiz::kCommonTone withObject:[NSNumber numberWithBool:NO]];
        case kStyle_Contemporary:
        case kStyle_Baroque:
            [so.harmonyRules replaceObjectAtIndex:HarmonyWiz::kBassAlwaysMoves withObject:[NSNumber numberWithBool:NO]];
            
        default:
            break;
    }
}

- (BOOL)arrangeMusic {
    @autoreleasepool {
        @synchronized(self) {
            BOOL result = NO;
            sCancelPointer = nullptr;
            string failureReason;
            self.harmonyFailureReason = nil;
            HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
            [self applyRulesForStyle:so.song.harmonyStyle];
            switch (so.song.musicTracks.count) {
                case 3:
                {
                    Arranger<3> arranger(self);
                    arranger.setup();
                    sCancelPointer = &arranger.getCancel();
                    result = arranger.solve();
                    failureReason = arranger.getLastGuardName();
#if TESTFLIGHTSDK
                    if (result)
                        [TestFlight passCheckpoint:@"Arrangement in three parts succeeded"];
#endif
                    sCancelPointer = nullptr;
                }
                    break;
                case 4:
                {
                    Arranger<4> arranger(self);
                    arranger.setup();
                    sCancelPointer = &arranger.getCancel();
                    result = arranger.solve();
                    failureReason = arranger.getLastGuardName();
#if TESTFLIGHTSDK
                    if (result)
                        [TestFlight passCheckpoint:@"Arrangement in four parts succeeded"];
#endif
                    sCancelPointer = nullptr;
                }
                    break;
                case 5:
                {
                    Arranger<5> arranger(self);
                    arranger.setup();
                    arranger.setThirdMaxCount(2);
                    sCancelPointer = &arranger.getCancel();
                    result = arranger.solve();
                    failureReason = arranger.getLastGuardName();
#if TESTFLIGHTSDK
                    if (result)
                        [TestFlight passCheckpoint:@"Arrangement in five parts succeeded"];
#endif
                    sCancelPointer = nullptr;
                }
                    break;
                default:
                    break;
            }
            if (!result) {
                self.harmonyFailureReason = [NSString stringWithCString:failureReason.c_str() encoding:[NSString defaultCStringEncoding]];
            }
            return result;
        }
    }
}

@end
