//
//  HarmonyWizAddController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizAddController.h"

NSString * const HarmonyWizAddBarNotification = @"AddBar";
NSString * const HarmonyWizRemoveBarNotification = @"RemoveBar";
NSString * const HarmonyWizClearSongNotification = @"ClearSong";
NSString * const HarmonyWizClearArrangementNotification = @"ClearArrangement";
NSString * const HarmonyWizRemoveTrackNotification = @"RemoveTrack";
NSString * const HarmonyWizFreezeTrackNotification = @"FreezeTrack";
NSString * const HarmonyWizClearTrackNotification = @"ClearTrack";

@interface HarmonyWizAddController ()

@end

@implementation HarmonyWizAddController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Add and remove";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 2;
            break;
        case 2:
            return 4;
            break;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Bars";
            break;
        case 1:
            return @"Song";
            break;
        case 2:
            return @"Tracks";
            break;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Add bar to end";
                    break;
                case 1:
                    cell.textLabel.text = @"Remove bar from end";
                    break;
            }
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Clear arrangement";
                    break;
                case 1:
                    cell.textLabel.text = @"Clear song";
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Create track";
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    break;
                case 1:
                    cell.textLabel.text = @"Remove selected track";
                    break;
                case 2:
                    cell.textLabel.text = @"Freeze selected track";
                    break;
                case 3:
                    cell.textLabel.text = @"Clear selected track";
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    [nc postNotificationName:HarmonyWizAddBarNotification object:self];
                    break;
                case 1:
                    [nc postNotificationName:HarmonyWizRemoveBarNotification object:self];
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0:
                    [nc postNotificationName:HarmonyWizClearArrangementNotification object:self];
                    break;
                case 1:
                    [nc postNotificationName:HarmonyWizClearSongNotification object:self];
                    break;
            }
            break;
        case 2:
            switch (indexPath.row) {
                case 0:
                    [self.navigationController pushViewController:self.createTrackController animated:YES];
                    break;
                case 1:
                    [nc postNotificationName:HarmonyWizRemoveTrackNotification object:self];
                    break;
                case 2:
                    [nc postNotificationName:HarmonyWizFreezeTrackNotification object:self];
                    break;
                case 3:
                    [nc postNotificationName:HarmonyWizClearTrackNotification object:self];
                    break;
            }
            break;
    }
}

@end
