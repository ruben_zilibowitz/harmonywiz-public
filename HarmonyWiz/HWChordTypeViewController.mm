//
//  HWChordTypeViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWChordTypeViewController.h"
#import "HWCollectionViewController.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "HWChordAttributesViewController.h"

enum {
    kChordType_Any,
    kChordType_Triad,
    kChordType_Seven,
    kChordType_AddTwo,
    kChordType_AddFour,
    kChordType_AddSix,
    kChordType_Nine,
    kChordType_Eleven,
    kChordType_Thirteen,
    kNumChordTypes
};

@interface HWChordTypeViewController ()

@end

@implementation HWChordTypeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    HWChordAttributesViewController *cavc = [self.navigationController.viewControllers objectAtIndex:0];
    NSValue *type = [cavc.harmonyEvent.constraints objectForKey:HarmonyWizChordTypeProperty];
    if (type) {
        HarmonyWiz::Figures::ChordType chordType = type.HWChordTypeValue;
        self.selectedRow = chordType+1;
    }
    else {
        self.selectedRow = 0;
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:self.selectedRow inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

/*
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (indexPath.row >= kChordType_Seven && doc.song.musicTracks.count < 4) {
        cell.userInteractionEnabled = NO;
        cell.textLabel.textColor = [UIColor grayColor];
    }
    else if (indexPath.row >= kChordType_Nine && doc.song.musicTracks.count < 5) {
        cell.userInteractionEnabled = NO;
        cell.textLabel.textColor = [UIColor grayColor];
    }
    else {
        cell.userInteractionEnabled = YES;
        cell.textLabel.textColor = [UIColor blackColor];
    }
}
*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)updateConstraintsForHarmonyEvent:(HarmonyWizHarmonyEvent*)harmonyEvent {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [doc.hwUndoManager startUndoableChanges];
    [harmonyEvent.properties removeAllObjects];
    if (self.selectedRow == 0) {
        [harmonyEvent.constraints removeObjectForKey:HarmonyWizChordTypeProperty];
    }
    else {
        NSUInteger row = self.selectedRow;
        if (doc.song.musicTracks.count == 3) {
            row = 1;
        }
        else if (doc.song.musicTracks.count == 4 && row > 5) {
            row -= 3;
        }
        [harmonyEvent.constraints setObject:[NSValue valueWithHWChordType:(HarmonyWiz::Figures::ChordType)(row-1)] forKey:HarmonyWizChordTypeProperty];
    }
    [doc.hwUndoManager finishUndoableChangesName:@"Chord type"];
}

@end
