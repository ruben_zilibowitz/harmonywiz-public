//
//  HarmonyWizDownloadSamplesViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSZipArchive/SSZipArchive.h"

extern NSString * const HWSampleDownloadCompleteNotification;

@interface HWSampleDownload : NSObject
@property (nonatomic,strong) NSString *downloadZipPath;
@property (nonatomic,strong) NSOutputStream *downloadStream;
@property (nonatomic, assign) uint32_t downloadSize, downloadCount;
@property (nonatomic,strong) NSURLConnection *presetConnection;
@property (nonatomic,strong) NSIndexPath *indexPath;
@end

@interface HarmonyWizDownloadSamplesViewController : UITableViewController <NSURLConnectionDelegate, SSZipArchiveDelegate>

@property (nonatomic,strong)  NSArray *currentInstrumentNames;
@property (nonatomic,strong) NSMutableSet *downloads;

- (void)renewListing;

@end
