//
// PPSparkleEmitterView.h
// Created by Particle Playground on 1/12/2013
//

#import "PPSparkleEmitterView.h"

@interface PPSparkleEmitterView ()
@property (nonatomic,strong) CAEmitterCell *sparkleCell;
@end

@implementation PPSparkleEmitterView

-(id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
	if (self) {
		self.backgroundColor = [UIColor clearColor];
	}
	
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	
	if (self) {
		self.backgroundColor = [UIColor clearColor];
	}
	
	return self;
}

+ (Class) layerClass {
    //configure the UIView to have emitter layer
    return [CAEmitterLayer class];
}

- (UIImage *)flippedVerticalImage:(UIImage*)image {
    UIGraphicsBeginImageContext(image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *flippedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return flippedImage;
}

- (UIImage*)sparkleImage {
    if (SYSTEM_VERSION_LESS_THAN(@"7"))
        return [self flippedVerticalImage:[UIImage imageNamed:@"images/sparkle"]];
    else
        return [UIImage imageNamed:@"images/sparkle"];
}

- (void)pause {
    [(CAEmitterLayer*)self.layer setValue:@(0.f) forKeyPath:@"emitterCells.sparkles.birthRate"];
}

- (void)resume {
    [(CAEmitterLayer*)self.layer setValue:@(40.f) forKeyPath:@"emitterCells.sparkles.birthRate"];
}

- (void)setup {
    CAEmitterLayer *emitterLayer = (CAEmitterLayer*)self.layer;
    
	emitterLayer.name = @"emitterLayer";
	emitterLayer.emitterPosition = CGPointMake(512.f, 167.5f);
	emitterLayer.emitterZPosition = 0;
    
	emitterLayer.emitterSize = CGSizeMake(1.f, 1.f);
	emitterLayer.emitterDepth = 0.00;
    
	emitterLayer.emitterShape = kCAEmitterLayerRectangle;
    
	emitterLayer.renderMode = kCAEmitterLayerAdditive;
    
	emitterLayer.seed = (uint32_t)time(NULL);
    
    
    
	
	// Create the emitter Cell
	self.sparkleCell = [CAEmitterCell emitterCell];
	
	self.sparkleCell.name = @"sparkles";
	self.sparkleCell.enabled = YES;
    
	self.sparkleCell.contents = (id)self.sparkleImage.CGImage;
	self.sparkleCell.contentsRect = CGRectMake(0.00, 0.00, 1.00, 1.00);
    
	self.sparkleCell.magnificationFilter = kCAFilterLinear;
	self.sparkleCell.minificationFilter = kCAFilterLinear;
	self.sparkleCell.minificationFilterBias = 0.00;
    
	self.sparkleCell.scale = 0.70;
	self.sparkleCell.scaleRange = 0.00;
	self.sparkleCell.scaleSpeed = 0.10;
    
	self.sparkleCell.color = [[UIColor colorWithRed:0.30 green:0.80 blue:0.80 alpha:1.00] CGColor];
	self.sparkleCell.redRange = 0.20;
	self.sparkleCell.greenRange = 0.70;
	self.sparkleCell.blueRange = 0.70;
	self.sparkleCell.alphaRange = 0.00;
    
	self.sparkleCell.redSpeed = 0.00;
	self.sparkleCell.greenSpeed = 0.00;
	self.sparkleCell.blueSpeed = 0.00;
	self.sparkleCell.alphaSpeed = 0.00;
    
	self.sparkleCell.lifetime = 1.00;
	self.sparkleCell.lifetimeRange = 0.50;
	self.sparkleCell.birthRate = 0.f;
	self.sparkleCell.velocity = 40.00;
	self.sparkleCell.velocityRange = 25.00;
	self.sparkleCell.xAcceleration = 0.00;
	self.sparkleCell.yAcceleration = 0.00;
	self.sparkleCell.zAcceleration = 0.00;
    
	// these values are in radians, in the UI they are in degrees
	self.sparkleCell.spin = 0.000;
	self.sparkleCell.spinRange = 12.566;
	self.sparkleCell.emissionLatitude = 0.000;
	self.sparkleCell.emissionLongitude = 0.000;
	self.sparkleCell.emissionRange = 6.283;
    
    
    emitterLayer.emitterCells = @[self.sparkleCell];
}

@end
