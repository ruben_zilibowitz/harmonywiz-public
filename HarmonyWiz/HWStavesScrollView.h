//
//  HWStavesScrollView.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizStaveView.h"
#import "HWTimeRulerScrollView.h"
#import "LBorderView.h"
#import "HWViewKeepingTop.h"

@interface HWLocatorTriangle : UIView
@end

@interface HWStavesScrollView : UIScrollView <UIPopoverControllerDelegate, UIGestureRecognizerDelegate>
- (void)createAllStaveViews;
//- (void)updateAllStaves;
- (void)updateAllStavesNow;
- (void)flushAllStaves;
- (void)updatePositionIndicator;
- (void)setNeedsLayoutStaves;
- (void)updateVisible:(BOOL)redraw;

- (void)createPencilLayerForStave:(HarmonyWizStaveView*)stave event:(HarmonyWizMusicEvent*)event;
- (void)updatePencilLayerForStave:(HarmonyWizStaveView*)stave event:(HarmonyWizMusicEvent*)event;
- (void)removePencilLayer;
- (void)setLocatorTriangleHidden:(BOOL)hidden;
- (CGPoint)convertPoint:(CGPoint)point fromStave:(HarmonyWizStaveView *)stave;
- (CGRect)convertRect:(CGRect)rect fromStave:(HarmonyWizStaveView*)stave;
- (void)updateTouchableFrames;
- (void)showSelectMenuIn:(CGRect)targetRect;
- (void)purgeQueues;
- (void)maintainQueueMaxSize;

@property (nonatomic,strong) HWViewKeepingTop *contentView;

@property (nonatomic,assign) BOOL isEditing;
@property (nonatomic,strong) NSArray *musicStaveViews;
@property (nonatomic,strong) HarmonyWizStaveView *harmonyStaveView;
@property (nonatomic,strong) UIView *positionIndicator;
@property (nonatomic,strong) HWLocatorTriangle *locatorTriangle;
@property (nonatomic,strong) LBorderView *selectBoxView;
@property (nonatomic,weak) HWTimeRulerScrollView *rulerControl;
@property (nonatomic,assign) SInt16 leftMostVisibleMeasure, rightMostVisibleMeasure;

@property (nonatomic,strong) NSMutableArray *staveLineViewQueue, *staveEventLabelQueue, *staveTieQueue, *staveHarmonyInfoLabelQueue, *staveBarLineQueue, *singleStaveLineViewQueue;
@property (nonatomic,strong) NSMutableArray *tailLabelQueue, *rhythmNoteHeadQueue, *legerLinesQueue;

@end

extern NSString * const HarmonyWizClefMenuNotification;
extern NSString * const HarmonyWizTimeSignatureMenuNotification;
extern NSString * const HarmonyWizKeySignatureMenuNotification;
extern NSString * const HarmonyWizEditConstraintsUserInfoKey;
extern NSString * const HarmonyWizEditConstraintsNotification;
extern NSString * const HarmonyWizPlacedEventNotification;
extern NSString * const HWSelectTrackNotification;
extern NSString * const HWDraggedLocatorTriangle;
