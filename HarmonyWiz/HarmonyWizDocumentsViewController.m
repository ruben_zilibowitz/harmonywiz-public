//
//  HarmonyWizDocumentsViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 26/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizDocumentsViewController.h"
#import "HarmonyWizSong.h"
#import "HarmonyWizSharedObjects.h"
#import "HarmonyWizViewController.h"
#import "NSString+Paths.h"

@interface HarmonyWizDocumentsViewController ()
@property (nonatomic,strong) NSArray *documents;
@end

@implementation HarmonyWizDocumentsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    self.navigationItem.title = @"Documents";
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)updateDocumentsArray {
    NSError *err = nil;
    NSString *documentsDirectory = [NSString documentsDirectory];
    NSArray *docs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&err];
    if (err) {
        [NSException raise:@"HarmonyWiz" format:@"%@", err.localizedDescription];
    }
    NSMutableArray *docsNoExt = [NSMutableArray arrayWithCapacity:docs.count];
    for (NSString *str in docs) {
        if ([str.pathExtension isEqualToString:@"hwz"])
            [docsNoExt addObject:str.stringByDeletingPathExtension];
    }
    self.documents = docsNoExt;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateDocumentsArray];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.documents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [self.documents objectAtIndex:indexPath.row];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        {
            NSString *documentsDirectory = [NSString documentsDirectory];
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            NSString *filePath = [[documentsDirectory stringByAppendingPathComponent:cell.textLabel.text] stringByAppendingPathExtension:@"hwz"];
            NSURL *fileURL = [NSURL fileURLWithPath:filePath];
            NSError *err = nil;
            [[NSFileManager defaultManager] removeItemAtURL:fileURL error:&err];
            if (err) {
                [NSException raise:@"HarmonyWiz" format:@"%@", err.localizedFailureReason];
            }
            [self updateDocumentsArray];
        }
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    {
        NSString *documentsDirectory = [NSString documentsDirectory];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *filePath = [[documentsDirectory stringByAppendingPathComponent:cell.textLabel.text] stringByAppendingPathExtension:@"hwz"];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        
        NSError *error;
        HarmonyWizSong *song = [HarmonyWizSong songWithURL:fileURL error:&error];
        if (error) {
            [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else {
            [self.viewController applySong:song];
            [self.viewController closeAllPopovers];
        }
    }
}

@end
