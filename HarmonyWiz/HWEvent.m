//
//  HWEvent.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWEvent.h"

@implementation HWEvent
+ (HWEvent*)eventFor:(HarmonyWizMusicEvent*)event {
    HWEvent *result = [[HWEvent alloc] init];
    result->_event = event;
    return result;
}
- (NSComparisonResult)compare:(HWEvent*)obj {
    return [self.event compare:obj.event];
}
- (void)setAlpha:(CGFloat)alpha {
    for (UIView *view in self.labels) {
        view.alpha = alpha;
    }
    for (UIView *view in self.ties) {
        view.alpha = alpha;
    }
    for (UIView *view in self.harmonyInfoLabels) {
        view.alpha = alpha;
    }
}
- (void)removeViewsFromSuperview {
    [self.labels makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.ties makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.harmonyInfoLabels makeObjectsPerformSelector:@selector(removeFromSuperview)];
}
@end
