//
//  NSFileManager+UniqueTemporaryDirectory.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/08/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (UniqueTemporaryDirectory)

- (NSString*)createUniqueTemporaryDirectory;

@end
