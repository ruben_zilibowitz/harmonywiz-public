//
//  HWData.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 12/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWData.h"
#import "HarmonyWizArchiver.h"
#import "HarmonyWizUnarchiver.h"
#import "HWAppDelegate.h"

@implementation HWData

- (id)initWithSong:(HarmonyWizSong *)song {
    if ((self = [super init])) {
        self.song = song;
    }
    return self;
}

- (id)init {
    return [self initWithSong:nil];
}

#pragma mark NSCoding

#define kVersionKey @"DataVersion"
#define kSongKey @"Song"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt32:[encoder systemVersion] forKey:kVersionKey];
    NSData * songData = [HarmonyWizArchiver archivedDataWithRootObject:self.song];
    [encoder encodeObject:songData forKey:kSongKey];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super init])) {
        [decoder decodeInt32ForKey:kVersionKey];
        NSData * songData = [decoder decodeObjectForKey:kSongKey];
        NSError *error = nil;
        self.openingSongError = nil;
        HarmonyWizSong *song = [HarmonyWizSong songWithData:songData error:&error];
        if (error) {
            NSLog(@"%@", error);
            recordError(error)
            self.openingSongError = error;
        }
        else {
            [self migrateIAP:song];
            self.song = song;
        }
    }
    return self;
}

- (void)migrateIAP:(HarmonyWizSong*)song {
    BOOL didMigrate = NO;
    
    for (HarmonyWizMusicTrack *track in song.musicTracks) {
        // ThumbJam pack 1 -> Wizdom acoustic 1
        if ([track.instrument isEqualToString:@"Tenor Sax (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Tenor Sax";
        }
        else if ([track.instrument isEqualToString:@"Trumpet (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Regal Trumpet";
        }
        else if ([track.instrument isEqualToString:@"Trombone (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Trombone";
        }
        else if ([track.instrument isEqualToString:@"Trumpet Harmon Mute (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Trumpet Harmon Mute";
        }
        else if ([track.instrument isEqualToString:@"Trombone Plunger (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Trombone Plunger";
        }
        else if ([track.instrument isEqualToString:@"Vibes (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Vibes";
        }
        
        // ThumbJam pack 2 -> Wizdom acoustic 2
        else if ([track.instrument isEqualToString:@"Bass Bowed (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Bass Bowed";
        }
        else if ([track.instrument isEqualToString:@"Cello (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Expressive Cello";
        }
        else if ([track.instrument isEqualToString:@"Violin (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Legato Violin";
        }
        else if ([track.instrument isEqualToString:@"Bass Plucked (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Bass Plucked";
        }
        else if ([track.instrument isEqualToString:@"Viola (TJ)"]) {
            didMigrate = YES;
            track.instrument = @"Viola";
        }
    }
    
    if (didMigrate) {
        [[[UIAlertView alloc] initWithTitle:@"Sound Packs" message:@"The names of some of your purchased sounds have changed in this update. Instrument names have been updated accordingly." delegate:nil cancelButtonTitle:@"OK, thanks!" otherButtonTitles:nil] show];
    }
}

@end
