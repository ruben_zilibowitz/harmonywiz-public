//
//  HWQuantisationMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 20/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWQuantisationMenu : UITableViewController
@property (nonatomic,strong) NSIndexPath *lastSelectedCell;
@end
