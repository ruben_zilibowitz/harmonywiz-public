//
//  HWTutorialViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWTutorialViewController.h"
#import "HWTutorialPage.h"
#import "HWTutorial.h"
#import "HWAppDelegate.h"

@interface HWTutorialViewController ()
@property (nonatomic,strong) HWTutorial *tutorialOpened;
@property (nonatomic,strong) NSDate *openDate;
@end

@implementation HWTutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 540, 40)];
    header.backgroundColor = [UIColor clearColor];
    header.opaque = NO;
    [self.view addSubview:header];
    
    UIButton *closeBtn = [UIButton buttonWithType:(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") ? UIButtonTypeSystem : UIButtonTypeRoundedRect)];
    closeBtn.frame = CGRectMake(540-80, 2, 70, header.frame.size.height - 4);
    [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:closeBtn];
    
//    UILabel *helpLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 2, 80, header.frame.size.height - 4)];
//    helpLabel.backgroundColor = [UIColor clearColor];
//    helpLabel.opaque = NO;
//    helpLabel.text = @"Help";
//    [header addSubview:helpLabel];
    
    self.openDate = [NSDate date];
}

- (void)setTutorialName:(NSString *)tutorialName {
    _tutorialName = tutorialName;
    self.tutorialOpened = [[HWTutorial alloc] initWithName:tutorialName];
    self.delegate = self.tutorialOpened;
    self.dataSource = self.tutorialOpened;
    HWTutorialPage *initialViewController = [self.tutorialOpened viewControllerAtIndex:0];
    [self setViewControllers:@[initialViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)close:(id)sender {
    [HWAppDelegate recordEvent:@"close_help_after_duration" segmentation:@{ @"name" : self.tutorialName } sum:(-[self.openDate timeIntervalSinceNow])];
    [self dismissViewControllerAnimated:YES completion:nil];
    self.tutorialOpened = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)viewWillLayoutSubviews{
//    [super viewWillLayoutSubviews];
//    NSLog(@"size %@", NSStringFromCGRect(self.view.superview.bounds));
//    self.view.superview.bounds = CGRectMake(0, 0, 540, 360);
//    self.view.superview.bounds = CGRectMake(0, 0, 540, 660);
//}

@end
