//
//  HarmonyWizMusicEvent+CPP.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizMusicEvent+CPP.h"
#include "ScaleNote.h"

using namespace HarmonyWiz;

@implementation HarmonyWizMusicEvent (CPP)

- (void)roundPitch:(HarmonyWizKeySignEvent*)keySign {
    if ([self isKindOfClass:[HarmonyWizNoteEvent class]]) {
        HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)self;
        ne.noteValue = ScaleNote(ne.noteValue,keySign.root,keySign.tonality).roundUp(keySign.tonality).chromatic(keySign.root,keySign.tonality);
        ne.enharmonicShift = kDefaultAccidental;
    }
}

- (void)roundPitchModal:(HarmonyWizModeEvent*)mode {
    if ([self isKindOfClass:[HarmonyWizNoteEvent class]]) {
        HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)self;
        ne.enharmonicShift = kDefaultAccidental;
        
        int closest = 12;
        BOOL found = NO;
        for (NSNumber *mp in mode.modePitches) {
            int dist = imod(ne.noteValue - (mp.intValue + mode.transposition),12);
            if (dist > 6) {
                dist -= 12;
            }
            if (std::abs(dist) < std::abs(closest)) {
                closest = dist;
                found = YES;
            }
        }
        if (found) {
            ne.noteValue -= closest;
        }
        // correct spelling
        const int shift = [[mode modeAccidentalTypes][imod(ne.noteValue,12)] intValue];
        [ne applyNoteShift:shift];
    }
}

- (void)makeRaised:(BOOL)raised forKey:(HarmonyWizKeySignEvent*)keySign {
    if (keySign.tonality == kTonality_Minor && [self isKindOfClass:[HarmonyWizNoteEvent class]]) {
        HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)self;
        ScaleNote sn(ne.noteValue,keySign.root,keySign.tonality);
        ScaleNote snom(sn.octaveMod());
        if (snom.note == 5 || snom.note == 6) {
            if (sn.isFlattened()) {
                // wtf?
                @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"result should not have flattened alteration" userInfo:nil];
            }
            sn.alteration = (raised ? ScaleNote::kAlteration_Sharpen : ScaleNote::kAlteration_None);
        }
        ne.noteValue = sn.chromatic(keySign.root,keySign.tonality);
        ne.enharmonicShift = kDefaultAccidental;
    }
}

@end
