//
//  Solver.cpp
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#include <iostream>
#include "Solver.h"

using namespace HarmonyWiz;
using namespace std;
//
//template <size_t rows>
//ostream& operator<<(ostream& stream, const Solver<rows>& solver) {
//    for (size_t r = 0; r < rows; r++) {
//        for (size_t c = 0; c < solver.cols(); c++) {
//            stream << solver.at(r,c) << " ";
//        }
//        stream << endl;
//    }
//    return stream;
//}
