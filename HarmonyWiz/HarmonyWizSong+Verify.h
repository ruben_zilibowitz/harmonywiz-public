//
//  HarmonyWizSong+Verify.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 14/07/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong.h"

@interface HarmonyWizSong (Verify)
- (void)checkAllEventsSorted;
@end
