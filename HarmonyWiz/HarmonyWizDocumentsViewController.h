//
//  HarmonyWizDocumentsViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 26/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HarmonyWizViewController;

@interface HarmonyWizDocumentsViewController : UITableViewController

@property (nonatomic,weak) IBOutlet HarmonyWizViewController *viewController;

@end
