//
//  HWAppDelegate.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HWAppDelegate.h"
#import "NSString+Paths.h"
#import "SCUI.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import "UIApplication+Version.h"
#import "NSData+Hex.h"
#import "NSDate+FormattedStrings.h"
#import "CargoManager/CargoManager.h"
#import "HWIAPContentDelegate.h"
#import "AudioController.h"
#import "GroundControl/NSUserDefaults+GroundControl.h"
#import "CargoBay/AFNetworking/AFNetworking.h"
#import "PPTopMostController-Files/UIViewController+PPTopMostController.h"
#import "Countly.h"

#define kSoundCloud_Client_ID       @"hiddenkey"
#define kSoundCloud_Client_Secret   @"hiddenkey"
#define kLinguiSDK_AppID            @"hiddenkey"

NSString * const HWOpenDocumentFromURL = @"HWOpenDocumentFromURL";

NSString * const HWKeyboardScrolls = @"HWKeyboardScrolls";
NSString * const HWRecordFromMIDI = @"HWRecordFromMIDI";
NSString * const HWExpertMode = @"HWExpertMode";
NSString * const HWAutoFix = @"HWAutoFix";
NSString * const HWAutoScroll = @"HWAutoScroll";
NSString * const HWAskedShowFirstLaunchTutorial = @"HWAskedShowFirstLaunchTutorial";

NSString * const HWOpenAnySongNotification = @"HWOpenAnySongNotification";

NSString * const HWCountlyAPIKey = @"hiddenkey";

extern NSString * const HWOrderedDocumentsFilename;
extern NSInteger const HWZoomTransitionImageViewTag;

@interface HWAppDelegate () <NSURLConnectionDelegate>
@property (nonatomic,assign) NSUInteger idleTimerDisabledCounter;
@property (nonatomic,assign) BOOL anyDownloadsCancelled;
@end

@implementation HWAppDelegate

+ (void)initialize
{
    srand48(time(NULL));
    
    [SCSoundCloud  setClientID:kSoundCloud_Client_ID
                        secret:kSoundCloud_Client_Secret
                   redirectURL:[NSURL URLWithString:@"HarmonyWiz.soundcloud://"]];
}

+ (void)clearTempDirectory {
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}

- (void)createSamplesDirectory {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *samplesFilePath = [NSString samplerFilesPath];
    
    // remove samples directory and recreate
    BOOL samplesExist = [fileManager fileExistsAtPath:samplesFilePath];
#if kRefreshSamplesDirectory
    if (samplesExist) {
        [fileManager removeItemAtPath:samplesFilePath error:&error];
        if (error) {
            NSLog(@"%@", error);
            recordError(error)
        }
        samplesExist = NO;
    }
#endif
    if (!samplesExist) {
        [fileManager createDirectoryAtPath:samplesFilePath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:&error];
        if (error) {
            NSLog(@"%@", error);
            recordError(error)
        }
    }
}

- (void)cleanupSamplesDirectory {
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Sampler Files" withExtension:nil];
    NSString *samplesFilePath = [NSString samplerFilesPath];
    
    NSArray *dir = [fileManager contentsOfDirectoryAtURL:url includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    if (error) {
        NSLog(@"%@", error);
        recordError(error)
    }
    
    for (NSURL *fileURL in dir) {
        NSString *path = [samplesFilePath stringByAppendingPathComponent:fileURL.lastPathComponent];
        BOOL samplesExist = [fileManager fileExistsAtPath:path];
        if (samplesExist) {
            [fileManager removeItemAtPath:path error:&error];
            if (error) {
                NSLog(@"%@", error);
                recordError(error)
            }
        }
    }
}

- (void)cleanupOldDirectories {
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // move documents to new location
    {
        error = nil;
        NSString *oldDocuments = [NSString documentsDirectory];
        NSArray *dir = [fileManager contentsOfDirectoryAtPath:oldDocuments error:&error];
        if (error) {
            NSLog(@"%@", error);
            recordError(error)
        }
        for (NSString *filename in dir) {
            if ([filename isEqualToString:HWOrderedDocumentsFilename] || [filename hasSuffix:HW_EXTENSION]) {
                NSString *src = [oldDocuments stringByAppendingPathComponent:filename];
                NSString *dest = [[NSString saveFilesDirectory] stringByAppendingPathComponent:filename];
                error = nil;
                if ([[NSFileManager defaultManager] fileExistsAtPath:dest]) {
                    [[NSFileManager defaultManager] removeItemAtPath:src error:&error];
                }
                else {
                    [[NSFileManager defaultManager] moveItemAtPath:src toPath:dest error:&error];
                }
                if (error) {
                    NSLog(@"%@", error);
                    recordError(error)
                }
            }
        }
    }
    
    // move samples to new location
    {
        error = nil;
        NSString *oldSamples = [[NSString cachesDirectory] stringByAppendingPathComponent:@"Sampler Files"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:oldSamples]) {
            NSArray *dir = [fileManager contentsOfDirectoryAtPath:oldSamples error:&error];
            if (error) {
                NSLog(@"%@", error);
                recordError(error)
            }
            for (NSString *filename in dir) {
                NSString *src = [oldSamples stringByAppendingPathComponent:filename];
                NSString *dest = [[NSString samplerFilesPath] stringByAppendingPathComponent:filename];
                error = nil;
                [[NSFileManager defaultManager] moveItemAtPath:src toPath:dest error:&error];
                if (error) {
                    NSLog(@"%@", error);
                    recordError(error)
                }
            }
            
            // remove old samples directory
            error = nil;
            [fileManager removeItemAtPath:oldSamples error:&error];
            if (error) {
                NSLog(@"%@", error.localizedDescription);
                recordError(error)
            }
        }
    }
}

+ (BOOL) hasCydia {
    // doesn't seem to work on iOS 9 any more
    return false;
    //NSURL* url = [NSURL URLWithString:@"cydia://package/com.example.package"];
    //return [[UIApplication sharedApplication] canOpenURL:url];
}

+ (BOOL) jailbroken {
    NSFileManager * fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:@"/private/var/lib/apt/"];
}

+ (BOOL) compromised {
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *info = [bundle infoDictionary];
    if ([info objectForKey:@"SignerIdentity"] != nil) {
        return YES;
    }
    return NO;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:@"Failed to send report" message:error.localizedDescription delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
}

+ (void)recordError_:(NSError*)error func:(const char*)func line:(int)line {
    NSMutableArray *parameters = [NSMutableArray array];
    if (error.localizedDescription.length > 0) {
        [parameters addObject:error.localizedDescription];
    }
    if (func != nil && strnlen(func, 256) < 256) {
        [parameters addObject:[NSString stringWithUTF8String:func]];
        [parameters addObject:@(line)];
    }
    
    [[Countly sharedInstance] recordEvent:@"error" segmentation:@{@"info" : parameters} count:1];
}

+ (void)recordEvent:(NSString*)event {
    if ([HWAppDelegate analyticsEnabled]) {
        [[Countly sharedInstance] recordEvent:event count:1];
    }
}

+ (void)recordEvent:(NSString*)event segmentation:(NSDictionary*)segmentation {
    if ([HWAppDelegate analyticsEnabled]) {
        [[Countly sharedInstance] recordEvent:event segmentation:segmentation count:1];
    }
}

+ (void)recordEvent:(NSString*)event segmentation:(NSDictionary*)segmentation sum:(double)sum {
    if ([HWAppDelegate analyticsEnabled]) {
        [[Countly sharedInstance] recordEvent:event segmentation:segmentation count:1 sum:sum];
    }
}

+ (void)setBackgroundAudioEnabled:(BOOL)enabled {
    [[NSUserDefaults standardUserDefaults] setBool:enabled forKey:@"background_audio_preference"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)newFileStartMode {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"new_file_start_mode_preference"];
}

+ (BOOL)analyticsEnabled {
    return YES;
//    return [[NSUserDefaults standardUserDefaults] boolForKey:@"analytics_preference"];
}

+ (BOOL)backgroundAudioEnabled {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"background_audio_preference"];
}

+ (void)receiveGroundControl {
    [[NSUserDefaults standardUserDefaults] registerDefaultsWithURL:[NSURL URLWithString:@"http://harmonywiz.com/defaults.plist"] success:^(NSDictionary *defaults){
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } failure:^(NSError *error){
        NSLog(@"%@", error.localizedDescription);
        recordError(error)
    }];
}

- (void)migrateIAP {
    BOOL didMigrate = NO;
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:[NSString samplerFilesPath] error:&error];
    if (error) {
        recordError(error)
    }
    
    // migrate sound files
    for (NSString *name in contents) {
        // ThumbJam pack 1 -> Wizdom acoustic 1
        if ([name isEqualToString:@"Tenor Sax (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Tenor Sax (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Tenor Sax"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Trumpet (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Trumpet (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Regal Trumpet"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Trombone (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Trombone (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Trombone"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Trumpet Harmon Mute (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Trumpet Harmon Mute (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Trumpet Harmon Mute"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Trombone Plunger (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Trombone Plunger (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Trombone Plunger"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Vibes (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Vibes (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Vibes"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        
        // ThumbJam pack 2 -> Wizdom acoustic 2
        else if ([name isEqualToString:@"Bass Bowed (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Bass Bowed (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Bass Bowed"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Cello (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Cello (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Expressive Cello"] error:&error];
        }
        else if ([name isEqualToString:@"Violin (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Violin (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Legato Violin"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Bass Plucked (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Bass Plucked (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Bass Plucked"] error:&error];
            if (error) {
                recordError(error)
            }
        }
        else if ([name isEqualToString:@"Viola (TJ)"]) {
            didMigrate = YES;
            error = nil;
            [fileManager moveItemAtPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Viola (TJ)"] toPath:[[NSString samplerFilesPath] stringByAppendingPathComponent:@"Viola"] error:&error];
            if (error) {
                recordError(error)
            }
        }
    }
    
    // migrate user presets
    NSMutableArray *migratedUserPresets = [NSMutableArray array];
    NSArray *userPresets = [NSArray arrayWithContentsOfFile:[NSString userPresetsPath]];
    for (NSDictionary *preset in userPresets) {
        NSMutableDictionary *migratedPreset = [NSMutableDictionary dictionary];
        for (NSString *key in preset.allKeys) {
            NSString *obj = [preset objectForKey:key];
            
            // ThumbJam pack 1 -> Wizdom acoustic 1
            if ([obj isEqualToString:@"Tenor Sax (TJ)"]) {
                obj = @"Tenor Sax";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Trumpet (TJ)"]) {
                obj = @"Regal Trumpet";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Trombone (TJ)"]) {
                obj = @"Trombone";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Trumpet Harmon Mute (TJ)"]) {
                obj = @"Trumpet Harmon Mute";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Trombone Plunger (TJ)"]) {
                obj = @"Trombone Plunger";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Vibes (TJ)"]) {
                obj = @"Vibes";
                didMigrate = YES;
            }
            
            // ThumbJam pack 2 -> Wizdom acoustic 2
            else if ([obj isEqualToString:@"Bass Bowed (TJ)"]) {
                obj = @"Bass Bowed";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Cello (TJ)"]) {
                obj = @"Expressive Cello";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Violin (TJ)"]) {
                obj = @"Legato Violin";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Bass Plucked (TJ)"]) {
                obj = @"Bass Plucked";
                didMigrate = YES;
            }
            else if ([obj isEqualToString:@"Viola (TJ)"]) {
                obj = @"Viola";
                didMigrate = YES;
            }
            
            [migratedPreset setObject:obj forKey:key];
        }
        [migratedUserPresets addObject:migratedPreset];
    }
    if (didMigrate) {
        [migratedUserPresets writeToFile:[NSString userPresetsPath] atomically:YES];
    }
    
    if (didMigrate) {
        [[[UIAlertView alloc] initWithTitle:@"Sound Packs" message:@"The names of some of your purchased sounds have changed in this update." delegate:nil cancelButtonTitle:@"OK, thanks!" otherButtonTitles:nil] show];
    }
}

- (void)listFonts {
    // List all fonts on iPhone
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
        }
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [HWAppDelegate clearTempDirectory];
    
    [self registerDefaultSettings];
    
    application.applicationSupportsShakeToEdit = YES;
    
    [self createSamplesDirectory];
    [self cleanupOldDirectories];
    [self cleanupSamplesDirectory];
    [self migrateIAP];
    
    // page control appearance
    UIPageControl *pc = [UIPageControl appearance];
    pc.backgroundColor = [UIColor blackColor];//UIColorFromRGB(0xDCDCDC);
    pc.pageIndicatorTintColor = [UIColor whiteColor];
    pc.currentPageIndicatorTintColor = UIColorFromRGB(0x8888ff);
    
    // Crashlytics
    [Crashlytics sharedInstance].delegate = self;
    [Fabric with:@[CrashlyticsKit]];
//    [Crashlytics startWithAPIKey:@"43ddafdad835ad5e9d1fa8e49b3d5c75a824cf77"];
    
    // CargoManager
    [[CargoManager sharedManager] setContentDelegate:[HWIAPContentDelegate sharedManager]];
    [[CargoManager sharedManager] loadStore];
    
    // Ground Control
    [HWAppDelegate receiveGroundControl];
    
    // First launch
    [self handleFirstEverLaunch];
    
    // Instantiate audio controller
    [AudioController sharedAudioController];
    
#if AUDIOBUS_SUPPORT
    // Audiobus
    // Watch for connections
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(audiobusConnectionsChanged:)
                                                     name:ABConnectionsChangedNotification
                                                   object:nil];
    }
#endif
    
    // Analytics
    [[Countly sharedInstance] startOnCloudWithAppKey:HWCountlyAPIKey];
    
    [self checkIfCracked];
    
    if (! [HWAppDelegate analyticsEnabled]) {
        [[Countly sharedInstance] recordEvent:@"analytics_disabled" count:1];
    }
    
    //Lingui SDK
    NSString* linguiApplicationID = kLinguiSDK_AppID;
    [Lingui setLanguageSelectionMethod:LinguiLanguageSelectionUsingInAppMenu];
//    [Lingui setLanguageSelectionMethod:LinguiLanguageSelectionUsingDevicePreferredLanguages];
    [Lingui setLogLevel:LinguiLogLevelInfoAndErrors];
    [Lingui setUploadState:LinguiUploadStateResourcesAndScreenshots];
//#ifdef DEBUG
    [Lingui startInMode:LinguiModeDev withApplicationID:linguiApplicationID];
//#else
//    [Lingui startInMode:LinguiModeLive withApplicationID:linguiApplicationID];
//#endif
    
    return YES;
}

- (void)checkIfCracked {
    const BOOL compromised = [HWAppDelegate compromised];
    const BOOL jailbroken = [HWAppDelegate jailbroken];
    const BOOL hasCydia = [HWAppDelegate hasCydia];
    if (compromised || jailbroken || hasCydia) {
        [[Countly sharedInstance] recordEvent:@"cracked" segmentation:@{ @"compromised" : @(compromised), @"jailbroken" : @(jailbroken), @"hasCydia" : @(hasCydia) } count:1];
    }
}

#if AUDIOBUS_SUPPORT
- (void)audiobusConnectionsChanged:(NSNotification*)notification {
    const BOOL connected = [AudioController sharedAudioController].audiobusController.connected;
    if (connected) {
        if (! [HWAppDelegate backgroundAudioEnabled]) {
            // should enable background audio to use AudioBus
            [HWAppDelegate setBackgroundAudioEnabled:YES];
            
            [[[UIAlertView alloc] initWithTitle:@"AudioBus Connected" message:@"Background audio was disabled. It has been enabled now because a connection to AudioBus was detected." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
    
    [HWAppDelegate recordEvent:@"audiobus_connections_changed" segmentation:@{ @"connected" : @(connected) }];
}
#endif

- (void)handleFirstEverLaunch {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"HWFirstEverLaunchCompleted"] == nil) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HWFirstEverLaunchCompleted"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)registerDefaultSettings {
    NSDictionary *userDefaultsDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                          @(YES), @"analytics_preference",
                                          @(YES), @"background_audio_preference",
                                          @"0", @"new_file_start_mode_preference",
                                          @(NO),HWExpertMode,
                                          @(YES),HWAutoFix,
                                          @(YES),HWAutoScroll,
                                          nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:userDefaultsDefaults];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    // Cancel downloads when entering background
    self.anyDownloadsCancelled = [[HWIAPContentDelegate sharedManager] cancelAllDownloads];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    if (self.anyDownloadsCancelled) {
        [[[UIAlertView alloc] initWithTitle:@"Downloads cancelled" message:@"Your downloads were cancelled because HarmonyWiz entered the background. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        self.anyDownloadsCancelled = NO;
    }
    
    // Ground Control
    [HWAppDelegate receiveGroundControl];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
//    [self importDocuments];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    NSError *error = nil;
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSFileManager defaultManager] removeItemAtPath:[NSString undoPointsPath] error:&error];
    if (error) {
        NSLog(@"%@", error);
        recordError(error)
    }
    
    [HWAppDelegate clearTempDirectory];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([url.scheme isEqualToString:@"HarmonyWiz1.1.audiobus"]) {
#if AUDIOBUS_SUPPORT
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
            if (! [AudioController sharedAudioController].audiobusController) {
                [[NSNotificationCenter defaultCenter] postNotificationName:HWOpenAnySongNotification object:self];
            }
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"AudioBus 2.2.2" message:@"Only support iOS 7 or greater. Please upgrade your version of iOS to use AudioBus in HarmonyWiz." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
#endif
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWOpenDocumentFromURL object:self userInfo:@{ @"url" : url }];
    }
    return YES;
}

#pragma mark - Crashlytics delegate

//- (void)crashlyticsDidDetectCrashDuringPreviousExecution:(Crashlytics *)crashlytics {
//}

- (void)cannotSendMailAlert {
    [[[UIAlertView alloc] initWithTitle:@"Cannot send mail" message:@"The device is currently not able to send mail." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)crashlyticsDidDetectReportForLastExecution:(CLSReport *)report completionHandler:(void (^)(BOOL submit))completionHandler {
    
    // submit the report to Crashlytics website
    completionHandler(YES);
    
    // handle it
    NSString *title = @"Crash report submitted";
    NSString *message = [NSString stringWithFormat:@"We are sorry HarmonyWiz had an issue on: %@. A crash report has been submitted. Press support to send us an email describing what you were doing when the problem occurred.", [[report crashedOnDate] mediumString]];
    RIButtonItem *cancel = [RIButtonItem itemWithLabel:@"Cancel"];
    RIButtonItem *support = [RIButtonItem itemWithLabel:@"Support" action:^{
        if (![MFMailComposeViewController canSendMail]) {
            [self cannotSendMailAlert];
            return;
        }
        
        MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
        [mailVC setSubject:[NSString stringWithFormat:@"Diagnostic report # %@", [report identifier]]];
        [mailVC setToRecipients:@[@"devbugs@harmonywiz.com"]];
        [mailVC setMessageBody:@"Please describe what you were doing when the app closed.\n\n" isHTML:NO];
        [mailVC setMailComposeDelegate:self];
        [[UIViewController topMostController] presentViewController:mailVC animated:YES completion:nil];
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message cancelButtonItem:cancel otherButtonItems:support, nil];
    
    [alert show];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        recordError(error)
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - idle timer

- (void)pushIdleTimerDisabled {
    self.idleTimerDisabledCounter = self.idleTimerDisabledCounter + 1;
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)popIdleTimerDisabled {
    if (self.idleTimerDisabledCounter > 0) {
        self.idleTimerDisabledCounter = self.idleTimerDisabledCounter - 1;
    }
    [self resetIdleTimerDisabled];
}

- (void)resetIdleTimerDisabled {
    if (self.idleTimerDisabledCounter == 0) {
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    }
}

@end
