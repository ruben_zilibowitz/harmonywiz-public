//
//  HWStyleViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWStyleViewController.h"
#import "HWCollectionViewController.h"

NSString * const HWChangedStyleNotification = @"HWChangedStyle";
NSString * const HWArrangeNotification = @"HWArrange";

enum {
    kSection_Styles = 0,
    kSection_Arrange,
    kNumSections
};

@interface HWStyleViewController ()
@property (nonatomic,strong) UITableViewCell *selectedCell;
@end

@implementation HWStyleViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    self.selectedCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:doc.metadata.harmonyStyle inSection:kSection_Styles]];
    self.selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    if (! self.btnArrange) {
        // means we are opening the menu from the song menu
        self.navigationItem.title = @"Arrange styles";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == kSection_Arrange && self.btnArrange) {
        cell.userInteractionEnabled = self.btnArrange.enabled;
        cell.textLabel.textColor = (self.btnArrange.enabled ? [UIColor blackColor] : [UIColor lightGrayColor]);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSection_Styles) {
        self.selectedCell.accessoryType = UITableViewCellAccessoryNone;
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.selectedCell = cell;
        
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        doc.metadata.harmonyStyle = (enum HarmonyStyle)indexPath.row;
        
        doc.metadata.harmonyScore = nil;
//        [doc.hwUndoManager startUndoableChanges];
        doc.metadata.harmonyStyle = (enum HarmonyStyle)indexPath.row;
//        [doc.hwUndoManager finishUndoableChangesName:@"Harmony Style"];
        [[NSNotificationCenter defaultCenter] postNotificationName:HWChangedStyleNotification object:self];
    }
    
    else if (indexPath.section == kSection_Arrange) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWArrangeNotification object:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
