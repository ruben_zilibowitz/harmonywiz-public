//
//  HarmonyWizError.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizError.h"

NSString * const HarmonyWizErrorDomain = @"HarmonyWizErrorDomain";

NSInteger const HarmonyWizSongVersionErrorCode = 1;

@implementation HarmonyWizError

+ (HarmonyWizError*)songVersionErrorWithReason:(NSString*)reason {
    return (HarmonyWizError*)[NSError errorWithDomain:HarmonyWizErrorDomain
                               code:HarmonyWizSongVersionErrorCode
                           userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Song was saved with an older version of HarmonyWiz that is no longer supported.", NSLocalizedDescriptionKey, reason, NSLocalizedFailureReasonErrorKey, nil]];
}

+ (HarmonyWizError*)generalErrorWithName:(NSString*)name reason:(NSString*)reason {
    return (HarmonyWizError*)[NSError errorWithDomain:HarmonyWizErrorDomain
                               code:HarmonyWizSongVersionErrorCode
                           userInfo:[NSDictionary dictionaryWithObjectsAndKeys:name, NSLocalizedDescriptionKey, reason, NSLocalizedFailureReasonErrorKey, nil]];
}

@end
