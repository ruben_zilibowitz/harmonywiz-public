//
//  HarmonyWizError.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HarmonyWizError : NSError

+ (HarmonyWizError*)songVersionErrorWithReason:(NSString*)reason;
+ (HarmonyWizError*)generalErrorWithName:(NSString*)name reason:(NSString*)reason;

@end
