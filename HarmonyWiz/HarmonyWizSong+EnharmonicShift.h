//
//  HarmonyWizSong+EnharmonicShift.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 1/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong.h"

@interface HarmonyWizSong (EnharmonicShift)
- (BOOL)canNote:(HarmonyWizNoteEvent*)ne performEnharmonicShiftTo:(enum EnharmonicShift)shift;
@end
