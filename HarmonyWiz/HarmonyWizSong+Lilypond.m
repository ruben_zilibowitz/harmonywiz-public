//
//  HarmonyWizSong+Lilypond.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong+Lilypond.h"

#import "NSString+Concatenation.h"
#import "HarmonyWizSong+SplitEvent.h"

#include "divmod.h"

@implementation HarmonyWizSong (Lilypond)

- (NSString*)stringWithoutWhitespace:(NSString*)str {
    NSArray* words = [str componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceCharacterSet]];
    NSString* nospacestring = [words componentsJoinedByString:@""];
    return nospacestring;
}

- (NSString*)lilypondScript {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
    // some useful constants
    NSArray *majorKeyNames = @[@"c",@"des",@"d",@"ees",@"e",@"f",@"ges",@"g",@"aes",@"a",@"bes",@"b"];
    NSArray *minorKeyNames = @[@"c",@"cis",@"d",@"ees",@"e",@"f",@"fis",@"g",@"gis",@"a",@"bes",@"b"];
    NSArray *keyNames = (self.keySign.tonality == kTonality_Major ? majorKeyNames : minorKeyNames);
    NSArray *tonalityNames = @[@"major",@"minor"];
    NSArray *clefNames = @[@"treble",@"alto",@"tenor",@"bass"];
    NSArray *durationNames = @[@"_", @"16", @"8", @"8.",
                               @"4", @"_", @"4.", @"4..",
                               @"2", @"_", @"_", @"_",
                               @"2.", @"_", @"2..", @"_",
                               @"1", @"_", @"_", @"_",
                               @"_", @"_", @"_", @"_",
                               @"1.", @"_", @"_", @"_",
                               @"_", @"_", @"_", @"_",
                               @"\breve", @"_", @"_", @"_",
                               @"_", @"_", @"_", @"_",
                               @"_", @"_", @"_", @"_",
                               @"_", @"_", @"_", @"_",
                               @"\breve."];
    NSArray *noteNames = @[@[@"c",@"cis",@"d",@"dis",@"e",@"f",@"fis",@"g",@"gis",@"a",@"ais",@"b"],
                           @[@"c",@"des",@"d",@"ees",@"e",@"f",@"ges",@"g",@"aes",@"a",@"bes",@"b"],
                           @[@"bis",@"cis",@"d",@"dis",@"e",@"eis",@"fis",@"g",@"gis",@"a",@"ais",@"b"],
                           @[@"c",@"cis",@"d",@"ees",@"e",@"f",@"fis",@"g",@"aes",@"a",@"bes",@"b"],
                           @[@"bis",@"cis",@"cisis",@"dis",@"e",@"eis",@"fis",@"fisis",@"gis",@"a",@"ais",@"b"],
                           @[@"c",@"cis",@"d",@"dis",@"e",@"f",@"fis",@"g",@"gis",@"a",@"bes",@"b"],
                           @[@"c",@"des",@"d",@"ees",@"e",@"f",@"ges",@"g",@"aes",@"a",@"bes",@"ces"],
                           @[@"c",@"cis",@"d",@"dis",@"e",@"eis",@"fis",@"g",@"gis",@"a",@"ais",@"b"],
                           @[@"c",@"des",@"d",@"ees",@"e",@"f",@"fis",@"g",@"aes",@"a",@"bes",@"b"],
                           @[@"bis",@"cis",@"d",@"dis",@"e",@"eis",@"fis",@"fisis",@"gis",@"a",@"ais",@"b"],
                           @[@"c",@"cis",@"d",@"ees",@"e",@"f",@"fis",@"g",@"gis",@"a",@"bes",@"b"],
                           @[@"bis",@"cis",@"cisis",@"dis",@"e",@"eis",@"fis",@"fisis",@"gis",@"gisis",@"ais",@"b"]];
    NSArray *noteNamesForKey = [noteNames objectAtIndex:self.keySign.relativeMajorRoot];
    NSArray *octaveNames = @[@",,,,",@",,,",@",,",@",",@"",@"'",@"''",@"'''",@"''''"];
    
    NSArray *lines1 =
    @[
      [NSString stringWithFormat:@"%% Created on %@", [dateFormatter stringFromDate:[NSDate date]]],
      @"\\version \"2.12.2\"",
      @"",
      @"\\header {",
      @"    title = \"HarmonyWiz Song\"",
      [@"    composer = \"" : [[UIDevice currentDevice] name] : @"\""],
      @"}\n\n"
      ];
    
    NSMutableArray *lines2 = [NSMutableArray arrayWithCapacity:self.musicTracks.count];
    
    for (HarmonyWizMusicTrack *mt in self.musicTracks) {
        
        NSArray *trackLines = @[ [self stringWithoutWhitespace:[@"inst" : mt.title]] ];
        
        NSMutableArray *notes = [NSMutableArray arrayWithCapacity:mt.events.count];
        [notes addObject:@" = {"];
        if (mt == [self.musicTracks objectAtIndex:0]) {
            [notes addObject:[NSString stringWithFormat:@"\\tempo 4 = %d", self.tempo]];
        }
        for (HarmonyWizMusicEvent *originalEvent in mt.events) {
            NSArray *splittedEvents = [self recursivelySplitEvent:originalEvent];
            for (HarmonyWizMusicEvent *splittedEvent in splittedEvents) {
                UInt16 semiquaversDur = ([self eventDuration:splittedEvent] / (self.beatSubdivisions/4) * (4.f/self.timeSign.lowerNumeral));
                if (semiquaversDur > 0) {
                    NSString *durString = [durationNames objectAtIndex:semiquaversDur];
                    if ([durString isEqualToString:@"_"]) {
                        [NSException raise:@"HarmonyWiz" format:@"duration %d not supported by Lilypond", semiquaversDur];
                    }
                    if ([splittedEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
                        HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)splittedEvent;
                        SInt16 note = imod(ne.noteValue, 12);
                        NSString *noteString = [noteNamesForKey objectAtIndex:note];
                        SInt16 octave = idiv(ne.noteValue, 12) + 1;
                        if ([noteString isEqualToString:@"bis"])
                            octave--;
                        else if ([noteString isEqualToString:@"ces"])
                            octave++;
                        NSString *octaveString = [octaveNames objectAtIndex:octave];
                        [notes addObject:[noteString : octaveString : durString : (splittedEvent != splittedEvents.lastObject ? @"~" : @"")]];
                    }
                    else if ([splittedEvent isKindOfClass:[HarmonyWizRestEvent class]]) {
                        [notes addObject:[@"r" stringByAppendingString:durString]];
                    }
                }
            }
        }
        [notes addObject:@"\\bar \"|.\""];
        [notes addObject:@"}"];
        
        NSString *notesString = [notes componentsJoinedByString:@" "];
        [lines2 addObject:
         [[trackLines componentsJoinedByString:@"\n"] stringByAppendingFormat:@"%@\n\n", notesString]];
    }
    
    NSMutableArray *lines3 = [NSMutableArray arrayWithObjects:
                              @"\\score {",
                              @"   <<",
                              nil];
    
    // staves
    NSString *keyName = [keyNames objectAtIndex:self.keySign.root];
    NSString *tonalityName = [tonalityNames objectAtIndex:self.keySign.tonality];
    NSString *timeSignString = [NSString stringWithFormat:@"%d/%d", self.timeSign.upperNumeral, self.timeSign.lowerNumeral];
    for (HarmonyWizMusicTrack *mt in self.musicTracks) {
        NSString *clefName = [clefNames objectAtIndex:mt.clef];
        [lines3 addObject:[@"\\new Staff { \\clef " concat:clefName , @" \\key " , keyName , @" \\", tonalityName , @" \\time " ,  timeSignString, @" \\inst", [self stringWithoutWhitespace:mt.title], @" }", nil]];
    }
    
    // piano reduction
    NSString *pianoReduction = nil;
    if (self.musicTracks.count <= 4 &&
        [[self.musicTracks objectAtIndex:0] clef] == kClef_Treble &&
        [[self.musicTracks objectAtIndex:1] clef] == kClef_Treble &&
        [[self.musicTracks objectAtIndex:2] clef] == kClef_Bass &&
        (self.musicTracks.count==4?[[self.musicTracks objectAtIndex:3] clef] == kClef_Bass:YES)) {
        
        NSString *staff1 = [@"\\new Staff \\with { printPartCombineTexts = ##f } { \\clef treble \\key " concat: keyName, @" \\", tonalityName, @" \\time ", timeSignString, @" \\partcombine \\inst", [[self.musicTracks objectAtIndex:0] title], @" \\inst", [[self.musicTracks objectAtIndex:1] title], @" }", nil];
        
        NSString *staff2 = @"";
        if (self.musicTracks.count == 4) {
            staff2 = [@"\\new Staff \\with { printPartCombineTexts = ##f } { \\clef bass \\key " concat: keyName, @" \\", tonalityName, @" \\time ", timeSignString, @" \\partcombine \\inst", [[self.musicTracks objectAtIndex:2] title], @" \\inst", [[self.musicTracks objectAtIndex:3] title], @" }", nil];
        }
        else if (self.musicTracks.count == 3) {
            staff2 = [@"\\new Staff { \\clef bass \\key " concat: keyName, @" \\", tonalityName, @" \\time ", timeSignString, @" \\inst", [[self.musicTracks objectAtIndex:2] title], @" }", nil];
        }
        
        pianoReduction = [@[@"% Piano reduction",@"\\score {", @"<<",staff1,staff2,@">>",@"}"] componentsJoinedByString:@"\n"];
    }
    
    NSArray *lines4 = @[@"",
                        @"   >>",
                        @"}",
                        @"",
                        (pianoReduction ? pianoReduction : @""),
                        @"\\paper {",
                        @"}",
                        @""];
    
    return [NSString stringWithFormat:@"%@%@%@%@",
            [lines1 componentsJoinedByString:@"\n"],
            [lines2 componentsJoinedByString:@"\n"],
            [lines3 componentsJoinedByString:@"\n"],
            [lines4 componentsJoinedByString:@"\n"]];
}

@end
