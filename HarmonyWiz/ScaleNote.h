//
//  ScaleNote.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#ifndef HarmonyWiz_ScaleNote_h
#define HarmonyWiz_ScaleNote_h

extern "C" {
#include "divmod.h"
}
#include <iostream>

namespace HarmonyWiz {

struct ScaleNote {
    int16_t note;
    enum ScaleNoteAlteration {
        kAlteration_None = 0,
        kAlteration_Sharpen,
        kAlteration_Flatten,
        kAlteration_SharpenDouble,
        kAlteration_FlattenDouble
    } alteration;
    
    ScaleNote(short aNote = 0, ScaleNoteAlteration aAlteration = kAlteration_None)
    : note(aNote), alteration(aAlteration) {}
    
    bool isSharpened() const { return alteration == kAlteration_Sharpen; }
    bool isFlattened() const { return alteration == kAlteration_Flatten; }
    bool isUnaltered() const { return alteration == kAlteration_None; }
    
    ScaleNote octaveMod() const { return ScaleNote(imod(note,7),alteration); }
    
    ScaleNote roundUp(const bool isMinor) const {
        if (isFlattened())
            return note;
        const int16_t noteMod = imod(note,7);
        if (isSharpened() && isMinor && !(noteMod == 5 || noteMod == 6))
            return (note+1);
        if (isSharpened() && !isMinor)
            return (note+1);
        return (*this);
    }
    
    ScaleNote roundDown(const bool isMinor) const {
        if (isFlattened())
            return (note-1);
        const int16_t noteMod = imod(note,7);
        if (isSharpened() && isMinor && !(noteMod == 5 || noteMod == 6))
            return note;
        if (isSharpened() && !isMinor)
            return note;
        return (*this);
    }
    
    ScaleNote& minorInc() {
        const short note0 = imod(note,7);
        if (note0 == 5 || note0 == 6) {
            if (isSharpened()) {
                alteration = kAlteration_None;
                note++;
            }
            else {
                alteration = kAlteration_Sharpen;
            }
        }
        else {
            note++;
            alteration = kAlteration_None;
        }
        return *this;
    }
    
    ScaleNote& minorDec() {
        const short note0 = imod(note,7);
        if (note0 == 5 || note0 == 6) {
            if (isSharpened()) {
                alteration = kAlteration_None;
            }
            else {
                if (note0 == 6)
                    alteration = kAlteration_Sharpen;
                note--;
            }
        }
        else {
            note--;
            if (imod(note,7) == 6)
                alteration = kAlteration_Sharpen;
            else
                alteration = kAlteration_None;
        }
        return *this;
    }
    
    bool operator==(ScaleNote arg) const { return note == arg.note && alteration == arg.alteration; }
    bool operator!=(ScaleNote arg) const { return note != arg.note || alteration != arg.alteration; }
    
    bool operator>(const ScaleNote& arg) const {
        if (note == arg.note) {
            return isSharpened() && !arg.isSharpened();
        }
        else {
            return note > arg.note;
        }
    }
    
    bool operator<(const ScaleNote& arg) const {
        if (note == arg.note) {
            return !isSharpened() && arg.isSharpened();
        }
        else {
            return note < arg.note;
        }
    }
    
    bool operator>=(const ScaleNote& arg) const {
        return (*this) > arg || (*this) == arg;
    }
    
    bool operator<=(const ScaleNote& arg) const {
        return (*this) < arg || (*this) == arg;
    }
    
    int16_t chromatic(uint8_t key, bool isMinor) const {
        int16_t degree = note % 7;
        int16_t octave = note / 7;
        const int16_t chromsAeolian[] = {0,2,3,5,7,8,10};
        const int16_t chromsIonian[] = {0,2,4,5,7,9,11};
        int16_t chromAlteration = 0;
        const int16_t *chroms;
        if (degree < 0) {
            degree += 7;
            octave--;
        }
        if (isMinor) {
            chroms = chromsAeolian;
        }
        else {
            chroms = chromsIonian;
        }
        switch (alteration) {
            case ScaleNote::kAlteration_Sharpen:
                chromAlteration = 1;
                break;
            case ScaleNote::kAlteration_SharpenDouble:
                chromAlteration = 2;
                break;
            case ScaleNote::kAlteration_Flatten:
                chromAlteration = -1;
                break;
            case ScaleNote::kAlteration_FlattenDouble:
                chromAlteration = -2;
                break;
            default:
                break;
        }
        
        return chroms[degree] + 12*octave + key + chromAlteration;
    }
    
    ScaleNote(int16_t chrom, int16_t key, bool isMinor, bool enharmonicShift = false) {
        
        // convenience lambda function
        auto s = [enharmonicShift](int16_t x){ return (enharmonicShift ? ScaleNote(x+1,ScaleNote::kAlteration_Flatten) : ScaleNote(x,ScaleNote::kAlteration_Sharpen)); };
        
        short degree = (chrom-key) % 12;
        short octave = (chrom-key) / 12;
        const ScaleNote notesMajor[] = {0,s(0),1,s(1),2,3,s(3),4,s(4),5,s(5),6};
        const ScaleNote notesMinor[] = {0,s(0),1,2,s(2),3,s(3),4,5,s(5),6,s(6)};
        const ScaleNote *notes = isMinor ? notesMinor : notesMajor;
        
        if (degree < 0) {
            degree += 12;
            octave--;
        }
        
        note = notes[degree].note + 7*octave;
        alteration = notes[degree].alteration;
    }
    
    void forceDoubleSharp() {
        alteration = kAlteration_SharpenDouble;
        note --;
    }
    void forceDoubleFlat() {
        alteration = kAlteration_FlattenDouble;
        note ++;
    }
    void enharmonicShift() {
        if (alteration == kAlteration_Flatten) {
            alteration = kAlteration_Sharpen;
            note--;
        }
        else if (alteration == kAlteration_Sharpen) {
            alteration = kAlteration_Flatten;
            note++;
        }
    }
};
}

std::ostream& operator<<(std::ostream& stream, const HarmonyWiz::ScaleNote& note);

#endif
