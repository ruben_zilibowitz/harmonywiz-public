//
//  NSMutableArray+Sorted.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 6/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Sorted)

- (NSUInteger)indexOfInt:(int)number;
- (void)placeObject:(id)anObject;
- (void)placeObjectsFromArray:(NSArray *)otherArray;

@end
