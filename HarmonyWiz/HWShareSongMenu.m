//
//  HWShareSongMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWShareSongMenu.h"
#import "HWCollectionViewController.h"

NSString *const HWShareSongNotification = @"HWShareSongNotification";

@interface HWShareSongMenu ()

@end

@implementation HWShareSongMenu

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Export song";
}


- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.parentPopover dismissPopoverAnimated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (self.songSelectView) {
        [self.songSelectView shareSongWithMethod:cell.textLabel.text indexPath:indexPath];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWShareSongNotification object:self userInfo:@{ @"method" : cell.textLabel.text, @"indexPath" : indexPath }];
    }
}

@end
