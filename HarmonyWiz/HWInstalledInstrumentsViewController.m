//
//  HWInstalledInstrumentsViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWInstalledInstrumentsViewController.h"
#import "NSString+Paths.h"
#import "HWAppDelegate.h"

enum {
    kSection_PurchasedInstruments = 0,
    kSection_FactoryInstalledInstruments,
    kNumSections
};

@interface HWInstalledInstrumentsViewController ()
@property (nonatomic,strong) NSArray *factoryInstalledSamples;
@property (nonatomic,strong) NSArray *installedSamples;
@end

@implementation HWInstalledInstrumentsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateSamples];
    [self updateFactorySamples];
    self.navigationItem.title = @"Installed Instruments";
}

- (void)updateFactorySamples {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Sampler Files" withExtension:nil];
    NSError *error = nil;
    NSArray *installed = [fileManager contentsOfDirectoryAtURL:url includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    if (error) {
        [NSException raise:@"HarmonyWizError" format:@"contentsOfDirectoryAtURL: %@", error.localizedDescription];
        recordError(error)
    }
    else {
        self.factoryInstalledSamples = [installed sortedArrayUsingComparator:^NSComparisonResult(NSURL *obj1, NSURL *obj2){
            return [obj1.lastPathComponent localizedStandardCompare:obj2.lastPathComponent];
        }];
    }
}

- (void)updateSamples {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [NSURL fileURLWithPath:[NSString samplerFilesPath]];
    NSError *error = nil;
    NSArray *installed = [fileManager contentsOfDirectoryAtURL:url includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    if (error) {
        [NSException raise:@"HarmonyWizError" format:@"contentsOfDirectoryAtURL: %@", error.localizedDescription];
    }
    else {
        self.installedSamples = [installed sortedArrayUsingComparator:^NSComparisonResult(NSURL *obj1, NSURL *obj2){
            return [obj1.lastPathComponent localizedStandardCompare:obj2.lastPathComponent];
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == kSection_PurchasedInstruments) {
        if (self.installedSamples.count == 0)
            return 1;
        
        return self.installedSamples.count;
    }
    else {
        return self.factoryInstalledSamples.count;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == kSection_PurchasedInstruments) {
        return LinguiLocalizedString(@"Purchased Instruments", @"Purchased instruments menu title");
    }
    else if (section == kSection_FactoryInstalledInstruments) {
        return LinguiLocalizedString(@"Factory Installed Instruments", @"Factory instruments menu title");
    }
    return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (indexPath.section == kSection_PurchasedInstruments) {
        if (self.installedSamples.count == 0) {
            cell.textLabel.text = LinguiLocalizedString(@"Nothing installed", @"Nothing installed menu item");
        }
        else {
            NSURL *url = [self.installedSamples objectAtIndex:indexPath.row];
            cell.textLabel.text = url.lastPathComponent;
        }
    }
    else {
        NSURL *url = [self.factoryInstalledSamples objectAtIndex:indexPath.row];
        cell.textLabel.text = url.lastPathComponent;
    }
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSection_PurchasedInstruments) {
        // Return NO if you do not want the specified item to be editable.
        if (self.installedSamples.count == 0)
            return NO;
        
        return YES;
    }
    
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSURL *url = [self.installedSamples objectAtIndex:indexPath.row];
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            recordError(error)
        }
        else {
            [self updateSamples];
            if (self.installedSamples.count == 0) {
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSection_PurchasedInstruments] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            else {
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
