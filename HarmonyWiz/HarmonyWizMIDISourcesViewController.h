//
//  HarmonyWizMIDISourcesViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGMidi/PGMidi.h"

@interface HarmonyWizMIDISourcesViewController : UITableViewController
@property (nonatomic,weak) id <PGMidiSourceDelegate> midiSourceDelegate;
@end
