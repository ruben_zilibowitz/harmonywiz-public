//
//  HWIAPContentDelegate.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CargoManager/CargoManager.h"

@interface HWIAPContentDelegate : NSObject <CargoManagerContentDelegate>
+ (id)sharedManager;
- (SKPaymentTransaction*)transactionForProductID:(NSString*)productID;
- (SKDownload*)downloadForProductID:(NSString*)productID;
- (BOOL)productPurchased:(NSString*)productID;
- (BOOL)cancelAllDownloads;
- (void)pauseAllDownloads;
- (void)resumeAllDownloads;
@end
