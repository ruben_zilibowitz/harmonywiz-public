//
//  HWFocusButtons.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 8/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWFocusButtons : UIView
@property (nonatomic,assign) enum HWFocusState { kFocus_MultiTrack = 0, kFocus_SingleTrack, kNumFocus } state;
@property (nonatomic,strong) UIButton *leftbutton, *rightButton;
@end
