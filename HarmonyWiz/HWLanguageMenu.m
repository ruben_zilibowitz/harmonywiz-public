//
//  HWLanguageMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 14/05/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWLanguageMenu.h"
#import "UIAlertView+Blocks.h"

@interface HWLanguageMenu ()
@property (nonatomic,strong) NSIndexPath *selectedCell;
@end

@implementation HWLanguageMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [Lingui getTargetLanguages].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HWLanguageCell" forIndexPath:indexPath];
    
    NSString *language = [[Lingui getTargetLanguages] objectAtIndex:indexPath.row];
    cell.textLabel.text = language;
    
    if ([language isEqualToString:[Lingui getCurrentLanguage]]) {
        self.selectedCell = indexPath;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row != self.selectedCell.row || self.selectedCell == nil) {
        NSString *language = [[Lingui getTargetLanguages] objectAtIndex:indexPath.row];
        RIButtonItem *cancelButton = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")];
        __weak typeof(self) weakSelf = self;
        RIButtonItem *okButton = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"OK", @"OK button") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.selectedCell = indexPath;
            [tableView reloadRowsAtIndexPaths:@[indexPath, self.selectedCell] withRowAnimation:UITableViewRowAnimationAutomatic];
            [Lingui setLanguage:language];
        }];
        [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Confirmation", @"Confirmation alert title") message:[NSString stringWithFormat:LinguiLocalizedString(@"Are you sure", @"Are you sure alert message"), language] cancelButtonItem:cancelButton otherButtonItems:okButton, nil] show];
    }
}

@end
