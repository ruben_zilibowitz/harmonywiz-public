//
//  HarmonyWizSong+Splice.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizSong+Splice.h"
#include <vector>
#include <set>
#include <algorithm>

#include "ScaleNote.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "NSMutableArray+Sorted.h"

using namespace std;

@implementation HarmonyWizSong (Splice)

// class used in sort and sweep algorithm for splice function
class SweepEvent {
    
public:
    
    enum SweepEventType {
        kStart = 0,
        kFinish
    };
    
private:
    
    UInt16 pos;
    SweepEventType type;
    HarmonyWizMusicEvent *event;
    
public:
    
    SweepEvent(UInt16 aPos, SweepEventType aType, HarmonyWizMusicEvent *aEvent) : pos(aPos), type(aType), event(aEvent) {}
    
    bool operator<(const SweepEvent &ev) const {
        return (pos < ev.pos);
    }
    
    UInt16 getPos() const { return pos; }
    SweepEventType getType() const { return type; }
    HarmonyWizMusicEvent *getEvent() const { return event; }
};

// uses a sort and sweep algorithm
- (HarmonyWizMusicTrack*)spliceTracks:(NSArray*)tracks {
    
    // vector of events to be sorted and swept
    vector< SweepEvent > sweepVec;
    
    // build the sweep vector
    for (HarmonyWizTrack *track in tracks) {
//        [track sortEvents];
        for (id ev in [self unifiedEvents:track.events]) {
            if ([ev isKindOfClass:[HarmonyWizMusicEvent class]] &&
                ![ev isKindOfClass:[HarmonyWizRestEvent class]]) {
                if ([ev isKindOfClass:[HarmonyWizHarmonyEvent class]]) {
                    if ([(HarmonyWizHarmonyEvent*)ev noChord])
                        continue;
                }
                if (!track.locked && [ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    if (((HarmonyWizNoteEvent*)ev).type == kNoteEventType_Auto)
                        continue;
                }
                sweepVec.push_back( SweepEvent([self eventStartSubdivision:ev],SweepEvent::kStart,ev) );
                sweepVec.push_back( SweepEvent([self eventEndSubdivision:ev],SweepEvent::kFinish,ev) );
            }
        }
    }
    
    // sort the events, preserving order of equivalent elements
    stable_sort(sweepVec.begin(), sweepVec.end());
    
    // set of active events
    set<HarmonyWizMusicEvent*> activeEvents;
    
    // create the result track
    HarmonyWizMusicTrack *result = [[HarmonyWizMusicTrack alloc] init];
    result.title = @"Spliced";
    result.events = [NSMutableArray array];
    
    // sweep the events
    for (auto it = sweepVec.begin(); it != sweepVec.end(); it++) {
        if (it->getType() == SweepEvent::kStart) {
            activeEvents.insert(it->getEvent());
        }
        else {
            activeEvents.erase(it->getEvent());
        }
        
        if (activeEvents.size() > 0) {
            auto jt = it+1;
            
            const UInt16 dur = jt->getPos() - it->getPos();
            
            if (dur > 0) {
                HarmonyWizMusicEvent *preferredEvent = it->getEvent();
                for (auto ae : activeEvents) {
                    if ([ae isKindOfClass:[HarmonyWizHarmonyEvent class]]) {
                        preferredEvent = ae;
                        break;
                    }
                }
                HarmonyWizMusicEvent *currentEvent = [preferredEvent copy];
                currentEvent.beat = it->getPos() / self.beatSubdivisions;
                currentEvent.subdivision = it->getPos() % self.beatSubdivisions;
                currentEvent.durationBeats = dur / self.beatSubdivisions;
                currentEvent.durationSubdivisions = dur % self.beatSubdivisions;
                [result.events placeObject:currentEvent];
            }
        }
    }
    
    // add filler rests
    [self addFillerRestsTo:result.events];
    
    return result;
}

- (void)applyDefaultConstraintsToEvents:(HarmonyWizHarmonyEvent*)he1 :(HarmonyWizHarmonyEvent*)he2 {
    
    [self applyDefaultConstraintsToEvent:he2 allowSubmediant:NO];
    
    if (he2.temporaryConstraints != nil) {
        BOOL shouldForcePerfectCadence = [[he2.temporaryConstraints objectForKey:HarmonyWizDegreeProperty] HWScaleNoteValue] == HarmonyWiz::ScaleNote(0);
        if (shouldForcePerfectCadence) {
            for (HarmonyWizMusicTrack *mt in self.musicTracks) {
                NSArray *unifiedEvents = [self unifiedEvents:mt.events];
                NSIndexSet *overlaps = [self overlappingEvents:unifiedEvents forEvent:he1];
                if (overlaps.count > 0) {
                    NSUInteger currentIndex = overlaps.firstIndex;
                    while (currentIndex != NSNotFound) {
                        id ev = [unifiedEvents objectAtIndex:currentIndex];
                        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            HarmonyWizNoteEvent *ne = ev;
                            if (ne.type == kNoteEventType_Harmony || (ne.type == kNoteEventType_Auto && mt.locked)) {
                                HarmonyWiz::ScaleNote sn(ne.noteValue,self.keySign.root,self.keySign.tonality);
                                HarmonyWiz::ScaleNote snom(sn.octaveMod());
                                if (mt == self.musicTracks.lastObject) {
                                    if (!(snom.note == 4 && snom.isUnaltered())) {
                                        shouldForcePerfectCadence = NO;
                                        break;
                                    }
                                }
                                else if (!((snom.note == 4 || snom.note == 6 || snom.note == 1) && snom.isUnaltered())) {
                                    shouldForcePerfectCadence = NO;
                                    break;
                                }
                            }
                        }
                        currentIndex = [overlaps indexGreaterThanIndex:currentIndex];
                    }
                }
            }
        }
        
        if (shouldForcePerfectCadence) {
            he1.temporaryConstraints = [NSMutableDictionary dictionary];
            [he1.temporaryConstraints setObject:[NSValue valueWithHWScaleNote:HarmonyWiz::ScaleNote(4)] forKey:HarmonyWizDegreeProperty];
            [he1.temporaryConstraints setObject:[NSValue valueWithHWChordInversion:HarmonyWiz::Figures::kRootPosition] forKey:HarmonyWizInversionProperty];
        }
        else {
            he1.temporaryConstraints = nil;
        }
    }
    else {
        [self applyDefaultConstraintsToEvent:he2 allowSubmediant:YES];
    }
}

- (void)applyDefaultConstraintsToEvent:(HarmonyWizHarmonyEvent*)he allowSubmediant:(BOOL)allowSubmediant {
    if (self.keySign.tonality == kTonality_Minor) {
        allowSubmediant = NO;
    }
    BOOL shouldForceTonic = YES;
    BOOL shouldForceSubmediant = YES;
    for (HarmonyWizMusicTrack *mt in self.musicTracks) {
        NSArray *unifiedEvents = [self unifiedEvents:mt.events];
        NSIndexSet *overlaps = [self overlappingEvents:unifiedEvents forEvent:he];
        if (overlaps.count > 0) {
            NSUInteger currentIndex = overlaps.firstIndex;
            while (currentIndex != NSNotFound) {
                id ev = [unifiedEvents objectAtIndex:currentIndex];
                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *ne = ev;
                    if (ne.type == kNoteEventType_Harmony || (ne.type == kNoteEventType_Auto && mt.locked)) {
                        HarmonyWiz::ScaleNote sn(ne.noteValue,self.keySign.root,self.keySign.tonality);
                        HarmonyWiz::ScaleNote snom(sn.octaveMod());
                        if (mt == self.musicTracks.lastObject) {
                            if (snom.note != 0) {
                                shouldForceTonic = NO;
                            }
                            if (snom.note != 5) {
                                shouldForceSubmediant = NO;
                            }
                        }
                        else {
                            if (!(snom.note == 0 || snom.note == 2 || snom.note == 4)) {
                                shouldForceTonic = NO;
                            }
                            if (!(snom.note == 0 || snom.note == 2 || snom.note == 5)) {
                                shouldForceSubmediant = NO;
                            }
                        }
                    }
                }
                currentIndex = [overlaps indexGreaterThanIndex:currentIndex];
            }
        }
    }
    
    he.temporaryConstraints = [NSMutableDictionary dictionary];
    [he.temporaryConstraints setObject:[NSValue valueWithHWChordType:HarmonyWiz::Figures::kTriad] forKey:HarmonyWizChordTypeProperty];
    [he.temporaryConstraints setObject:[NSValue valueWithHWChordInversion:HarmonyWiz::Figures::kRootPosition] forKey:HarmonyWizInversionProperty];
    if (shouldForceTonic) {
        [he.temporaryConstraints setObject:[NSValue valueWithHWScaleNote:HarmonyWiz::ScaleNote(0)] forKey:HarmonyWizDegreeProperty];
    }
    else if (shouldForceSubmediant && allowSubmediant) {
        [he.temporaryConstraints setObject:[NSValue valueWithHWScaleNote:HarmonyWiz::ScaleNote(5)] forKey:HarmonyWizDegreeProperty];
    }
    else {
//        he.temporaryConstraints = nil;
    }
}

- (void)applyDefaultConstraints {
    NSArray *chordEvents = self.chordEvents;
    for (NSUInteger idx = 0; idx < chordEvents.count; idx++) {
        HarmonyWizHarmonyEvent *hei = [chordEvents objectAtIndex:idx];
        [self applyDefaultConstraintsToEvent:hei allowSubmediant:YES];
        if (hei.pause) {
            continue;
        }
        NSUInteger jdx;
        for (jdx = idx+1; jdx < chordEvents.count; jdx++) {
            HarmonyWizHarmonyEvent *hej = [chordEvents objectAtIndex:jdx];
            if (hej.pause) {
                if (jdx - idx >= 2) {
                    [self applyDefaultConstraintsToEvents:[chordEvents objectAtIndex:jdx-1]:hej];
                }
                else {
                    [self applyDefaultConstraintsToEvent:hej allowSubmediant:YES];
                }
                break;
            }
        }
        idx = jdx;
    }
    
    if ([chordEvents.lastObject temporaryConstraints].count == 0) {
        if (chordEvents.count > 0) {
            [self applyDefaultConstraintsToEvent:[chordEvents objectAtIndex:0] allowSubmediant:YES];
        }
        if (chordEvents.count > 2) {
            [self applyDefaultConstraintsToEvents:[chordEvents objectAtIndex:chordEvents.count-2]:chordEvents.lastObject];
        }
    }
}

- (void)spliceHarmony {
    HarmonyWizTrack *result = [self spliceTracks:self.musicTracks];
    
    // convert all events to harmony
    NSMutableArray *newEvents = [NSMutableArray arrayWithCapacity:result.events.count];
    for (HarmonyWizMusicEvent *event in result.events) {
        if ([event isKindOfClass:[HarmonyWizHarmonyEvent class]]) {
            [newEvents placeObject:event];
        }
        else {
            HarmonyWizHarmonyEvent *he = [[HarmonyWizHarmonyEvent alloc] init];
            he.beat = event.beat;
            he.subdivision = event.subdivision;
            he.durationBeats = event.durationBeats;
            he.durationSubdivisions = event.durationSubdivisions;
            he.noChord = [event isKindOfClass:[HarmonyWizRestEvent class]];
            if (! he.noChord) {
                HarmonyWizEvent *ev0 = [self.harmony eventAtBeat:he.beat subdivision:he.subdivision];
                if (ev0 && [ev0 isKindOfClass:[HarmonyWizHarmonyEvent class]] &&
                    !((HarmonyWizHarmonyEvent*)ev0).noChord) {
                    HarmonyWizHarmonyEvent *he0 = (HarmonyWizHarmonyEvent*)ev0;
                    he.constraints = he0.constraints;
                    he.pause = he0.pause;
                }
                else {
                    he.constraints = [NSMutableDictionary dictionary];
                }
                he.properties = [NSMutableDictionary dictionary];
            }
            [newEvents placeObject:he];
        }
    }
    self.harmony.events = newEvents;
    
    if (! self.isModal) {
        [self applyDefaultConstraints];
    }
}

@end
