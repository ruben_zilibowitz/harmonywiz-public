//
//  HWModeMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWModeMenu.h"
#import "HWCollectionViewController.h"

NSString * const HWModeChangedNotification = @"HWModeChangedNotification";

@interface HWModeMenu ()
@property NSIndexPath *selectedPath;
@end

@implementation HWModeMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return LinguiLocalizedString(@"The mode is the scale used in your melody. It also governs the harmonies that are permitted. Note that 'Quartal major' is the major scale but the triads are built out of perfect fourths instead of thirds.", @"Table footer");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return kMode_NumModes;
}

+ (NSString*)modeNameForID:(enum ModeName)modeID {
    return [@[
              LinguiLocalizedString(@"Quartal major", @"Mode name"),
//              LinguiLocalizedString(@"Quartal Perfect Major", @"Mode name"),
              LinguiLocalizedString(@"Harmonic minor", @"Mode name"),
              LinguiLocalizedString(@"Whole tone", @"Mode name"),
              LinguiLocalizedString(@"Octatonic", @"Mode name"),
              LinguiLocalizedString(@"Messiaen Mode #3", @"Mode name"),
//              LinguiLocalizedString(@"Messiaen Mode #4", @"Mode name"),
//              LinguiLocalizedString(@"Messiaen Mode #5", @"Mode name"),
//              LinguiLocalizedString(@"Messiaen Mode #6", @"Mode name"),
              LinguiLocalizedString(@"Messiaen Mode #7", @"Mode name"),
              LinguiLocalizedString(@"Nohkan", @"Mode name"),
              LinguiLocalizedString(@"Youlan China", @"Mode name")]
            objectAtIndex:modeID];
}

+ (NSArray*)modePitchesForID:(enum ModeName)modeID {
    return [@[
              @[@(0),@(2),@(4),@(5),@(7),@(9),@(11)],  // quartal major
              //              @[@(0),@(2),@(4),@(5),@(7),@(9),@(11)],  // quartal perfect major
              //              @[@(0),@(2),@(3),@(5),@(7),@(8),@(10)],  // minor
              @[@(0),@(2),@(3),@(5),@(7),@(8),@(11)],  // harmonic minor
              @[@(0),@(2),@(4),@(6),@(8),@(10)],        // whole tone
              @[@(0),@(2),@(3),@(5),@(6),@(8),@(9),@(11)],        // octatonic
              @[@(0),@(2),@(3),@(4),@(6),@(7),@(8),@(10),@(11)],        // Messiaen 3
//              @[@(0),@(1),@(2),@(5),@(6),@(7),@(8),@(11)],        // Messiaen 4
              //              @[@(0),@(1),@(5),@(6),@(7),@(11)],        // Messiaen 5
//              @[@(0),@(2),@(4),@(5),@(6),@(8),@(10),@(11)],        // Messiaen 6
              @[@(0),@(1),@(2),@(3),@(5),@(6),@(7),@(8),@(9),@(11)],        // Messiaen 7
              @[@(0),@(2),@(5),@(6),@(8),@(9),@(11)],   // nohkan
              @[@(0),@(1),@(2),@(4),@(5),@(6),@(7),@(9),@(10)]  // youlan china
              ] objectAtIndex:modeID];
}

+ (NSArray*)modeSpellingForID:(enum ModeName)modeID {
    return [@[
              @[@(0),@(1),@(2),@(3),@(4),@(5),@(6)],  // quartal major
              @[@(0),@(1),@(2),@(3),@(4),@(5),@(6)],  // harmonic minor
              @[@(0),@(1),@(2),@(3),@(4),@(5)],        // whole tone
              @[@(0),@(1),@(2),@(3),@(3),@(4),@(5),@(6)],        // octatonic
              @[@(0),@(1),@(1),@(2),@(3),@(4),@(4),@(5),@(6)],        // Messiaen 3
              @[@(0),@(0),@(1),@(1),@(2),@(3),@(4),@(4),@(5),@(6)],        // Messiaen 7
              @[@(0),@(1),@(2),@(3),@(4),@(5),@(6)],   // nohkan
              @[@(0),@(0),@(1),@(2),@(2),@(3),@(4),@(5),@(6)]  // youlan china
              ] objectAtIndex:modeID];
}

//- (void)updateTonalCenterSegment {
//    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
//    if (doc.song.isModal) {
//        self.topTones.enabled = YES;
//        self.bottomTones.enabled = YES;
//        int transposition = (doc.song.songMode.transposition % 12);
//        if (transposition >= 6) {
//            transposition -= 12;
//        }
//        if (transposition >= 0) {
//            self.topTones.selectedSegmentIndex = transposition;
//            self.bottomTones.selectedSegmentIndex = -1;
//        }
//        else {
//            self.topTones.selectedSegmentIndex = -1;
//            self.bottomTones.selectedSegmentIndex = 6 + transposition;
//        }
//    }
//    else {
//        self.topTones.enabled = NO;
//        self.bottomTones.enabled = NO;
//        self.topTones.selectedSegmentIndex = -1;
//        self.bottomTones.selectedSegmentIndex = -1;
//    }
//}

//- (NSInteger)tonalCenterSelected {
//    if (self.topTones.selectedSegmentIndex >= 0) {
//        return self.topTones.selectedSegmentIndex;
//    }
//    else if (self.bottomTones.selectedSegmentIndex >= 0) {
//        return self.bottomTones.selectedSegmentIndex + 6;
//    }
//    return -1;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    cell = [tableView dequeueReusableCellWithIdentifier:@"HWModeCell" forIndexPath:indexPath];
    
    cell.textLabel.text = [HWModeMenu modeNameForID:(enum ModeName)indexPath.row];
    
    if (doc.song.songMode != nil && doc.song.songMode.modeID == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.selectedPath = indexPath;
    }
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}
//
//- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return @"Tonal Center";
//    }
//    else {
//        return @"Mode";
//    }
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return 88.f;
//    }
//    else {
//        return 44.f;
//    }
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (! [indexPath isEqual:self.selectedPath]) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        if (doc.song.musicTracks.count != 4) {
            [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Change mode",@"Alert title") message:LinguiLocalizedString(@"To use modes you must have four tracks. Modes are not supported with three tracks or five tracks.",@"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK",@"OK Button") otherButtonTitles:nil] show];
        }
        else {
            HarmonyWizModeEvent *modeEvent = [HarmonyWizModeEvent new];
            modeEvent.modeID = (int)indexPath.row;
            modeEvent.transposition = (doc.song.isModal ? doc.song.songMode.transposition : doc.song.keySign.root);
            modeEvent.modePitches = [HWModeMenu modePitchesForID:(enum ModeName)indexPath.row];
            modeEvent.spelling = [HWModeMenu modeSpellingForID:(enum ModeName)indexPath.row];
            
            [doc.hwUndoManager startUndoableChanges];
            if (modeEvent.modeID == kMode_QuartalMajor) {
                doc.song.triadsType = kQuartalPerfect;
            }
            else {
                doc.song.triadsType = kTertian;
            }
            [doc.song setSongMode:modeEvent];
            [doc.song roundNotesToMode];
            [doc.hwUndoManager finishUndoableChangesName:@"Change Mode"];
            
            if (self.selectedPath)
                [self.tableView reloadRowsAtIndexPaths:@[indexPath,self.selectedPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            else
                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            self.selectedPath = indexPath;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:HWModeChangedNotification object:self];
        }
    }
}
//
//- (void)handleTonalCenterChange {
//    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
//    
//    HarmonyWizModeEvent *modeEvent = [HarmonyWizModeEvent new];
//    modeEvent.modeID = (int)self.selectedPath.row;
//    modeEvent.transposition = self.tonalCenterSelected;
//    modeEvent.modePitches = [HWModeMenu modePitchesForID:(enum ModeName)self.selectedPath.row];
//    
//    [doc.hwUndoManager startUndoableChanges];
//    [doc.song setSongMode:modeEvent];
//    [doc.song roundNotesToMode];
//    [doc.hwUndoManager finishUndoableChangesName:@"Change Mode"];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:HWModeChangedNotification object:self];
//}
//
//- (IBAction)topToneSelected:(UISegmentedControl*)sender {
//    self.bottomTones.selectedSegmentIndex = -1;
//    [self handleTonalCenterChange];
//}
//
//- (IBAction)bottomToneSelected:(UISegmentedControl*)sender {
//    self.topTones.selectedSegmentIndex = -1;
//    [self handleTonalCenterChange];
//}

@end
