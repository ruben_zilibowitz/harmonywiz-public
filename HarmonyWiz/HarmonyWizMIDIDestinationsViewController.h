//
//  HarmonyWizMIDIDestinationsViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HarmonyWizMIDIDestinationsViewController : UITableViewController

@end
