//
//  HWSongViewController+Arrange.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HWSongViewController+Arrange.h"
#import "HWCollectionViewController.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "HWTimeRulerScrollView.h"
#import "NSMutableArray+Sorted.h"
#import "MBProgressHUD/MBProgressHUD.h"
#import "HarmonyWizSong+ScaleNote.h"
#include "Solver.h"

extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;

// nb: don't make this number too low or the arrangement will
// never get anywhere. Around 250000 works pretty well.
const unsigned long HWMaxGuardCallsRowMultiplier = 250000UL;

using namespace HarmonyWiz;

@implementation HWSongViewController (Arrange)

static bool *sCancelPointer = nullptr;

template <size_t rows>
class Arranger : public Solver<rows> {
public:
    Arranger(HWSongViewController *vc)
    : Solver<rows>([HWCollectionViewController currentlyOpenDocument].song.harmonyEventCount,
                   [HWCollectionViewController currentlyOpenDocument].song.keySign.root,
                   [HWCollectionViewController currentlyOpenDocument].song.keySign.tonality == kTonality_Minor),
    viewController(vc),
    lastProgress(new ArrangerState(*this,[HWCollectionViewController currentlyOpenDocument].song.anythingSelected,[[HWCollectionViewController currentlyOpenDocument].song copy]))
    {}
    
    virtual ~Arranger() {
        delete lastProgress;
    }
    
    void applyConstraints(NSDictionary *constraints, size_t col, const bitset< kNumProperties > &properties) {
        id obj;
        if ((obj = [constraints objectForKey:HarmonyWizRelaxRulesProperty]) != nil) {
            if ([obj boolValue] == YES) {
                this->setRelaxConstraint(col);
            }
        }
        if ((obj = [constraints objectForKey:HarmonyWizDegreeProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedRoot(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizThirdProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedThird(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizFifthProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedFifth(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizSevenProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedSeventh(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizNineProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedNinth(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizElevenProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedEleventh(col, val);
        }
        if ((obj = [constraints objectForKey:HarmonyWizThirteenProperty]) != nil) {
            ScaleNote val = [obj HWScaleNoteValue];
            this->setAllowedThirteenth(col, val);
        }
        id obj1 = [constraints objectForKey:HarmonyWizChordTypeProperty];
        id obj2 = [constraints objectForKey:HarmonyWizInversionProperty];
        if (obj1 && obj2) {
            Figures::ChordType ch = [obj1 HWChordTypeValue];
            Figures::ChordInversion inv = [obj2 HWChordInversionValue];
            Figures::ChordTypeWithInversion chinv = Figures::packChord(ch, inv);
            this->getFigures(col).disallowAll();
            this->getFigures(col).allow(chinv);
        }
        else {
            if (obj1) {
                Figures::ChordType ch = [obj1 HWChordTypeValue];
                Figures::ChordInversion lastInversion = Figures::kThirdInversion;
                if (ch <= Figures::kAddSix)
                    lastInversion=Figures::ChordInversionPrev(lastInversion);
                this->getFigures(col).disallowAll();
                for (Figures::ChordInversion inv = Figures::kRootPosition; inv <= lastInversion; inv=Figures::ChordInversionNext(inv)) {
                    this->getFigures(col).allow(Figures::packChord(ch, inv));
                }
            }
            if (obj2) {
                Figures::ChordInversion inv = [obj2 HWChordInversionValue];
                this->getFigures(col).disallowAll();
                if (inv <= Figures::kSecondInversion)
                    this->getFigures(col).allow(Figures::packChord(Figures::kTriad, inv));
                if (inv <= Figures::kThirdInversion) {
                    this->getFigures(col).permission(Figures::packChord(Figures::kSeven, inv),
                                                     properties[kAllowSevenths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kAddTwo, inv),
                                                     properties[kAllowNinths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kAddFour, inv),
                                                     properties[kAllowElevenths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kAddSix, inv),
                                                     properties[kAllowThirteenths]);
                }
                if (inv <= Figures::kFourthInversion) {
                    this->getFigures(col).permission(Figures::packChord(Figures::kNine, inv),
                                                     properties[kAllowNinths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kEleven, inv),
                                                     properties[kAllowElevenths]);
                    this->getFigures(col).permission(Figures::packChord(Figures::kThirteen, inv),
                                                     properties[kAllowThirteenths]);
                }
            }
        }
    }
    
    void setup() {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        const BOOL anythingSelected = doc.song.anythingSelected;
        
        array< vector<tuple<bool,ScaleNote>>, rows > tracks;
        auto tr = tracks.begin();
        
        NSEnumerator *en = doc.song.musicTracks.objectEnumerator;
        HarmonyWizMusicTrack *track;
        while ((track = en.nextObject) != nil) {
            vector<tuple<bool,ScaleNote>> vec = convertTrack(track);
            *tr = vec;
            tr++;
        }
        
        // global properties
        bitset< kNumProperties > properties;
        for (UInt16 i = kCommonTone; i < kNumProperties; i++)
            properties[i] = [[doc.metadata.harmonyRules objectAtIndex:i] boolValue];
        this->setAllGlobalProperties(properties);
        
        // prevent dissonances occurring on harmonies just after a rest
        // for some styles
        /*
        if (doc.metadata.harmonyStyle < kStyle_Rudess2) {
            HarmonyWizHarmonyEvent *lastEvent = nil;
            size_t col = 0;
            NSEnumerator *en = doc.song.harmony.events.objectEnumerator;
            HarmonyWizHarmonyEvent *he;
            while ((he = en.nextObject) != nil) {
                if (lastEvent) {
                    if (lastEvent.noChord && ! he.noChord) {
                        this->getFigures(col).disallowAll();
                        this->getFigures(col).allow(Figures::kTriad_Root);
                        this->getFigures(col).allow(Figures::kTriad_First);
                        this->getFigures(col).allow(Figures::kTriad_Second);
                    }
                }
                if (! he.noChord)
                    col++;
                lastEvent = he;
            }
        }
         */
        
        // apply local constraints
        // nb: must be done after setting global properties
        {
            size_t col = 0;
            NSEnumerator *en = doc.song.harmony.events.objectEnumerator;
            HarmonyWizHarmonyEvent *he;
            while ((he = en.nextObject) != nil) {
                if (! he.noChord) {
                    if (he.constraints && he.constraints.count > 0) {
                        applyConstraints(he.constraints, col, properties);
                        this->setConstraintUsed(col);
                    }
                    else if (he.temporaryConstraints && he.temporaryConstraints.count > 0) {
                        applyConstraints(he.temporaryConstraints, col, properties);
                    }
                    if (he.pause) {
                        this->setPause(col);
                    }
                    col++;
                }
            }
        }
        
        // set fuzzy rules
        this->setFuzzyCommonTones(0.2);
        this->setFuzzyMinimumScore(0.005);
        switch (doc.metadata.harmonyStyle) {
            case kStyle_Baroque:
                break;
            case kStyle_Contemporary:
                this->setFuzzyCommonTones(0.4);
                this->setFuzzyPrepareAndOrResolveSevenths(0.01);
                this->setFuzzyPrepareAndOrResolveNinths(0.01);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.01);
                break;
            case kStyle_Rudess1:
                this->setFuzzyDoubledThirds(0.05);
                this->setFuzzyTritoneLeaps(0.01);
                this->setFuzzyParallelFifths(0.01);
                this->setFuzzyPrepareAndOrResolveSevenths(0.005);
                this->setFuzzyPrepareAndOrResolveNinths(0.005);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.005);
                break;
            case kStyle_Rudess2:
                this->setFuzzyParallelFifths(0.5);
                this->setFuzzyPrepareAndOrResolveSevenths(0.01);
                this->setFuzzyPrepareAndOrResolveNinths(0.01);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.01);
                break;
            case kStyle_Rudess3:
                this->setFuzzyParallelFifths(0.7);
                this->setFuzzyPrepareAndOrResolveSevenths(0.01);
                this->setFuzzyPrepareAndOrResolveNinths(0.01);
                this->setFuzzyPrepareAndOrResolveThirteenths(0.01);
                break;
            default:
                break;
        }
        
        // set the minimum score only if we are not arranging a selection
        if (doc.metadata.harmonyScore && ! anythingSelected) {
            // reduce score by number of harmony events to be more flexible
            this->setMinimumAllowedScore(doc.metadata.harmonyScore.floatValue - doc.song.harmony.events.count*0.5f);
        }
        
        // set start and end points
        if (anythingSelected) {
            HarmonyWizMusicEvent *leftMostEvent = [doc.song findFirstSelectedEvent];
            HarmonyWizMusicEvent *rightMostEvent = [doc.song findLastSelectedEvent];
            this->startColumn = Maybe<size_t>([doc.song findFirstHarmonyColumnForEvent:leftMostEvent]);
            this->endColumn = Maybe<size_t>([doc.song findLastHarmonyColumnForEvent:rightMostEvent]+1);
        }
        
        // set notes
        size_t col;
        for (UInt16 i = 0; i < rows; i++) {
            col = 0;
            for (auto nt : tracks.at(i)) {
                if (get<0>(nt))
                    this->setNote(i, col, get<1>(nt));
                col++;
            }
        }
        
        // set beats
        col = 0;
        for (HarmonyWizHarmonyEvent *he in doc.song.harmony.events) {
            if (! he.noChord) {
                this->setBeat(col,[doc.song eventDuration:he]);
                col++;
            }
        }
        
        // use crotchet beats
        this->setBeatSize(doc.song.beatSubdivisions * (doc.song.timeSign.lowerNumeral / 4));
        
        // set non harmonic tones
        {
            size_t row = 0;
            NSEnumerator *en = doc.song.musicTracks.objectEnumerator;
            HarmonyWizMusicTrack *track;
            while ((track = en.nextObject) != nil) {
                size_t col = 0;
                
                NSEnumerator *en1 = doc.song.harmony.events.objectEnumerator;
                HarmonyWizHarmonyEvent *he;
                while ((he = en1.nextObject) != nil) {
                    if (! he.noChord) {
                        NSIndexSet *idxs = [doc.song overlappingEvents:track.events
                                                                    forEvent:he];
                        if (idxs.count > 0) {
                            
                            NSUInteger currentIndex = idxs.firstIndex;
                            while (currentIndex != NSNotFound) {
                                id ev = [track.events objectAtIndex:currentIndex];
                                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                                    HarmonyWizNoteEvent *ne = ev;
                                    if (ne.isNonHarmonicTone) {
                                        ScaleNote nht = [doc.song HWNoteToScaleNote:ne];
                                        this->addNonHarmonicTone(nht,col);
                                    }
                                }
                                
                                currentIndex = [idxs indexGreaterThanIndex:currentIndex];
                            }
                        }
                        
                        col++;
                    }
                }
                row++;
            }
        }
        
        if (this->keyIsMinor) {
            // raised notes
            this->setRaisedTonesDefaultTreatment();
            
            NSArray *chords = [doc.song.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord == NO"]];
            size_t col = 0;
            for (HarmonyWizHarmonyEvent* he in chords) {
                if ([[he.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty] isEqualToString:HarmonyWizLeadingNoteTreatmentMelodic]) {
                    this->raisedSixthTreatment.at(col) = Arranger<rows>::kLeadingNote_Raised;
                    this->raisedSeventhTreatment.at(col) = Arranger<rows>::kLeadingNote_Raised;
                }
                else if ([[he.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty] isEqualToString:HarmonyWizLeadingNoteTreatmentHarmonic]) {
                    this->raisedSixthTreatment.at(col) = Arranger<rows>::kLeadingNote_Natural;
                    this->raisedSeventhTreatment.at(col) = Arranger<rows>::kLeadingNote_Raised;
                }
                col++;
            }
            
            for (NSUInteger idx = 1; idx < chords.count; idx++) {
                HarmonyWizHarmonyEvent *he0 = [chords objectAtIndex:idx-1];
                HarmonyWizHarmonyEvent *he1 = [chords objectAtIndex:idx];
                if ([he0.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty] == nil && [he1.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty] != nil) {
                    this->raisedSixthTreatment.at(idx-1) = Arranger<rows>::kLeadingNote_Free;
                    this->raisedSeventhTreatment.at(idx-1) = Arranger<rows>::kLeadingNote_Free;
                }
                else if ([he0.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty] != nil && [he1.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty] == nil) {
                    this->raisedSixthTreatment.at(idx) = Arranger<rows>::kLeadingNote_Free;
                    this->raisedSeventhTreatment.at(idx) = Arranger<rows>::kLeadingNote_Free;
                }
            }
        }
        
        // setup solver parameters
        for (UInt16 i = 0; i < rows; i++) {
            HarmonyWizMusicTrack *track = [doc.song.musicTracks objectAtIndex:i];
            this->setRange(i, std::make_tuple(track.lowestNoteAllowed - kChromaticShift,
                                              track.highestNoteAllowed - kChromaticShift));
        }
        for (UInt16 i = 0; i < rows-2; i++) {
            this->setMaxPartDistance(i, 12);
        }
        this->setMaxPartDistance(rows-2, 19);
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode]) {
//            this->unsetMaxGuardCallsBeforeHalt();
            this->setMaxGuardCallsBeforeHalt(HWMaxGuardCallsRowMultiplier * 1.5 * this->getRows());
        }
        else {
            this->setMaxGuardCallsBeforeHalt(HWMaxGuardCallsRowMultiplier * this->getRows());
        }
        
        lastProgress->save();
        
        startTime = CACurrentMediaTime();
        
        viewController.isRendering = NO;
    }
    
    bool solve() {
        bool result = Solver<rows>::solve();
        
        // Do a final synchronization with the main thread before exiting for
        // the last time from solve.
        dispatch_sync(dispatch_get_main_queue(), ^{
            lastProgress->plugBackProgress([HWCollectionViewController currentlyOpenDocument].song);
            [viewController.allStavesControl updateAllStavesNow];
            [viewController.rulerControl updatePlaybackIndicatorPosition];
            [viewController.allStavesControl updatePositionIndicator];
            [viewController scrollToPlaybackPosition];
        });
        
        return result;
    }
    
protected:
    const __weak HWSongViewController * const viewController;
    CFTimeInterval startTime;
    
    class ArrangerState : public Solver<rows>::SavedGlobalState {
        const dispatch_queue_t queue;
        const bool anythingSelected;
        const __weak HarmonyWizSong * const preservedSong;
        const ScaleNote *maybeNote(const tuple<bool,ScaleNote>& note) const {
            if (get<0>(note))
                return &(get<1>(note));
            return nullptr;
        }
    public:
        ArrangerState(Arranger<rows> &aArranger,const bool inAnythingSelected,const HarmonyWizSong * const inPreservedSong) : Solver<rows>::SavedGlobalState(aArranger), queue(dispatch_queue_create("com.harmonywiz.ArrangerStateQueue", DISPATCH_QUEUE_SERIAL)), anythingSelected(inAnythingSelected), preservedSong(inPreservedSong) {}
        virtual ~ArrangerState() {}
        void save() {
            dispatch_sync(queue, ^{
                @autoreleasepool {
                    Solver<rows>::SavedGlobalState::save();
                }
            });
        }
        void restore() const {
            dispatch_sync(queue, ^{
                @autoreleasepool {
                    Solver<rows>::SavedGlobalState::restore();
                }
            });
        }
        void plugBackProgress(HarmonyWizSong *song) const {
            dispatch_sync(queue, ^{
                @autoreleasepool {
                    // must have set the last guard column.
                    // if not set it means there is no progress to plug back in anyhow.
                    if (! (this->lastGuardColumn).isJust())
                        return;
                    
                    const size_t pos = (this->lastGuardColumn).fromJust();
                    
                    NSArray *chordEvents = [song.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord == NO"]];
                    
                    // place the indicator
                    {
                        HarmonyWizHarmonyEvent *he = [chordEvents objectAtIndex:pos];
                        [song placePlaybackIndicatorAtEvent:he];
                    }
                    
                    // clear arrangement between start and end
                    {
                        if ((this->startColumn).isJust() && (this->endColumn).isJust()) {
                            [song clearArrangementFrom:[chordEvents objectAtIndex:(this->startColumn).fromJust()] to:[chordEvents objectAtIndex:(this->endColumn).fromJust()-1]];
                        }
                        else if ((this->startColumn).isJust()) {
                            [song clearArrangementFrom:[chordEvents objectAtIndex:(this->startColumn).fromJust()] to:chordEvents.lastObject];
                        }
                        else if ((this->endColumn).isJust()) {
                            [song clearArrangementFrom:[chordEvents objectAtIndex:0] to:[chordEvents objectAtIndex:(this->endColumn).fromJust()-1]];
                        }
                        else {
                            [song clearArrangement];
                        }
                    }
                    
                    // plug solution back into the song
                    {
                        size_t row = 0;
                        NSEnumerator *en = song.musicTracks.objectEnumerator;
                        HarmonyWizMusicTrack *track;
                        size_t startCol = ((this->startColumn).isJust() ? (this->startColumn).fromJust() : 0);
                        size_t endCol = ((this->endColumn).isJust() ? MIN((this->endColumn).fromJust(),pos+1) : pos+1);
                        while ((track = en.nextObject) != nil) {
                            size_t col = 0;
                            
                            NSMutableArray *newEvents = [NSMutableArray arrayWithCapacity:track.events.count];
                            NSEnumerator *en1 = track.events.objectEnumerator;
                            HarmonyWizMusicEvent *ev;
                            while ((ev = en1.nextObject) != nil) {
                                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && (((HarmonyWizNoteEvent*)ev).wasCreatedByUser || track.locked || col < startCol || col >= endCol)) {
                                    [newEvents placeObject:ev];
                                }
                            }
                            
                            NSEnumerator *en2 = song.harmony.events.objectEnumerator;
                            HarmonyWizHarmonyEvent *he;
                            while ((he = en2.nextObject) != nil) {
                                if (! he.noChord) {
                                    NSIndexSet *idxs = [song overlappingEvents:track.events
                                                                      forEvent:he];
                                    BOOL userEvent = NO;
                                    if (idxs.count > 0) {
                                        
                                        NSUInteger currentIndex = idxs.firstIndex;
                                        while (currentIndex != NSNotFound) {
                                            id ev = [track.events objectAtIndex:currentIndex];
                                            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                                                HarmonyWizNoteEvent *ne = ev;
                                                if (ne.wasCreatedByUser || track.locked || col < startCol || col >= endCol) {
                                                    userEvent = YES;
                                                    if (![newEvents containsObject:ne]) {
                                                        [newEvents placeObject:ne];
                                                    }
                                                }
                                            }
                                            
                                            currentIndex = [idxs indexGreaterThanIndex:currentIndex];
                                        }
                                    }
                                    if (!userEvent && col >= startCol && col < endCol) {
                                        const ScaleNote &sn = this->at(row,col);
                                        HarmonyWizNoteEvent *ne = [song ScaleNoteToHWNote:sn];
                                        ne.beat = he.beat;
                                        ne.subdivision = he.subdivision;
                                        ne.durationBeats = he.durationBeats;
                                        ne.durationSubdivisions = he.durationSubdivisions;
                                        ne.type = kNoteEventType_Auto;
                                        [newEvents placeObject:ne];
                                    }
                                    col++;
                                    if (col >= this->solver.getCols())
                                        break;
                                }
                            }
                            track.events = newEvents;
                            [song addFillerRestsTo:track.events];
                            row++;
                        }
                    }
                    
                    [song tieAdjacentAutoNotesPreservingSelection:preservedSong];
                    
                    // set harmony properties
                    {
                        NSUInteger col = 0;
                        NSEnumerator *en = song.harmony.events.objectEnumerator;
                        HarmonyWizHarmonyEvent *he;
                        while ((he = en.nextObject) != nil) {
                            if (! he.noChord) {
                                const Figures::ChordTypeWithInversion ch = this->chords.at(col);
                                Figures::ChordType type;
                                Figures::ChordInversion inversion;
                                tie(type,inversion) = Figures::unpackChord(ch);
                                
                                // clear the properties
                                he.properties = [NSMutableDictionary dictionary];
                                
                                // root
                                [he.properties setObject:[NSValue valueWithHWScaleNote:this->roots.at(col).octaveMod()]
                                                  forKey:HarmonyWizDegreeProperty];
                                
                                // third
                                if (this->thirds.at(col).isJust())
                                    [he.properties setObject:[NSValue valueWithHWScaleNote:this->thirds.at(col).fromJust().octaveMod()] forKey:HarmonyWizThirdProperty];
                                
                                // fifth
                                if (this->fifths.at(col).isJust())
                                    [he.properties setObject:[NSValue valueWithHWScaleNote:this->fifths.at(col).fromJust().octaveMod()] forKey:HarmonyWizFifthProperty];
                                
                                // seventh
                                if (this->sevenths.at(col).isJust())
                                    [he.properties setObject:[NSValue valueWithHWScaleNote:this->sevenths.at(col).fromJust().octaveMod()] forKey:HarmonyWizSevenProperty];
                                
                                // ninth
                                if (this->ninths.at(col).isJust())
                                    [he.properties setObject:[NSValue valueWithHWScaleNote:this->ninths.at(col).fromJust().octaveMod()] forKey:HarmonyWizNineProperty];
                                
                                // eleventh
                                if (this->elevenths.at(col).isJust())
                                    [he.properties setObject:[NSValue valueWithHWScaleNote:this->elevenths.at(col).fromJust().octaveMod()] forKey:HarmonyWizElevenProperty];
                                
                                // thirteenth
                                if (this->thirteenths.at(col).isJust())
                                    [he.properties setObject:[NSValue valueWithHWScaleNote:this->thirteenths.at(col).fromJust().octaveMod()] forKey:HarmonyWizThirteenProperty];
                                
                                // chord type
                                [he.properties setObject:[NSValue valueWithHWChordType:type]
                                                  forKey:HarmonyWizChordTypeProperty];
                                
                                // inversion
                                [he.properties setObject:[NSValue valueWithHWChordInversion:inversion]
                                                  forKey:HarmonyWizInversionProperty];
                                
                                col++;
                                if (col > pos)
                                    break;
                            }
                        }
                    }
                }
            });
        }
    } *lastProgress;
    
    virtual void madeProgress(bool columnProgressed) const {
        Solver<rows>::madeProgress(columnProgressed);
        
        if (this->endColumn.isJust() && this->getProgress() >= this->endColumn.fromJust()) {
            if (this->endColumn.fromJust() >= 1)
                viewController.arrangeGuardColumn = (UInt32)(this->endColumn.fromJust()-1);
            else
                viewController.arrangeGuardColumn = 0;
        }
        else {
            viewController.arrangeGuardColumn = (UInt32)(this->getProgress());
            viewController.arrangeGuardCoverage = this->getLastGuardCoverage();
        }
        
        lastProgress->save();
        
        if (! viewController.isRendering) {
            viewController.isRendering = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                size_t progress = this->getProgress() - this->getDovetailedStartColumn();
                float percentage = float(progress) / float(this->getDovetailedEndColumn() - this->getDovetailedStartColumn());
                viewController.arrangeHUD.progress = percentage;

                lastProgress->plugBackProgress([HWCollectionViewController currentlyOpenDocument].song);
                
                [viewController.allStavesControl updateAllStavesNow];
                [viewController.rulerControl updatePlaybackIndicatorPosition];
                [viewController.allStavesControl updatePositionIndicator];
                [viewController scrollToPlaybackPosition];
                
                viewController.isRendering = NO;
            });
        }
    }
    
    virtual void warningProgressHalted() const {
        [viewController timeOutArrangement];
    }
    
    virtual void solutionFound() {
        Solver<rows>::solutionFound();
        
//        CFTimeInterval endTime = CACurrentMediaTime();
//        CFTimeInterval dt = endTime - startTime;
//        NSLog(@"%lu guard check calls in %lf seconds. %lf calls per second", (unsigned long)this->countCheckGuardCalls, dt, this->countCheckGuardCalls/dt);
        
        // move progress to end of arrangement
        if (this->endColumn.isJust())
            this->lastGuardColumn = Maybe<size_t>(this->endColumn.fromJust()-1);
        else
            this->lastGuardColumn = Maybe<size_t>(this->getCols()-1);
        
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        if (doc.metadata.harmonyScore)
            doc.metadata.harmonyScore = [NSNumber numberWithFloat:MAX(this->score(), doc.metadata.harmonyScore.floatValue)];
        else
            doc.metadata.harmonyScore = [NSNumber numberWithFloat:this->score()];
        
        NSLog(@"Solution found with score: %f", doc.metadata.harmonyScore.floatValue);
        
        lastProgress->save();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            viewController.arrangeHUD.progress = 1;
        });
    }
    
    vector<tuple<bool,ScaleNote>> convertTrack(HarmonyWizMusicTrack* track) {
        NSIndexSet *idxSet;
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        bool found;
        vector<tuple<bool,ScaleNote>> outVec;
        
        outVec.clear();
        
        NSArray *unifiedEvents = [doc.song unifiedEvents:track.events];
        
        NSEnumerator *en = doc.song.harmony.events.objectEnumerator;
        HarmonyWizHarmonyEvent *he;
        while ((he = en.nextObject) != nil) {
            if (! he.noChord) {
                idxSet = [doc.song overlappingEvents:unifiedEvents forEvent:he];
                found = false;
                if (idxSet.count > 0) {
                    NSUInteger currentIndex = idxSet.firstIndex;
                    while (currentIndex != NSNotFound) {
                        id ev = [unifiedEvents objectAtIndex:currentIndex];
                        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            HarmonyWizNoteEvent *ne = ev;
                            BOOL noteIsConstraint = (ne.type == kNoteEventType_Harmony || (ne.type == kNoteEventType_Auto && track.locked));
                            if (noteIsConstraint) {
                                ScaleNote sn = [doc.song HWNoteToScaleNote:ne];
                                outVec.push_back(make_tuple(true,sn));
                                found = true;
                                break;
                            }
                        }
                        
                        currentIndex = [idxSet indexGreaterThanIndex:currentIndex];
                    }
                }
                if (!found) {
                    outVec.push_back(make_tuple(false,0));
                }
            }
        }
        
        return outVec;
    }
};

- (void)cancelArrangement {
    self.arrangeHUD.labelText = LinguiLocalizedString(@"Cancelling", @"HUD text");
    self.arrangeHUD.showCancelButton = NO;
    self.arrangeHUD.detailsLabelText = nil;
    self.arrangeHUD.mode = MBProgressHUDModeIndeterminate;
    
    self.didCancelArrange = YES;
    if (sCancelPointer != nullptr) {
        *sCancelPointer = true;
    }
}

- (void)timeOutArrangement {
    self.didArrangeTimeOut = YES;
    if (sCancelPointer != nullptr) {
        *sCancelPointer = true;
    }
}

- (void)applyRulesForStyle:(enum HarmonyStyle)style {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const uint16_t numbits = HarmonyWiz::kNumProperties;
    [doc.metadata.harmonyRules removeAllObjects];
    for (uint16_t i = 0; i < numbits; i++)
        [doc.metadata.harmonyRules addObject:@(YES)];
    switch (style) {
        case kStyle_Rudess3:
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kPrepareNinths withObject:@(NO)];
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kResolveNinths withObject:@(NO)];
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kResolveSevenths withObject:@(NO)];
        case kStyle_Rudess2:
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kPrepareSevenths withObject:@(NO)];
        case kStyle_Rudess1:
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kCommonTone withObject:@(NO)];
        case kStyle_Contemporary:
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kPrepareVIIDiminished withObject:@(NO)];
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kAllowThirteenths withObject:@(NO)];
        case kStyle_Baroque:
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kBassAlwaysMoves withObject:@(YES)];
            [doc.metadata.harmonyRules replaceObjectAtIndex:HarmonyWiz::kAllowElevenths withObject:@(NO)];
            
        default:
            break;
    }
}

- (BOOL)arrangeMusic {
    NSAssert([HWCollectionViewController currentlyOpenDocument].song.keySign != nil, @"Song does not have a key selected");
    NSAssert([HWCollectionViewController currentlyOpenDocument].song.songMode == nil, @"Song has a mode selected");
    
    BOOL result;
    __block BOOL fixed;
    string failureReason;
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
//    const BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    
    do {
        result = NO;
        fixed = NO;
        sCancelPointer = nullptr;
        self.didCancelArrange = NO;
        self.didArrangeTimeOut = NO;
        
        self.harmonyFailureReason = nil;
        [self applyRulesForStyle:doc.metadata.harmonyStyle];
        switch (doc.song.musicTracks.count) {
            case 3:
            {
                Arranger<3> arranger(self);
                arranger.setup();
                sCancelPointer = &arranger.getCancel();
                result = arranger.solve();
                failureReason = arranger.getLastGuardName();
                sCancelPointer = nullptr;
            }
                break;
            case 4:
            {
                Arranger<4> arranger(self);
                arranger.setup();
                sCancelPointer = &arranger.getCancel();
                result = arranger.solve();
                failureReason = arranger.getLastGuardName();
                sCancelPointer = nullptr;
            }
                break;
            case 5:
            {
                Arranger<5> arranger(self);
                arranger.setup();
                arranger.setThirdMaxCount(2);
                sCancelPointer = &arranger.getCancel();
                result = arranger.solve();
                failureReason = arranger.getLastGuardName();
                sCancelPointer = nullptr;
            }
                break;
            default:
                [NSException raise:@"HarmonyWiz" format:@"Harmony in %lu parts is not supported", (unsigned long)doc.song.musicTracks.count];
                break;
        }
        if (!result) {
            self.harmonyFailureReason = LinguiLocalizedString([NSString stringWithCString:failureReason.c_str() encoding:[NSString defaultCStringEncoding]],@"Harmonisation failure reason");
            
            // apply fixes and continue if necessary
            if (/*! expertMode &&*/ ! self.didCancelArrange && self.arrangeGuardCoverage > 0) {
                [self.fixesApplied addObject:@{ @"reason" : self.harmonyFailureReason, @"position" : @(self.doc.song.playbackPosition) }];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    fixed = [self arrangeFailed:@"Fix and restart auto"];
                });
            }
        }
        
    } while (/*! expertMode &&*/fixed && ! self.didCancelArrange && ! result);
    
    return result;
}

@end
