//
//  HWInstrumentPresetsMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 13/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWInstrumentPresetsMenu.h"
#import "NSString+Paths.h"
#import "UIAlertView+Blocks.h"
#import "NSString+Concatenation.h"
#import "HWCollectionViewController.h"
#import "NSString+Paths.h"

NSString * const HWInstrumentPresetNotification = @"HWInstrumentPresetNotification";
NSString * const HWInstrumentUserPresetNotification = @"HWInstrumentUserPresetNotification";
NSString * const HWSavedInstrumentUserPresetNotification = @"HWSavedInstrumentUserPresetNotification";

enum {
    kSection_SaveUserPreset = 0,
    kSection_FactoryPresets,
    kSection_UserPresets,
    kNumSections
};

@interface HWInstrumentPresetsMenu ()
@property (nonatomic,strong) NSArray *availableInstruments;
@property (nonatomic,strong) NSArray *factoryPresets;
@property (nonatomic,strong) UIAlertView *alert;
@property (nonatomic,strong) NSArray *userPresets;
@end

@implementation HWInstrumentPresetsMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.title = LinguiLocalizedString(@"Ensemble Presets", @"menu title");
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.userPresets = [NSArray arrayWithContentsOfFile:[NSString userPresetsPath]];
//    NSLog(@"self.userPresets %@", self.userPresets);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (self.userPresets.count == 0 ? kNumSections-1 : kNumSections);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == kSection_SaveUserPreset)
        return 1;
    else if (section == kSection_FactoryPresets)
        return [[self availableFactoryPresets] count];
    else if (section == kSection_UserPresets)
        return self.userPresets.count;
    
    return -1;
}

- (NSArray*)availableInstruments {
    if (_availableInstruments == nil) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSURL *url = [NSURL fileURLWithPath:[NSString samplerFilesPath]];
        NSError *error = nil;
        NSArray *installed = [fileManager contentsOfDirectoryAtURL:url includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
        if (error) {
            [NSException raise:@"HarmonyWizError" format:@"contentsOfDirectoryAtURL: %@", error.localizedDescription];
        }
        
        NSURL *mainBundleSamples = [[NSBundle mainBundle] URLForResource:@"Sampler Files" withExtension:nil];
        NSArray *shipped = [fileManager contentsOfDirectoryAtURL:mainBundleSamples includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
        if (error) {
            [NSException raise:@"HarmonyWizError" format:@"contentsOfDirectoryAtURL: %@", error.localizedDescription];
        }
        _availableInstruments = [[installed arrayByAddingObjectsFromArray:shipped] valueForKey:@"lastPathComponent"];
    }
    
    return _availableInstruments;
}

- (NSArray*)factoryPresets {
    if (_factoryPresets == nil) {
        _factoryPresets = @[
                     @{@"name" : @"Piano",
                       @"preset" : @[@"Jordan Piano",@"Jordan Piano",@"Jordan Piano",@"Jordan Piano",@"Jordan Piano",@"Jordan Piano"]},
                     @{@"name" : @"Vibes",
                       @"preset" : @[@"Vibes",@"Vibes",@"Vibes",@"Vibes",@"Vibes",@"Vibes"]},
                     @{@"name" : @"Orchestral",
                       @"preset" : @[@"Flute",@"Clarinet",@"Clarinet",@"French Horn",@"French Horn",@"Cello"]},
                     @{@"name" : @"Strings",
                       @"preset" : @[@"Violin",@"Violin",@"Violin",@"Cello",@"Cello",@"Cello"]},
                     @{@"name" : @"Acoustic Guitar",
                       @"preset" : @[@"Acoustic Guitar",@"Acoustic Guitar",@"Acoustic Guitar",@"Acoustic Guitar",@"Acoustic Guitar",@"Acoustic Guitar"]},
                     @{@"name" : @"Strings (Wizdom acoustic)",
                       @"preset" : @[@"Legato Violin",@"Legato Violin",@"Viola",@"Expressive Cello",@"Expressive Cello",@"Bass Bowed"]},
                     @{@"name" : @"Brass",
                       @"preset" : @[@"Trumpet",@"Trumpet",@"Trumpet",@"French Horn",@"French Horn",@"French Horn"]},
                     @{@"name" : @"Brass (Wizdom acoustic)",
                       @"preset" : @[@"Regal Trumpet",@"Regal Trumpet",@"Regal Trumpet",@"Trombone",@"Trombone",@"Trombone"]}
                     ];
    }
    return _factoryPresets;
}

- (NSArray*)availableFactoryPresets {
    return [self.factoryPresets filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSDictionary *obj, NSDictionary *bindings){
        NSArray *preset = [obj objectForKey:@"preset"];
        return [preset filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"! (SELF in %@)", self.availableInstruments]].count == 0;
    }]];
}

- (BOOL)userPresetIsAvailable:(NSString*)name {
//    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    NSDictionary *preset = [[self.userPresets filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.name == %@", name]] firstObject];
    
    if (preset) {
//        NSMutableArray *presetTrackTitles = preset.allKeys.mutableCopy;
//        [presetTrackTitles removeObject:@"name"];
//        NSArray *songTrackTitles = [doc.song.musicTracks valueForKey:@"title"];
        NSMutableArray *instruments = preset.allValues.mutableCopy;
        [instruments removeObject:[preset objectForKey:@"name"]];
        BOOL instrumentsAllAvailable = [instruments filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"! (SELF in %@)", self.availableInstruments]].count == 0;
//        return ([[NSSet setWithArray:presetTrackTitles] isEqualToSet:[NSSet setWithArray:songTrackTitles]] && instrumentsAllAvailable);
        return instrumentsAllAvailable;
    }
    
    return NO;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == kSection_SaveUserPreset) {
        return LinguiLocalizedString(@"Save the current set of instruments as a user defined preset.", @"table footer");
    }
    else if (section == kSection_FactoryPresets) {
        return nil;
    }
    else if (section == kSection_UserPresets) {
        return nil;
    }
    
    return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == kSection_SaveUserPreset) {
        return nil;
    }
    else if (section == kSection_FactoryPresets) {
        return LinguiLocalizedString(@"Built in presets", @"Menu header");
    }
    else if (section == kSection_UserPresets) {
        return LinguiLocalizedString(@"User defined presets", @"Menu header");
    }
    
    return nil;
}

- (void)setupCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath *)indexPath content:(BOOL)content {
    
    if (indexPath.section == kSection_SaveUserPreset) {
        if (content)
            cell.textLabel.text = LinguiLocalizedString(@"Save custom preset", @"Menu item");
    }
    else if (indexPath.section == kSection_FactoryPresets) {
        if (content)
            cell.textLabel.text = LinguiLocalizedString([[[self availableFactoryPresets] objectAtIndex:indexPath.row] objectForKey:@"name"], @"Menu item");
    }
    else if (indexPath.section == kSection_UserPresets) {
        if (content)
            cell.textLabel.text = [[[self userPresets] objectAtIndex:indexPath.row] objectForKey:@"name"];
        else {
            cell.userInteractionEnabled = [self userPresetIsAvailable:cell.textLabel.text];
            cell.textLabel.textColor = (cell.userInteractionEnabled ? [UIColor blackColor] : [UIColor lightGrayColor]);
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setupCell:cell forRowAtIndexPath:indexPath content:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [self setupCell:cell forRowAtIndexPath:indexPath content:YES];
    
    return cell;
}

- (void)saveCustomPresetWithName:(NSString*)name {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    for (HarmonyWizMusicTrack *track in doc.song.musicTracks) {
        if (track.instrument != nil) {
            [dic setObject:track.instrument forKey:track.title];
        }
    }
    [dic setObject:name forKey:@"name"];
    if (self.userPresets)
        self.userPresets = [self.userPresets arrayByAddingObject:dic];
    else
        self.userPresets = @[dic];
    [self.userPresets writeToFile:[NSString userPresetsPath] atomically:YES];
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:HWSavedInstrumentUserPresetNotification object:self userInfo:dic];
}

- (BOOL)userPresetNameIsValid:(NSString*)name {
    name = name.lowercaseString;
    for (NSString *str in [self.userPresets valueForKey:@"name"]) {
        if ([str.lowercaseString isEqualToString:name]) {
            return NO;
        }
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == kSection_SaveUserPreset) {
        __weak typeof(self) weakSelf = self;
        RIButtonItem *cancel = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.alert = nil;
        }];
        RIButtonItem *save = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Save", @"Save button") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            NSString *title = [[[strongSelf.alert textFieldAtIndex:0] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([strongSelf userPresetNameIsValid:title]) {
                [strongSelf saveCustomPresetWithName:title];
            }
            else {
                [[[UIAlertView alloc] initWithTitle:LinguiLocalizedStringForBlocks(@"Excuse me", @"Excuse me alert title") message:[NSString stringWithFormat:LinguiLocalizedStringForBlocks(@"Name %@ already used", @"Name used alert message"), title] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            strongSelf.alert = nil;
        }];
        self.alert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Save custom preset", @"Alert message") message:LinguiLocalizedString(@"Enter a name for the preset.", @"Alert message") cancelButtonItem:cancel otherButtonItems:save, nil];
        self.alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [self.alert textFieldAtIndex:0].autocapitalizationType = UITextAutocapitalizationTypeWords;
        [self.alert show];
    }
    else if (indexPath.section == kSection_FactoryPresets) {
        NSDictionary *preset = [[self availableFactoryPresets] objectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter] postNotificationName:HWInstrumentPresetNotification object:self userInfo:preset];
    }
    else if (indexPath.section == kSection_UserPresets) {
        NSDictionary *preset = [[self userPresets] objectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter] postNotificationName:HWInstrumentUserPresetNotification object:self userInfo:preset];
    }
}

 // Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == kSection_UserPresets);
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray *array = self.userPresets.mutableCopy;
        [array removeObject:[self.userPresets objectAtIndex:indexPath.row]];
        {
            NSString *name = [[self.userPresets objectAtIndex:indexPath.row] valueForKey:@"name"];
            HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
            if ([doc.metadata.instrumentsPresets isEqualToString:name]) {
                doc.metadata.instrumentsPresets = @"Custom";
            }
        }
        self.userPresets = array;
        [array writeToFile:[NSString userPresetsPath] atomically:YES];
        if (self.userPresets.count == 0) {
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:kSection_UserPresets] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else {
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}

@end
