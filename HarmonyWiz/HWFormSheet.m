//
//  HWFormSheet.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 31/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWFormSheet.h"
#import "HWAppDelegate.h"

@interface HWFormSheet ()
@property (nonatomic,strong) UITapGestureRecognizer *tapBehind;
@property (nonatomic,strong) NSDate *openDate;
@end

@implementation HWFormSheet

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!self.tapBehind) {
        self.tapBehind = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBehindDetected:)];
        self.tapBehind.delegate = self;
        
        [self.view.window addGestureRecognizer:self.tapBehind];
    }
    
    self.openDate = [NSDate date];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.view.window removeGestureRecognizer:self.tapBehind];
    
    const NSTimeInterval interval = (-[self.openDate timeIntervalSinceNow]);
    [HWAppDelegate recordEvent:@"close_form_sheet_after_duration" segmentation:@{ @"name" : self.name } sum:interval];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint location = [touch locationInView:self.view];
    BOOL result = (![self.view pointInside:location withEvent:nil]);
    if (result && SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // weird bug fix in iOS 8
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    return result;
}

- (void)tapBehindDetected:(UIGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded && SYSTEM_VERSION_LESS_THAN(@"8")) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

@end
