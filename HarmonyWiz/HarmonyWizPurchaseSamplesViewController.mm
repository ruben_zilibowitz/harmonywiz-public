//
//  HarmonyWizPurchaseSamplesViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HarmonyWizPurchaseSamplesViewController.h"
#import "HarmonyWizAppDelegate.h"
#import "NSString+Paths.h"
#import "NSString+Concatenation.h"
#import "AudioController.h"
#import "HWIAPContentDelegate.h"
#import <AEAudioFilePlayer.h>
#import "HWAppDelegate.h"
#include <vector>

int const HWTagStride = 100;

@interface SKProduct (Mb)
- (float)totalMegabytes;
@end

@implementation SKProduct (Mb)
- (float)totalMegabytes {
    long long byteCount = 0;
    for (NSNumber *num in self.downloadContentLengths) {
        byteCount += num.longLongValue;
    }
    const float Mb = (byteCount / 1024.f) / 1024.f;
    return Mb;
}
@end

@interface HarmonyWizPurchaseSamplesViewController ()
@property (nonatomic,strong) NSNumberFormatter *currencyFormatter;
@end

@implementation HarmonyWizPurchaseSamplesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = LinguiLocalizedString(@"Purchase Instruments", @"Purchase instruments menu title");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[CargoManager sharedManager] setUIDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[CargoManager sharedManager] setUIDelegate:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.f;
}

- (void)demo:(UIButton*)sender {
    const NSInteger section = sender.tag % HWTagStride;
    const NSInteger row = sender.tag / HWTagStride;
    NSString *productID = [[[HWIAPContentDelegate sharedManager] productIdentifiers] objectAtIndex:section];
    SKProduct *product = [[CargoManager sharedManager] productForIdentifier:productID];
    NSString *packTitle = product.localizedTitle;
    NSString *instrumentTitle = [[product.localizedDescription componentsSeparatedByString:@", "] objectAtIndex:row];
    NSURL *demoURL = [[NSBundle mainBundle] URLForResource:instrumentTitle withExtension:@"m4a" subdirectory:[@"JR HW Demos" stringByAppendingPathComponent:packTitle]];
    
    if (demoURL == nil) {
        [[[UIAlertView alloc] initWithTitle:@"Demo not found" message:[@"The instrument \"" : instrumentTitle : @"\" does not have a demo."] delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
    }
    else {
        NSError *error;
        AudioController *ac = [AudioController sharedAudioController];
        AEAudioController *aeac = ac.audioController;
        if (ac.filePlayer && ac.filePlayer.channelIsPlaying) {
            [self resetPlayingDemoButton];
            [aeac removeChannels:@[ac.filePlayer]];
        }
        ac.filePlayer = [AEAudioFilePlayer audioFilePlayerWithURL:demoURL error:&error];
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            recordError(error)
        }
        else {
            ac.filePlayer.loop = NO;
            ac.filePlayer.removeUponFinish = YES;
            ac.filePlayer.completionBlock = ^{
                [self resetPlayingDemoButton];
            };
            [aeac addChannels:@[ac.filePlayer]];
            ac.demoPlayingIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
            
            [sender setTitle:LinguiLocalizedString(@"Stop", @"Stop button") forState:UIControlStateNormal];
            [sender removeTarget:self action:@selector(demo:) forControlEvents:UIControlEventAllEvents];
            [sender addTarget:self action:@selector(stopDemo:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)resetPlayingDemoButton {
    AudioController *ac = [AudioController sharedAudioController];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:ac.demoPlayingIndexPath];
    UIButton *but = (id)cell.accessoryView;
    [but setTitle:@"Demo" forState:UIControlStateNormal];
    [but removeTarget:self action:@selector(stopDemo:) forControlEvents:UIControlEventAllEvents];
    [but addTarget:self action:@selector(demo:) forControlEvents:UIControlEventTouchUpInside];
    ac.demoPlayingIndexPath = nil;
}

- (void)stopDemo:(UIButton*)sender {
    AudioController *ac = [AudioController sharedAudioController];
    AEAudioController *aeac = ac.audioController;
    if (ac.filePlayer && ac.filePlayer.channelIsPlaying) {
        [self resetPlayingDemoButton];
        [aeac removeChannels:@[ac.filePlayer]];
    }
}

- (void)purchase:(UIButton*)sender {
    id headerView = sender;
    do {
        headerView = [headerView superview];
    } while (![headerView isKindOfClass:[UITableViewHeaderFooterView class]] && headerView != nil);
    
    if (headerView == nil) {
        [NSException raise:@"HarmonyWiz" format:@"failed to find header view"];
    }
    
    const NSUInteger section = [headerView tag];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSString *productID = [[[HWIAPContentDelegate sharedManager] productIdentifiers] objectAtIndex:section];
    SKProduct *prod = [[CargoManager sharedManager] productForIdentifier:productID];
    
    SKDownload *thisDownload = [[HWIAPContentDelegate sharedManager] downloadForProductID:productID];
    
    if (thisDownload) {
        // should not get here
        [[SKPaymentQueue defaultQueue] cancelDownloads:@[thisDownload]];
        [self.tableView reloadData];
    }
    else {
        [sender setEnabled:NO];
        [sender setTitle:@"Connecting" forState:UIControlStateDisabled];
        [[CargoManager sharedManager] buyProduct:prod];
    }
}

- (void)setupHeaderView:(UITableViewHeaderFooterView*)headerView section:(NSInteger)section {
    UILabel *title = (id)[headerView viewWithTag:'ttle'];
    UIButton *purchase = (id)[headerView viewWithTag:'pchs'];
    UILabel *info = (id)[headerView viewWithTag:'info'];
    UIProgressView *progress = (id)[headerView viewWithTag:'prgs'];
    
    if (!(title&&purchase&&info&&progress))
        return;
    
    title.adjustsFontSizeToFitWidth = YES;
    
    title.hidden = NO;
    purchase.hidden = YES;
    info.hidden = YES;
    progress.hidden = YES;
    
    NSString *productID = [[[HWIAPContentDelegate sharedManager] productIdentifiers] objectAtIndex:section];
    SKProduct *prod = [[CargoManager sharedManager] productForIdentifier:productID];
    
    purchase.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    if (prod) {
        title.text = prod.localizedTitle;
        
        SKDownload *thisDownload = [[HWIAPContentDelegate sharedManager] downloadForProductID:productID];
        
        if (thisDownload) {
            switch (thisDownload.downloadState) {
                case SKDownloadStateActive: {
                    progress.hidden = NO;
                    progress.progress = thisDownload.progress;
                    
                    static BOOL alertNotShown = YES;
                    if (alertNotShown) {
                        alertNotShown = NO;
                        [[[UIAlertView alloc] initWithTitle:@"Download Started" message:@"Your purchase download has started. Please do not close HarmonyWiz whilst the download is in progress." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    }
                }
                    break;
                case SKDownloadStateWaiting: {
                    [purchase setEnabled:NO];
                    purchase.hidden = NO;
                    [purchase setTitle:@"Waiting" forState:UIControlStateDisabled];
                }
                    break;
                case SKDownloadStateFinished: {
                    [purchase setEnabled:NO];
                    purchase.hidden = NO;
                    [purchase setTitle:@"Finished" forState:UIControlStateDisabled];
                }
                    break;
                case SKDownloadStatePaused: {
                    [purchase setEnabled:YES];
                    purchase.hidden = NO;
                    [purchase setTitle:@"Paused" forState:UIControlStateNormal];
                }
                    break;
                case SKDownloadStateCancelled: {
                    [purchase setEnabled:YES];
                    purchase.hidden = NO;
                    [purchase setTitle:@"Cancelled" forState:UIControlStateNormal];
                }
                    break;
                case SKDownloadStateFailed: {
                    [purchase setEnabled:YES];
                    purchase.hidden = NO;
                    [purchase setTitle:@"Failed" forState:UIControlStateNormal];
                }
                    break;
            }
        }
        else if ([[HWIAPContentDelegate sharedManager] transactionForProductID:prod.productIdentifier]) {
            [purchase setEnabled:NO];
            purchase.hidden = NO;
            [purchase setTitle:@"Connecting" forState:UIControlStateDisabled];
        }
        else if ([[HWIAPContentDelegate sharedManager] productPurchased:prod.productIdentifier]) {
            [purchase setEnabled:YES];
            purchase.hidden = NO;
            [purchase setTitle:@"Restore" forState:UIControlStateNormal];
        }
        else {
            [purchase setEnabled:YES];
            purchase.hidden = NO;
            [self.currencyFormatter setLocale:prod.priceLocale];
            NSString *currencyString = [self.currencyFormatter internationalCurrencySymbol]; // EUR, GBP, USD...
            NSString *format = [self.currencyFormatter positiveFormat];
            format = [format stringByReplacingOccurrencesOfString:@"¤" withString:currencyString];
            // ¤ is a placeholder for the currency symbol
            [self.currencyFormatter setPositiveFormat:format];
            NSString *price = [self.currencyFormatter stringFromNumber:prod.price];
            
            [purchase setTitle:price forState:UIControlStateNormal];
        }
    }
    else {
        title.text = @"Loading...";
    }
    
    headerView.tag = section;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    if (headerView == nil) {
        headerView = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"header"];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 215, 44)];
        title.tag = 'ttle';
        title.font = [UIFont boldSystemFontOfSize:16.f];
        title.opaque = NO;
        title.backgroundColor = [UIColor clearColor];
        [headerView.contentView addSubview:title];
        
        UIButton *purchase = [UIButton buttonWithType:(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") ? UIButtonTypeSystem : UIButtonTypeRoundedRect)];
        purchase.frame = CGRectMake(210, (44-28)*0.5f, 100, 28);
        purchase.tag = 'pchs';
        purchase.opaque = NO;
        purchase.backgroundColor = [UIColor clearColor];
        [purchase addTarget:self action:@selector(purchase:) forControlEvents:UIControlEventTouchUpInside];
        [headerView.contentView addSubview:purchase];
        
        UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        progress.frame = CGRectMake(200, (44-1)*0.5f, 110, 1);
        progress.tag = 'prgs';
        progress.opaque = NO;
        progress.backgroundColor = [UIColor clearColor];
        progress.hidden = YES;
        [headerView.contentView addSubview:progress];
        
        UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(230, (44-28)*0.5f, 90, 28)];
        info.tag = 'info';
        info.font = [UIFont boldSystemFontOfSize:14.f];
        info.opaque = NO;
        info.backgroundColor = [UIColor clearColor];
        info.text = @"Info";
        info.textColor = [UIColor blueColor];
        info.hidden = YES;
        [headerView.contentView addSubview:info];
    }
    
    [self setupHeaderView:headerView section:section];
    
    return headerView;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *productID = [[[HWIAPContentDelegate sharedManager] productIdentifiers] objectAtIndex:section];
    SKProduct *prod = [[CargoManager sharedManager] productForIdentifier:productID];
    
    if (prod) {
        return [NSString stringWithFormat:LinguiLocalizedString(@"Total size is %.2f Mb", @"File size description"), prod.totalMegabytes];
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[[HWIAPContentDelegate sharedManager] productIdentifiers] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *productID = [[[HWIAPContentDelegate sharedManager] productIdentifiers] objectAtIndex:section];
    SKProduct *prod = [[CargoManager sharedManager] productForIdentifier:productID];
    
    if (prod) {
        NSArray *items = [prod.localizedDescription componentsSeparatedByString:@", "];
        return items.count;
    }
    else {
        return 0;
    }
}

- (NSNumberFormatter*)currencyFormatter {
    if (_currencyFormatter == nil) {
        _currencyFormatter = [[NSNumberFormatter alloc] init];
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    }
    return _currencyFormatter;
}

- (void)setupCell:(UITableViewCell*)cell forIndexPath:(NSIndexPath*)indexPath {
    NSString *productID = [[[HWIAPContentDelegate sharedManager] productIdentifiers] objectAtIndex:indexPath.section];
    SKProduct *prod = [[CargoManager sharedManager] productForIdentifier:productID];
    NSArray *items = [prod.localizedDescription componentsSeparatedByString:@", "];
    cell.textLabel.text = [items objectAtIndex:indexPath.row];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.textLabel.font = [UIFont systemFontOfSize:16.f];
    
    UIButton *but = (id)cell.accessoryView;
    but.tag = indexPath.section + indexPath.row*HWTagStride;
    
    AudioController *ac = [AudioController sharedAudioController];
    if ((ac.filePlayer && ac.filePlayer.channelIsPlaying) && [indexPath isEqual:ac.demoPlayingIndexPath]) {
        [but setTitle:LinguiLocalizedString(@"Stop", @"Stop button") forState:UIControlStateNormal];
        [but removeTarget:self action:@selector(demo:) forControlEvents:UIControlEventAllEvents];
        [but addTarget:self action:@selector(stopDemo:) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [but setTitle:LinguiLocalizedString(@"Demo", @"Demo button") forState:UIControlStateNormal];
        [but removeTarget:self action:@selector(stopDemo:) forControlEvents:UIControlEventAllEvents];
        [but addTarget:self action:@selector(demo:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = [UIButton buttonWithType:(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7") ? UIButtonTypeSystem : UIButtonTypeRoundedRect)];
        [(UIButton*)cell.accessoryView setFrame:CGRectMake(0, 0, 50, 36)];
    }
    
    [self setupCell:cell forIndexPath:indexPath];
    cell.userInteractionEnabled = YES;
    
    return cell;
}

#pragma mark - cargo manager UI delegate

- (void)transactionDidFinishWithSuccess:(BOOL)success {
    [self.tableView reloadData];
}

- (void)restoredTransactionsDidFinishWithSuccess:(BOOL)success {
    [self.tableView reloadData];
}

- (void)downloadUpdated:(SKDownload *)download {
    const NSUInteger idx = [[[HWIAPContentDelegate sharedManager] productIdentifiers] indexOfObject:download.transaction.payment.productIdentifier];
    if (idx == NSNotFound) {
        NSLog(@"product not found %@", download.transaction.payment.productIdentifier);
    }
    else {
        UITableViewHeaderFooterView *headerView = [self.tableView headerViewForSection:idx];
        if (headerView) {
            [self setupHeaderView:headerView section:idx];
        }
    }
}

@end
