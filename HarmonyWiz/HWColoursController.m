//
//  HWColoursController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/09/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWColoursController.h"
#import "NSString+Concatenation.h"
#import "HWToolCustomCellBackground.h"
#import "HWCollectionViewController.h"

NSString * const HWNoteColourNotification = @"HWNoteColour";
extern NSString * const HWExpertMode;

@interface HWColoursController ()

@end

@implementation HWColoursController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noteColourChanged:) name:HWNoteColourNotification object:self];
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)noteColourChanged:(NSNotification*)notif {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    for (NSUInteger idx = 0; idx < 5; idx++) {
        BOOL highlighted = NO;
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        if (expertMode) {
            switch (doc.metadata.currentNoteColour) {
                case kNoteColour_Harmony:
                    highlighted = (idx == 0);
                    break;
                case kNoteColour_Passing:
                    highlighted = (idx == 1);
                    break;
                case kNoteColour_Suspension:
                    highlighted = (idx == 2);
                    break;
                case kNoteColour_Auto:
                    highlighted = (idx == 3);
                    break;
                case kNoteColour_Rest:
                    highlighted = (idx == 4);
                    break;
                default:
                    break;
            }
        }
        else {
            switch (doc.metadata.currentNoteColour) {
                case kNoteColour_Harmony:
                    highlighted = (idx == 0);
                    break;
                case kNoteColour_Passing:
                    highlighted = (idx == 1);
                    break;
                case kNoteColour_Suspension:
                    highlighted = (idx == 2);
                    break;
                case kNoteColour_Rest:
                    highlighted = (idx == 3);
                    break;
                default:
                    break;
            }
        }
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
        [[cell viewWithTag:3] setHidden:!highlighted];
        [(UILabel*)[cell viewWithTag:2] setTextColor:(highlighted ? [UIColor orangeColor] : [UIColor blackColor])];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (indexPath.section == 0) {
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        if (expertMode) {
            switch (indexPath.row) {
                case 0:
                    doc.metadata.currentNoteColour = kNoteColour_Harmony;
                    break;
                case 1:
                    doc.metadata.currentNoteColour = kNoteColour_Passing;
                    break;
                case 2:
                    doc.metadata.currentNoteColour = kNoteColour_Suspension;
                    break;
                case 3:
                    doc.metadata.currentNoteColour = kNoteColour_Auto;
                    break;
                case 4:
                    doc.metadata.currentNoteColour = kNoteColour_Rest;
                    break;
                    
                default:
                    break;
            }
        }
        else {
            switch (indexPath.row) {
                case 0:
                    doc.metadata.currentNoteColour = kNoteColour_Harmony;
                    break;
                case 1:
                    doc.metadata.currentNoteColour = kNoteColour_Passing;
                    break;
                case 2:
                    doc.metadata.currentNoteColour = kNoteColour_Suspension;
                    break;
                case 3:
                    doc.metadata.currentNoteColour = kNoteColour_Rest;
                    break;
                    
                default:
                    break;
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HWNoteColourNotification object:self];
    }
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


#pragma mark - UICollectionView Datasource

// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    
    if (expertMode) {
        switch (section) {
            case 0:
                return kNumNoteTypes;
                break;
                
            default:
                break;
        }
    }
    else {
        switch (section) {
            case 0:
                return kNumNoteTypes-1;
                break;
                
            default:
                break;
        }
    }
    return -1;
}

// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID = [@"cell" : @(indexPath.section).stringValue : @"," : @(indexPath.row).stringValue];
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    if (indexPath.section == 0 && cell.selectedBackgroundView == nil) {
        cell.selectedBackgroundView = [[HWToolCustomCellBackground alloc] initWithFrame:CGRectZero];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        BOOL highlighted = NO;
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        if (expertMode) {
            switch (doc.metadata.currentNoteColour) {
                case kNoteColour_Harmony:
                    highlighted = (indexPath.row == 0);
                    break;
                case kNoteColour_Passing:
                    highlighted = (indexPath.row == 1);
                    break;
                case kNoteColour_Suspension:
                    highlighted = (indexPath.row == 2);
                    break;
                case kNoteColour_Auto:
                    highlighted = (indexPath.row == 3);
                    break;
                case kNoteColour_Rest:
                    highlighted = (indexPath.row == 4);
                    break;
                default:
                    break;
            }
        }
        else {
            switch (doc.metadata.currentNoteColour) {
                case kNoteColour_Harmony:
                    highlighted = (indexPath.row == 0);
                    break;
                case kNoteColour_Passing:
                    highlighted = (indexPath.row == 1);
                    break;
                case kNoteColour_Suspension:
                    highlighted = (indexPath.row == 2);
                    break;
                case kNoteColour_Rest:
                    highlighted = (indexPath.row == 3);
                    break;
                default:
                    break;
            }
        }
        [[cell viewWithTag:3] setHidden:!highlighted];
        [(UILabel*)[cell viewWithTag:2] setTextColor:(highlighted ? [UIColor orangeColor] : [UIColor blackColor])];
    }
    
    return cell;
}

@end
