#import "LBorderView.h"

@implementation LBorderView

#pragma mark - Init

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.borderColor = [UIColor blackColor];
        self.shapeLayer.fillColor = [[UIColor clearColor] CGColor];
        self.shapeLayer.lineCap = kCALineCapRound;
        [self updatePath];
    }
    return self;
}

#pragma mark - Drawing

- (CAShapeLayer*)shapeLayer {
    return (CAShapeLayer*)self.layer;
}

- (void)updatePath {
//    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.cornerRadius];
//	self.shapeLayer.path = path.CGPath;
    
    // creating a path
	CGMutablePathRef path = CGPathCreateMutable();
    
    // drawing a border around a view
	CGPathMoveToPoint(path, NULL, 0, self.frame.size.height - self.cornerRadius);
	CGPathAddLineToPoint(path, NULL, 0, self.cornerRadius);
	CGPathAddArc(path, NULL, self.cornerRadius, self.cornerRadius, self.cornerRadius, M_PI, -M_PI_2, NO);
	CGPathAddLineToPoint(path, NULL, self.frame.size.width - self.cornerRadius, 0);
	CGPathAddArc(path, NULL, self.frame.size.width - self.cornerRadius, self.cornerRadius, self.cornerRadius, -M_PI_2, 0, NO);
	CGPathAddLineToPoint(path, NULL, self.frame.size.width, self.frame.size.height - self.cornerRadius);
	CGPathAddArc(path, NULL, self.frame.size.width - self.cornerRadius, self.frame.size.height - self.cornerRadius, self.cornerRadius, 0, M_PI_2, NO);
	CGPathAddLineToPoint(path, NULL, self.cornerRadius, self.frame.size.height);
	CGPathAddArc(path, NULL, self.cornerRadius, self.frame.size.height - self.cornerRadius, self.cornerRadius, M_PI_2, M_PI, NO);
    
    // path is set as the _shapeLayer object's path
	self.shapeLayer.path = path;
	CGPathRelease(path);
}

- (void)updateDashPattern {
	NSInteger dashPattern1 = _dashPattern;
	NSInteger dashPattern2 = _spacePattern;
	self.shapeLayer.lineDashPattern = _borderType == BorderTypeDashed ? @[@(dashPattern1),@(dashPattern2)] : nil;
}

#pragma mark - Setters


- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self updatePath];
}


- (void)setBorderType:(BorderType)borderType
{
    _borderType = borderType;
    
    [self updateDashPattern];
}


- (void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    
	self.layer.cornerRadius = cornerRadius;
    [self updatePath];
}


- (void)setBorderWidth:(CGFloat)borderWidth
{
    _borderWidth = borderWidth;
    
	self.shapeLayer.lineWidth = borderWidth;
}


- (void)setDashPattern:(NSUInteger)dashPattern
{
    _dashPattern = dashPattern;
    
    [self updateDashPattern];
}


- (void)setSpacePattern:(NSUInteger)spacePattern
{
    _spacePattern = spacePattern;
    
    [self updateDashPattern];
}


- (void)setBorderColor:(UIColor *)borderColor
{
    _borderColor = borderColor;
    
	self.shapeLayer.strokeColor = borderColor.CGColor;
}


#pragma mark -


@end
