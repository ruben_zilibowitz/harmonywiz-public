//
//  HWSongViewController+Arrange.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HWSongViewController.h"

@interface HWSongViewController (Arrange)

- (void)cancelArrangement;
- (BOOL)arrangeMusic;

@end
