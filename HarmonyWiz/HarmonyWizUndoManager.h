//
//  HarmonyWizUndoManager.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "KVUndoManager.h"
#import "HarmonyWizSong.h"

@interface HarmonyWizUndoManager : KVUndoManager

- (void)startUndoableChanges;
- (void)cancelAndRevertUndoableChanges;
- (void)cancelUndoableChanges;
- (void)finishUndoableChangesName:(NSString*)name;

@property (atomic,assign) BOOL shouldRedisplay;
@property (atomic,strong) NSString *lastActionName;

- (BOOL)canStartUndoableChanges;

@end

extern NSString * const HarmonyWizRevertUndoNotification;
