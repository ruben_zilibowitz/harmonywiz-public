//
//  divmod.h
//  Tonalis
//
//  Created by Ruben Zilibowitz on 8/11/11.
//  Copyright (c) 2011 N.A. All rights reserved.
//

#ifndef Tonalis_divmod_h
#define Tonalis_divmod_h

short imod(short x, short y);
short idiv(short x, short y);

#endif
