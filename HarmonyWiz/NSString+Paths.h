//
//  NSString+Paths.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 22/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Paths)

+ (NSString*)applicationSupportPath;
+ (NSString*)samplerFilesPath;
+ (NSString*)documentsDirectory;
+ (NSString*)cachesDirectory;
+ (NSString*)libraryDirectory;
+ (NSString*)saveFilesDirectory;
+ (NSString*)userPresetsPath;
+ (NSString*)undoPointsPath;

@end
