//
//  KVSlider.h
//  Accompanist
//
//  Created by Ruben Zilibowitz on 15/10/12.
//  Copyright (c) 2012 N.A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KVSlider : UISlider

@property (nonatomic) float kvValue;

@end
