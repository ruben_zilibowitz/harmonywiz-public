//
//  HWRestorePurchasesViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "CargoManager/CargoManager.h"

@interface HWRestorePurchasesViewController : UITableViewController <CargoManagerUIDelegate>

- (IBAction)restore:(UIButton*)sender;

@end
