//
//  HarmonyWizSong+ScaleNote.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 1/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong+ScaleNote.h"

using namespace HarmonyWiz;

@implementation HarmonyWizSong (ScaleNote)

- (HarmonyWiz::ScaleNote)HWNoteToScaleNote:(HarmonyWizNoteEvent*)ne {
    ScaleNote sn(ne.noteValue - kChromaticShift, self.keySign.root, self.keySign.tonality);
    if (sn.alteration == ScaleNote::kAlteration_Sharpen && ne.enharmonicShift == kSharpsToFlats) {
        sn.enharmonicShift();
    }
    else if (sn.alteration == ScaleNote::kAlteration_Flatten && ne.enharmonicShift == kFlatsToSharps) {
        sn.enharmonicShift();
    }
    return sn;
}

- (HarmonyWizNoteEvent*)ScaleNoteToHWNote:(ScaleNote)sn {
    HarmonyWizNoteEvent *ne = [[HarmonyWizNoteEvent alloc] init];
    ne.noteValue = sn.chromatic(self.keySign.root, self.keySign.tonality == kTonality_Minor) + kChromaticShift;
    if (sn.isSharpened()) {
        ne.enharmonicShift = kFlatsToSharps;
    }
    else if (sn.isFlattened()) {
        ne.enharmonicShift = kSharpsToFlats;
    }
    else {
        ne.enharmonicShift = kDefaultAccidental;
    }
    return ne;
}

@end
