//
//  HWTutorialPage.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWTutorialPage.h"
#import "NSString+Concatenation.h"

extern NSUInteger const HWTutorialNumberingPrefixLength;

@interface HWTutorialPage ()
@property (nonatomic,strong,readonly) NSString *name;
@end

@implementation HWTutorialPage

- (id)initWithName:(NSString*)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor blackColor];//UIColorFromRGB(0xDCDCDC);
    
    NSString *nameWithoutNumber = [self.name substringFromIndex:HWTutorialNumberingPrefixLength];
    NSString *tutorialPrefix = [nameWithoutNumber stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    NSString *imgFilename = [NSString stringWithFormat:@"images/Tutorial/%@/Tutorial_%@_%ld", self.name, tutorialPrefix, (long)(self.index+1)];
//    UIImage *img = [UIImage imageNamed:imgFilename];
    UIImage *img = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imgFilename ofType:@"png"]];
    self.imageView = [[UIImageView alloc] initWithImage:img];
    self.imageView.frame = CGRectMake(0, 40, img.size.width, img.size.height);
    [self.view addSubview:self.imageView];
    
    UILabel *heading = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 540, 40)];
    heading.textAlignment = NSTextAlignmentCenter;
    heading.text = LinguiLocalizedString(nameWithoutNumber, @"Tutorial heading");
    heading.textColor = [UIColor whiteColor];
    heading.font = [UIFont boldSystemFontOfSize:18.f];
    heading.backgroundColor = [UIColor clearColor];
    heading.opaque = NO;
    [self.view addSubview:heading];
    
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectInset(CGRectMake(0, img.size.height-135, img.size.width, 135), 12, 2)];
    text.textAlignment = NSTextAlignmentCenter;
    NSString *textName = [NSString stringWithFormat:@"Tutorial_%@_%ld", tutorialPrefix, (long)(self.index+1)];
    text.text = LinguiLocalizedString(textName, @"Tutorial text");
    text.textColor = [UIColor whiteColor];
    text.font = [UIFont systemFontOfSize:18.f];
    text.backgroundColor = [UIColor clearColor];
    text.opaque = NO;
    text.numberOfLines = -1;
    text.adjustsFontSizeToFitWidth = YES;
    [self.imageView addSubview:text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
