//
//  HWSongViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HWSongViewController.h"
#import "AudioController.h"
#import "HWExceptions.h"

#import "HWTimeRulerScrollView.h"
#import "HarmonyWizInstrumentsViewController.h"
//#import "HarmonyWizStyleViewController.h"
#import "HWStyleViewController.h"
#import "HWCollectionViewController.h"
#import "HarmonyWizPiano.h"
#import "HarmonyWizSong+Splice.h"
#import "MBProgressHUD/MBProgressHUD.h"
#import "HWAppDelegate.h"
#import "NSString+Concatenation.h"
#import "HWSongViewController+Arrange.h"
#import "HWSongViewController+ModalArrange.h"
#import "NSMutableArray+Sorted.h"
#import "HarmonyWizSong+MIDI.h"
#import "NSString+RomanNumerals.h"
#import "HWKeySignatureMenu.h"
#import "HWTimeSignatureMenu.h"
#import "HarmonyWizClefViewController.h"
#import "HWColourScheme.h"
#import "UIImage+Tint.h"
#import "UIApplication+Version.h"
#import "HarmonyWizSong+AutoColour.h"
#import "HWTransportButtons.h"
#import "HWFocusButtons.h"
#import "UIImage+animatedGIF.h"
#import "HWReverbMenu.h"
#import "HWTutorial.h"
#import "HWTutorialPage.h"
#import "HWEvent.h"
#import "HWChordAttributesViewController.h"
#import "HWChordAttributeSubmenuViewController.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"
#import "HWMagicPadScrollView.h"
#import "HarmonyWizSong+EnharmonicShift.h"
#import "HWOctaveTransposeViewController.h"
#import "HWZoomView.h"
#import "HWTutorialViewController.h"
#import "HWWandMenu.h"
#import "HWFixesViewController.h"
#import "HWShareSongMenu.h"
#import "AERecorder.h"
#import "AudioShareSDK.h"
#import "SCUI.h"
#import "NSString+Paths.h"
#import "HarmonyWizSong+Lilypond.h"
#import "HarmonyWizSong+MusicXML.h"
#import "HarmonyWizMIDISourcesViewController.h"
#import "HWTracksScrollView.h"
#import "HWFormSheet.h"
#import "HWSidePanel.h"
#import "HWModeMenu.h"
#import "HarmonyWizSong+Verify.h"
#import "NSFileManager+UniqueTemporaryDirectory.h"
#import <sys/utsname.h>
#import <Crashlytics/Crashlytics.h>

#include "divmod.h"

NSTimeInterval const HWContextErrorDelayTime = 0.2;
CGFloat const HarmonyWizStaveVerticalScale = 3.f;
CGFloat const kKeyboardWizdomBottomSpace = 40;
CGFloat const kKeyboardWizdomHeight = 368+kKeyboardWizdomBottomSpace;
NSUInteger const HWMaxFixColumns = 2;   // maximum number of columns that can be fixed at a time
NSTimeInterval const kEditTrackAnimationDuration = 0.5;
CGFloat const kFocusTrackFontPointSize = 92.0f;
NSTimeInterval const kTogglePianoDuration = 0.5;
NSTimeInterval const HWArrangeMessageUpdateTime = 5.;
UInt16 const HWPrecountBars = 2;
NSInteger const HWBarButtonButtonTag = 'bbtn';
NSInteger const HWBarButtonWandTag = 'wand';
NSInteger const HWAllStavesControlTag = 'alst';
NSInteger const HWInstrumentLabel = 'inst';
NSInteger const HWTransportButtonsTag = 'TSPT';
NSInteger const HWFocusButtonTag = 'FOCS';
NSInteger const HWScrollButtons = 'scrl';
NSInteger const HWWebViewLoadingHUDTag = 123;

CGFloat const HWTracksViewClosedWidth = 125;
CGFloat const HWTracksViewOpenedWidth = 250;
NSTimeInterval const HWRecordingRedrawDelay = 0.5f;
const CGFloat HWPaddingAfterStave = 60.f;

extern CGFloat const HWRulerHeight;
extern CGFloat const HWRulerHeightMagicMode;
extern CGFloat const HWToolbarHeight;
extern CGFloat const HWPanelWidth;
extern CGFloat const HWDividerWidth;

extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;
extern NSString * const HWAskedShowFirstLaunchTutorial;
extern NSString * const HWAutoFix;
extern NSString * const HWAutoScroll;

NSTimeInterval const HWSampleLoadDelay = 0.5;

CGFloat const HWMinNoteSpacing = 4;
CGFloat const HWMaxNoteSpacing = 16;

NSString * const HWAllNotesOffNotification = @"HWAllNotesOff";
NSString * const HWClearSongNotification = @"HWClearSong";
NSString * const HWClearArrangementNotification = @"HWClearArrangement";
NSString * const HWClearMagicPadNotification = @"HWClearMagicPad";
NSString * const HWNewTrackNotification = @"HWNewTrack";
NSString * const HWMasterReverbNotification = @"HWMasterReverb";
NSString * const HWQuantizationNotification = @"HWQuantization";
NSString * const HWSongTitleNotification = @"HWSongTitle";
NSString * const HWStateChangedNotification = @"HWStateChanged";
NSString * const HWEventsPasteboardType = @"com.wizdom-music.harmonywiz.events";

NSString * const HWArrangeCancelledMessage = @"How would you like to proceed?";
NSString * const HWArrangeFailedMessage = @"May I have your attention?";

//NSString * const HWSupportEmailAddress = @"HarmonyWiz@wizdommusic.com";
NSString * const HWSupportEmailAddress = @"support@harmonywiz.com";

NSString * const HWFAQURLString = @"http://harmonywiz.com/HWHelp/FAQ/index.html";

extern NSString * const HWStartedStaveEventNotification;
extern NSString * const HWEndedStaveEventNotification;
extern NSString * const HWCancelledStaveEventNotification;
extern NSString * const HWUpdateCanRedoNotification;
extern NSString * const HWUpdateCanUndoNotification;

extern NSString * const HWAutoTieNotification;
extern NSString * const HWHelpMenuPressed;
extern NSString * const HWInstrumentPresetNotification;
extern NSString * const HWInstrumentUserPresetNotification;
extern NSString * const HWSavedInstrumentUserPresetNotification;
extern NSString * const HWChangedOctaveTransposeNotification;
extern NSString * const HWEraseNotesFinishedNotification;
extern NSString * const HWPlaybackPositionChanged;
extern NSString * const HWMIDISourcesWillAppearNotification;
extern NSString * const HWShareSongNotification;
extern NSString * const HWArrangeThisChordNowNotification;
extern CGFloat const HWToolboxWidth;
extern CGFloat const HWStatusBarHeight;
extern CGFloat const HWScreenWidth;
extern CGFloat const HWScreenHeight;

extern void clearAifFilesFromTempDir();
extern void copyFilesToDocumentsDirectory(NSArray* results);

@interface HWSongViewController ()
@property (nonatomic,strong) UIPopoverController *lastPopoverShown;
@property (nonatomic,strong) NSMutableSet *visiblePopovers;
@property (nonatomic,strong) HarmonyWizInstrumentsViewController *instrumentsViewController;
@property (nonatomic,strong) HarmonyWizPiano *keyboardWizdom;
@property (nonatomic,strong) HarmonyWizClefViewController *clefViewController;
@property (nonatomic,strong) UINavigationController *chordAttributesViewController;
@property (nonatomic,strong) HWStyleViewController *styleViewController;
@property (nonatomic,strong) UIAlertView *currentAlert, *memoryAlert;
@property (nonatomic,strong) id preArrangeCheckResult;
@property (nonatomic,strong) UIPopoverController *instrumentsPopover, *stylesPopover, *clefPopover, *constraintsPopover, *wandPopover;
@property (nonatomic,strong) NSArray *arrangeMessages;
@property (nonatomic,strong) NSTimer *arrangeMessageTimer;
@property (nonatomic,strong) NSTimer *updatePlaybackTimer;

@property (nonatomic,assign) NSUInteger arrangeMessageIndex;
@property (nonatomic,assign) CGFloat fullScoreHeight, fullScorePointSize;
@property (atomic,assign) MusicPlayer musicPlayer;
@property (atomic,assign) BOOL recordNoteOn;
@property (atomic,assign) SInt32 recordCurrentNoteStartSubdiv;
@property (atomic,assign) float recordCurrentNoteStartPosition;
@property (atomic,assign) Byte recordCurrentNote;
@property (atomic,assign) BOOL arrangeSucceeded;
//@property (nonatomic,strong) NSDate *lastNoteRecorded;
@property (nonatomic,strong) NSMutableArray *liveRecording;
@property (nonatomic,assign) enum ToolType fullScoreTool;
@property (nonatomic,assign) float playbackPositionBeforeArrange;
@property (nonatomic,assign) BOOL stavesStoppedEnabled;

@property (nonatomic,strong) HWZoomView *zoomView;
@property (nonatomic,strong) NSString *tutorialOpenedName;

@property (atomic,assign) BOOL bounceCancelled;
@property (nonatomic,strong) NSDate *arrangeStartDate;
@property (nonatomic,strong) UIDocumentInteractionController *documentInteraction;

@property (atomic,strong) NSArray *leadingNotesMenuItems, *enharmonicShiftMenuItems, *noteTypeMenuItems;

#if AUDIOBUS_SUPPORT
@property (nonatomic,strong) ABTrigger *rewindTrigger, *playTrigger;
#endif

#if (! RELEASE_BUILD)
@property (nonatomic,assign) CFTimeInterval easterEggTime;
@property (nonatomic,strong) NSMutableArray *easterEggNotes;
#endif

@end

NSTimeInterval const HWBouncePaddingTime = 2;

@implementation HWSongViewController
{
    dispatch_queue_t _arrangeQueue;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.fixesApplied = [NSMutableArray array];
    self.visiblePopovers = [NSMutableSet set];
    self.doc.metadata.thumbnail = nil;
    self.arrangeMessages = [NSArray arrayWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"ArrangeMessages" withExtension:@"plist"]];
    
    if (_arrangeQueue == nil)
        _arrangeQueue = dispatch_queue_create("com.wizdom-music.harmonywiz.arrangeQueue", NULL);
    
    self.stavesStoppedEnabled = self.allStavesControl.userInteractionEnabled;
}

- (CGFloat)defaultStavesHeight {
    return (HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight+HWRulerHeight));
}

- (void)updateToolbarAndRuler {
    BOOL editingScore = self.allStavesControl.isEditing;
    BOOL enabledValue = (self.state == kStateStopped) && !editingScore;
    
    self.btnExit.enabled = enabledValue;
    self.btnArrange.enabled = enabledValue;
    self.btnMagic.enabled = enabledValue;
    self.btnSong.enabled = enabledValue;
    self.btnSettings.enabled = enabledValue;
    self.btnHelp.enabled = enabledValue;
    self.btnKeyboard.enabled = enabledValue || ! self.doc.metadata.magicMode;
    
    if (editingScore) {
        self.btnUndo.enabled = NO;
        self.btnRedo.enabled = NO;
    }
    else {
        self.btnUndo.enabled = self.doc.hwUndoManager.kvCanUndo;
        self.btnRedo.enabled = self.doc.hwUndoManager.kvCanRedo;
    }
    
    if (self.state == kStateRecording) {
        self.rulerControl.internalColor = [UIColor redColor];
    }
    else {
        self.rulerControl.internalColor = [UIColor lightTextColor];
    }
    
    HWTransportButtons *tsport = (id)[self.btnRewPlayRec.customView viewWithTag:HWTransportButtonsTag];
    tsport.leftbutton.enabled = !editingScore;
    tsport.midButton.enabled = !editingScore;
    tsport.rightButton.enabled = (! self.doc.metadata.magicMode) && !editingScore;
    
    HWFocusButtons *focus = (id)[self.btnFocus.customView viewWithTag:HWFocusButtonTag];
    focus.leftbutton.enabled = (self.doc.metadata.currentlySelectedTrackNumber < self.doc.song.musicTracks.count) && !editingScore;
    focus.rightButton.enabled = !editingScore;
}

- (void)setState:(enum HWState)state {
    _state = state;
    
    // change AudioBus trigger states
    switch (state) {
        case kStatePlaying:
            [[Crashlytics sharedInstance] setObjectValue:@"playing" forKey:@"state"];
#if AUDIOBUS_SUPPORT
            self.playTrigger.state = ABTriggerStateSelected;
            self.rewindTrigger.state = ABTriggerStateDisabled;
#endif
            break;
        case kStateRecording:
            [[Crashlytics sharedInstance] setObjectValue:@"recording" forKey:@"state"];
#if AUDIOBUS_SUPPORT
            self.playTrigger.state = ABTriggerStateSelected;
            self.rewindTrigger.state = ABTriggerStateDisabled;
#endif
            break;
        case kStateStopped:
            [[Crashlytics sharedInstance] setObjectValue:@"stopped" forKey:@"state"];
#if AUDIOBUS_SUPPORT
            self.playTrigger.state = ABTriggerStateNormal;
            self.rewindTrigger.state = ABTriggerStateNormal;
#endif
            break;
    }
    
    // don't allow staves interaction whilst playing or recording
    if (state == kStateStopped) {
        self.allStavesControl.userInteractionEnabled = self.stavesStoppedEnabled;
    }
    else {
        self.stavesStoppedEnabled = self.allStavesControl.userInteractionEnabled;
        self.allStavesControl.userInteractionEnabled = NO;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HWStateChangedNotification
                                                        object:self
                                                      userInfo:@{ @"state" : @(state) }];
    
    [self updateToolbarAndRuler];
}

- (void)setupNotifications {
	NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
    
    [dnc addObserver:self selector:@selector(enteringBackground:) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];
    [dnc addObserver:self selector:@selector(enteringForeground:) name:UIApplicationWillEnterForegroundNotification object:[UIApplication sharedApplication]];
    
    [dnc addObserver:self selector:@selector(startedStaveEvent:) name:HWStartedStaveEventNotification object:nil];
    [dnc addObserver:self selector:@selector(endedStaveEvent:) name:HWEndedStaveEventNotification object:nil];
    [dnc addObserver:self selector:@selector(endedStaveEvent:) name:HWCancelledStaveEventNotification object:nil];
    [dnc addObserver:self selector:@selector(updateCanUndo:) name:HWUpdateCanRedoNotification object:nil];
    [dnc addObserver:self selector:@selector(updateCanUndo:) name:HWUpdateCanUndoNotification object:nil];
    
    [dnc addObserver:self selector:@selector(draggedLocatorTriangle:) name:HWDraggedLocatorTriangle object:nil];
    [dnc addObserver:self selector:@selector(selectTrackNumber:) name:HWSelectTrackNotification object:self.allStavesControl];
    [dnc addObserver:self selector:@selector(stepRecordToggled:) name:HWStepRecordToggledNotification object:self.keyboardWizdom];
    [dnc addObserver:self selector:@selector(undoManagerDidUndo:) name:NSUndoManagerDidUndoChangeNotification object:self.doc.hwUndoManager];
    [dnc addObserver:self selector:@selector(undoManagerDidRedo:) name:NSUndoManagerDidRedoChangeNotification object:self.doc.hwUndoManager];
    [dnc addObserver:self selector:@selector(undoManagerDidRevert:) name:HarmonyWizRevertUndoNotification object:self.doc.hwUndoManager];
    
    [dnc addObserver:self selector:@selector(beginDraggingTrackControl:) name:HWBeginDraggingTrackControlNotification object:nil];
    [dnc addObserver:self selector:@selector(endDraggingTrackControl:) name:HWEndDraggingTrackControlNotification object:nil];
    
    [dnc addObserver:self selector:@selector(placedEvent:) name:HarmonyWizPlacedEventNotification object:nil];
    
    [dnc addObserver:self selector:@selector(autoTieCurrentTrack:) name:HWAutoTieNotification object:nil];
    [dnc addObserver:self selector:@selector(setInstrument:) name:HarmonyWizSetInstrumentNotification object:nil];
    [dnc addObserver:self selector:@selector(pressedStyle:) name:HarmonyWizPressedStyle object:nil];
    [dnc addObserver:self selector:@selector(changedStyle:) name:HWChangedStyleNotification object:nil];
    [dnc addObserver:self selector:@selector(changedInstrument:) name:HarmonyWizChangedInstrumentNotification object:self.instrumentsViewController.instrumentSelectViewController];
    [dnc addObserver:self selector:@selector(instrumentPresetSelected:) name:HWInstrumentPresetNotification object:nil];
    [dnc addObserver:self selector:@selector(instrumentUserPresetSelected:) name:HWInstrumentUserPresetNotification object:nil];
    [dnc addObserver:self selector:@selector(savedInstrumentUserPresetSelected:) name:HWSavedInstrumentUserPresetNotification object:nil];
    [dnc addObserver:self selector:@selector(octaveTransposeChanged:) name:HWChangedOctaveTransposeNotification object:self.instrumentsViewController.octaveViewController];
    [dnc addObserver:self selector:@selector(eraseNotesFinished:) name:HWEraseNotesFinishedNotification object:nil];
    [dnc addObserver:self selector:@selector(arrangeThisChordNow:) name:HWArrangeThisChordNowNotification object:nil];
    [dnc addObserver:self selector:@selector(shareSong:) name:HWShareSongNotification object:nil];
    [dnc addObserver:self selector:@selector(playbackPositionChanged:) name:HWPlaybackPositionChanged object:nil];
    [dnc addObserver:self selector:@selector(virtualNoteOn:) name:HWNotificationVirtualPianoNoteOn object:self.keyboardWizdom];
    [dnc addObserver:self selector:@selector(virtualNoteOff:) name:HWNotificationVirtualPianoNoteOff object:self.keyboardWizdom];
    [dnc addObserver:self selector:@selector(midiSourcesWillAppear:) name:HWMIDISourcesWillAppearNotification object:nil];
    [dnc addObserver:self selector:@selector(selectAllTrack:) name:HWSelectAllTrackNotification object:nil];
    [dnc addObserver:self selector:@selector(removeTrack:) name:HWRemoveTrackNotification object:nil];
    [dnc addObserver:self selector:@selector(freezeTrack:) name:HWFreezeTrackNotification object:nil];
    [dnc addObserver:self selector:@selector(clearTrack:) name:HWClearTrackNotification object:nil];
    
    [dnc addObserver:self selector:@selector(allNotesOff:) name:HWAllNotesOffNotification object:nil];
    [dnc addObserver:self selector:@selector(addBar:) name:HWDidAddBarNotification object:nil];
    [dnc addObserver:self selector:@selector(removeBar:) name:HWDidRemoveBarNotification object:nil];
    [dnc addObserver:self selector:@selector(clearSong:) name:HWClearSongNotification object:nil];
    [dnc addObserver:self selector:@selector(clearArrangement:) name:HWClearArrangementNotification object:nil];
    [dnc addObserver:self selector:@selector(clearMagicPad:) name:HWClearMagicPadNotification object:nil];
    
    [dnc addObserver:self selector:@selector(toolChange:) name:HWToolChangeNotification object:nil];
    [dnc addObserver:self selector:@selector(createTrack:) name:HWNewTrackNotification object:nil];
    [dnc addObserver:self selector:@selector(masterReverb:) name:HWMasterReverbNotification object:nil];
    [dnc addObserver:self selector:@selector(quantization:) name:HWQuantizationNotification object:nil];
    [dnc addObserver:self selector:@selector(songTitle:) name:HWSongTitleNotification object:nil];
    [dnc addObserver:self selector:@selector(helpMenuPressed:) name:HWHelpMenuPressed object:nil];
    [dnc addObserver:self selector:@selector(languageChanged:) name:kLinguiLanguageUpdatedNotification object:nil];
    
    [dnc addObserver:self selector:@selector(keyChanged:) name:HWKeySignatureChangedNotification object:nil];
    [dnc addObserver:self selector:@selector(modeChanged:) name:HWModeChangedNotification object:nil];
    [dnc addObserver:self selector:@selector(timeSignChanged:) name:HWTimeSignatureChangedNotification object:nil];
    [dnc addObserver:self selector:@selector(clefMenu:) name:HarmonyWizClefMenuNotification object:nil];
    [dnc addObserver:self selector:@selector(editConstraints:) name:HarmonyWizEditConstraintsNotification object:nil];
    [dnc addObserver:self selector:@selector(constraintModified:) name:HWChordAttributeModifiedNotification object:nil];
    
    [dnc addObserver:self selector:@selector(soloMuteStatusChanged:) name:HarmonyWizSoloMuteStatusChanged object:nil];
    [dnc addObserver:self selector:@selector(lockStatusChanged:) name:HarmonyWizLockStatusChanged object:nil];
    [dnc addObserver:self selector:@selector(willSelectTrack:) name:HarmonyWizWillSelectTrack object:nil];
    [dnc addObserver:self selector:@selector(didSelectTrack:) name:HarmonyWizDidSelectTrack object:nil];
    
    [dnc addObserver:self selector:@selector(volumeChanged:) name:HWTrackVolumeChanged object:nil];
    [dnc addObserver:self selector:@selector(panChanged:) name:HWTrackPanChanged object:nil];
    
    [dnc addObserver:self selector:@selector(performArrange:) name:HWArrangeNotification object:nil];
    [dnc addObserver:self selector:@selector(painted:) name:HWPaintedNotification object:nil];
    
    [dnc addObserver:self selector:@selector(touchedRuler:) name:HWTouchedRuler object:self.rulerControl];
    
    // receive KVO notifications of changes to harmonyScore
    [self.doc.metadata addObserver:self
                        forKeyPath:@"harmonyScore"
                           options:(NSKeyValueObservingOptionNew |
                                    NSKeyValueObservingOptionOld)
                           context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"harmonyScore"] && object == self.doc.metadata) {
        // nb: instead of changing text, change the icon...
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL unscored = ([change objectForKey:@"new"] == [NSNull null]);
            [self updateArrangeButton:unscored];
        });
    }
    else if ([keyPath isEqualToString:@"enabled"] && [object isKindOfClass:[UIBarButtonItem class]]) {
        UIBarButtonItem *barBtn = object;
        UIButton *btn = (id)[barBtn.customView viewWithTag:HWBarButtonButtonTag];
        btn.enabled = [[change objectForKey:@"new"] boolValue];
    }
}

- (void)startAnimateArrangeButton {
    UIImage *img = [UIImage animatedImageWithAnimatedGIFURL:[[NSBundle mainBundle] URLForResource:[@"HarmonyMagicWideAnimated" : ([[UIScreen mainScreen] scale] > 1.5 ? @"@2x" : @"")] withExtension:@"gif" subdirectory:@"images/TopIcon"]];
    self.btnArrange.customView = [self barButtonWithImage:img selector:@selector(cancelArrangement)];
    self.btnArrange.width = img.size.width;
}

- (void)stopAnimateArrangeButton {
    [self updateArrangeButton:NO];
}

- (void)updateArrangeButton:(BOOL)unscored {
    // nb: could use a different image when unscored is NO to indicate
    // a score exists
    UIImage *img = [UIImage imageNamed:@"images/TopIcon/HarmonyMagicWide"];
    UIImage *imgGreyed = [UIImage imageNamed:@"images/TopIcon/HarmonyMagicWide_Disabled"];
    self.btnArrange.customView = [self barButtonWithImage:img highlighted:imgGreyed disabled:imgGreyed selector:@selector(arrange:)];
    self.btnArrange.width = img.size.width;
}

- (void)setupViews {
    
    self.mainView.backgroundColor = [HWColourScheme mainBackgroundColour];
    
    // instruments view controller
    {
        self.instrumentsViewController = [[HarmonyWizInstrumentsViewController alloc]
                                          initWithStyle:UITableViewStyleGrouped];
        UINavigationController *navController = [[UINavigationController alloc]
                                                 initWithRootViewController:self.instrumentsViewController];
        navController.delegate = self;
        navController.navigationBar.translucent = NO;
        self.instrumentsPopover = [[UIPopoverController alloc]
                                   initWithContentViewController:navController];
        self.instrumentsPopover.delegate = self;
        self.instrumentsPopover.popoverContentSize = CGSizeMake(320, 500);
    }
    
    // style view controller
    {
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                      bundle:nil];
        self.styleViewController = [sb instantiateViewControllerWithIdentifier:@"harmony"];
//        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7"))
//            self.styleViewController.preferredContentSize = CGSizeMake(320, 700);
        UINavigationController *navController = [[UINavigationController alloc]
                                                 initWithRootViewController:self.styleViewController];
        navController.delegate = self;
        navController.navigationBar.translucent = NO;
        self.stylesPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
        self.stylesPopover.delegate = self;
        self.stylesPopover.popoverContentSize = CGSizeMake(320, 500);
    }
    
    // clefs view controller
    {
        self.clefViewController = [[HarmonyWizClefViewController alloc] initWithStyle:UITableViewStyleGrouped];
        self.clefPopover = [[UIPopoverController alloc]
                            initWithContentViewController:self.clefViewController];
        self.clefPopover.delegate = self;
        self.clefPopover.popoverContentSize = CGSizeMake(150, 400);
    }
    
    // constraints popover
    {
        self.chordAttributesViewController = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"chordAttributes"];
        self.constraintsPopover = [[UIPopoverController alloc]
                                   initWithContentViewController:self.chordAttributesViewController];
        self.constraintsPopover.delegate = self;
        self.constraintsPopover.popoverContentSize = CGSizeMake(320,600);
    }
    
    // wand popover
    {
        HWWandMenu *wandMenu = [[HWWandMenu alloc] initWithStyle:UITableViewStyleGrouped];
        UINavigationController *navController = [[UINavigationController alloc]
                                                 initWithRootViewController:wandMenu];
        navController.delegate = self;
        navController.navigationBar.translucent = NO;
        self.wandPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
        self.wandPopover.delegate = self;
        self.wandPopover.popoverContentSize = CGSizeMake(320,200);
    }
    
    // tracks content view
    HWTracksScrollView *tracksScrollView = [[HWTracksScrollView alloc] initWithFrame:CGRectMake(0, 0, HWTracksViewOpenedWidth, self.defaultStavesHeight)];
    tracksScrollView.contentView.frame = CGRectMake(0, 0, HWTracksViewOpenedWidth, self.defaultStavesHeight);
    tracksScrollView.contentSize = CGSizeMake(HWTracksViewOpenedWidth, self.defaultStavesHeight);
    tracksScrollView.delegate = self;
    tracksScrollView.showsHorizontalScrollIndicator = NO;
    tracksScrollView.showsVerticalScrollIndicator = NO;
    tracksScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.mainView addSubview:tracksScrollView];
    self.mainView.tracksView = tracksScrollView;
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openDrawer:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeDrawer:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.mainView.tracksView addGestureRecognizer:swipeLeft];
    [self.mainView.tracksView addGestureRecognizer:swipeRight];
    
    // staves content view
    HWStavesScrollView *stavesControl = [[HWStavesScrollView alloc] initWithFrame:CGRectMake(0, 0, HWScreenWidth-HWTracksViewClosedWidth, self.defaultStavesHeight)];
    //    self.allStavesControl.backgroundTextures = self.doc.metadata.scoreTexturesEnabled;
    stavesControl.translatesAutoresizingMaskIntoConstraints = NO;
    stavesControl.tag = HWAllStavesControlTag;
    stavesControl.multipleTouchEnabled = NO;
//    stavesControl.canCancelContentTouches = YES;
//    stavesControl.scrollEnabled = YES;
//    stavesControl.delaysContentTouches = YES;
    
    // staves scroll view
    self.mainView.stavesView = stavesControl;
    self.mainView.stavesView.contentSize = CGSizeMake(HWScreenWidth-HWTracksViewClosedWidth, self.defaultStavesHeight);
    [self.mainView.stavesView addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchStaves:)]];
    self.mainView.stavesView.delegate = self;
    self.mainView.stavesView.showsHorizontalScrollIndicator = NO;
    self.mainView.stavesView.showsVerticalScrollIndicator = NO;
    self.mainView.stavesView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.mainView addSubview:self.mainView.stavesView];
    
    // ruler control
    [self createRulerControl];
    self.allStavesControl.rulerControl = self.rulerControl;
    
    // tracks and staves
    [self createAllTrackAndStaveControls];
    [self.mainView.tracksView setNeedsLayout];
    [self.mainView.stavesView setNeedsLayout];
    
    // horizontal border view
    self.mainView.borderHorizontalView = [[UIView alloc] initWithFrame:CGRectMake(HWTracksViewClosedWidth, HWToolbarHeight+HWStatusBarHeight, HWScreenWidth-HWTracksViewClosedWidth, HWDividerWidth)];
    self.mainView.borderHorizontalView.backgroundColor = [HWColourScheme horizontalDividerColour];
    self.mainView.borderHorizontalView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.mainView addSubview:self.mainView.borderHorizontalView];
    [self.mainView bringSubviewToFront:self.mainView.borderHorizontalView];
    
    // vertical border view
    self.mainView.borderVerticalView = [[UIView alloc] initWithFrame:CGRectMake(HWTracksViewClosedWidth, HWToolbarHeight+HWStatusBarHeight, HWDividerWidth, HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight))];
    self.mainView.borderVerticalView.backgroundColor = [HWColourScheme verticalDividerColour];
    self.mainView.borderVerticalView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.mainView addSubview:self.mainView.borderVerticalView];
    [self.mainView bringSubviewToFront:self.mainView.borderVerticalView];
    
    [self.mainView bringSubviewToFront:self.mainView.tracksView];
    
    // piano control
    self.keyboardWizdom = [[HarmonyWizPiano alloc] initWithFrame:CGRectMake(0, HWScreenHeight, HWScreenWidth, kKeyboardWizdomHeight)];
    {
        HarmonyWizTrack *selectedTrack = self.doc.song.harmony;
        if (self.doc.metadata.currentlySelectedTrackNumber < self.doc.song.musicTracks.count)
            selectedTrack = [self.doc.song.musicTracks objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber];
        [self.keyboardWizdom showRangeForTrack:selectedTrack];
    }
    self.keyboardWizdom.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.keyboardWizdom];
    self.mainView.piano = self.keyboardWizdom;
    
    // copy piano to toolbox controller
    self.toolboxController.keyboardWizdom = self.keyboardWizdom;
    
    // magic pad
    {
        HWMagicPadScrollView *magicPad = [[HWMagicPadScrollView alloc] initWithFrame:CGRectMake(0, HWScreenHeight, HWScreenWidth, 20)];
        magicPad.rulerControl = self.rulerControl;
        magicPad.mainView = self.mainView;
        magicPad.layer.borderWidth = 2.f;
        magicPad.layer.borderColor = [UIColor lightGrayColor].CGColor;
        magicPad.multipleTouchEnabled = NO;
        magicPad.scrollEnabled = NO;
        magicPad.clipsToBounds = NO;
        magicPad.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.mainView.magicView = magicPad;
        [self.mainView addSubview:self.mainView.magicView];
    }
    
    {
        self.mainView.leftPanel = [[HWSidePanel alloc] initWithFrame:CGRectMake(-HWPanelWidth, HWToolbarHeight+HWStatusBarHeight, HWPanelWidth, HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight))];
        self.mainView.leftPanel.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.75f];
        self.mainView.leftPanel.layer.borderColor = [[UIColor grayColor] colorWithAlphaComponent:0.75f].CGColor;
        self.mainView.leftPanel.layer.borderWidth = 1.f;
        self.mainView.leftPanel.translatesAutoresizingMaskIntoConstraints = NO;
        [((HWSidePanel*)self.mainView.leftPanel) setMagicView:self.mainView.magicView];
        [self.mainView addSubview:self.mainView.leftPanel];
        
        self.mainView.rightPanel = [[HWSidePanel alloc] initWithFrame:CGRectMake(HWScreenWidth, HWToolbarHeight+HWStatusBarHeight, HWPanelWidth, HWScreenHeight - (HWToolbarHeight+HWStatusBarHeight))];
        self.mainView.rightPanel.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.75f];
        self.mainView.rightPanel.layer.borderColor = [[UIColor grayColor] colorWithAlphaComponent:0.75f].CGColor;
        self.mainView.rightPanel.layer.borderWidth = 1.f;
        self.mainView.rightPanel.translatesAutoresizingMaskIntoConstraints = NO;
        [((HWSidePanel*)self.mainView.rightPanel) setMagicView:self.mainView.magicView];
        [self.mainView addSubview:self.mainView.rightPanel];
        
        UILabel *instrumentLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, HWPanelWidth-2, 64.f)];
        instrumentLabel.backgroundColor = [UIColor clearColor];
        instrumentLabel.opaque = NO;
        instrumentLabel.adjustsFontSizeToFitWidth = YES;
        instrumentLabel.tag = HWInstrumentLabel;
        instrumentLabel.numberOfLines = 0;
        instrumentLabel.font = [UIFont fontWithName:@"Arial-BoldItalicMT" size:12.f];
        instrumentLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.mainView.leftPanel addSubview:instrumentLabel];
        
        UIButton *leftPanelScrollBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        leftPanelScrollBtn.tag = HWScrollButtons;
        leftPanelScrollBtn.backgroundColor = [UIColor clearColor];
        leftPanelScrollBtn.opaque = NO;
        [leftPanelScrollBtn setImage:[UIImage imageNamed:@"images/Controls/ArrowLeft"] forState:UIControlStateNormal];
//        [leftPanelScrollBtn setTitle:@"<" forState:UIControlStateNormal];
//        leftPanelScrollBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:36.f];
        leftPanelScrollBtn.frame = CGRectMake(0, self.mainView.leftPanel.frame.size.height-(HWRulerHeightMagicMode+4.f), HWPanelWidth, (HWRulerHeightMagicMode+4.f));
        [leftPanelScrollBtn addTarget:self action:@selector(scrollOnePageLeft:) forControlEvents:UIControlEventTouchUpInside];
        leftPanelScrollBtn.translatesAutoresizingMaskIntoConstraints = NO;
        [self.mainView.leftPanel addSubview:leftPanelScrollBtn];
        
        UIButton *rightPanelScrollBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightPanelScrollBtn.tag = HWScrollButtons;
        rightPanelScrollBtn.backgroundColor = [UIColor clearColor];
        rightPanelScrollBtn.opaque = NO;
        [rightPanelScrollBtn setImage:[UIImage imageNamed:@"images/Controls/ArrowRight"] forState:UIControlStateNormal];
//        [rightPanelScrollBtn setTitle:@">" forState:UIControlStateNormal];
//        rightPanelScrollBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:36.f];
        rightPanelScrollBtn.frame = CGRectMake(0, self.mainView.rightPanel.frame.size.height-(HWRulerHeightMagicMode+4.f), HWPanelWidth, (HWRulerHeightMagicMode+4.f));
        [rightPanelScrollBtn addTarget:self action:@selector(scrollOnePageRight:) forControlEvents:UIControlEventTouchUpInside];
        rightPanelScrollBtn.translatesAutoresizingMaskIntoConstraints = NO;
        [self.mainView.rightPanel addSubview:rightPanelScrollBtn];
    }
    
    if (SYSTEM_VERSION_LESS_THAN(@"7")) {
        UIEdgeInsets insets = UIEdgeInsetsMake(2, 0, -2, 0);
        self.btnUndo.imageInsets = insets;
        self.btnRedo.imageInsets = insets;
        self.btnExit.imageInsets = insets;
        self.btnFocus.imageInsets = insets;
//        self.btnRewPlayRec.imageInsets = insets;
//        self.btnArrange.imageInsets = insets;
        self.btnSong.imageInsets = insets;
        self.btnSettings.imageInsets = insets;
        self.btnMagic.imageInsets = insets;
        self.btnHelp.imageInsets = insets;
//        self.btnKeyboard.imageInsets = insets;
    }
    
    [self updateArrangeButton:self.doc.metadata.harmonyScore == 0];
    self.styleViewController.btnArrange = self.btnArrange;
    
    // setup transport button
    {
        const BOOL iOSBefore7 = SYSTEM_VERSION_LESS_THAN(@"7");
        const float shiftY = 18;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 167.5, iOSBefore7 ? 36 : 36+shiftY)];
        HWTransportButtons *transport = [[HWTransportButtons alloc] initWithFrame:CGRectMake(0, iOSBefore7 ? 0 : shiftY, 167.5, 36)];
        transport.tag = HWTransportButtonsTag;
        [view addSubview:transport];
        self.btnRewPlayRec.customView = view;
        self.btnRewPlayRec.width = 167.5;
        
        [transport.leftbutton addTarget:self action:@selector(stopRewind:) forControlEvents:UIControlEventTouchUpInside];
        [transport.midButton addTarget:self action:@selector(play:) forControlEvents:UIControlEventTouchUpInside];
        [transport.rightButton addTarget:self action:@selector(record:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // setup focus button
    {
        const BOOL iOSBefore7 = SYSTEM_VERSION_LESS_THAN(@"7");
        const float shiftY = 18;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 112, iOSBefore7 ? 36 : 36+shiftY)];
        HWFocusButtons *focus = [[HWFocusButtons alloc] initWithFrame:CGRectMake(0, iOSBefore7 ? 0 : shiftY, 112, 36)];
        focus.tag = HWFocusButtonTag;
        [view addSubview:focus];
        self.btnFocus.customView = view;
        self.btnFocus.width = 112;
        
        [focus.leftbutton addTarget:self action:@selector(changeFocusToSingleTrack:) forControlEvents:UIControlEventTouchUpInside];
        [focus.rightButton addTarget:self action:@selector(changeFocusToMultiTrack:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self setupKeyboardBarButton];
    
    if (self.doc.metadata.magicMode) {
        [(HWMagicPadScrollView*)self.mainView.magicView updateShapeViewsFromSong];
    }
    
    [self.btnArrange addObserver:self
                      forKeyPath:@"enabled"
                         options:(NSKeyValueObservingOptionNew |
                                  NSKeyValueObservingOptionOld)
                         context:NULL];
    
    // toolbox
    [self createToolbox];
    self.mainView.toolbox = self.toolboxController.toolboxView;
    
    // zoom view
    {
        self.zoomView = [[HWZoomView alloc] initWithFrame:CGRectZero];
        self.zoomView.backgroundColor = [UIColor clearColor];
        self.zoomView.opaque = NO;
        self.zoomView.hidden = YES;
        self.mainView.zoomView = self.zoomView;
        [self.mainView addSubview:self.zoomView];
    }
    
    // update toolbar
    [self updateToolbarAndRuler];
    
    if (self.doc.metadata.magicMode) {
        [(HWMagicPadScrollView*)self.mainView.magicView setStaveView:self.allStavesControl.musicStaveViews.lastObject];
        self.fullScoreTool = kToolType_Hand;
        self.doc.metadata.currentToolType = kToolType_Hand;
    }
    
    // touch exclusivity
    // important: crashes may occur without this
    self.mainView.toolbar.exclusiveTouch = YES;
    self.mainView.toolbox.exclusiveTouch = YES;
    self.mainView.magicView.exclusiveTouch = YES;
    self.trackControlContentView.exclusiveTouch = YES;
    self.allStavesControl.exclusiveTouch = YES;
    self.keyboardWizdom.exclusiveTouch = YES;
    self.rulerControl.exclusiveTouch = YES;
    
    self.mainView.tracksView.directionalLockEnabled = YES;
    self.rulerControl.directionalLockEnabled = YES;
    
    // apply any settings for current tool
    [self toolChange:nil];
    
    // ruler should be at back
    [self.mainView sendSubviewToBack:self.mainView.rulerView];
    
    // toolbar should be top most view
    [self.view bringSubviewToFront:self.toolbar];
    
    // make sure first track is selected in non-expert mode
    const BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    if (! expertMode && self.doc.metadata.currentlySelectedTrackNumber != 0) {
        HarmonyWizTrack *track = [self.trackControlContentView.tracks firstObject];
        [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizWillSelectTrack object:self userInfo:@{@"track" : track}];
        self.doc.metadata.currentlySelectedTrackNumber = 0;
        [self.trackControlContentView.tracks makeObjectsPerformSelector:@selector(updateSelectionVisuals)];
        //    [tcs makeObjectsPerformSelector:@selector(setNeedsDisplay)];
        [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizDidSelectTrack object:self userInfo:@{@"track" : track}];
    }
    
    [self.mainView disableConstraintsForAllSubviews];
    
    if (self.doc.metadata.isNewFile) {
        HarmonyWizMusicTrack *track = [self.doc.song.musicTracks objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber];
        [self.keyboardWizdom scrollKeyboardToRangeForTrack:track];
    }
    else {
        [self.keyboardWizdom updateScroll];
    }
}

- (void)setupKeyboardBarButton {
    UIBarButtonItem *button = nil;
    if (self.doc.metadata.magicMode) {
        button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"images/TopIcon/AutoTie"] style:UIBarButtonItemStylePlain target:self action:@selector(magicWandMenu:)];
        button.tintColor = UIColorFromRGB(0x000000);
        if (SYSTEM_VERSION_LESS_THAN(@"7")) {
            button.imageInsets = UIEdgeInsetsMake(2, 0, -2, 0);
        }
        else {
            button.imageInsets = UIEdgeInsetsMake(10, 0, -10, 0);
        }
    }
    else {
        UIView *view = [self barButtonWithImage:[UIImage imageNamed:@"images/TopIcon/Keyboard"] highlighted:[UIImage imageNamed:@"images/TopIcon/Keyboard_Disabled"] disabled:[UIImage imageNamed:@"images/TopIcon/Keyboard_Disabled"] selector:@selector(togglePiano:)];
        button = [[UIBarButtonItem alloc] initWithCustomView:view];
    }
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.toolbar.items];
    NSUInteger keyboardIdx = [array indexOfObject:self.btnKeyboard];
    [array replaceObjectAtIndex:keyboardIdx withObject:button];
    
    self.toolbar.items = array;
    
    self.btnKeyboard = button;
}

- (UIView*)barButtonWithImage:(UIImage*)img selector:(SEL)selector {
    UIImage *imgGreyed = [img tintedImageUsingColor:[UIColor lightGrayColor]];
    return [self barButtonWithImage:img highlighted:imgGreyed disabled:imgGreyed selector:selector];
}

- (UIView*)barButtonWithImage:(UIImage*)img highlighted:(UIImage*)img2 disabled:(UIImage*)img3 selector:(SEL)selector {
    const BOOL iOSBefore7 = SYSTEM_VERSION_LESS_THAN(@"7");
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = HWBarButtonButtonTag;
    [btn setImage:img forState:UIControlStateNormal];
    [btn setImage:img2 forState:UIControlStateHighlighted];
    [btn setImage:img3 forState:UIControlStateDisabled];
    btn.frame = CGRectMake(0, iOSBefore7 ? 0 : 20, img.size.width, 36);
    if (selector != nil)
        [btn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, img.size.width, iOSBefore7 ? 36 : 56)];
    [view addSubview:btn];
    return view;
}

- (void)handleFirstLaunch {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:HWAskedShowFirstLaunchTutorial]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:HWAskedShowFirstLaunchTutorial];
        [[NSUserDefaults standardUserDefaults] synchronize];
        __weak typeof(self) weakSelf = self;
        RIButtonItem *Yes = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Yes please!", @"Button text") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.tutorialOpenedName = @"01 Overview";
            [strongSelf performSegueWithIdentifier:@"showHelp" sender:self];
        }];
        
        self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"First Launch", @"Alert title") message:LinguiLocalizedString(@"This is your first launch of HarmonyWiz. Would you like to see the tutorial?", @"Alert message") cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"No thanks", @"Button text")] otherButtonItems:Yes, nil];
        
        [self.currentAlert show];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupViews];
    
    // setup audio
    MBProgressHUD *progressHud = [[MBProgressHUD alloc] initWithView:self.view];
    progressHud.mode = MBProgressHUDModeIndeterminate;
    progressHud.dimBackground = YES;
    progressHud.removeFromSuperViewOnHide = YES;
    progressHud.labelText = @"Initializing audio";
    [self.view addSubview:progressHud];
    __weak typeof(self) weakSelf = self;
    [progressHud showAnimated:YES whileExecutingBlock:^{
        @autoreleasepool {
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf setupAudio];
        }
    }
              completionBlock:^{
                  __strong typeof(self) strongSelf = weakSelf;
                  [strongSelf.musicTrackControls makeObjectsPerformSelector:@selector(updateInstrument)];
                  [strongSelf setupMIDI];
                  [strongSelf setupNotifications];
                  [strongSelf handleFirstLaunch];
                  if (strongSelf.doc.metadata.magicMode) {
                      [strongSelf updateLeftPanelInstrumentLabel];
                  }
                  
                  [strongSelf setupAudioBusTriggers];
              }];
    
    if (self.doc.metadata.isNewFile) {
        [self autoSizeVertical];
    }
    
    [self.rulerControl updatePlaybackIndicatorPosition];
    [self.allStavesControl updatePositionIndicator];
}

- (void)setupAudioBusTriggers {
#if AUDIOBUS_SUPPORT
    self.rewindTrigger = [ABTrigger triggerWithSystemType:ABTriggerTypeRewind block:^(ABTrigger *trigger, NSSet *ports){
        [self rewind];
    }];
    self.playTrigger = [ABTrigger triggerWithSystemType:ABTriggerTypePlayToggle block:^(ABTrigger *trigger, NSSet *ports){
        [self play:nil];
    }];
    [[[AudioController sharedAudioController] audiobusController] addTrigger:self.rewindTrigger];
    [[[AudioController sharedAudioController] audiobusController] addTrigger:self.playTrigger];
#endif
}

- (void)setupMIDI {
    PGMidi *midi = [PGMidi sharedInstance];
    midi.networkEnabled = YES;
    midi.delegate = self;
    self.doc.lastMIDIDestinationAdded = nil;
    self.doc.midiDestination = nil;
    for (PGMidiSource *source in midi.sources) {
        [source addDelegate:self];
    }
}

- (void)cleanupMIDI {
    PGMidi *midi = [PGMidi sharedInstance];
    midi.networkEnabled = NO;
    midi.delegate = nil;
    for (PGMidiSource *source in midi.sources) {
        [source removeDelegate:self];
    }
    self.doc.lastMIDIDestinationAdded = nil;
    self.doc.midiDestination = nil;
}

- (void)setupAudio {
    [self.instrumentsViewController updateInstrumentNames];
    [self applyDefaultInstrumentsToAllTracks];
    [self.doc applyAllAudioSettings];
    [self applyAllMIDISettings];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    AudioController *ac = [AudioController sharedAudioController];
    
    // close all popovers
    [self closeAllPopovers];
    
    // cleanup audio
    [ac stopProcessingGraph];
    
    // cleanup midi
    [self cleanupMIDI];
    
    // remove the observer previous setup
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // invalidate all timers
    [self.arrangeMessageTimer invalidate];
    self.arrangeMessageTimer = nil;
    [self.updatePlaybackTimer invalidate];
    self.updatePlaybackTimer = nil;
    
    // remove audiobus triggers
#if AUDIOBUS_SUPPORT
    [[[AudioController sharedAudioController] audiobusController] removeTrigger:self.rewindTrigger];
    [[[AudioController sharedAudioController] audiobusController] removeTrigger:self.playTrigger];
    self.rewindTrigger = nil;
    self.playTrigger = nil;
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    if (self.state != kStateStopped) {
        [self stopRewind:nil];
    }
    
    [self killScroll];
    
    [[AudioController sharedAudioController] stopProcessingGraph];
    [[AudioController sharedAudioController] releaseAllPresets];
    [(HWMagicPadScrollView*)self.mainView.magicView removeShapeViews];
    
    [self.rulerControl purgeQueues];
    [self.allStavesControl purgeQueues];
    
    /*
    NSTimeInterval const HWMemoryHUDDelay = 5;
    
    MBProgressHUD *memoryHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    memoryHUD.mode = MBProgressHUDModeDeterminate;
    memoryHUD.labelText = @"Low memory";
    memoryHUD.detailsLabelText = @"Please wait";
    memoryHUD.removeFromSuperViewOnHide = YES;
    [self.view.window addSubview:memoryHUD];
    const CFTimeInterval startTime = CACurrentMediaTime();
    __weak typeof(self) weakSelf = self;
    __weak typeof(memoryHUD) weakMemoryHUD = memoryHUD;
    [memoryHUD showAnimated:YES whileExecutingBlock:^{
        @autoreleasepool {
            __strong typeof(self) strongSelf = weakSelf;
            CFTimeInterval dt;
            do {
                dt = (CACurrentMediaTime() - startTime) / HWMemoryHUDDelay;
                weakMemoryHUD.progress = dt;
                [NSThread sleepForTimeInterval:0.05];
            } while (dt < 1);
            weakMemoryHUD.mode = MBProgressHUDModeIndeterminate;
            weakMemoryHUD.labelText = @"Loading instruments";
            [strongSelf.doc applyAllAudioSettings];
        }
    } completionBlock:^{
        __strong typeof(self) strongSelf = weakSelf;
        if (strongSelf.doc.metadata.magicMode) {
            [(HWMagicPadScrollView*)self.mainView.magicView updateShapeViewsFromSong];
        }
        [[AudioController sharedAudioController] startProcessingGraph];
    }];
     */
    
    // only show alert if it is not currently visible
    if (self.memoryAlert == nil) {
        __weak typeof(self) weakSelf = self;
        RIButtonItem *continueButton = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Continue", @"Button title") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.memoryAlert = nil;
            [strongSelf applyAudioSettingsBackground];
            if (strongSelf.doc.metadata.magicMode) {
                [(HWMagicPadScrollView*)strongSelf.mainView.magicView updateShapeViewsFromSong];
            }
            [[AudioController sharedAudioController] startProcessingGraph];
        }];
        RIButtonItem *exitSongButton = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Exit song", @"Button title") action:^{
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                __strong typeof(self) strongSelf = weakSelf;
                strongSelf.memoryAlert = nil;
                [strongSelf saveAndExit];
            }];
        }];
        
        self.memoryAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Memory low", @"Alert title") message:LinguiLocalizedString(@"Try closing background apps to free more space before pressing continue.", @"Alert message") cancelButtonItem:exitSongButton otherButtonItems:continueButton, nil];
        [self.memoryAlert show];
    }
}

- (void)cancelBounce:(id)sender {
    self.bounceCancelled = YES;
}

- (void)bounceToPath:(NSString*)outputFilePath completion:(void (^)(BOOL success))completionBlock {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    hud.removeFromSuperViewOnHide = YES;
    hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    hud.labelText = LinguiLocalizedString(@"Bouncing song", @"HUD title button");
    hud.cancelButtonTitle = LinguiLocalizedString(@"Stop", @"Stop button");
    hud.showCancelButton = YES;
    [hud addCancelButtonTarget:self selector:@selector(cancelBounce:)];
    [self.view.window addSubview:hud];
    
    if (self.state != kStateStopped) {
        [self stopRewind:nil];
    }
    [self stopRewind:nil];
    
    self.bounceCancelled = NO;
    
    __block BOOL success = NO;
    
    
    AudioController *ac = [AudioController sharedAudioController];
    AEAudioController *aeac = ac.audioController;
    
    // create file
    AERecorder *recorder = [[AERecorder alloc] initWithAudioController:aeac];
    NSError *error = nil;
    if ( ![recorder beginRecordingToFileAtPath:outputFilePath fileType:kAudioFileAIFFType error:&error] ) {
        recordError(error)
    }
    else {
        [aeac addOutputReceiver:recorder];
        
        __weak typeof(self) weakSelf = self;
        __weak typeof(hud) weakHUD = hud;
        [hud showAnimated:YES whileExecutingBlock:^{
            __strong typeof(self) strongSelf = weakSelf;
            
            // slight pause at start
            [NSThread sleepForTimeInterval:0.5];
            
            // calculate number of crotchets per bar
            const float crotchetBeatsPerBeat = (4.f / strongSelf.doc.song.timeSign.lowerNumeral);
            const float crotchetBeatsPerBar = crotchetBeatsPerBeat * strongSelf.doc.song.timeSign.upperNumeral;
            
            // calculate padding time to allow reverb to fade out
            NSTimeInterval paddingTime = [strongSelf.doc.song countEmptyBeatsAtEnd] * crotchetBeatsPerBeat * (60.f/strongSelf.doc.song.tempo);
            if (paddingTime < HWBouncePaddingTime) {
                paddingTime = HWBouncePaddingTime;
            }
            
            // endTime
            const MusicTimeStamp sequenceLength = (strongSelf.doc.song.finalBarNumber * crotchetBeatsPerBar) + paddingTime*(strongSelf.doc.song.tempo/60.f);
            
            [strongSelf startPlayback:NO precount:0];
            
            // start player and record
            {
                MusicTimeStamp currentTime;
                const Float64 progressPeriod = 0.25;
                do {
                    [NSThread sleepForTimeInterval:progressPeriod];
                    
                    HWThrowIfAudioError(MusicPlayerGetTime (strongSelf.musicPlayer, &currentTime));
                    
                    if (weakHUD) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            weakHUD.progress = (currentTime / sequenceLength);
                        });
                    }
                } while (currentTime < sequenceLength && !strongSelf.bounceCancelled);
            }
            
            [strongSelf stopPlayback];
        } completionBlock:^{
            [aeac removeOutputReceiver:recorder];
            [recorder finishRecording];
            success = YES;
            
            if (completionBlock != nil) {
                completionBlock(success);
            }
        }];
    }
}

- (void)postToSoundCloud:(NSURL*)trackURL {
    SCShareViewController *shareViewController;
    shareViewController = [SCShareViewController shareViewControllerWithFileURL:trackURL
                                                              completionHandler:^(NSDictionary *trackInfo, NSError *error) {
                                                                  if (SC_CANCELED(error)) {
                                                                      NSLog(@"Canceled!");
                                                                  } else if (error) {
                                                                      NSLog(@"Ooops, something went wrong: %@", [error localizedDescription]);
                                                                  } else {
                                                                      // If you want to do something with the uploaded
                                                                      // track this is the right place for that.
                                                                      NSLog(@"Uploaded track: %@", trackInfo);
                                                                  }
                                                                  clearAifFilesFromTempDir();
                                                              }];
    
    // If your app is a registered foursquare app, you can set the client id and secret.
    // The user will then see a place picker where a location can be selected.
    // If you don't set them, the user sees a plain plain text filed for the place.
    //    [shareViewController setFoursquareClientID:@"<foursquare client id>"
    //                                  clientSecret:@"<foursquare client secret>"];
    
    // We can preset the title ...
    [shareViewController setTitle:LinguiLocalizedString(@"My HarmonyWiz Song", @"Share by SoundCloud title")];
    
    // ... and other options like the private flag.
    [shareViewController setPrivate:NO];
    
    // Now present the share view controller.
    [self presentViewController:shareViewController animated:YES completion:nil];
}

- (void)showSavedAlert {
    [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Saved", @"Saved alert title") message:LinguiLocalizedString(@"Your files have been saved to the documents directory and can be accessed from iTunes.", @"Saved to documents alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
}

- (void)shareSongWithMethod:(NSString*)method indexPath:(NSIndexPath*)indexPath {
    [HWAppDelegate recordEvent:@"share_song_from_song_view" segmentation:@{ @"method" : method, @"section" : @(indexPath.section), @"row" : @(indexPath.row), @"indexPath" : @[@(indexPath.section),@(indexPath.row)] }];
    
    enum ShareVia shareVia = (enum ShareVia)[indexPath section];
    
    if (ShareMethodIsSoundCloud(indexPath)) {
        NSString *bouncePath = [[[[[NSFileManager defaultManager] createUniqueTemporaryDirectory] stringByAppendingPathComponent:self.doc.fileURL.lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_AUDIO_EXTENSION];
        if (!bouncePath) {
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"Failed to create temporary directory" userInfo:nil];
        }
        [self bounceToPath:bouncePath completion:^(BOOL success) {
            if (success) {
                [self postToSoundCloud:[NSURL fileURLWithPath:bouncePath]];
            }
        }];
    }
    else if (AudioShareAsAudio(indexPath)) {
        NSString *name = [self.doc.fileURL.lastPathComponent stringByDeletingPathExtension];
        NSString *bouncePath = [[[[[NSFileManager defaultManager] createUniqueTemporaryDirectory] stringByAppendingPathComponent:self.doc.fileURL.lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_AUDIO_EXTENSION];
        if (!bouncePath) {
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"Failed to create temporary directory" userInfo:nil];
        }
        [self bounceToPath:bouncePath completion:^(BOOL success) {
            if (success) {
                [[AudioShare sharedInstance] addSoundFromURL:[NSURL fileURLWithPath:bouncePath] withName:name];
            }
        }];
    }
    else if (AudioShareAsMIDI(indexPath)) {
        NSData *data = [self.doc.song convertToMIDIData];
        if (data) {
            NSString *name = [self.doc.fileURL.lastPathComponent stringByDeletingPathExtension];
            [[AudioShare sharedInstance] addSoundFromData:data withName:name];
        }
    }
    else if (EMailAudioFile(indexPath) || SaveAudioFile(indexPath)) {
        NSString *bouncePath = [[[[[NSFileManager defaultManager] createUniqueTemporaryDirectory] stringByAppendingPathComponent:self.doc.fileURL.lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_AUDIO_EXTENSION];
        if (!bouncePath) {
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"Failed to create temporary directory" userInfo:nil];
        }
        __weak typeof(self) weakSelf = self;
        [self bounceToPath:bouncePath completion:^(BOOL success) {
            __strong typeof(self) strongSelf = weakSelf;
            if (success) {
                if (shareVia == kShareVia_Email) {
                    NSData *data = [NSData dataWithContentsOfFile:bouncePath];
                    [self shareByEmailWithAttachment:data mimeType:@"audio/aif" filename:bouncePath.lastPathComponent];
                }
                else if (shareVia == kShareVia_iTunes) {
                    copyFilesToDocumentsDirectory(@[bouncePath]);
                    [strongSelf showSavedAlert];
                }
            }
        }];
    }
    else if (EMailSongFile(indexPath) || SaveSongFile(indexPath)) {
        if (shareVia == kShareVia_Email) {
            NSData *data = [self.doc dataForExport];
            NSString *filename = [self.doc.fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:HW_GZIPPED_EXTENSION];
            [self shareByEmailWithAttachment:data mimeType:@"song/x-hwgz" filename:filename];
        }
        else if (shareVia == kShareVia_iTunes) {
            NSString *path = [[[[NSString documentsDirectory] stringByAppendingPathComponent:[self.doc.fileURL lastPathComponent]] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_GZIPPED_EXTENSION];
            NSData *data = [self.doc dataForExport];
            [data writeToFile:path atomically:YES];
            [self showSavedAlert];
        }
    }
    else if (EMailLilypondFile(indexPath) || SaveLilypondFile(indexPath)) {
        NSString *lilypond = [self.doc.song lilypondScript];
        /*
        NSString *tempDirPath = [[NSFileManager defaultManager] createUniqueTemporaryDirectory];
        NSURL *lilypondURL = [NSURL fileURLWithPath:[tempDirPath stringByAppendingPathComponent:@"file.ly"]];
        NSError *error = nil;
        [lilypond writeToURL:lilypondURL atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            [NSException raise:@"HarmonyWiz" format:@"%@", error.localizedDescription];
        }
        self.documentInteraction = [UIDocumentInteractionController interactionControllerWithURL:lilypondURL];
        self.documentInteraction.delegate = self;
        [self.documentInteraction presentOptionsMenuFromRect:self.view.frame inView:self.view animated:YES];
//        [self.documentInteraction presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
//        [dic presentOptionsMenuFromBarButtonItem:self.btnSong animated:YES];
//        [dic presentOpenInMenuFromBarButtonItem:self.btnSong animated:YES];
        */

        if (shareVia == kShareVia_Email) {
            NSData *data = [lilypond dataUsingEncoding:NSUTF8StringEncoding];
            NSString *filename = [self.doc.fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:HW_LILYPOND_EXTENSION];
            [self shareByEmailWithAttachment:data mimeType:@"text/x-lilypond" filename:filename];
        }
        else if (shareVia == kShareVia_iTunes) {
            NSString *path = [[[[NSString documentsDirectory] stringByAppendingPathComponent:[self.doc.fileURL lastPathComponent]] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_LILYPOND_EXTENSION];
            NSError *error = nil;
            [lilypond writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
            if (error) {
                NSLog(@"%@", error.localizedDescription);
                recordError(error)
            }
            else {
                [self showSavedAlert];
            }
        }
    }
    else if (EMailMusicXMLFile(indexPath) || SaveMusicXMLFile(indexPath)) {
        NSData *mxl_data = [self.doc.song MXL_data:self.doc.fileURL.lastPathComponent.stringByDeletingPathExtension];
        if (shareVia == kShareVia_Email) {
            NSString *filename = [self.doc.fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:HW_MUSICXML_EXTENSION];
            [self shareByEmailWithAttachment:mxl_data mimeType:@"application/vnd.recordare.musicxml" filename:filename];
        }
        else if (shareVia == kShareVia_iTunes) {
            NSString *path = [[[[NSString documentsDirectory] stringByAppendingPathComponent:[self.doc.fileURL lastPathComponent]] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_MUSICXML_EXTENSION];
            BOOL success = [mxl_data writeToFile:path atomically:YES];
            if (! success) {
                recordError([NSError errorWithDomain:@"Failed to write MXL data" code:1 userInfo:nil])
            }
            else {
                [self showSavedAlert];
            }
        }
    }
    else if (EMailMIDIFile(indexPath) || SaveMIDIFile(indexPath)) {
        NSData *data = [self.doc.song convertToMIDIData];
        if (shareVia == kShareVia_Email) {
            NSString *filename = [self.doc.fileURL.lastPathComponent.stringByDeletingPathExtension stringByAppendingPathExtension:HW_MIDI_EXTENSION];
            [self shareByEmailWithAttachment:data mimeType:@"audio/midi" filename:filename];
        }
        else if (shareVia == kShareVia_iTunes) {
            NSString *path = [[[[NSString documentsDirectory] stringByAppendingPathComponent:[self.doc.fileURL lastPathComponent]] stringByDeletingPathExtension] stringByAppendingPathExtension:HW_MIDI_EXTENSION];
            [data writeToFile:path atomically:YES];
            [self showSavedAlert];
        }
    }
}

- (void)shareByEmailWithAttachment:(NSData*)data mimeType:(NSString*)mimeType filename:(NSString*)filename {
    if (![MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    [mailVC setSubject:LinguiLocalizedString(@"Shared HarmonyWiz Song", @"Shared one song mail subject")];
    [mailVC setMessageBody:LinguiLocalizedString(@"I would like to share this HarmonyWiz song with you.", @"Shared one song mail body") isHTML:NO];
    [mailVC addAttachmentData:data mimeType:mimeType fileName:filename];
    [mailVC setMailComposeDelegate:self];
    
    [[AudioController sharedAudioController] stopProcessingGraph];
    
    [self presentViewController:mailVC animated:YES completion:nil];
}

- (UIImage *) scoreSnapshotWithSize:(CGSize)size withHarmonyTrack:(BOOL)harmony {
    UIImage * img;
    
    // image rect
    const CGRect rect = CGRectMake(0.f, 0.f, size.width, size.height);
    
    if (self.doc.metadata.magicMode) {
        HarmonyWizStaveView *sv = [self.allStavesControl.musicStaveViews objectAtIndex:0];
        const CGFloat scoreHeight = sv.touchableFrame.size.height * self.allStavesControl.musicStaveViews.count;
        const CGFloat vertScaleFactor = rect.size.height / (scoreHeight + self.mainView.magicView.frame.size.height);
        const CGFloat horizScaleFactor = rect.size.width / (self.mainView.magicView.frame.size.width);
        
        // grab screenshot of main view
        UIGraphicsBeginImageContextWithOptions(rect.size, YES, 0.0);
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        
        CGContextSaveGState(ctx);
        CGContextScaleCTM(ctx, horizScaleFactor, vertScaleFactor);
        CGContextTranslateCTM(ctx, -self.mainView.stavesView.contentOffset.x, 0);
        [self.allStavesControl.layer renderInContext:ctx];
        CGContextRestoreGState(ctx);
        
        CGContextSaveGState(ctx);
        CGContextScaleCTM(ctx, horizScaleFactor, vertScaleFactor);
        CGContextTranslateCTM(ctx, -self.mainView.magicView.contentOffset.x, scoreHeight);
        [self.mainView.magicView.layer renderInContext:ctx];
        CGContextRestoreGState(ctx);
        
        img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else {
        const CGFloat verticalScaleFactor = rect.size.height / (self.allStavesControl.frame.size.height + (self.doc.metadata.pianoOpened ? self.keyboardWizdom.frame.size.height : 0));
        const CGFloat horizScaleFactor = rect.size.width / (self.allStavesControl.frame.size.width - HWToolboxWidth);
        
        // grab screenshot of main view
        UIGraphicsBeginImageContextWithOptions(rect.size, YES, 0.0);
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        
        CGContextSaveGState(ctx);
        CGContextScaleCTM(ctx, horizScaleFactor, verticalScaleFactor);
        CGContextTranslateCTM(ctx, -self.mainView.stavesView.contentOffset.x, -self.mainView.stavesView.contentOffset.y);
        [self.allStavesControl.layer renderInContext:ctx];
        CGContextRestoreGState(ctx);
        
        if (self.doc.metadata.pianoOpened) {
            CGContextSaveGState(ctx);
            CGContextScaleCTM(ctx, horizScaleFactor, verticalScaleFactor);
            CGContextTranslateCTM(ctx, -CGRectGetMaxX(self.mainView.tracksView.frame), self.keyboardWizdom.frame.origin.y - self.allStavesControl.frame.origin.y);
            [self.keyboardWizdom.layer renderInContext:ctx];
            CGContextRestoreGState(ctx);
        }
        
        img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return img;
}

- (UIImage *) scoreSnapshot {
    return [self scoreSnapshotWithSize:CGSizeMake(512.f, 384.f) withHarmonyTrack:YES];
}


- (NSUndoManager*)undoManager {
    return self.doc.undoManager;
}

#pragma mark - notifications

- (void)playbackPositionChanged:(NSNotification*)notif {
    [self.rulerControl updatePlaybackIndicatorPosition];
    [self.allStavesControl updatePositionIndicator];
    [self scrollToPlaybackPosition];
}

- (void)touchedRuler:(NSNotification*)notif {
    [self.allStavesControl updatePositionIndicator];
}

- (void)volumeChanged:(NSNotification*)notif {
    float volume = [[notif.userInfo objectForKey:@"volume"] floatValue];
    NSInteger channel = [[notif.userInfo objectForKey:@"channel"] integerValue];
    [self midiVolumeChange:volume channel:channel];
}

- (void)performArrange:(NSNotification*)notif {
    [self closeAllPopovers];
    [self beginArrange];
}

- (void)painted:(NSNotification*)notif {
    [[self.allStavesControl.musicStaveViews objectAtIndex:0] updateStave];
    [self.rulerControl updatePlaybackIndicatorPosition];
    [self.allStavesControl updatePositionIndicator];
    
    if (self.state == kStatePlaying) {
        OSStatus result;
        
        result = MusicPlayerStop(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStop result is %d", (int)result];
        
        MusicTimeStamp timeStamp;
        result = MusicPlayerGetTime(self.musicPlayer, &timeStamp);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerGetTime result is %d", (int)result];
        
        [self updateMusicPlayer:NO precount:0];
        
        result = MusicPlayerSetTime(self.musicPlayer, timeStamp);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerSetTime result is %d", (int)result];
        
        result = MusicPlayerStart(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStart result is %d", (int)result];
    }
}

- (void)panChanged:(NSNotification*)notif {
    float pan = [[notif.userInfo objectForKey:@"pan"] floatValue];
    NSInteger channel = [[notif.userInfo objectForKey:@"channel"] integerValue];
    [self midiPanChange:pan channel:channel];
}

- (void)willSelectTrack:(NSNotification*)notif {
    HarmonyWizTrackControl *tc = [notif.userInfo objectForKey:@"track"];
    [self.keyboardWizdom releaseAllNotes];
    if (self.doc.metadata.pianoOpened) {
        [self.keyboardWizdom showRangeForTrack:tc.track];
    }
}

- (void)didSelectTrack:(NSNotification*)notif {
    [self updateToolbarAndRuler];
    [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
}

- (void)lockStatusChanged:(NSNotification*)notif {
    [self redrawStaves:NO];
}

- (void)soloMuteStatusChanged:(NSNotification*)notif {
    [self.doc applyMuteAndSolo];
    [self midiApplyMuteAndSolo];
    [self redrawStaves:NO];
}

- (void)allNotesOff:(NSNotification*)notif {
    [self closeAllPopovers];
    
    RIButtonItem *ok = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"OK", @"OK button") action:^{
        [[AudioController sharedAudioController] sendAllNotesOff];
    }];
    
    self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"All notes off", @"MIDI panic alert title") message:LinguiLocalizedString(@"Send all notes off", @"MIDI panic alert message") cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:ok, nil];
    
    [self.currentAlert show];
}

- (void)clearSong:(NSNotification*)notif {
    [self closeAllPopovers];
    [self.doc.hwUndoManager startUndoableChanges];
    [self.doc.song clearBars:self.doc.song.finalBarNumber];
    [self.doc.song selectNone];
    [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
    [self.doc.song.misc setObject:@[] forKey:@"shapeViews"];
    [self.doc.hwUndoManager finishUndoableChangesName:@"Clear Song"];
    self.doc.metadata.harmonyScore = nil;
    [self applyDefaultStaveHeight];
    [self.mainView.stavesView setContentOffset:CGPointZero animated:NO];
    [self redrawStaves:NO];
}

- (void)clearArrangement:(NSNotification*)notif {
    [self closeAllPopovers];
    [self.doc.hwUndoManager startUndoableChanges];
    [self.doc.song clearArrangement];
    [self.doc.song clearHarmonyRelaxConstraints];
    [self.doc.song selectNone];
    [self.doc.song spliceHarmony];
    self.doc.metadata.harmonyScore = nil;
    [self.doc.hwUndoManager finishUndoableChangesName:@"Clear Arrangement"];
    [self redrawStaves:NO];
}

- (void)clearMagicPad:(NSNotification*)notif {
    [self closeAllPopovers];
    [self.doc.hwUndoManager startUndoableChanges];
    [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
    HarmonyWizMusicTrack *tr = [self.doc.song.musicTracks objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber];
    [tr.events removeAllObjects];
    [self.doc.song addFillerRestsTo:tr.events];
    [self.doc.song selectNone];
    [self.doc.hwUndoManager finishUndoableChangesName:@"Clear Magic Pad"];
    [self redrawStaves:NO];
}

- (void)addBar:(NSNotification*)notif {
    [self.mainView setNeedsLayout];
    [self redrawStaves:YES];
}

- (void)removeBar:(NSNotification*)notif {
    [self.mainView setNeedsLayout];
    [self redrawStaves:YES];
}

- (void)freezeTrack:(NSNotification*)notif {
    [self closeAllPopovers];
    /*HarmonyWizSharedObjects *so = [HarmonyWizSharedObjects sharedManager];
     HarmonyWizTrack *track = [so currentlySelectedTrack];
     [self.doc.hwUndoManager startUndoableChanges];
     if (track != self.doc.song.harmony) {
     track.events = [self.doc.song unifiedEvents:track];
     }
     [self.doc.song spliceHarmony];
     [self.doc.hwUndoManager finishUndoableChangesName:@"Unified Track"];
     [self redrawStaves:NO];*/
    
    HarmonyWizTrackControl *tc = [notif.userInfo objectForKey:@"trackControl"];
    HarmonyWizTrack *track = tc.track;
    if (track == self.doc.song.harmony) {
        [self.doc.hwUndoManager startUndoableChanges];
        for (HarmonyWizHarmonyEvent *ev in self.doc.song.harmony.events) {
            [ev.constraints removeAllObjects];
            id degProp = [ev.properties objectForKey:HarmonyWizDegreeProperty];
            id CTProp = [ev.properties objectForKey:HarmonyWizChordTypeProperty];
            id IProp = [ev.properties objectForKey:HarmonyWizInversionProperty];
            if (degProp)
                [ev.constraints setObject:degProp forKey:HarmonyWizDegreeProperty];
            if (CTProp)
                [ev.constraints setObject:CTProp forKey:HarmonyWizChordTypeProperty];
            if (IProp)
                [ev.constraints setObject:IProp forKey:HarmonyWizInversionProperty];
            [ev.properties removeAllObjects];
        }
        [self.doc.hwUndoManager finishUndoableChangesName:@"Freeze Harmony"];
        [self redrawStaves:NO];
    }
    else if (track) {
        if (track) {
            [self.doc.hwUndoManager startUndoableChanges];
            for (HarmonyWizMusicEvent *ev in track.events) {
                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)ev;
                    if (! ne.wasCreatedByUser) {
                        ne.type = kNoteEventType_Harmony;
                    }
                }
            }
            [self.doc.song spliceHarmony];
            [self.doc.hwUndoManager finishUndoableChangesName:@"Freeze Track"];
            [self redrawStaves:NO];
        }
    }
}

- (void)clearTrack:(NSNotification*)notif {
    [self closeAllPopovers];
    HarmonyWizTrackControl *tc = [notif.userInfo objectForKey:@"trackControl"];
    HarmonyWizTrack *track = tc.track;
    if (track == self.doc.song.harmony) {
        [self.doc.hwUndoManager startUndoableChanges];
        for (HarmonyWizHarmonyEvent *ev in self.doc.song.harmony.events) {
            [ev.properties removeAllObjects];
            [ev.constraints removeAllObjects];
        }
        [self.doc.hwUndoManager finishUndoableChangesName:@"Clear Harmony"];
        [self redrawStaves:NO];
    }
    else if (track) {
        [self.doc.hwUndoManager startUndoableChanges];
        [track.events removeAllObjects];
        if ([track isKindOfClass:[HarmonyWizMusicTrack class]])
            [self.doc.song addFillerRestsTo:track.events];
        [self.doc.song clearHarmonyProperties];
        self.doc.metadata.harmonyScore = nil;
        [self.doc.song spliceHarmony];
        [self.doc.hwUndoManager finishUndoableChangesName:@"Clear Track"];
        [self redrawStaves:NO];
    }
}

- (void)timeSignChanged:(NSNotification*)notif {
    [self redrawStaves:YES];
    [self applyDefaultStaveHeight];
    const UInt16 upper = self.doc.song.timeSign.upperNumeral;
    const UInt16 lower = self.doc.song.timeSign.lowerNumeral;
    [HWAppDelegate recordEvent:@"time_sign_changed" segmentation:@{ @"upper" : @(upper), @"lower" : @(lower), @"time_sign" : @[@(upper),@(lower)] }];
}

- (void)clefMenu:(NSNotification*)notif {
    CGPoint loc = [[notif.userInfo objectForKey:@"location"] CGPointValue];
    if (! self.clefPopover.popoverVisible) {
        [self closeAllPopovers];
        self.clefViewController.staveView = notif.object;
        [self.clefPopover presentPopoverFromRect:CGRectMake(loc.x-2.f, loc.y-2.f, 4.f, 4.f) inView:self.clefViewController.staveView.parentScrollView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        [self registerPopover:self.clefPopover];
    }
}

- (void)registerPopover:(UIPopoverController*)pc {
    [self.visiblePopovers addObject:pc];
    self.lastPopoverShown = pc;
}

- (void)editConstraints:(NSNotification*)notif {
    HarmonyWizStaveView *stave = notif.object;
    HarmonyWizHarmonyEvent *he = [notif.userInfo objectForKey:HarmonyWizEditConstraintsUserInfoKey];
    CGFloat xpos = [self.rulerControl xposFromEvent:he];
    CGFloat ypos = stave.touchableFrame.size.height * 0.5f;
    CGPoint loc = [self.allStavesControl convertPoint:CGPointMake(xpos, ypos) fromStave:stave];
    
    id vc = self.chordAttributesViewController.topViewController;
    if ([vc isKindOfClass:[HWChordAttributesViewController class]]) {
        HWChordAttributesViewController *attributesVC =  vc;
        attributesVC.harmonyEvent = he;
        attributesVC.navigationController.delegate = self;
        attributesVC.navigationController.navigationBar.translucent = NO;
        [self.constraintsPopover presentPopoverFromRect:CGRectMake(loc.x-2.f, loc.y-2.f, 4.f, 4.f) inView:stave.parentScrollView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        [self registerPopover:self.constraintsPopover];
        [self killScroll];
    }
}

- (void)constraintModified:(NSNotification*)notif {
    self.doc.metadata.harmonyScore = nil;
    HWChordAttributeSubmenuViewController *vc = notif.object;
    HWChordAttributesViewController *cavc = [vc.navigationController.viewControllers objectAtIndex:0];
    
    NSArray *staveEvents = [self.allStavesControl.harmonyStaveView staveEventsFromMusicEvents:@[cavc.harmonyEvent]];
    [self.allStavesControl.harmonyStaveView updateStaveEventsIn:staveEvents];
    for (id harmonyLabel in [(HWEvent*)staveEvents.lastObject harmonyInfoLabels]) {
        [self.allStavesControl.harmonyStaveView layoutHarmonyLabel:harmonyLabel];
        {
            CGRect frame = [self.allStavesControl convertRect:[harmonyLabel frame] fromStave:self.allStavesControl.harmonyStaveView];
            [harmonyLabel setFrame:frame];
        }
    }
}

- (void)languageChanged:(NSNotification*)notif {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self redrawStaves:YES];
    });
}

- (void)keyChanged:(NSNotification*)notif {
    self.doc.metadata.harmonyScore = nil;
    [self.rulerControl updatePlaybackIndicatorPosition];
    [self.allStavesControl updatePositionIndicator];
    [self.allStavesControl.musicStaveViews makeObjectsPerformSelector:@selector(updateKeySignatureOrMode)];
    [self applyDefaultStaveHeight];
    [self.trackControlContentView.tracks.lastObject updateInstrument];
    [self redrawStaves:YES];
    
    const BOOL tonality = self.doc.song.keySign.tonality;
    const UInt16 root = self.doc.song.keySign.root;
    [HWAppDelegate recordEvent:@"key_sign_changed" segmentation:@{ @"root" : @(root), @"tonality" : @(tonality), @"key_sign" : @[@(root),@(tonality)] }];
}

- (void)modeChanged:(NSNotification*)notif {
    self.doc.metadata.harmonyScore = nil;
    [self.rulerControl updatePlaybackIndicatorPosition];
    [self.allStavesControl updatePositionIndicator];
    [self.allStavesControl.musicStaveViews makeObjectsPerformSelector:@selector(updateKeySignatureOrMode)];
    [self applyDefaultStaveHeight];
    [self.trackControlContentView.tracks.lastObject updateInstrument];
    [self redrawStaves:YES];
    
    const NSString *name = [HWModeMenu modeNameForID:self.doc.song.songMode.modeID];
    [HWAppDelegate recordEvent:@"mode_changed" segmentation:@{ @"name" : name, @"transposition" : @(self.doc.song.songMode.transposition) }];
}

- (void)helpMenuPressed:(NSNotification*)notif {
    NSIndexPath *indexPath = [notif.userInfo objectForKey:@"indexPath"];
    NSString *name = [notif.userInfo objectForKey:@"name"];
    [self closeAllPopovers];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kPresentViewDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (indexPath.section == 0) {
            self.tutorialOpenedName = name;
            [self performSegueWithIdentifier:@"showHelp" sender:self];
        }
        else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                [self performSegueWithIdentifier:@"showFAQ" sender:self];
            }
            else if (indexPath.row == 1) {
                [self contactSupport];
            }
        }
        else if (indexPath.section == 2) {
            if (indexPath.row == 0) {
                [self performSegueWithIdentifier:@"showAbout" sender:self];
            }
            else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.harmonywiz.com/privacy"]];
            }
        }
    });
}

NSString* machineName() {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

- (void)cannotSendMailAlert {
    [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Excuse me",@"Alert title") message:LinguiLocalizedString(@"This device is not currently able to send mail.", @"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
}

- (void)contactSupport {
    if (![MFMailComposeViewController canSendMail]) {
        [self cannotSendMailAlert];
        return;
    }
    
    NSString *info = [NSString stringWithFormat:@"HarmonyWiz version: %@\niOS version: %@\nDevice model: %@\nPlease enter your question below this line\n--------\n\n", [UIApplication appVersion], [[UIDevice currentDevice] systemVersion], machineName()];
    
    NSString *to = HWSupportEmailAddress;
    NSString *subject = @"HarmonyWiz customer support";
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    [mailVC setToRecipients:@[to]];
    [mailVC setSubject:subject];
    [mailVC setMessageBody:info isHTML:NO];
    [mailVC setMailComposeDelegate:self];
    
    [[AudioController sharedAudioController] stopProcessingGraph];
    [self presentViewController:mailVC animated:YES completion:nil];
    
    /*
     info = [info stringByReplacingOccurrencesOfString:@"\n" withString:@"%0A"];
     info = [info stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
     subject = [subject stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSString *url = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@", to, subject, info];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
     */
}

- (void)songTitle:(NSNotification*)notif {
    NSString *title = [notif.userInfo objectForKey:@"title"];
    if (! [self.doc.song.title isEqualToString:title]) {
        [self.doc.hwUndoManager startUndoableChanges];
        self.doc.song.title = title;
        [self.doc.hwUndoManager finishUndoableChangesName:@"Song title"];
    }
    
    // must become first responder again
    [self becomeFirstResponder];
}

- (void)makeSelectionNoteType:(enum NoteEventType)type {
    if (self.doc.song.anythingSelected) {
        BOOL changed = NO;
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        [self.doc.hwUndoManager startUndoableChanges];
        for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks) {
            for (HarmonyWizMusicEvent *ev in mt.events) {
                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && ev.selected) {
                    HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)ev;
                    ne.type = type;
                    changed = YES;
                }
            }
            // handle non-export mode
            if (! expertMode) {
                [self.doc.song removeOtherEventsOverlappingWith:mt.selectedEvents];
            }
        }
        if (changed) {
            self.doc.metadata.harmonyScore = nil;
            [self.doc.song spliceHarmony];
            if (expertMode) {
                [self.doc.hwUndoManager finishUndoableChangesName:@"Note Type"];
            }
            else {
                [self beginArrange];
            }
        }
        else {
            [self.doc.hwUndoManager cancelUndoableChanges];
        }
    }
    
    {
        NSString *typeString = nil;
        switch (type) {
            case kNoteEventType_Harmony:
                typeString = @"Harmony";
                break;
            case kNoteEventType_Passing:
                typeString = @"Passing";
                break;
            case kNoteEventType_Suspension:
                typeString = @"Suspension";
                break;
            case kNoteEventType_Auto:
                typeString = @"Arrange";
                break;
            default:
                typeString = @"???";
                break;
        }
        [HWAppDelegate recordEvent:@"change_note_type_of_selection" segmentation:@{ @"type" : typeString }];
    }
}

- (void)autoTieNotes:(id)sender {
    [self.doc.hwUndoManager startUndoableChanges];
    
    [self.doc.song autoTieSelectedNotes];
    
    self.doc.metadata.harmonyScore = nil;
    
    [self.doc.hwUndoManager finishUndoableChangesName:@"Auto-tie Selection"];
    
    [self redrawStaves:NO];
}

- (void)tieNotes:(id)sender {
    [self.doc.hwUndoManager startUndoableChanges];
    
    const NSArray *selectedTracks = [self.doc.song.musicTracks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"anythingSelected == YES"]];
    HarmonyWizMusicTrack *track = selectedTracks.lastObject;
    const NSArray *selectedEvents = [[track events] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.selected == YES"]];
    HarmonyWizNoteEvent *firstNote = [selectedEvents objectAtIndex:0];
    HarmonyWizNoteEvent *lastNote = selectedEvents.lastObject;
    const UInt16 dur = ([self.doc.song eventEndSubdivision:lastNote] - [self.doc.song eventStartSubdivision:firstNote]);
    
    HarmonyWizNoteEvent *newNote = [firstNote copy];
    [self.doc.song setEventDuration:newNote to:dur];
    NSIndexSet *is = [self.doc.song overlappingEvents:track.events forEvent:newNote];
    [track.events removeObjectsAtIndexes:is];
    [track.events placeObject:newNote];
    
    [self.doc.song spliceHarmony];
    
    [self.doc.hwUndoManager finishUndoableChangesName:@"Tie notes"];
    
    [self redrawStaves:NO];
}

- (void)leadingNotesFree:(id)sender {
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizHarmonyEvent *he in [self.doc.song harmonyEventsOverlappingSelection]) {
        [he.properties removeAllObjects];
        [he.constraints removeObjectForKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
    [self.doc.hwUndoManager finishUndoableChangesName:@"free leading notes"];
    [self redrawStaves:NO];
}

- (void)ascendingMelodicMinor:(id)sender {
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizHarmonyEvent *he in [self.doc.song harmonyEventsOverlappingSelection]) {
        [he.properties removeAllObjects];
        [he.constraints setObject:HarmonyWizLeadingNoteTreatmentMelodic forKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
    [self.doc.hwUndoManager finishUndoableChangesName:@"ascending melodic minor"];
    [self redrawStaves:NO];
}

- (void)harmonicMinor:(id)sender {
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizHarmonyEvent *he in [self.doc.song harmonyEventsOverlappingSelection]) {
        [he.properties removeAllObjects];
        [he.constraints setObject:HarmonyWizLeadingNoteTreatmentHarmonic forKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
    [self.doc.hwUndoManager finishUndoableChangesName:@"harmonic minor"];
    [self redrawStaves:NO];
}

- (void)leadingNotes:(id)sender {
    UIMenuController *menu = [UIMenuController sharedMenuController];
    
    if (1) {
        UIMenuItem *free = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Free", @"Menu item") action:@selector(leadingNotesFree:)];
        UIMenuItem *melodicMinor = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Ascending melodic minor", @"Menu item") action:@selector(ascendingMelodicMinor:)];
        UIMenuItem *harmonicMinor = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Harmonic minor", @"Menu item") action:@selector(harmonicMinor:)];
        self.leadingNotesMenuItems = @[free,melodicMinor,harmonicMinor];
    }
    
    [menu setMenuItems:self.leadingNotesMenuItems];
    
    CGRect targetRect = CGRectInset(self.allStavesControl.selectBoxView.virtualFrame, 6 ,6);
    [menu setTargetRect:targetRect inView:self.allStavesControl];
    [menu setMenuVisible:YES animated:YES];
}

- (void)enharmonicShift:(id)sender {
    UIMenuController *menu = [UIMenuController sharedMenuController];
    
    if (1) {
        UIMenuItem *defaultAccidental = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Default accidental", @"Menu item") action:@selector(defaultAccidental:)];
        UIMenuItem *flatsToSharps = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Flats to sharps", @"Menu item") action:@selector(flatsToSharps:)];
        UIMenuItem *sharpsToFlats = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Sharps to flats", @"Menu item") action:@selector(sharpsToFlats:)];
        self.enharmonicShiftMenuItems = @[defaultAccidental,flatsToSharps,sharpsToFlats];
    }
    
    [menu setMenuItems:self.enharmonicShiftMenuItems];
    
    CGRect targetRect = CGRectInset(self.allStavesControl.selectBoxView.virtualFrame, 6 ,6);
    [menu setTargetRect:targetRect inView:self.allStavesControl];
    [menu setMenuVisible:YES animated:YES];
}

- (void)noteType:(id)sender {
    UIMenuController *menu = [UIMenuController sharedMenuController];
    
    if (1) {
        UIMenuItem *harmony = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Harmony", @"Menu item") action:@selector(makeSelectionHarmony:)];
        UIMenuItem *passing = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Passing", @"Menu item") action:@selector(makeSelectionPassing:)];
        UIMenuItem *suspension = [[UIMenuItem alloc] initWithTitle:LinguiLocalizedString(@"Suspension", @"Menu item") action:@selector(makeSelectionSuspension:)];
        self.noteTypeMenuItems = @[harmony,passing,suspension];
    }
    
    [menu setMenuItems:self.noteTypeMenuItems];
    
    CGRect targetRect = CGRectInset(self.allStavesControl.selectBoxView.virtualFrame, 6 ,6);
    [menu setTargetRect:targetRect inView:self.allStavesControl];
    [menu setMenuVisible:YES animated:YES];
}

- (void)togglePause:(id)sender {
    const NSArray *selectedTracks = [[self.doc.song.musicTracks arrayByAddingObject:self.doc.song.harmony] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"anythingSelected == YES"]];
    HarmonyWizHarmonyEvent *he = nil;
    for (HarmonyWizMusicTrack *track in selectedTracks) {
        const NSArray *selectedEvents = [[track events] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.selected == YES"]];
        HarmonyWizHarmonyEvent *thisHE = [self.doc.song findLastHarmonyEventForEvent:selectedEvents.lastObject];
        if (he == nil || [self.doc.song eventStartSubdivision:thisHE] >= [self.doc.song eventStartSubdivision:he]) {
            he = thisHE;
        }
    }
    
    he.pause = ! he.pause;
    
    [self redrawStaves:YES];
}

- (void)performEnharmonicShiftTo:(enum EnharmonicShift)shift withUndoName:(NSString*)name {
    NSMutableSet *tracksToRedraw = [NSMutableSet setWithCapacity:5];
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizMusicTrack *track in self.doc.song.musicTracks) {
        for (id ev in track.events) {
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && [ev selected]) {
                if ([self.doc.song canNote:ev performEnharmonicShiftTo:shift]) {
                    [ev setEnharmonicShift:shift];
                    [tracksToRedraw addObject:track];
                }
            }
        }
    }
    [self.doc.hwUndoManager finishUndoableChangesName:name];
    for (HarmonyWizMusicTrack *track in tracksToRedraw) {
        HarmonyWizStaveView *stave = [self.allStavesControl.musicStaveViews objectAtIndex:track.trackNumber];
        [stave flushStave];
        [stave updateStave];
    }
    self.doc.metadata.harmonyScore = nil;
    
    {
        NSString *shiftString = nil;
        switch (shift) {
            case kDefaultAccidental:
                shiftString = @"default";
                break;
            case kSharpsToFlats:
                shiftString = @"to_sharps";
                break;
            case kFlatsToSharps:
                shiftString = @"to_flats";
                break;
            default:
                shiftString = @"???";
                break;
        }
        [HWAppDelegate recordEvent:@"enharmonic_shift" segmentation:@{ @"type" : shiftString }];
    }
}

- (void)defaultAccidental:(id)sender {
    [self performEnharmonicShiftTo:kDefaultAccidental withUndoName:@"Default accidental"];
}

- (void)flatsToSharps:(id)sender {
    [self performEnharmonicShiftTo:kFlatsToSharps withUndoName:@"Flats to sharps"];
}

- (void)sharpsToFlats:(id)sender {
    [self performEnharmonicShiftTo:kSharpsToFlats withUndoName:@"Sharps to flats"];
}

- (BOOL)allSelectedNotesSamePitch:(NSArray*)events {
    SInt16 pitch = 0;
    BOOL found = NO;
    for (id ev in events) {
        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *note = ev;
            if (note.selected) {
                if (found && pitch != note.noteValue) {
                    return NO;
                }
                else {
                    found = YES;
                    pitch = note.noteValue;
                }
            }
        }
    }
    return found;
}

- (BOOL)selectionIsContiguous:(NSArray*)events {
    UInt16 lastEnd = 0;
    BOOL found = NO;
    for (id ev in events) {
        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *note = ev;
            if (note.selected) {
                if (found) {
                    if ([self.doc.song eventStartSubdivision:note] != lastEnd) {
                        return NO;
                    }
                }
                else {
                    found = YES;
                }
                lastEnd = [self.doc.song eventEndSubdivision:note];
            }
        }
    }
    return found;
}

- (BOOL)canSelectionPerformEnharmonicShiftTo:(enum EnharmonicShift)shift {
    for (HarmonyWizMusicTrack *track in self.doc.song.musicTracks) {
        for (id ev in track.events) {
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && [ev selected]) {
                if ([self.doc.song canNote:ev performEnharmonicShiftTo:shift]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    const BOOL isStopped = (self.state == kStateStopped);
    const BOOL anythingSelected = self.doc.song.anythingSelected;
    const NSArray *selectedTracks = [self.doc.song.musicTracks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"anythingSelected == YES"]];
    const NSUInteger selectedTracksCount = [selectedTracks count];
    
    if ((action == @selector(makeSelectionHarmony:) ||
         action == @selector(makeSelectionPassing:) ||
         action == @selector(makeSelectionSuspension:) ||
         action == @selector(cut:) ||
         action == @selector(copy:) ||
         action == @selector(paste:) ||
         action == @selector(delete:)) &&
        self.doc.song.harmony.anythingSelected) {
        return NO;
    }
    
    if (action == @selector(autoTieNotes:)) {
        if (! isStopped)
            return NO;
        return [self.doc.song canPerformAutoTie];
    }
    
    if (action == @selector(transposeOctaveUp:) && (selectedTracksCount == 1) && [selectedTracks.lastObject selectionIsBelowRange]) {
        if (! isStopped)
            return NO;
        return YES;
    }
    else if (action == @selector(transposeOctaveDown:) && (selectedTracksCount == 1) && [selectedTracks.lastObject selectionIsAboveRange]) {
        if (! isStopped)
            return NO;
        return YES;
    }
    else if ((action == @selector(transposeOctaveUp:) || action == @selector(transposeOctaveDown:))) {
        return NO;
    }
    
    if (anythingSelected) {
        if (! isStopped)
            return NO;
        if (action == @selector(makeSelectionHarmony:) ||
            action == @selector(makeSelectionPassing:) ||
            action == @selector(makeSelectionSuspension:) ||
            action == @selector(arrangeSelection:))
            return YES;
        if (selectedTracksCount > 1 && (action == @selector(cut:)||action == @selector(copy:)))
            return NO;
    }
    
    if (action == @selector(paste:) && ![[UIPasteboard generalPasteboard] containsPasteboardTypes:@[HWEventsPasteboardType]])
        return NO;
    
    if (action == @selector(paste:) && self.allStavesControl.selectBoxView == nil && self.doc.metadata.currentlySelectedTrackNumber == self.doc.song.harmony.trackNumber)
        return NO;
    
    if (action == @selector(tieNotes:)) {
        if (! isStopped)
            return NO;
        NSArray *events = [selectedTracks.lastObject events];
        NSArray *selectedEvents = [events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.selected == YES"]];
        return (selectedTracksCount == 1 && [self allSelectedNotesSamePitch:events] && [self selectionIsContiguous:events] && selectedEvents.count > 1);
    }
    
    if (action == @selector(leadingNotes:)) {
        return (self.doc.song.keySign.tonality == kTonality_Minor);
    }
    
    if (action == @selector(noteType:)) {
        return anythingSelected;
    }
    
    if (action == @selector(enharmonicShift:)) {
        return ([self canSelectionPerformEnharmonicShiftTo:kDefaultAccidental] || [self canSelectionPerformEnharmonicShiftTo:kFlatsToSharps] || [self canSelectionPerformEnharmonicShiftTo:kSharpsToFlats]);
    }
    
    if (action == @selector(leadingNotesFree:) || action == @selector(ascendingMelodicMinor:) || action == @selector(harmonicMinor:)) {
        return YES;
    }
    
    if (action == @selector(togglePause:)) {
        return anythingSelected;
    }
    
    if (action == @selector(defaultAccidental:)) {
        return [self canSelectionPerformEnharmonicShiftTo:kDefaultAccidental];
    }
    
    if (action == @selector(flatsToSharps:)) {
        return [self canSelectionPerformEnharmonicShiftTo:kFlatsToSharps];
    }
    
    if (action == @selector(sharpsToFlats:)) {
        return [self canSelectionPerformEnharmonicShiftTo:kSharpsToFlats];
    }
    
    if (!anythingSelected && (
                              action == @selector(cut:)||
                              action == @selector(copy:)||
                              action == @selector(delete:)||
                              action == @selector(makeSelectionHarmony:) ||
                              action == @selector(makeSelectionPassing:) ||
                              action == @selector(makeSelectionSuspension:) ||
                              action == @selector(arrangeSelection:))) {
        return NO;
    }
    
    if ([[[UIMenuController sharedMenuController] menuItems] isEqualToArray:self.leadingNotesMenuItems] ||
        [[[UIMenuController sharedMenuController] menuItems] isEqualToArray:self.enharmonicShiftMenuItems] ||
        [[[UIMenuController sharedMenuController] menuItems] isEqualToArray:self.noteTypeMenuItems]) {
        return NO;
    }
    
    return [super canPerformAction:action withSender:sender];
}

- (void)cut:(id)sender {
    [self copy:sender];
    [self delete:sender];
}

- (void)copy:(id)sender {
    for (HarmonyWizMusicTrack *tr in self.doc.song.musicTracks) {
        if (tr.anythingSelected) {
            NSArray * selectedEvents = [tr.events filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"selected == YES"]];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:selectedEvents];
            [[UIPasteboard generalPasteboard] setData:data forPasteboardType:HWEventsPasteboardType];
            [HWAppDelegate recordEvent:@"copy"];
            break;
        }
    }
}


- (void)pasteInTrack:(HarmonyWizMusicTrack*)tr start:(UInt16)start end:(UInt16)end {
    NSData *data = [[UIPasteboard generalPasteboard] dataForPasteboardType:HWEventsPasteboardType];
    NSArray *pasteEvents = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    HarmonyWizMusicEvent *pasteEvent0 = [pasteEvents objectAtIndex:0];
    SInt16 offset = (SInt16)start - (SInt16)[self.doc.song eventStartSubdivision:pasteEvent0];
    NSMutableArray *pasteEventsOffset = [NSMutableArray arrayWithCapacity:pasteEvents.count];
    for (HarmonyWizMusicEvent *ev in pasteEvents) {
        HarmonyWizMusicEvent *ev1 = [ev copy];
        UInt16 pos = [self.doc.song eventStartSubdivision:ev1] + offset;
        [self.doc.song setEventPosition:ev1 to:pos];
        if ([self.doc.song eventEndSubdivision:ev1] <= end)
            [pasteEventsOffset addObject:ev1];
    }
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizMusicEvent *ev in pasteEventsOffset) {
        NSIndexSet *is = [self.doc.song overlappingEvents:tr.events forEvent:ev];
        [tr.events removeObjectsAtIndexes:is];
        [tr.events placeObject:ev];
    }
    [self.doc.song addFillerRestsTo:tr.events];
    [self.doc.song spliceHarmony];
    [self.doc.hwUndoManager finishUndoableChangesName:@"Paste"];
    
    [HWAppDelegate recordEvent:@"paste"];
}

- (void)paste:(id)sender {
    if (self.allStavesControl.selectBoxView != nil) {
        for (HarmonyWizMusicTrack *tr in self.doc.song.musicTracks) {
            if (tr.anythingSelected) {
                NSArray * selectedEvents = [tr.events filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"selected == YES"]];
                HarmonyWizMusicEvent *firstEvent = [selectedEvents objectAtIndex:0];
                HarmonyWizMusicEvent *lastEvent = selectedEvents.lastObject;
                UInt16 start = [self.doc.song eventStartSubdivision:firstEvent];
                UInt16 end = [self.doc.song eventEndSubdivision:lastEvent];
                [self pasteInTrack:tr start:start end:end];
                break;
            }
        }
    }
    else {
        HarmonyWizMusicTrack *tr = (id)self.currentlySelectedTrack;
        
        const UInt16 beatSubs = self.doc.song.beatSubdivisions*(self.doc.song.timeSign.lowerNumeral/4);
        
        // get playback position in beat subdivisions
        SInt32 subdiv = (int)(self.doc.song.playbackPosition * beatSubs + 0.5f);
        
        if (subdiv < 0) {
            subdiv = 0;
        }
        
        // apply granularity
        subdiv /= (beatSubs / 8);
        subdiv *= (beatSubs / 8);
        
        UInt16 start = subdiv;
        UInt16 end = self.doc.song.finalBarSubdivision;
        
        [self pasteInTrack:tr start:start end:end];
    }
}
- (void)makeSelectionHarmony:(id)sender {
    [self makeSelectionNoteType:kNoteEventType_Harmony];
}
- (void)makeSelectionPassing:(id)sender {
    [self makeSelectionNoteType:kNoteEventType_Passing];
}
- (void)makeSelectionSuspension:(id)sender {
    [self makeSelectionNoteType:kNoteEventType_Suspension];
}
- (void)delete:(id)sender {
    BOOL changed = NO;
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks) {
        for (NSUInteger idx = 0; idx < mt.events.count; idx++) {
            HarmonyWizMusicEvent *ev = [mt.events objectAtIndex:idx];
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && ev.selected) {
                HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                [self.doc.song setEventPosition:re to:[self.doc.song eventStartSubdivision:ev]];
                [self.doc.song setEventDuration:re to:[self.doc.song eventDuration:ev]];
                [mt.events replaceObjectAtIndex:idx withObject:re];
                changed = YES;
            }
        }
    }
    if (changed) {
        [self.doc.song spliceHarmony];
        [self.doc.hwUndoManager finishUndoableChangesName:@"Convert to rests"];
    }
    else {
        [self.doc.hwUndoManager cancelUndoableChanges];
    }
}
- (void)arrangeSelection:(id)sender {
    [self closeAllPopovers];
    [self beginArrange];
    [HWAppDelegate recordEvent:@"arrange_selection"];
}

- (void)quantization:(NSNotification*)notif {
    NSUInteger quant = [[notif.userInfo objectForKey:@"quantization"] unsignedIntegerValue];
    self.doc.metadata.quantization = (enum Quantization)quant;
    [self.rulerControl redisplay];
    
    [HWAppDelegate recordEvent:@"quantization_changed" segmentation:@{ @"quantization" : @(quant) }];
}

- (void)masterReverb:(NSNotification*)notif {
    SInt32 verbType = (SInt32)[[notif.userInfo objectForKey:@"row"] unsignedIntegerValue];
    self.doc.metadata.masterReverb = verbType;
    [[AudioController sharedAudioController] applyReverb:verbType];
}

- (void)toolChange:(NSNotification*)notif {
    switch (self.doc.metadata.currentToolType) {
        case kToolType_Pen:
        case kToolType_Select:
        case kToolType_Eraser:
            self.mainView.stavesView.panGestureRecognizer.minimumNumberOfTouches = 2;
            self.mainView.stavesView.panGestureRecognizer.maximumNumberOfTouches = 2;
            break;
        case kToolType_Hand:
            self.mainView.stavesView.panGestureRecognizer.minimumNumberOfTouches = 1;
            self.mainView.stavesView.panGestureRecognizer.maximumNumberOfTouches = 2;
            break;
            
        default:
            break;
    }
}

- (void)createTrack:(NSNotification*)notif {
    
    [self closeAllPopovers];
    
    if (self.doc.metadata.magicMode) {
        [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Excuse me", @"Alert title") message:LinguiLocalizedString(@"Please exit magic mode before creating a new track.", @"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
    }
    else if (self.doc.song.musicTracks.count < 5) {
        
        [self.doc.hwUndoManager startUndoableChanges];
        
        NSUInteger trackType = [[notif.userInfo objectForKey:@"row"] unsignedIntegerValue];
        HarmonyWizMusicTrack *track = [self.doc.song addMusicTrackOfType:trackType];
        
        track.instrument = [self defaultInstrumentNameForTrackTitle:track.title];
        
        // select new track
        self.doc.metadata.currentlySelectedTrackNumber = track.trackNumber;
        
        // create controls
        [self clearAllTrackAndStaveControls];
        [self createAllTrackAndStaveControls];
        [self applyAudioSettingsBackground];
        [self applyAllMIDISettings];
        
        // clear harmony score
        self.doc.metadata.harmonyScore = nil;
        
        // auto size score
        [self autoSizeVertical];
        
        // clear magic pad
        [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
        
        // finish undoable changes
        [self.doc.hwUndoManager finishUndoableChangesName:@"Create Track"];
        
        [HWAppDelegate recordEvent:@"new_track" segmentation:@{ @"type" : @(trackType) }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Excuse me", @"Alert title") message:LinguiLocalizedString(@"You cannot have more than five tracks.", @"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil];
        [alert show];
    }
}

- (void)selectAllTrack:(NSNotification*)notif {
    [self closeAllPopovers];
    
    HarmonyWizTrackControl *tc = [notif.userInfo objectForKey:@"trackControl"];
    HarmonyWizTrack *track = tc.track;
    
    if ([track isKindOfClass:[HarmonyWizMusicTrack class]]) {
        HarmonyWizMusicTrack *mt = (id)track;
        
        [self.doc.song selectNone];
        [mt selectAll];
        
        [self redrawStaves:NO];
        
        HarmonyWizStaveView *sv = [self.allStavesControl.musicStaveViews objectAtIndex:mt.trackNumber];
        CGRect rect = sv.touchableFrame;
        rect.size.width = 768;
        [self.allStavesControl showSelectMenuIn:rect];
        
        [HWAppDelegate recordEvent:@"select_all" segmentation:@{ @"trackNumber" : @(mt.trackNumber) }];
    }
}

- (void)removeTrack:(NSNotification*)notif {
    
    [self closeAllPopovers];
    
    HarmonyWizTrackControl *tc = [notif.userInfo objectForKey:@"trackControl"];
    HarmonyWizTrack *track = tc.track;
    
    if (track == self.doc.song.harmony) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Error", @"Error alert title") message:@"Cannot remove harmony track" delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil];
        [alert show];
    }
    else if (self.doc.song.musicTracks.count > 3) {
        
        // clear controls
        [self clearAllTrackAndStaveControls];
        
        [self.doc.hwUndoManager startUndoableChanges];
        
        // get track
        HarmonyWizMusicTrack *musicTrack = (id)track;
        
        // remove track from song
        [self.doc.song removeMusicTrack:musicTrack];
        
        // create controls
        [self createAllTrackAndStaveControls];
        [self applyAudioSettingsBackground];
        [self applyAllMIDISettings];
        
        // select new track
        if (self.doc.metadata.currentlySelectedTrackNumber >= self.doc.song.musicTracks.count)
            self.doc.metadata.currentlySelectedTrackNumber = self.doc.song.harmony.trackNumber;
        
        self.doc.metadata.harmonyScore = nil;
        
        // auto size score
        [self autoSizeVertical];
        
        // clear magic pad
        [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
        
        // finish undoable changes
        [self.doc.hwUndoManager finishUndoableChangesName:@"Remove Track"];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Error", @"Error alert title") message:@"Cannot have less than three tracks" delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil];
        [alert show];
    }
}

- (void)virtualNoteOn:(NSNotification*)notif {
    int key = [[notif.userInfo objectForKey:@"key"] intValue];
    
    const Byte kMIDINoteOn = 0x90;
    const Byte channel = self.doc.metadata.currentlySelectedTrackNumber;
    const Byte note = key+12;
    const Byte velocity = 100;
    UInt8 bytes[3] = {kMIDINoteOn + channel, note, velocity};
    size_t size = sizeof(bytes);
    
    if (self.doc.midiDestination) {
        [self.doc.midiDestination sendBytes:bytes size:(UInt32)size];
    }
    
    Byte packetBuffer[size+100];
    MIDIPacketList *packetList = (MIDIPacketList*)packetBuffer;
    MIDIPacket     *packet     = MIDIPacketListInit(packetList);
    /*packet =*/ MIDIPacketListAdd(packetList, sizeof(packetBuffer), packet, 0, size, bytes);
    [self midiSource:nil midiReceived:packetList];
}

- (void)midiSourcesWillAppear:(NSNotification*)notif {
    id viewController = notif.object;
    [viewController setMidiSourceDelegate:self];
}

- (void)virtualNoteOff:(NSNotification*)notif {
    int key = [[notif.userInfo objectForKey:@"key"] intValue];
    
    const Byte kMIDINoteOff = 0x80;
    const Byte channel = self.doc.metadata.currentlySelectedTrackNumber;
    const Byte note = key+12;
    const Byte velocity = 0;
    UInt8 bytes[3] = {kMIDINoteOff + channel, note, velocity};
    size_t size = sizeof(bytes);
    
    if (self.doc.midiDestination) {
        [self.doc.midiDestination sendBytes:bytes size:(UInt32)size];
    }
    
    Byte packetBuffer[size+100];
    MIDIPacketList *packetList = (MIDIPacketList*)packetBuffer;
    MIDIPacket     *packet     = MIDIPacketListInit(packetList);
    /*packet =*/ MIDIPacketListAdd(packetList, sizeof(packetBuffer), packet, 0, size, bytes);
    [self midiSource:nil midiReceived:packetList];
}

- (void)placedEvent:(NSNotification*)notif {
    [self.doc.song spliceHarmony];
    self.doc.metadata.harmonyScore = nil;
    [self.doc.song clearHarmonyProperties];
    [self.doc.hwUndoManager finishUndoableChangesName:@"New Event"];
    
    [self updateMusicPlayerAfterEdit];
}

- (void)applyAudioSettingsBackground {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = LinguiLocalizedString(@"Loading", @"HUD title");
    hud.removeFromSuperViewOnHide = YES;
    [self.view.window addSubview:hud];
    __weak typeof(self) weakSelf = self;
    [hud showAnimated:YES whileExecutingBlock:^{
        @autoreleasepool {
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.doc applyAllAudioSettings];
        }
    }];
}

- (void)undoManagerDidUndo:(NSNotification *)notification {
    //    AudioController *audioController = [AudioController sharedAudioController];
    
    [self closeAllPopovers];
    self.doc.metadata.harmonyScore = nil;
    [self.rulerControl updatePlaybackIndicatorPosition];
    [self.allStavesControl updatePositionIndicator];
    if (self.doc.hwUndoManager.shouldRedisplay) {
        [self applyDefaultStaveHeight];
        self.doc.hwUndoManager.shouldRedisplay = NO;
    }
    
    const NSString *lastActionName = self.doc.hwUndoManager.lastActionName;
    
    if ([lastActionName isEqualToString:@"Create Track"] ||
        [lastActionName isEqualToString:@"Remove Track"]) {
        [self clearAllTrackAndStaveControls];
        [self createAllTrackAndStaveControls];
        [self applyAudioSettingsBackground];
        [self applyAllMIDISettings];
        [self.mainView setNeedsLayout];
    }
    else if ([lastActionName isEqualToString:@"Instrument"]) {
        [self applyAudioSettingsBackground];
        [self applyAllMIDISettings];
        [self.trackControlContentView.tracks makeObjectsPerformSelector:@selector(updateInstrument)];
    }
    else if ([lastActionName isEqualToString:@"Instruments preset"]) {
        [self applyAudioSettingsBackground];
        [self.trackControlContentView.tracks makeObjectsPerformSelector:@selector(updateInstrument)];
    }
    else if ([lastActionName isEqualToString:@"Reverb"]) {
        [[AudioController sharedAudioController] applyReverb:self.doc.metadata.masterReverb];
    }
    else if ([lastActionName isEqualToString:@"Add bars"] ||
        [lastActionName isEqualToString:@"Remove bars"]) {
        [self.rulerControl updateBarViews];
    }
    else if ([lastActionName isEqualToString:@"Gain"] ||
        [lastActionName isEqualToString:@"Pan"]) {
        [self redisplayAllTrackControls];
        [self applyGains];
        [self applyPans];
    }
    
    else if ([lastActionName isEqualToString:@"Mute"]) {
        [self.musicTrackControls makeObjectsPerformSelector:@selector(updateMute)];
        [self.musicTrackControls makeObjectsPerformSelector:@selector(setNeedsDisplay)];
    }
    
    else if ([lastActionName isEqualToString:@"Solo"]) {
        [self.musicTrackControls makeObjectsPerformSelector:@selector(updateSolo)];
        [self.musicTrackControls makeObjectsPerformSelector:@selector(setNeedsDisplay)];
    }
    
    else if ([lastActionName isEqualToString:@"Locked"]) {
        [self.musicTrackControls makeObjectsPerformSelector:@selector(updateLocked)];
        [self.musicTrackControls makeObjectsPerformSelector:@selector(setNeedsDisplay)];
    }

    else if ([lastActionName isEqualToString:@"Change Mode"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWModeChangedNotification object:nil];
    }
    else if ([lastActionName isEqualToString:@"Change Key"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWKeySignatureChangedNotification object:nil];
    }
    
    //    [self.settingsViewController.tempoViewController updateTempoValue];
    
    [self redrawStaves:NO];
}

- (void)undoManagerDidRedo:(NSNotification *)notification {
    // do the same thing as when undoing
    [self undoManagerDidUndo:notification];
}

- (void)undoManagerDidRevert:(NSNotification*)notification {
//    [self applyDefaultStaveHeight];
    [self redrawStaves:NO];
}

#pragma mark - actions

- (void)scrollOnePageRight:(id)sender {
    CGPoint pt = self.mainView.stavesView.contentOffset;
    pt.x += (HWScreenWidth - HWPanelWidth * 2.f);
    if (pt.x > self.allStavesControl.contentSize.width - HWScreenWidth) {
        pt.x = self.allStavesControl.contentSize.width - HWScreenWidth;
    }
    [self.mainView.stavesView setContentOffset:pt animated:YES];
}

- (void)scrollOnePageLeft:(id)sender {
    CGPoint pt = self.mainView.stavesView.contentOffset;
    pt.x -= (HWScreenWidth - HWPanelWidth * 2.f);
    if (pt.x < 0) {
        pt.x = 0.f;
    }
    [self.mainView.stavesView setContentOffset:pt animated:YES];
}

- (void)transposeOctaveUp:(id)sender {
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks) {
        for (HarmonyWizMusicEvent *ev in mt.events) {
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && ev.selected) {
                HarmonyWizNoteEvent *ne = (id)ev;
                ne.noteValue += 12;
            }
        }
    }
    [self.doc.hwUndoManager finishUndoableChangesName:@"Transpose octave up"];
}

- (void)transposeOctaveDown:(id)sender {
    [self.doc.hwUndoManager startUndoableChanges];
    for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks) {
        for (HarmonyWizMusicEvent *ev in mt.events) {
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]] && ev.selected) {
                HarmonyWizNoteEvent *ne = (id)ev;
                ne.noteValue -= 12;
            }
        }
    }
    [self.doc.hwUndoManager finishUndoableChangesName:@"Transpose octave down"];
}

- (IBAction)verticalScaleChanged:(UISlider*)sender {
    float value = [sender value];
    self.doc.metadata.notesPointSize = value;
    CGFloat newHeight = HarmonyWizStaveVerticalScale * value;
    [self applyStaveHeight:newHeight];
    [self redisplayAllTrackControls];
    [self.mainView setNeedsLayout];
    [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
    [self.allStavesControl updateVisible:YES];
}

- (IBAction)horizontalScaleChanged:(UISlider*)sender {
    float value = [sender value];
    self.doc.metadata.noteSpacing = value;
    [self.allStavesControl updateVisible:NO];
    [self applyDefaultStaveHeight];
    [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
    [self.allStavesControl updateVisible:YES];
}

//- (IBAction)closeTutorial:(id)sender {
//    
//}

- (IBAction)autoMelody:(UISlider*)sender {
    sender.value = roundf(sender.value);
    
    [self.doc.hwUndoManager startUndoableChanges];
    self.doc.metadata.autoMelody = sender.value;
    [self.doc.hwUndoManager finishUndoableChangesName:@"Auto Melody"];
}

- (IBAction)expertMode:(UISwitch*)sender {
    if (! sender.on) {
        [self.trackControlContentView.tracks.firstObject selectThisTrack];
    }
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:HWExpertMode];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self redrawStaves:NO];
    
    [HWAppDelegate recordEvent:@"expert_mode_toggle" segmentation:@{ @"enabled" : @(sender.on) }];
}

- (IBAction)autoFix:(UISwitch*)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:HWAutoFix];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [HWAppDelegate recordEvent:@"auto_fix_toggle" segmentation:@{ @"enabled" : @(sender.on) }];
}

- (IBAction)autoScroll:(UISwitch*)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:HWAutoScroll];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [HWAppDelegate recordEvent:@"auto_scroll_toggle" segmentation:@{ @"enabled" : @(sender.on) }];
}

- (IBAction)keyboardWizdomScrollEnabled:(UISwitch*)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:HWKeyboardScrolls];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [HWAppDelegate recordEvent:@"keyboard_scroll_toggled" segmentation:@{ @"enabled" : @(sender.on) }];
}

//- (IBAction)varispeedBrush:(UISwitch*)sender {
//    self.doc.metadata.varispeedBrush = sender.on;
//}

- (IBAction)recordFromMIDI:(UISwitch*)sender {
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:@"recordFromMIDI"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)keyboardWizdomIsAnimating {
    return (self.keyboardWizdom.layer.animationKeys != nil);
}

- (BOOL)toolboxIsAnimating {
    return (self.toolboxController.toolboxView.layer.animationKeys != nil);
}

- (BOOL)trackControlIsAnimating {
    return (self.mainView.tracksView.layer.animationKeys != nil);
}

- (void)magicWandMenu:(id)sender {
    [self closeAllPopovers];
    [self.wandPopover presentPopoverFromBarButtonItem:self.btnKeyboard permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [self registerPopover:self.wandPopover];
}

- (IBAction)togglePiano:(id)sender {
    if (! self.keyboardWizdomIsAnimating) {
        if (self.doc.metadata.pianoOpened) {
            [self.keyboardWizdom releaseAllNotes];
            [self concealKeyboardWizdom];
        }
        else {
            [self revealKeyboardWizdom];
        }
    }
}

- (void)removeAllMIDISources {
    for (PGMidiSource *source in [PGMidi sharedInstance].sources) {
        for (id del in source.delegates) {
            [source removeDelegate:del];
        }
    }
}

- (void)saveAndExit {
    
    NSLog(@"Closing %@...", self.doc.fileURL);
    
    {
        const BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        [HWAppDelegate recordEvent:@"closing_song" segmentation:
         @{
           @"expert_mode" : @(expertMode),
           @"parts_count" : @(self.doc.song.musicTracks.count),
           @"magic_mode" : @(self.doc.metadata.magicMode),
           @"loop_enabled" : @(self.doc.metadata.loopingEnabled),
           @"song_length" : @(self.doc.song.finalBarNumber)
           }];
    }
    
    [self removeAllMIDISources];
    
    [self killScroll];
    
    @try {
        [self.doc.metadata removeObserver:self forKeyPath:@"harmonyScore"];
        [self.btnArrange removeObserver:self forKeyPath:@"enabled"];
    }
    @catch (NSException *exception) {
        NSLog(@"exception.reason %@", exception.reason);
    }
    @finally {
    }
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:self.view.window];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Saving";
    hud.removeFromSuperViewOnHide = YES;
    [self.view.window addSubview:hud];
    [hud show:YES];
    
    // hide these for the score snapshot
    self.allStavesControl.positionIndicator.hidden = YES;
    self.allStavesControl.locatorTriangle.hidden = YES;
    
    // take snapshot on background thread
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            self.doc.metadata.thumbnail = [self scoreSnapshot];
            [[AudioController sharedAudioController] releaseAllPresets];
            
            // now save file
            [self.doc saveToURL:self.doc.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
                __strong typeof(self) strongSelf = weakSelf;
                [strongSelf.doc closeWithCompletionHandler:^(BOOL success) {
                    __strong typeof(self) strongSelf = weakSelf;
                    if (!success) {
                        NSLog(@"Failed to close %@", strongSelf.doc.fileURL);
                        // Continue anyway...
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        __strong typeof(self) strongSelf = weakSelf;
                        // hide the HUD
                        [hud hide:NO];
                        
                        // do the unwind segue
                        [strongSelf performSegueWithIdentifier:@"closeSong" sender:self];
                    });
                }];
            }];
        }
    });
}

- (IBAction)exitToMainView:(id)sender {
    [self saveAndExit];
}

- (void)revealKeyboardIfNoMIDIIn {
    PGMidi *pgMidi = [PGMidi sharedInstance];
    
    NSArray *enabledSources = [pgMidi.sources filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"delegates[SIZE] > 0"]];
    const BOOL netMidiEnabled = pgMidi.networkEnabled && [[enabledSources valueForKey:@"name"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF beginswith 'Network Session'"]].count > 0;
    
    if (enabledSources.count == (netMidiEnabled?1:0) && ! self.doc.metadata.pianoOpened) {
        [self revealKeyboardWizdom];
    }
}

- (IBAction)record:(id)sender {
    if (self.doc.metadata.magicMode) {
        return;
    }
    
    switch (self.state) {
        case kStateRecording:
            [self stopRewind:nil];
            break;
            
        case kStatePlaying:
        {
            HarmonyWizTrack *track = self.currentlySelectedTrack;
            if (track != self.doc.song.harmony) {
                [self revealKeyboardIfNoMIDIIn];
                
                self.state = kStateRecording;
                
                {
                    OSStatus result;
                    
                    result = MusicPlayerStop(self.musicPlayer);
                    if (result != noErr)
                        [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStop result is %d", (int)result];
                    
                    MusicTimeStamp timeStamp;
                    result = MusicPlayerGetTime(self.musicPlayer, &timeStamp);
                    if (result != noErr)
                        [NSException raise:@"HarmonyWizError" format:@"MusicPlayerGetTime result is %d", (int)result];
                    
                    [self updateMusicPlayer:self.doc.metadata.metronome precount:(self.doc.metadata.precount ? HWPrecountBars : 0)];
                    
                    if (self.doc.metadata.precount) {
                        timeStamp += (HWPrecountBars * self.doc.song.timeSign.upperNumeral * (4.f / self.doc.song.timeSign.lowerNumeral));
                    }
                    
                    result = MusicPlayerSetTime(self.musicPlayer, timeStamp);
                    if (result != noErr)
                        [NSException raise:@"HarmonyWizError" format:@"MusicPlayerSetTime result is %d", (int)result];
                    
                    result = MusicPlayerStart(self.musicPlayer);
                    if (result != noErr)
                        [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStart result is %d", (int)result];
                }
                
                [self.doc.hwUndoManager startUndoableChanges];
                
                // clear selected track
                self.doc.metadata.harmonyScore = nil;
                [self.doc.song clearHarmonyProperties];
                
                // initialise variables
                self.recordNoteOn = NO;
                self.liveRecording = [NSMutableArray array];
            }
        }
            break;
            
        case kStateStopped:
        {
            
            HarmonyWizTrack *track = self.currentlySelectedTrack;
            if (track == self.doc.song.harmony) {
                [[[UIAlertView alloc] initWithTitle:@"Record" message:@"Cannot record onto harmony track. Please select another track." delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
            }
            else {
                [self revealKeyboardIfNoMIDIIn];
                
                self.state = kStateRecording;
                [self.doc.hwUndoManager startUndoableChanges];
                
                // clear selected track
                self.doc.metadata.harmonyScore = nil;
                [self.doc.song clearHarmonyProperties];
                
                // initialise variables
                self.recordNoteOn = NO;
                self.liveRecording = [NSMutableArray array];
                
                // begin playing song
                [self startPlayback:self.doc.metadata.metronome precount:(self.doc.metadata.precount ? HWPrecountBars : 0)];
            }
        }
            break;
    }
}

- (IBAction)play:(id)sender {
    switch (self.state) {
        case kStatePlaying:
        case kStateRecording:
            [self stopRewind:nil];
            break;
            
        case kStateStopped:
        {
            const UInt16 crotchetSubdivs = self.doc.song.beatSubdivisions * self.doc.song.timeSign.lowerNumeral / 4;
            if ((self.doc.song.playbackPosition+0.1f)*crotchetSubdivs > self.doc.song.finalBarSubdivision) {
                self.doc.song.playbackPosition = 0.f;
            }
            self.state = kStatePlaying;
            [self startPlayback:NO precount:0];
        }
            break;
    }
}

- (void)updateStaveAfterRecording {
    const NSArray *modeAccidentalTypes = [self.doc.song.songMode modeAccidentalTypes];
    if ([self.currentlySelectedTrack isKindOfClass:[HarmonyWizMusicTrack class]]) {
        HarmonyWizMusicTrack *track = (id)self.currentlySelectedTrack;
        
        [self.doc.song autoColour:self.liveRecording withCadence:YES];
        for (id ev in self.liveRecording) {
            [track.events removeObjectsAtIndexes:[self.doc.song overlappingEvents:track.events forEvent:ev]];
            HarmonyWizNoteEvent *ne = ev;
            if (self.doc.song.isModal) {
                const int shift = [modeAccidentalTypes[imod(ne.noteValue,12)] intValue];
                [ne applyNoteShift:shift];
            }
        }
        [track.events placeObjectsFromArray:self.liveRecording];
        [self.doc.song addFillerRestsTo:track.events];
        
        // handle non-expert mode requirements
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        if (! expertMode) {
            [self.doc.song removeOtherEventsOverlappingWith:self.liveRecording];
        }
        
        self.doc.metadata.harmonyScore = nil;
        [self.doc.song spliceHarmony];
    }
}

- (void)rewind {
    self.doc.song.playbackPosition = 0;
    [self.rulerControl redisplay];
    [self.allStavesControl updatePositionIndicator];
    [self.mainView.stavesView setContentOffset:CGPointZero animated:NO];
}

- (IBAction)stopRewind:(id)sender {
    switch (self.state) {
        case kStatePlaying:
            [self stopPlayback];
            break;
        case kStateRecording:
            [self stopPlayback];
            [self updateStaveAfterRecording];
            [self.doc.hwUndoManager finishUndoableChangesName:@"Record"];
            [self redrawStaves:YES];
            break;
        case kStateStopped:
            [self rewind];
            break;
    }
    
    self.state = kStateStopped;
}

- (void)killScroll {
    if (self.mainView.stavesView.decelerating) {
        CGPoint offset = self.mainView.stavesView.contentOffset;
        [self.mainView.stavesView setContentOffset:offset animated:NO];
    }
    if (self.mainView.rulerView.decelerating) {
        CGPoint offset = self.mainView.rulerView.contentOffset;
        [self.mainView.rulerView setContentOffset:offset animated:NO];
    }
    if (self.mainView.tracksView.decelerating) {
        CGPoint offset = self.mainView.tracksView.contentOffset;
        [self.mainView.tracksView setContentOffset:offset animated:NO];
    }
}

- (void)beginArrange {
    self.arrangeStartDate = [NSDate date];
    if (self.state != kStateStopped) {
        [self stopRewind:nil];
    }
    [self killScroll];
    self.playbackPositionBeforeArrange = self.doc.song.playbackPosition;
    [self.fixesApplied removeAllObjects];
    [self continueArrange];
}

- (void)continueArrange {
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    if ((self.preArrangeCheckResult = [self preArrangeCheck]) != nil) {
        NSString *msg = [self.preArrangeCheckResult objectForKey:@"message"];
        if ([self.preArrangeCheckResult objectForKey:@"fix"]) {
            RIButtonItem *fixAction = [RIButtonItem itemWithLabel:@"Fix" action:^{
                void (^fixBlock)(void) = [self.preArrangeCheckResult objectForKey:@"fix"];
                [self.doc.hwUndoManager startUndoableChanges];
                fixBlock();
                [self redrawStaves:YES];
                [self.doc.hwUndoManager finishUndoableChangesName:@"Fix applied"];
            }];
            self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Pre-arrange check", @"Alert title") message:msg cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:fixAction, nil];
            [self.currentAlert show];
        }
        else if ([self.preArrangeCheckResult objectForKey:@"replacement"] != nil) {
            if (expertMode) {
                __weak typeof(self) weakSelf = self;
                RIButtonItem *once = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Fix once", @"Button title") action:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    HarmonyWizNoteEvent *ev = [strongSelf.preArrangeCheckResult objectForKey:@"offending"];
                    HarmonyWizNoteEvent *replacement = [strongSelf.preArrangeCheckResult objectForKey:@"replacement"];
                    [strongSelf.doc.hwUndoManager startUndoableChanges];
                    [strongSelf.doc.song replaceMusicEvent:ev with:replacement];
                    BOOL shouldSplice = [[strongSelf.preArrangeCheckResult objectForKey:@"splice"] boolValue];
                    if (shouldSplice) {
                        [strongSelf.doc.song spliceHarmony];
                    }
                    [strongSelf.doc.hwUndoManager finishUndoableChangesName:LinguiLocalizedString(@"Fix once", @"Button title")];
                    [strongSelf redrawStaves:NO];
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        __strong typeof(self) strongSelf = weakSelf;
                        [strongSelf continueArrange];
                    }];
                }];
                
                RIButtonItem *all = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Fix all", @"Button title") action:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    [strongSelf fixAll];
                }];
                
                self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Pre-arrange check", @"Alert title") message:msg cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:once, all, nil];
                
                [self.currentAlert show];
                
                HarmonyWizNoteEvent *ev = [self.preArrangeCheckResult objectForKey:@"offending"];
                [self.doc.song placePlaybackIndicatorAtEvent:ev];
                [self scrollToPlaybackPosition];
                [self.rulerControl redisplay];
                [self.allStavesControl updatePositionIndicator];
            }
            else {
                [self fixAll];
            }
        }
        else {
            [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Pre-arrange check", @"Alert title") message:msg delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
        }
    }
    else {
        [[AudioController sharedAudioController] stopProcessingGraph];
        [(HWAppDelegate*)[[UIApplication sharedApplication] delegate] pushIdleTimerDisabled];
        if ([self.doc.hwUndoManager canStartUndoableChanges]) {
            [self.doc.hwUndoManager startUndoableChanges];
        }
        
        [UIApplication sharedApplication].applicationSupportsShakeToEdit = NO;
        
        NSLog(@"continuing arrange with score %@", self.doc.metadata.harmonyScore);
        [self startAnimateArrangeButton];
        
        self.arrangeHUD = [[MBProgressHUD alloc] initWithWindow:self.view.window];
        self.arrangeHUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
        self.arrangeHUD.labelText = LinguiLocalizedString(@"Arranging your masterpiece", @"HUD title");
        self.arrangeHUD.cancelButtonTitle = LinguiLocalizedString(@"Stop", @"Stop button");
        self.arrangeHUD.showCancelButton = YES;
        self.arrangeHUD.delegate = self;
        [self.view.window addSubview:self.arrangeHUD];
        self.arrangeHUD.removeFromSuperViewOnHide = YES;
        [self.arrangeHUD show:YES];
        
        [self updateArrangeMessage:nil];
        self.arrangeMessageTimer = [NSTimer scheduledTimerWithTimeInterval:HWArrangeMessageUpdateTime target:self selector:@selector(updateArrangeMessage:) userInfo:nil repeats:YES];
        
        // start the arrange process asynchronously
        self.doc.disableAutosave = YES;
        self.allStavesControl.userInteractionEnabled = NO;
        dispatch_async(_arrangeQueue, ^{
            @autoreleasepool {
                self.arrangeSucceeded = NO;
                if (self.doc.song.isModal)
                    self.arrangeSucceeded = [self arrangeModalMusic];
                else
                    self.arrangeSucceeded = [self arrangeMusic];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self stopAnimateArrangeButton];
                    [self completeArrange];
                    [self.arrangeHUD hide:YES];
                    self.arrangeHUD = nil;
                    [self.arrangeMessageTimer invalidate];
                    self.arrangeMessageTimer = nil;
                    self.doc.disableAutosave = NO;
                });
            }
        });
    }
}

- (void)hudWasCancelled:(MBProgressHUD *)hud {
    if (hud == self.arrangeHUD) {
        if (self.doc.song.isModal)
            [self cancelModalArrangement];
        else
            [self cancelArrangement];
    }
}

- (IBAction)arrange:(id)sender {
    [self closeAllPopovers];
    
    if (self.doc.metadata.magicMode) {
        [self exitEditMode:nil completion:^{
            [self beginArrange];
        }];
    }
    else {
        [self beginArrange];
    }
}

- (IBAction)undo:(id)sender {
    if (self.doc.hwUndoManager.canUndo)
        [self.doc.hwUndoManager undo];
}

- (IBAction)redo:(id)sender {
    if (self.doc.hwUndoManager.canRedo)
        [self.doc.hwUndoManager redo];
}

- (void)changeFocusToSingleTrack:(id)sender {
    if (self.currentlySelectedTrack != self.doc.song.harmony) {
        if (! self.doc.metadata.magicMode) {
            [self editTrack:self.doc.metadata.currentlySelectedTrackNumber];
        }
    }
}

- (void)changeFocusToMultiTrack:(id)sender {
    if (self.currentlySelectedTrack != self.doc.song.harmony) {
        if (self.doc.metadata.magicMode) {
            [self exitEditMode:nil completion:nil];
        }
    }
}

#pragma mark - song

// check for problems before arrange
- (NSDictionary*)preArrangeCheck {
    HarmonyWizSong *song = self.doc.song;
    
    // check for empty song
    if (song.harmony.isEmpty) {
        return @{@"message" : @"Song is empty"};
    }
    
    if (song.isModal) {
        if (![song validateForMode]) {
            return @{@"message" : @"Some harmony notes in your song do not belong to the currently selected mode. Please correct this before arranging.", @"fix" : ^{[self.doc.song roundNotesToMode];}};
        }
    }

    // check for unprepared passing notes
    {
        for (HarmonyWizMusicTrack *track in song.musicTracks) {
            HarmonyWizEvent *lastEvent = nil;
            for (HarmonyWizEvent *event in track.events) {
                if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *note = (HarmonyWizNoteEvent*)event;
                    if (note.type == kNoteEventType_Passing) {
                        if (([lastEvent isKindOfClass:[HarmonyWizNoteEvent class]] &&
                             !(((HarmonyWizNoteEvent*)lastEvent).type == kNoteEventType_Harmony ||
                               ((HarmonyWizNoteEvent*)lastEvent).type == kNoteEventType_Passing)) ||
                            ![lastEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            HarmonyWizNoteEvent *replacement = [note copy];
                            replacement.type = kNoteEventType_Harmony;
                            return @{@"message" : @"Unprepared passing note", @"offending" : note, @"replacement" : replacement, @"splice" : @(YES)};
                        }
                    }
                }
                lastEvent = event;
            }
        }
    }
    
    // check for unresolved suspension notes
    {
        for (HarmonyWizMusicTrack *track in song.musicTracks) {
            HarmonyWizEvent *lastEvent = nil;
            for (HarmonyWizEvent *event in track.events) {
                if ([lastEvent isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *note = (HarmonyWizNoteEvent*)lastEvent;
                    if (note.type == kNoteEventType_Suspension) {
                        if (([event isKindOfClass:[HarmonyWizNoteEvent class]] &&
                             !(((HarmonyWizNoteEvent*)event).type == kNoteEventType_Harmony ||
                               ((HarmonyWizNoteEvent*)event).type == kNoteEventType_Suspension)) ||
                            ![event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            HarmonyWizNoteEvent *replacement = [note copy];
                            replacement.type = kNoteEventType_Harmony;
                            return @{@"message" : @"Unresolved suspension note", @"offending" : note, @"replacement" : replacement, @"splice" : @(YES)};
                        }
                    }
                }
                lastEvent = event;
            }
            
            if ([track.events.lastObject isKindOfClass:[HarmonyWizNoteEvent class]]) {
                HarmonyWizNoteEvent *note = (HarmonyWizNoteEvent*)track.events.lastObject;
                if (note.type == kNoteEventType_Suspension) {
                    HarmonyWizNoteEvent *replacement = [note copy];
                    replacement.type = kNoteEventType_Harmony;
                    return @{@"message" : @"Suspension note at end of track", @"offending" : note, @"replacement" : replacement, @"splice" : @(YES)};
                }
            }
        }
    }
    
    // check for out of range
    {
        for (HarmonyWizMusicTrack *track in song.musicTracks) {
            for (HarmonyWizEvent *event in track.events) {
                if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *note = (HarmonyWizNoteEvent*)event;
                    if (note.type == kNoteEventType_Harmony && (note.noteValue < track.lowestNoteAllowed || note.noteValue > track.highestNoteAllowed)) {
                        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                        [song setEventPosition:re to:[song eventStartSubdivision:note]];
                        [song setEventDuration:re to:[song eventDuration:note]];
                        return @{ @"message" : @"Note is outside permitted range", @"offending" : note, @"replacement" : re, @"splice" : @(NO)};
                    }
                }
            }
        }
    }
    
    // check for overlapping parts
    {
        for (NSUInteger idx = 0; idx < song.musicTracks.count-1; idx++) {
            for (NSUInteger jdx = idx+1; jdx < song.musicTracks.count; jdx++) {
                NSArray *harmonyEvents = [song.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord == NO"]];
                for (NSUInteger kdx = 0; kdx < harmonyEvents.count; kdx++) {
                    HarmonyWizHarmonyEvent *he0 = [harmonyEvents objectAtIndex:kdx];
                    if (he0.pause)
                        continue;
                    HarmonyWizHarmonyEvent *he1 = nil;
                    if (kdx+1 < harmonyEvents.count)
                        he1 = [harmonyEvents objectAtIndex:kdx+1];
                    
                    HarmonyWizTrack *tr0 = [song.musicTracks objectAtIndex:idx];
                    HarmonyWizTrack *tr1 = [song.musicTracks objectAtIndex:jdx];
                    NSArray *notes0 = [tr0.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"(class == %@) AND (type == %d)", [HarmonyWizNoteEvent class], kNoteEventType_Harmony]];
                    NSArray *notes1 = [tr1.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"(class == %@) AND (type == %d)", [HarmonyWizNoteEvent class], kNoteEventType_Harmony]];
                    
                    NSMutableIndexSet *indices0 = [NSMutableIndexSet indexSet];
                    [indices0 addIndexes:[song overlappingEvents:notes0 forEvent:he0]];
                    if (he1)
                        [indices0 addIndexes:[song overlappingEvents:notes0 forEvent:he1]];
                    NSMutableIndexSet *indices1 = [NSMutableIndexSet indexSet];
                    [indices1 addIndexes:[song overlappingEvents:notes1 forEvent:he0]];
                    if (he1)
                        [indices1 addIndexes:[song overlappingEvents:notes1 forEvent:he1]];
                    
                    // check if any note in indices1 is a higher pitch than any note in indices0
                    NSArray *evs0 = [notes0 objectsAtIndexes:indices0];
                    NSArray *evs1 = [notes1 objectsAtIndexes:indices1];
                    
                    for (HarmonyWizMusicEvent *ev0 in evs0) {
                        if ([ev0 isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            for (HarmonyWizMusicEvent *ev1 in evs1) {
                                if ([ev1 isKindOfClass:[HarmonyWizNoteEvent class]]) {
                                    HarmonyWizNoteEvent *ne0 = (HarmonyWizNoteEvent*)ev0;
                                    HarmonyWizNoteEvent *ne1 = (HarmonyWizNoteEvent*)ev1;
                                    if (ne0.type == kNoteEventType_Harmony && ne1.type == kNoteEventType_Harmony && ne1.noteValue > ne0.noteValue) {
                                        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                                        [song setEventPosition:re to:[song eventStartSubdivision:ne1]];
                                        [song setEventDuration:re to:[song eventDuration:ne1]];
                                        return @{@"message" : @"Overlapping parts", @"offending" : ne1, @"replacement" : re, @"splice" : @(NO)};
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    // greater than octave leap
    {
        for (HarmonyWizMusicTrack *mt in song.musicTracks) {
            NSArray *notes = [mt.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"(class == %@) AND (type == %d)", [HarmonyWizNoteEvent class], kNoteEventType_Harmony]];
            for (NSUInteger idx = 1; idx < notes.count; idx++) {
                HarmonyWizNoteEvent *ev0 = [notes objectAtIndex:idx-1];
                if ([[self.doc.song findLastHarmonyEventForEvent:ev0] pause])
                    continue;
                HarmonyWizNoteEvent *ev1 = [notes objectAtIndex:idx];
                if (ev0.type == kNoteEventType_Harmony && ev1.type == kNoteEventType_Harmony && ABS(ev0.noteValue - ev1.noteValue) > 12) {
                    HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                    [song setEventPosition:re to:[song eventStartSubdivision:ev1]];
                    [song setEventDuration:re to:[song eventDuration:ev1]];
                    return @{@"message" : @"Greater than octave leap", @"offending" : ev1, @"replacement" : re, @"splice" : @(NO)};
                }
            }
        }
    }
    
    return nil;
}

- (BOOL)arrangeFailed:(NSString*)actionName {
    BOOL result = YES;
    const BOOL autoFixEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:HWAutoFix];
    const BOOL autoFixing = ![actionName hasSuffix:@"auto"] || autoFixEnabled;
    
    if ([actionName isEqualToString:@"Styles"]) {
        [self closeAllPopovers];
        [self.stylesPopover presentPopoverFromRect:self.harmonyTrackControl.instrumentLabel.frame inView:self.harmonyTrackControl permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        [self registerPopover:self.stylesPopover];
    }
    else if ([actionName hasPrefix:@"Fix"]) {
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        if (expertMode)
            self.doc.metadata.harmonyScore = nil;
        HarmonyWizSong *song = self.doc.song;
        NSArray *chordEvents = song.chordEvents;
        const UInt8 maxFixColumns = MIN(self.arrangeGuardCoverage, HWMaxFixColumns);
        HarmonyWizHarmonyEvent *first_he = [chordEvents objectAtIndex:((maxFixColumns<=1+self.arrangeGuardColumn)?self.arrangeGuardColumn - maxFixColumns+1:0)];
        HarmonyWizHarmonyEvent *last_he = [chordEvents objectAtIndex:self.arrangeGuardColumn];
        const UInt16 startFix = [song eventStartSubdivision:first_he];
        const UInt16 endFix = [song eventEndSubdivision:last_he];
        
        if ([self.harmonyFailureReason isEqualToString:LinguiLocalizedString(@"minimum score not reached", @"Minimum score not reached failure reason")]) {
            // in this instance no auto-fix should be applied
            // instead just reset the score so the user can
            // keep going.
            self.doc.metadata.harmonyScore = nil;
        }
        else {
            BOOL fixed = NO;
            
            // apply fixes
            for (UInt8 idx = self.arrangeGuardColumn+1-maxFixColumns; idx <= self.arrangeGuardColumn; idx++) {
                HarmonyWizHarmonyEvent *he = [chordEvents objectAtIndex:idx];
                if (autoFixing && [self relaxRules:he]) {
                    fixed = YES;
                }
                else if (autoFixing && [self smoothPassingNotes:he]) {
                    fixed = YES;
                    [self.doc.song spliceHarmony];
                    break;
                }
                else if (autoFixing && [self removeLowerHarmony:he]) {
                    fixed = YES;
                    [self.doc.song spliceHarmony];
                    break;
                }
                else if (autoFixing && [self smoothHarmonyNotes:he]) {
                    fixed = YES;
                    break;
                }
            }
            if (! fixed) {
                if (autoFixing) {
                    const UInt16 idx = self.arrangeGuardColumn+1-maxFixColumns;
                    HarmonyWizHarmonyEvent *he = [chordEvents objectAtIndex:idx];
                    [self makeBlackNotes:he start:startFix end:endFix];
                }
                else {
                    result = NO;
                }
            }
        }
        
        if ([actionName isEqualToString:@"Fix and continue"]) {
            [song selectNone];
            for (HarmonyWizHarmonyEvent *he in song.harmony.events) {
                if ([he compare:first_he] != NSOrderedAscending && he.noChord == NO) {
                    he.selected = YES;
                }
            }
        }
    }
    
    NSLog(@"dong %d", result);
    return result;
}

- (BOOL)smoothPassingNotes:(HarmonyWizHarmonyEvent*)he {
    for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks) {
        NSIndexSet *overlaps = [self.doc.song overlappingEvents:mt.events forEvent:he];
        if (overlaps.count > 0) {
            NSMutableArray *passing = [NSMutableArray array];
            NSMutableArray *suspension = [NSMutableArray array];
            for (NSUInteger idx = overlaps.firstIndex; idx != NSNotFound; idx = [overlaps indexGreaterThanIndex:idx]) {
                id ev = [mt.events objectAtIndex:idx];
                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *ne = ev;
                    if (ne.type == kNoteEventType_Passing) {
                        [passing addObject:ne];
                    }
                    else if (ne.type == kNoteEventType_Suspension) {
                        [suspension addObject:ne];
                    }
                }
            }
            if (suspension.count > 0) {
                HarmonyWizNoteEvent *middleSuspension = [suspension objectAtIndex:suspension.count/2];
                middleSuspension.type = kNoteEventType_Harmony;
                return YES;
            }
            else if (passing.count > 0) {
                HarmonyWizNoteEvent *middlePassing = [passing objectAtIndex:passing.count/2];
                middlePassing.type = kNoteEventType_Harmony;
                return YES;
            }
        }
    }
    
    return NO;
}

- (BOOL)relaxRules:(HarmonyWizHarmonyEvent*)he {
    NSNumber *relax = [he.constraints objectForKey:HarmonyWizRelaxRulesProperty];
    if (relax && relax.boolValue == YES) {
        return NO;
    }
    else {
        [he.constraints removeAllObjects];
        [he.constraints setObject:@(YES) forKey:HarmonyWizRelaxRulesProperty];
        return YES;
    }
}

- (BOOL)removeLowerHarmony:(HarmonyWizHarmonyEvent*)he {
    if (he == nil) {
        return NO;
    }
    
    NSUInteger partCount = 0;
    
    for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks.reverseObjectEnumerator) {
        NSIndexSet *overlaps = [self.doc.song overlappingEvents:mt.events forEvent:he];
        if (overlaps.count > 0) {
            NSUInteger currentIndex = overlaps.firstIndex;
            while (currentIndex != NSNotFound) {
                id ev = [mt.events objectAtIndex:currentIndex];
                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *ne = ev;
                    if (ne.type != kNoteEventType_Auto) {
                        partCount ++;
                        break;
                    }
                }
                currentIndex = [overlaps indexGreaterThanIndex:currentIndex];
            }
        }
    }
    
    if (partCount > 1) {
        const UInt16 startFix = [self.doc.song eventStartSubdivision:he];
        const UInt16 endFix = [self.doc.song eventEndSubdivision:he];
        for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks.reverseObjectEnumerator.allObjects) {
            NSIndexSet *overlaps = [self.doc.song overlappingEvents:mt.events forEvent:he];
            if (overlaps.count > 0) {
                NSUInteger currentIndex = overlaps.firstIndex;
                while (currentIndex != NSNotFound) {
                    id ev = [mt.events objectAtIndex:currentIndex];
                    if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                        HarmonyWizNoteEvent *ne = ev;
                        if (ne.type != kNoteEventType_Auto) {
                            const UInt16 start = [self.doc.song eventStartSubdivision:ne];
                            const UInt16 end = [self.doc.song eventEndSubdivision:ne];
                            if (start < startFix) {
                                UInt16 duration = startFix - start;
                                ne.durationBeats = duration / self.doc.song.beatSubdivisions;
                                ne.durationSubdivisions = duration % self.doc.song.beatSubdivisions;
                            }
                            else if (end > endFix) {
                                UInt16 duration = end - endFix;
                                ne.beat = endFix / self.doc.song.beatSubdivisions;
                                ne.subdivision = endFix % self.doc.song.beatSubdivisions;
                                ne.durationBeats = duration / self.doc.song.beatSubdivisions;
                                ne.durationSubdivisions = duration % self.doc.song.beatSubdivisions;
                            }
                            else {
                                ne.type = kNoteEventType_Auto;
                            }
                            return YES;
                        }
                    }
                    currentIndex = [overlaps indexGreaterThanIndex:currentIndex];
                }
            }
        }
    }
    return NO;
}

- (BOOL)smoothHarmonyNotes:(HarmonyWizHarmonyEvent*)he {
    if (he == nil) {
        return NO;
    }
    
    // min duration is sixteenth note
    const UInt16 kMinDuration = (self.doc.song.beatSubdivisions / 2) /** (self.doc.song.timeSign.lowerNumeral / 4)*/;
    
    BOOL didMakeChanges = NO;
    {
        UInt16 dur = [self.doc.song eventDuration:he];
        if (! he.noChord) {
            // smooth second note event
            if (dur >= 2*kMinDuration) {
                UInt16 newDur = dur;
                newDur /= 2;
                newDur /= kMinDuration;
                newDur *= kMinDuration;
                HarmonyWizHarmonyEvent *he_smooth = [he copy];
                [he.constraints removeAllObjects];
                [he.temporaryConstraints removeAllObjects];
                [self.doc.song setEventDuration:he_smooth to:newDur];
                [self.doc.song setEventDuration:he to:(dur-newDur)];
                [self.doc.song setEventPosition:he to:[self.doc.song eventEndSubdivision:he_smooth]];
                [self.doc.song.harmony.events placeObject:he_smooth];
                didMakeChanges = YES;
            }
        }
        [self.doc.song checkAllEventsSorted];
    }
    return didMakeChanges;
}

- (void)makeBlackNotes:(HarmonyWizHarmonyEvent*)he start:(UInt16)startFix end:(UInt16)endFix {
    for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks) {
        NSIndexSet *overlaps = [self.doc.song overlappingEvents:mt.events forEvent:he];
        if (overlaps.count > 0) {
            NSUInteger currentIndex = overlaps.firstIndex;
            while (currentIndex != NSNotFound) {
                id ev = [mt.events objectAtIndex:currentIndex];
                if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                    HarmonyWizNoteEvent *ne = ev;
                    const UInt16 start = [self.doc.song eventStartSubdivision:ne];
                    const UInt16 end = [self.doc.song eventEndSubdivision:ne];
                    if (start < startFix) {
                        UInt16 duration = startFix - start;
                        ne.durationBeats = duration / self.doc.song.beatSubdivisions;
                        ne.durationSubdivisions = duration % self.doc.song.beatSubdivisions;
                    }
                    else if (end > endFix) {
                        UInt16 duration = end - endFix;
                        ne.beat = endFix / self.doc.song.beatSubdivisions;
                        ne.subdivision = endFix % self.doc.song.beatSubdivisions;
                        ne.durationBeats = duration / self.doc.song.beatSubdivisions;
                        ne.durationSubdivisions = duration % self.doc.song.beatSubdivisions;
                    }
                    else {
                        ne.type = kNoteEventType_Auto;
                    }
                }
                currentIndex = [overlaps indexGreaterThanIndex:currentIndex];
            }
            [self.doc.song addFillerRestsTo:mt.events];
        }
    }
}

- (NSString*)stringFromStyle:(NSUInteger)style {
    switch (style) {
        case 0:
            return LinguiLocalizedString(@"Baroque", @"style name");
            break;
        case 1:
            return LinguiLocalizedString(@"Contemporary", @"style name");
            break;
        case 2:
            return LinguiLocalizedString(@"Smooth", @"style name");
            break;
        case 3:
            return LinguiLocalizedString(@"Free", @"style name");
            break;
        case 4:
            return LinguiLocalizedString(@"Open", @"style name");
            break;
            
        default:
            break;
    }
    return nil;
}

- (void)completeArrange {
    
    const NSTimeInterval interval = (-[self.arrangeStartDate timeIntervalSinceNow]);
    
    [(HWAppDelegate*)[[UIApplication sharedApplication] delegate] popIdleTimerDisabled];
    
    [[AudioController sharedAudioController] startProcessingGraph];
    
    if (self.arrangeSucceeded) {
        [HWAppDelegate recordEvent:@"arrange_succeeded_after_duration" segmentation:@{ @"style" : [self stringFromStyle:self.doc.metadata.harmonyStyle], @"parts" : @(self.doc.song.musicTracks.count) } sum:interval];
        
        [self.doc.song selectNone];
        self.doc.song.playbackPosition = self.playbackPositionBeforeArrange;
        [self.doc.hwUndoManager finishUndoableChangesName:@"Arrange"];
        
        [self.rulerControl updatePlaybackIndicatorPosition];
        [self.allStavesControl updatePositionIndicator];
        
        if (self.fixesApplied.count > 0) {
            __weak typeof(self) weakSelf = self;
            RIButtonItem *info = [RIButtonItem itemWithLabel:@"Info" action:^{
                __strong typeof(self) strongSelf = weakSelf;
                [strongSelf reportFixesApplied];
            }];
            
            self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Some important info", @"Alert title") message:LinguiLocalizedString(@"We made a few changes to your composition to fit the selected style.\nHope you like it!", @"Alert message") cancelButtonItem:nil otherButtonItems:info, [RIButtonItem itemWithLabel:LinguiLocalizedString(@"OK", @"OK button")], nil];
            
            [self.currentAlert show];
        }
        
//        if (self.arrangeCompleteHook != nil) {
//            self.arrangeCompleteHook();
//            self.arrangeCompleteHook = nil;
//        }
    }
    else if (self.doc.song.isModal) {
        [self.doc.hwUndoManager finishUndoableChangesName:@"Arrange Partially"];
        
        NSString *message = [NSString stringWithFormat:@"Could not resolve \"%@\"", self.harmonyFailureReason];
        self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(HWArrangeFailedMessage, @"failed message") message:message cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"OK", @"OK button")] otherButtonItems:nil];
        [self.currentAlert show];
    }
    else {
        [self.doc.hwUndoManager finishUndoableChangesName:@"Arrange Partially"];
        
        __weak typeof(self) weakSelf = self;
        RIButtonItem *styles = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Styles", @"button text") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf arrangeFailed:@"Styles"];
            [strongSelf redrawStaves:NO];
            
            [HWAppDelegate recordEvent:@"arrange_then_style_change"];
        }];
        RIButtonItem *fixContinue = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Fix and continue", @"button text") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.doc.hwUndoManager startUndoableChanges];
            [strongSelf arrangeFailed:@"Fix and continue"];
            [strongSelf.doc.hwUndoManager finishUndoableChangesName:@"Fix and continue"];
            [strongSelf continueArrange];
            
            [HWAppDelegate recordEvent:@"fix_and_continue"];
        }];
        RIButtonItem *fixRestart = [RIButtonItem itemWithLabel:LinguiLocalizedString(@"Fix and restart", @"button text") action:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.doc.hwUndoManager startUndoableChanges];
            [strongSelf arrangeFailed:@"Fix and restart"];
            [strongSelf.doc.hwUndoManager finishUndoableChangesName:@"Fix and restart"];
            [strongSelf continueArrange];
            
            [HWAppDelegate recordEvent:@"fix_and_restart"];
        }];
        
        if (self.didCancelArrange) {
            [HWAppDelegate recordEvent:@"arrange_cancelled_after_duration" segmentation:@{ @"style" : [self stringFromStyle:self.doc.metadata.harmonyStyle], @"parts" : @(self.doc.song.musicTracks.count) } sum:interval];
            
            BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
            if (expertMode) {
                NSString *message = [NSString stringWithFormat:LinguiLocalizedString(@"Cancelled while resolving \"%@\"", @"Alert message"), self.harmonyFailureReason];
                if (self.arrangeGuardCoverage > 0) {
                    
                    self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(HWArrangeCancelledMessage, @"cancelled message") message:message cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:fixContinue, fixRestart, styles, nil];
                    }
                else {
                    self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(HWArrangeCancelledMessage, @"cancelled message") message:message cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:styles, nil];
                }
                [self.currentAlert show];
            }
            else {
                [self.doc.hwUndoManager undo];
                [self scrollToPlaybackPosition];
            }
        }
        else {
            [HWAppDelegate recordEvent:@"arrange_failed_after_duration" segmentation:@{ @"style" : [self stringFromStyle:self.doc.metadata.harmonyStyle], @"parts" : @(self.doc.song.musicTracks.count) } sum:interval];
            
            if (self.arrangeGuardCoverage > 0) {
                BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
                if (expertMode) {
                    NSString *message = [NSString stringWithFormat:@"Could not resolve \"%@\"", self.harmonyFailureReason];
                    self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(HWArrangeFailedMessage, @"failed message")  message:message cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:fixContinue, fixRestart, styles, nil];
                    [self.currentAlert show];
                }
//                else {
//                    [self.fixesApplied addObject:@{ @"reason" : self.harmonyFailureReason, @"position" : @(self.doc.song.playbackPosition) }];
//                    [self arrangeFailed:@"Fix and continue"];
//                    [self continueArrange];
//                }
            }
            else {
                NSString *message = [NSString stringWithFormat:@"Could not resolve \"%@\"", self.harmonyFailureReason];
                self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(HWArrangeFailedMessage, @"failed message") message:message cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"Cancel", @"Cancel button")] otherButtonItems:styles, nil];
                [self.currentAlert show];
            }
        }
    }
    
    [UIApplication sharedApplication].applicationSupportsShakeToEdit = YES;
    self.allStavesControl.userInteractionEnabled = YES;
}


#pragma mark - user interface

- (void)stepRecordToggled:(NSNotification*)notif {
    [self.toolboxController updateEnabled];
    if (self.doc.metadata.stepRecord) {
        [self.toolboxController.toolboxView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
    
    [HWAppDelegate recordEvent:@"step_record_toggled" segmentation:@{ @"enabled" : @(self.doc.metadata.stepRecord) }];
}

- (void)enteringBackground:(NSNotification*)notif {
    AudioController *ac = [AudioController sharedAudioController];
    if ( ![HWAppDelegate backgroundAudioEnabled] ) {
        if (self.state != kStateStopped) {
            [self stopRewind:nil];
        }
        [ac stopProcessingGraph];
    }
}

- (void)enteringForeground:(NSNotification*)notif {
    // In case audio graph was stopped when moving to background
    // restart it here.
    [[AudioController sharedAudioController] startProcessingGraph];
}

- (void)startedStaveEvent:(NSNotification*)notif {
    if (self.allStavesControl.isEditing) {
        [self killScroll];
    }
    [self updateToolbarAndRuler];
}

- (void)endedStaveEvent:(NSNotification*)notif {
    [self updateToolbarAndRuler];
}

- (void)updateCanUndo:(NSNotification*)notif {
    [self updateToolbarAndRuler];
}

- (void)draggedLocatorTriangle:(NSNotification*)notif {
    CGPoint loc = [[notif.userInfo objectForKey:@"loc"] CGPointValue];
    self.doc.song.playbackPosition = [self.rulerControl playbackPositionFromLocation:loc.x];
    [self.rulerControl updatePlaybackIndicatorPosition];
    [self.allStavesControl updatePositionIndicator];
}

- (void)selectTrackNumber:(NSNotification*)notif {
    NSUInteger num = [[notif.userInfo objectForKey:@"trackNumber"] unsignedIntegerValue];
    [[self.trackControlContentView.tracks objectAtIndex:num] selectThisTrack];
}

- (void)beginDraggingTrackControl:(NSNotification*)notif {
    for (UIGestureRecognizer *gest in self.mainView.tracksView.gestureRecognizers) {
        gest.enabled = NO;
    }
}

- (void)endDraggingTrackControl:(NSNotification*)notif {
    for (UIGestureRecognizer *gest in self.mainView.tracksView.gestureRecognizers) {
        gest.enabled = YES;
    }
}

- (void)autoSizeVertical {
    CGFloat stavesSpace = CGRectGetHeight(self.mainView.stavesView.frame);
    if (self.doc.metadata.pianoOpened) {
        stavesSpace += CGRectGetHeight(self.mainView.piano.frame);
    }
    CGFloat pointSize = (stavesSpace / (self.doc.song.musicTracks.count+1)) / HarmonyWizStaveVerticalScale;
    self.doc.metadata.notesPointSize = pointSize;
    CGFloat newHeight = HarmonyWizStaveVerticalScale * pointSize;
    [self applyStaveHeight:newHeight];
    [self redisplayAllTrackControls];
    [self.allStavesControl setNeedsLayout];
    [self.mainView setNeedsLayout];
}

- (void)pinchStaves:(UIPinchGestureRecognizer*)pinch {
    if (self.doc.metadata.currentToolType == kToolType_Hand) {
        if (self.doc.metadata.noteSpacing * pinch.scale < HWMinNoteSpacing) {
            pinch.scale = HWMinNoteSpacing / self.doc.metadata.noteSpacing;
        }
        if (self.doc.metadata.noteSpacing * pinch.scale > HWMaxNoteSpacing) {
            pinch.scale = HWMaxNoteSpacing / self.doc.metadata.noteSpacing;
        }
        
        switch (pinch.state) {
            case UIGestureRecognizerStateBegan:
            {
                self.zoomView.spacing = self.rulerControl.distanceBetweenBeats;
                self.zoomView.scale = pinch.scale;
                self.zoomView.alpha = 0.f;
                self.zoomView.hidden = NO;
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.5 animations:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    strongSelf.zoomView.alpha = 1.f;
                }];
            }
                break;
            case UIGestureRecognizerStateChanged:
            {
                self.zoomView.scale = pinch.scale;
            }
                break;
            case UIGestureRecognizerStateEnded:
            {
                __weak typeof(self) weakSelf = self;
                [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    strongSelf.zoomView.alpha = 0.f;
                } completion:^(BOOL finished){
                    __strong typeof(self) strongSelf = weakSelf;
                    strongSelf.zoomView.hidden = YES;
                }];
                
                float value = self.doc.metadata.noteSpacing * pinch.scale;
                self.doc.metadata.noteSpacing = value;
                [(HWMagicPadScrollView*)self.mainView.magicView clearAll];
                [self.mainView setNeedsLayout];
                [self.allStavesControl setNeedsLayoutStaves];
                [self.allStavesControl updateVisible:YES];
            }
                break;
                
            default:
                break;
        }
    }
}

- (void)openDrawer:(UISwipeGestureRecognizer*)gest {
    
    if (! self.trackControlIsAnimating && ! self.mainView.drawerOpened) {
        self.mainView.drawerOpened = YES;
        
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:kEditTrackAnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.mainView updateAllFrames];
        } completion:^(BOOL finished){
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.trackControlContentView.subviews makeObjectsPerformSelector:@selector(setNeedsDisplay)];
        }];
    }
}

- (void)closeDrawer:(UISwipeGestureRecognizer*)gest {
    
    if (! self.trackControlIsAnimating && self.mainView.drawerOpened) {
        self.mainView.drawerOpened = NO;
        
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:kEditTrackAnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.mainView updateAllFrames];
        } completion:^(BOOL finished){
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.trackControlContentView.subviews makeObjectsPerformSelector:@selector(setNeedsDisplay)];
        }];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    if ([identifier isEqualToString:@"clearMenu"]) {
        for (UIPopoverController *pc in self.visiblePopovers) {
            if ([pc.contentViewController.title isEqualToString:@"Clear Menu"]) {
                [self closeAllPopovers];
                return NO;
            }
        }
    }
    if ([identifier isEqualToString:@"songMenu"]) {
        for (UIPopoverController *pc in self.visiblePopovers) {
            if ([pc.contentViewController.title isEqualToString:@"Song Menu"]) {
                [self closeAllPopovers];
                return NO;
            }
        }
    }
    if ([identifier isEqualToString:@"settingsMenu"]) {
        for (UIPopoverController *pc in self.visiblePopovers) {
            if ([pc.contentViewController.title isEqualToString:@"Settings Menu"]) {
                [self closeAllPopovers];
                return NO;
            }
        }
    }
    if ([identifier isEqualToString:@"helpMenu"]) {
        for (UIPopoverController *pc in self.visiblePopovers) {
            if ([pc.contentViewController.title isEqualToString:@"Help Menu"]) {
                [self closeAllPopovers];
                return NO;
            }
        }
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self closeAllPopovers];
    
    if ([segue.destinationViewController respondsToSelector:@selector(navigationBar)]) {
        [segue.destinationViewController navigationBar].translucent = NO;
    }
    
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]]) {
        UIPopoverController *pc = [((UIStoryboardPopoverSegue*)segue) popoverController];
        pc.delegate = self;
        if (pc.contentViewController.navigationController) {
            pc.contentViewController.navigationController.delegate = self;
        }
        [self registerPopover:pc];
    }
    
    if ([segue.identifier isEqualToString:@"showHelp"]) {
        HWTutorialViewController *pvc = segue.destinationViewController;
        NSAssert(self.tutorialOpenedName != nil, @"No tutorial opened");
        pvc.tutorialName = self.tutorialOpenedName;
    }
    
    if ([segue.identifier isEqualToString:@"showAbout"]) {
        HWFormSheet *vc = segue.destinationViewController;
        UIWebView *wv = (id)[vc.view viewWithTag:1];
        NSURL *contentURL = [[NSBundle mainBundle] URLForResource:@"HWHelp/About/index" withExtension:@"html"];
        NSError *err = nil;
        NSString *content = [NSString stringWithContentsOfURL:contentURL encoding:NSUTF8StringEncoding error:&err];
        if (err) {
            [NSException raise:@"HarmonyWiz" format:@"%@", err.localizedDescription];
        }
        content = [content stringByReplacingOccurrencesOfString:@"{VERSION_STRING}" withString:[UIApplication appVersion]];
        [wv loadHTMLString:content baseURL:contentURL];
        vc.name = @"about";
    }
    
    if ([segue.identifier isEqualToString:@"showFAQ"]) {
        HWFormSheet *vc = segue.destinationViewController;
        UIWebView *wv = (id)[vc.view viewWithTag:1];
        NSURL *contentURL = [NSURL URLWithString:HWFAQURLString];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:contentURL];
        
        wv.delegate = self;
        [wv loadRequest:request];
        vc.name = @"FAQ";
    }
}

- (void)redisplayAllTrackControls {
    [self.trackControlContentView.subviews makeObjectsPerformSelector:@selector(setNeedsDisplay)];
}

- (void)updateArrangeMessage:(NSTimer*)timer {
    if (self.arrangeHUD) {
        self.arrangeMessageIndex = (self.arrangeMessageIndex + 1) % self.arrangeMessages.count;
        NSString *newMsg = [self.arrangeMessages objectAtIndex:self.arrangeMessageIndex];
        if (newMsg) {
            self.arrangeHUD.detailsLabelText = LinguiLocalizedString(newMsg, @"HUD detail text");
        }
    }
    else {
        [timer invalidate];
    }
}

- (void)revealKeyboardWizdom {
    if (! self.doc.metadata.pianoOpened) {
        self.doc.metadata.pianoOpened = YES;
        HarmonyWizMusicTrack *track = (self.doc.metadata.currentlySelectedTrackNumber < self.doc.song.musicTracks.count ? [self.doc.song.musicTracks objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber] : nil);
        [self.keyboardWizdom showRangeForTrack:track];
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:kTogglePianoDuration
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             __strong typeof(self) strongSelf = weakSelf;
                             [strongSelf.mainView updateAllFrames];
                         }
                         completion:^(BOOL finished) {
                             __strong typeof(self) strongSelf = weakSelf;
                             [strongSelf.toolboxController updateEnabled];
                             if (strongSelf.doc.metadata.stepRecord) {
                                 [strongSelf.toolboxController.toolboxView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
                             }
                             
                             // scroll keyboard to track range
                             [strongSelf.keyboardWizdom scrollKeyboardToRangeForTrack:track];
                         }];
    }
}

- (void)concealKeyboardWizdom {
    
    if (self.doc.metadata.pianoOpened) {
        self.doc.metadata.pianoOpened = NO;
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:kTogglePianoDuration
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             __strong typeof(self) strongSelf = weakSelf;
                             [strongSelf.mainView updateAllFrames];
                         }
                         completion:^(BOOL finished) {
                             __strong typeof(self) strongSelf = weakSelf;
                             [strongSelf.toolboxController updateEnabled];
                         }];
    }
}

- (HarmonyWizTrackControl*)harmonyTrackControl {
    return self.trackControlContentView.tracks.lastObject;
}

- (void)autoTieCurrentTrack:(NSNotification*)notif {
    [self closeAllPopovers];
    
    [self.doc.hwUndoManager startUndoableChanges];
    
    id track = [self.doc.song.musicTracks objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber];
    [self.doc.song selectNone];
    [track selectAll];
    [self.doc.song autoTieSelectedNotes];
    [self.doc.song selectNone];
    
    self.doc.metadata.harmonyScore = nil;
    
    [self.doc.hwUndoManager finishUndoableChangesName:@"Auto-tie"];
    
    [self redrawStaves:NO];
    
    [HWAppDelegate recordEvent:@"auto_tie_track" segmentation:@{ @"track" : @(self.doc.metadata.currentlySelectedTrackNumber) }];
}

- (void)setInstrument:(NSNotification*)notif {
    [self closeAllPopovers];
    [self.instrumentsViewController.navigationController popToRootViewControllerAnimated:NO];
    
    HarmonyWizTrackControl *tc = notif.object;
    self.instrumentsViewController.trackControl = tc;
    [self.instrumentsPopover setPopoverContentSize:CGSizeMake(320, 700) animated:NO];
    [self.instrumentsPopover presentPopoverFromRect:tc.instrumentLabel.frame inView:tc permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    [self registerPopover:self.instrumentsPopover];
}

- (void)changedStyle:(NSNotification*)notif {
    [self.harmonyTrackControl updateInstrument];
    [HWAppDelegate recordEvent:@"style_changed" segmentation:@{ @"style" : @((int)self.doc.metadata.harmonyStyle) }];
}

- (void)pressedStyle:(NSNotification*)notif {
    HarmonyWizTrackControl *tc = notif.object;
    //    [self performSegueWithIdentifier:@"openHarmony" sender:tc.instrumentLabel];
    [self closeAllPopovers];
    [self.stylesPopover presentPopoverFromRect:tc.instrumentLabel.frame inView:tc permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [self registerPopover:self.stylesPopover];
}

- (void)octaveTransposeChanged:(NSNotification*)notif {
    HWOctaveTransposeViewController *vc = notif.object;
    NSAssert([vc.trackControl.track isKindOfClass:[HarmonyWizMusicTrack class]], @"Not music track");
    int transposition = [[notif.userInfo objectForKey:@"transposition"] intValue];
    [self.doc.hwUndoManager startUndoableChanges];
    [((HarmonyWizMusicTrack*)vc.trackControl.track) setTransposition:transposition];
    [self.doc.hwUndoManager finishUndoableChangesName:@"Transposition"];
}

- (void)updateMusicPlayerAfterEdit {
    if (self.state != kStateStopped) {
        OSStatus result;
        
        result = MusicPlayerStop(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStop result is %d", (int)result];
        
        MusicTimeStamp timeStamp;
        result = MusicPlayerGetTime(self.musicPlayer, &timeStamp);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerGetTime result is %d", (int)result];
        
        if (self.state == kStatePlaying)
            [self updateMusicPlayer:NO precount:0];
        else
            [self updateMusicPlayer:self.doc.metadata.metronome precount:(self.doc.metadata.precount ? HWPrecountBars : 0)];
        
        result = MusicPlayerSetTime(self.musicPlayer, timeStamp);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerSetTime result is %d", (int)result];
        
        result = MusicPlayerStart(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStart result is %d", (int)result];
    }
}

- (void)arrangeThisChordNow:(NSNotification*)notif {
    [self closeAllPopovers];
    HarmonyWizHarmonyEvent *ev = notif.object;
    [self.doc.song selectNone];
    ev.selected = YES;
    [self beginArrange];
}

- (void)shareSong:(NSNotification*)notification {
    NSString *method = [notification.userInfo objectForKey:@"method"];
    NSIndexPath *indexPath = [notification.userInfo objectForKey:@"indexPath"];
    
    [self closeAllPopovers];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kPresentViewDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self shareSongWithMethod:method indexPath:indexPath];
    });
}

- (void)eraseNotesFinished:(NSNotification*)notif {
    [self updateMusicPlayerAfterEdit];
}

- (void)savedInstrumentUserPresetSelected:(NSNotification*)notif {
    NSString *name = [notif.userInfo objectForKey:@"name"];
    self.doc.metadata.instrumentsPresets = name;
}

- (void)instrumentUserPresetSelected:(NSNotification*)notif {
    {
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:self.view.window];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = LinguiLocalizedString(@"Loading", @"HUD title");
        hud.dimBackground = YES;
        hud.removeFromSuperViewOnHide = YES;
        [self.view.window addSubview:hud];
        [self.doc.hwUndoManager startUndoableChanges];
        
        [HWAppDelegate recordEvent:@"user_preset_selected"];
        
        [[AudioController sharedAudioController] stopProcessingGraph];
//        if ([AudioController sharedAudioController].numberOfActiveSamplers != self.doc.song.musicTracks.count) {
//            [[AudioController sharedAudioController] releaseAllPresets];
//            [[AudioController sharedAudioController] createPresetsForVoices:self.doc.song.musicTracks.count];
//        }
        [[AudioController sharedAudioController] updatePresetCountTo:self.doc.song.musicTracks.count];
        
        __weak typeof(self) weakSelf = self;
        [hud showAnimated:YES whileExecutingBlock:^{
            @autoreleasepool {
                __strong typeof(self) strongSelf = weakSelf;
                NSString *name = [notif.userInfo objectForKey:@"name"];
                strongSelf.doc.metadata.instrumentsPresets = name;
                BOOL loadedOK = YES;
                for (HarmonyWizMusicTrack *track in strongSelf.doc.song.musicTracks) {
                    NSString *name = [notif.userInfo objectForKey:track.title];
                    if (name == nil) {
                        // find closest alternative
                        NSArray *sopranoAlternatives = @[@"Mezzo",@"Contralto",@"Tenor",@"Baritone",@"Bass"];
                        NSArray *mezzoAlternatives = @[@"Soprano",@"Contralto",@"Tenor",@"Baritone",@"Bass"];
                        NSArray *altoAlternatives = @[@"Mezzo",@"Tenor",@"Soprano",@"Baritone",@"Bass"];
                        NSArray *tenorAlternatives = @[@"Contralto",@"Baritone",@"Mezzo",@"Bass",@"Soprano"];
                        NSArray *baritoneAlternatives = @[@"Tenor",@"Bass",@"Contralto",@"Mezzo",@"Soprano"];
                        NSArray *bassAlternatives = @[@"Baritone",@"Tenor",@"Contralto",@"Mezzo",@"Soprano"];
                        NSArray *alternatives;
                        if ([track.title hasPrefix:@"Soprano"])
                            alternatives = sopranoAlternatives;
                        else if ([track.title hasPrefix:@"Mezzo"])
                            alternatives = mezzoAlternatives;
                        else if ([track.title hasPrefix:@"Contralto"])
                            alternatives = altoAlternatives;
                        else if ([track.title hasPrefix:@"Tenor"])
                            alternatives = tenorAlternatives;
                        else if ([track.title hasPrefix:@"Baritone"])
                            alternatives = baritoneAlternatives;
                        else if ([track.title hasPrefix:@"Bass"])
                            alternatives = bassAlternatives;
                        else
                            alternatives = nil;
                        for (NSString *alt in alternatives) {
                            NSArray *keys = [notif.userInfo.allKeys filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF beginswith %@", alt]];
                            if (keys.count > 0) {
                                name = [notif.userInfo objectForKey:keys.firstObject];
                                if (name != nil) {
                                    break;
                                }
                            }
                        }
                        NSAssert(name != nil, @"No valid instrument found");
                    }
                    BOOL result = [[AudioController sharedAudioController] createPresetFromSamplesIn:name withExtension:@"aif" forVoice:track.trackNumber];
                    if (result) {
                        track.instrument = name;
                    }
                    else {
                        loadedOK = NO;
                    }
                    if (track != strongSelf.doc.song.musicTracks.lastObject) {
                        [NSThread sleepForTimeInterval:HWSampleLoadDelay];
                    }
                }
                if (! loadedOK) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[[UIAlertView alloc] initWithTitle:@"Failed to load samples" message:@"There was a problem loading some of the samples." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    });
                }
            }
        } completionBlock:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.musicTrackControls makeObjectsPerformSelector:@selector(updateInstrument)];
            [strongSelf.doc.hwUndoManager finishUndoableChangesName:@"Instruments preset"];
            
            if (strongSelf.doc.metadata.magicMode) {
                [strongSelf updateLeftPanelInstrumentLabel];
            }
            
            [[AudioController sharedAudioController] startProcessingGraph];
        }];
    }
    [self closeAllPopovers];
}

- (void)instrumentPresetSelected:(NSNotification*)notif {
    {
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:self.view.window];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = LinguiLocalizedString(@"Loading", @"HUD title");
        hud.dimBackground = YES;
        hud.removeFromSuperViewOnHide = YES;
        [self.view.window addSubview:hud];
        [self.doc.hwUndoManager startUndoableChanges];
        
        [[AudioController sharedAudioController] stopProcessingGraph];
//        if ([AudioController sharedAudioController].numberOfActiveSamplers != self.doc.song.musicTracks.count) {
//            [[AudioController sharedAudioController] releaseAllPresets];
//            [[AudioController sharedAudioController] createPresetsForVoices:self.doc.song.musicTracks.count];
//        }
        [[AudioController sharedAudioController] updatePresetCountTo:self.doc.song.musicTracks.count];
        
        [HWAppDelegate recordEvent:@"factory_preset_selected" segmentation:@{ @"name" : [notif.userInfo objectForKey:@"name"] }];
        
        __weak typeof(self) weakSelf = self;
        [hud showAnimated:YES whileExecutingBlock:^{
            @autoreleasepool {
                __strong typeof(self) strongSelf = weakSelf;
                NSArray *preset = [notif.userInfo objectForKey:@"preset"];
                NSString *name = [notif.userInfo objectForKey:@"name"];
                strongSelf.doc.metadata.instrumentsPresets = name;
                BOOL loadedOK = YES;
                for (HarmonyWizMusicTrack *track in strongSelf.doc.song.musicTracks) {
                    NSString *name = nil;
                    if ([track.title hasPrefix:@"Soprano"])
                        name = [preset objectAtIndex:0];
                    else if ([track.title hasPrefix:@"Mezzo"])
                        name = [preset objectAtIndex:1];
                    else if ([track.title hasPrefix:@"Contralto"])
                        name = [preset objectAtIndex:2];
                    else if ([track.title hasPrefix:@"Tenor"])
                        name = [preset objectAtIndex:3];
                    else if ([track.title hasPrefix:@"Baritone"])
                        name = [preset objectAtIndex:4];
                    else if ([track.title hasPrefix:@"Bass"])
                        name = [preset objectAtIndex:5];
                    
                    BOOL result = [[AudioController sharedAudioController] createPresetFromSamplesIn:name withExtension:@"aif" forVoice:track.trackNumber];
                    if (result) {
                        track.instrument = name;
                    }
                    else {
                        loadedOK = NO;
                    }
                    if (track != strongSelf.doc.song.musicTracks.lastObject) {
                        [NSThread sleepForTimeInterval:HWSampleLoadDelay];
                    }
                }
                if (! loadedOK) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[[UIAlertView alloc] initWithTitle:@"Failed to load samples" message:@"There was a problem loading some of the samples." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    });
                }
            }
        } completionBlock:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.musicTrackControls makeObjectsPerformSelector:@selector(updateInstrument)];
            [strongSelf.doc.hwUndoManager finishUndoableChangesName:@"Instruments preset"];
            
            if (strongSelf.doc.metadata.magicMode) {
                [strongSelf updateLeftPanelInstrumentLabel];
            }
            [[AudioController sharedAudioController] startProcessingGraph];
        }];
    }
    [self closeAllPopovers];
}

- (void)updateLeftPanelInstrumentLabel {
    HarmonyWizMusicTrack *track = [self.doc.song.musicTracks objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber];
    UILabel *instrumentLabel = (id)[self.mainView.leftPanel viewWithTag:HWInstrumentLabel];
    instrumentLabel.text = track.instrument;
    CGSize sz = [instrumentLabel sizeThatFits:CGSizeMake(HWPanelWidth-2, 64.f)];
    instrumentLabel.frame = CGRectMake(4, 4, HWPanelWidth-2, sz.height);
}

- (void)changedInstrument:(NSNotification*)notif {
    NSString *name = [notif.userInfo objectForKey:@"instrumentName"];
    [self.doc.hwUndoManager startUndoableChanges];
    self.doc.metadata.instrumentsPresets = @"Custom";
    [self.instrumentsViewController.trackControl loadSamples:name];
    [self.doc.hwUndoManager finishUndoableChangesName:@"Instrument"];
    [HWAppDelegate recordEvent:@"changed_instrument" segmentation:@{ @"name" : name }];
}

- (void)exitEditMode:(id)sender completion:(void (^)(void))completionBlock {
    if (self.doc.metadata.magicMode) {
        self.view.userInteractionEnabled = NO;
        self.doc.metadata.magicMode = NO;
        HWFocusButtons *focus = (id)[self.btnFocus.customView viewWithTag:HWFocusButtonTag];
        focus.state = kFocus_MultiTrack;
        
        [(HWMagicPadScrollView*)self.mainView.magicView removeShapeViews];
        
        [self clearAllTrackAndStaveControls];
        [self createAllTrackAndStaveControls];
        [self.allStavesControl setNeedsLayout];
        
        if ([self applyDefaultInstrumentsToAllTracks]) {
            [self.musicTrackControls makeObjectsPerformSelector:@selector(updateInstrument)];
            [self applyAudioSettingsBackground];
        }
        
        self.doc.metadata.currentToolType = self.fullScoreTool;
        [self toolChange:nil];
        
        [self closeAllPopovers];
        [self setupKeyboardBarButton];
        
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:kEditTrackAnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.mainView updateAllFrames];
        } completion:^(BOOL finished){
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf updateToolbarAndRuler];
            [strongSelf redrawStaves:NO];
            [strongSelf.rulerControl setNeedsLayout];
            strongSelf.rulerControl.isPointer = YES;
            [strongSelf.rulerControl updateBarViews];
            strongSelf.allStavesControl.locatorTriangleHidden = YES;
            [strongSelf.mainView.magicView setNeedsLayout];
            self.view.userInteractionEnabled = YES;
            if (completionBlock != nil) {
                completionBlock();
            }
            
            [HWAppDelegate recordEvent:@"exit_single_track_mode"];
        }];
    }
}

- (void)editTrack:(NSUInteger)trackNumber {
    if (! self.doc.metadata.magicMode) {
        self.view.userInteractionEnabled = NO;
        self.doc.metadata.magicMode = YES;
        HWFocusButtons *focus = (id)[self.btnFocus.customView viewWithTag:HWFocusButtonTag];
        focus.state = kFocus_SingleTrack;
        
        [self clearAllTrackAndStaveControls];
        [self createAllTrackAndStaveControls];
        [self.allStavesControl setNeedsLayout];
        
        [self updateToolbarAndRuler];
        
        [(HWMagicPadScrollView*)self.mainView.magicView setStaveView:self.allStavesControl.musicStaveViews.lastObject];
        self.fullScoreTool = self.doc.metadata.currentToolType;
        self.doc.metadata.currentToolType = kToolType_Hand;
        
        [self updateLeftPanelInstrumentLabel];
        
        self.rulerControl.isPointer = NO;
        self.allStavesControl.locatorTriangleHidden = NO;
        
        [self closeAllPopovers];
        
        [self setupKeyboardBarButton];
        
        [(HWMagicPadScrollView*)self.mainView.magicView updateShapeViewsFromSong];
        
        __weak typeof(self) weakSelf = self;
        [UIView animateWithDuration:kEditTrackAnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.mainView updateAllFrames];
        } completion:^(BOOL finished){
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf toolChange:nil];
            [strongSelf.rulerControl setNeedsLayout];
            [strongSelf.rulerControl updateBarViews];
            [strongSelf.allStavesControl setNeedsLayoutStaves];
            [strongSelf.mainView.magicView setNeedsLayout];
            self.view.userInteractionEnabled = YES;
            [HWAppDelegate recordEvent:@"enter_single_track_mode"];
        }];
    }
}

- (void)scrollToPlaybackPosition {
    const CGFloat dist = 80.f;
    if (self.rulerControl.playbackIndicatorXLoc - self.mainView.stavesView.contentOffset.x > CGRectGetWidth(self.mainView.stavesView.bounds) - dist) {
        const CGFloat newXLoc = MIN(MAX(self.rulerControl.playbackIndicatorXLoc - dist,0), self.mainView.stavesView.contentSize.width - CGRectGetWidth(self.mainView.stavesView.bounds));
        [self.mainView.stavesView setContentOffset:CGPointMake(newXLoc, self.mainView.stavesView.contentOffset.y) animated:NO];
    }
    else if (self.rulerControl.playbackIndicatorXLoc < self.mainView.stavesView.contentOffset.x) {
        [self.mainView.stavesView setContentOffset:CGPointMake(MAX(self.rulerControl.playbackIndicatorXLoc - dist,0), self.mainView.stavesView.contentOffset.y) animated:NO];
    }
}

- (void)closeAllPopovers {
    for (UIPopoverController *p in self.visiblePopovers)
        [p dismissPopoverAnimated:YES];
    [self.visiblePopovers removeAllObjects];
}

- (void)createToolbox {
    self.toolboxController.toolboxView.frame = CGRectMake(HWScreenWidth-HWToolboxWidth, 76.f, HWToolboxWidth, self.defaultStavesHeight);
    self.toolboxController.toolboxView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.toolboxController setup];
    [self.view bringSubviewToFront:self.toolboxController.toolboxView];
}

- (NSArray*)musicTrackControls {
    if (self.doc.metadata.magicMode) {
        return self.trackControlContentView.tracks;
    }
    else {
        return [self.trackControlContentView.tracks subarrayWithRange:NSMakeRange(0, self.trackControlContentView.tracks.count-1)];
    }
}


- (void)clearAllTrackAndStaveControls {
    [self.trackControlContentView.tracks makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

- (void)applyDefaultStaveHeight {
    [self applyStaveHeight:HarmonyWizStaveVerticalScale * self.doc.metadata.notesPointSize];
}

- (void)applyStaveHeight:(CGFloat)height {
    
    CGFloat totalHeight = 0.f;
    
    // set height of track controls
    for (UIView *view in self.trackControlContentView.subviews) {
        if ([view isKindOfClass:[HarmonyWizTrackControl class]]) {
            HarmonyWizTrackControl *tc = (HarmonyWizTrackControl*)view;
            tc.height = height;
            totalHeight += height;
        }
    }
    
    self.mainView.tracksView.contentSize = CGSizeMake(HWTracksViewOpenedWidth, totalHeight);
    [self.trackControlContentView setNeedsLayout];
    [self.mainView.tracksView setNeedsLayout];
    
    [self resizeScrollViewContentToFitSongWithHeight:totalHeight];
}

- (void)resizeScrollViewContentToFitSongWithHeight:(CGFloat)height {
    const CGFloat xOffset = self.rulerControl.xOffset;
    const CGFloat contentWidth = [self.rulerControl totalSongWidth] + xOffset + HWPaddingAfterStave;
    const CGFloat trackHeight = HarmonyWizStaveVerticalScale * self.doc.metadata.notesPointSize;
    
    const CGFloat rulerHeight = (self.doc.metadata.magicMode ? HWRulerHeightMagicMode : HWRulerHeight);
    
    const CGFloat magicPadHeight = HWScreenHeight - rulerHeight - (HWToolbarHeight+HWStatusBarHeight+trackHeight);
    
    self.mainView.magicView.contentSize = CGSizeMake(contentWidth, magicPadHeight);
    self.mainView.stavesView.contentSize = CGSizeMake(contentWidth,height);
    self.mainView.rulerView.contentSize = CGSizeMake(contentWidth,rulerHeight);
    
    self.mainView.tracksView.contentSize = CGSizeMake(HWTracksViewOpenedWidth, height);
    
    [self.allStavesControl updateTouchableFrames];
    [self.allStavesControl setNeedsLayoutStaves];
    [self.rulerControl redisplay];
    
    self.allStavesControl.contentOffset = self.doc.metadata.scoreScroll;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.view layoutIfNeeded];
    
    [self applyDefaultStaveHeight];
}

- (void)redrawStaves:(BOOL)flush {
    if (flush) {
        [self.allStavesControl flushAllStaves];
    }
    [self.allStavesControl updateAllStavesNow];
    
//    if (self.doc.metadata.magicMode) {
//        [(HWMagicPadScrollView*)self.mainView.magicPad clearAll];
//    }
}

- (HWStavesScrollView*)allStavesControl {
    return (id)self.mainView.stavesView;
}

- (HWTimeRulerScrollView*)rulerControl {
    return (id)self.mainView.rulerView;
}

- (HWTracksContentsView*)trackControlContentView {
    return [(HWTracksScrollView*)self.mainView.tracksView contentView];
}

- (void)createRulerControl {
    HWTimeRulerScrollView *rulerControl = [[HWTimeRulerScrollView alloc] initWithFrame:CGRectMake(HWTracksViewClosedWidth, HWToolbarHeight, HWScreenWidth-HWTracksViewClosedWidth, HWRulerHeight)];
    rulerControl.translatesAutoresizingMaskIntoConstraints = NO;
    rulerControl.tag = HWRulerTag;
    
    self.mainView.rulerView = rulerControl;
    self.mainView.rulerView.contentSize = CGSizeMake(HWScreenWidth-HWTracksViewClosedWidth, 32);
    self.mainView.rulerView.delegate = self;
    self.mainView.rulerView.showsHorizontalScrollIndicator = NO;
    self.mainView.rulerView.showsVerticalScrollIndicator = NO;
    [rulerControl updateVisible:YES];
    self.mainView.rulerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.mainView addSubview:self.mainView.rulerView];
}

- (void)createAllTrackAndStaveControls {
    HWDocument *doc = self.doc;
    
    [self.allStavesControl createAllStaveViews];
    
//    AudioController *ac = [AudioController sharedAudioController];
    if (self.doc.metadata.magicMode) {
        HarmonyWizMusicTrack *track = [doc.song.musicTracks objectAtIndex:doc.metadata.currentlySelectedTrackNumber];
        [self createTrackControlForTrack:track];
//        [ac setChannel:(UInt32)track.trackNumber enabled:YES];
    }
    else {
        for (HarmonyWizMusicTrack *track in doc.song.musicTracks) {
            [self createTrackControlForTrack:track];
//            [ac setChannel:(UInt32)track.trackNumber enabled:YES];
        }
        [self createTrackControlForTrack:doc.song.harmony];
    }
//    for (NSUInteger idx = doc.song.musicTracks.count; idx < HWAudioControllerSamplerCount; idx++) {
//        [ac setChannel:(UInt32)idx enabled:NO];
//    }
//    [self applyDefaultStaveHeight];
}


- (void)createTrackControlForTrack:(HarmonyWizTrack*)track {
    
    ///////////////////////////////////
    // create track control
    ///////////////////////////////////
    HarmonyWizTrackControl *trackControl = [[HarmonyWizTrackControl alloc] initWithFrame:CGRectMake(0, track.trackNumber*100, HWTracksViewClosedWidth, 100) trackNumber:track.trackNumber];
    
    trackControl.autoresizingMask = UIViewAutoresizingNone;
    
    trackControl.tag = track.trackNumber;
    
    [trackControl updateSolo];
    [trackControl updateMute];
    [trackControl updateLocked];
    
    // add to scroll view]
    trackControl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.trackControlContentView addSubview:trackControl];
    [self.trackControlContentView setNeedsLayout];
}

- (void) updatePlayback:(NSTimer *)sender {
    NSAssert(sender == self.updatePlaybackTimer, @"wrong timer to updatePlayback");
    if (sender == self.updatePlaybackTimer) {
        Boolean playing = FALSE;
        OSStatus result;
        
        if (self.musicPlayer) {
            result = MusicPlayerIsPlaying(self.musicPlayer, &playing);
            NSAssert(result == noErr, @"MusicPlayerIsPlaying");
        }
        
        if (playing) {
            MusicTimeStamp time;
            
            result = MusicPlayerGetTime(self.musicPlayer, &time);
            NSAssert(result == noErr, @"MusicPlayerGetTime");
            
            if (self.doc.metadata.precount && self.state == kStateRecording) {
                time -= HWPrecountBars * self.doc.song.timeSign.upperNumeral * (4.f / self.doc.song.timeSign.lowerNumeral);
            }
            self.doc.song.playbackPosition = time;
            [self.rulerControl updatePlaybackIndicatorPosition];
            [self.allStavesControl updatePositionIndicator];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:HWAutoScroll]) {
                [self scrollToPlaybackPosition];
            }
            
            //        if (self.doc.metadata.currentlySelectedTrackNumber >= 0 && self.doc.metadata.currentlySelectedTrackNumber < self.allStavesControl.musicStaveViews.count) {
            //            if (self.lastNoteRecorded && -[self.lastNoteRecorded timeIntervalSinceNow] > HWRecordingRedrawDelay) {
            //                [[self.allStavesControl.musicStaveViews objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber] updateStave];
            //            }
            //        }
        }
        else {
            [self.updatePlaybackTimer invalidate];
            self.updatePlaybackTimer = nil;
        }
    }
}

- (NSString*)songPositionToString:(float)position {
    const SInt16 bar = (SInt16)(position / self.doc.song.timeSign.upperNumeral * (self.doc.song.timeSign.lowerNumeral/4.));
    const SInt16 beat = (SInt16)(fmodf(position, self.doc.song.timeSign.upperNumeral * (4.f/self.doc.song.timeSign.lowerNumeral)) * (self.doc.song.timeSign.lowerNumeral/4.f));
    return [@"bar " : @(bar+1).stringValue : @" beat " : @(beat+1).stringValue];
}

- (void)reportFixesApplied {
    UINavigationController *navVC = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"fixesReport"];
    navVC.modalPresentationStyle = UIModalPresentationFormSheet;
    HWFixesViewController *fixesVC = (id)navVC.topViewController;
    fixesVC.fixes = self.fixesApplied;
    [self presentViewController:navVC animated:YES completion:nil];
}

- (void)fixAll {
    [self.doc.hwUndoManager startUndoableChanges];
    NSUInteger fixCount = 0;
    {
        NSDictionary *fixes;
        do {
            fixes = [self preArrangeCheck];
            if ([fixes objectForKey:@"replacement"] == nil)
                break;  // means there is nothing to fix
            {
                NSString *reason = LinguiLocalizedString([fixes objectForKey:@"message"], @"Harmonisation failure reason");
                HarmonyWizNoteEvent *event = [fixes objectForKey:@"offending"];
                NSAssert(event != nil, @"no offending note");
                const float beat = event.beat + (float)event.subdivision / (float)self.doc.song.beatSubdivisions;
                const float position = beat * (4.f / self.doc.song.timeSign.lowerNumeral);
                [self.fixesApplied addObject:@{ @"reason" : reason, @"position" : @(position) }];
            }
            HarmonyWizNoteEvent *ev = [fixes objectForKey:@"offending"];
            HarmonyWizNoteEvent *replacement = [fixes objectForKey:@"replacement"];
            [self.doc.song replaceMusicEvent:ev with:replacement];
            BOOL shouldSplice = [[fixes objectForKey:@"splice"] boolValue];
            if (shouldSplice) {
                [self.doc.song spliceHarmony];
            }
            fixCount ++;
        } while (fixes != nil);
    }
    [self.doc.hwUndoManager finishUndoableChangesName:LinguiLocalizedString(@"Fix all", @"Button title")];
    [self redrawStaves:NO];
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    if (expertMode) {
        __weak typeof(self) weakSelf = self;
        RIButtonItem *info = [RIButtonItem itemWithLabel:@"Info" action:^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf reportFixesApplied];
        }];
        RIButtonItem *arrange = [RIButtonItem itemWithLabel:@"Arrange" action:^{
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                __strong typeof(self) strongSelf = weakSelf;
                [strongSelf continueArrange];
            }];
        }];
        self.currentAlert = [[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Fix all", @"Button title") message:[NSString stringWithFormat:@"%lu fix%@ applied", (unsigned long)fixCount, fixCount>1?@"es":@""] cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"OK", @"OK button")] otherButtonItems:info, arrange, nil];
        [self.currentAlert show];
    }
    else {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{[self continueArrange];}];
    }
}

#pragma mark - alert view

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if (alertView == self.currentAlert) {
//        self.currentAlert.delegate = nil;
//        self.currentAlert = nil;
//    }
    [NSException raise:@"HarmonyWiz" format:@"Alert view delegates should not be used"];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [NSException raise:@"HarmonyWiz" format:@"Alert view delegates should not be used"];
}

#pragma mark - instruments

- (BOOL)applyDefaultInstrumentsToAllTracks {
    BOOL result = NO;
    for (HarmonyWizTrackControl *tc in self.musicTrackControls) {
        HarmonyWizMusicTrack *mt = (id)tc.track;
        if (mt.instrument == nil) {
            mt.instrument = [self defaultInstrumentNameForTrackTitle:mt.title];
            result = YES;
        }
    }
    return result;
}

- (NSString*)defaultInstrumentNameForTrackTitle:(NSString*)title {
    NSString *defaultInstrumentName = nil;
    if (self.instrumentsViewController.instrumentNames.count > 0) {
        NSString *name = nil;
        if ([title hasPrefix:@"Soprano"])
            name = @"Violin";
        else if ([title hasPrefix:@"Mezzo"])
            name = @"Violin";
        else if ([title hasPrefix:@"Contralto"])
            name = @"Violin";
        else if ([title hasPrefix:@"Tenor"])
            name = @"Cello";
        else if ([title hasPrefix:@"Baritone"])
            name = @"Cello";
        else if ([title hasPrefix:@"Bass"])
            name = @"Cello";
        
        if ([self.instrumentsViewController.instrumentNames containsObject:name]) {
            defaultInstrumentName = name;
        }
        else {
            defaultInstrumentName = [self.instrumentsViewController.instrumentNames objectAtIndex:0];
        }
    }
    return defaultInstrumentName;
}


#pragma mark - audio


const struct EndData {
    UInt32  length;
    UInt32  code;
} defaultMusicSequenceEndData = {4,'end.'};

static void EndMusicSequenceUserCallback (
                                          void                      *inClientData,
                                          MusicSequence             inSequence,
                                          MusicTrack                inTrack,
                                          MusicTimeStamp            inEventTime,
                                          const MusicEventUserData  *inEventData,
                                          MusicTimeStamp            inStartSliceBeat,
                                          MusicTimeStamp            inEndSliceBeat
                                          ) {
    const struct EndData *endData = (const struct EndData*)inEventData;
    const BOOL validData = (endData->length == defaultMusicSequenceEndData.length &&
                            endData->code == defaultMusicSequenceEndData.code);
    if (validData) {
        HWSongViewController *aSelf = (__bridge HWSongViewController*)inClientData;
        [aSelf handleEndMusicSequence];
    }
}

- (void)handleEndMusicSequence {
    if (self.doc.metadata.loopingEnabled)
        [self performSelectorOnMainThread:@selector(restartPlayback) withObject:nil waitUntilDone:NO];
    else
        [self performSelectorOnMainThread:@selector(stopRewind:) withObject:nil waitUntilDone:NO];
}

- (void)restartPlayback {
    if (self.state == kStateRecording) {
        OSStatus result;
        
        result = MusicPlayerStop(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStop result is %d", (int)result];
        
        [self updateStaveAfterRecording];
        [self redrawStaves:NO];
        
        [self updateMusicPlayer:self.doc.metadata.metronome precount:(self.doc.metadata.precount ? HWPrecountBars : 0)];
        
        result = MusicPlayerStart(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStart result is %d", (int)result];
    }
    
    if (self.state == kStateRecording && self.doc.metadata.precount) {
        MusicPlayerSetTime(_musicPlayer, HWPrecountBars * self.doc.song.timeSign.upperNumeral * (4.f / self.doc.song.timeSign.lowerNumeral));
    }
    else {
        MusicPlayerSetTime(_musicPlayer, 0);
    }
}

- (void)updateMusicPlayer:(BOOL)enableMetronome precount:(int)precount {
    OSStatus result;
    
    // Create music sequence
    MusicSequence musicSequence = [self.doc.song convertToMusicSequence:precount includeControlMessages:NO transposing:YES];
    MusicTrack metronomeTrack = NULL;
    
    if (musicSequence == NULL) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Sorry but I encountered a problem playing the song. If this problem persists, you may need to restart the app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    if (enableMetronome) {
        HWThrowIfError(MusicSequenceNewTrack(musicSequence, &metronomeTrack));
        for (UInt16 bar = 0; bar < self.doc.song.finalBarNumber + precount; bar++) {
            for (UInt16 beat = 0; beat < self.doc.song.timeSign.upperNumeral; beat++) {
                MIDINoteMessage message = {9,60,127,0,(4. / self.doc.song.timeSign.lowerNumeral)};
                MusicTimeStamp timestamp = (bar * self.doc.song.timeSign.upperNumeral * (4. / self.doc.song.timeSign.lowerNumeral)) + beat * (4. / self.doc.song.timeSign.lowerNumeral);
                HWThrowIfError(MusicTrackNewMIDINoteEvent(metronomeTrack, timestamp, &message));
                if (beat == 0) {
                    message.note = 61;
                    HWThrowIfError(MusicTrackNewMIDINoteEvent(metronomeTrack, timestamp, &message));
                }
            }
        }
    }
    
    // create user track and set callback
    MusicTrack userTrack = NULL;
    result = MusicSequenceNewTrack(musicSequence, &userTrack);
    if (result != noErr)
        [NSException raise:@"HarmonyWizError" format:@"MusicSequenceNewTrack result is %d", (int)result];
    
    const MusicTimeStamp endTime = (self.doc.song.finalBarNumber + precount) * self.doc.song.timeSign.upperNumeral * (4. / self.doc.song.timeSign.lowerNumeral);
    result = MusicTrackNewUserEvent(userTrack, endTime,
                                    (MusicEventUserData*)&defaultMusicSequenceEndData);
    if (result != noErr)
        [NSException raise:@"HarmonyWizError" format:@"MusicTrackNewUserEvent result is %d", (int)result];
    
    result = MusicSequenceSetUserCallback(musicSequence,
                                          EndMusicSequenceUserCallback,
                                          (__bridge void*)self);
    if (result != noErr)
        [NSException raise:@"HarmonyWizError" format:@"MusicSequenceSetUserCallback result is %d", (int)result];
    
    if (self.doc.midiDestination) {
        
        [self midiApplyMuteAndSolo];
        
        // set MIDI endpoint
        result = MusicSequenceSetMIDIEndpoint(musicSequence, self.doc.midiDestination.endpoint);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicSequenceSetMIDIEndpoint result is %d", (int)result];
    }
    else {
        
        AudioController *audioController = [AudioController sharedAudioController];
        
        // set processing graph
        result = MusicSequenceSetAUGraph(musicSequence, audioController.audioController.audioGraph);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicSequenceSetAUGraph result is %d", (int)result];
        
        // set track nodes
        for (UInt32 i = 0; i < self.doc.song.musicTracks.count; i++) {
            MusicTrack musicTrack;
            result = MusicSequenceGetIndTrack(musicSequence, i, &musicTrack);
            if (result != noErr)
                [NSException raise:@"HarmonyWizError" format:@"MusicSequenceGetIndTrack result is %d", (int)result];
            result = MusicTrackSetDestNode(musicTrack, [audioController samplerNodeForVoice:i]);
            if (result != noErr)
                [NSException raise:@"HarmonyWizError" format:@"MusicTrackSetDestNode result is %d", (int)result];
        }
        if (enableMetronome) {
            result = MusicTrackSetDestNode(metronomeTrack, [audioController metronomeSamplerNode]);
            if (result != noErr)
                [NSException raise:@"HarmonyWizError" format:@"MusicTrackSetDestNode result is %d", (int)result];
        }
    }
    
    result = MusicPlayerSetSequence(self.musicPlayer, musicSequence);
    if (result != noErr)
        [NSException raise:@"HarmonyWizError" format:@"MusicPlayerSetSequence result is %d", (int)result];
}

- (void)startPlayback:(BOOL)enableMetronome precount:(int)precount {
    
    OSStatus result;
    
    [self.doc.song checkAllEventsSorted];
    
    {
        // create music player
        result = NewMusicPlayer(&_musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"NewMusicPlayer result is %d", (int)result];
        
        [self updateMusicPlayer:enableMetronome precount:precount];
        
        result = MusicPlayerSetTime(self.musicPlayer, self.doc.song.playbackPosition /*- precount * self.doc.song.timeSign.upperNumeral / (self.doc.song.timeSign.lowerNumeral/4)*/);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerSetTime result is %d", (int)result];
        result = MusicPlayerStart(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStart result is %d", (int)result];
    }
    
    // setup playback indicator
    {
        NSTimeInterval const HWPlaybackUpdateInterval = 0.025;
        self.updatePlaybackTimer = [NSTimer timerWithTimeInterval:HWPlaybackUpdateInterval target:self selector:@selector(updatePlayback:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.updatePlaybackTimer forMode:NSRunLoopCommonModes];
        
//        CADisplayLink *dlink = [CADisplayLink displayLinkWithTarget:self
//                                                           selector:@selector(updatePlayback:)];
//        [dlink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    }
}

- (void)stopPlayback {
    OSStatus result;
    MusicSequence seq;
    
    if (self.musicPlayer) {
        result = MusicPlayerGetSequence(self.musicPlayer, &seq);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerGetSequence result is %d", (int)result];
        
        result = MusicPlayerStop(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"MusicPlayerStop result is %d", (int)result];
        
        result = DisposeMusicPlayer(self.musicPlayer);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"DisposeMusicPlayer result is %d", (int)result];
        
        result = DisposeMusicSequence(seq);
        if (result != noErr)
            [NSException raise:@"HarmonyWizError" format:@"DisposeMusicSequence result is %d", (int)result];
        
        self.musicPlayer = nil;
    }
}

- (void) applyGains {
    AudioController *ac = [AudioController sharedAudioController];
    for (UInt8 i = 0; i < self.doc.song.musicTracks.count; i++) {
        float gain = [[self.doc.song.musicTracks objectAtIndex:i] volume];
        [ac setMixerInput:i gain:gain];
        [self midiVolumeChange:gain channel:i];
    }
    [self midiApplyMuteAndSolo];
}

- (void) applyPans {
    AudioController *ac = [AudioController sharedAudioController];
    for (UInt8 i = 0; i < self.doc.song.musicTracks.count; i++) {
        float pan = [[self.doc.song.musicTracks objectAtIndex:i] pan];
        [ac setMixerInput:i pan:pan];
        [self midiPanChange:pan channel:i];
    }
}

#if (! RELEASE_BUILD)
- (void)checkNoteOnEasterEggFor:(Byte)noteValue {
    const CFTimeInterval thisTime = CACurrentMediaTime();
    
    if (thisTime - self.easterEggTime > 1) {
        self.easterEggNotes = [NSMutableArray array];
    }
    self.easterEggTime = thisTime;
    
    [self.easterEggNotes addObject:@(noteValue)];
    if ([self.easterEggNotes isEqualToArray:@[@(62),@(63),@(60),@(59),@(64)]]) {
        [self.easterEggNotes removeAllObjects];
        
        NSLog(@"Easter egg triggered");
        [NSException raise:@"Force crash" format:nil];
        [[Crashlytics sharedInstance] crash];
    }
}
#endif

#pragma mark - MIDI

// MIDI constants
enum {
    kMIDIControlChange = 0xB0,
    kMIDIControlChangeVolume = 0x07,
    kMIDIControlChangePan = 0x0A,
    kMIDIControlModWheel1 = 0x01,
    kMIDIControlModWheel2 = 0x02,
    kMIDICommandPitchBend = 0xE0,
    kMIDINoteOn = 0x90,
    kMIDINoteOff = 0x80
};

- (void)applyAllMIDISettings {
    for (UInt8 i = 0; i < self.doc.song.musicTracks.count; i++) {
        float gain = [[self.doc.song.musicTracks objectAtIndex:i] volume];
        [self midiVolumeChange:gain channel:i];
        float pan = [[self.doc.song.musicTracks objectAtIndex:i] pan];
        [self midiPanChange:pan channel:i];
    }
    [self midiApplyMuteAndSolo];
}

- (void)midiApplyMuteAndSolo {
    if (self.doc.midiDestination) {
        AudioController *ac = [AudioController sharedAudioController];
        for (HarmonyWizMusicTrack *mt in self.doc.song.musicTracks) {
            const BOOL muted = [ac getChannelMuted:(UInt32)mt.trackNumber];
            const AudioUnitParameterValue volume = (muted ? 0 : mt.volume);
            const Byte channel = mt.trackNumber;
            const MIDIChannelMessage volumeMsg = {(UInt8)(kMIDIControlChange + channel), kMIDIControlChangeVolume, (UInt8)(volume*127.f+0.5f), 0};
            [self.doc.midiDestination sendBytes:(UInt8*)&volumeMsg size:3];
        }
    }
}

- (void)midiVolumeChange:(AudioUnitParameterValue)volume channel:(Byte)channel {
    if (self.doc.midiDestination) {
        const MIDIChannelMessage volumeMsg = {(UInt8)(kMIDIControlChange + channel), kMIDIControlChangeVolume, (UInt8)(volume*127.f+0.5f), 0};
        [self.doc.midiDestination sendBytes:(UInt8*)&volumeMsg size:3];
    }
}

- (void)midiPanChange:(AudioUnitParameterValue)pan channel:(Byte)channel {
    if (self.doc.midiDestination) {
        const MIDIChannelMessage panMsg = {(UInt8)(kMIDIControlChange + channel), kMIDIControlChangePan, (UInt8)((pan+1.f)*0.5f*127.f+0.5f), 0};
        [self.doc.midiDestination sendBytes:(UInt8*)&panMsg size:3];
    }
}

- (void)midiNoteOn:(Byte)note velocity:(Byte)velocity channel:(Byte)channel {
    if (self.doc.midiDestination) {
        UInt8 bytes[3] = {(UInt8)(kMIDINoteOn + channel), note, velocity};
        [self.doc.midiDestination sendBytes:bytes size:sizeof(bytes)];
    }
}

- (void)midiNoteOff:(Byte)note channel:(Byte)channel {
    if (self.doc.midiDestination) {
        UInt8 bytes[3] = {(UInt8)(kMIDINoteOff + channel), note, 0};
        [self.doc.midiDestination sendBytes:bytes size:sizeof(bytes)];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        recordError(error)
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [controller dismissViewControllerAnimated:YES completion:^{
            [[AudioController sharedAudioController] startProcessingGraph];
        }];
    });
}

#pragma mark - PGMIDIDelegate

- (void) midi:(PGMidi*)midi sourceAdded:(PGMidiSource *)source {
    [source addDelegate:self];
}

- (void) midi:(PGMidi*)midi sourceRemoved:(PGMidiSource *)source {
}

- (void) midi:(PGMidi*)midi destinationAdded:(PGMidiDestination *)destination {
    if (! destination.isNetworkSession) {
        self.doc.lastMIDIDestinationAdded = destination;
        __weak typeof(self) weakSelf = self;
        RIButtonItem *enable = [RIButtonItem itemWithLabel:@"Enable" action:^{
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.doc.midiDestination = strongSelf.doc.lastMIDIDestinationAdded;
            strongSelf.doc.lastMIDIDestinationAdded = nil;
        }];
        self.currentAlert = [[UIAlertView alloc] initWithTitle:@"MIDI Destination Added" message:[@"\"" : destination.name : @"\" has been added."] cancelButtonItem:[RIButtonItem itemWithLabel:LinguiLocalizedString(@"OK", @"OK button")] otherButtonItems:enable, nil];
        [self.currentAlert show];
    }
}

- (void) midi:(PGMidi*)midi destinationRemoved:(PGMidiDestination *)destination {
    if (self.doc.midiDestination == destination)
        self.doc.midiDestination = nil;
    
    if (! destination.isNetworkSession) {
        [[[UIAlertView alloc] initWithTitle:@"MIDI Destination Removed" message:[@"\"" : destination.name : @"\" has been removed."] delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
    }
}

#pragma mark - PGMidiSourceDelegate

- (HarmonyWizTrack*)currentlySelectedTrack {
    if (self.doc.metadata.currentlySelectedTrackNumber < self.doc.song.musicTracks.count)
        return [self.doc.song.musicTracks objectAtIndex:self.doc.metadata.currentlySelectedTrackNumber];
    else
        return self.doc.song.harmony;
}

- (void) midiSource:(PGMidiSource*)input midiReceived:(const MIDIPacketList *)packetList {
    @autoreleasepool {
        if ([self.currentlySelectedTrack isKindOfClass:[HarmonyWizMusicTrack class]]) {
            AudioController *audioController = [AudioController sharedAudioController];
            AudioUnit sampler = [audioController samplerUnitForVoice:self.doc.metadata.currentlySelectedTrackNumber];
            HarmonyWizMusicTrack *track = (id)self.currentlySelectedTrack;
            BOOL redisplayStave = NO;
            NSInteger redisplayTrack = -1;
            BOOL updateAndRedisplayRuler = NO;
            
            const MIDIPacket *packet = &packetList->packet[0];
            for (int i = 0; i < packetList->numPackets; ++i) {
                Byte midiStatus = packet->data[0];
                Byte midiCommand = midiStatus >> 4;
                
                // handle control changes
                if (midiCommand == (kMIDIControlChange >> 4)) {
                    Byte channel = midiStatus & 0x0F;
                    Byte control = packet->data[1];
                    Byte value = packet->data[2];
                    
                    if (channel < self.doc.song.musicTracks.count) {
                        HarmonyWizMusicTrack *trackForChannel = [self.doc.song.musicTracks objectAtIndex:channel];
                        switch (control) {
                            case kMIDIControlChangeVolume:
                            {
                                float result = (value / 127.f);
                                trackForChannel.volume = result;
                                [audioController setMixerInput:channel gain:result];
                                redisplayTrack = channel;
                            }
                                break;
                            case kMIDIControlChangePan:
                            {
                                float result = ((value / 127.f) - 0.5f) * 2.f;
                                trackForChannel.pan = result;
                                [audioController setMixerInput:channel pan:result];
                                redisplayTrack = channel;
                            }
                                break;
                            case kMIDIControlModWheel1:
                            {
                                AudioUnitParameterValue mod = value;
                                [audioController setChannel:(UInt32)self.doc.metadata.currentlySelectedTrackNumber modWheel:mod];
                            }
                                break;
                            case kMIDIControlModWheel2:
                            {
                                AudioUnitParameterValue mod = value;
                                [audioController setChannel:(UInt32)self.doc.metadata.currentlySelectedTrackNumber modWheel:mod];
                            }
                                break;
                                
                            default:
//                                NSLog(@"cha %x ctr %x val %x", channel, control, value);
                                break;
                        }
                    }
                }
                
                // is it a note-on or note-off
                else if (midiCommand == (kMIDINoteOn >> 4) || midiCommand == (kMIDINoteOff >> 4)) {
//                    Byte channel = midiStatus & 0x0F;
                    Byte note = (packet->data[1] & 0x7F) + track.transposition;
                    Byte velocity = packet->data[2] & 0x7F;
                    
                    BOOL isNoteOn = (midiCommand == (kMIDINoteOn >> 4) && velocity > 0);
                    BOOL isNoteOff = (midiCommand == (kMIDINoteOff >> 4) || (midiCommand == (kMIDINoteOn >> 4) && velocity == 0));
                    
#if (! RELEASE_BUILD)
                    if (isNoteOn) {
                        [self checkNoteOnEasterEggFor:note];
                    }
#endif
                    
                    // play through
                    HWThrowIfAudioError(MusicDeviceMIDIEvent (
                                                              sampler,
                                                              midiStatus,
                                                              note,
                                                              velocity,
                                                              0));
                    
                    ////
                    // recording
                    ////
                    if (self.state == kStateRecording) {
                        
                        UInt16 beatSubs = self.doc.song.beatSubdivisions*(self.doc.song.timeSign.lowerNumeral/4);
                        
                        // get playback position in beat subdivisions
                        UInt16 quantization = self.doc.song.beatSubdivisions * (powf(2.f,2.f-self.doc.metadata.quantization)) * (self.doc.song.timeSign.lowerNumeral/4);
                        SInt32 subdiv = (SInt32)(self.doc.song.playbackPosition * beatSubs / quantization + 0.5f);
                        subdiv *= quantization;
                        
                        if (subdiv < 0) {
                            subdiv = 0;
                        }
                        
                        HarmonyWizNoteEvent *newNote = nil;
                        
                        // duration of the potential new note
                        UInt16 newNoteDuration = (SInt32)((self.doc.song.playbackPosition - self.recordCurrentNoteStartPosition) * beatSubs / quantization + 0.5f);
                        newNoteDuration *= quantization;
                        
                        if (newNoteDuration == 0) {
                            newNoteDuration = quantization;
                        }
                        
                        if (newNoteDuration > 0) {
                            // note on event
                            if (isNoteOn) {
                                
                                if (self.recordNoteOn) {
                                    // truncate previous note
                                    newNote = [[HarmonyWizNoteEvent alloc] init];
                                    [self.doc.song setEventPosition:newNote to:self.recordCurrentNoteStartSubdiv];
                                    [self.doc.song setEventDuration:newNote to:newNoteDuration];
                                    newNote.noteValue = (self.recordCurrentNote - 12);
                                    newNote.type = kNoteEventType_Harmony;
                                }
                                
                                self.recordNoteOn = YES;
                                self.recordCurrentNote = note;
                                self.recordCurrentNoteStartSubdiv = subdiv;
                                self.recordCurrentNoteStartPosition = self.doc.song.playbackPosition;
                            }
                            // note off event
                            else if (isNoteOff) {
                                if (note == self.recordCurrentNote && self.recordNoteOn) {
                                    self.recordNoteOn = NO;
                                    
                                    // create the new event
                                    newNote = [[HarmonyWizNoteEvent alloc] init];
                                    [self.doc.song setEventPosition:newNote to:self.recordCurrentNoteStartSubdiv];
                                    [self.doc.song setEventDuration:newNote to:newNoteDuration];
                                    newNote.noteValue = (note - 12);
                                    newNote.type = kNoteEventType_Harmony;
                                }
                            }
                        }
                        
                        if (newNote) {
                            if ([self.doc.song eventEndSubdivision:newNote] <= self.doc.song.finalBarSubdivision) {
                                
                                // truncate overlapped notes
                                NSArray *overlaps = [self.liveRecording objectsAtIndexes:[self.doc.song overlappingEvents:self.liveRecording forEvent:newNote]];
                                for (HarmonyWizNoteEvent *ne in overlaps) {
                                    UInt16 dur = [self.doc.song eventStartSubdivision:newNote] - [self.doc.song eventStartSubdivision:ne];
                                    if (dur > 0) {
                                        [self.doc.song setEventDuration:ne to:dur];
                                    }
                                }
                                
                                // check note is valid and if so, insert it
                                if ([self.doc.song eventEndSubdivision:newNote] <= self.doc.song.finalBarSubdivision && [self.doc.song eventEndSubdivision:newNote] > [self.doc.song eventStartSubdivision:newNote]) {
                                    
                                    // remove anything that still overlaps
                                    [self.liveRecording removeObjectsAtIndexes:[self.doc.song overlappingEvents:self.liveRecording forEvent:newNote]];
                                    
                                    // place the new note
                                    [self.liveRecording placeObject:newNote];
                                }
                                
                                redisplayStave = NO;    // nb: redrawing the stave is too slow during live recording
                            }
                        }
                    }
                    
                    ////
                    // record note ons - in stopped state only
                    ////
                    BOOL recordFromMIDI = [[NSUserDefaults standardUserDefaults] boolForKey:@"recordFromMIDI"];
                    BOOL stepRecord = ((input != nil && recordFromMIDI) || (input == nil && self.doc.metadata.stepRecord));
                    if (self.state == kStateStopped && isNoteOn && stepRecord) {
                        
                        const UInt16 beatSubs = self.doc.song.beatSubdivisions*(self.doc.song.timeSign.lowerNumeral/4);
                        
                        // get playback position in beat subdivisions
                        SInt32 subdiv = (int)(self.doc.song.playbackPosition * beatSubs + 0.5f);
                        
                        if (subdiv < 0) {
                            subdiv = 0;
                        }
                        
                        // apply granularity
                        subdiv /= (beatSubs / 8);
                        subdiv *= (beatSubs / 8);
                        
                        // create the new event
                        HarmonyWizMusicEvent *ev = [self.doc.metadata.currentToolEvent copy];
                        [self.doc.song setEventPosition:ev to:subdiv];
                        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                            HarmonyWizNoteEvent *ne = (id)ev;
                            ne.noteValue = (note - 12);
                            if (self.doc.song.isModal) {
                                const int shift = [[self.doc.song.songMode modeAccidentalTypes][imod(note,12)] intValue];
                                [ne applyNoteShift:shift];
                            }
                        }
                        
                        // check if event can be placed here
                        if ([self.doc.song eventEndSubdivision:ev] <= self.doc.song.finalBarSubdivision) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.doc.metadata.harmonyScore = nil;
                                
                                [self.doc.hwUndoManager startUndoableChanges];
                                
                                // insert into selected track
                                [track.events removeObjectsAtIndexes:[self.doc.song overlappingEvents:track.events forEvent:ev]];
                                [track.events placeObject:ev];
                                [self.doc.song addFillerRestsTo:track.events];
                                
                                // handle non-export mode
                                BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
                                if (! expertMode && [ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                                    [self.doc.song removeOtherEventsOverlappingWith:[NSArray arrayWithObject:ev]];
                                }
                                
                                // move playback indicator
                                self.doc.song.playbackPosition += ((float)[self.doc.song eventDuration:ev] / (float)beatSubs);
                                
                                // re-splice harmony
                                [self.doc.song spliceHarmony];
                                
                                [self.doc.hwUndoManager finishUndoableChangesName:@"MIDI note input"];
                            });
                            
                            redisplayStave = YES;
                            updateAndRedisplayRuler = YES;
                        }
                    }
                }
                else if (midiCommand == (kMIDICommandPitchBend >> 4)) {
//                    Byte channel = midiStatus & 0x0F;
//                    Byte valueLSB = packet->data[1];
                    Byte valueMSB = packet->data[2];
                    AudioUnitParameterValue bend = valueMSB;
                    [audioController setChannel:(UInt32)self.doc.metadata.currentlySelectedTrackNumber pitchBend:bend];
                }
                else {
//                    NSLog(@"cmd %x sta %x", midiCommand, midiStatus);
                }
                
                packet = MIDIPacketNext(packet);
            }
            
            // do UI stuff on main thread
            if (redisplayStave || redisplayTrack >= 0 || updateAndRedisplayRuler) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // redraw staves
                    if (redisplayStave) {
                        [self redrawStaves:NO];
                    }
                    
                    // update playback indicator and redraw ruler control
                    if (updateAndRedisplayRuler) {
                        [self scrollToPlaybackPosition];
                        [self.rulerControl redisplay];
                        [self.allStavesControl updatePositionIndicator];
                    }
                    
                    // track control
                    if (redisplayTrack >= 0) {
                        [[self.musicTrackControls objectAtIndex:redisplayTrack] setNeedsDisplay];
                    }
                });
            }
        }
    }
}


#pragma mark - UIScrollView delegate

- (void)matchScrollViewX:(UIScrollView *)first toScrollView:(UIScrollView *)second {
    CGPoint offset = first.contentOffset;
    offset.x = second.contentOffset.x;
    [first setContentOffset:offset];
}

- (void)matchScrollViewY:(UIScrollView *)first toScrollView:(UIScrollView *)second {
    CGPoint offset = first.contentOffset;
    offset.y = second.contentOffset.y;
    [first setContentOffset:offset];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.mainView.tracksView) {
        [self matchScrollViewY:self.mainView.stavesView toScrollView:self.mainView.tracksView];
    }
    else if (scrollView == self.mainView.stavesView) {
        [self matchScrollViewY:self.mainView.tracksView toScrollView:self.mainView.stavesView];
        [self matchScrollViewX:self.mainView.rulerView toScrollView:self.mainView.stavesView];
        [self matchScrollViewX:self.mainView.magicView toScrollView:self.mainView.stavesView];
        self.doc.metadata.scoreScroll = self.mainView.stavesView.contentOffset;
        [(HWMagicPadScrollView*)self.mainView.magicView updateLabel];
        
        // prevent vertical bounce
        if (self.mainView.stavesView.contentSize.height > self.mainView.stavesView.frame.size.height) {
            if (self.mainView.stavesView.contentOffset.y < 0) {
                [self.mainView.stavesView setContentOffset:CGPointMake(self.mainView.stavesView.contentOffset.x,0)];
            }
            
            if (self.mainView.stavesView.contentOffset.y >= self.mainView.stavesView.contentSize.height - self.mainView.stavesView.frame.size.height) {
                [self.mainView.stavesView setContentOffset:CGPointMake(self.mainView.stavesView.contentOffset.x,self.mainView.stavesView.contentSize.height - self.mainView.stavesView.frame.size.height)];
            }
        }
    }
    else if (scrollView == self.mainView.rulerView) {
        [self matchScrollViewX:self.mainView.stavesView toScrollView:self.mainView.rulerView];
        [self matchScrollViewX:self.mainView.magicView toScrollView:self.mainView.rulerView];
        [(HWMagicPadScrollView*)self.mainView.magicView updateLabel];
    }
    
    [self.allStavesControl updateVisible:YES];
    
    if (self.constraintsPopover.popoverVisible) {
        [self.constraintsPopover dismissPopoverAnimated:YES];
    }
    
//    if (self.doc.metadata.magicMode) {
//        [(HWMagicPadScrollView*)self.magicPad clearAll];
//    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
}

#pragma mark - navigation controller delegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
//    if ([viewController isKindOfClass:[UITableViewController class]]) {
//        UITableViewController *tvc = (id)viewController;
//        CGSize sz = tvc.tableView.contentSize;
//        if (self.lastPopoverShown.popoverArrowDirection == UIPopoverArrowDirectionLeft ||
//            self.lastPopoverShown.popoverArrowDirection == UIPopoverArrowDirectionRight) {
//            sz.height += 50.f;
//        }
//        sz.width = 320.f;
//        self.lastPopoverShown.popoverContentSize = sz;
//    }
}

#pragma mark - web view delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return [request.URL isEqual:[NSURL URLWithString:HWFAQURLString]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:webView];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading FAQ";
    hud.detailsLabelText = @"Please wait";
    hud.removeFromSuperViewOnHide = YES;
    hud.dimBackground = YES;
    hud.tag = HWWebViewLoadingHUDTag;
    [webView addSubview:hud];
    [hud show:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    id hud = [webView viewWithTag:HWWebViewLoadingHUDTag];
    [hud hide:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"%@", error.localizedDescription);
    recordError(error)
    
    id hud = [webView viewWithTag:HWWebViewLoadingHUDTag];
    [hud setMode:MBProgressHUDModeText];
    [hud setLabelText:@"Sorry, I was unable to load the FAQ"];
    [hud setDetailsLabelText:@"(You must be connected to the internet)"];
}

#pragma mark - popover delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if (popoverController == self.constraintsPopover) {
        [self.chordAttributesViewController popToRootViewControllerAnimated:NO];
//        self.mainView.stavesView.scrollEnabled = YES;
    }
    [self.visiblePopovers removeObject:popoverController];
    if (popoverController == self.lastPopoverShown) {
        self.lastPopoverShown = nil;
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    return YES;
}

#pragma mark - document interaction delegate

//- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
//    
//}


- (void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller
{
    self.documentInteraction = nil;
    NSLog(@"UIDocumentInteractionController dismissed");
}

#pragma mark - first responder

- (BOOL)canBecomeFirstResponder {
    return YES;
}

@end

@implementation HWSongTitleMenu

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.field.text = [HWCollectionViewController currentlyOpenDocument].song.title;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.field becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 'titl') {
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc postNotificationName:HWSongTitleNotification object:self userInfo:@{ @"title" : textField.text }];
    }
}

@end
