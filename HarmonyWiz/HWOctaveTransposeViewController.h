//
//  HWOctaveTransposeViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizTrackControl.h"

@interface HWOctaveTransposeViewController : UITableViewController
@property (nonatomic,weak) HarmonyWizTrackControl *trackControl;
@end
