//
//  HWSongMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWSongMenu.h"
#import "HWCollectionViewController.h"
#import "NSString+Concatenation.h"
#import "HWReverbMenu.h"
#import "AudioController.h"
#import "HWAppDelegate.h"
#import "HWModeMenu.h"

extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;
extern NSInteger const HWRulerTag;

@interface HWSongMenu ()

@end

@implementation HWSongMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        
        self.reverb.text = LinguiLocalizedString([[HWReverbMenu reverbNames] objectAtIndex:doc.metadata.masterReverb], @"Reverb name");
        self.quantisation.text = [@[LinguiLocalizedString(@"Whole notes",@"Note length"),LinguiLocalizedString(@"Half notes",@"Note length"),LinguiLocalizedString(@"Quarter notes",@"Note length"),LinguiLocalizedString(@"Eighth notes",@"Note length"),LinguiLocalizedString(@"Sixteenth notes",@"Note length"),LinguiLocalizedString(@"Thirty-second notes",@"Note length")] objectAtIndex:doc.metadata.quantization];
        
//        self.songTitle.text = doc.song.title;
        
        self.tempo.text = [@(doc.song.tempo) stringValue];
        
        if (doc.song.isModal) {
            self.key.text = [[HWModeMenu modeNameForID:doc.song.songMode.modeID] : @" in " : @[@"C",@"D♭",@"D",@"E♭",@"E",@"F",@"G♭",@"G",@"A♭",@"A",@"B♭",@"B"][doc.song.songMode.transposition % 12]];
        }
        else {
            if (doc.song.keySign.tonality == kTonality_Major) {
                self.key.text = [NSString stringWithFormat:LinguiLocalizedString(@"%@ major", @"key tonality"), [@[@"C",@"D♭",@"D",@"E♭",@"E",@"F",@"G♭",@"G",@"A♭",@"A",@"B♭",@"B"] objectAtIndex:doc.song.keySign.root]];
            }
            else {
                self.key.text = [NSString stringWithFormat:LinguiLocalizedString(@"%@ minor", @"key tonality"), [@[@"C",@"C#",@"D",@"E♭",@"E",@"F",@"F#",@"G",@"G#",@"A",@"B♭",@"B"] objectAtIndex:doc.song.keySign.root]];
            }
        }
        
        self.time.text = [[@(doc.song.timeSign.upperNumeral) stringValue] : @"/" : [@(doc.song.timeSign.lowerNumeral) stringValue]];
        
        NSString *singularMeasure = LinguiLocalizedString(@"%ld measure", @"singular for 'measure'");
        NSString *pluralMeasure = LinguiLocalizedString(@"%ld measures", @"plural for 'measures'");
        self.measures.text = [NSString stringWithFormat:(doc.song.finalBarNumber>1 ? pluralMeasure : singularMeasure), doc.song.finalBarNumber];
        
        if ([doc.metadata.instrumentsPresets isEqualToString:@"Custom"]) {
            self.presets.text = LinguiLocalizedString(@"Custom", @"Custom instrument preset");
        }
        else {
            self.presets.text = doc.metadata.instrumentsPresets;
        }
        
        self.loopSongSwitch.on = doc.metadata.loopingEnabled;
        
        self.masterEffectsSwitch.on = doc.metadata.masterEffectsEnabled;
        
        /*
        switch (doc.song.triadsType) {
            case kTertian:
                self.triads.text = LinguiLocalizedString(@"Tertian", @"Triad type");
                break;
            case kQuartalPerfect:
                self.triads.text = LinguiLocalizedString(@"Quartal Perfect", @"Triad type");
                break;
            case kQuartal:
                self.triads.text = LinguiLocalizedString(@"Quartal", @"Triad type");
                break;
        }
         */
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
//    self.songTitle.text = doc.song.title;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view section header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view section footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    if (cell == self.addNewTrackCell) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        BOOL enabled = (! doc.metadata.magicMode) && expertMode;
        cell.textLabel.enabled = enabled;
        cell.userInteractionEnabled = enabled;
    }
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//}

- (IBAction)loopSong:(UISwitch*)sender {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    doc.metadata.loopingEnabled = sender.on;
    
    id ruler = [self.view.window viewWithTag:HWRulerTag];
    [ruler updateBackgroundColour];
    
    [HWAppDelegate recordEvent:@"looping_toggled" segmentation:@{ @"enabled" : @(sender.on) }];
}

- (IBAction)masterEffects:(UISwitch*)sender {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    doc.metadata.masterEffectsEnabled = sender.on;
    
    AudioController *ac = [AudioController sharedAudioController];
    if (sender.on) {
        [ac enableReverb];
        [ac enableDynamicsProcessor];
        [ac applyReverb:doc.metadata.masterReverb];
    }
    else {
        [ac disableReverb];
        [ac disableDynamicsProcessor];
    }
    
    [HWAppDelegate recordEvent:@"fx_toggled" segmentation:@{ @"enabled" : @(sender.on) }];
}

@end
