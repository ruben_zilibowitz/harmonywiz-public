//
//  HWHelpViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 12/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWHelpViewController.h"
#import "NSString+Concatenation.h"
#import "UIApplication+Version.h"

NSUInteger const HWTutorialNumberingPrefixLength = 3;

NSString * const HWHelpMenuPressed = @"HWHelpMenuPressed";

enum {
    kSection_Tutorials = 0,
    kSection_Support,
    kSection_About,
    kNumSections
};

@interface HWHelpViewController ()
@property (nonatomic,strong) NSArray *tutorialNames;
@end

@implementation HWHelpViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"images/Tutorial" ofType:nil];
    self.tutorialNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return kNumSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == kSection_Tutorials) {
        return self.tutorialNames.count;
    }
    else if (section == kSection_Support) {
        return 2;
    }
    else if (section == kSection_About) {
        return 2;
    }
    
    return -1;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == kSection_Tutorials) {
        return LinguiLocalizedString(@"Tutorials", @"Help menu tutorials item");
    }
    else if (section == kSection_Support) {
        return LinguiLocalizedString(@"Support", @"Help menu support item");
    }
    else if (section == kSection_About) {
        return LinguiLocalizedString(@"About", @"Help menu about item");
    }
    
    return nil;
}

- (NSString*)tutorialNameWithoutNumber:(NSUInteger)idx {
    return LinguiLocalizedString([[self.tutorialNames objectAtIndex:idx] substringFromIndex:HWTutorialNumberingPrefixLength], @"Tutorial menu item");
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HWHelpCell" forIndexPath:indexPath];
    
    if (indexPath.section == kSection_Tutorials) {
        cell.textLabel.text = [self tutorialNameWithoutNumber:indexPath.row];
    }
    else if (indexPath.section == kSection_Support) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = LinguiLocalizedString(@"Frequently Asked Questions (FAQ)", @"Help menu FAQ item");
                break;
            case 1:
                cell.textLabel.text = LinguiLocalizedString(@"Email customer support", @"Help menu support item");
                break;
        }
    }
    else if (indexPath.section == kSection_About) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = LinguiLocalizedString(@"Version info and credits", @"Help menu credits item");
                break;
            case 1:
                cell.textLabel.text = LinguiLocalizedString(@"Privacy policy", @"Help menu privacy item");
                break;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case kSection_Tutorials:
            [[NSNotificationCenter defaultCenter] postNotificationName:HWHelpMenuPressed object:self userInfo:@{@"indexPath" : indexPath, @"name" : [self.tutorialNames objectAtIndex:indexPath.row]}];
            break;
        case kSection_Support:
            [[NSNotificationCenter defaultCenter] postNotificationName:HWHelpMenuPressed object:self userInfo:@{@"indexPath" : indexPath, @"name" : @"Support"}];
            break;
        case kSection_About:
            [[NSNotificationCenter defaultCenter] postNotificationName:HWHelpMenuPressed object:self userInfo:@{@"indexPath" : indexPath, @"name" : @"About"}];
            break;
            
        default:
            break;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == kSection_Support) {
        return LinguiLocalizedString(@"Please contact us for questions and feature requests. Do not use reviews for communication: we cannot reply there!", @"Help menu footer asking to contact us for any problems.");
    }
    
    return nil;
}

@end
