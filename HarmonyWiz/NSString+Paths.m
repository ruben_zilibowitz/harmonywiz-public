//
//  NSString+Paths.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 22/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "NSString+Paths.h"

@implementation NSString (Paths)

+ (NSString*)applicationSupportPath {
    NSString* applicationSupport = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    NSString *result = [applicationSupport stringByAppendingPathComponent:bundleIdentifier];
    return result;
}

+ (NSString*)samplerFilesPath {
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return [path stringByAppendingPathComponent:@"Sampler Files"];
}

+ (NSString*)documentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

+ (NSString*)cachesDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [paths objectAtIndex:0];
    return cachesDirectory;
}

+ (NSString*)libraryDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *dir = [paths objectAtIndex:0];
    return dir;
}

+ (NSString*)saveFilesDirectory {
    return [[NSString libraryDirectory] stringByAppendingPathComponent:@"Documents"];
}

+ (NSString*)userPresetsPath {
    return [[NSString libraryDirectory] stringByAppendingPathComponent:@"userPresets.plist"];
}

+ (NSString*)undoPointsPath {
    return [[NSString libraryDirectory] stringByAppendingPathComponent:@"UndoPoints"];
}

@end
