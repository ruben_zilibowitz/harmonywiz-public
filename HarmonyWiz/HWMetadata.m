//
//  HWMetadata.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWMetadata.h"

extern NSString * const HWExpertMode;

@implementation HWMetadata

- (id)initWithThumbnail:(UIImage *)thumbnail {
    if ((self = [super init])) {
        self.thumbnail = thumbnail;
        self.harmonyRules = [NSMutableArray array];
        self.autoMelody = 0.f;
        self.scoreScroll = CGPointZero;
        self.metronome = YES;
        self.precount = YES;
        self.isNewFile = YES;
        self.quantization = kQuantization_Eighth;
        self.notePlainDuration = kDuration_Crotchet;
        self.instrumentsPresets = @"Custom";
        self.currentToolType = kToolType_Pen;
        self.masterReverb = 2;
        self.masterEffectsEnabled = YES;
        self.keyboardType = ([[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode] ? kKeyboardType_Chromatic : kKeyboardType_Major);
        self.loopingEnabled = NO;
        self.pianoOpened = NO;
        self.pianoScrollOffsetX = 0;
    }
    return self;
}

- (id)init {
    return [self initWithThumbnail:nil];
}

#pragma mark NSCoding

#define kVersionKey @"MetadataVersion"
#define kThumbnailKey @"Thumbnail"

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt32:[encoder systemVersion] forKey:kVersionKey];
    NSData * thumbnailData = UIImagePNGRepresentation(self.thumbnail);
    [encoder encodeObject:thumbnailData forKey:kThumbnailKey];
    [encoder encodeInt32:self.currentNoteColour forKey:@"currentNoteColour"];
    [encoder encodeInt32:self.currentToolType forKey:@"currentToolType"];
    [encoder encodeInt32:(int32_t)self.noteDots forKey:@"noteDots"];
    [encoder encodeInt32:self.notePlainDuration forKey:@"notePlainDuration"];
    [encoder encodeInt32:self.quantization forKey:@"quantization"];
    [encoder encodeObject:self.currentToolEvent forKey:@"currentToolEvent"];
    [encoder encodeInteger:self.currentlySelectedTrackNumber forKey:@"currentlySelectedTrackNumber"];
    [encoder encodeFloat:self.notesPointSize forKey:@"notesPointSize"];
    [encoder encodeFloat:self.noteSpacing forKey:@"noteSpacing"];
    [encoder encodeObject:self.harmonyRules forKey:@"harmonyRules"];
    [encoder encodeObject:self.harmonyScore forKey:@"harmonyScore"];
    [encoder encodeObject:self.instrumentsPresets forKey:@"instrumentsPresets"];
    [encoder encodeInt32:self.masterReverb forKey:@"masterReverb"];
    [encoder encodeBool:self.masterEffectsEnabled forKey:@"masterEffectsEnabled"];
    [encoder encodeInt32:self.keyboardType forKey:@"keyboardType"];
    [encoder encodeInt32:self.harmonyStyle forKey:@"harmonyStyle"];
    [encoder encodeFloat:self.autoMelody forKey:@"autoMelody"];
    [encoder encodeCGPoint:self.scoreScroll forKey:@"scoreScroll"];
    [encoder encodeBool:self.magicMode forKey:@"magicMode"];
    [encoder encodeBool:self.enableTranspose forKey:@"enableTranspose"];
    [encoder encodeBool:self.stepRecord forKey:@"stepRecord"];
    [encoder encodeBool:self.metronome forKey:@"metronome"];
    [encoder encodeBool:self.precount forKey:@"precount"];
    [encoder encodeBool:self.loopingEnabled forKey:@"loopingEnabled"];
    [encoder encodeBool:self.pianoOpened forKey:@"pianoOpened"];
    [encoder encodeFloat:self.pianoScrollOffsetX forKey:@"pianoScrollOffsetX"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super init])) {
        [decoder decodeInt32ForKey:kVersionKey];
        NSData * thumbnailData = [decoder decodeObjectForKey:kThumbnailKey];
        self.thumbnail = [UIImage imageWithData:thumbnailData];
        self.currentNoteColour = [decoder decodeInt32ForKey:@"currentNoteColour"];
        self.currentToolType =[decoder decodeInt32ForKey:@"currentToolType"];
        self.noteDots = [decoder decodeInt32ForKey:@"noteDots"];
        self.notePlainDuration = [decoder decodeInt32ForKey:@"notePlainDuration"];
        self.quantization = [decoder decodeInt32ForKey:@"quantization"];
        self.currentToolEvent = [decoder decodeObjectForKey:@"currentToolEvent"];
        self.currentlySelectedTrackNumber = [decoder decodeIntegerForKey:@"currentlySelectedTrackNumber"];
        self.notesPointSize = [decoder decodeFloatForKey:@"notesPointSize"];
        self.noteSpacing = [decoder decodeFloatForKey:@"noteSpacing"];
        self.harmonyRules = [decoder decodeObjectForKey:@"harmonyRules"];
        self.harmonyScore = [decoder decodeObjectForKey:@"harmonyScore"];
        self.instrumentsPresets = [decoder decodeObjectForKey:@"instrumentsPresets"];
        self.masterReverb = [decoder decodeInt32ForKey:@"masterReverb"];
        self.masterEffectsEnabled = [decoder decodeBoolForKey:@"masterEffectsEnabled"];
        self.keyboardType = [decoder decodeInt32ForKey:@"keyboardType"];
        self.harmonyStyle = [decoder decodeInt32ForKey:@"harmonyStyle"];
        self.autoMelody = [decoder decodeFloatForKey:@"autoMelody"];
        self.scoreScroll = [decoder decodeCGPointForKey:@"scoreScroll"];
        self.magicMode = [decoder decodeBoolForKey:@"magicMode"];
        self.enableTranspose = [decoder decodeBoolForKey:@"enableTranspose"];
        self.stepRecord = [decoder decodeBoolForKey:@"stepRecord"];
        self.metronome = [decoder decodeBoolForKey:@"metronome"];
        self.precount = [decoder decodeBoolForKey:@"precount"];
        self.loopingEnabled = [decoder decodeBoolForKey:@"loopingEnabled"];
        self.pianoOpened = [decoder decodeBoolForKey:@"pianoOpened"];
        self.pianoScrollOffsetX = [decoder decodeFloatForKey:@"pianoScrollOffsetX"];
    }
    return self;
}

@end
