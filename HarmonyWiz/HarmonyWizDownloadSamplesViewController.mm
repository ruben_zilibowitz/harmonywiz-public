//
//  HarmonyWizDownloadSamplesViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/01/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HarmonyWizDownloadSamplesViewController.h"
#import "HarmonyWizAppDelegate.h"
#import "NSString+Paths.h"
#include <vector>

NSString * const HWSampleDownloadCompleteNotification = @"HWSampleDownloadComplete";

@implementation HWSampleDownload
@end

@interface HarmonyWizDownloadSamplesViewController ()
@property (nonatomic,strong) NSURLConnection *listingConnection;
@property (nonatomic,strong) NSArray *availableSamples;
@property (nonatomic,strong) NSMutableData *downloadedData;
@end

@implementation HarmonyWizDownloadSamplesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)renewListing {
    if (self.listingConnection == nil) {
        NSURLRequest *request;
        {
            NSString *postString = @"key=hidden";
            NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:NO];
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            NSString *baseurl = @"http://www.zilibowitzproductions.com/HarmonyWiz/Presets/listing.php";
            NSURL *url = [NSURL URLWithString:baseurl];
            NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
            [urlRequest setHTTPMethod: @"POST"];
            [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [urlRequest setHTTPBody:postData];
            request = urlRequest;
        }
        self.listingConnection = [NSURLConnection connectionWithRequest:request delegate:self];
        if (self.listingConnection) {
            self.downloadedData = [NSMutableData data];
            [self.listingConnection start];
        }
        else {
            NSLog(@"Error creating NSURLConnection");
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (self.downloads == nil)
        self.downloads = [NSMutableSet set];
    
    [self renewListing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = LinguiLocalizedString(@"Download samples", @"Download samples menu title");
    [self renewListing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alertWithMessage:(NSString*)msg {
    [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Error", @"Error alert title") message:msg delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
}

- (void)alertWithMessageOnMain:(NSString*)msg {
    [self performSelectorOnMainThread:@selector(alertWithMessage:)
                           withObject:msg
                        waitUntilDone:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.availableSamples) {
        return self.availableSamples.count;
    }
    else {
        return 1;
    }
}

- (void)setupCell:(UITableViewCell*)cell forIndexPath:(NSIndexPath*)indexPath {
    NSSet *result = [self.downloads objectsPassingTest:^BOOL(HWSampleDownload *dl, BOOL *stop){ return [dl.indexPath isEqual:indexPath]; }];
    
    NSDictionary *entry = [self.availableSamples objectAtIndex:indexPath.row];
    cell.textLabel.text = [[entry objectForKey:@"file"] stringByDeletingPathExtension];
    
    if (result.count == 0) {
        if ([self.currentInstrumentNames containsObject:cell.textLabel.text]) {
            cell.textLabel.enabled = NO;
            cell.detailTextLabel.enabled = NO;
            cell.userInteractionEnabled = NO;
            cell.detailTextLabel.text = @"Installed";
        }
        else {
            cell.textLabel.enabled = YES;
            cell.detailTextLabel.enabled = YES;
            cell.userInteractionEnabled = YES;
            NSNumber *filesize = [entry objectForKey:@"size"];
            float megs = filesize.unsignedLongValue / 1024.f / 1024.f;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2f Mb", megs];
        }
    }
    else {
        HWSampleDownload *dl = result.anyObject;
        cell.textLabel.enabled = NO;
        cell.detailTextLabel.enabled = NO;
        cell.userInteractionEnabled = NO;
        if (dl.presetConnection) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1f%%", 100.f * float(dl.downloadCount) / float(dl.downloadSize)];
        }
        else {
            cell.detailTextLabel.text = @"Installing";
        }
    }
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if (self.availableSamples) {
        [self setupCell:cell forIndexPath:indexPath];
    }
    else {
        cell.textLabel.text = @"Loading...";
        cell.detailTextLabel.text = nil;
        cell.textLabel.enabled = YES;
        cell.userInteractionEnabled = NO;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSString *presetName = [[[tableView cellForRowAtIndexPath:indexPath] textLabel] text];
    
    // run check
    {
        NSString *path = [[NSString samplerFilesPath] stringByAppendingPathComponent:presetName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Error", @"Error alert title") message:@"Samples already installed" delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
            [tableView reloadData];
            return;
        }
    }
    
    NSURL *baseURL = [NSURL URLWithString:@"http://www.zilibowitzproductions.com/HarmonyWiz/Presets"];
    NSURL *fileURL = [baseURL URLByAppendingPathComponent:presetName];
    fileURL = [fileURL URLByAppendingPathExtension:@"hwzpreset"];
    
    HWSampleDownload *download = [[HWSampleDownload alloc] init];
    download.indexPath = indexPath;
    download.downloadCount = 0;
    download.downloadSize = (int32_t)[[[self.availableSamples objectAtIndex:indexPath.row] objectForKey:@"size"]
                             unsignedLongValue];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
    download.presetConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    if (download.presetConnection) {
        //        self.tableView.scrollEnabled = NO;
        
        download.downloadZipPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[presetName stringByAppendingPathExtension:@"zip"]];
        download.downloadStream = [NSOutputStream outputStreamToFileAtPath:download.downloadZipPath append:NO];
        [download.downloadStream open];
        [download.presetConnection start];
        
        [self.downloads addObject:download];
        
        [(HarmonyWizAppDelegate*)[[UIApplication sharedApplication] delegate] pushIdleTimerDisabled];
        
        [tableView reloadRowsAtIndexPaths:@[download.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else {
        NSLog(@"Error creating NSURLConnection");
        [self alertWithMessage:@"Could not connect to server"];
    }
}

- (void)handleDownloadError:(NSURLConnection*)connection {
    NSSet *result = [self.downloads objectsPassingTest:^BOOL(HWSampleDownload *dl, BOOL *stop){ return (dl.presetConnection == connection); }];
    for (HWSampleDownload *dl in result) {
        [connection cancel];
        [dl.downloadStream close];
        dl.downloadStream = nil;
        dl.presetConnection = nil;
    }
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if (connection == self.listingConnection) {
        NSLog(@"%@", error.localizedDescription);
        self.downloadedData = nil;
        self.listingConnection = nil;
    }
    else {
        NSSet *result = [self.downloads objectsPassingTest:^BOOL(HWSampleDownload *dl, BOOL *stop){ return (dl.presetConnection == connection); }];
        for (HWSampleDownload *dl in result) {
            [(HarmonyWizAppDelegate*)[[UIApplication sharedApplication] delegate] popIdleTimerDisabled];
            [dl.downloadStream close];
            dl.downloadStream = nil;
            dl.presetConnection = nil;
        }
        [self alertWithMessage:error.localizedDescription];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == self.listingConnection) {
        [self.downloadedData appendData:data];
    }
    else {
        NSSet *result = [self.downloads objectsPassingTest:^BOOL(HWSampleDownload *dl, BOOL *stop){ return (dl.presetConnection == connection); }];
        for (HWSampleDownload *dl in result) {
            std::vector<uint8_t> buf(data.length);
            [data getBytes:(void*)&buf.front() length:data.length];
            const char kOTP = 0xDE;
            for (size_t i = 0; i < data.length; i++) {
                buf[i] ^= kOTP;
            }
            
            NSInteger bytesWritten;
            NSInteger bytesWrittenSoFar = 0;
            do {
                bytesWritten = [dl.downloadStream write:&buf[bytesWrittenSoFar] maxLength:data.length - bytesWrittenSoFar];
                assert(bytesWritten != 0);
                if (bytesWritten == -1) {
                    [self handleDownloadError:connection];
                    [self alertWithMessage:@"File write error"];
                    break;
                } else {
                    bytesWrittenSoFar += bytesWritten;
                }
            } while (bytesWrittenSoFar != data.length);
            
            dl.downloadCount += (uint32_t)bytesWrittenSoFar;
            if (dl.downloadSize > 0) {
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:dl.indexPath];
                [self setupCell:cell forIndexPath:dl.indexPath];
            }
        }
    }
}

- (NSString*)createUniqueTemporaryDirectory {
    NSString* templateStr = [NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.XXXXX"];
    char temp[templateStr.length + 1];
    
    strcpy(temp, [templateStr cStringUsingEncoding:NSASCIIStringEncoding]);
    
    int fd = mkstemp(temp);
    char filename[PATH_MAX];
    if (fcntl(fd, F_GETPATH, filename) == -1) {
        NSLog(@"Could not create temporary file");
        return nil;
    }
    
    NSString *result = [NSString stringWithCString:filename encoding:NSASCIIStringEncoding];
    NSError *error;
    if (![[NSFileManager defaultManager] removeItemAtPath:result error:&error]) {
        NSLog(@"Remove temporary file error: %@", error.localizedDescription);
        return nil;
    }
    if (![[NSFileManager defaultManager] createDirectoryAtPath:result
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error]) {
        NSLog(@"Create directory error: %@", error.localizedDescription);
        return nil;
    }
    return result;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == self.listingConnection) {
        NSError *err = nil;
        NSArray *result = [NSJSONSerialization JSONObjectWithData:self.downloadedData
                                                          options:0 error:&err];
        if (err == nil) {
            self.availableSamples = result;
            [self.tableView reloadData];
        }
        else {
            NSLog(@"%@", err.localizedDescription);
        }
        
        self.downloadedData = nil;
        self.listingConnection = nil;
    }
    else {
        NSSet *result = [self.downloads objectsPassingTest:^BOOL(HWSampleDownload *dl, BOOL *stop){ return (dl.presetConnection == connection); }];
        for (HWSampleDownload *dl in result) {
            
            [dl.downloadStream close];
            //            self.tableView.scrollEnabled = YES;
            
            dl.downloadStream = nil;
            dl.presetConnection = nil;
            
            [self.tableView reloadRowsAtIndexPaths:@[dl.indexPath] withRowAnimation:UITableViewRowAnimationNone];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                @autoreleasepool {
                    [self install:dl];
                }
            });
        }
    }
}

- (void)install:(HWSampleDownload*)dl {
    NSError *error;
    NSString *dest = [self createUniqueTemporaryDirectory];
    if (dest) {
        [SSZipArchive unzipFileAtPath:dl.downloadZipPath
                        toDestination:dest
                            overwrite:YES
                             password:nil
                                error:&error
                             delegate:self];
        
        [(HarmonyWizAppDelegate*)[[UIApplication sharedApplication] delegate] popIdleTimerDisabled];
        
        if (error) {
            [self alertWithMessageOnMain:error.localizedDescription];
        }
        
        [self.downloads removeObject:dl];
        [self.tableView reloadRowsAtIndexPaths:@[dl.indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - SSZipArchive delegate

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath {
    
    NSError *error;
    
    // install into Library
    
    NSString *installLocation = [NSString samplerFilesPath];
    
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:unzippedPath error:&error];
    if (error) {
        [self alertWithMessageOnMain:error.localizedDescription];
    }
    else if (contents.count == 1) {
        NSString *instrumentName = [contents objectAtIndex:0];
        NSString *samplesDirectory = [unzippedPath stringByAppendingPathComponent:instrumentName];
        NSString *samplesDestination = [installLocation stringByAppendingPathComponent:instrumentName];
        
        // move the data into place
        [[NSFileManager defaultManager] moveItemAtPath:samplesDirectory
                                                toPath:samplesDestination
                                                 error:&error];
        if (error) {
            [self alertWithMessageOnMain:error.localizedDescription];
        }
        else {
            self.currentInstrumentNames = [self.currentInstrumentNames arrayByAddingObject:instrumentName];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:HWSampleDownloadCompleteNotification object:self userInfo:@{ @"name" : instrumentName }];
        }
    }
    else {
        [self alertWithMessageOnMain:@"Contents of unzipped file not recognised"];
    }
    
    // remove zip file
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    if (error) {
        [self alertWithMessageOnMain:error.localizedDescription];
    }
    
    // remove temp directory
    [[NSFileManager defaultManager] removeItemAtPath:unzippedPath error:&error];
    if (error) {
        [self alertWithMessageOnMain:error.localizedDescription];
    }
    
    [self.tableView reloadData];
}

@end
