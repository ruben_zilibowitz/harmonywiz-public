//
//  HWViewKeepingTop.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWViewKeepingTop.h"

@implementation HWViewKeepingTop

- (void)setViewsOnTop:(NSArray *)viewsOnTop {
    _viewsOnTop = viewsOnTop;
    [self keepTop];
}

- (void)keepTop {
    if (self.viewsOnTop) {
        for (UIView *view in self.viewsOnTop) {
            [super bringSubviewToFront:view];
        }
    }
}

- (void)addSubview:(UIView *)view {
    [super addSubview:view];
    [self keepTop];
}

- (void)bringSubviewToFront:(UIView *)view {
    [super bringSubviewToFront:view];
    [self keepTop];
}

@end
