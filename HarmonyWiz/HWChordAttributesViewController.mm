//
//  HWChordAttributesViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWChordAttributesViewController.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "HWCollectionViewController.h"
#import "NSString+Concatenation.h"
#import "HWChordRootViewController.h"

extern "C" NSString * const HWArrangeThisChordNowNotification = @"HWArrangeThisChordNowNotification";

@interface HWChordAttributesViewController ()

@end

@implementation HWChordAttributesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSValue *deg = [self.harmonyEvent.constraints objectForKey:HarmonyWizDegreeProperty];
    NSValue *type = [self.harmonyEvent.constraints objectForKey:HarmonyWizChordTypeProperty];
    NSValue *inv = [self.harmonyEvent.constraints objectForKey:HarmonyWizInversionProperty];
    NSString *leading = [self.harmonyEvent.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty];
    
    HarmonyWiz::ScaleNote chord_deg = deg.HWScaleNoteValue;
    HarmonyWiz::Figures::ChordType chord_type = type.HWChordTypeValue;
    HarmonyWiz::Figures::ChordInversion chord_inv = inv.HWChordInversionValue;
    
    if (deg != nil) {
        NSString *root = [@[@"I",@"II",@"III",@"IV",@"V",@"VI",@"VII"] objectAtIndex:chord_deg.note];
        switch (chord_deg.alteration) {
            case HarmonyWiz::ScaleNote::kAlteration_Flatten:
                root = [@"♭" : root];
                break;
            case HarmonyWiz::ScaleNote::kAlteration_Sharpen:
                root = [@"#" : root];
                break;
            case HarmonyWiz::ScaleNote::kAlteration_None:
                break;
        }
        self.root.text = root;
    }
    else {
        self.root.text = LinguiLocalizedString(@"Any", @"Cell text");
    }
    
    if (type != nil) {
        self.type.text = [@[LinguiLocalizedString(@"Triad", @"Cell text"),LinguiLocalizedString(@"Seven", @"Cell text"),LinguiLocalizedString(@"Add two", @"Cell text"),LinguiLocalizedString(@"Add four", @"Cell text"),LinguiLocalizedString(@"Add six", @"Cell text"),LinguiLocalizedString(@"Nine", @"Cell text"),LinguiLocalizedString(@"Eleven", @"Cell text"),LinguiLocalizedString(@"Thirteen", @"Cell text")] objectAtIndex:chord_type];
    }
    else {
        self.type.text = LinguiLocalizedString(@"Any", @"Cell text");
    }
    
    if (inv != nil) {
        self.inversion.text = [@[LinguiLocalizedString(@"Root position", @"Cell text"),LinguiLocalizedString(@"First inversion", @"Cell text"),LinguiLocalizedString(@"Second inversion", @"Cell text"),LinguiLocalizedString(@"Third inversion", @"Cell text"),LinguiLocalizedString(@"Fourth inversion", @"Cell text")] objectAtIndex:chord_inv];
    }
    else {
        self.inversion.text = LinguiLocalizedString(@"Any", @"Cell text");
    }
    
    if ([leading isEqualToString:HarmonyWizLeadingNoteTreatmentMelodic]) {
        self.leading.text = LinguiLocalizedString(@"Melodic Minor", @"Cell text");
    }
    else if ([leading isEqualToString:HarmonyWizLeadingNoteTreatmentHarmonic]) {
        self.leading.text = LinguiLocalizedString(@"Harmonic Minor", @"Cell text");
    }
    else {
        self.leading.text = LinguiLocalizedString(@"Free", @"Cell text");
    }
    
    if ([HWCollectionViewController currentlyOpenDocument].song.keySign.tonality == kTonality_Minor) {
        self.leadingNoteCell.userInteractionEnabled = YES;
        self.leadingNoteCell.textLabel.textColor = [UIColor blackColor];
        self.leadingNoteCell.detailTextLabel.textColor = [UIColor lightGrayColor];
    }
    else {
        self.leadingNoteCell.userInteractionEnabled = NO;
        self.leadingNoteCell.textLabel.textColor = [UIColor lightGrayColor];
        self.leadingNoteCell.detailTextLabel.textColor = [UIColor lightGrayColor];
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 4) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HWArrangeThisChordNowNotification object:self.harmonyEvent];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"chordRoot"]) {
        HWChordRootViewController *crvc = (id)segue.destinationViewController;
        NSValue *deg = [self.harmonyEvent.constraints objectForKey:HarmonyWizDegreeProperty];
        HarmonyWiz::ScaleNote chord_deg = deg.HWScaleNoteValue;
        crvc.selectedRow = (deg == 0 ? 0 : chord_deg.note+chord_deg.alteration+1);
    }
}

@end
