//
//  HWSongViewController+ModalArrange.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import "HWSongViewController.h"

@interface HWSongViewController (ModalArrange)
- (void)cancelModalArrangement;
- (BOOL)arrangeModalMusic;
@end
