//
//  HWChordAttributeSubmenuViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizEvent.h"

extern NSString * const HWChordAttributeModifiedNotification;

@interface HWChordAttributeSubmenuViewController : UITableViewController

@property (nonatomic,assign) NSUInteger selectedRow;

- (void)updateConstraintsForHarmonyEvent:(HarmonyWizHarmonyEvent*)harmonyEvent;

@end
