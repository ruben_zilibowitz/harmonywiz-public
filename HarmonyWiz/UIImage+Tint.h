//
//  UIImage-Tint.h
//  Tonalis
//
//  Created by Ruben Zilibowitz on 22/07/11.
//  Copyright 2011 N.A. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Tint)

- (UIImage *)tintedImageUsingColor:(UIColor *)tintColor;

@end
