
#import "HWToolCell.h"
#import "HWToolCustomCellBackground.h"
#import "MBProgressHUD/MBProgressHUD.h"

CGFloat const HWDefaultFontSize = 36.f;
CFTimeInterval const HWLongPressDuration = 0.5;

@interface HWOutlineView : UIView
@end

@implementation HWOutlineView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(ctx, 1, 1, 1, 0.75);
    CGContextBeginPath(ctx);
    CGContextMoveToPoint(ctx, 0, 0);
    CGContextAddLineToPoint(ctx, self.bounds.size.width, 0);
    CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height);
    CGContextAddLineToPoint(ctx, 0, self.bounds.size.height);
    CGContextAddLineToPoint(ctx, 0, 0);
    
    const CGFloat inset = CGRectGetWidth(self.bounds) * 0.05f;
    CGContextMoveToPoint(ctx, inset, inset);
    CGContextAddLineToPoint(ctx, inset, self.bounds.size.height-inset);
    CGContextAddLineToPoint(ctx, self.bounds.size.width-inset, self.bounds.size.height-inset);
    CGContextAddLineToPoint(ctx, self.bounds.size.width-inset, inset);
    CGContextAddLineToPoint(ctx, inset, inset);
    CGContextFillPath(ctx);
}
@end

@interface HWToolCellView : UIView
@property (nonatomic, strong) HWToolCell *toolcell;
@end

@implementation HWToolCellView
- (id)initWithFrame:(CGRect)frame owner:(HWToolCell*)tc
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.toolcell = tc;
    }
    return self;
}
- (void)drawRect:(CGRect)rect {
    if (self.toolcell.interiorColor) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, self.toolcell.interiorColor.CGColor);
        CGFloat const insetAmount = 4;
        CGContextFillRect(context, CGRectInset(self.bounds, insetAmount, insetAmount));
    }
    if (self.toolcell.contentString) {
        UIFont *font = (self.toolcell.font ? self.toolcell.font : [UIFont fontWithName:@"AloisenNew" size:HWDefaultFontSize]);
        [self.toolcell.contentString drawInRect:self.bounds withFont:font color:[UIColor blackColor] lineBreakMode:NSLineBreakByCharWrapping alignment:NSTextAlignmentCenter verticalAlignment:self.toolcell.verticalAlignment];
    }
    if (self.toolcell.icon) {
        [self.toolcell.icon drawInRect:self.bounds];
    }
}
@end

@interface HWToolCell ()
@property (nonatomic,strong) HWOutlineView *outlineView;
@property (nonatomic,strong) MBProgressHUD *progressHUD;
@end

@implementation HWToolCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

//- (void)longPress:(UILongPressGestureRecognizer*)gesture {
//    if (gesture.state == UIGestureRecognizerStateBegan) {
//        self.progressHUD = [[MBProgressHUD alloc] initWithView:self.superview];
//        self.progressHUD.removeFromSuperViewOnHide = YES;
//        self.progressHUD.mode = MBProgressHUDModeText;
//        self.progressHUD.detailsLabelText = self.helpText;
//        self.progressHUD.dimBackground = NO;
//        self.progressHUD.animationType = MBProgressHUDAnimationFade;
//        [[[self.window subviews] objectAtIndex:0] addSubview:self.progressHUD];
//        [self.progressHUD show:YES];
//    }
//    if (gesture.state == UIGestureRecognizerStateEnded) {
//        [self.progressHUD hide:YES];
//        self.progressHUD = nil;
//    }
//}

- (void)setup {
    self.selectedBackgroundView = [[HWToolCustomCellBackground alloc] initWithFrame:CGRectZero];
    self.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    
//    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
//    longPress.minimumPressDuration = HWLongPressDuration;
//    [self.contentView addGestureRecognizer:longPress];
    
    [self.contentView addSubview:[[HWToolCellView alloc] initWithFrame:self.contentView.bounds owner:self]];
    self.backgroundColor = [UIColor whiteColor];
    self.outlineView = [[HWOutlineView alloc] initWithFrame:self.contentView.bounds];
    self.outlineView.hidden = YES;
    self.verticalAlignment = HWVerticalTextAlignmentMiddle;
    [self.contentView addSubview:self.outlineView];
}

- (void)setOutline:(BOOL)outline {
    _outline = outline;
    if (self.outline)
        self.outlineView.hidden = NO;
    else
        self.outlineView.hidden = YES;
}

@end
