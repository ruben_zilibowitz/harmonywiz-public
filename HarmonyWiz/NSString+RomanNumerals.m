//
//  NSString+RomanNumerals.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 23/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "NSString+RomanNumerals.h"

@implementation NSString (RomanNumerals)

+ (NSString *)romanNumeralsFromInt:(NSUInteger)input {
    NSUInteger i;
    NSUInteger deflate = input;
    NSString *romanValue = @"";
    NSDictionary *pairs = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"i", @"1",
                           @"x", @"10",
                           @"c", @"100",
                           @"m", @"1000",
                           @"iv", @"4",
                           @"xl", @"40",
                           @"cd", @"400",
                           @"v", @"5",
                           @"l", @"50",
                           @"d", @"500",
                           @"ix", @"9",
                           @"xc", @"90",
                           @"cm", @"900",
                           nil];
    
    NSArray *values = [NSArray arrayWithObjects:@"1000", @"900", @"500", @"400", @"100", @"90", @"50", @"40", @"10", @"9", @"5", @"4", @"1", nil];
    NSUInteger itemCount = [values count], itemValue;
    
    for (i = 0; i < itemCount; i++)
    {
        itemValue = [[values objectAtIndex:i] intValue];
        
        while (deflate >= itemValue)
        {
            romanValue = [romanValue stringByAppendingString:[pairs objectForKey:[values objectAtIndex:i]]];
            deflate -= itemValue;
        }
    }
    
    return romanValue;
}

@end
