//
//  HWPainter.h
//  ProceduralDrawing
//
//  Created by Ruben Zilibowitz on 11/02/2014.
//  Copyright (c) 2014 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

struct HWPoint {
    float x, y;
};

@interface HWPainter : NSObject <NSCoding>
- (BOOL)appendPoint:(struct HWPoint)pt;
- (void)startWithPoint:(struct HWPoint)pt;
- (void)draw;
//- (void)drawInRect:(CGRect)rect;
- (id)initWithOffscreenBuffer:(BOOL)buffer contentOffset:(CGPoint)contentOffset size:(CGSize)size;
- (void)killOffscreenBuffer;
- (BOOL)startEndWithPoint:(struct HWPoint)pt;
- (BOOL)continueEnd;

@property (nonatomic,assign) float div, speed, width, peturbAmount;
@property (nonatomic,assign,readonly) CGRect bounds, redrawRect;
@property (nonatomic,strong) NSMutableArray *midPoints, *midSpeeds;
@property (nonatomic,assign,readonly) CGContextRef offscreenCtx;
@property (nonatomic,assign,readonly) CGSize offscreenSize;
@property (nonatomic,assign,readonly) CGPoint contentOffset;
@end

struct Painter {
    float dx, dy, ax, ay, div, ease;
    
    struct HWPoint pts[5], as[5];
    struct HWPoint peturb;
    ushort ctr;
};
