//
// PPSparkleEmitterView.h
// Created by Particle Playground on 1/12/2013
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PPSparkleEmitterView : UIView
- (void)setup;
- (void)pause;
- (void)resume;
@end
