//
//  HWKeyboardTypeMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWKeyboardTypeMenu.h"
#import "HWMetadata.h"
#import "HWDocument.h"
#import "HWCollectionViewController.h"
#import "HWModeMenu.h"
#import "NSString+Concatenation.h"

NSString * const HWKeyboardTypeChangedNotification = @"HWKeyboardTypeChangedNotification";

@interface HWKeyboardTypeMenu ()

@end

@implementation HWKeyboardTypeMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kNumKeyboardTypes;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const enum Tonality tonality = doc.song.keySign.tonality;
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = LinguiLocalizedString(@"Chromatic", @"Table view text");
            cell.textLabel.textColor = [UIColor blackColor];
            cell.userInteractionEnabled = YES;
            break;
        case 1:
            cell.textLabel.text = LinguiLocalizedString(@"Major", @"Table view text");
            cell.textLabel.textColor = (!doc.song.isModal && tonality == kTonality_Major ? [UIColor blackColor] : [UIColor lightGrayColor]);
            cell.userInteractionEnabled = (tonality == kTonality_Major);
            break;
        case 2:
            cell.textLabel.text = LinguiLocalizedString(@"Natural Minor", @"Table view text");
            cell.textLabel.textColor = (!doc.song.isModal && tonality == kTonality_Minor ? [UIColor blackColor] : [UIColor lightGrayColor]);
            cell.userInteractionEnabled = (tonality == kTonality_Minor);
            break;
        case 3:
            // modal case
        {
            if (doc.song.isModal) {
                cell.textLabel.text = [[HWModeMenu modeNameForID:doc.song.songMode.modeID] : @" (modal)"];
                cell.textLabel.textColor = [UIColor blackColor];
                cell.userInteractionEnabled = YES;
            }
            else {
                cell.textLabel.text = LinguiLocalizedString(@"Modal", @"Table view text");
                cell.textLabel.textColor = [UIColor lightGrayColor];
                cell.userInteractionEnabled = NO;
            }
        }
            break;
    }
    
    cell.accessoryType = (indexPath.row == doc.metadata.keyboardType ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (indexPath.row != doc.metadata.keyboardType) {
        const enum KeyboardType oldType = doc.metadata.keyboardType;
        doc.metadata.keyboardType = (enum KeyboardType)indexPath.row;
        [tableView reloadRowsAtIndexPaths:@[indexPath,[NSIndexPath indexPathForItem:oldType inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HWKeyboardTypeChangedNotification object:self];
    }
}

@end
