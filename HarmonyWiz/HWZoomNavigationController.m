//
//  HWZoomNavigationController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 17/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWZoomNavigationController.h"
#import "HWCollectionViewController.h"
#import "HWZoomTransitionSegue.h"
#import "HWSongViewController.h"

@interface HWZoomNavigationController ()

@end

@implementation HWZoomNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

extern const CGFloat HWToolbarHeight;
extern CGFloat const HWStatusBarHeight;
extern CGFloat const HWToolboxWidth;
extern CGFloat const kKeyboardWizdomHeight;

- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
    if ([identifier isEqualToString:@"closeSong"]) {
        HWSongViewController *songViewController = (id)fromViewController;
        HWCollectionViewController *collectionViewController = (id)toViewController;
        
        HWZoomTransitionSegue *imageTransition = [[HWZoomTransitionSegue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
        
        imageTransition.unwinding = YES;
        imageTransition.transitionImage = songViewController.doc.metadata.thumbnail;
        
        if (songViewController.doc.metadata.magicMode) {
            imageTransition.sourceRect = CGRectUnion(songViewController.mainView.stavesView.frame, songViewController.mainView.magicView.frame);
        }
        else {
            CGRect rect = songViewController.mainView.stavesView.frame;
            rect.size.width -= HWToolboxWidth;
            if (songViewController.doc.metadata.pianoOpened) {
                rect.size.height += kKeyboardWizdomHeight;
            }
            imageTransition.sourceRect = rect;
        }
        
        CGRect rect = collectionViewController.zoomTransitionSourceRect;
        rect.origin.y += (HWStatusBarHeight + HWToolbarHeight);
        imageTransition.destinationRect = rect;
        
        return imageTransition;
    }
    else {
        return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
    }
}

@end
