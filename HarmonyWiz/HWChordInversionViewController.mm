//
//  HWChordInversionViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWChordInversionViewController.h"
#import "HWCollectionViewController.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "HWChordAttributesViewController.h"

@interface HWChordInversionViewController ()

@end

@implementation HWChordInversionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    HWChordAttributesViewController *cavc = [self.navigationController.viewControllers objectAtIndex:0];
    NSValue *inv = [cavc.harmonyEvent.constraints objectForKey:HarmonyWizInversionProperty];
    if (inv) {
        HarmonyWiz::Figures::ChordInversion inversion = inv.HWChordInversionValue;
        self.selectedRow = inversion+1;
    }
    else {
        self.selectedRow = 0;
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:self.selectedRow inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateConstraintsForHarmonyEvent:(HarmonyWizHarmonyEvent*)harmonyEvent {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [doc.hwUndoManager startUndoableChanges];
    [harmonyEvent.properties removeAllObjects];
    if (self.selectedRow == 0) {
        [harmonyEvent.constraints removeObjectForKey:HarmonyWizInversionProperty];
    }
    else {
        [harmonyEvent.constraints setObject:[NSValue valueWithHWChordInversion:(HarmonyWiz::Figures::ChordInversion)(self.selectedRow-1)] forKey:HarmonyWizInversionProperty];
    }
    [doc.hwUndoManager finishUndoableChangesName:@"Chord inversion"];
}

@end
