//
//  HarmonyWizClefViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HarmonyWizStaveView.h"

@interface HarmonyWizClefViewController : UITableViewController

@property (nonatomic,strong) HarmonyWizStaveView *staveView;

@end
