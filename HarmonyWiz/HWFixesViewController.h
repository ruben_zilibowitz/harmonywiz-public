//
//  HWFixesViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWFixesViewController : UITableViewController
@property (nonatomic,strong) NSArray *fixes;
@end
