//
//  Solver.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#ifndef __HarmonyWiz__Solver__
#define __HarmonyWiz__Solver__

#include <iostream>
#include <sstream>
#include <vector>
#include <list>
#include <array>
#include <tuple>
#include <bitset>
#include <algorithm>
#include <cassert>
#include "dynamicArray.h"
#include "ScaleNote.h"
#include "Figures.h"
#include "Properties.h"
#include "Monads.h"

namespace HarmonyWiz {

#error Public Repository - The code from this file is currently closed source

}

#endif /* defined(__HarmonyWiz__Solver__) */
