//
//  HarmonyWizUnarchiver.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 25/12/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizUnarchiver.h"
#import "HWPainter.h"
#include "ScaleNote.h"

NSString * const HarmonyWizVersionKey = @"harmonyWizVersion";

using namespace HarmonyWiz;

@implementation HarmonyWizUnarchiver

- (void)decodeValueOfObjCType:(const char *)type at:(void *)data {
    if (strcmp(type, @encode(ScaleNote)) == 0) {
        ScaleNote sn;
        [self decodeValueOfObjCType:@encode(int16_t) at:&sn.note];
        [self decodeValueOfObjCType:@encode(ScaleNote::ScaleNoteAlteration) at:&sn.alteration];
        
        memcpy(data, &sn, sizeof(sn));
    }
    else if (strcmp(type, @encode(struct HWPoint)) == 0) {
        struct HWPoint point;
        [self decodeValueOfObjCType:@encode(float) at:&point.x];
        [self decodeValueOfObjCType:@encode(float) at:&point.y];
        memcpy(data, &point, sizeof(struct HWPoint));
    }
    else if (strcmp(type, @encode(struct Painter)) == 0) {
        struct Painter painter;
        [self decodeValueOfObjCType:@encode(float) at:&painter.dx];
        [self decodeValueOfObjCType:@encode(float) at:&painter.dy];
        [self decodeValueOfObjCType:@encode(float) at:&painter.ax];
        [self decodeValueOfObjCType:@encode(float) at:&painter.ay];
        [self decodeValueOfObjCType:@encode(float) at:&painter.div];
        [self decodeValueOfObjCType:@encode(float) at:&painter.ease];
        for (int i = 0; i < 5; i++) {
            [self decodeValueOfObjCType:@encode(float) at:&(painter.pts[i].x)];
            [self decodeValueOfObjCType:@encode(float) at:&(painter.pts[i].y)];
        }
        for (int i = 0; i < 5; i++) {
            [self decodeValueOfObjCType:@encode(float) at:&(painter.as[i].x)];
            [self decodeValueOfObjCType:@encode(float) at:&(painter.as[i].y)];
        }
        {
            [self decodeValueOfObjCType:@encode(float) at:&(painter.peturb.x)];
            [self decodeValueOfObjCType:@encode(float) at:&(painter.peturb.y)];
        }
        [self decodeValueOfObjCType:@encode(ushort) at:&painter.ctr];
        
        memcpy(data, &painter, sizeof(painter));
    }
    else {
        [super decodeValueOfObjCType:type at:data];
    }
}

@end
