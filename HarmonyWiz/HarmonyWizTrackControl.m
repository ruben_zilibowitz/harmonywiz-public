//
//  HarmonyWizTrackControl.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HarmonyWizTrackControl.h"
#import "AudioController.h"
#import "NSString+HWVerticalAlign.h"
#import "HarmonyWizUndoManager.h"
#import "HarmonyWizSong+Splice.h"
//#import "HWSongViewController.h"
//#import "HarmonyWizStyleViewController.h"
#import "HWDocument.h"
#import "HWCollectionViewController.h"
#import "NSString+Concatenation.h"
#import "HWStavesScrollView.h"
#import "HWColourScheme.h"
#import "UIView+Recursive/UIView+Recursive.h"

NSString * const HarmonyWizSoloMuteStatusChanged = @"SoloMuteStatusChanged";
NSString * const HarmonyWizLockStatusChanged = @"LockStatusChanged";
NSString * const HarmonyWizSetInstrumentNotification = @"SetInstrument";
NSString * const HarmonyWizPressedStyle = @"HWPressedStyle";
NSString * const HarmonyWizWillSelectTrack = @"HWWillSelectTrack";
NSString * const HarmonyWizDidSelectTrack = @"HarmonyWizDidSelectTrack";
NSString * const HWTrackVolumeChanged = @"HWTrackVolumeChanged";
NSString * const HWTrackPanChanged = @"HWTrackPanChanged";
NSString * const HWBeginDraggingTrackControlNotification = @"HWBeginDraggingControl";
NSString * const HWEndDraggingTrackControlNotification = @"HWEndDraggingControl";
extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;

extern CGFloat const HWTracksViewOpenedWidth;
extern CGFloat const HWTracksViewClosedWidth;

extern NSInteger const HWAllStavesControlTag;

CGFloat const HWButtonCornerRadius = 0.f;

#define DRAW_TRACK_HEADERS  0

@interface ButtonLayerDelegate : NSObject
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx;
@property (nonatomic,strong) NSString *text;
@property (nonatomic,strong) UIFont *font;
@property (nonatomic,strong) UIColor *textColor;
@property (nonatomic,strong) UIImage *image;
@end
@implementation ButtonLayerDelegate
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    if (self.text != nil) {
        UIGraphicsPushContext(ctx);
        [self.text drawInRect:layer.bounds withFont:self.font color:self.textColor lineBreakMode:NSLineBreakByCharWrapping alignment:NSTextAlignmentCenter verticalAlignment:HWVerticalTextAlignmentMiddle];
        UIGraphicsPopContext();
    }
    else if (self.image != nil) {
        UIGraphicsPushContext(ctx);
        [self.image drawInRect:CGRectInset(layer.bounds, 2, 2)];
        UIGraphicsPopContext();
    }
}
@end

@interface GainLayerDelegate : ButtonLayerDelegate
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx;
@property (nonatomic,weak) HarmonyWizTrackControl *trackControl;
@property (nonatomic,strong) NSString *key;
@property (nonatomic,assign) BOOL isTracking;
@property (nonatomic,strong) UIColor *sliderColor;
@property (nonatomic,assign) float minValue, maxValue;
@end
@implementation GainLayerDelegate
- (void)setIsTracking:(BOOL)isTracking {
    _isTracking = isTracking;
    if (isTracking)
        [[NSNotificationCenter defaultCenter] postNotificationName:HWBeginDraggingTrackControlNotification object:self];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:HWEndDraggingTrackControlNotification object:self];
}
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    UIGraphicsPushContext(ctx);
    float value = [[self.trackControl valueForKey:self.key] floatValue];
    value = (value - self.minValue) / (self.maxValue - self.minValue);
    const CGFloat xpos = value * layer.bounds.size.width;
    CGContextSetFillColorWithColor(ctx, self.sliderColor.CGColor);
    CGContextFillRect(ctx, CGRectMake(xpos, 0, layer.bounds.size.width-xpos, layer.bounds.size.height));
    CGRect textRect = CGRectInset(layer.bounds, 8, 0);
    [self.text drawInRect:textRect withFont:self.font color:self.textColor lineBreakMode:NSLineBreakByCharWrapping alignment:NSTextAlignmentLeft verticalAlignment:HWVerticalTextAlignmentMiddle];
    NSString *percentage = [NSString stringWithFormat:@"%d%%", (int)(value * 100.f + 0.5f)];
    [percentage drawInRect:textRect withFont:self.font color:self.textColor lineBreakMode:NSLineBreakByCharWrapping alignment:NSTextAlignmentRight verticalAlignment:HWVerticalTextAlignmentMiddle];
    UIGraphicsPopContext();
}
@end

@interface HarmonyWizTrackControl ()
@property (nonatomic,assign) CGRect trackNameRect;
@property (nonatomic,strong) CALayer *muteLayer, *soloLayer, *lockLayer, *gainLayer, *panLayer;
@property (nonatomic,strong) ButtonLayerDelegate *muteLayerDelegate, *soloLayerDelegate, *lockLayerDelegate;
@property (nonatomic,strong) GainLayerDelegate *gainLayerDelegate, *panLayerDelegate;
@property (nonatomic,strong) UIColor *controlOutlineColor, *buttonColor, *buttonTextColor, *selectedColor, *unselectedColor;
@property (nonatomic,strong) CALayer *trackedLayer;
@property (nonatomic,strong) NSString *iconFilename;
@property (nonatomic,strong) UIImage *iconImage;
@property (nonatomic,strong) UIView *separatorView;
@property (nonatomic,strong) UIGestureRecognizer *singleTap, *longPress;
@end

@implementation HarmonyWizTrackControl

- (void)setupButtonLayer:(CALayer*)layer withDelegate:(ButtonLayerDelegate*)delegate withFrame:(CGRect)frame {
    layer.frame = frame;
    layer.backgroundColor = self.buttonColor.CGColor;
    layer.cornerRadius = HWButtonCornerRadius;
    layer.borderColor = self.controlOutlineColor.CGColor;
    layer.borderWidth = 0.f;
    layer.masksToBounds = YES;
    layer.contentsScale = [UIScreen mainScreen].scale;
    layer.edgeAntialiasingMask = kCALayerLeftEdge | kCALayerRightEdge | kCALayerBottomEdge | kCALayerTopEdge;
    [self.layer addSublayer:layer];
    
    UIFont *buttonFont = [UIFont fontWithName:@"GillSans-Bold" size:16.f];
    delegate.font = buttonFont;
    delegate.textColor = self.buttonTextColor;
    layer.delegate = delegate;
}

- (id)initWithFrame:(CGRect)frame trackNumber:(NSUInteger)aTrackNumber
{
    self = [super initWithFrame:frame];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.trackNumber = aTrackNumber;
        self.clipsToBounds = YES;
        
        self.trackNameRect = CGRectMake(10, 1, 150, 22);
#if DRAW_TRACK_HEADERS
        _trackLabel = [[UILabel alloc] initWithFrame:CGRectMake(HWTracksViewOpenedWidth - HWTracksViewClosedWidth + 10, 0, 100, 24)];
        self.trackLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.trackLabel.backgroundColor = [UIColor clearColor];
        self.trackLabel.userInteractionEnabled = NO;
#endif
        
        CGFloat const offset = 0;
#if DRAW_TRACK_HEADERS
        offset = 20;
#endif
        
        self.separatorView = [[UIView alloc] initWithFrame:CGRectZero];
        self.separatorView.backgroundColor = [UIColor darkGrayColor];
        self.separatorView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.separatorView];
        
        _instrumentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.instrumentLabel.backgroundColor = [UIColor clearColor];
        
        _instrumentIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.instrumentIcon.backgroundColor = [UIColor clearColor];
        
        [self updateInstrument];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(pressInstrument:)];
        longPress.delegate = self;
        longPress.minimumPressDuration = 0.25f;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapInstrument:)];
        self.instrumentLabel.userInteractionEnabled = YES;
        [self.instrumentLabel addGestureRecognizer:tap];
        [self addGestureRecognizer:longPress];
        
        self.singleTap = tap;
        self.longPress = longPress;
        
        self.instrumentIcon.translatesAutoresizingMaskIntoConstraints = NO;
        self.instrumentLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.instrumentLabel];
        [self addSubview:self.instrumentIcon];
#if DRAW_TRACK_HEADERS
        self.trackLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.trackLabel];
#endif
        
        self.controlOutlineColor = [UIColor blackColor];
        self.buttonColor = UIColorFromRGB(0x27211F);
        self.buttonTextColor = UIColorFromRGB(0xffffff);
        self.selectedColor = [UIColor orangeColor];
        self.unselectedColor = [UIColor darkGrayColor];
        
        if (! self.isHarmonyTrack) {
            CGRect muteRect = CGRectMake(10, 8 + offset, 36, 27);
            CGRect soloRect = CGRectMake(48, 8 + offset, 36, 27);
            CGRect lockRect = CGRectMake(86, 8 + offset, 36, 27);
            
            CGRect gainRect = CGRectMake(CGRectGetMinX(muteRect), CGRectGetMaxY(muteRect)+4.f, CGRectGetMaxX(lockRect)-CGRectGetMinX(muteRect), CGRectGetHeight(muteRect));
            
            CGRect panRect = CGRectApplyAffineTransform(gainRect, CGAffineTransformMakeTranslation(0, gainRect.size.height + 4));
            
            self.soloLayer = [CALayer layer];
            self.soloLayer.backgroundColor = [UIColor blackColor].CGColor;
            self.soloLayerDelegate = [[ButtonLayerDelegate alloc] init];
//            self.soloLayerDelegate.text = @"S";
            [self setupButtonLayer:self.soloLayer withDelegate:self.soloLayerDelegate withFrame:soloRect];
            
            self.muteLayer = [CALayer layer];
            self.muteLayer.backgroundColor = [UIColor blackColor].CGColor;
            self.muteLayerDelegate = [[ButtonLayerDelegate alloc] init];
//            self.muteLayerDelegate.text = @"M";
            [self setupButtonLayer:self.muteLayer withDelegate:self.muteLayerDelegate withFrame:muteRect];
            
            self.lockLayer = [CALayer layer];
            self.lockLayer.backgroundColor = [UIColor blackColor].CGColor;
            self.lockLayerDelegate = [[ButtonLayerDelegate alloc] init];
//            self.lockLayerDelegate.text = @"L";
            [self setupButtonLayer:self.lockLayer withDelegate:self.lockLayerDelegate withFrame:lockRect];
            
            self.gainLayer = [CALayer layer];
            self.gainLayerDelegate = [[GainLayerDelegate alloc] init];
            self.gainLayerDelegate.trackControl = self;
            self.gainLayerDelegate.sliderColor = UIColorFromRGB(0x676767);//UIColorFromRGB(0x33BBFF);
            self.gainLayerDelegate.text = LinguiLocalizedString(@"Volume", @"Label text");
            self.gainLayerDelegate.key = @"volume";
            self.gainLayerDelegate.minValue = 0.f;
            self.gainLayerDelegate.maxValue = 1.f;
            [self setupButtonLayer:self.gainLayer withDelegate:self.gainLayerDelegate withFrame:gainRect];
            self.gainLayerDelegate.font = [UIFont fontWithName:@"Helvetica" size:14.f];
            
            self.panLayer = [CALayer layer];
            self.panLayerDelegate = [[GainLayerDelegate alloc] init];
            self.panLayerDelegate.trackControl = self;
            self.panLayerDelegate.sliderColor = UIColorFromRGB(0x676767);
            self.panLayerDelegate.text = LinguiLocalizedString(@"Pan", @"Label text");
            self.panLayerDelegate.key = @"pan";
            self.panLayerDelegate.minValue = -1.f;
            self.panLayerDelegate.maxValue = 1.f;
            [self setupButtonLayer:self.panLayer withDelegate:self.panLayerDelegate withFrame:panRect];
            self.panLayerDelegate.font = [UIFont fontWithName:@"Helvetica" size:14.f];
        }
        
        self.backgroundColor = [UIColor whiteColor];
        [self updateSelectionVisuals];
        
        [self disableConstraintsForAllSubviews];
    }
    return self;
}

- (void)disableConstraintsForAllSubviews {
    self.instrumentLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.instrumentIcon.translatesAutoresizingMaskIntoConstraints = NO;
    self.separatorView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat const offset = 0;
#if DRAW_TRACK_HEADERS
    offset = 20;
#endif
    self.instrumentLabel.frame = CGRectMake(HWTracksViewOpenedWidth - HWTracksViewClosedWidth + 10, offset + 4, 100, 24);
    CGFloat iconSize = 80;
    self.instrumentIcon.frame = CGRectMake(CGRectGetMidX(self.instrumentLabel.frame)-iconSize*0.5f, CGRectGetMaxY(self.instrumentLabel.frame)+4, iconSize, iconSize);
    self.separatorView.frame = CGRectMake(0, self.bounds.size.height-2, self.bounds.size.width, 2);
}

- (void)addSubview:(UIView *)view {
    [super addSubview:view];
    if (self.separatorView) {
        [self bringSubviewToFront:self.separatorView];
    }
}

- (void)setNeedsDisplay {
    [super setNeedsDisplay];
    [self.soloLayer setNeedsDisplay];
    [self.muteLayer setNeedsDisplay];
    [self.lockLayer setNeedsDisplay];
    [self.gainLayer setNeedsDisplay];
    [self.panLayer setNeedsDisplay];
}

- (void)updateInstrument {
    /// Instrument name
    UIFont *labelFont = [UIFont fontWithName:@"Helvetica-Bold" size:12.f];
    self.instrumentLabel.font = labelFont;
    self.instrumentLabel.textColor = [self textColor];
    if (self.isHarmonyTrack) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        if (doc.song.isModal) {
            self.instrumentLabel.text = LinguiLocalizedString(@"Modal", @"style name");
        }
        else {
            self.instrumentLabel.text = [@[LinguiLocalizedString(@"Baroque", @"style name"), LinguiLocalizedString(@"Contemporary", @"style name"), LinguiLocalizedString(@"Smooth", @"style name"), LinguiLocalizedString(@"Free", @"style name"), LinguiLocalizedString(@"Open", @"style name")] objectAtIndex:doc.metadata.harmonyStyle];
        }
    }
    else {
        if (self.instrumentName && self.instrumentName.length > 0)
            self.instrumentLabel.text = self.instrumentName;
        else
            self.instrumentLabel.text = @"No instrument";
    }
    
    /// Instrument icon
    self.instrumentIcon.image = [self icon];
}

- (UIColor*)textColor {
    return (self.trackIsSelected ? self.selectedColor : self.unselectedColor);
}

- (BOOL)trackIsSelected {
        return ([HWCollectionViewController currentlyOpenDocument].metadata.currentlySelectedTrackNumber == self.track.trackNumber);
}

- (void)updateSelectionVisuals {
    [self updateInstrument];
    [self setNeedsDisplay];
}

- (BOOL)solo {
    return ([self.track isKindOfClass:[HarmonyWizMusicTrack class]] && ((HarmonyWizMusicTrack*)self.track).solo);
}

- (BOOL)mute {
    return ([self.track isKindOfClass:[HarmonyWizMusicTrack class]] && ((HarmonyWizMusicTrack*)self.track).mute);
}

- (BOOL)locked {
    return ([self.track isKindOfClass:[HarmonyWizMusicTrack class]] && ((HarmonyWizMusicTrack*)self.track).locked);
}

- (NSString*)instrumentName {
    if ([self.track isKindOfClass:[HarmonyWizMusicTrack class]])
        return ((HarmonyWizMusicTrack*)self.track).instrument;
    return nil;
}

- (int)transposition {
    if (! [self.track isKindOfClass:[HarmonyWizMusicTrack class]])
        [NSException raise:@"HarmonyWiz" format:@"Non music track has no transposition"];
    
    return ((HarmonyWizMusicTrack*)self.track).transposition;
}

- (void)setVolume:(float)volume {
    volume = MIN(MAX(volume,self.gainLayerDelegate.minValue),self.gainLayerDelegate.maxValue);
    if ([self.track isKindOfClass:[HarmonyWizMusicTrack class]])
        ((HarmonyWizMusicTrack*)self.track).volume = volume;
    [[AudioController sharedAudioController] setMixerInput:(UInt32)self.tag gain:volume];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HWTrackVolumeChanged object:self userInfo:@{ @"volume" : @(volume), @"channel" : @(self.tag) }];
}

- (float)volume {
    return ((HarmonyWizMusicTrack*)self.track).volume;
}

- (float)pan {
    return ((HarmonyWizMusicTrack*)self.track).pan;
}

- (void)setPan:(float)pan {
    pan = MIN(MAX(pan,self.panLayerDelegate.minValue),self.panLayerDelegate.maxValue);
    if ([self.track isKindOfClass:[HarmonyWizMusicTrack class]])
        ((HarmonyWizMusicTrack*)self.track).pan = pan;
    [[AudioController sharedAudioController] setMixerInput:(UInt32)self.tag pan:pan];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HWTrackPanChanged object:self userInfo:@{ @"pan" : @(pan), @"channel" : @(self.tag) }];
}

- (BOOL)isHarmonyTrack {
    return (self.track == [HWCollectionViewController currentlyOpenDocument].song.harmony);
}

- (HarmonyWizTrack*)track {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.song.harmony.trackNumber == self.trackNumber)
        return doc.song.harmony;
    else if (self.trackNumber < doc.song.musicTracks.count)
        return [doc.song.musicTracks objectAtIndex:self.trackNumber];
    else
        return nil;
}

- (UIImage*)icon {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (self.isHarmonyTrack) {
        NSString *str = [@"WizardHat_0" : doc.song.isModal ? @"1" : @(doc.metadata.harmonyStyle+1).stringValue];
        if (! [self.iconFilename isEqualToString:str]) {
            self.iconFilename = str;
            self.iconImage = [UIImage imageNamed:[@"images/WizardHats/" : self.iconFilename]];
        }
    }
    else {
        NSString *filename = @"018";
        
        if (self.instrumentName) {
            NSString *instrument = [self.instrumentName lowercaseString];
            if ([instrument rangeOfString:@"clarinet"].location != NSNotFound) {
                filename = @"001";
            }
            else if ([instrument rangeOfString:@"bassoon"].location != NSNotFound) {
                filename = @"002";
            }
            else if ([instrument rangeOfString:@"flute"].location != NSNotFound) {
                filename = @"003";
            }
            else if ([instrument rangeOfString:@"sax"].location != NSNotFound) {
                filename = @"004";
            }
            else if ([instrument rangeOfString:@"violin"].location != NSNotFound) {
                filename = @"007";
            }
            else if ([instrument rangeOfString:@"viola"].location != NSNotFound) {
                filename = @"007";
            }
            else if ([instrument rangeOfString:@"cello"].location != NSNotFound) {
                filename = @"006";
            }
            else if ([instrument rangeOfString:@"contrabass"].location != NSNotFound) {
                filename = @"005";
            }
            else if ([instrument rangeOfString:@"bass bowed"].location != NSNotFound) {
                filename = @"005";
            }
            else if ([instrument rangeOfString:@"bass plucked"].location != NSNotFound) {
                filename = @"005";
            }
            else if ([instrument rangeOfString:@"french horn"].location != NSNotFound) {
                filename = @"010";
            }
            else if ([instrument rangeOfString:@"tuba"].location != NSNotFound) {
                filename = @"011";
            }
            else if ([instrument rangeOfString:@"euphonium"].location != NSNotFound) {
                filename = @"011";
            }
            else if ([instrument rangeOfString:@"trumpet"].location != NSNotFound) {
                filename = @"012";
            }
            else if ([instrument rangeOfString:@"trombone"].location != NSNotFound) {
                filename = @"013";
            }
            else if ([instrument rangeOfString:@"electric guitar"].location != NSNotFound) {
                filename = @"023";
            }
            else if ([instrument rangeOfString:@"bass guitar"].location != NSNotFound) {
                filename = @"025";
            }
            else if ([instrument rangeOfString:@"guitar"].location != NSNotFound) {
                filename = @"015";
            }
            else if ([instrument rangeOfString:@"piano"].location != NSNotFound) {
                filename = @"019";
            }
            else if ([instrument rangeOfString:@"female"].location != NSNotFound) {
                filename = @"020";
            }
            else if ([instrument rangeOfString:@"male"].location != NSNotFound) {
                filename = @"021";
            }
            else if ([instrument rangeOfString:@"voice"].location != NSNotFound) {
                filename = @"021";
            }
        }
        
        if (! [filename isEqualToString:self.iconFilename]) {
            self.iconFilename = filename;
            
            // load encrypted PNG file and decrypt it
            // uses a simple XOR one time pad
            NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"gpn" inDirectory:@"images/Instruments"];
            NSMutableData *data = [NSMutableData dataWithContentsOfFile:path];
            char *buffer = data.mutableBytes;
            const char kOTP = 0xDE;
            for (size_t i = 0; i < data.length; i++) {
                buffer[i] ^= kOTP;
            }
            
            self.iconImage = [UIImage imageWithData:data];
        }
    }
    
    return self.iconImage;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // draw left panel
//    [(self.trackNumber % 2 == 0 ? UIColorFromRGB(0xE0C96B) : UIColorFromRGB(0xD9BF61)) setFill];
//    CGContextFillRect(context, CGRectMake(0, 0, 6, CGRectGetHeight(self.frame)-1));
    
    
#if DRAW_TRACK_HEADERS
    // Track header
    UIFont *labelFont = [UIFont fontWithName:@"Helvetica-Bold" size:12.f];
    if (! self.trackIsSelected) {
        CGContextSaveGState(context);
		CGFloat gradLocations[2] = {0.0f, 1.0f};
        
        CFMutableArrayRef colorArray = CFArrayCreateMutable(NULL, 2, &kCFTypeArrayCallBacks);
        CFArrayAppendValue(colorArray, UIColorFromRGB(0xAE3939).CGColor);
        CFArrayAppendValue(colorArray, UIColorFromRGB(0x813232).CGColor);
		
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGColorSpaceRelease(colorSpace);
        CGContextClipToRect(context,CGRectMake(0, 0, self.frame.size.width, 24));
		CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, colorArray, gradLocations);
        CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, 12), kCGGradientDrawsAfterEndLocation);
        CGGradientRelease(gradient);
        CFRelease(colorArray);

        CGContextRestoreGState(context);
    }
    
    self.trackLabel.font = labelFont;
    self.trackLabel.textColor = [self textColor];
    self.trackLabel.text = self.track.title;
#endif
    
    if (self.trackIsSelected) {
        CGContextSetFillColorWithColor(context, self.selectedColor.CGColor);
        CGContextFillRect(context, CGRectMake(CGRectGetWidth(self.bounds)-8, 0, 8, CGRectGetHeight(self.bounds)));
    }
}

- (void)postMuteSoloUpdate {
    [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizSoloMuteStatusChanged object:self];
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint loc = [touch locationInView:self];
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    HarmonyWizMusicTrack *musicTrack = nil;
    if ([self.track isKindOfClass:[HarmonyWizMusicTrack class]])
        musicTrack = (HarmonyWizMusicTrack*)self.track;
    
    if (CGRectContainsPoint(self.soloLayer.frame, loc) && musicTrack) {
        [doc.hwUndoManager startUndoableChanges];
        musicTrack.solo = ! musicTrack.solo;
        if (musicTrack.solo && musicTrack.mute)
            musicTrack.mute = NO;
        [doc.hwUndoManager finishUndoableChangesName:@"Solo"];
        [self updateSolo];
        [self.soloLayer setNeedsDisplay];
        [self updateMute];
        [self.muteLayer setNeedsDisplay];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{[self postMuteSoloUpdate];}];
    }
    else if (CGRectContainsPoint(self.muteLayer.frame, loc) && musicTrack) {
        [doc.hwUndoManager startUndoableChanges];
        musicTrack.mute = ! musicTrack.mute;
        [doc.hwUndoManager finishUndoableChangesName:@"Mute"];
        [self updateMute];
        [self.muteLayer setNeedsDisplay];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{[self postMuteSoloUpdate];}];
    }
    else if (CGRectContainsPoint(self.lockLayer.frame, loc) && musicTrack) {
        [doc.hwUndoManager startUndoableChanges];
        musicTrack.locked = ! musicTrack.locked;
        [doc.song spliceHarmony];
        [doc.hwUndoManager finishUndoableChangesName:@"Locked"];
        [self updateLocked];
        [self.lockLayer setNeedsDisplay];
        [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizLockStatusChanged object:self];
    }
    else if (CGRectContainsPoint(self.gainLayer.frame, loc) && musicTrack) {
        [doc.hwUndoManager startUndoableChanges];
        self.gainLayerDelegate.isTracking = YES;
        [self.gainLayer setNeedsDisplay];
        self.trackedLayer = self.gainLayer;
        return YES;
    }
    else if (CGRectContainsPoint(self.panLayer.frame, loc) && musicTrack) {
        [doc.hwUndoManager startUndoableChanges];
        self.panLayerDelegate.isTracking = YES;
        [self.panLayer setNeedsDisplay];
        self.trackedLayer = self.panLayer;
        return YES;
    }
    else {
        [self selectThisTrack];
    }
    
    return NO;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (self.trackedLayer == self.gainLayer) {
        self.gainLayerDelegate.isTracking = NO;
        [self.gainLayer setNeedsDisplay];
        [doc.hwUndoManager finishUndoableChangesName:@"Gain"];
    }
    else if (self.trackedLayer == self.panLayer) {
        self.panLayerDelegate.isTracking = NO;
        [self.panLayer setNeedsDisplay];
        [doc.hwUndoManager finishUndoableChangesName:@"Pan"];
    }
    self.trackedLayer = nil;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    if (self.trackedLayer == self.gainLayer) {
        CGPoint loc = [touch locationInView:self];
        self.volume = (loc.x - self.gainLayer.frame.origin.x) / self.gainLayer.frame.size.width * (self.gainLayerDelegate.maxValue - self.gainLayerDelegate.minValue) + self.gainLayerDelegate.minValue;
        [self.gainLayer setNeedsDisplay];
    }
    else if (self.trackedLayer == self.panLayer) {
        CGPoint loc = [touch locationInView:self];
        self.pan = (loc.x - self.panLayer.frame.origin.x) / self.panLayer.frame.size.width * (self.panLayerDelegate.maxValue - self.panLayerDelegate.minValue) + self.panLayerDelegate.minValue;
        [self.panLayer setNeedsDisplay];
    }
    return YES;
}

- (void)cancelTrackingWithEvent:(UIEvent *)event {
    [self endTrackingWithTouch:nil withEvent:event];
}

- (void)updateSolo {
    if (self.solo) {
        self.soloLayerDelegate.image = [UIImage imageNamed:@"images/MSL_Icons/Solo_Active"];
//        self.soloLayer.backgroundColor = [UIColor yellowColor].CGColor;
//        self.soloLayer.colors = nil;
//        self.soloLayerDelegate.textColor = [UIColor blackColor];
    }
    else {
        self.soloLayerDelegate.image = [UIImage imageNamed:@"images/MSL_Icons/Solo_Inactive"];
//        self.soloLayer.backgroundColor = self.buttonColor.CGColor;
//        self.soloLayerDelegate.textColor = self.buttonTextColor;
    }
}

- (void)updateMute {
    if (self.mute) {
        self.muteLayerDelegate.image = [UIImage imageNamed:@"images/MSL_Icons/Mute_Active"];
//        self.muteLayer.backgroundColor = [UIColor redColor].CGColor;
//        self.muteLayer.colors = nil;
//        self.muteLayerDelegate.textColor = [UIColor blackColor];
    }
    else {
        self.muteLayerDelegate.image = [UIImage imageNamed:@"images/MSL_Icons/Mute_Inactive"];
//        self.muteLayer.backgroundColor = self.buttonColor.CGColor;
//        self.muteLayerDelegate.textColor = self.buttonTextColor;
    }
}

- (void)updateLocked {
    if (self.locked) {
        self.lockLayerDelegate.image = [UIImage imageNamed:@"images/MSL_Icons/Lock_Active"];
//        self.lockLayer.backgroundColor = [HWColourScheme lockedAutoColour].CGColor;
//        self.lockLayer.colors = nil;
//        self.lockLayerDelegate.textColor = [UIColor blackColor];
    }
    else {
        self.lockLayerDelegate.image = [UIImage imageNamed:@"images/MSL_Icons/Lock_Inactive"];
//        self.lockLayer.backgroundColor = self.buttonColor.CGColor;
//        self.lockLayerDelegate.textColor = self.buttonTextColor;
    }
}

- (NSComparisonResult)compare:(HarmonyWizTrackControl*)control {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.song.harmony == self.track)
        return NSOrderedDescending;
    else if (doc.song.harmony == control.track)
        return NSOrderedAscending;
    else {
        NSUInteger idx1 = [doc.song.musicTracks indexOfObject:self.track];
        NSUInteger idx2 = [doc.song.musicTracks indexOfObject:control.track];
        if (idx1 == idx2)
            return NSOrderedSame;
        else if (idx1 < idx2)
            return NSOrderedAscending;
        else
            return NSOrderedDescending;
    }
}

- (void)selectThisTrack {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    if (expertMode) {
        [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizWillSelectTrack object:self userInfo:@{@"track" : self}];
        doc.metadata.currentlySelectedTrackNumber = self.track.trackNumber;
        NSArray *tcs = [self.superview.subviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"class == %@", [HarmonyWizTrackControl class]]];
        [tcs makeObjectsPerformSelector:@selector(updateSelectionVisuals)];
        //    [tcs makeObjectsPerformSelector:@selector(setNeedsDisplay)];
        [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizDidSelectTrack object:self userInfo:@{@"track" : self}];
    }
}

#pragma mark - gesture recogniser

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (self.longPress == gestureRecognizer) {
        CGPoint point = [touch locationInView:self];
        if (point.x < 0.5f*CGRectGetWidth(self.bounds) && !self.isHarmonyTrack) {
            return NO;
        }
        
        HWStavesScrollView *staves = (id)[self.window viewWithTag:HWAllStavesControlTag];
        return !staves.isEditing;
    }
    return YES;
}

- (void)pressInstrument:(UIGestureRecognizer*)gest {
    if (gest.state == UIGestureRecognizerStateBegan) {
        HarmonyWizMusicTrack *musicTrack = nil;
        if ([self.track isKindOfClass:[HarmonyWizMusicTrack class]])
            musicTrack = (HarmonyWizMusicTrack*)self.track;
        
        if (musicTrack) {
            [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizSetInstrumentNotification object:self userInfo:nil];
        }
        else if (! [[[HWCollectionViewController currentlyOpenDocument] song] isModal]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:HarmonyWizPressedStyle object:self userInfo:nil];
        }
    }
}

- (void)tapInstrument:(UITapGestureRecognizer*)gest {
    if (gest.state == UIGestureRecognizerStateEnded) {
        [self selectThisTrack];
    }
}

- (void)loadSamples:(NSString*)name {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:self.window];
    hud.removeFromSuperViewOnHide = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = LinguiLocalizedString(@"Loading", @"HUD title");
    hud.dimBackground = YES;
    [self.window addSubview:hud];
    __weak typeof(self) weakSelf = self;
    [hud showAnimated:YES whileExecutingBlock:^{
        @autoreleasepool {
            __strong typeof(self) strongSelf = weakSelf;
            [[AudioController sharedAudioController] stopProcessingGraph];
#if kAlwaysRecreateSamplers
            [[AudioController sharedAudioController] releasePresetForVoice:strongSelf.tag];
            [[AudioController sharedAudioController] createPresetForVoice:strongSelf.tag];
#endif
            BOOL result = [[AudioController sharedAudioController] createPresetFromSamplesIn:name withExtension:@"aif" forVoice:strongSelf.tag];
            if (result) {
                ((HarmonyWizMusicTrack*)strongSelf.track).instrument = name;
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[UIAlertView alloc] initWithTitle:@"Failed to load sample" message:[NSString stringWithFormat:@"There was a problem loading in the instrument samples for %@.", name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                });
            }
            [NSThread sleepForTimeInterval:0.5f];
        }
    } completionBlock:^{
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf updateInstrument];
        [[AudioController sharedAudioController] startProcessingGraph];
    }];
}

@end
