//
//  HWLeadingNoteViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 17/03/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import "HWLeadingNoteViewController.h"
#import "HWCollectionViewController.h"
#import "HWChordAttributesViewController.h"

@interface HWLeadingNoteViewController ()

@end

@implementation HWLeadingNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    HWChordAttributesViewController *cavc = [self.navigationController.viewControllers objectAtIndex:0];
    NSString *leading = [cavc.harmonyEvent.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty];
    if ([leading isEqualToString:HarmonyWizLeadingNoteTreatmentMelodic]) {
        self.selectedRow = 1;
    }
    else if ([leading isEqualToString:HarmonyWizLeadingNoteTreatmentHarmonic]) {
        self.selectedRow = 2;
    }
    else {
        self.selectedRow = 0;
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:self.selectedRow inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateConstraintsForHarmonyEvent:(HarmonyWizHarmonyEvent*)harmonyEvent {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [doc.hwUndoManager startUndoableChanges];
    [harmonyEvent.properties removeAllObjects];
    
    if (self.selectedRow == 0) {
        [harmonyEvent.constraints removeObjectForKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
    else if (self.selectedRow == 1) {
        [harmonyEvent.constraints setObject:HarmonyWizLeadingNoteTreatmentMelodic forKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
    else if (self.selectedRow == 2) {
        [harmonyEvent.constraints setObject:HarmonyWizLeadingNoteTreatmentHarmonic forKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
    [doc.hwUndoManager finishUndoableChangesName:@"Leading note treatment"];
}

@end
