//
//  HWInstrumentSelectViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 20/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>

#import "HWInstrumentSelectViewController.h"
#import "HarmonyWizTrackControl.h"
#import "HarmonyWizPurchaseSamplesViewController.h"
#import <StoreKit/StoreKit.h>

NSString * const HarmonyWizChangedInstrumentNotification = @"HWChangedInstrument";

@interface HWInstrumentSelectViewController ()
@property (nonatomic,assign) NSInteger selectedRow;
@end

@implementation HWInstrumentSelectViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.selectedRow = -1;
    self.navigationItem.title = @"Select Instruments";
//    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return self.currentInstrumentNames.count;
            break;
        case 1:
            return 1;
            break;
    }
    return -1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        cell.textLabel.text = [self.currentInstrumentNames objectAtIndex:indexPath.row];
        if (self.selectedRow == -1 && [self.trackControl.instrumentName isEqualToString:cell.textLabel.text]) {
            self.selectedRow = indexPath.row;
        }
        const BOOL rowIsSelected = (self.selectedRow == indexPath.row);
        cell.accessoryType = (rowIsSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    }
    else {
        cell.textLabel.text = LinguiLocalizedString(@"Purchase Instruments", @"Table cell title");
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1)
        return LinguiLocalizedString(@"Need more?", @"Menu header");
    return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view section footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && self.selectedRow != indexPath.row) {
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc postNotificationName:HarmonyWizChangedInstrumentNotification object:self userInfo:[NSDictionary dictionaryWithObject:[self.currentInstrumentNames objectAtIndex:indexPath.row] forKey:@"instrumentName"]];
        
        const NSInteger oldRow = self.selectedRow;
        self.selectedRow = indexPath.row;
        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:oldRow inSection:0], indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if (indexPath.section == 1) {
        if ([SKPaymentQueue canMakePayments]) {
            [self.navigationController pushViewController:self.purchaseSamplesViewController animated:YES];
            
        }
        else {
            [[[UIAlertView alloc] initWithTitle:LinguiLocalizedString(@"Purchase Instruments", @"Alert title") message:LinguiLocalizedString(@"In-app purchase is not available on this device.", @"Alert message") delegate:nil cancelButtonTitle:LinguiLocalizedString(@"OK", @"OK button") otherButtonTitles:nil] show];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
