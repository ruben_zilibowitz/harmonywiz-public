//
// PPStarsEmitterView.h
// Created by Particle Playground on 8/05/2014
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PPStarsEmitterView : UIView
- (void)setup;
- (void)pause;
- (void)resume;
@end