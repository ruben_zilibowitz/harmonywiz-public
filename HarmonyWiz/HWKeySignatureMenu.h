//
//  HWKeySignatureMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const HWKeySignatureChangedNotification;

@interface HWKeySignatureMenu : UITableViewController
@property (nonatomic,weak) IBOutlet UISegmentedControl *row1, *row2, *tonality;
@property (nonatomic,weak) IBOutlet UILabel *modeName;
@property (nonatomic,weak) IBOutlet UITableViewCell *enableTransposeCell;

- (IBAction)keyRow1:(UISegmentedControl*)sender;
- (IBAction)keyRow2:(UISegmentedControl*)sender;
- (IBAction)tonality:(UISegmentedControl*)sender;

@end
