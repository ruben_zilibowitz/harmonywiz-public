//
//  HarmonyWizSong+AutoColour.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 13/11/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong+AutoColour.h"
#import "NSMutableArray+Sorted.h"
#include "ScaleNote.h"

using namespace HarmonyWiz;

@implementation HarmonyWizSong (AutoColour)

- (void)autoColour:(NSMutableArray *)events withCadence:(BOOL)candence {
    if (self.timeSign.lowerNumeral == 4) {
        return [self autoColour:events withPattern:@[@(1)] withCadence:candence];
    }
    else if (self.timeSign.lowerNumeral == 8) {
        if (self.timeSign.upperNumeral % 3 == 0) {
            return [self autoColour:events withPattern:@[@(3)] withCadence:candence];
        }
        else if (self.timeSign.upperNumeral % 4 == 0) {
            return [self autoColour:events withPattern:@[@(2)] withCadence:candence];
        }
        else if (self.timeSign.upperNumeral == 2) {
            return [self autoColour:events withPattern:@[@(2)] withCadence:candence];
        }
        else if (self.timeSign.upperNumeral == 5) {
            return [self autoColour:events withPattern:@[@(3),@(2)] withCadence:candence];
        }
        else if (self.timeSign.upperNumeral == 7) {
            return [self autoColour:events withPattern:@[@(3),@(2),@(2)] withCadence:candence];
        }
        else if (self.timeSign.upperNumeral == 10) {
            return [self autoColour:events withPattern:@[@(3),@(3),@(2),@(2)] withCadence:candence];
        }
        else if (self.timeSign.upperNumeral == 11) {
            return [self autoColour:events withPattern:@[@(3),@(3),@(3),@(2)] withCadence:candence];
        }
    }
}

- (void)autoColour:(NSMutableArray*)events withPattern:(NSArray*)pattern withCadence:(BOOL)candence {
    NSArray *noteEvents = [events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [HarmonyWizNoteEvent class]]];
    
    // make sure sorted
    noteEvents = [noteEvents sortedArrayUsingSelector:@selector(compare:)];
    
    // truncate notes to end of song
    for (HarmonyWizNoteEvent *ne in noteEvents) {
        if ([self eventStartSubdivision:ne] >= self.finalBarSubdivision) {
            [NSException raise:@"HarmonyWiz" format:@"note starts after end of song: %@", ne];
        }
        else if ([self eventEndSubdivision:ne] > self.finalBarSubdivision) {
            [self setEventDuration:ne to:self.finalBarSubdivision - [self eventStartSubdivision:ne]];
        }
    }
    
    // apply colours pass 1
    // make everything a passing note
    for (HarmonyWizNoteEvent *ne in noteEvents) {
        ne.type = kNoteEventType_Passing;
    }
    
    // apply colours pass 2
    // make first note of each pattern block a harmony note,
    // unless there is a note ending in this block that did not start there
    for (UInt16 bar = 0; bar < self.finalBarNumber; bar++) {
        for (UInt16 idx = 0; idx < self.timeSign.upperNumeral; ) {
            for (NSNumber *num in pattern) {
                const UInt16 patternStart = (idx+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                const UInt16 patternEnd = (idx+num.unsignedIntValue+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                
                for (HarmonyWizNoteEvent *ne in noteEvents) {
                    const UInt16 endSubdiv = [self eventEndSubdivision:ne];
                    const UInt16 startSubdiv = [self eventStartSubdivision:ne];
                    if (endSubdiv > patternStart && endSubdiv <= patternEnd) {
                        if (!(startSubdiv >= patternStart && startSubdiv < patternEnd)) {
                            break;
                        }
                    }
                    if (startSubdiv >= patternStart && startSubdiv < patternEnd) {
                        ne.type = kNoteEventType_Harmony;
                        break;
                    }
                }
                idx += num.unsignedIntValue;
            }
        }
    }
    
    UInt16 maxSuspensionCount = 0;
    
    // calculate the maximum number of suspensions used in final cadence
    if (candence) {
        UInt16 cadenceLength = 0;
        for (SInt16 bar = self.finalBarNumber-1; bar >= 0; bar--) {
            for (SInt16 idx = self.timeSign.upperNumeral; idx >= 0; ) {
                for (NSNumber *num in pattern.reverseObjectEnumerator) {
                    if (cadenceLength >= 2 || cadenceLength != maxSuspensionCount)
                        goto exitMaxSuspension;
                    idx -= num.unsignedIntValue;
                    if (idx >= 0) {
                        const UInt16 patternStart = (idx+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                        const UInt16 patternEnd = (idx+num.unsignedIntValue+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                        
                        for (HarmonyWizNoteEvent *ne in noteEvents.reverseObjectEnumerator) {
                            const UInt16 startSubdiv = [self eventStartSubdivision:ne];
                            if (startSubdiv >= patternStart && startSubdiv < patternEnd) {
                                const ScaleNote sn(ne.noteValue,self.keySign.root,self.keySign.tonality);
                                const int16_t note = imod(sn.note, 7);
                                if (maxSuspensionCount == 0 && (note == 0 || note == 2 || note == 4)) {
                                    maxSuspensionCount++;
                                }
                                else if (maxSuspensionCount == 1 && (note == 4 || note == 6 || note == 1)) {
                                    maxSuspensionCount++;
                                }
                                cadenceLength++;
                                break;
                            }
                        }
                    }
                }
            }
        }
    exitMaxSuspension: ;
    }
    
    // apply colours pass 3
    // apply suspensions to final candence
    if (maxSuspensionCount > 0) {
        UInt16 suspensionCount = 0;
        for (SInt16 bar = self.finalBarNumber-1; bar >= 0; bar--) {
            for (SInt16 idx = self.timeSign.upperNumeral; idx >= 0; ) {
                for (NSNumber *num in pattern.reverseObjectEnumerator) {
                    if (suspensionCount >= maxSuspensionCount)
                        goto exitSuspension;
                    idx -= num.unsignedIntValue;
                    if (idx >= 0) {
                        const UInt16 patternStart = (idx+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                        const UInt16 patternEnd = (idx+num.unsignedIntValue+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                        
                        for (HarmonyWizNoteEvent *ne in noteEvents) {
                            const UInt16 endSubdiv = [self eventEndSubdivision:ne];
                            if (endSubdiv > patternStart && endSubdiv <= patternEnd) {
                                ne.type = kNoteEventType_Suspension;
                            }
                        }
                        for (HarmonyWizNoteEvent *ne in noteEvents.reverseObjectEnumerator) {
                            const UInt16 startSubdiv = [self eventStartSubdivision:ne];
                            if (startSubdiv >= patternStart && startSubdiv < patternEnd) {
                                ne.type = kNoteEventType_Harmony;
                                suspensionCount++;
                                break;
                            }
                        }
                    }
                }
            }
        }
    exitSuspension: ;
    }
    
    BOOL didApplyLegato = NO;
    
    // apply legato inside patterns
    // for any two adjacent notes that end inside the same pattern,
    // extend the duration of the first one so it ends at the start of the second
    for (UInt16 bar = 0; bar < self.finalBarNumber; bar++) {
        for (UInt16 idx = 0; idx < self.timeSign.upperNumeral; ) {
            for (NSNumber *num in pattern) {
                const UInt16 patternStart = (idx+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                const UInt16 patternEnd = (idx+num.unsignedIntValue+bar*self.timeSign.upperNumeral)*self.beatSubdivisions;
                
                // get all notes ending inside this pattern
                NSArray *patternNotes = [noteEvents filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id obj, NSDictionary *bindings){
                    const UInt16 endSubdiv = [self eventEndSubdivision:obj];
                    return (endSubdiv > patternStart && endSubdiv <= patternEnd);
                }]];
                for (NSUInteger jdx = 1; jdx < patternNotes.count; jdx++) {
                    HarmonyWizNoteEvent *ne = [patternNotes objectAtIndex:jdx-1];
                    HarmonyWizNoteEvent *nf = [patternNotes objectAtIndex:jdx];
                    if ([self eventEndSubdivision:ne] != [self eventStartSubdivision:nf]) {
                        UInt16 dur = ([self eventStartSubdivision:nf] - [self eventStartSubdivision:ne]);
                        [self setEventDuration:ne to:dur];
                        didApplyLegato = YES;
                    }
                }
                if (patternNotes.lastObject != nil) {
                    const UInt16 lastInPatternEndSubdiv = [self eventEndSubdivision:patternNotes.lastObject];
                    UInt16 finalSubdiv = patternEnd;
                    NSAssert(finalSubdiv >= lastInPatternEndSubdiv, @"Last note in pattern ends after end of pattern. This should never happen.");
                    for (HarmonyWizNoteEvent *ne in noteEvents) {
                        const UInt16 startSubdiv = [self eventStartSubdivision:ne];
                        if (startSubdiv >= lastInPatternEndSubdiv && startSubdiv < finalSubdiv) {
                            finalSubdiv = startSubdiv;
                            break;
                        }
                    }
                    if (lastInPatternEndSubdiv < finalSubdiv) {
                        UInt16 dur = (finalSubdiv - [self eventStartSubdivision:patternNotes.lastObject]);
                        [self setEventDuration:patternNotes.lastObject to:dur];
                        didApplyLegato = YES;
                    }
                }
                idx += num.unsignedIntValue;
            }
        }
    }
    
    if (didApplyLegato) {
        // redo all rests
        NSArray *restEvents = [events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [HarmonyWizRestEvent class]]];
        [events removeObjectsInArray:restEvents];
    }
}

@end
