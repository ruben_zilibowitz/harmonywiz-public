// Solver failure reasons
"minimum score not reached" = "I could not improve on the last arrangement";
"chord not recognised" = "This chord is not in my repertoire";
"parts are crossed" = "Some parts are crossed";
"distances between parts too great" = "Some parts are too far apart";
"clash of note alterations" = "Some notes on the same scale degree have different accidentals";
"clash with raised sixth NHT" = "Non harmonic-tones have a raised sixth that clashes with other parts";
"clash with raised seventh NHT" = "Non harmonic-tones have a raised seventh that clashes with other parts";
"clash with unraised sixth NHT" = "Non harmonic-tones have a lowered sixth that clashes with other parts";
"clash with unraised seventh NHT" = "Non harmonic-tones have a lowered seventh that clashes with other parts";
"doubling of thirds exceeded limit" = "This chord contains too many doubled thirds";
"doubling of sevenths" = "This chord's seventh is doubled";
"doubling of ninths" = "This chord's ninth is doubled";
"doubling of elevenths" = "This chord's eleventh is doubled";
"doubling of thirteenths" = "This chord's thirteenth is doubled";
"VII diminished not prepared" = "VII diminished chord is not prepared";
"VII diminished not resolved" = "VII diminished chord is not resolved";
"III augmented not prepared" = "III augmented chord is not prepared";
"III augmented not resolved" = "III augmented chord is not resolved";
"II diminished not prepared" = "II diminished chord is not prepared";
"II diminished not resolved" = "II diminished chord is not resolved";
"VI diminished not prepared" = "VI diminished chord is not prepared";
"VI diminished not resolved" = "VI diminished chord is not resolved";
"doubled fifth on VII diminished" = "The VII diminished chord must not have a doubled fifth";
"doubled fifth on III augmented" = "The III augmented chord must not have a doubled fifth";
"doubled fifth on II diminished" = "The II diminished chord must not have a doubled fifth";
"doubled fifth on VI diminished" = "The VI diminished chord must not have a doubled fifth";

"VII diminished on first beat" = "The first chord of a song should not be a VII diminished chord";
"III augmented on first beat" = "The first chord of a song should not be a III augmented chord";
"II diminished on first beat" = "The first chord of a song should not be a II diminished chord";
"VI diminished on first beat" = "The first chord of a song should not be a VI diminished chord";

"VII diminished on last beat" = "The last chord of a song should not be a VII diminished chord";
"III augmented on last beat" = "The last chord of a song should not be a III augmented chord";
"II diminished on last beat" = "The last chord of a song should not be a II diminished chord";
"VI diminished on last beat" = "The last chord of a song should not be a VI diminished chord";

"seventh on first beat" = "The first chord of a song should not be a seventh chord";
"ninth on first beat" = "The first chord of a song should not be a ninth chord";
"eleventh on first beat" = "The first chord of a song should not be an eleventh chord";
"thirteenth on first beat" = "The first chord of a song should not be a thirteenth chord";

"seventh on last beat" = "The last chord of a song should not be a seventh chord";
"ninth on last beat" = "The last chord of a song should not be a ninth chord";
"eleventh on last beat" = "The last chord of a song should not be an eleventh chord";
"thirteenth on last beat" = "The last chord of a song should not be a thirteenth chord";

"second or higher inversion on first or last beat" = "The first and last chords of a song should not be either in root position or first inversion, but not higher inversions";

"no common tones" = "These adjacent chords do not contain any common tones";
"bass must move" = "The bass note is the same in two adjacent chords";
"parts overlap" = "Some parts are overlapping";
"leap larger than a fourth in non bass part" = "There is a leap of an interval larger than one fourth in some part that is not the bass";
"leap larger than fifth and not an octave in bass" = "The bass part leaps by an interval that is larger than a perfect fifth and is not an octave";
"tritone leap" = "Tritone leap";
"parallel fifths" = "Parallel fifths";
"parallel octaves" = "Parallel octaves";
"consecutive second inversions" = "Adjacent chords should not both be in second inversion";
"chord must change" = "The same chord occurs twice in a row";
"seventh not prepared" = "Chord seventh is not prepared";
"seventh not resolved" = "Chord seventh is not resolved";
"ninth not prepared" = "Chord ninth is not prepared";
"ninth not resolved" = "Chord ninth is not resolved";
"eleventh not prepared" = "Chord eleventh is not prepared";
"eleventh not resolved" = "Chord eleventh is not resolved";
"thirteenth not prepared" = "Chord thirteenth is not prepared";
"thirteenth not resolved" = "Chord thirteenth is not resolved";
"pivot tone first rule fail" = "Pivot tone issue";
"pivot tone second rule fail" = "Pivot tone issue";
"pivot tone third rule fail" = "Pivot tone issue";
"pivot tone fourth rule fail" = "Pivot tone issue";
"cross relations" = "Cross relations";
"cross relation for NHTs" = "Cross relations involving non-harmonic tones";
"cannot use raised tone here" = "The raised scale degrees are not permitted at this point";
"VII diminished not prepared and resolved" = "VII diminished chord not prepared and resolved";
"III augmented not prepared and resolved" = "II augmented chord not prepared and resolved";
"II diminished not prepared and resolved" = "II diminished chord not prepared and resolved";
"VI diminished not prepared and resolved" = "VI diminished chord not prepared and resolved";
"seventh not prepared and resolved" = "Seventh chord not prepared and resolved";
"ninth not prepared and resolved" = "Ninth chord not prepared and resolved";
"eleventh not prepared and resolved" = "Eleventh chord not prepared and resolved";
"thirteenth not prepared and resolved" = "Thirteenth chord not prepared and resolved";
"second or higher inversion bass not prepared and resolved" = "This second or higher inversion chord is not correctly prepared and resolved";
"bass note repeated for more than 6 beats" = "Bass note is repeated for more than 6 beats";

// pre arrange checks
"No arrangable notes in selection" = "There is nothing to arrange in the current selection of notes";
"Note is outside permitted range" = "This note is outside the permitted range of the track";
"Unprepared passing note" = "This passing note is not preceded by a harmony note";
"Unresolved suspension note" = "This suspension note is not followed by a harmony note";
"Suspension note at end of track" = "The final note of a track must not be a suspension note";
"Overlapping parts" = "Two or more parts overlap";
"Greater than octave leap" = "A leap of greater than one octave occurred using harmony notes";
"Song is empty" = "The song is empty";

// User Interface
"HarmonyWiz" = "HarmonyWiz";
"None" = "None";
"Download samples" = "Download samples";
"OK" = "OK";
"Cancel" = "Cancel";
"Error" = "Error";
"Instruments" = "Instruments";
"Purchase Instruments" = "Purchase Instruments";
"Select a song" = "Select a song";
"Opening" = "Opening";
"Loading" = "Loading";
"Delete" = "Delete";
"Song" = "Song";
"Saved" = "Saved";

"Bouncing audio" = "Bouncing audio";
"Initializing" = "Initializing";
"Preparing songs" = "Preparing songs";
"Adding attachments" = "Adding attachments";
"Importing documents" = "Importing documents";
"Opening document" = "Opening document";
"My HarmonyWiz Song" = "My HarmonyWiz Song";
"Opened song" = "Opened song";

"1 song selected" = "1 song selected";
"%u songs selected" = "%u songs selected";
"Too many selected" = "Too many selected";
"Cannot duplicate more than %d songs at a time." = "Cannot duplicate more than %d songs at a time.";
"Cannot delete more than %d songs at a time." = "Cannot delete more than %d songs at a time.";
"Are you sure you want to delete the selected song? This cannot be undone." = "Are you sure you want to delete the selected song? This cannot be undone.";
"Are you sure you want to delete the selected songs? This cannot be undone." = "Are you sure you want to delete the selected songs? This cannot be undone.";

"\"%@\" is already taken. Please choose a different name." = "\"%@\" is already taken. Please choose a different name.";

"Shared HarmonyWiz Songs" = "Shared HarmonyWiz Songs";
"Shared HarmonyWiz Song" = "Shared HarmonyWiz Song";
"I would like to share this HarmonyWiz song with you." = "I would like to share this HarmonyWiz song with you.";
"I would like to share these HarmonyWiz songs with you." = "I would like to share these HarmonyWiz songs with you.";
"Open .ly files with Lilypond (http://www.lilypond.org) to convert them into scores." = "Open .ly files with Lilypond (http://www.lilypond.org) to convert them into scores.";
"You can open .mxl files in Finale, as well as several other popular music programs." = "You can open .mxl files in Finale, as well as several other popular music programs.";
"New song has been saved as \'%@\'" = "New song has been saved as \'%@\'";
"Your files have been saved to the documents directory and can be accessed from iTunes." = "Your files have been saved to the documents directory and can be accessed from iTunes.";
"You may only post one song at a time to SoundCloud." = "You may only post one song at a time to SoundCloud.";

// Tutorials
"Tutorial_Arrange_1" = "HarmonyWiz's unique arrange engine comes with different styles that change the harmonic language.\nBaroque style is the most traditional and uses simpler chord types that are not very dissonant sounding.\nGoing from Contemporary to Open the harmony rules are relaxed, resulting in more dissonant sounding chords.";
"Tutorial_Arrange_2" = "When arranging a melody in easy mode, HarmonyWiz may change your melody in some places to make it easier to harmonize.";
"Tutorial_Arrange_3" = "If you are happy with the changes you can keep them.\nOtherwise press undo to go back.\nChange to a more relaxed harmony style to allow for more complex melodies.";
"Tutorial_Arrange_4" = "Minor keys are arranged using notes from the natural minor scale by default.\n\nIf the melody uses raised sixth or seventh scale degrees then the harmony will reflect this too.";
"Tutorial_Arrange_5" = "Each track has a range of pitches that are allowed. This reflects what registers different human voices or instruments can comfortably play in.\nIf a melody does not fit within the range of its track, you can select it and do an octave transpose.";
"Tutorial_Arrange_6" = "Alternatively you can transpose the whole song to another key.";

"Tutorial_Easy_and_Expert_Modes_1" = "HarmonyWiz has been designed for people of different musical backgrounds. There are two operating modes: easy and expert.";
"Tutorial_Easy_and_Expert_Modes_2" = "The easy mode has been designed so you do not require any knowledge of music theory.\nPaint in the magic view then press arrange.\nEasy!";
"Tutorial_Easy_and_Expert_Modes_3" = "You can also edit your melody in the stave or record it with a keyboard.\nYou can change instruments, adjust the tempo, the song length and share your creations.";
"Tutorial_Easy_and_Expert_Modes_4" = "The expert mode is designed for musicians who want to explore deeper into the app. In this mode you are allowed to edit all tracks, not only the melody.";
"Tutorial_Easy_and_Expert_Modes_5" = "Add and remove tracks to harmonise in three, four, or five separate parts.";
"Tutorial_Easy_and_Expert_Modes_6" = "Change chord attributes for individual chords. You can choose a root degree, inversion, or chord extension.\n\nHarmonyWiz will try to apply these attributes when arranging the song.";
"Tutorial_Easy_and_Expert_Modes_7" = "Open the clefs menu by tapping on a clef in a track.";

"Tutorial_Overview_1" = "Compose music with only the touch of a finger in HarmonyWiz!\nPaint in the music with slow or fast gestures.";
"Tutorial_Overview_2" = "That created a melody line to get us started.\nWhen you are ready, press the HW button.";
"Tutorial_Overview_3" = "Then as if by magic your melody is arranged with harmony.\nThere are different styles available. Press and hold on the harmony track control panel to open a menu.";
"Tutorial_Overview_4" = "How about a different set of instruments? Open Ensemble presets from the Song menu. You can choose from different combinations here.";
"Tutorial_Overview_5" = "For more control over instrumentation, press and hold on a track. You can choose new sounds for individual tracks in this menu.";
"Tutorial_Overview_6" = "For musicians who want to explore further, HarmonyWiz does not disappoint. Use the toolbox on the right to edit your melodies with accuracy.\n\nWhen you are finished tap “My creations” to go to the home screen.";
"Tutorial_Overview_7" = "Here you can share and manage your songs library.\nThis tutorial covered basic usage of HarmonyWiz.\nTo learn more of what this app can do, take a look at some of the other on board tutorials.";

"Tutorial_Recording_1" = "Record allows you to input music from a keyboard.\nYou can use the built in keyboard or an external USB keyboard.";
"Tutorial_Recording_2" = "Press the keyboard button in the toolbar to toggle the built in keyboard.";
"Tutorial_Recording_3" = "Change the quantization value. HarmonyWiz will adjust your recorded note values to this resolution.";
"Tutorial_Recording_4" = "Toggle a two measure pre-count for recording using a button on the keyboard.";
"Tutorial_Recording_5" = "Step recording allows you to input notes at your own pace.\n\nSelect the desired note duration from the toolbox.\nThen pressing on the keyboard will insert a note in the selected track at the song locator position.";

"Tutorial_Toolbox_1" = "The toolbox has everything you need to write and edit the music.\nThe Hand allows you to scroll and zoom using touch gestures.";
"Tutorial_Toolbox_2" = "The Pen can add notes and rests to the stave. Select the note or rest duration from the toolbox. Then press on the stave in the right place. Drag vertically to find the right pitch.";
"Tutorial_Toolbox_3" = "Different types of notes change the way the harmony can occur. You have a choice between harmony notes, passing notes, and suspension notes.\nBy changing the note types of a phrase then pressing Arrange, you can get different results.";
"Tutorial_Toolbox_4" = "The Eraser removes notes from the staves. Simply paint over the notes you wish to remove.";
"Tutorial_Toolbox_5" = "The Select Box allows you to perform a number of different actions on notes in the staves. Drag over an area to get a popup menu of choices.\nThere are the usual cut, copy, and paste operations. You can also arrange the selection leaving the rest of the score unchanged.";
