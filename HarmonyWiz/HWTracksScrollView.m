//
//  HWTracksScrollView.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HWTracksScrollView.h"
#import "HarmonyWizTrackControl.h"
#import "UIView+Recursive/UIView+Recursive.h"

const CGFloat HWHandleWidth = 9.f;
const CGFloat HWHandleHeight = 32.f;
extern const CGFloat HWTracksViewOpenedWidth;

@interface HWDrawerHandle : UIView
@end

@implementation HWDrawerHandle

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    [[UIColorFromRGB(0x001234) colorWithAlphaComponent:0.5f] setFill];
    [[[UIColor whiteColor] colorWithAlphaComponent:0.5f] setStroke];
    CGRect handleRect = CGRectMake(CGRectGetMaxX(self.bounds)-HWHandleWidth, CGRectGetMidY(self.bounds)-HWHandleHeight*0.5f, HWHandleWidth, HWHandleHeight);
    UIBezierPath *handlePath = [UIBezierPath bezierPathWithRoundedRect:handleRect byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(4, 4)];
    [handlePath fill];
    
    CGPoint lines[] = {
        CGPointMake(CGRectGetWidth(handleRect)/3, CGRectGetMinY(handleRect)+2),
        CGPointMake(CGRectGetWidth(handleRect)/3, CGRectGetMaxY(handleRect)-2),
        CGPointMake(CGRectGetWidth(handleRect)/3*2, CGRectGetMinY(handleRect)+2),
        CGPointMake(CGRectGetWidth(handleRect)/3*2, CGRectGetMaxY(handleRect)-2)};
    CGContextStrokeLineSegments(ctx, lines, sizeof(lines)/sizeof(lines[0]));
}

@end

@interface HWTracksContentsView ()
@property (nonatomic,strong) HWDrawerHandle *handle;
@end

@implementation HWTracksContentsView

- (NSArray*)tracks {
    NSArray *unsortedTracks = [self.subviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"class == %@", [HarmonyWizTrackControl class]]];
    return [unsortedTracks sortedArrayUsingSelector:@selector(compare:)];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat cumulativeHeight = 0;
    for (HarmonyWizTrackControl *tc in self.tracks) {
        tc.frame = CGRectMake(0, cumulativeHeight, CGRectGetWidth(self.frame), tc.height);
        cumulativeHeight += tc.height;
    }
}

@end

@implementation HWTracksScrollView

- (void)setContentSize:(CGSize)contentSize {
    self.contentView.frame = CGRectMake(0, 0, contentSize.width, contentSize.height);
    [super setContentSize:contentSize];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView = [[HWTracksContentsView alloc] initWithFrame:CGRectMake(0, 0, HWTracksViewOpenedWidth, HWTracksViewOpenedWidth)];
        [self addSubview:self.contentView];
        self.contentSize = CGSizeMake(HWTracksViewOpenedWidth, HWTracksViewOpenedWidth);
        
        self.contentView.handle = [[HWDrawerHandle alloc] initWithFrame:CGRectZero];
        self.contentView.handle.opaque = NO;
        self.contentView.handle.contentMode = UIViewContentModeCenter;
        self.contentView.handle.backgroundColor = nil;
        self.contentView.handle.userInteractionEnabled = NO;
        self.contentView.handle.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.contentView.handle];
        
        self.contentView.viewsOnTop = @[self.contentView.handle];
        
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

- (NSArray*)tracks {
    return self.contentView.tracks;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect handleRect = CGRectMake(CGRectGetMaxX(self.bounds)-HWHandleWidth, CGRectGetMidY(self.bounds)-HWHandleHeight*0.5f, HWHandleWidth, HWHandleHeight);
    if (self.contentSize.height < self.bounds.size.height) {
        handleRect.origin.y = (self.contentSize.height - HWHandleHeight)*0.5f;
    }
    self.contentView.handle.frame = handleRect;
}

@end
