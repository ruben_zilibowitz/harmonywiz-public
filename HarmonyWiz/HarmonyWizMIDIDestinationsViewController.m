//
//  HarmonyWizMIDIDestinationsViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/05/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HarmonyWizMIDIDestinationsViewController.h"
#import "HWCollectionViewController.h"

@interface HarmonyWizMIDIDestinationsViewController ()
@property (nonatomic,strong) NSIndexPath *lastSelectedIndexPath;
@end

@implementation HarmonyWizMIDIDestinationsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 1;
    if (section == 1)
        return [PGMidi sharedInstance].destinations.count;
    
    return -1;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"HWCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (indexPath.section == 0) {
        cell.textLabel.text = LinguiLocalizedString(@"Don't send MIDI", @"Table cell text");
        
        cell.accessoryType = (doc.midiDestination ? UITableViewCellAccessoryNone : UITableViewCellAccessoryCheckmark);
    }
    else {
        PGMidiDestination *destination = [[PGMidi sharedInstance].destinations objectAtIndex:indexPath.row];
        
        cell.textLabel.text = destination.name;
        
        cell.accessoryType = (doc.midiDestination == destination ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    }
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        self.lastSelectedIndexPath = indexPath;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == 1)
        return LinguiLocalizedString(@"Choose which destination you want to send MIDI data to.", @"Table footer");
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (indexPath.section == 0) {
        doc.midiDestination = nil;
        [tableView cellForRowAtIndexPath:self.lastSelectedIndexPath].accessoryType = UITableViewCellAccessoryNone;
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else if (indexPath.section == 1) {
        PGMidiDestination *destination = [[PGMidi sharedInstance].destinations objectAtIndex:indexPath.row];
        doc.midiDestination = destination;
        [tableView cellForRowAtIndexPath:self.lastSelectedIndexPath].accessoryType = UITableViewCellAccessoryNone;
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    self.lastSelectedIndexPath = indexPath;
}

@end
