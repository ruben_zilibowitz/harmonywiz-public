//
//  HWFocusButtons.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 8/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWFocusButtons.h"
#import "HWCollectionViewController.h"
#import "HWDocument.h"

@interface HWFocusButtons ()
@property (nonatomic,strong) UIImageView *backgroundFrame;
@end

@implementation HWFocusButtons

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        
        self.leftbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftbutton.frame = CGRectMake(0, 0, frame.size.width / 2, frame.size.height);
        self.leftbutton.opaque = NO;
        self.leftbutton.backgroundColor = [UIColor clearColor];
        [self addSubview:self.leftbutton];
        
        self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rightButton.frame = CGRectMake(frame.size.width / 2 - 1.f, 0, frame.size.width / 2, frame.size.height);
        self.rightButton.opaque = NO;
        self.rightButton.backgroundColor = [UIColor clearColor];
        [self addSubview:self.rightButton];
        
        self.backgroundFrame = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/TopIcon/Focus/2in1_frame"]];
        self.backgroundFrame.bounds = CGRectMake(0, 0, frame.size.width, frame.size.height);
        self.backgroundFrame.opaque = NO;
        self.backgroundFrame.backgroundColor = [UIColor clearColor];
        [self addSubview:self.backgroundFrame];
        
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        self.state = (doc.metadata.magicMode ? kFocus_SingleTrack : kFocus_MultiTrack);
    }
    return self;
}

- (void)setState:(enum HWFocusState)state {
    _state = state;
    switch (state) {
        case kFocus_MultiTrack:
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/Focus/2in1_1_black"] forState:UIControlStateNormal];
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/Focus/2in1_1_grey"] forState:UIControlStateDisabled];
            [self.rightButton setImage:[UIImage imageNamed:@"images/TopIcon/Focus/2in1_2_active"] forState:UIControlStateNormal];
            break;
        case kFocus_SingleTrack:
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/Focus/2in1_1_active"] forState:UIControlStateNormal];
            [self.leftbutton setImage:[UIImage imageNamed:@"images/TopIcon/Focus/2in1_1_grey"] forState:UIControlStateDisabled];
            [self.rightButton setImage:[UIImage imageNamed:@"images/TopIcon/Focus/2in1_2_black"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

@end
