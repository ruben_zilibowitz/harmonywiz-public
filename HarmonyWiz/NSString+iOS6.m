//
//  NSString+iOS6.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/10/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "NSString+iOS6.h"

@implementation NSString (iOS6)

- (void)iOS6_drawAtPoint:(CGPoint)point withAttributes:(NSDictionary *)attrs {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
        [self drawAtPoint:point withAttributes:attrs];
    }
    else {
        [[[NSAttributedString alloc] initWithString:self attributes:attrs] drawAtPoint:point];
    }
}

- (void)iOS6_drawInRect:(CGRect)rect withAttributes:(NSDictionary *)attrs {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
        [self drawInRect:rect withAttributes:attrs];
    }
    else {
        [[[NSAttributedString alloc] initWithString:self attributes:attrs] drawInRect:rect];
    }
}

- (CGRect)iOS6_boundingRectWithSize:(CGSize)size options:(NSStringDrawingOptions)options attributes:(NSDictionary *)attributes context:(NSStringDrawingContext *)context {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7")) {
        return [self boundingRectWithSize:size options:options attributes:attributes context:context];
    }
    else {
        return [[[NSAttributedString alloc] initWithString:self attributes:attributes] boundingRectWithSize:size options:options context:context];
    }
}

@end
