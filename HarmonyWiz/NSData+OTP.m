//
//  NSData+OTP.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "NSData+OTP.h"

@implementation NSData (OTP)

- (NSData*)oneTimePad:(const int8_t)key {
    NSMutableData *data = self.mutableCopy;
    char *buffer = data.mutableBytes;
    for (size_t i = 0; i < data.length; i++) {
        buffer[i] ^= key;
    }
    return data;
}

@end
