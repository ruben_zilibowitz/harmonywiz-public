//
//  HarmonyWizTrack.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizTrack.h"
#import "HarmonyWizEvent.h"
#import "NSMutableArray+Sorted.h"

@implementation HarmonyWizTrack

- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizTrack *result = [[[self class] allocWithZone:zone] init];
    if (result) {
        result.title = [self.title copyWithZone:zone];
        result.trackNumber = self.trackNumber;
        result.events = [[NSMutableArray allocWithZone:zone] initWithArray:self.events copyItems:YES];
        result.locked = self.locked;
        result.clef = self.clef;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.trackNumber = [aDecoder decodeIntegerForKey:@"trackNumber"];
        self.events = [aDecoder decodeObjectForKey:@"events"];
        self.locked = [aDecoder decodeBoolForKey:@"locked"];
        self.clef = [aDecoder decodeInt32ForKey:@"clef"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeInteger:self.trackNumber forKey:@"trackNumber"];
    [aCoder encodeObject:self.events forKey:@"events"];
    [aCoder encodeBool:self.locked forKey:@"locked"];
    [aCoder encodeInt32:self.clef forKey:@"clef"];
}

- (HarmonyWizEvent*)eventAtBeat:(UInt32)beat subdivision:(UInt16)subdivision {
    for (HarmonyWizEvent *ev in self.events) {
        if (ev.beat == beat && ev.subdivision == subdivision)
            return ev;
    }
    return nil;
}

- (void)selectEvent:(HarmonyWizEvent*)event {
    for (HarmonyWizEvent *ev in self.events)
        ev.selected = NO;
    
    if (event) {
        event.selected = YES;
    }
}

- (BOOL)isSelected:(HarmonyWizEvent*)event {
    return event.selected;
}

- (void)selectEvents:(NSSet*)events {
    [self selectEvent:nil];
    if (events) {
        for (HarmonyWizEvent *ev in events)
            ev.selected = YES;
    }
}

- (BOOL)anythingSelected {
    for (HarmonyWizEvent *ev in self.events) {
        if (ev.selected) {
            return YES;
        }
    }
    return NO;
}

- (NSArray*)selectedEvents {
    return [self.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected == YES"]];
}

- (BOOL)isEmpty {
    for (HarmonyWizEvent *ev in self.events) {
        if (([ev isKindOfClass:[HarmonyWizHarmonyEvent class]] && !((HarmonyWizHarmonyEvent*)ev).noChord) || [ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
            return NO;
        }
    }
    return YES;
}

@end

@implementation HarmonyWizMusicTrack

- (id)init {
    self = [super init];
    if (self) {
        self.volume = 0.8f;
        self.pan = 0.f;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizMusicTrack *result = [super copyWithZone:zone];
    if (result) {
        result.lowestNoteAllowed = self.lowestNoteAllowed;
        result.highestNoteAllowed = self.highestNoteAllowed;
        result.mute = self.mute;
        result.solo = self.solo;
        result.volume = self.volume;
        result.pan = self.pan;
        result.instrument = [self.instrument copyWithZone:zone];
        result.transposition = self.transposition;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.lowestNoteAllowed = [aDecoder decodeIntForKey:@"lowestNoteAllowed"];
        self.highestNoteAllowed = [aDecoder decodeIntForKey:@"highestNoteAllowed"];
        self.mute = [aDecoder decodeBoolForKey:@"mute"];
        self.solo = [aDecoder decodeBoolForKey:@"solo"];
        self.volume = [aDecoder decodeFloatForKey:@"volume"];
        self.pan = [aDecoder decodeFloatForKey:@"pan"];
        self.instrument = [aDecoder decodeObjectForKey:@"instrument"];
        self.transposition = [aDecoder decodeIntForKey:@"transposition"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:self.lowestNoteAllowed forKey:@"lowestNoteAllowed"];
    [aCoder encodeInt:self.highestNoteAllowed forKey:@"highestNoteAllowed"];
    [aCoder encodeBool:self.mute forKey:@"mute"];
    [aCoder encodeBool:self.solo forKey:@"solo"];
    [aCoder encodeFloat:self.volume forKey:@"volume"];
    [aCoder encodeFloat:self.pan forKey:@"pan"];
    [aCoder encodeObject:self.instrument forKey:@"instrument"];
    [aCoder encodeInt:self.transposition forKey:@"transposition"];
}

- (NSComparisonResult)compare:(HarmonyWizMusicTrack*)track {
    
    if (self.lowestNoteAllowed == track.lowestNoteAllowed) {
        if (self.highestNoteAllowed == track.highestNoteAllowed) {
            return [self.title compare:track.title];
        }
        else {
            return (self.highestNoteAllowed > track.highestNoteAllowed ? NSOrderedAscending : NSOrderedDescending);
        }
    }
    else {
        return (self.lowestNoteAllowed > track.lowestNoteAllowed ? NSOrderedAscending : NSOrderedDescending);
    }
}

- (BOOL)selectionIsBelowRange {
    for (id event in self.events) {
        if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *note = event;
            if (note.selected && note.noteValue < self.lowestNoteAllowed) {
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)selectionIsAboveRange {
    for (id event in self.events) {
        if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *note = event;
            if (note.selected && note.noteValue > self.highestNoteAllowed) {
                return YES;
            }
        }
    }
    return NO;
}

- (void)selectAll {
    for (id ev in self.events) {
        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
            [ev setSelected:YES];
        }
    }
}

@end
