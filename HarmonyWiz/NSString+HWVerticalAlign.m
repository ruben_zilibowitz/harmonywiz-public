//
//  NSString+HWVerticalAlign.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/03/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "NSString+HWVerticalAlign.h"
#import "NSString+iOS6.h"

@implementation NSString (HWVerticalAlign)
- (void)drawInRect:(CGRect)rect withFont:(UIFont *)font color:(UIColor*)color verticalAlignment:(HWVerticalTextAlignment)vAlign
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    
    CGRect boundingRect = [self iOS6_boundingRectWithSize:rect.size options:0 attributes:@{ NSParagraphStyleAttributeName : paraStyle } context:nil];
    
    switch (vAlign) {
        case HWVerticalTextAlignmentTop:
            break;
            
        case HWVerticalTextAlignmentMiddle:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + ((rect.size.height - boundingRect.size.height) / 2);
            break;
            
        case HWVerticalTextAlignmentMiddleWithTail:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + ((rect.size.height - boundingRect.size.height*0.75) / 2);
            break;
            
        case HWVerticalTextAlignmentBottom:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + rect.size.height - boundingRect.size.height;
            break;
    }
    if (color == nil)
        color = [UIColor blackColor];
    [self iOS6_drawInRect:rect withAttributes:@{ NSFontAttributeName : font, NSForegroundColorAttributeName : color }];
}

- (void)drawInRect:(CGRect)rect withFont:(UIFont *)font color:(UIColor*)color lineBreakMode:(NSLineBreakMode)lineBreakMode verticalAlignment:(HWVerticalTextAlignment)vAlign
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    
    CGRect boundingRect = [self iOS6_boundingRectWithSize:rect.size options:0 attributes:@{ NSParagraphStyleAttributeName : paraStyle } context:nil];
    
    switch (vAlign) {
        case HWVerticalTextAlignmentTop:
            break;
            
        case HWVerticalTextAlignmentMiddle:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + ((rect.size.height - boundingRect.size.height) / 2);
            break;
            
        case HWVerticalTextAlignmentMiddleWithTail:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + ((rect.size.height - boundingRect.size.height*0.75) / 2);
            break;
            
        case HWVerticalTextAlignmentBottom:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + rect.size.height - boundingRect.size.height;
            break;
    }
    if (color == nil)
        color = [UIColor blackColor];
    
    [self iOS6_drawInRect:rect withAttributes:@{ NSFontAttributeName : font, NSParagraphStyleAttributeName : paraStyle, NSForegroundColorAttributeName : color }];
}

- (void)drawInRect:(CGRect)rect withFont:(UIFont *)font color:(UIColor*)color lineBreakMode:(NSLineBreakMode)lineBreakMode alignment:(NSTextAlignment)alignment verticalAlignment:(HWVerticalTextAlignment)vAlign
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = alignment;
    
    CGRect boundingRect = [self iOS6_boundingRectWithSize:rect.size options:0 attributes:@{ NSParagraphStyleAttributeName : paraStyle } context:nil];
    
    switch (vAlign) {
        case HWVerticalTextAlignmentTop:
            break;
            
        case HWVerticalTextAlignmentMiddle:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + ((rect.size.height - boundingRect.size.height) / 2);
            break;
            
        case HWVerticalTextAlignmentMiddleWithTail:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + ((rect.size.height - boundingRect.size.height*0.75) / 2);
            break;
            
        case HWVerticalTextAlignmentBottom:
            rect.origin.y = rect.origin.y + boundingRect.origin.y + rect.size.height - boundingRect.size.height;
            break;
    }
    if (color == nil)
        color = [UIColor blackColor];
    [self iOS6_drawInRect:rect withAttributes:@{ NSFontAttributeName : font, NSParagraphStyleAttributeName : paraStyle, NSForegroundColorAttributeName : color }];
}
@end
