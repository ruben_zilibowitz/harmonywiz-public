//
//  KVUndoManager.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "KVUndoManager.h"

NSString * const HWUpdateCanRedoNotification = @"HWUpdateCanRedoNotification";
NSString * const HWUpdateCanUndoNotification = @"HWUpdateCanUndoNotification";

@implementation KVUndoManager

- (void)updateProperties {
    if (self.kvCanRedo != self.canRedo) {
        _kvCanRedo = self.canRedo;
        [[NSNotificationCenter defaultCenter] postNotificationName:HWUpdateCanRedoNotification object:self];
    }
    if (self.kvCanUndo != self.canUndo) {
        _kvCanUndo = self.canUndo;
        [[NSNotificationCenter defaultCenter] postNotificationName:HWUpdateCanUndoNotification object:self];
    }
}

- (void)registerUndoWithTarget:(id)target selector:(SEL)selector object:(id)anObject {
    [super registerUndoWithTarget:target selector:selector object:anObject];
    [self updateProperties];
}

- (id)prepareWithInvocationTarget:(id)target {
    id result = [super prepareWithInvocationTarget:target];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{[self updateProperties];}];
    
    return result;
}

- (void)undo {
    [super undo];
    [self updateProperties];
}

- (void)redo {
    [super redo];
    [self updateProperties];
}

- (void)removeAllActions {
    [super removeAllActions];
    [self updateProperties];
}

@end
