//
//  HWReverbMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWReverbMenu.h"
#import "AudioController.h"
#import "HWCollectionViewController.h"
#import "HWAppDelegate.h"

@interface HWReverbMenu ()

@end

@implementation HWReverbMenu

+ (NSArray*)reverbNames {
    static NSArray *reverbNames = nil;
    if (reverbNames == nil) {
        AudioController *ac = [AudioController sharedAudioController];
        if (SYSTEM_VERSION_LESS_THAN(@"7") || ac.reverb == nil) {
            reverbNames = @[@"Small Room", @"Medium Room", @"Large Room", @"Medium Hall", @"Large Hall", @"Plate", @"Medium Chamber", @"Large Chamber", @"Cathedral", @"Large Room 2", @"Medium Hall 2", @"Medium Hall 3", @"Large Hall 2"];
        }
        else {
            reverbNames = [ac getPresetNamesFor:ac.reverb.audioUnit];
        }
    }
    
    return reverbNames;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[HWReverbMenu reverbNames] count];
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = LinguiLocalizedString([[HWReverbMenu reverbNames] objectAtIndex:indexPath.row], @"Reverb name");
    cell.accessoryType = ([HWCollectionViewController currentlyOpenDocument].metadata.masterReverb == indexPath.row ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (doc.metadata.masterReverb == indexPath.row)
        return;
    
    UITableViewCell *cell0 = [tableView cellForRowAtIndexPath:indexPath];
    UITableViewCell *cell1 = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:doc.metadata.masterReverb inSection:0]];
    
    cell0.accessoryType = UITableViewCellAccessoryCheckmark;
    cell1.accessoryType = UITableViewCellAccessoryNone;
    
    doc.metadata.masterReverb = (SInt32)(indexPath.row);
    
    [[AudioController sharedAudioController] applyReverb:doc.metadata.masterReverb];
    
    [HWAppDelegate recordEvent:@"reverb_changed" segmentation:@{ @"preset" : @(doc.metadata.masterReverb) }];
}

@end
