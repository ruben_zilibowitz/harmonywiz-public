//
//  HarmonyWizMagicControl.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 8/07/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HarmonyWizMagicControl : UIControl
@property (nonatomic) UInt16 numLines;
@property (nonatomic) UInt16 highestNote, lowestNote;
@property (nonatomic,strong) NSMutableArray *notes;
- (void)reset;
- (void)stopPlayback;
- (void)startPlayback;
@end
