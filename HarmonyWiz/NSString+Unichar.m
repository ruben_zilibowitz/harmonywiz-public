//
//  NSString+Unichar.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "NSString+Unichar.h"

@implementation NSString (Unichar)

+ (NSString*)stringWithUnichar:(unichar)ch {
    return [NSString stringWithFormat:@"%C", ch];
}

@end
