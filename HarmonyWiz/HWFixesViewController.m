//
//  HWFixesViewController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWFixesViewController.h"
#import "HWCollectionViewController.h"
#import "NSString+iOS6.h"

NSString * const HWPlaybackPositionChanged = @"HWPlaybackPositionChanged";

@interface HWFixesViewController ()

@end

@implementation HWFixesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setFixes:(NSArray *)fixes {
    _fixes = [fixes sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [[obj1 objectForKey:@"position"] compare:[obj2 objectForKey:@"position"]];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    self.navigationItem.rightBarButtonItem = done;
    
    self.navigationItem.title = LinguiLocalizedString(@"Here are the problems that were fixed", @"Dialog title");
}

- (void)done:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fixes.count;
}

- (NSString*)songPositionToString:(float)position {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    const SInt16 bar = (SInt16)(position / doc.song.timeSign.upperNumeral * (doc.song.timeSign.lowerNumeral/4.));
    const SInt16 beat = (SInt16)(fmodf(position, doc.song.timeSign.upperNumeral * (4.f/doc.song.timeSign.lowerNumeral)) * (doc.song.timeSign.lowerNumeral/4.f));
    return [NSString stringWithFormat:LinguiLocalizedString(@"Measure %d, beat %d", @"Song position"), bar+1, beat+1];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HWFixCell" forIndexPath:indexPath];
    
    NSDictionary *fix = [self.fixes objectAtIndex:indexPath.row];
    NSString *reason = [fix objectForKey:@"reason"];
    float position = [[fix objectForKey:@"position"] floatValue];
    
    cell.textLabel.text = [self songPositionToString:position];
    cell.detailTextLabel.text = reason;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    NSDictionary *fix = [self.fixes objectAtIndex:indexPath.row];
    float position = [[fix objectForKey:@"position"] floatValue];
    doc.song.playbackPosition = position;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HWPlaybackPositionChanged object:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // dynamically compute cell height
    
    NSDictionary *fix = [self.fixes objectAtIndex:indexPath.row];
    NSString *reason = [fix objectForKey:@"reason"];
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    CGRect rect = [reason iOS6_boundingRectWithSize:CGSizeMake(520.f, 9999.f)
                                       options:NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:16], NSParagraphStyleAttributeName : paraStyle }
                                       context:nil];
    
    return rect.size.height + 32.f;
}

@end
