//
//  HWSongCellSelectedBackground.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWSongCellSelectedBackground : UIView
- (id)initWithFrame:(CGRect)frame imageView:(UIView*)aImageView;
@property (nonatomic,strong) UIView* songImageView;
@end
