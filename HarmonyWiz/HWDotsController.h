//
//  HWDotsController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/09/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWDotsController : UICollectionViewController

@end

extern NSString * const HWNoteDotsNotification;
