//
//  HarmonyWizSong+Accidentals.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/08/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong.h"
#include "ScaleNote.h"

@interface HarmonyWizSong (Accidentals)

enum AccidentalType {
    kAccidental_None = 0,
    kAccidental_Sharp,
    kAccidental_Flat,
    kAccidental_Natural,
    kAccidental_DoubleSharp,
    kAccidental_DoubleFlat,
    kNumAccidentals
};

- (AccidentalType)accidentalForScaleNote:(const HarmonyWiz::ScaleNote)sn;

@end
