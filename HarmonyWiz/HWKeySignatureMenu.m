//
//  HWKeySignatureMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 15/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWKeySignatureMenu.h"
#import "HWCollectionViewController.h"
#import "HarmonyWizSong+Splice.h"
#import "HWModeMenu.h"

NSString * const HWKeySignatureChangedNotification = @"HWKeyChanged";
extern NSString * const HWModeChangedNotification;

@interface HWKeySignatureMenu ()
@end

@implementation HWKeySignatureMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
    
    // if song is empty grey out the apply with transpose button
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.song.isEmpty) {
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.userInteractionEnabled = NO;
    }
    else {
        cell.textLabel.textColor = [UIColor blackColor];
        cell.userInteractionEnabled = YES;
    }
    
    if (doc.song.isModal) {
        self.modeName.text = [HWModeMenu modeNameForID:doc.song.songMode.modeID];
    }
    else {
        self.modeName.text = (doc.song.keySign.tonality == kTonality_Minor ? LinguiLocalizedString(@"Minor", @"Tonality") : LinguiLocalizedString(@"Major", @"Tonality"));
    }
    [self updateKeyCell];
    
    self.enableTransposeCell.accessoryType = (doc.metadata.enableTranspose ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateKeyNames:(BOOL)isMajor {
    [self.row1 setTitle:(isMajor ? @"D♭" : @"C#") forSegmentAtIndex:1];
    [self.row2 setTitle:(isMajor ? @"G♭" : @"F#") forSegmentAtIndex:0];
    [self.row2 setTitle:(isMajor ? @"A♭" : @"G#") forSegmentAtIndex:2];
}

- (void)updateKeyCell {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.song.keySign == nil) {
        self.tonality.selectedSegmentIndex = -1;
        if (doc.song.songMode.transposition < 6) {
            self.row2.selectedSegmentIndex = -1;
            self.row1.selectedSegmentIndex = doc.song.songMode.transposition;
        }
        else {
            self.row1.selectedSegmentIndex = -1;
            self.row2.selectedSegmentIndex = doc.song.songMode.transposition - 6;
        }
        [self updateKeyNames:doc.song.keySign.tonality == kTonality_Major];
    }
    else {
        self.tonality.selectedSegmentIndex = doc.song.keySign.tonality;
        if (doc.song.keySign.root < 6) {
            self.row2.selectedSegmentIndex = -1;
            self.row1.selectedSegmentIndex = doc.song.keySign.root;
        }
        else {
            self.row1.selectedSegmentIndex = -1;
            self.row2.selectedSegmentIndex = doc.song.keySign.root - 6;
        }
        [self updateKeyNames:doc.song.keySign.tonality == kTonality_Major];
    }
}

- (void)doKeyChange:(HarmonyWizKeySignEvent*)theKeySign transposing:(BOOL)transpose {
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    [doc.hwUndoManager startUndoableChanges];
    
    // transpose if required
    if (transpose) {
        SInt16 trans = (theKeySign.relativeMajorRoot - doc.song.keySign.relativeMajorRoot + 24) % 12;
        if (trans > 6)
            trans -= 12;
        [doc.song transposeSongBySemitones:trans];
    }
    
    // apply new key
    doc.song.keySign = theKeySign;
    [doc.song roundNotesToDiatonic];
    
    if (theKeySign.tonality == kTonality_Major) {
        [doc.song clearLeadingNoteConstraints];
    }
    [doc.song clearHarmonyProperties];
    [doc.song applyDefaultConstraints];
    [doc.hwUndoManager finishUndoableChangesName:@"Change Key"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HWKeySignatureChangedNotification object:self];
    
    self.modeName.text = (doc.song.keySign.tonality == kTonality_Minor ? LinguiLocalizedString(@"Minor", @"Mode name") : LinguiLocalizedString(@"Major", @"Mode name"));
}

- (void)doModeChange:(HarmonyWizModeEvent*)theMode transposing:(BOOL)transpose {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    [doc.hwUndoManager startUndoableChanges];
    if (transpose) {
        SInt16 trans = (theMode.transposition - doc.song.songMode.transposition + 24) % 12;
        if (trans > 6)
            trans -= 12;
        [doc.song transposeSongBySemitones:trans];
    }
    [doc.song setSongMode:theMode];
    [doc.song roundNotesToMode];
    [doc.song clearHarmonyProperties];
    [doc.song clearHarmonyConstraints];
    [doc.hwUndoManager finishUndoableChangesName:@"Change Mode"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HWModeChangedNotification object:self];
    
    self.modeName.text = [HWModeMenu modeNameForID:theMode.modeID];
}

- (IBAction)keyRow1:(UISegmentedControl*)sender {
    self.row2.selectedSegmentIndex = -1;
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (doc.song.isModal) {
        HarmonyWizModeEvent *mode = doc.song.songMode.copy;
        mode.transposition = sender.selectedSegmentIndex;
        [self doModeChange:mode transposing:doc.metadata.enableTranspose];
    }
    else {
        if (self.tonality.selectedSegmentIndex == -1) {
            self.tonality.selectedSegmentIndex = 0;
        }
        
        HarmonyWizKeySignEvent *keySign = [HarmonyWizKeySignEvent new];
        keySign.beat = 0;
        keySign.subdivision = 0;
        keySign.root = sender.selectedSegmentIndex;
        keySign.tonality = (self.tonality.selectedSegmentIndex == 0 ? kTonality_Major : kTonality_Minor);
        [self doKeyChange:keySign transposing:doc.metadata.enableTranspose];
    }
}

- (IBAction)keyRow2:(UISegmentedControl*)sender {
    self.row1.selectedSegmentIndex = -1;
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (doc.song.isModal) {
        HarmonyWizModeEvent *mode = doc.song.songMode.copy;
        mode.transposition = sender.selectedSegmentIndex + 6;
        [self doModeChange:mode transposing:doc.metadata.enableTranspose];
    }
    else {
        if (self.tonality.selectedSegmentIndex == -1) {
            self.tonality.selectedSegmentIndex = 0;
        }
        
        HarmonyWizKeySignEvent *keySign = [HarmonyWizKeySignEvent new];
        keySign.beat = 0;
        keySign.subdivision = 0;
        keySign.root = sender.selectedSegmentIndex + 6;
        keySign.tonality = (self.tonality.selectedSegmentIndex == 0 ? kTonality_Major : kTonality_Minor);
        [self doKeyChange:keySign transposing:doc.metadata.enableTranspose];
    }
}

- (IBAction)tonality:(UISegmentedControl*)sender {
    [self updateKeyNames:sender.selectedSegmentIndex == 0];
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (doc.song.keySign == nil && self.row1.selectedSegmentIndex == -1 && self.row2.selectedSegmentIndex == -1) {
        self.row1.selectedSegmentIndex = 0;
        self.row2.selectedSegmentIndex = -1;
    }
    self.modeName.text = (sender.selectedSegmentIndex == 0 ? LinguiLocalizedString(@"Major", @"Tonality") : LinguiLocalizedString(@"Minor", @"Tonality"));
    
    HarmonyWizKeySignEvent *keySign = [HarmonyWizKeySignEvent new];
    keySign.beat = 0;
    keySign.subdivision = 0;
    keySign.root = (self.row1.selectedSegmentIndex == -1 ? self.row2.selectedSegmentIndex + 6 : self.row1.selectedSegmentIndex);
    keySign.tonality = (sender.selectedSegmentIndex == 0 ? kTonality_Major : kTonality_Minor);
    [self doKeyChange:keySign transposing:doc.metadata.enableTranspose];
}

#pragma mark - Table view delegate

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        doc.metadata.enableTranspose = ! doc.metadata.enableTranspose;
        self.enableTransposeCell.accessoryType = (doc.metadata.enableTranspose ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    }
}

@end
