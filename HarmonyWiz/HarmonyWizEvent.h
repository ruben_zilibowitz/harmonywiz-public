//
//  HarmonyWizEvent.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

///
// HarmonyWizEvent
///
@interface HarmonyWizEvent : NSObject <NSCopying, NSCoding>
@property (atomic,assign) UInt32 beat;
@property (atomic,assign) UInt16 subdivision;
@property (atomic,assign) BOOL selected;
- (NSComparisonResult)compare:(HarmonyWizEvent*)event;
@end

///
// HarmonyWizKeySignEvent
///
@interface HarmonyWizKeySignEvent : HarmonyWizEvent <NSCopying, NSCoding>
enum Tonality {
    kTonality_Major = 0,
    kTonality_Minor
};
@property (atomic,assign) UInt8 root;
@property (atomic,assign) enum Tonality tonality;
- (UInt8)relativeMajorRoot;
- (SInt8)sharpsFlatsCount;  // > 0 means sharps, < 0 means flats
@end

///
// HarmonyWizModeEvent
///
@interface HarmonyWizModeEvent : HarmonyWizEvent <NSCopying, NSCoding>
@property (atomic,assign) int modeID;
@property (atomic,strong) NSArray *modePitches;
@property (atomic,strong) NSArray *spelling;
@property (atomic,assign) unsigned char transposition;
@property (readonly,atomic,strong) NSArray *modeAccidentalTypes;
- (SInt8)sharpsFlatsCount;  // > 0 means sharps, < 0 means flats
@end

///
// HarmonyWizTimeSignEvent
///
@interface HarmonyWizTimeSignEvent : HarmonyWizEvent <NSCopying, NSCoding>
@property (atomic,assign) UInt8 upperNumeral;
@property (atomic,assign)UInt8 lowerNumeral;
@end

///
// HarmonyWizSpecialBarLineEvent
///
@interface HarmonyWizSpecialBarLineEvent : HarmonyWizEvent <NSCopying, NSCoding>
enum SpecialBarLine {
    kBarLine_Double = 0,
    kBarLine_StartRepeat,
    kBarLine_EndRepeat,
    kBarLine_EndSong
};
@property (atomic,assign) enum SpecialBarLine barLine;
@end

///
// HarmonyWizMusicEvent
///
@interface HarmonyWizMusicEvent : HarmonyWizEvent <NSCopying, NSCoding>
@property (atomic,assign) UInt16 durationBeats, durationSubdivisions;
+ (UInt16)middleC;
- (void)carryForBeatSubdivision:(UInt16)beatSubdivision;
- (NSComparisonResult)compareDuration:(HarmonyWizMusicEvent*)event;
@end


///
// HarmonyWizRestEvent
///
@interface HarmonyWizRestEvent : HarmonyWizMusicEvent <NSCopying, NSCoding>
@end

///
// HarmonyWizNoteEvent
///
enum NoteEventType { kNoteEventType_Harmony = 0, kNoteEventType_Passing, kNoteEventType_Suspension, kNoteEventType_Auto };
@interface HarmonyWizNoteEvent : HarmonyWizMusicEvent <NSCopying, NSCoding>
@property (atomic,assign) enum NoteEventType type;
@property (atomic,assign) SInt16 noteValue;
@property (atomic,assign) enum EnharmonicShift { kDefaultAccidental, kFlatsToSharps, kSharpsToFlats, kForceDoubleSharp, kForceDoubleFlat } enharmonicShift;
- (BOOL)wasCreatedByUser;
- (BOOL)isNonHarmonicTone;
- (void)applyNoteShift:(int)shift;
@end

///
// HarmonyWizHarmonyEvent
///
@interface HarmonyWizHarmonyEvent : HarmonyWizMusicEvent <NSCopying, NSCoding>
@property (atomic,assign) BOOL noChord;
@property (atomic,assign) BOOL pause;
@property (atomic,strong) NSMutableDictionary *constraints;
@property (atomic,strong) NSMutableDictionary *temporaryConstraints;   // like constraints but are not kept when splicing
@property (atomic,strong) NSMutableDictionary *properties;
@end

extern NSString * const HarmonyWizLeadingNoteTreatmentMelodic;
extern NSString * const HarmonyWizLeadingNoteTreatmentHarmonic;
extern NSString * const HarmonyWizLeadingNoteTreatmentProperty;
extern NSString * const HarmonyWizRelaxRulesProperty;
extern NSString * const HarmonyWizDegreeProperty;
extern NSString * const HarmonyWizChordTypeProperty;
extern NSString * const HarmonyWizInversionProperty;
extern NSString * const HarmonyWizThirdProperty;
extern NSString * const HarmonyWizFifthProperty;
extern NSString * const HarmonyWizSevenProperty;
extern NSString * const HarmonyWizNineProperty;
extern NSString * const HarmonyWizElevenProperty;
extern NSString * const HarmonyWizThirteenProperty;
