//
//  HarmonyWizSong+MusicXML.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/08/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong.h"

@interface HarmonyWizSong (MusicXML)

- (NSString*)musicXML;
- (NSData*)MXL_data:(NSString*)filename;

@end
