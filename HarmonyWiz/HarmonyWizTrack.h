//
//  HarmonyWizTrack.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HarmonyWizEvent;

@interface HarmonyWizTrack : NSObject <NSCopying, NSCoding>
@property (nonatomic,assign) enum ClefKind clef;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,assign) NSInteger trackNumber;
@property (nonatomic,strong) NSMutableArray *events;
@property (nonatomic,assign) BOOL locked;
- (HarmonyWizEvent*)eventAtBeat:(UInt32)beat subdivision:(UInt16)subdivision;
- (BOOL)isEmpty;
- (void)selectEvents:(NSSet*)events;
- (void)selectEvent:(HarmonyWizEvent*)event;
- (BOOL)isSelected:(HarmonyWizEvent*)event;
- (BOOL)anythingSelected;
- (NSArray*)selectedEvents;
@end

@interface HarmonyWizMusicTrack : HarmonyWizTrack <NSCopying, NSCoding>
@property (nonatomic,assign) SInt16 lowestNoteAllowed, highestNoteAllowed;
@property (nonatomic,strong) NSString *instrument;
@property (nonatomic,assign) int transposition; // in semitones
@property (nonatomic,assign) BOOL mute, solo;
@property (nonatomic,assign) float volume;     // 0 -> 1
@property (nonatomic,assign) float pan;        // -1 -> 1
- (NSComparisonResult)compare:(HarmonyWizMusicTrack*)track;
- (BOOL)selectionIsBelowRange;
- (BOOL)selectionIsAboveRange;
- (void)selectAll;
@end
