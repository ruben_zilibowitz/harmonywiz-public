//
//  HWTutorial.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HWTutorialPage;

@interface HWTutorial : NSObject <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

- (id)initWithName:(NSString*)name;
- (HWTutorialPage *)viewControllerAtIndex:(NSUInteger)index;

@end
