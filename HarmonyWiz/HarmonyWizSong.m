//
//  HarmonyWizSong.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizSong.h"
#import "HarmonyWizSong+Splice.h"
#import "HarmonyWizUnarchiver.h"
#import "HarmonyWizError.h"
#import "HarmonyWizMusicEvent+CPP.h"
#import "NSMutableArray+Sorted.h"
#import "HarmonyWizSong+SplitEvent.h"
#import "NSString+Concatenation.h"
#import "NSString+RomanNumerals.h"
#import "HWExceptions.h"
#import "HarmonyWizSong+Verify.h"

NSString * const HarmonyWizSongVersionException = @"SongVersionException";
UInt16 const HWDefaultSongMeasures = 4;

@interface HarmonyWizSong ()
@property (atomic,strong) NSMutableArray *mutableMusicTracks;
- (void)sortAndNumberTracks;
@end

@implementation HarmonyWizSong

static UInt16 gcdr(UInt16 a, UInt16 b) {
    if (a == 0)
        return b;
    return gcdr(b%a, a);
}

#pragma mark - init

- (id)init {
    if (self = [super init]) {
        _creatorVersion = [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"];
        self.misc = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (HarmonyWizSong*)songWithData:(NSData*)data error:(NSError**)outError {
    
    HarmonyWizSong *song = nil;
    @try {
        song = [HarmonyWizUnarchiver unarchiveObjectWithData:data];
    }
    @catch (HWSongVersionException *exception) {
        *outError = [HarmonyWizError songVersionErrorWithReason:[NSString stringWithFormat:@"The song was created with HarmonyWiz version %@. Unfortunately %@ is the oldest compatible version of HarmonyWiz.", exception.versionString, self.oldestCompatibleSongVersion]];
    }
    @catch (NSException *exception) {
        *outError = [HarmonyWizError generalErrorWithName:exception.name reason:exception.reason];
    }
    @finally {
        return song;
    }
}

+ (HarmonyWizSong*)songWithURL:(NSURL*)url error:(NSError**)outError {
    
    NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:outError];
    if (*outError != nil) {
        return nil;
    }
    
    return [HarmonyWizSong songWithData:data error:outError];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizSong *result = [[[self class] allocWithZone:zone] init];
    if (result) {
        result.title = [self.title copyWithZone:zone];
//        result.lastModified = [self.lastModified copyWithZone:zone];
        result.tempo = self.tempo;
        result.beatSubdivisions = self.beatSubdivisions;
        result.playbackPosition = self.playbackPosition;
        result.keySigns = [self.keySigns copyWithZone:zone];
        result.modes = [self.modes copyWithZone:zone];
        result.timeSigns = [self.timeSigns copyWithZone:zone];
        result.specialBarLines = [self.specialBarLines copyWithZone:zone];
        result.mutableMusicTracks = [[NSMutableArray alloc] initWithArray:self.mutableMusicTracks copyItems:YES];
        result.harmony = [self.harmony copyWithZone:zone];
        result.misc = [self.misc mutableCopyWithZone:zone];
    }
    return result;
}

#pragma mark - NSCoding

+ (NSString*)oldestCompatibleSongVersion {
    return @"1.0";
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _creatorVersion = [aDecoder decodeObjectForKey:@"creatorVersion"];
        
        // version check
        if ([[HarmonyWizSong oldestCompatibleSongVersion] compare:self.creatorVersion options:NSNumericSearch] == NSOrderedDescending) {
            [[HWSongVersionException exceptionWithVersion:self.creatorVersion] raise];
        }
        
        self.title = [aDecoder decodeObjectForKey:@"title"];
//        self.lastModified = [aDecoder decodeObjectForKey:@"lastModified"];
        self.tempo = [aDecoder decodeIntForKey:@"tempo"];
        self.keySigns = [aDecoder decodeObjectForKey:@"keySigns"];
        
        // before 1.0.7 modes did not exist
        if ([@"1.0.7" compare:self.creatorVersion options:NSNumericSearch] == NSOrderedDescending) {
            self.modes = [[HarmonyWizTrack alloc] init];
        }
        else {
            self.modes = [aDecoder decodeObjectForKey:@"modes"];
        }
        
        self.timeSigns = [aDecoder decodeObjectForKey:@"timeSigns"];
        self.triadsType = [aDecoder decodeIntForKey:@"triadsType"];
        self.specialBarLines = [aDecoder decodeObjectForKey:@"specialBarLines"];
        self.mutableMusicTracks = [aDecoder decodeObjectForKey:@"mutableMusicTracks"];
        self.harmony = [aDecoder decodeObjectForKey:@"harmony"];
        self.beatSubdivisions = [aDecoder decodeIntForKey:@"beatSubdivisions"];
        self.playbackPosition = [aDecoder decodeFloatForKey:@"playbackPosition"];
        
//        self.misc = [NSMutableDictionary dictionary];  // this is not archived so create an empty one
        NSDictionary *misc = [aDecoder decodeObjectForKey:@"misc"];
        self.misc = [NSMutableDictionary dictionaryWithDictionary:misc];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
//    [aCoder encodeObject:self.creatorVersion forKey:@"creatorVersion"];
    [aCoder encodeObject:[[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"] forKey:@"creatorVersion"];
    
    [aCoder encodeObject:self.title forKey:@"title"];
//    [aCoder encodeObject:self.lastModified forKey:@"lastModified"];
    [aCoder encodeInt:self.tempo forKey:@"tempo"];
    [aCoder encodeObject:self.keySigns forKey:@"keySigns"];
    [aCoder encodeObject:self.modes forKey:@"modes"];
    [aCoder encodeObject:self.timeSigns forKey:@"timeSigns"];
    [aCoder encodeInt:self.triadsType forKey:@"triadsType"];
    [aCoder encodeObject:self.specialBarLines forKey:@"specialBarLines"];
    [aCoder encodeObject:self.mutableMusicTracks forKey:@"mutableMusicTracks"];
    [aCoder encodeObject:self.harmony forKey:@"harmony"];
    [aCoder encodeInt:self.beatSubdivisions forKey:@"beatSubdivisions"];
    [aCoder encodeFloat:self.playbackPosition forKey:@"playbackPosition"];
    [aCoder encodeObject:self.misc forKey:@"misc"];
}

#pragma mark -

- (NSArray*)musicTracks {
    return self.mutableMusicTracks;
}

+ (UInt16)sopranoHigh {
    return [HarmonyWizMusicEvent middleC] + 24;
}
+ (UInt16)sopranoLow {
    return [HarmonyWizMusicEvent middleC];
}
+ (UInt16)mezzoHigh {
    return [HarmonyWizMusicEvent middleC] + 24 - 3;
}
+ (UInt16)mezzoLow {
    return [HarmonyWizMusicEvent middleC] - 3;
}
+ (UInt16)altoHigh {
    return [HarmonyWizMusicEvent middleC] + 24 - 7;
}
+ (UInt16)altoLow {
    return [HarmonyWizMusicEvent middleC] - 7;
}
+ (UInt16)tenorHigh {
    return [HarmonyWizMusicEvent middleC] + 12 - 3;
}
+ (UInt16)tenorLow {
    return [HarmonyWizMusicEvent middleC] - 12;
}
+ (UInt16)baritoneHigh {
    return [HarmonyWizMusicEvent middleC] + 5;
}
+ (UInt16)baritoneLow {
    return [HarmonyWizMusicEvent middleC] - 12 - 7;
}
+ (UInt16)bassHigh {
    return [HarmonyWizMusicEvent middleC] + 4;
}
+ (UInt16)bassLow {
    return [HarmonyWizMusicEvent middleC] - 24;
}
+ (UInt16)highestNoteForTrackType:(enum TrackType)type {
    UInt16 high = 0;
    switch (type) {
        case kTrackType_Soprano:
            high = [HarmonyWizSong sopranoHigh];
            break;
        case kTrackType_Mezzo:
            high = [HarmonyWizSong mezzoHigh];
            break;
        case kTrackType_Contralto:
            high = [HarmonyWizSong altoHigh];
            break;
        case kTrackType_Tenor:
            high = [HarmonyWizSong tenorHigh];
            break;
        case kTrackType_Baritone:
            high = [HarmonyWizSong baritoneHigh];
            break;
        case kTrackType_Bass:
            high = [HarmonyWizSong bassHigh];
            break;
            
        default:
            break;
    }
    return high;
}
+ (UInt16)lowestNoteForTrackType:(enum TrackType)type {
    UInt16 low = 0;
    switch (type) {
        case kTrackType_Soprano:
            low = [HarmonyWizSong sopranoLow];
            break;
        case kTrackType_Mezzo:
            low = [HarmonyWizSong mezzoLow];
            break;
        case kTrackType_Contralto:
            low = [HarmonyWizSong altoLow];
            break;
        case kTrackType_Tenor:
            low = [HarmonyWizSong tenorLow];
            break;
        case kTrackType_Baritone:
            low = [HarmonyWizSong baritoneLow];
            break;
        case kTrackType_Bass:
            low = [HarmonyWizSong bassLow];
            break;
            
        default:
            break;
    }
    return low;
}

- (BOOL)isModal {
    if (self.songMode == nil && self.keySign != nil) {
        return NO;
    }
    else if (self.songMode != nil && self.keySign == nil) {
        return YES;
    }
    else if (self.songMode == nil && self.keySign == nil) {
        [NSException raise:@"HarmonyWiz" format:@"No mode or key sign is set"];
    }
    else {
        [NSException raise:@"HarmonyWiz" format:@"Must be either modal or key signature but not both"];
    }
    return NO;
}

// checks that all harmony notes belong to selected mode
- (BOOL)validateForMode {
    NSAssert(self.songMode != nil, @"Song not using a mode");
    if (self.songMode == nil) {
        return NO;
    }
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        for (id event in track.events) {
            if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                HarmonyWizNoteEvent *ne = event;
                if (ne.type == kNoteEventType_Harmony || (ne.type == kNoteEventType_Auto && track.locked)) {
                    BOOL found = NO;
                    for (NSNumber *num in self.songMode.modePitches) {
                        if (((num.intValue + self.songMode.transposition - ne.noteValue) % 12) == 0) {
                            found = YES;
                            break;
                        }
                    }
                    if (!found) {
                        return NO;
                    }
                }
            }
        }
    }
    return YES;
}

- (void)tieAdjacentAutoNotesPreservingSelection:(const HarmonyWizSong*)preservedSong {
    const UInt16 measureSubdivs = self.beatSubdivisions * self.timeSign.upperNumeral;
    const UInt16 maxTieLength = 2 * measureSubdivs;
    
    // tie adjacent notes of same value if created by solver
    NSEnumerator *en = self.musicTracks.objectEnumerator;
    HarmonyWizMusicTrack *track;
    size_t row = 0;
    while ((track = en.nextObject) != nil) {
        NSMutableArray *newEvents = [NSMutableArray arrayWithCapacity:track.events.count];
        NSUInteger i, j;
        for (i = 0; i < track.events.count; i++) {
            HarmonyWizMusicEvent *evi = [track.events objectAtIndex:i];
            UInt16 dur = [self eventDuration:evi];
            for (j = i + 1; j < track.events.count; j++) {
                HarmonyWizMusicEvent *evj = [track.events objectAtIndex:j];
                BOOL tied = NO;
                if ([evi isKindOfClass:[HarmonyWizNoteEvent class]] &&
                    [evj isKindOfClass:[HarmonyWizNoteEvent class]] &&
                    ![[self findLastHarmonyEventForEvent:[track.events objectAtIndex:j-1]] pause] &&
                    dur < maxTieLength) {
                    
                    HarmonyWizNoteEvent *nei = (HarmonyWizNoteEvent*)evi;
                    HarmonyWizNoteEvent *nej = (HarmonyWizNoteEvent*)evj;
                    if (nei.noteValue == nej.noteValue && !nei.wasCreatedByUser && !nej.wasCreatedByUser) {
                        dur += [self eventDuration:evj];
                        tied = YES;
                    }
                }
                if (! tied)
                    break;
            }
            i = j-1;
            HarmonyWizMusicEvent *newEvent = [evi copy];
            newEvent.durationBeats = dur / self.beatSubdivisions;
            newEvent.durationSubdivisions = dur % self.beatSubdivisions;
            
            // preserve selection
            if (preservedSong && preservedSong.anythingSelected) {
                HarmonyWizTrack *pt = [preservedSong.musicTracks objectAtIndex:row];
                NSIndexSet *preservedSelectedIndices = [preservedSong overlappingEvents:pt.events forEvent:newEvent];
                NSArray *preservedSelectedObjects = [pt.events objectsAtIndexes:preservedSelectedIndices];
                NSUInteger selectedCount = [[preservedSelectedObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"selected = YES"]] count];
                newEvent.selected = (selectedCount > 0);
            }
            
            [newEvents placeObject:newEvent];
        }
        track.events = newEvents;
        row++;
    }
}

- (HarmonyWizMusicTrack*)addMusicTrackOfType:(enum TrackType)trackType {
    
    // create track
    HarmonyWizMusicTrack *track = [[HarmonyWizMusicTrack alloc] init];
    track.events = [NSMutableArray array];
    
    track.lowestNoteAllowed = [HarmonyWizSong lowestNoteForTrackType:trackType];
    track.highestNoteAllowed = [HarmonyWizSong highestNoteForTrackType:trackType];
    
    NSString *trackName = nil;
    
    switch (trackType) {
        case kTrackType_Soprano:
            // soprano
            track.clef = kClef_Treble;
            trackName = @"Soprano";
            break;
        case kTrackType_Mezzo:
            // mezzo-soprano
            track.clef = kClef_Treble;
            trackName = @"Mezzo-soprano";
            break;
        case kTrackType_Contralto:
            // contralto
            track.clef = kClef_Treble;
            trackName = @"Contralto";
            break;
        case kTrackType_Tenor:
            // tenor
            track.clef = kClef_Bass;
            trackName = @"Tenor";
            break;
        case kTrackType_Baritone:
            // baritone
            track.clef = kClef_Bass;
            trackName = @"Baritone";
            break;
        case kTrackType_Bass:
            // bass
            track.clef = kClef_Bass;
            trackName = @"Bass";
            break;
        default:
            break;
    }
    
    // get track name
    NSUInteger count = 0;
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        if ([track.title hasPrefix:trackName]) {
            count++;
        }
    }
    if (count > 0) {
        trackName = [trackName stringByAppendingFormat:@" %@", [[NSString romanNumeralsFromInt:count+1] uppercaseString]];
    }
    track.title = trackName;
    
    NSLog(@"Creating new track with name %@", trackName);
    
    // fill track with rests
    for (UInt32 beat = 0; beat < self.finalBarNumber * self.timeSign.upperNumeral; beat++) {
        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
        re.beat = beat;
        re.durationBeats = 1;
        [track.events placeObject:re];
    }
    
    // add track
    [self addMusicTrack:track];
    
    return track;
}

- (void)addMusicTrack:(HarmonyWizMusicTrack*)track {
    [self.mutableMusicTracks addObject:track];
    [self sortAndNumberTracks];
}

- (void)removeMusicTrack:(HarmonyWizMusicTrack*)track {
    [self.mutableMusicTracks removeObject:track];
    [self sortAndNumberTracks];
}

- (void)sortAndNumberTracks {
    [self.mutableMusicTracks sortUsingSelector:@selector(compare:)];
    for (NSInteger i = 0; i < self.mutableMusicTracks.count; i++) {
        [[self.mutableMusicTracks objectAtIndex:i] setTrackNumber:i];
    }
    self.harmony.trackNumber = self.mutableMusicTracks.count;
}

- (void)transposeSongBySemitones:(SInt16)chromatic {
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        for (HarmonyWizMusicEvent *event in track.events) {
            if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)event;
                ne.noteValue += chromatic;
            }
        }
    }
}

- (void)roundNotesToDiatonic {
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        for (HarmonyWizMusicEvent *event in track.events) {
            [event roundPitch:self.keySign];
        }
    }
}

- (void)roundNotesToMode {
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        for (HarmonyWizMusicEvent *event in track.events) {
            [event roundPitchModal:self.songMode];
        }
    }
}

- (UInt16) barNumberFromEvent:(HarmonyWizEvent*)event {
    NSAssert(event.subdivision < self.beatSubdivisions, @"Event has too many subdivisions");
    return (event.beat / self.timeSign.upperNumeral);
}

- (UInt16) barNumberFromEventEnd:(HarmonyWizMusicEvent*)event {
    return (([self eventEndSubdivision:event] / self.beatSubdivisions) / self.timeSign.upperNumeral);
}

- (UInt16) barBeatFromEvent:(HarmonyWizEvent*)event {
    NSAssert(event.subdivision < self.beatSubdivisions, @"Event has too many subdivisions");
    return (event.beat % self.timeSign.upperNumeral);
}

- (UInt16) finalBarNumber {
    return [self barNumberFromEvent:self.finalBarLine];
}

- (UInt16) finalBarSubdivision {
    return [self eventStartSubdivision:self.finalBarLine];
}

- (void)roundUpFinalBar {
    HarmonyWizSpecialBarLineEvent *finalBar = self.finalBarLine;
    UInt16 subdivs = [self eventStartSubdivision:finalBar];
    UInt16 subdivsPerBar = self.timeSign.upperNumeral * self.beatSubdivisions;
    if (subdivs % subdivsPerBar != 0) {
        subdivs = (subdivs / subdivsPerBar + 1) * subdivsPerBar;
    }
    [self setEventPosition:finalBar to:subdivs];
    
    for (HarmonyWizMusicTrack *mt in self.musicTracks)
        [self addFillerRestsTo:mt.events];
    [self spliceHarmony];
}

- (void) setFinalBarNumber:(UInt16)n {
    if (n < 1)
        n = 1;
    HarmonyWizSpecialBarLineEvent *event = self.finalBarLine;
    if (event)
        event.beat = n * self.timeSign.upperNumeral;
    else {
        HarmonyWizSpecialBarLineEvent *finalBar = [[HarmonyWizSpecialBarLineEvent alloc] init];
        finalBar.beat = n * self.timeSign.upperNumeral;
        finalBar.subdivision = 0;
        finalBar.barLine = kBarLine_EndSong;
        self.finalBarLine = finalBar;
    }
}

- (BOOL) canPlaceEvent:(HarmonyWizMusicEvent*)nextEvent afterEvent:(HarmonyWizMusicEvent*)lastEvent {
    UInt16 beat = lastEvent.beat + lastEvent.durationBeats;
    UInt16 subdivision = lastEvent.subdivision + lastEvent.durationSubdivisions;
    UInt16 totalSubdivs = beat * self.beatSubdivisions + subdivision;
    
    const UInt16 startBar = (totalSubdivs / self.beatSubdivisions) / self.timeSign.upperNumeral;
    
    totalSubdivs += nextEvent.durationBeats * self.beatSubdivisions + nextEvent.durationSubdivisions;
    
    const UInt16 endBar = (totalSubdivs / self.beatSubdivisions) / self.timeSign.upperNumeral;
    
    return ((startBar == endBar) || (endBar==startBar+1 && (totalSubdivs / self.beatSubdivisions) % self.timeSign.upperNumeral == 0 && totalSubdivs % self.beatSubdivisions == 0));
}

- (BOOL) canPlaceEvent:(HarmonyWizMusicEvent*)nextEvent atEvent:(HarmonyWizMusicEvent*)thisEvent {
    const UInt16 endPos = [self eventStartSubdivision:thisEvent] + [self eventDuration:nextEvent];
    return (endPos <= self.finalBarSubdivision);
}

- (BOOL)isEmpty {
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        if (! track.isEmpty) {
            return false;
        }
    }
    return true;
}

- (NSArray*) chordEvents {
    return [self.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord == NO"]];
}

- (void)replaceMusicEvent:(HarmonyWizMusicEvent*)ev with:(HarmonyWizMusicEvent*)replacement {
    for (HarmonyWizMusicTrack *mt in self.musicTracks) {
        if ([mt.events containsObject:ev]) {
            NSUInteger idx = [mt.events indexOfObject:ev];
            [mt.events replaceObjectAtIndex:idx withObject:replacement];
        }
    }
}

- (void)setEventDuration:(HarmonyWizMusicEvent*)ev to:(UInt16)subdivs {
    ev.durationBeats = subdivs / self.beatSubdivisions;
    ev.durationSubdivisions = subdivs % self.beatSubdivisions;
}

- (void)setEventPosition:(HarmonyWizEvent*)ev to:(UInt16)subdivs {
    ev.beat = subdivs / self.beatSubdivisions;
    ev.subdivision = subdivs % self.beatSubdivisions;
}


//- (BOOL) canPlaceTimeSignUpperNumeral:(UInt16)upper lowerNumeral:(UInt16)lower {
//    const UInt16 upper0 = self.timeSign.upperNumeral;
//    const UInt16 lower0 = self.timeSign.lowerNumeral;
//    BOOL result = YES;
//    
//    self.timeSign.upperNumeral = upper;
//    self.timeSign.lowerNumeral = lower;
//    
//    for (HarmonyWizTrack *track in self.musicTracks) {
//        for (HarmonyWizMusicEvent *event in track.events) {
//            if (! [self canPlaceEvent:event atEvent:event]) {
//                result = NO;
//                break;
//            }
//        }
//        if (!result)
//            break;
//    }
//    
//    if (result) {
//        for (HarmonyWizMusicEvent *event in self.harmony.events) {
//            if (! [self canPlaceEvent:event atEvent:event]) {
//                result = NO;
//                break;
//            }
//        }
//    }
//    
//    self.timeSign.upperNumeral = upper0;
//    self.timeSign.lowerNumeral = lower0;
//    
//    return result;
//}

- (void)addFillerRestsTo:(NSMutableArray*)events {
    
    // assumes events are already sorted
    [events sortUsingSelector:@selector(compare:)];
    
//    [self checkAllEventsSorted];
    
    NSMutableArray *rests = [NSMutableArray array];
    
    if (events.count == 0) {
        const UInt16 totalBeats = self.finalBarNumber * self.timeSign.upperNumeral;
        for (UInt16 curBeat = 0; curBeat < totalBeats; curBeat++) {
            HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
            re.beat = curBeat;
            re.subdivision = 0;
            re.durationBeats = 1;
            re.durationSubdivisions = 0;
            [rests placeObject:re];
        }
    }
    else {
        
        // pad start with rests
        {
            HarmonyWizMusicEvent *firstEvent = [events objectAtIndex:0];
            const UInt16 firstPost = [self eventStartSubdivision:firstEvent];
            if (firstPost > 0) {
                HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                [self setEventPosition:re to:0];
                [self setEventDuration:re to:firstPost];
                [rests placeObjectsFromArray:[self splitEventOnBeats:re]];
            }
        }
        
        // fill holes with rests
        for (NSUInteger idx = 1; idx < events.count; idx++) {
            HarmonyWizMusicEvent *currentEvent = [events objectAtIndex:idx-1];
            HarmonyWizMusicEvent *nextEvent = [events objectAtIndex:idx];
            NSAssert([self eventStartSubdivision:nextEvent]>=[self eventEndSubdivision:currentEvent],@"ordering");
            const UInt16 dur = [self eventStartSubdivision:nextEvent] - [self eventEndSubdivision:currentEvent];
            if (dur > 0) {
                HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                const UInt16 pos = [self eventEndSubdivision:currentEvent];
                [self setEventPosition:re to:pos];
                [self setEventDuration:re to:dur];
                [rests placeObjectsFromArray:[self splitEventOnBeats:re]];
            }
        }
        
        // pad with rests until final bar line
        {
            HarmonyWizMusicEvent *lastEvent = [events lastObject];
            const UInt16 lastPos = [self eventEndSubdivision:lastEvent];
            const UInt16 finalBarPos = [self eventStartSubdivision:self.finalBarLine];
            if (finalBarPos > lastPos) {
                const UInt16 dur = finalBarPos - lastPos;
                HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                [self setEventPosition:re to:lastPos];
                [self setEventDuration:re to:dur];
                [rests placeObjectsFromArray:[self splitEventOnBeats:re]];
            }
        }
    }
    
    // add rests
    [events placeObjectsFromArray:rests];
//    [self checkAllEventsSorted];
    [self checkNoGapsBetweenEventsForTrack:events];
//    [track.events placeObjectsFromArray:rests];
}

- (UInt16)eventStartSubdivision:(HarmonyWizEvent*)event {
    return (event.beat * self.beatSubdivisions + event.subdivision);
}

- (UInt16)eventDuration:(HarmonyWizMusicEvent*)event {
    return (event.durationBeats * self.beatSubdivisions + event.durationSubdivisions);
}

- (UInt16)eventEndSubdivision:(HarmonyWizMusicEvent*)event {
    return [self eventDuration:event] + [self eventStartSubdivision:event];
}

- (BOOL)event:(HarmonyWizMusicEvent*)eventA overlapsWith:(HarmonyWizMusicEvent*)eventB {
    const UInt16 startA = [self eventStartSubdivision:eventA];
    const UInt16 endA = [self eventEndSubdivision:eventA];
    const UInt16 startB = [self eventStartSubdivision:eventB];
    const UInt16 endB = [self eventEndSubdivision:eventB];
    
    return (MAX(startA,startB) < MIN(endA,endB));
}

- (NSIndexSet*) overlappingEvents:(NSArray*)events forEvent:(HarmonyWizMusicEvent*)event {
    NSMutableIndexSet *is = [NSMutableIndexSet indexSet];
    for (NSUInteger idx = 0; idx < events.count; idx++) {
        HarmonyWizMusicEvent* cur = [events objectAtIndex:idx];
        if ([self event:cur overlapsWith:event]) {
            [is addIndex:idx];
        }
    }
    return is;
}

- (NSArray*) fillerRestsForEvents:(NSArray*)events replacedBy:(HarmonyWizMusicEvent*)newEvent {
    UInt16 totalSubdivs = 0;
    for (HarmonyWizMusicEvent *e in events) {
        totalSubdivs += [self eventDuration:e];
    }
    SInt16 remainingSubdivs = totalSubdivs - [self eventDuration:newEvent];
    NSMutableArray *result = nil;
    if (remainingSubdivs > 0) {
        UInt16 gcdSubdivs = gcdr(remainingSubdivs, self.beatSubdivisions);
        UInt16 count = (remainingSubdivs / gcdSubdivs);
        result = [NSMutableArray arrayWithCapacity:count];
        if (gcdSubdivs == self.beatSubdivisions) {
            for (UInt16 i = 0; i < count; i++) {
                HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                re.durationBeats = 1;
                re.durationSubdivisions = 0;
                re.beat = newEvent.beat + newEvent.durationBeats + i;
                re.subdivision = newEvent.subdivision + newEvent.durationSubdivisions;
                [re carryForBeatSubdivision:self.beatSubdivisions];
                [result placeObject:re];
            }
        }
        else {
            for (UInt16 i = 0; i < count; i++) {
                HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                re.durationBeats = 0;
                re.durationSubdivisions = gcdSubdivs;
                re.beat = newEvent.beat + newEvent.durationBeats;
                re.subdivision = newEvent.subdivision + newEvent.durationSubdivisions + i*gcdSubdivs;
                [re carryForBeatSubdivision:self.beatSubdivisions];
                [result placeObject:re];
            }
        }
    }
    return result;
}

- (UInt16)findFirstHarmonyColumnForEvent:(HarmonyWizMusicEvent*)event {
    UInt16 result = 0;
    UInt16 eventStart = [self eventStartSubdivision:event];
    UInt16 eventEnd = [self eventEndSubdivision:event];
    for (HarmonyWizHarmonyEvent *he in self.harmony.events) {
        if (! he.noChord) {
            if (MAX([self eventStartSubdivision:he],eventStart) < MIN([self eventEndSubdivision:he],eventEnd))
                break;  // overlap exists
            result++;
        }
    }
    return result;
}

- (UInt16)findLastHarmonyColumnForEvent:(HarmonyWizMusicEvent*)event {
    NSArray *chordEvents = [self.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord == NO"]];
    UInt16 result = 0;
    UInt16 eventStart = [self eventStartSubdivision:event];
    UInt16 eventEnd = [self eventEndSubdivision:event];
    for (HarmonyWizHarmonyEvent *he in chordEvents.reverseObjectEnumerator.allObjects) {
        if (MAX([self eventStartSubdivision:he],eventStart) < MIN([self eventEndSubdivision:he],eventEnd))
            break;  // overlap exists
        result++;
    }
    NSAssert(result+1 <= chordEvents.count, @"column too large");
    return chordEvents.count - result - 1;
}

- (NSSet*)harmonyEventsOverlappingSelection {
    const NSArray *selectedTracks = [[self.musicTracks arrayByAddingObject:self.harmony] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"anythingSelected == YES"]];
    NSMutableSet *harmonyEvents = [NSMutableSet set];
    for (HarmonyWizMusicTrack *track in selectedTracks) {
        const NSArray *selectedEvents = [[track events] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.selected == YES"]];
        NSMutableIndexSet *is = [NSMutableIndexSet indexSet];
        for (HarmonyWizMusicEvent *event in selectedEvents) {
            [is addIndexes:[self overlappingEvents:self.harmony.events forEvent:event]];
        }
        [harmonyEvents addObjectsFromArray:[self.harmony.events objectsAtIndexes:is]];
    }
    return harmonyEvents;
}

- (HarmonyWizHarmonyEvent*)findLastHarmonyEventForEvent:(HarmonyWizMusicEvent*)event {
    UInt16 col = [self findLastHarmonyColumnForEvent:event];
    NSArray *chordEvents = [self.harmony.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"noChord == NO"]];
    HarmonyWizHarmonyEvent *ev = [chordEvents objectAtIndex:col];
    return ev;
}

- (HarmonyWizMusicEvent*)findFirstSelectedEvent {
    HarmonyWizMusicEvent *result = nil;
    for (HarmonyWizTrack *track in [self.musicTracks arrayByAddingObject:self.harmony]) {
        for (HarmonyWizMusicEvent *event in track.events) {
            if (event.selected) {
                if (result == nil || [self eventStartSubdivision:result] > [self eventStartSubdivision:event]) {
                    result = event;
                }
            }
        }
    }
    return result;
}

- (HarmonyWizMusicEvent*)findLastSelectedEvent {
    HarmonyWizMusicEvent *result = nil;
    for (HarmonyWizTrack *track in [self.musicTracks arrayByAddingObject:self.harmony]) {
        for (HarmonyWizMusicEvent *event in track.events) {
            if (event.selected) {
                if (result == nil || [self eventEndSubdivision:event] > [self eventEndSubdivision:result]) {
                    result = event;
                }
            }
        }
    }
    return result;
}

- (UInt16)countEmptyBeatsAtEnd {
    HarmonyWizHarmonyEvent *he = nil;
    NSEnumerator *en = self.harmony.events.reverseObjectEnumerator;
    while ((he = en.nextObject) != nil) {
        if (! he.noChord) {
            break;
        }
    }
    if (he == nil) {
        return 0;
    }
    else {
        UInt16 emptySize = [self eventStartSubdivision:self.finalBarLine] - [self eventEndSubdivision:he];
        UInt16 beats = ((float)emptySize / (float)self.beatSubdivisions);
        return beats;
    }
}

- (void)clearArrangementFrom:(HarmonyWizHarmonyEvent*)he1 to:(HarmonyWizHarmonyEvent*)he2 {
    NSAssert([he1 compare:he2] != NSOrderedDescending, @"Badly ordered events");
    UInt16 he1_start = [self eventStartSubdivision:he1];
    UInt16 he2_end = [self eventEndSubdivision:he2];
    for (HarmonyWizTrack *track in self.musicTracks) {
        NSMutableArray *newEvents = [NSMutableArray arrayWithArray:track.events];
        for (id ev in track.events) {
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)ev;
                if (! ne.wasCreatedByUser && ! track.locked) {
                    UInt16 ne_start = [self eventStartSubdivision:ne];
                    UInt16 ne_end = [self eventEndSubdivision:ne];
                    
                    // case 1: note fully contained between harmony events
                    if (ne_start >= he1_start && ne_end <= he2_end) {
                        [newEvents removeObject:ne];
                        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                        re.beat = ne.beat;
                        re.subdivision = ne.subdivision;
                        re.durationBeats = ne.durationBeats;
                        re.durationSubdivisions = ne.durationSubdivisions;
                        [newEvents placeObjectsFromArray:[self splitEventOnBeats:re]];
                    }
                    
                    // case 2: note overlaps first harmony event only
                    else if (ne_start < he1_start && ne_end > he1_start && ne_end <= he2_end) {
                        [self setEventDuration:ne to:he1_start - ne_start];
                        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                        [self setEventPosition:re to:he1_start];
                        [self setEventDuration:re to:ne_end - he1_start];
                        [newEvents placeObjectsFromArray:[self splitEventOnBeats:re]];
                    }
                    
                    // case 3: note overlaps second harmony event only
                    else if (ne_start < he2_end && ne_end > he2_end && ne_start >= he1_start) {
                        [self setEventDuration:ne to:ne_end - he2_end];
                        [self setEventPosition:ne to:he2_end];
                        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                        [self setEventPosition:re to:ne_start];
                        [self setEventDuration:re to:he2_end - ne_start];
                        [newEvents placeObjectsFromArray:[self splitEventOnBeats:re]];
                    }
                    
                    // case 4: note overlaps both harmony events
                    else if (ne_start < he1_start && ne_end > he2_end) {
                        HarmonyWizNoteEvent *ne2 = [ne copy];
                        [self setEventDuration:ne to:he1_start - ne_start];
                        [self setEventDuration:ne2 to:ne_end - he2_end];
                        [self setEventPosition:ne2 to:he2_end];
                        [newEvents placeObject:ne2];
                        HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                        [self setEventPosition:re to:he1_start];
                        [self setEventDuration:re to:he2_end - he1_start];
                        [newEvents placeObjectsFromArray:[self splitEventOnBeats:re]];
                    }
                    
                    else {
                        // nothing to do in other cases
                        NSAssert(MAX(ne_start,he1_start) >= MIN(ne_end,he2_end), @"Should not be any overlap");
                    }
                }
            }
        }
        track.events = newEvents;
    }
    [self clearHarmonyProperties];
}

- (void)clearArrangement {
    for (HarmonyWizTrack *track in self.musicTracks) {
        NSMutableArray *newEvents = [NSMutableArray arrayWithArray:track.events];
        for (id ev in track.events) {
            if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
                HarmonyWizNoteEvent *ne = (HarmonyWizNoteEvent*)ev;
                if (! ne.wasCreatedByUser && ! track.locked) {
                    [newEvents removeObject:ne];
                    HarmonyWizRestEvent *re = [[HarmonyWizRestEvent alloc] init];
                    re.beat = ne.beat;
                    re.subdivision = ne.subdivision;
                    re.durationBeats = ne.durationBeats;
                    re.durationSubdivisions = ne.durationSubdivisions;
                    [newEvents placeObjectsFromArray:[self splitEventOnBeats:re]];
                }
            }
        }
        track.events = newEvents;
    }
    [self clearHarmonyProperties];
}

- (void)placePlaybackIndicatorAtEvent:(HarmonyWizEvent*)event {
    float beat = event.beat + (float)event.subdivision / (float)self.beatSubdivisions;
    self.playbackPosition = beat * (4.f / self.timeSign.lowerNumeral);
}

- (void)removeOtherEventsOverlappingWith:(NSArray*)eventsArray {
    for (HarmonyWizMusicTrack *mt in self.musicTracks) {
        for (HarmonyWizMusicEvent *ev in eventsArray) {
            NSIndexSet *is = [self overlappingEvents:mt.events forEvent:ev];
            NSMutableArray *overlaps = [NSMutableArray arrayWithArray:[[mt.events objectsAtIndexes:is] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"class == %@", [HarmonyWizNoteEvent class]]]];
            [overlaps removeObjectsInArray:eventsArray];
            [mt.events removeObjectsInArray:overlaps];
        }
        [self addFillerRestsTo:mt.events];
    }
}

- (void)truncateEventToEndOfSong:(HarmonyWizMusicEvent*)event {
    if ([self eventEndSubdivision:event] > self.finalBarSubdivision) {
        const UInt16 len = self.finalBarSubdivision - [self eventStartSubdivision:event];
        event.durationBeats = len / self.beatSubdivisions;
        event.durationSubdivisions = len % self.beatSubdivisions;
    }
}

// general algorithm for unifying different note types
// used for resolving suspensions and passing notes
- (NSMutableArray*)unifyNoteType:(enum NoteEventType)fromType to:(enum NoteEventType)toType forEvents:(NSArray*)events inReverse:(BOOL)reverse {
    
    // assume self.events is sorted
    [self checkAllEventsSorted];
    
    if (reverse) {
        events = events.reverseObjectEnumerator.allObjects;
    }
    
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:events.count];
    
    for (NSUInteger idx = 0; idx < events.count; idx++) {
        id ev = [events objectAtIndex:idx];
        if ([ev isKindOfClass:[HarmonyWizRestEvent class]]) {
            // rests pass through
            [result placeObject:ev];
        }
        else if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *ne = ev;
            
            // unify fromType
            if (ne.type == fromType) {
                id ev1 = nil;
                HarmonyWizNoteEvent *ne1 = nil;
                BOOL fromTypeResolved = NO;
                for (NSUInteger jdx = idx+1; jdx < events.count; jdx++) {
                    ev1 = [events objectAtIndex:jdx];
                    if ([ev1 isKindOfClass:[HarmonyWizNoteEvent class]]) {
                        ne1 = ev1;
                        if (ne1.type == fromType) {
                            // nothing to do here
                        }
                        else if (ne1.type == toType) {
                            fromTypeResolved = YES;
                            idx = jdx;
                            break;
                        }
                        else {
                            // unresolved fromType
                            break;
                        }
                    }
                    else {
                        // unresolved fromType
                        break;
                    }
                }
                if (fromTypeResolved) {
                    if (reverse) {
                        id swap = ev;
                        ev = ev1;
                        ev1 = swap;
                    }
                    const UInt16 totalSubdivs = [self eventEndSubdivision:ev1] - [self eventStartSubdivision:ev];
                    HarmonyWizNoteEvent *ne2 = [ev copy];
                    ne2.noteValue = ne1.noteValue;
                    ne2.type = toType;
                    ne2.durationBeats = totalSubdivs / self.beatSubdivisions;
                    ne2.durationSubdivisions = totalSubdivs % self.beatSubdivisions;
                    [result placeObject:ne2];
                }
                else {
                    // unresolved fromType
                    // do nothing
                }
            }
            else {
                // other note types pass through
                [result placeObject:ev];
            }
        }
        else {
            // wtf?
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"incorrect event type" userInfo:nil];
        }
    }
    return result;
}

- (NSMutableArray*)unifiedSuspensions:(NSArray *)events {
    return [self unifyNoteType:kNoteEventType_Suspension to:kNoteEventType_Harmony forEvents:events inReverse:NO];
}

- (NSMutableArray*)unifiedPassingNotes:(NSArray*)events {
    return [self unifyNoteType:kNoteEventType_Passing to:kNoteEventType_Harmony forEvents:events inReverse:YES];
}

- (NSMutableArray*)unifiedEvents:(NSArray*)events {
    NSMutableArray *stage1 = [self unifiedSuspensions:events];
    NSMutableArray *stage2 = [self unifiedPassingNotes:stage1];
    return stage2;
}

-(void)clearLeadingNoteConstraints {
    for (HarmonyWizHarmonyEvent *he in self.harmony.events) {
        [he.constraints removeObjectForKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
}

- (void)clearHarmonyConstraints {
    for (HarmonyWizHarmonyEvent *he in self.harmony.events) {
        [he.constraints removeAllObjects];
    }
}

- (void)clearHarmonyProperties {
    for (HarmonyWizHarmonyEvent *he in self.harmony.events) {
        he.properties = nil;
    }
}

- (void)clearHarmonyRelaxConstraints {
    for (HarmonyWizHarmonyEvent *he in self.harmony.events) {
        [he.constraints removeObjectForKey:HarmonyWizRelaxRulesProperty];
    }
}

- (HarmonyWizKeySignEvent*)keySign {
    if (self.keySigns.events.count > 0) {
        HarmonyWizKeySignEvent *keySign = [self.keySigns.events objectAtIndex:0];
        NSAssert(keySign.beat == 0 && keySign.subdivision == 0, @"Key sign not at start");
        return keySign;
    }
    else {
        return nil;
    }
}

- (HarmonyWizModeEvent*)songMode {
    if (self.modes.events.count > 0) {
        HarmonyWizModeEvent *mode = [self.modes.events objectAtIndex:0];
        NSAssert(mode.beat == 0 && mode.subdivision == 0, @"Mode not at start");
        return mode;
    }
    else {
        return nil;
    }
}

- (void)setKeySign:(HarmonyWizKeySignEvent*)keySignEvent {
    NSAssert(keySignEvent.beat == 0 && keySignEvent.subdivision == 0, @"Key sign not at start");
    self.keySigns.events = [NSMutableArray arrayWithObject:keySignEvent];
    [self.modes.events removeAllObjects];
}

- (void)setSongMode:(HarmonyWizModeEvent*)mode {
    NSAssert(mode.beat == 0 && mode.subdivision == 0, @"Mode not at start");
    self.modes.events = [NSMutableArray arrayWithObject:mode];
    [self.keySigns.events removeAllObjects];
}

- (void)autoTieSelectedNotes {
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        NSArray *notes = [track.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.selected == YES"]];
        if (notes.count > 0) {
            NSMutableArray *tiedNotes = [NSMutableArray arrayWithCapacity:notes.count];
            NSArray *unifiedNotes = [[self unifiedEvents:notes] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [HarmonyWizNoteEvent class]]];
            for (HarmonyWizNoteEvent *note in unifiedNotes) {
                const UInt16 start = [self eventStartSubdivision:note];
                const UInt16 end = [self eventEndSubdivision:note];
                NSArray *localNotes = [notes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(HarmonyWizNoteEvent *obj, NSDictionary *bindings){return ([self eventStartSubdivision:obj] >= start && [self eventEndSubdivision:obj] <= end);}]];
                for (NSUInteger idx = 0; idx < localNotes.count;) {
                    HarmonyWizNoteEvent *ne = [localNotes objectAtIndex:idx];
                    NSUInteger jdx;
                    for (jdx = idx+1; jdx < localNotes.count; jdx++) {
                        HarmonyWizNoteEvent *nf = [localNotes objectAtIndex:jdx];
                        if (nf.noteValue != ne.noteValue)
                            break;
                    }
                    HarmonyWizNoteEvent *nf = [localNotes objectAtIndex:jdx-1];
                    HarmonyWizNoteEvent *newNote = nil;
//                    NSAssert(ne.type != kNoteEventType_Suspension, @"cannot tie suspension notes");
                    if (ne.type == kNoteEventType_Harmony || ne.type == kNoteEventType_Passing) {
                        newNote = [ne copy];
                        UInt16 dur = [self eventEndSubdivision:nf] - [self eventStartSubdivision:ne];
                        [self setEventDuration:newNote to:dur];
                        [tiedNotes addObject:newNote];
                    }
                    else {
                        [tiedNotes addObject:ne];
                    }
                    
                    idx = jdx;
                }
            }
            notes = tiedNotes;
            {
                for (id ev in notes)
                    [track.events removeObjectsAtIndexes:[self overlappingEvents:track.events forEvent:ev]];
                [track.events placeObjectsFromArray:notes];
                [self addFillerRestsTo:track.events];
            }
        }
    }
    [self spliceHarmony];
}

- (HarmonyWizTimeSignEvent*)timeSign {
    HarmonyWizTimeSignEvent *timeSign = [self.timeSigns.events objectAtIndex:0];
    NSAssert(timeSign.beat == 0 && timeSign.subdivision == 0, @"Time sign not at start");
    return timeSign;
}

- (void)setTimeSign:(HarmonyWizTimeSignEvent*)timeSignEvent {
    NSAssert(timeSignEvent.beat == 0 && timeSignEvent.subdivision == 0, @"Time sign not at start");
    self.timeSigns.events = [NSMutableArray arrayWithObject:timeSignEvent];
}

- (void)checkNoGapsBetweenEventsForTrack:(NSArray*)events {
    if ([self eventStartSubdivision:[events objectAtIndex:0]] != 0) {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"no event at start" userInfo:nil];
    }
    if ([self eventEndSubdivision:events.lastObject] < [self eventStartSubdivision:self.finalBarLine]) {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"no event at end" userInfo:nil];
    }
    if ([self eventStartSubdivision:events.lastObject] > [self eventStartSubdivision:self.finalBarLine]) {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"event start past end" userInfo:nil];
    }
    if ([self eventEndSubdivision:events.lastObject] > [self eventStartSubdivision:self.finalBarLine]) {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"event end past end" userInfo:nil];
    }
    for (NSUInteger idx = 1; idx < events.count; idx++) {
        if ([self eventStartSubdivision:[events objectAtIndex:idx]] > [self eventEndSubdivision:[events objectAtIndex:idx-1]]) {
            @throw [NSException exceptionWithName:@"HarmonyWiz" reason:@"gap between events" userInfo:nil];
        }
    }
}

- (void)selectNone {
    for (HarmonyWizMusicTrack *mt in self.musicTracks)
        [mt selectEvent:nil];
    [self.harmony selectEvent:nil];
}

- (BOOL)anythingSelected {
    for (HarmonyWizTrack *tr in [self.musicTracks arrayByAddingObject:self.harmony]) {
        if (tr.anythingSelected) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)canPerformAutoTie {
    for (HarmonyWizMusicTrack *track in self.musicTracks) {
        NSArray *notes = [track.events filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.selected == YES"]];
        if (notes.count > 0) {
            NSArray *unifiedNotes = [self unifiedEvents:notes];
            for (HarmonyWizNoteEvent *note in unifiedNotes) {
                const UInt16 start = [self eventStartSubdivision:note];
                const UInt16 end = [self eventEndSubdivision:note];
                NSArray *localNotes = [notes filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(HarmonyWizNoteEvent *obj, NSDictionary *bindings){return ([self eventStartSubdivision:obj] >= start && [self eventEndSubdivision:obj] <= end);}]];
                for (NSUInteger idx = 1; idx < localNotes.count; idx++) {
                    HarmonyWizNoteEvent *ne0 = [localNotes objectAtIndex:idx-1];
                    HarmonyWizNoteEvent *ne1 = [localNotes objectAtIndex:idx];
                    if ([self eventEndSubdivision:ne0] == [self eventStartSubdivision:ne1]) {
                        if (ne0.noteValue == ne1.noteValue) {
                            if (ne0.type == ne1.type && ne0.type != kNoteEventType_Harmony) {
                                return YES;
                            }
                            if (ne0.type == kNoteEventType_Harmony && ne1.type == kNoteEventType_Passing) {
                                return YES;
                            }
                            if (ne0.type == kNoteEventType_Suspension && ne1.type == kNoteEventType_Harmony) {
                                return YES;
                            }
                        }
                    }
                }
            }
        }
    }
    return NO;
}

- (void)setupSATB {
    self.title = @"My song";
    self.tempo = 80;
    self.beatSubdivisions = 16;
    
    self.keySigns = [[HarmonyWizTrack alloc] init];
    self.modes = [[HarmonyWizTrack alloc] init];
    self.timeSigns = [[HarmonyWizTrack alloc] init];
    self.specialBarLines = [[HarmonyWizTrack alloc] init];
    self.specialBarLines.events = [NSMutableArray array];
    
    // soprano
    HarmonyWizMusicTrack *soprano = [[HarmonyWizMusicTrack alloc] init];
    soprano.title = @"Soprano";
    soprano.events = [NSMutableArray array];
    soprano.lowestNoteAllowed = [HarmonyWizSong sopranoLow];
    soprano.highestNoteAllowed = [HarmonyWizSong sopranoHigh];
    soprano.clef = kClef_Treble;
    soprano.volume = 0.9f;
    
    // alto
    HarmonyWizMusicTrack *alto = [[HarmonyWizMusicTrack alloc] init];
    alto.title = @"Contralto";
    alto.events = [NSMutableArray array];
    alto.lowestNoteAllowed = [HarmonyWizSong altoLow];
    alto.highestNoteAllowed = [HarmonyWizSong altoHigh];
    alto.clef = kClef_Treble;
    alto.volume = 0.8f;
    
    // tenor
    HarmonyWizMusicTrack *tenor = [[HarmonyWizMusicTrack alloc] init];
    tenor.title = @"Tenor";
    tenor.events = [NSMutableArray array];
    tenor.lowestNoteAllowed = [HarmonyWizSong tenorLow];
    tenor.highestNoteAllowed = [HarmonyWizSong tenorHigh];
    tenor.clef = kClef_Bass;
    tenor.volume = 0.8f;
    
    // bass
    HarmonyWizMusicTrack *bass = [[HarmonyWizMusicTrack alloc] init];
    bass.title = @"Bass";
    bass.events = [NSMutableArray array];
    bass.lowestNoteAllowed = [HarmonyWizSong bassLow];
    bass.highestNoteAllowed = [HarmonyWizSong bassHigh];
    bass.clef = kClef_Bass;
    bass.volume = 0.8f;
    
    // add music tracks
    self.mutableMusicTracks = [NSMutableArray arrayWithObjects:soprano, alto, tenor, bass, nil];
    
    // harmony
    self.harmony = [[HarmonyWizTrack alloc] init];
    self.harmony.title = @"Harmony";
    self.harmony.events = [NSMutableArray array];
    
    // must sort and number tracks
    [self sortAndNumberTracks];
    
    HarmonyWizTimeSignEvent *tsEvent = [[HarmonyWizTimeSignEvent alloc] init];
    tsEvent.upperNumeral = 4;
    tsEvent.lowerNumeral = 4;
    self.timeSign = tsEvent;
    
    HarmonyWizKeySignEvent *ksEvent = [[HarmonyWizKeySignEvent alloc] init];
    ksEvent.root = 0;
    ksEvent.tonality = kTonality_Major;
    self.keySign = ksEvent;
    
    [self clearBars:HWDefaultSongMeasures];
}

- (void)clearBars:(UInt16)numBars {
    
    self.finalBarNumber = numBars;
    
    for (HarmonyWizTrack *track in self.musicTracks)
        [track.events removeAllObjects];
    [self.harmony.events removeAllObjects];
    const UInt16 lastBeat = self.finalBarNumber * self.timeSign.upperNumeral;
    for (UInt16 beat = 0; beat < lastBeat; beat++) {
        HarmonyWizRestEvent *rest = [[HarmonyWizRestEvent alloc] init];
        rest.beat = beat;
        rest.durationBeats = 1;
        for (HarmonyWizTrack *track in self.musicTracks)
            [track.events placeObject:rest];
        HarmonyWizHarmonyEvent *harmony = [[HarmonyWizHarmonyEvent alloc] init];
        harmony.beat = beat;
        harmony.durationBeats = 1;
        harmony.noChord = YES;
        [self.harmony.events placeObject:harmony];
    }
}

- (void)addBarsToEnd:(NSUInteger)count {
    const UInt16 offset = self.finalBarNumber * self.timeSign.upperNumeral;
    for (UInt16 beat = 0; beat < count * self.timeSign.upperNumeral; beat++) {
        HarmonyWizRestEvent *rest = [[HarmonyWizRestEvent alloc] init];
        rest.beat = offset + beat;
        rest.durationBeats = 1;
        for (HarmonyWizTrack *track in self.musicTracks)
            [track.events placeObject:rest];
        HarmonyWizHarmonyEvent *harmony = [[HarmonyWizHarmonyEvent alloc] init];
        harmony.beat = offset + beat;
        harmony.durationBeats = 1;
        harmony.noChord = YES;
        [self.harmony.events placeObject:harmony];
    }
    self.finalBarNumber = self.finalBarNumber + count;
}

- (void)removeBarsFromEnd:(NSUInteger)count {
    NSAssert(count < self.finalBarNumber, @"Cannot remove last bar");
    const UInt16 lastBar = self.finalBarNumber - count;
    const UInt16 lastSubdiv = lastBar*self.beatSubdivisions*self.timeSign.upperNumeral;
    
    self.finalBarNumber = lastBar;
    
    for (HarmonyWizTrack *track in [self.musicTracks arrayByAddingObject:self.harmony]) {
        NSMutableIndexSet *is = [NSMutableIndexSet indexSet];
        for (NSUInteger idx = 0; idx < track.events.count; idx++) {
            HarmonyWizMusicEvent* cur = [track.events objectAtIndex:idx];
            if ([self eventEndSubdivision:cur] > lastSubdiv) {
                [is addIndex:idx];
            }
        }
        [track.events removeObjectsAtIndexes:is];
        [self addFillerRestsTo:track.events];
    }
}

- (UInt16)harmonyEventCount {
    UInt16 result = 0;
    for (HarmonyWizHarmonyEvent *he in self.harmony.events) {
        if (! he.noChord)
            result++;
    }
    return result;
}

- (struct NoteType) noteTypeFromMusicEvent:(HarmonyWizMusicEvent*)event {
    struct NoteType noteType = {kNote_Unsupported,0};
    
    const UInt16 subdivs = [self eventDuration:event];
    const UInt16 gcd = gcdr(subdivs, self.beatSubdivisions);
    const UInt16 num = subdivs / gcd;
    const UInt16 den = self.beatSubdivisions / gcd;
    
    if (num == 1 && den == 16)
        noteType.noteHead = kNote_Hemidemisemiquaver;
    else if (num == 1 && den == 8)
        noteType.noteHead = kNote_Demisemiquaver;
    else if (num == 3 && den == 16) {
        noteType.noteHead = kNote_Demisemiquaver;
        noteType.dots = 1;
    }
    else if (num == 1 && den == 4)
        noteType.noteHead = kNote_Semiquaver;
    else if (num == 3 && den == 8) {
        noteType.noteHead = kNote_Semiquaver;
        noteType.dots = 1;
    }
    else if (num == 7 && den == 16) {
        noteType.noteHead = kNote_Semiquaver;
        noteType.dots = 2;
    }
    else if (num == 1 && den == 2)
        noteType.noteHead = kNote_Quaver;
    else if (num == 3 && den == 4) {
        noteType.noteHead = kNote_Quaver;
        noteType.dots = 1;
    }
    else if (num == 7 && den == 8) {
        noteType.noteHead = kNote_Quaver;
        noteType.dots = 2;
    }
    else if (num == 1 && den == 1)
        noteType.noteHead = kNote_Crotchet;
    else if (num == 3 && den == 2) {
        noteType.noteHead = kNote_Crotchet;
        noteType.dots = 1;
    }
    else if (num == 7 && den == 4) {
        noteType.noteHead = kNote_Crotchet;
        noteType.dots = 2;
    }
    else if (num == 2 && den == 1)
        noteType.noteHead = kNote_Minim;
    else if (num == 3 && den == 1) {
        noteType.noteHead = kNote_Minim;
        noteType.dots = 1;
    }
    else if (num == 7 && den == 2) {
        noteType.noteHead = kNote_Minim;
        noteType.dots = 2;
    }
    else if (num == 4 && den == 1)
        noteType.noteHead = kNote_Semibreve;
    else if (num == 6 && den == 1) {
        noteType.noteHead = kNote_Semibreve;
        noteType.dots = 1;
    }
    else if (num == 7 && den == 1) {
        noteType.noteHead = kNote_Semibreve;
        noteType.dots = 2;
    }
    else if (num == 8 && den == 1)
        noteType.noteHead = kNote_Breve;
    else if (num == 12 && den == 1) {
        noteType.noteHead = kNote_Breve;
        noteType.dots = 1;
    }
    else if (num == 14 && den == 1) {
        noteType.noteHead = kNote_Breve;
        noteType.dots = 2;
    }
    
    if (self.timeSign.lowerNumeral == 8 && noteType.noteHead != kNote_Unsupported) {
        noteType.noteHead++;
    }
    
    return noteType;
}

- (HarmonyWizSpecialBarLineEvent*)finalBarLine {
    for (HarmonyWizSpecialBarLineEvent *ev in self.specialBarLines.events) {
        if (ev.barLine == kBarLine_EndSong) {
            return ev;
        }
    }
    return nil;
}

- (void)setFinalBarLine:(HarmonyWizSpecialBarLineEvent*)finalBarLine {
    for (NSUInteger idx = 0; idx < self.specialBarLines.events.count; idx++) {
        HarmonyWizSpecialBarLineEvent *ev = [self.specialBarLines.events objectAtIndex:idx];
        if (ev.barLine == kBarLine_EndSong) {
            [self.specialBarLines.events replaceObjectAtIndex:idx withObject:finalBarLine];
            return;
        }
    }
    [self.specialBarLines.events placeObject:finalBarLine];
}

@end
