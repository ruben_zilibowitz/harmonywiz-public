//
//  HarmonyWizStaveView.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/04/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizStaveView.h"
#import "NSString+Unichar.h"
#import "HWTimeRulerScrollView.h"
#import "NSValue+HarmonyWizAdditions.h"
#import "NSMutableArray+Sorted.h"
#import "HarmonyWizSong+SplitEvent.h"
#import "HWDocument.h"
#import "HWCollectionViewController.h"
#import "NSString+Concatenation.h"
#import "NSString+iOS6.h"
#import "HWColourScheme.h"
#import "AudioController.h"
#import "HWEvent.h"
#import "UIView+Recursive/UIView+Recursive.h"
#import "HarmonyWizSong+Accidentals.h"

#include <map>
#include <vector>
#include "ScaleNote.h"

NSArray * const kScaleDegrees = [NSArray arrayWithObjects:@"I", @"II", @"III", @"IV", @"V", @"VI", @"VII", nil];

extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWRecordFromMIDI;

using namespace std;
using namespace HarmonyWiz;

@interface HWNoteHeads : NSObject
+ (UIBezierPath*)stemAtScale:(float)scale;
+ (UIBezierPath*)filledRhythmAtScale:(float)scale;
+ (UIBezierPath*)unfilledRhythmAtScale:(float)scale;
@end

@implementation HWNoteHeads
+ (UIBezierPath*)stemAtScale:(float)scale {
    UIBezierPath *_stem = nil;
    _stem = [UIBezierPath bezierPath];
    [_stem moveToPoint: CGPointMake(-1.62f * scale,2.6775f * scale)];
    [_stem addLineToPoint: CGPointMake(-1.62f * scale,11.9575f * scale)];
    _stem.miterLimit = 4;
    _stem.lineCapStyle = kCGLineCapRound;
    _stem.lineJoinStyle = kCGLineJoinBevel;
    _stem.lineWidth = 0.25 * scale;
    return _stem;
}
+ (UIBezierPath*)filledRhythmAtScale:(float)scale {
    UIBezierPath *_filledRhythm = nil;
    _filledRhythm = [UIBezierPath bezierPath];
    [_filledRhythm moveToPoint: CGPointMake((-1.53f) * scale,(8.75e-2f) * scale)];
    [_filledRhythm addLineToPoint: CGPointMake((1.55f) * scale,(-2.6225f) * scale)];
    [_filledRhythm addLineToPoint: CGPointMake((1.53f) * scale,(-8.25e-2f) * scale)];
    [_filledRhythm addLineToPoint: CGPointMake((-1.55f) * scale,(2.6175f) * scale)];
    [_filledRhythm addLineToPoint: CGPointMake((-1.53f) * scale,(8.75e-2f) * scale)];
    [_filledRhythm closePath];
    _filledRhythm.miterLimit = 4;
    _filledRhythm.lineWidth = 0.5 * scale;
    return _filledRhythm;
}
+ (UIBezierPath*)unfilledRhythmAtScale:(float)scale {
    UIBezierPath *_unfilledRhythm = nil;
    _unfilledRhythm = [UIBezierPath bezierPath];
    [_unfilledRhythm moveToPoint: CGPointMake((1.55f) * scale,(-2.6175f) * scale)];
    [_unfilledRhythm addLineToPoint: CGPointMake((1.53f) * scale,(-8.75e-2f) * scale)];
    [_unfilledRhythm addLineToPoint: CGPointMake((-1.55f) * scale,(2.6225f) * scale)];
    [_unfilledRhythm addLineToPoint: CGPointMake((-1.53f) * scale,(8.25e-2f) * scale)];
    [_unfilledRhythm addLineToPoint: CGPointMake((1.55f) * scale,(-2.6175f) * scale)];
    [_unfilledRhythm closePath];
    _unfilledRhythm.miterLimit = 4;
    _unfilledRhythm.lineWidth = 0.5 * scale;
    return _unfilledRhythm;
}
@end

#pragma mark - clef label

@interface HWClefLabel : UILabel
@property (nonatomic,assign) enum ClefKind kind;
@property (nonatomic,assign,readonly) ScaleNote middleLine, topSpace, bottomSpace;
@end

@implementation HWClefLabel
+ (HWClefLabel*)clefKind:(enum ClefKind)kind {
    HWClefLabel *result = [[HWClefLabel alloc] initWithFrame:CGRectMake(0, 0, 64, 128)];
    result.opaque = NO;
    result.backgroundColor = [UIColor clearColor];
    result.lineBreakMode = NSLineBreakByClipping;
    result.textAlignment = NSTextAlignmentCenter;
    result.textColor = [UIColor blackColor];
    result.translatesAutoresizingMaskIntoConstraints = NO;
    result.kind = kind;
    return result;
}
- (void)setKind:(enum ClefKind)kind {
    _kind = kind;
    unichar clefChar;
    switch (kind) {
        case kClef_Treble:
            clefChar = 0xF080;
            break;
        case kClef_Alto:
            clefChar = 0xF082;
            break;
        case kClef_Tenor:
            clefChar = 0xF082;
            break;
        case kClef_Bass:
            clefChar = 0xF081;
            break;
        case kClef_Neutral:
            clefChar = 0xF083;
            break;
    }
    self.text = [NSString stringWithUnichar:clefChar];
    
    _middleLine = [self computeMiddleLine];
    _topSpace = [self computeTopSpace];
    _bottomSpace = [self computeBottomSpace];
}

- (ScaleNote)computeMiddleLine {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    ScaleNote middleLine;
    switch (self.kind) {
        case kClef_Treble:
        {
            const SInt8 chromaticBByKey[] = {0,-1,0,-1,0,-1,-1,0,-1,0,-1,0};
            const SInt8 keysChromaticB = chromaticBByKey[[doc.song.keySign relativeMajorRoot]];
            middleLine = ScaleNote([HarmonyWizMusicEvent middleC] + 11 + keysChromaticB, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Alto:
        {
            const SInt8 chromaticCByKey[] = {0,0,1,0,1,0,-1,0,0,1,0,1};
            const SInt8 keysChromaticC = chromaticCByKey[[doc.song.keySign relativeMajorRoot]];
            middleLine = ScaleNote([HarmonyWizMusicEvent middleC] + keysChromaticC, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Tenor:
        {
            const SInt8 chromaticAByKey[] = {0,-1,0,-1,0,0,-1,0,-1,0,0,1};
            const SInt8 keysChromaticA = chromaticAByKey[[doc.song.keySign relativeMajorRoot]];
            middleLine = ScaleNote([HarmonyWizMusicEvent middleC] - 3 + keysChromaticA, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Bass:
        {
            const SInt8 chromaticDByKey[] = {0,-1,0,0,1,0,-1,0,-1,1,0,1};
            const SInt8 keyChromaticD = chromaticDByKey[[doc.song.keySign relativeMajorRoot]];
            middleLine = ScaleNote([HarmonyWizMusicEvent middleC] - 10 + keyChromaticD, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        default:
            break;
    }
    return middleLine;
}

- (ScaleNote)computeTopSpace {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    ScaleNote topSpace;
    switch (self.kind) {
        case kClef_Treble:
        {
            const SInt8 chromaticGByKey[] = {0,-1,0,0,1,0,-1,0,0,1,0,1};
            const SInt8 keysChromaticG = chromaticGByKey[[doc.song.keySign relativeMajorRoot]];
            topSpace = ScaleNote([HarmonyWizMusicEvent middleC] + 19 + keysChromaticG, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Alto:
        {
            const SInt8 chromaticAByKey[] = {0,-1,0,-1,0,0,-1,0,-1,0,0,1};
            const SInt8 keysChromaticA = chromaticAByKey[[doc.song.keySign relativeMajorRoot]];
            topSpace = ScaleNote([HarmonyWizMusicEvent middleC] + 9 + keysChromaticA, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Tenor:
        {
            const SInt8 chromaticFByKey[] = {0,0,1,0,1,0,0,1,0,1,0,1};
            const SInt8 keysChromaticF = chromaticFByKey[[doc.song.keySign relativeMajorRoot]];
            topSpace = ScaleNote([HarmonyWizMusicEvent middleC] + 5 + keysChromaticF, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Bass:
        {
            const SInt8 chromaticBByKey[] = {0,-1,0,-1,0,-1,-1,0,-1,0,-1,0};
            const SInt8 keysChromaticB = chromaticBByKey[[doc.song.keySign relativeMajorRoot]];
            topSpace = ScaleNote([HarmonyWizMusicEvent middleC] - 1 + keysChromaticB, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        default:
            break;
    }
    return topSpace;
}

- (ScaleNote)computeBottomSpace {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    ScaleNote bottomSpace;
    switch (self.kind) {
        case kClef_Treble:
        {
            const SInt8 chromaticDByKey[] = {0,-1,0,0,1,0,-1,0,-1,1,0,1};
            const SInt8 keyChromaticD = chromaticDByKey[[doc.song.keySign relativeMajorRoot]];
            bottomSpace = ScaleNote([HarmonyWizMusicEvent middleC] + 2 + keyChromaticD, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Alto:
        {
            const SInt8 chromaticEByKey[] = {0,-1,0,-1,0,0,-1,0,-1,0,-1,0};
            const SInt8 keyChromaticE = chromaticEByKey[[doc.song.keySign relativeMajorRoot]];
            bottomSpace = ScaleNote([HarmonyWizMusicEvent middleC] - 8 + keyChromaticE, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Tenor:
        {
            const SInt8 chromaticCByKey[] = {0,0,1,0,1,0,-1,0,0,1,0,1};
            const SInt8 keysChromaticC = chromaticCByKey[[doc.song.keySign relativeMajorRoot]];
            bottomSpace = ScaleNote([HarmonyWizMusicEvent middleC] - 12 + keysChromaticC, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        case kClef_Bass:
        {
            const SInt8 chromaticFByKey[] = {0,0,1,0,1,0,0,1,0,1,0,1};
            const SInt8 keysChromaticF = chromaticFByKey[[doc.song.keySign relativeMajorRoot]];
            bottomSpace = ScaleNote([HarmonyWizMusicEvent middleC] - 19 + keysChromaticF, doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
        }
            break;
        default:
            break;
    }
    return bottomSpace;
}
@end

#pragma mark - rhythm note head view

@interface HWRhythmNoteHeadView : UIView
@property (nonatomic,strong) UIColor *color;
@property (nonatomic,assign) BOOL filled;
@property (nonatomic,assign) CGFloat scale;
@end

@implementation HWRhythmNoteHeadView
+ (HWRhythmNoteHeadView*)rhythmNoteHead {
    HWRhythmNoteHeadView *result = [[HWRhythmNoteHeadView alloc] initWithFrame:CGRectZero];
    result.translatesAutoresizingMaskIntoConstraints = NO;
    result.backgroundColor = [UIColor clearColor];
    result.opaque = NO;
    result.scale = 1;
    result.color = [UIColor blackColor];
    [result sizeToFit];
    return result;
}
- (void)setColor:(UIColor *)color {
    _color = color;
    [self setNeedsDisplay];
}
- (void)drawRect:(CGRect)rect {
    [self.color set];
    if (self.filled) {
        UIBezierPath *path = [HWNoteHeads filledRhythmAtScale:self.scale];
        [path applyTransform:CGAffineTransformMakeTranslation(self.center.x, self.center.y)];
        [path fill];
        [path stroke];
    }
    else  {
        UIBezierPath *path = [HWNoteHeads unfilledRhythmAtScale:self.scale];
        [path applyTransform:CGAffineTransformMakeTranslation(self.center.x, self.center.y)];
        [path stroke];
    }
}
- (CGSize)sizeThatFits:(CGSize)size {
    UIBezierPath *path = (self.filled ? [HWNoteHeads filledRhythmAtScale:self.scale] : [HWNoteHeads unfilledRhythmAtScale:self.scale]);
    return CGRectInset(path.bounds, -0.3f*self.scale, -0.3f*self.scale).size;
}
- (void)setScale:(CGFloat)scale {
    _scale = scale;
    [self setNeedsDisplay];
}
- (void)setFilled:(BOOL)filled {
    _filled = filled;
    [self setNeedsDisplay];
}
@end

#pragma mark - event labels

@interface HWEventLabel : UIView
@property (nonatomic,strong,readonly) UILabel *noteLabel;
@property (nonatomic,strong,readonly) UILabel *tailLabel;
@property (nonatomic,strong,readonly) UILabel *pauseLabel;
@property (nonatomic,strong,readonly) HWRhythmNoteHeadView *rhythmNoteHead;
@property (nonatomic,strong,readonly) NSArray *legerLines;

@property (nonatomic,weak,readonly) NSMutableArray *tailLabelQueue, *rhythmNoteHeadQueue, *legerLinesQueue;

@property (nonatomic,strong) HarmonyWizMusicEvent *event, *parent;
@property (nonatomic,strong) UIColor *color;
@property (nonatomic,assign,readonly) ScaleNote scaleNote;
@property (nonatomic,assign,readonly) BOOL isNoteOnLine;
@property (nonatomic,assign,readonly) NoteType noteType;
@property (nonatomic,assign,readonly) BOOL stemDown;
@property (nonatomic,assign,readonly) int yoffset;
@property (nonatomic,strong,readonly) NSString *accidentals, *noteWithDots;
@property (nonatomic,assign) float kernAmount;
@property (nonatomic,assign) AccidentalType accidentalType;
@property (nonatomic,assign) BOOL shouldNeutraliseAccidental;
- (NSComparisonResult)compare:(HWEventLabel*)obj;
- (CGFloat)xoffset;
@end

@implementation HWEventLabel
- (void)reset {
    if (self.pauseLabel) {
        [self.pauseLabel removeFromSuperview];
        _pauseLabel = nil;
    }
    if (self.tailLabel) {
        [self.tailLabelQueue addObject:self.tailLabel];
        [self.tailLabel removeFromSuperview];
        _tailLabel = nil;
    }
    if (self.rhythmNoteHead) {
        [self.rhythmNoteHeadQueue addObject:self.rhythmNoteHead];
        [self.rhythmNoteHead removeFromSuperview];
        _rhythmNoteHead = nil;
    }
    if ([self.legerLines count] > 0) {
        [self.legerLinesQueue addObjectsFromArray:self.legerLines];
        [self.legerLines makeObjectsPerformSelector:@selector(removeFromSuperview)];
        _legerLines = nil;
    }
    if (self.noteLabel) {
        self.noteLabel.text = nil;
    }
    _scaleNote = 0;
    _isNoteOnLine = NO;
    _noteType.noteHead = NoteType::kNote_Breve;
    _noteType.dots = 0;
    _stemDown = NO;
    _yoffset = 0;
    _accidentals = nil;
    _noteWithDots = nil;
    self.kernAmount = 4;
    self.accidentalType = kAccidental_None;
    self.shouldNeutraliseAccidental = NO;
    [self clearAccidentalsAndDots];
}
+ (HWEventLabel*)eventLabelFor:(HarmonyWizMusicEvent*)ev withParentEvent:(HarmonyWizMusicEvent*)parent clef:(HWClefLabel*)clef tailLabelQueue:(NSMutableArray*)tailLabelQueue rhythmNoteHeadQueue:(NSMutableArray*)rhythmNoteHeadQueue legerLinesQueue:(NSMutableArray*)legerLinesQueue {
    HWEventLabel *result = [[HWEventLabel alloc] initWithFrame:CGRectMake(0, 0, 64, 128)];
    result.translatesAutoresizingMaskIntoConstraints = NO;
    result->_tailLabelQueue = tailLabelQueue;
    result->_rhythmNoteHeadQueue = rhythmNoteHeadQueue;
    result->_legerLinesQueue = legerLinesQueue;
//    result.layer.borderWidth = 1;
//    result.layer.borderColor = [UIColor redColor].CGColor;
    result->_noteLabel = [[UILabel alloc] initWithFrame:result.bounds];
    result.noteLabel.backgroundColor = [UIColor clearColor];
    result.noteLabel.opaque = NO;
    result.noteLabel.lineBreakMode = NSLineBreakByClipping;
    result.noteLabel.textAlignment = NSTextAlignmentCenter;
    result.color = [UIColor blackColor];
    result.noteLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [result addSubview:result.noteLabel];
    result->_event = ev;
    result->_parent = parent;
    [result reset];
    [result updateForClef:clef];
    return result;
}
- (void)clearAccidentalsAndDots {
    _accidentals = @"";
    _noteWithDots = @"";
}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.noteLabel) {
        CGSize sz = [self.noteLabel sizeThatFits:self.frame.size];
        self.noteLabel.frame = CGRectMake(-sz.width*0.5f, 0, sz.width*2, self.frame.size.height);
    }
    
    if (self.tailLabel) {
        CGSize sz = [self.tailLabel sizeThatFits:self.frame.size];
        self.tailLabel.frame = CGRectMake(-sz.width*0.5f, 0, sz.width*2, self.frame.size.height);
    }
    
    if (self.rhythmNoteHead) {
        CGSize sz = [self.rhythmNoteHead sizeThatFits:CGSizeZero];
        self.rhythmNoteHead.frame = CGRectMake(0, 0, sz.width, self.frame.size.height);
    }
    
    if (self.pauseLabel) {
        CGRect rect = CGRectZero;
        rect.size.width = self.noteLabel.font.pointSize;
        rect.size.height = self.noteLabel.font.pointSize;
        rect.origin.x -= self.noteLabel.font.pointSize*0.25f;
        self.pauseLabel.frame = rect;
    }
    
    for (NSUInteger idx = 0; idx < self.legerLines.count; idx++) {
        UILabel *leger = [self.legerLines objectAtIndex:idx];
        CGFloat ypos = idx * self.noteLabel.font.pointSize * 0.25f;
        if (!self.isNoteOnLine)
            ypos += 0.5f * self.noteLabel.font.pointSize * 0.25f;
        CGFloat xpos = -self.noteLabel.font.pointSize * 0.05f - self.xoffset;
        if (self.noteType.noteHead == NoteType::kNote_Semibreve)
            xpos += self.noteLabel.font.pointSize * 0.05f;
        if (!self.stemDown)
            ypos = -ypos;
        leger.frame = CGRectMake(xpos, ypos, self.frame.size.width, self.frame.size.height);
    }
}
- (void)setFont:(UIFont*)font {
    if (self.noteLabel)
        self.noteLabel.font = font;
    if (self.tailLabel)
        self.tailLabel.font = font;
    if (self.pauseLabel)
        self.pauseLabel.font = font;
    if (self.rhythmNoteHead)
        self.rhythmNoteHead.scale = font.pointSize * 0.08f;
    [self.legerLines makeObjectsPerformSelector:@selector(setFont:) withObject:font];
}
- (void)setColor:(UIColor *)color {
    _color = color;
    if (self.rhythmNoteHead) {
        self.rhythmNoteHead.color = color;
    }
    if (self.noteLabel)
        self.noteLabel.textColor = color;
    if (self.pauseLabel)
        self.pauseLabel.textColor = color;
    if (self.tailLabel)
        self.tailLabel.textColor = color;
    [self.legerLines makeObjectsPerformSelector:@selector(setTextColor:) withObject:color];
}
- (void)updateForClef:(HWClefLabel*)clef {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    _noteType = [doc.song noteTypeFromMusicEvent:self.event];
    
    if ([self.event isKindOfClass:[HarmonyWizNoteEvent class]]) {
        HarmonyWizNoteEvent *ne = (id)self.event;
        HarmonyWizNoteEvent *ne_parent = (id)self.parent;
        ne.noteValue = ne_parent.noteValue;
        _scaleNote = ScaleNote(ne.noteValue,doc.song.keySign.root,doc.song.keySign.tonality==kTonality_Minor);
        const auto noteClass = imod(_scaleNote.note,7);
        if (_scaleNote.alteration == ScaleNote::kAlteration_Sharpen && ne.enharmonicShift == kSharpsToFlats) {
            _scaleNote.enharmonicShift();
        }
        else if (_scaleNote.alteration == ScaleNote::kAlteration_Flatten && ne.enharmonicShift == kFlatsToSharps) {
            _scaleNote.enharmonicShift();
        }
        else if (_scaleNote.alteration == ScaleNote::kAlteration_None && (noteClass == 0 || noteClass == 3) && ne.enharmonicShift == kFlatsToSharps) {
            _scaleNote.alteration = ScaleNote::kAlteration_Sharpen;
            _scaleNote.note--;
        }
        else if (_scaleNote.alteration == ScaleNote::kAlteration_None && (noteClass == 6 || noteClass == 2) && ne.enharmonicShift == kSharpsToFlats) {
            _scaleNote.alteration = ScaleNote::kAlteration_Flatten;
            _scaleNote.note++;
        }
        else if (_scaleNote.alteration == ScaleNote::kAlteration_None && ne.enharmonicShift == kForceDoubleFlat) {
            _scaleNote.forceDoubleFlat();
        }
        else if (_scaleNote.alteration == ScaleNote::kAlteration_None && ne.enharmonicShift == kForceDoubleSharp) {
            _scaleNote.forceDoubleSharp();
        }
        _stemDown = (self.scaleNote.note > clef.middleLine.note);
        const unichar noteChar = (0xF040 + self.noteType.noteHead + (self.stemDown&&self.noteType.noteHead>=NoteType::kNote_Minim ? 0x0010 : 0x0));
        
        NSString *str = [NSString stringWithUnichar:noteChar];
        
        if (self.accidentalType == kAccidental_None) {
            _accidentals = @"";
        }
        else if (self.shouldNeutraliseAccidental) {
            const unichar naturalChar = 0xF023;
            const unichar accidentalChar = 0xF020 + self.accidentalType;
            vector<unichar> accidentalStr = {naturalChar, accidentalChar};
            _accidentals = [NSString stringWithCharacters:&accidentalStr[0] length:accidentalStr.size()];
        }
        else {
            const unichar accidentalChar = 0xF020 + self.accidentalType;
            _accidentals = [NSString stringWithUnichar:accidentalChar];
        }
        
        const unichar dotChar = 0xF0C7;
        const vector<unichar> dotsStr(self.noteType.dots,dotChar);
        _noteWithDots = [str : [NSString stringWithCharacters:&dotsStr[0] length:dotsStr.size()]];
        NSString *text = [self.accidentals : self.noteWithDots];
        NSDictionary *attrs = (text.length > 1 ? @{ NSKernAttributeName : @(self.kernAmount) } : @{});
        self.noteLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:text
                                                                               attributes:attrs];
        
        _yoffset = (clef.middleLine.note - self.scaleNote.note);
        
        UInt8 numLegerLines = 0;
        if (self.scaleNote.note > clef.topSpace.note) {
            numLegerLines = (self.scaleNote.note - clef.topSpace.note + 1)/2;
        }
        else if (self.scaleNote.note < clef.bottomSpace.note) {
            numLegerLines = (clef.bottomSpace.note - self.scaleNote.note + 1)/2;
        }
        [self.legerLines makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.legerLinesQueue addObjectsFromArray:self.legerLines];
        _legerLines = nil;
        if (numLegerLines > 0) {
            UILabel *arr[numLegerLines];
            for (UInt8 idx = 0; idx < numLegerLines; idx++) {
                UILabel *legerLabel = [self.legerLinesQueue firstObject];
                if (legerLabel == nil) {
                    legerLabel = [[UILabel alloc] initWithFrame:self.bounds];
                    legerLabel.backgroundColor = [UIColor clearColor];
                    legerLabel.lineBreakMode = NSLineBreakByClipping;
                    legerLabel.textAlignment = NSTextAlignmentLeft;
                    legerLabel.textColor = self.color;
                    const unichar legerChar = 0xF06E;
                    legerLabel.text = [NSString stringWithUnichar:legerChar];
                }
                else {
                    [self.legerLinesQueue removeObject:legerLabel];
                }
                arr[idx] = legerLabel;
                legerLabel.translatesAutoresizingMaskIntoConstraints = NO;
                [self addSubview:legerLabel];
            }
            _legerLines = [NSArray arrayWithObjects:arr count:numLegerLines];
        }
        else {
            _legerLines = @[];
        }
        _isNoteOnLine = (clef.middleLine.note - self.scaleNote.note + 1)%2;
    }
    else if ([self.event isKindOfClass:[HarmonyWizRestEvent class]] || ([self.event isKindOfClass:[HarmonyWizHarmonyEvent class]] && [(HarmonyWizHarmonyEvent*)self.event noChord])) {
        const unichar noteChar = 0xF060 + self.noteType.noteHead;
        NSString *str = [NSString stringWithUnichar:noteChar];
        _yoffset = 0;
        if (self.noteType.noteHead == NoteType::NoteHeadType::kNote_Quaver)
            _yoffset--;
        else if (self.noteType.noteHead >= NoteType::NoteHeadType::kNote_Semiquaver)
            _yoffset++;
        
        const unichar dotChar = 0xF0C7;
        const vector<unichar> dotsStr(self.noteType.dots,dotChar);
        _noteWithDots = [str : [NSString stringWithCharacters:&dotsStr[0] length:dotsStr.size()]];
        NSString *text = [self.accidentals : self.noteWithDots];
        NSDictionary *attrs = (text.length > 1 ? @{ NSKernAttributeName : @(self.kernAmount) } : @{});
        self.noteLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:text
                                                                               attributes:attrs];
    }
    else if ([self.event isKindOfClass:[HarmonyWizHarmonyEvent class]]) {
        if (! [(HarmonyWizHarmonyEvent*)self.parent noChord] && self.rhythmNoteHead == nil) {
            _rhythmNoteHead = [self.rhythmNoteHeadQueue firstObject];
            if (_rhythmNoteHead == nil) {
                _rhythmNoteHead = [HWRhythmNoteHeadView rhythmNoteHead];
            }
            else {
                [self.rhythmNoteHeadQueue removeObject:self.rhythmNoteHead];
            }
            self.rhythmNoteHead.translatesAutoresizingMaskIntoConstraints = NO;
            [self addSubview:self.rhythmNoteHead];
        }
        
        if ([(HarmonyWizHarmonyEvent*)self.parent pause] && self.pauseLabel == nil) {
            _pauseLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            self.pauseLabel.opaque = NO;
            self.pauseLabel.backgroundColor = [UIColor clearColor];
            self.pauseLabel.lineBreakMode = NSLineBreakByClipping;
            self.pauseLabel.textAlignment = NSTextAlignmentCenter;
            self.pauseLabel.textColor = self.color;
            self.pauseLabel.text = [NSString stringWithUnichar:0xF0B2];
            [self addSubview:self.pauseLabel];
        }
        
        _stemDown = NO;
        const unichar noteChar = (self.noteType.noteHead>=NoteType::kNote_Minim ? 0xF06A : ' ');
        
        if (self.noteType.noteHead>=NoteType::kNote_Quaver) {
            if (_tailLabel == nil) {
                _tailLabel = [self.tailLabelQueue firstObject];
                if (_tailLabel == nil) {
                    _tailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
                    self.tailLabel.opaque = NO;
                    self.tailLabel.backgroundColor = [UIColor clearColor];
                    self.tailLabel.lineBreakMode = NSLineBreakByClipping;
                    self.tailLabel.textAlignment = NSTextAlignmentCenter;
                    self.tailLabel.textColor = self.color;
                }
                else {
                    [self.tailLabelQueue removeObject:self.tailLabel];
                }
                self.tailLabel.translatesAutoresizingMaskIntoConstraints = NO;
                [self addSubview:self.tailLabel];
            }
            _tailLabel.text = [NSString stringWithUnichar:0xF049 + self.noteType.noteHead - NoteType::kNote_Quaver];
        }
        
        if (self.rhythmNoteHead) {
            self.rhythmNoteHead.filled = (self.noteType.noteHead > NoteType::kNote_Minim);
        }
        
        NSString *str = [NSString stringWithUnichar:noteChar];
        
        _accidentals = @"";
        
        const unichar dotChar = 0xF0C7;
        const vector<unichar> dotsStr(self.noteType.dots,dotChar);
        _noteWithDots = [str : [NSString stringWithCharacters:&dotsStr[0] length:dotsStr.size()]];
        self.noteLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:
                                         [self.accidentals : self.noteWithDots] attributes:@{ NSKernAttributeName : @(self.kernAmount) }];
        
        _yoffset = 0;
    }
}
- (NSComparisonResult)compare:(HWEventLabel*)obj {
    return [self.event compare:obj.event];
}
- (CGFloat)xoffset {
    if (self.accidentals.length > 0) {
        CGRect rect1 = [[self.accidentals : self.noteWithDots] iOS6_boundingRectWithSize:self.bounds.size options:(NSStringDrawingOptions)0 attributes:@{ NSFontAttributeName : self.noteLabel.font, NSKernAttributeName : @(self.kernAmount) } context:nil];
        CGRect rect2 = [self.noteWithDots iOS6_boundingRectWithSize:self.bounds.size options:(NSStringDrawingOptions)0 attributes:@{ NSFontAttributeName : self.noteLabel.font, NSKernAttributeName : @(self.kernAmount) } context:nil];
        return (CGRectGetWidth(rect2) - CGRectGetWidth(rect1));
    }
    
    return 0;
}
@end

#pragma mark - event ties

@interface HWEventTie : UIView
@property (nonatomic,strong) UIColor *color;
@property (nonatomic,strong) HWEventLabel *labelA, *labelB;
@property (nonatomic,assign,readonly) BOOL stemDown;
@property (nonatomic,weak,readonly) HWTimeRulerScrollView *rulerControl;
@end

@implementation HWEventTie
+ (HWEventTie*)eventTieForLabel:(HWEventLabel*)labelA to:(HWEventLabel*)labelB ruler:(HWTimeRulerScrollView*)ruler {
    HWEventTie *result = [[HWEventTie alloc] initWithFrame:CGRectZero];
    result.translatesAutoresizingMaskIntoConstraints = NO;
    result.opaque = NO;
    result.labelA = labelA;
    result.labelB = labelB;
    result->_rulerControl = ruler;
    result->_stemDown = labelA.stemDown;
    result.contentMode = UIViewContentModeRedraw;
    result.backgroundColor = [UIColor clearColor];
//    result.layer.borderWidth = 1;
//    result.layer.borderColor = [UIColor greenColor].CGColor;
    return result;
}
- (void)setColor:(UIColor *)color {
    _color = color;
    [self setNeedsDisplay];
}
- (void)setLabelA:(HWEventLabel *)labelA {
    _labelA = labelA;
    self.color = labelA.color;
}
- (void)setLabelB:(HWEventLabel *)labelB {
    _labelB = labelB;
    self.color = labelB.color;
}
- (void)computeFrameInStave:(HarmonyWizStaveView*)stave {
    CGRect frameA = self.labelA.frame;
    CGRect frameB = self.labelB.frame;
    frameA.origin.x -= stave.touchableFrame.origin.x;
    frameA.origin.y -= stave.touchableFrame.origin.y;
    frameB.origin.x -= stave.touchableFrame.origin.x;
    frameB.origin.y -= stave.touchableFrame.origin.y;
    CGRect frame = CGRectUnion(frameA, frameB);
    self.frame = frame;
}
- (void)update {
    if (self.labelA.stemDown != self.stemDown) {
        _stemDown = self.labelA.stemDown;
        [self setNeedsDisplay];
    }
}
- (void)drawRect:(CGRect)rect {
    CGFloat ypos = CGRectGetMidY(self.labelA.frame) - self.rulerControl.staveLineSpacing * 1.f;
    if (!self.stemDown)
        ypos += self.rulerControl.staveLineSpacing * 2.f;
    
    CGPoint p1 = CGPointMake([self.rulerControl xposFromEvent:self.labelA.event], ypos);
    CGPoint p2 = CGPointMake([self.rulerControl xposFromEvent:self.labelB.event], ypos);
    CGPoint cp1 = CGPointMake(0.5f*p1.x+0.5f*p2.x, ypos + self.rulerControl.staveLineSpacing * (self.stemDown?-3.f:3.f));
    
    p1 = [self convertPoint:p1 fromView:self.superview];
    p2 = [self convertPoint:p2 fromView:self.superview];
    cp1 = [self convertPoint:cp1 fromView:self.superview];
    
    p1.x += self.rulerControl.staveLineSpacing*0.75f;
    p2.x += self.rulerControl.staveLineSpacing*0.75f;
    cp1.x += self.rulerControl.staveLineSpacing*0.75f;
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:p1];
    [bezierPath addQuadCurveToPoint:p2 controlPoint:cp1];
    cp1.y += self.rulerControl.staveLineSpacing * (self.stemDown?-0.5f:0.5f);
    [bezierPath addQuadCurveToPoint:p1 controlPoint:cp1];
    [self.color set];
    [bezierPath fill];
}
@end

#pragma mark - harmony label

@interface HWHarmonyLabel : UILabel
@property (nonatomic,strong) HWEventLabel *eventLabel;
@end

@implementation HWHarmonyLabel
+ (HWHarmonyLabel*)harmonyLabelFor:(HWEventLabel*)eventLabel {
    HWHarmonyLabel *obj = [[HWHarmonyLabel alloc] initWithFrame:CGRectZero];
    obj.translatesAutoresizingMaskIntoConstraints = NO;
    obj.opaque = NO;
    obj->_eventLabel = eventLabel;
    [obj update];
    return obj;
}
- (void)update {
    HarmonyWizHarmonyEvent *harmony = (id)self.eventLabel.parent;
    if (harmony.properties && harmony.properties.count > 0) {
        self.text = [self stringForProperties:harmony.properties showType:NO showRootPosition:NO];
        if ([harmony.constraints.allKeys containsObject:HarmonyWizRelaxRulesProperty]) {
            self.textColor = [UIColor purpleColor];
        }
        else {
            self.textColor = [UIColor darkTextColor];
        }
        self.font = [UIFont systemFontOfSize:12];
    }
    else if (harmony.constraints && harmony.constraints.count > 0) {
        self.text = [self stringForProperties:harmony.constraints showType:YES showRootPosition:YES];
        self.textColor = [UIColor brownColor];
        self.font = [UIFont boldSystemFontOfSize:12];
        NSString *leading = [harmony.constraints objectForKey:HarmonyWizLeadingNoteTreatmentProperty];
    }
    else if (harmony.temporaryConstraints && harmony.temporaryConstraints.count > 0) {
        self.text = [self stringForProperties:harmony.temporaryConstraints showType:YES showRootPosition:YES];
        self.textColor = [UIColor orangeColor];
        self.font = [UIFont boldSystemFontOfSize:12];
    }
    else {
        self.text = @"";
    }
}

- (NSString*)stringForProperties:(NSDictionary*)properties showType:(BOOL)showType showRootPosition:(BOOL)showRootPosition {
    // relax
    NSString *relaxString = (NSString *)[NSNull null];
    if ([[properties objectForKey:HarmonyWizRelaxRulesProperty] boolValue]) {
        relaxString = @"r";
    }
    
    // degree
    NSValue *degree = [properties objectForKey:HarmonyWizDegreeProperty];
    NSString *degreeString = (NSString *)[NSNull null];
    if (degree)
        degreeString = ScaleNoteToDegreeString(degree.HWScaleNoteValue);
    
    NSString *typeString = (NSString *)[NSNull null];
    if (showType) {
        NSValue *type = [properties objectForKey:HarmonyWizChordTypeProperty];
        if (type)
            typeString = ChordTypeToString(type.HWChordTypeValue);
    }
    
    // inversion
    NSValue *inversion = [properties objectForKey:HarmonyWizInversionProperty];
    NSString *inversionString = (NSString *)[NSNull null];
    if (inversion && (inversion.HWChordInversionValue > 0 || showRootPosition))
        inversionString = [NSString stringWithFormat:@"%ui", inversion.HWChordInversionValue];
    
    // third
    NSValue *third = [properties objectForKey:HarmonyWizThirdProperty];
    NSString *thirdString = (NSString *)[NSNull null];
    if (third) {
        if (! third.HWScaleNoteValue.isUnaltered()) {
            thirdString = [NSString stringWithFormat:@"%@3", ScaleNoteToAlterationString(third.HWScaleNoteValue)];
        }
    }
    
    // fifth
    NSValue *fifth = [properties objectForKey:HarmonyWizFifthProperty];
    NSString *fifthString = (NSString *)[NSNull null];
    if (fifth) {
        if (! fifth.HWScaleNoteValue.isUnaltered())
            fifthString = [NSString stringWithFormat:@"%@5", ScaleNoteToAlterationString(fifth.HWScaleNoteValue)];
    }
    
    // seventh
    NSValue *seventh = [properties objectForKey:HarmonyWizSevenProperty];
    NSString *seventhString = (NSString *)[NSNull null];
    if (seventh)
        seventhString = [NSString stringWithFormat:@"%@7", ScaleNoteToAlterationString(seventh.HWScaleNoteValue)];
    
    // ninth
    NSValue *ninth = [properties objectForKey:HarmonyWizNineProperty];
    NSString *ninthString = (NSString *)[NSNull null];
    if (ninth)
        ninthString = [NSString stringWithFormat:@"%@9", ScaleNoteToAlterationString(ninth.HWScaleNoteValue)];
    
    // eleventh
    NSValue *eleventh = [properties objectForKey:HarmonyWizElevenProperty];
    NSString *eleventhString = (NSString *)[NSNull null];
    if (eleventh)
        eleventhString = [NSString stringWithFormat:@"%@11", ScaleNoteToAlterationString(eleventh.HWScaleNoteValue)];
    
    // thirteenth
    NSValue *thirteenth = [properties objectForKey:HarmonyWizThirteenProperty];
    NSString *thirteenthString = (NSString *)[NSNull null];
    if (thirteenth)
        thirteenthString = [NSString stringWithFormat:@"%@13", ScaleNoteToAlterationString(thirteenth.HWScaleNoteValue)];
    
    // create properties string
    NSMutableArray *allPropertiesString = [NSMutableArray arrayWithObjects:typeString, inversionString, thirdString, fifthString, seventhString, ninthString, eleventhString, thirteenthString, nil];
    [allPropertiesString removeObjectIdenticalTo:[NSNull null]];
    NSString *propertiesString = [allPropertiesString componentsJoinedByString:@", "];
    
    NSMutableArray *components = [@[degreeString, propertiesString, relaxString] mutableCopy];
    [components removeObjectIdenticalTo:[NSNull null]];
    return [components componentsJoinedByString:@"\n"];
}

static NSString *ScaleNoteToAlterationString(const ScaleNote &sn) {
    NSString *alt = @"";
    if (sn.isSharpened())
        alt = @"#";
    else if (sn.isFlattened())
        alt = @"♭";
    return alt;
}

static NSString *ScaleNoteToDegreeString(const ScaleNote &sn) {
    NSString *prefix = ScaleNoteToAlterationString(sn);
    return [prefix stringByAppendingString:[kScaleDegrees objectAtIndex:sn.note]];
}

static NSString *ChordTypeToString(Figures::ChordType type) {
    NSString *result = (NSString *)[NSNull null];
    switch (type) {
        case Figures::kTriad: result = @"T"; break;
        case Figures::kSeven: result = @"7"; break;
        case Figures::kAddTwo: result = @"+2"; break;
        case Figures::kAddFour: result = @"+4"; break;
        case Figures::kAddSix: result = @"+6"; break;
        case Figures::kNine: result = @"9"; break;
        case Figures::kEleven: result = @"11"; break;
        case Figures::kThirteen: result = @"13"; break;
        
        default:
            break;
    }
    return result;
}
@end

#pragma mark - barline

@interface HWBarLine : UILabel
@property (nonatomic,assign) NSUInteger barNumber;
@property (nonatomic,assign) enum BarlineKind {
kBarline_Thin = 0,
kBarline_Thick,
kBarline_Start,
kBarline_End,
kBarline_NumTypes} kind;
@end

@implementation HWBarLine
+ (HWBarLine*)barLineFor:(NSUInteger)num kind:(enum BarlineKind)kind {
    HWBarLine *result = [[HWBarLine alloc] initWithFrame:CGRectMake(0, 0, 64, 128)];
    result.translatesAutoresizingMaskIntoConstraints = NO;
    result.opaque = NO;
    result->_barNumber = num;
    result.backgroundColor = [UIColor clearColor];
    result.lineBreakMode = NSLineBreakByClipping;
    result.textAlignment = NSTextAlignmentLeft;
    result.textColor = [UIColor blackColor];
    result->_kind = kBarline_NumTypes;
    result.kind = kind;
    return result;
}
- (void)setKind:(enum BarlineKind)kind {
    if (_kind != kind) {
        _kind = kind;
        const unichar barChar = 0xF0BB + kind;
        self.text = [NSString stringWithUnichar:barChar];
    }
}
- (NSComparisonResult)compare:(HWBarLine*)obj {
    if (self.barNumber < obj.barNumber)
        return NSOrderedAscending;
    else if (self.barNumber > obj.barNumber)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
- (NSComparisonResult)compareWithValue:(NSNumber*)obj {
    if (self.barNumber < obj.integerValue)
        return NSOrderedAscending;
    else if (self.barNumber > obj.integerValue)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
@end

#pragma mark - time signature

@interface HWTimeSignature : UIView
@property (nonatomic,assign) NSUInteger upperNumeral, lowerNumeral;
@property (nonatomic,strong,readonly) UILabel *upperLabel, *lowerLabel;
@property (nonatomic,strong) UIColor *color;
+ (HWTimeSignature*)timeSignatureWithUpper:(NSUInteger)upper lower:(NSUInteger)lower;
@end

@implementation HWTimeSignature
+ (HWTimeSignature*)timeSignatureWithUpper:(NSUInteger)upper lower:(NSUInteger)lower {
    HWTimeSignature *result = [[HWTimeSignature alloc] initWithFrame:CGRectMake(0, 0, 64, 128)];
    result.translatesAutoresizingMaskIntoConstraints = NO;
    result.opaque = NO;
    result->_upperLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 64, 64)];
    result->_lowerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, 64, 64)];
    result.backgroundColor = [UIColor clearColor];
    result.upperLabel.backgroundColor = [UIColor clearColor];
    result.upperLabel.lineBreakMode = NSLineBreakByClipping;
    result.upperLabel.textAlignment = NSTextAlignmentCenter;
    result.lowerLabel.backgroundColor = [UIColor clearColor];
    result.lowerLabel.lineBreakMode = NSLineBreakByClipping;
    result.lowerLabel.textAlignment = NSTextAlignmentCenter;
    result.color = [UIColor blackColor];
    result.upperNumeral = upper;
    result.lowerNumeral = lower;
    result.upperLabel.translatesAutoresizingMaskIntoConstraints = NO;
    result.lowerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [result addSubview:result.upperLabel];
    [result addSubview:result.lowerLabel];
    return result;
}
- (void)setFont:(UIFont*)font {
    self.upperLabel.font = font;
    self.lowerLabel.font = font;
//    [self.subviews makeObjectsPerformSelector:@selector(setFont:) withObject:font];
}
- (void)setColor:(UIColor *)color {
    _color = color;
    self.upperLabel.textColor = color;
    self.lowerLabel.textColor = color;
}
- (void)setUpperNumeral:(NSUInteger)upperNumeral {
    _upperNumeral = upperNumeral;
    unichar str[32];
    UInt8 len = [self timeSign:upperNumeral toUnicodeString:str];
    self.upperLabel.text = [NSString stringWithCharacters:str length:len];
}
- (void)setLowerNumeral:(NSUInteger)lowerNumeral {
    _lowerNumeral = lowerNumeral;
    unichar str[32];
    UInt8 len = [self timeSign:lowerNumeral toUnicodeString:str];
    self.lowerLabel.text = [NSString stringWithCharacters:str length:len];
}
- (UInt8)timeSign:(UInt8)number toUnicodeString:(unichar*)str {
    const char *numStr0 = [[NSString stringWithFormat:@"%u", number] UTF8String];
    UInt8 i;
    for (i = 0; numStr0[i] != '\0'; i++)
        str[i] = numStr0[i] - '0' + 0xF030;
    str[i] = 0;
    return i;
}
@end

#pragma mark - key signature

@interface HWKeySignature : UIView
@property (nonatomic,assign) NSInteger key;
+ (HWKeySignature*)keySignatureWith:(NSInteger)aKey ruler:(HWTimeRulerScrollView*)ruler clef:(HWClefLabel*)clef;
@property (nonatomic,weak,readonly) HWTimeRulerScrollView *rulerControl;
@property (nonatomic,weak,readonly) HWClefLabel *clefLabel;
@property (nonatomic,strong) UIColor *color;
@end

@implementation HWKeySignature
+ (HWKeySignature*)keySignatureWith:(NSInteger)aKey ruler:(HWTimeRulerScrollView*)ruler clef:(HWClefLabel*)clef {
    HWKeySignature *result = [[HWKeySignature alloc] initWithFrame:CGRectMake(0, 0, 128, 128)];
    result.translatesAutoresizingMaskIntoConstraints = NO;
    result.opaque = NO;
    result->_rulerControl = ruler;
    result->_clefLabel = clef;
    result.backgroundColor = [UIColor clearColor];
    result.key = aKey;
    result.color = [UIColor blackColor];
    return result;
}
- (void)setFont:(UIFont*)font {
    [self.subviews makeObjectsPerformSelector:@selector(setFont:) withObject:font];
}
- (void)setColor:(UIColor *)color {
    _color = color;
    [self.subviews makeObjectsPerformSelector:@selector(setTextColor:) withObject:color];
}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    const CGFloat accidentalMargin = 0.25f*self.rulerControl.staveLineSpacing;
    const CGFloat dx = self.rulerControl.staveLineSpacing*(self.key<0?0.6f:1.f) + accidentalMargin;
    const SInt8 sharpsFlats = self.key;
    
    CGFloat xpos = 0;
    SInt16 note = 0;
    SInt16 bound = 0;
    if (sharpsFlats < 0) {
        switch (self.clefLabel.kind) {
            case kClef_Treble:  note = 0; bound = 4;   break;
            case kClef_Alto:    note = -1; bound = 3;  break;
            case kClef_Tenor:   note = 1; bound = 4;   break;
            case kClef_Bass:    note = -2; bound = 2;  break;
            default: break;
        }
        for (UILabel *label in self.subviews) {
            CGFloat ypos = (self.rulerControl.staveLineSpacing * (-note)*0.5f) + self.rulerControl.staveLineSpacing * 2.f;
            label.frame = CGRectMake(xpos, ypos, 64, self.bounds.size.height);
            xpos += dx;
            note += 3;
            if (note > bound)
                note -= 7;
        }
    }
    else if (sharpsFlats > 0) {
        switch (self.clefLabel.kind) {
            case kClef_Treble:  note = 4; bound = 5;   break;
            case kClef_Alto:    note = 3; bound = 4;  break;
            case kClef_Tenor:   note = -2; bound = 4;   break;
            case kClef_Bass:    note = 2; bound = 4;  break;
            default: break;
        }
        for (UILabel *label in self.subviews) {
            CGFloat ypos = (self.rulerControl.staveLineSpacing * (-note)*0.5f) + self.rulerControl.staveLineSpacing * 2.f;
            label.frame = CGRectMake(xpos, ypos, 64, self.bounds.size.height);
            xpos += dx;
            note += 4;
            if (note > bound)
                note -= 7;
        }
    }
    
}
- (void)setKey:(NSInteger)key {
    const NSUInteger absKey = ABS(key);
    if (key * _key < 0) {
        // if changing from sharps to flats or visa versa remove all
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    _key = key;
    if (self.subviews.count > absKey) {
        [[self.subviews subarrayWithRange:NSMakeRange(0, self.subviews.count-absKey)] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    const unichar keyChar = (self.key>=0?0xF021:0xF022);
    for (UInt8 i = self.subviews.count; i < absKey; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 64, 64)];
        label.backgroundColor = [UIColor clearColor];
        label.lineBreakMode = NSLineBreakByClipping;
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor blackColor];
        label.text = [NSString stringWithUnichar:keyChar];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:label];
    }
    [self setNeedsLayout];
}
@end

#pragma mark - stave view

@interface HWStaveLines : NSObject
@property (nonatomic,assign) NSInteger barNumber;
@property (nonatomic,strong) NSArray *lines;
@property (nonatomic,assign) BOOL enabled;
- (void)removeLinesFromSuperview;
- (NSComparisonResult)compare:(HWStaveLines*)obj;
@end

@implementation HWStaveLines
- (void)removeLinesFromSuperview {
    [self.lines makeObjectsPerformSelector:@selector(removeFromSuperview)];
}
- (void)setEnabled:(BOOL)enabled {
    if (_enabled != enabled) {
        _enabled = enabled;
        if (enabled) {
            [self.lines makeObjectsPerformSelector:@selector(setBackgroundColor:) withObject:[UIColor blackColor]];
        }
        else {
            [self.lines makeObjectsPerformSelector:@selector(setBackgroundColor:) withObject:[UIColor lightGrayColor]];
        }
    }
}
- (NSComparisonResult)compare:(HWStaveLines*)obj {
    if (self.barNumber < obj.barNumber)
        return NSOrderedAscending;
    else if (self.barNumber > obj.barNumber)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
- (NSComparisonResult)compareWithValue:(NSNumber*)obj {
    if (self.barNumber < obj.integerValue)
        return NSOrderedAscending;
    else if (self.barNumber > obj.integerValue)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}
@end

#pragma mark - stave view

@interface HarmonyWizStaveView ()
@property (nonatomic,weak) HWTimeRulerScrollView *rulerControl;
@property (nonatomic,strong) NSMutableArray *staveLineViews;
@property (nonatomic,strong) NSMutableArray *staveEvents;
@property (nonatomic,strong) NSMutableArray *staveBarlines;
@property (nonatomic,assign) BOOL enabled;
@end

@implementation HarmonyWizStaveView

- (NSMutableArray*)staveLineViewQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    if (self.numberOfStaveLines == 1)
        return [self.parentScrollView singleStaveLineViewQueue];
    else
        return [self.parentScrollView staveLineViewQueue];
}
- (NSMutableArray*)staveEventLabelQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    return [self.parentScrollView staveEventLabelQueue];
}
- (NSMutableArray*)staveTieQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    return [self.parentScrollView staveTieQueue];
}
- (NSMutableArray*)staveHarmonyInfoLabelQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    return [self.parentScrollView staveHarmonyInfoLabelQueue];
}
- (NSMutableArray*)staveBarLineQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    return [self.parentScrollView staveBarLineQueue];
}
- (NSMutableArray*)tailLabelQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    return [self.parentScrollView tailLabelQueue];
}
- (NSMutableArray*)rhythmNoteHeadQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    return [self.parentScrollView rhythmNoteHeadQueue];
}
- (NSMutableArray*)legerLinesQueue {
    NSAssert(self.parentScrollView != nil, @"no container view");
    return [self.parentScrollView legerLinesQueue];
}

- (NSArray*)allViews {
    NSMutableArray *result = [NSMutableArray array];
    for (HWStaveLines *staveLines in self.staveLineViews) {
        [result addObjectsFromArray:staveLines.lines];
    }
    for (HWEvent *event in self.staveEvents) {
        [result addObjectsFromArray:event.labels];
        [result addObjectsFromArray:event.ties];
        [result addObjectsFromArray:event.harmonyInfoLabels];
    }
    [result addObjectsFromArray:self.staveBarlines];
    if (self.keySignatureView)
        [result addObject:self.keySignatureView];
    if (self.timeSignatureView)
        [result addObject:self.timeSignatureView];
    if (self.clefLabel)
        [result addObject:self.clefLabel];
    return result;
}

- (void)removeAllViewsFromSuperview {
    [self.allViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (HWEvent *obj in self.staveEvents) {
        [obj.labels makeObjectsPerformSelector:@selector(reset)];
        [self.staveEventLabelQueue addObjectsFromArray:obj.labels];
        [self.staveTieQueue addObjectsFromArray:obj.ties];
        [self.staveHarmonyInfoLabelQueue addObjectsFromArray:obj.harmonyInfoLabels];
    }
    [self.staveEvents removeAllObjects];
    
    [self.staveLineViewQueue addObjectsFromArray:self.staveLineViews];
    [self.staveLineViews removeAllObjects];
    
    [self.staveBarLineQueue addObjectsFromArray:self.staveBarlines];
    [self.staveBarlines removeAllObjects];
    
    self.keySignatureView = nil;
    self.timeSignatureView = nil;
    self.clefLabel = nil;
}

- (NSArray*)allEventLabels {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:self.staveEvents.count];
    for (NSArray *arr in [self.staveEvents valueForKey:@"labels"]) {
        [result addObjectsFromArray:arr];
    }
    return result;
}

- (void)removeUnusedEvents {
    NSArray *unused = [self.staveEvents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"! (event IN %@)", self.track.events]];
    for (HWEvent *obj in unused) {
//        [obj removeViewsFromSuperview];
        [obj.labels makeObjectsPerformSelector:@selector(reset)];
        [self.staveEventLabelQueue addObjectsFromArray:obj.labels];
        [self.staveTieQueue addObjectsFromArray:obj.ties];
        [self.staveHarmonyInfoLabelQueue addObjectsFromArray:obj.harmonyInfoLabels];
    }
    [self.staveEvents removeObjectsInArray:unused];
}

- (HWEvent*)generateStaveEventFor:(HarmonyWizMusicEvent*)event middleLine:(const ScaleNote)middleLine  {
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    NSArray *splitEvents = [song recursivelySplitEvent:event];
    NSMutableArray *labels = [NSMutableArray arrayWithCapacity:splitEvents.count];
    NSMutableArray *ties = [NSMutableArray arrayWithCapacity:splitEvents.count];
    NSMutableArray *harmonyInfos = ([event isKindOfClass:[HarmonyWizHarmonyEvent class]] ? [NSMutableArray arrayWithCapacity:splitEvents.count] : [NSMutableArray array]);
    for (HarmonyWizMusicEvent *splitEv in splitEvents) {
        HWEventLabel *label = [self.staveEventLabelQueue firstObject];
        if (label == nil) {
            label = [HWEventLabel eventLabelFor:splitEv
                                withParentEvent:event
                                           clef:self.clefLabel
                                 tailLabelQueue:self.tailLabelQueue
                            rhythmNoteHeadQueue:self.rhythmNoteHeadQueue
                                legerLinesQueue:self.legerLinesQueue];
            
            label.font = self.notesFont;
        }
        else {
            [label reset];
            label.alpha = 1.f;
            label.event = splitEv;
            label.parent = event;
            [label updateForClef:self.clefLabel];
            [label setNeedsLayout];
            [self.staveEventLabelQueue removeObject:label];
        }
        label.color = [self eventColour:event];
        [labels placeObject:label];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        [self.parentScrollView.contentView addSubview:label];
        
        if ([splitEv isKindOfClass:[HarmonyWizHarmonyEvent class]] && ! [(HarmonyWizHarmonyEvent*)event noChord]) {
            HWHarmonyLabel *harmonyLabel = [self.staveHarmonyInfoLabelQueue firstObject];
            if (harmonyLabel == nil) {
                harmonyLabel = [HWHarmonyLabel harmonyLabelFor:label];
                harmonyLabel.backgroundColor = [UIColor clearColor];
                harmonyLabel.textAlignment = NSTextAlignmentCenter;
                harmonyLabel.lineBreakMode = NSLineBreakByWordWrapping;
                harmonyLabel.numberOfLines = 0;
            }
            else {
                [self.staveHarmonyInfoLabelQueue removeObject:harmonyLabel];
                harmonyLabel.eventLabel = label;
                [harmonyLabel update];
                harmonyLabel.alpha = 1.f;
            }
            [harmonyInfos addObject:harmonyLabel];
            harmonyLabel.translatesAutoresizingMaskIntoConstraints = NO;
            [self.parentScrollView.contentView addSubview:harmonyLabel];
        }
    }
    if ([event isKindOfClass:[HarmonyWizNoteEvent class]] || ([event isKindOfClass:[HarmonyWizHarmonyEvent class]] && ![(HarmonyWizHarmonyEvent*)event noChord])) {
        for (NSUInteger idx = 1; idx < labels.count; idx++) {
            HWEventTie *tie = [self.staveTieQueue firstObject];
            if (tie == nil) {
                tie = [HWEventTie eventTieForLabel:[labels objectAtIndex:idx-1]
                                                        to:[labels objectAtIndex:idx]
                                                     ruler:self.rulerControl];
            }
            else {
                [self.staveTieQueue removeObject:tie];
                tie.labelA = [labels objectAtIndex:idx-1];
                tie.labelB = [labels objectAtIndex:idx];
                [tie update];
                tie.alpha = 1.f;
            }
            [ties addObject:tie];
            tie.translatesAutoresizingMaskIntoConstraints = NO;
            [self.parentScrollView.contentView addSubview:tie];
        }
    }
    
    HWEvent *hwEvent = [HWEvent eventFor:event];
    hwEvent.labels = labels;
    hwEvent.ties = ties;
    hwEvent.harmonyInfoLabels = harmonyInfos;
    [self.staveEvents placeObject:hwEvent];
    
    return hwEvent;
}

- (BOOL)isEventVisible:(HarmonyWizMusicEvent*)event {
    
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    // fix a weird bug where crash occurs when exiting song while looking at last measure on stave
    if (doc.song == nil)
        return NO;
    
    const SInt16 leftBar = (self.parentScrollView).leftMostVisibleMeasure;
    const SInt16 rightBar = (self.parentScrollView).rightMostVisibleMeasure;
    
    const UInt16 barNumStarts = [doc.song barNumberFromEvent:event];
    const UInt16 subdivEnd = [doc.song eventEndSubdivision:event];
    const UInt16 barNumEnd = (subdivEnd / doc.song.beatSubdivisions) / doc.song.timeSign.upperNumeral - (subdivEnd % (doc.song.beatSubdivisions * doc.song.timeSign.upperNumeral) == 0 ? 1 : 0);
    
    if (MAX(leftBar, barNumStarts) <= MIN(rightBar, barNumEnd)) {
        return YES;
    }
    return NO;
}

- (void)generateStaveEvents {
    const SInt16 leftBar = (self.parentScrollView).leftMostVisibleMeasure;
    const SInt16 rightBar = (self.parentScrollView).rightMostVisibleMeasure;
    
    NSArray *labelledEvents = [self.staveEvents valueForKey:@"event"];
    
    NSMutableArray *generatedHWEvents = [NSMutableArray array];
    HarmonyWizSong *song = [HWCollectionViewController currentlyOpenDocument].song;
    std::vector<NSMutableArray *> bars(rightBar - leftBar + 1, nil);
    for (HarmonyWizMusicEvent *ev in self.track.events) {
        if ([self isEventVisible:ev]) {
            HWEvent *hwEvent = nil;
            if ([labelledEvents containsObject:ev]) {
                for (HWEvent *hwev in self.staveEvents) {
                    if (hwev.event == ev) {
                        hwEvent = hwev;
                        break;
                    }
                }
            }
            else {
                hwEvent = [self generateStaveEventFor:ev middleLine:self.clefLabel.middleLine];
                [generatedHWEvents addObject:hwEvent];
            }
            if ([hwEvent.event isKindOfClass:[HarmonyWizNoteEvent class]]) {
                for (HWEventLabel *eventLabel in hwEvent.labels) {
                    int barNum = [song barNumberFromEvent:eventLabel.event];
                    if (leftBar <= barNum && barNum <= rightBar) {
                        const UInt16 idx = barNum - leftBar;
                        if (bars.at(idx) == nil) {
                            bars.at(idx) = [NSMutableArray array];
                        }
                        [bars.at(idx) addObject:eventLabel];
                    }
                }
            }
        }
    }
    
    for (int idx = 0; idx < bars.size(); idx++) {
        if (bars.at(idx) != nil) {
            [self updateAccidentalsFor:bars.at(idx)];
        }
    }
    
    for (HWEvent *hwEvent in generatedHWEvents) {
        [self positionEventInScore:hwEvent];
    }
}

- (void)positionEventInScore:(HWEvent*)hwEvent {
    for (id obj in hwEvent.labels) {
        [self layoutEventLabel:obj];
        CGRect frame = [(self.parentScrollView) convertRect:[obj frame] fromStave:self];
        [obj setFrame:frame];
    }
    for (id obj in hwEvent.ties) {
        [obj computeFrameInStave:self];
        CGRect frame = [(self.parentScrollView) convertRect:[obj frame] fromStave:self];
        [obj setFrame:frame];
    }
    for (id obj in hwEvent.harmonyInfoLabels) {
        [self layoutHarmonyLabel:obj];
        CGRect frame = [(self.parentScrollView) convertRect:[obj frame] fromStave:self];
        [obj setFrame:frame];
    }
}

- (void)removeInvisibleEvents {
    NSMutableArray *unused = [NSMutableArray array];
    for (HWEvent *obj in self.staveEvents) {
        if (! [self isEventVisible:obj.event]) {
            [unused addObject:obj];
//            [obj removeViewsFromSuperview];
            [obj.labels makeObjectsPerformSelector:@selector(reset)];
            [self.staveEventLabelQueue addObjectsFromArray:obj.labels];
            [self.staveTieQueue addObjectsFromArray:obj.ties];
            [self.staveHarmonyInfoLabelQueue addObjectsFromArray:obj.harmonyInfoLabels];
        }
    }
    [self.staveEvents removeObjectsInArray:unused];
}

- (void)generateBarLines {
    const SInt16 leftBar = (self.parentScrollView).leftMostVisibleMeasure + 1;
    const SInt16 rightBar = (self.parentScrollView).rightMostVisibleMeasure + 1;
    
    {
        NSMutableArray *removing = [NSMutableArray array];
        for (HWBarLine *barLine in self.staveBarlines) {
            if (barLine.barNumber < leftBar || barLine.barNumber > rightBar) {
//                [barLine removeFromSuperview];
                [removing addObject:barLine];
            }
        }
        [self.staveBarlines removeObjectsInArray:removing];
        [self.staveBarLineQueue addObjectsFromArray:removing];
    }
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    const NSUInteger barCount = song.finalBarNumber;
    {
        for (int bar = leftBar; bar <= rightBar; bar++) {
            BOOL notFound = ([self.staveBarlines indexOfInt:bar] == NSNotFound);
            if (notFound) {
                HWBarLine *barLine = [self.staveBarLineQueue firstObject];
                if (barLine == nil) {
                    barLine = [HWBarLine barLineFor:bar kind:kBarline_Thin];
                }
                else {
                    [self.staveBarLineQueue removeObject:barLine];
                }
                barLine.font = self.notesFont;
                barLine.barNumber = bar;
                [self.staveBarlines placeObject:barLine];
                barLine.translatesAutoresizingMaskIntoConstraints = NO;
                if (barLine.superview == nil)
                    [self.parentScrollView.contentView addSubview:barLine];
                [self layoutBarLine:barLine];
                CGRect frame = [(self.parentScrollView) convertRect:[barLine frame] fromStave:self];
                [barLine setFrame:frame];
            }
        }
    }
    [self.staveBarLineQueue makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for (HWBarLine *bl in self.staveBarlines) {
        bl.kind = (bl.barNumber == barCount ? kBarline_End : kBarline_Thin);
        bl.enabled = self.enabled;
    }
    
//    NSLog(@"count %d %d", self.staveBarlines.count, self.staveBarLineQueue.count);
//    for (HWBarLine *bl in self.staveBarlines) {
//        printf("%d ", bl.barNumber);
//    }
//    printf("\n");
}

- (void)updateStaveLines {
    const SInt16 leftBar = (self.parentScrollView).leftMostVisibleMeasure == 0 ? -1 : (self.parentScrollView).leftMostVisibleMeasure;
    const SInt16 rightBar = (self.parentScrollView).rightMostVisibleMeasure;
    
    {
        NSMutableArray *removing = [NSMutableArray array];
        for (HWStaveLines *lines in self.staveLineViews) {
            if (lines.barNumber < leftBar || lines.barNumber > rightBar) {
//                [lines removeLinesFromSuperview];
                [removing addObject:lines];
            }
        }
        [self.staveLineViews removeObjectsInArray:removing];
        [self.staveLineViewQueue addObjectsFromArray:removing];
    }
    const CGFloat staveWidth = self.rulerControl.distanceBetweenBars;
    const UInt16 barCount = [HWCollectionViewController currentlyOpenDocument].song.finalBarNumber;
    {
        for (int bar = leftBar; bar <= rightBar; bar++) {
            BOOL notFound = ([self.staveLineViews indexOfInt:bar] == NSNotFound);
            if (notFound) {
                HWStaveLines *staveLines = [self.staveLineViewQueue firstObject];
                if (staveLines == nil) {
                    staveLines = [[HWStaveLines alloc] init];
                    staveLines.enabled = YES;
                    NSMutableArray *lineViews = [NSMutableArray arrayWithCapacity:self.numberOfStaveLines];
                    for (NSUInteger jdx = 0; jdx < self.numberOfStaveLines; jdx++) {
                        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
                        view.backgroundColor = [UIColor blackColor];
                        [lineViews addObject:view];
                        view.translatesAutoresizingMaskIntoConstraints = NO;
                        [self.parentScrollView.contentView addSubview:view];
                    }
                    staveLines.lines = lineViews;
                }
                else {
                    [self.staveLineViewQueue removeObject:staveLines];
                    for (UIView *lv in staveLines.lines) {
                        if (lv.superview == nil) {
                            lv.translatesAutoresizingMaskIntoConstraints = NO;
                            [self.parentScrollView.contentView addSubview:lv];
                        }
                    }
                }
                staveLines.barNumber = bar;
                [self.staveLineViews placeObject:staveLines];
                [self layoutStaveLineObject:staveLines staveWidth:staveWidth barCount:barCount];
            }
        }
    }
    
    [self.staveLineViewQueue makeObjectsPerformSelector:@selector(removeLinesFromSuperview)];
    
    for (HWStaveLines *staveLines in self.staveLineViews) {
        staveLines.enabled = self.enabled;
    }
    
//    NSLog(@"count %d %d", self.staveLineViews.count, self.staveLineViewQueue.count);
//    for (HWStaveLines *sl in self.staveLineViews) {
//        printf("%d ", sl.barNumber);
//    }
//    printf("\n");
}

- (void)updateEnabled {
    BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (expertMode) {
        self.enabled = ![doc.mutedTracks containsObject:self.track];
    }
    else {
        self.enabled = (self.trackNumber == 0) && ![doc.mutedTracks containsObject:self.track];
    }
}

- (void)generateEverything {
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    [self updateEnabled];
    
    if (self.staveLineViews == nil) {
        self.staveLineViews = [NSMutableArray arrayWithCapacity:song.finalBarNumber];
    }
    [self updateStaveLines];
    
    if (self.clefLabel) {
        self.clefLabel.kind = (enum ClefKind)self.track.clef;
    }
    else if (self.track) {
        self.clefLabel = [HWClefLabel clefKind:(enum ClefKind)self.track.clef];
        self.clefLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.parentScrollView.contentView addSubview:self.clefLabel];
    }
    self.clefLabel.textColor = (self.enabled ? [UIColor blackColor] : [UIColor lightGrayColor]);
    self.clefLabel.font = self.notesFont;
    
    if (self.timeSignatureView) {
        [self updateTimeSignature];
    }
    else if (song.timeSign) {
        self.timeSignatureView = [HWTimeSignature timeSignatureWithUpper:song.timeSign.upperNumeral lower:song.timeSign.lowerNumeral];
        [self updateTimeSignature];
        self.timeSignatureView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.parentScrollView.contentView addSubview:self.timeSignatureView];
    }
    self.timeSignatureView.font = self.notesFont;
    
    if (self.keySignatureView) {
        [self updateKeySignatureOrMode];
    }
    else if (self.track != song.harmony && self.rulerControl && self.clefLabel) {
        self.keySignatureView = [HWKeySignature keySignatureWith:song.keySign.sharpsFlatsCount ruler:self.rulerControl clef:self.clefLabel];
        [self updateKeySignatureOrMode];
        self.keySignatureView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.parentScrollView.contentView addSubview:self.keySignatureView];
    }
    self.keySignatureView.font = self.notesFont;
    
    [self generateBarLines];
    [self generateStaveEvents];
}

- (void)updateAllAccidentals {
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    
    NSMutableArray *labelsForBar = [NSMutableArray arrayWithCapacity:song.finalBarNumber];
    for (UInt16 bar = 0; bar < song.finalBarNumber; bar++) {
        [labelsForBar addObject:[NSMutableArray array]];
    }
    for (HWEventLabel *label in self.allEventLabels) {
        if ([label.parent isKindOfClass:[HarmonyWizNoteEvent class]]) {
            const UInt16 bar = [song barNumberFromEvent:label.event];
            NSMutableArray *labelsAtBar = [labelsForBar objectAtIndex:bar];
            [labelsAtBar placeObject:label];
        }
    }
    
    for (UInt16 bar = 0; bar < song.finalBarNumber; bar++) {
        [self updateAccidentalsFor:[labelsForBar objectAtIndex:bar]];
    }
}

- (void)updateAccidentalsFor:(NSArray*)labels {
    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
    map<SInt16,ScaleNote::ScaleNoteAlteration> effectiveAccidental;
    
    for (HWEventLabel *eventLabel in labels) {
        if (effectiveAccidental[eventLabel.scaleNote.note] != eventLabel.scaleNote.alteration) {
            
            const AccidentalType accidental = [song accidentalForScaleNote:eventLabel.scaleNote];
            
            // check if should neutralise
            const AccidentalType prevAccidental = [song accidentalForScaleNote:
                                                   ScaleNote(eventLabel.scaleNote.note,effectiveAccidental[eventLabel.scaleNote.note])];
            const BOOL shouldNeutraliseAccidental = ((prevAccidental == kAccidental_DoubleSharp && accidental == kAccidental_Sharp) || (prevAccidental == kAccidental_DoubleFlat && accidental == kAccidental_Flat));
            
            eventLabel.shouldNeutraliseAccidental = shouldNeutraliseAccidental;
            eventLabel.accidentalType = accidental;
            
            // save alteration
            effectiveAccidental[eventLabel.scaleNote.note] = eventLabel.scaleNote.alteration;
        }
        else {
            eventLabel.shouldNeutraliseAccidental = NO;
            eventLabel.accidentalType = kAccidental_None;
        }
        
        // update label
        [eventLabel updateForClef:self.clefLabel];
        eventLabel.color = [self eventColour:eventLabel.parent];
        eventLabel.font = self.notesFont;
    }
}

- (NSArray*)staveEventsFromMusicEvents:(NSArray*)events {
    return [self.staveEvents filteredArrayUsingPredicate:
            [NSPredicate predicateWithFormat:@"event IN %@", events]];
}

- (void)updateStaveEventsIn:(NSArray *)staveEvents {
    for (HWEvent *item in staveEvents) {
        UIColor *textColor = [self eventColour:item.event];
        for (HWEventLabel *label in item.labels) {
            [label updateForClef:self.clefLabel];
            label.color = textColor;
            label.font = self.notesFont;
        }
        for (HWEventTie *tie in item.ties) {
            tie.color = textColor;
            [tie update];
            [tie setNeedsDisplay];
        }
        for (HWHarmonyLabel *harmonyLabel in item.harmonyInfoLabels) {
            harmonyLabel.textColor = textColor;
            [harmonyLabel update];
        }
    }
    
    // update accidentals
//    [self updateAllAccidentals];
    
//    HarmonyWizSong *song = [[HWCollectionViewController currentlyOpenDocument] song];
//    NSMutableSet *bars = [NSMutableSet set];
//    for (HarmonyWizMusicEvent *ev in [staveEvents valueForKey:@"event"]) {
//        if ([ev isKindOfClass:[HarmonyWizNoteEvent class]]) {
//            UInt16 bar = [song barNumberFromEvent:ev];
//            [bars addObject:@(bar)];
//        }
//    }
//    for (NSNumber *bar in bars) {
//        [self updateAccidentalsFor:[self eventLabelsForBar:bar.unsignedShortValue]];
//    }
}

- (void)flushStave {
    for (HWEvent *obj in self.staveEvents) {
        [obj removeViewsFromSuperview];
        [obj.labels makeObjectsPerformSelector:@selector(reset)];
        [self.staveEventLabelQueue addObjectsFromArray:obj.labels];
        [self.staveTieQueue addObjectsFromArray:obj.ties];
        [self.staveHarmonyInfoLabelQueue addObjectsFromArray:obj.harmonyInfoLabels];
    }
    [self.staveEvents removeAllObjects];
    
//    for (HWBarLine *barLine )
}

- (void)updateStave {
    [self removeUnusedEvents];
    [self removeInvisibleEvents];
    [self generateEverything];
    [self updateStaveEventsIn:self.staveEvents];
    [self layoutStaveViews];
    for (UIView *view in self.staveEventLabelQueue)
        [view removeFromSuperview];
    for (UIView *view in self.staveHarmonyInfoLabelQueue)
        [view removeFromSuperview];
    for (UIView *view in self.staveTieQueue)
        [view removeFromSuperview];
    
    [self.parentScrollView maintainQueueMaxSize];
}

- (void)updateKeySignatureOrMode {
    HarmonyWizSong *song = [HWCollectionViewController currentlyOpenDocument].song;
    if (song.isModal) {
        self.keySignatureView.key = 0; //song.songMode.sharpsFlatsCount;
    }
    else {
        self.keySignatureView.key = song.keySign.sharpsFlatsCount;
    }
    self.keySignatureView.color = (self.enabled ? [UIColor blackColor] : [UIColor lightGrayColor]);
}

- (void)updateTimeSignature {
    HarmonyWizTimeSignEvent *timeSign = [HWCollectionViewController currentlyOpenDocument].song.timeSign;
    self.timeSignatureView.upperNumeral = timeSign.upperNumeral;
    self.timeSignatureView.lowerNumeral = timeSign.lowerNumeral;
    self.timeSignatureView.color = (self.enabled ? [UIColor blackColor] : [UIColor lightGrayColor]);
}

- (void)layoutHarmonyLabel:(HWHarmonyLabel*)harmonyLabel {
    CGFloat xpos = [self.rulerControl xposFromEvent:harmonyLabel.eventLabel.event];
    CGFloat ypos = (self.touchableFrame.size.height*0.5f) + self.rulerControl.staveLineSpacing;
    CGFloat size = self.rulerControl.staveLineSpacing * 4;
    CGSize sz = [harmonyLabel sizeThatFits:CGSizeMake(size, self.touchableFrame.size.height)];
    harmonyLabel.frame = CGRectMake(xpos - sz.width*0.5f, ypos, sz.width, self.rulerControl.staveLineSpacing*4);
}

- (void)layoutBarLine:(HWBarLine*)barLine {
    const CGFloat barLineDistance = [self.rulerControl distanceBetweenBars];
    const CGFloat xpos = self.rulerControl.firstBeatXLoc - self.rulerControl.distanceBetweenBeats + barLine.barNumber * barLineDistance;
    CGRect frame = CGRectMake(xpos, self.rulerControl.staveLineSpacing*2, self.rulerControl.staveLineSpacing, self.touchableFrame.size.height);
    // add a little check for finiteness
    if (isfinite(frame.origin.x) && isfinite(frame.origin.y) && isfinite(frame.size.width) && isfinite(frame.size.height)) {
        barLine.frame = frame;
    }
}

- (void)layoutStaveViews {
    for (id view in self.allViews) {
        if ([view isKindOfClass:[HWEventLabel class]]) {
            HWEventLabel *eventLabel = view;
            [self layoutEventLabel:eventLabel];
            [eventLabel setNeedsLayout];
        }
        else if ([view isKindOfClass:[HWEventTie class]]) {
            HWEventTie *eventTie = view;
            [eventTie computeFrameInStave:self];
        }
        else if ([view isKindOfClass:[HWBarLine class]]) {
            HWBarLine *barLine = view;
            barLine.font = self.notesFont;
            [self layoutBarLine:barLine];
        }
        else if ([view isKindOfClass:[HWClefLabel class]]) {
            HWClefLabel *clef = view;
            CGFloat ypos = 0;
            switch (clef.kind) {
                case kClef_Treble:
                    ypos = self.rulerControl.staveLineSpacing*1.f;
                    break;
                case kClef_Alto:
                    ypos = 0;
                    break;
                case kClef_Tenor:
                    ypos = -self.rulerControl.staveLineSpacing*1.f;
                    break;
                case kClef_Bass:
                    ypos = -self.rulerControl.staveLineSpacing*1.f;
                    break;
                case kClef_Neutral:
                    ypos = 0;
                    break;
            }
            clef.font = self.notesFont;
            CGSize sz = [clef sizeThatFits:CGSizeMake(self.rulerControl.staveLineSpacing*4.f, self.touchableFrame.size.height)];
            clef.frame = CGRectMake(self.rulerControl.clefXLoc, ypos, sz.width, self.touchableFrame.size.height);
        }
        else if ([view isKindOfClass:[HWTimeSignature class]]) {
            HWTimeSignature *timeSign = view;
            const CGFloat timeSignatureWidth = self.rulerControl.staveLineSpacing * 3;
            timeSign.frame = CGRectMake(self.rulerControl.timeSignatureXLoc, -self.rulerControl.staveLineSpacing*1, timeSignatureWidth, self.touchableFrame.size.height);
            timeSign.upperLabel.font = self.notesFont;
            timeSign.lowerLabel.font = self.notesFont;
            timeSign.upperLabel.frame = CGRectMake(0, 0, timeSign.frame.size.width, timeSign.frame.size.height);
            timeSign.lowerLabel.frame = CGRectMake(0, self.rulerControl.staveLineSpacing*2, timeSign.frame.size.width, timeSign.frame.size.height);
        }
        else if ([view isKindOfClass:[HWKeySignature class]]) {
            HWKeySignature *keySign = view;
            keySign.font = self.notesFont;
            keySign.frame = CGRectMake(self.rulerControl.keySignatureXLoc, -self.rulerControl.staveLineSpacing*2, self.rulerControl.staveLineSpacing*(keySign.subviews.count+1), self.touchableFrame.size.height);
            [keySign setNeedsLayout];
        }
        else if ([view isKindOfClass:[HWHarmonyLabel class]]) {
            [self layoutHarmonyLabel:view];
        }
        
        {
            CGRect frame = [(self.parentScrollView) convertRect:[view frame] fromStave:self];
            [view setFrame:frame];
        }
    }
    
    if (self.staveLineViews) {
        [self layoutStaveLines];
    }
}

- (void)layoutStaveLineObject:(HWStaveLines*)staveLines staveWidth:(CGFloat)staveWidth barCount:(UInt16)barCount {
    const CGFloat testingOffset = 0;   // only for testing. make it 0 for production.
    
    const CGFloat staveLineYOffset = -self.rulerControl.staveLineSpacing*0.08f;
    if (staveLines.barNumber == -1) {
        // before first bar
        CGFloat ypos = self.touchableFrame.size.height * 0.5f - (self.numberOfStaveLines-1)*self.rulerControl.staveLineSpacing*0.5f + staveLineYOffset;
        for (UIView *line in staveLines.lines) {
            line.frame = CGRectMake(self.rulerControl.xOffset, ypos-0.5f, self.rulerControl.firstBeatXLoc - self.rulerControl.xOffset - testingOffset, 1);
            ypos += self.rulerControl.staveLineSpacing;
        }
    }
    else if (staveLines.barNumber+1 == barCount) {
        // final bar
        CGFloat ypos = self.touchableFrame.size.height*0.5f - (self.numberOfStaveLines-1)*self.rulerControl.staveLineSpacing*0.5f + staveLineYOffset;
        for (UIView *line in staveLines.lines) {
            const CGFloat barLineDistance = [self.rulerControl distanceBetweenBars];
            const CGFloat finalBarLineXpos = self.rulerControl.firstBeatXLoc - self.rulerControl.distanceBetweenBeats + barCount * barLineDistance;
            
            const CGFloat xpos = self.rulerControl.firstBeatXLoc + staveLines.barNumber * staveWidth;
            line.frame = CGRectMake(xpos, ypos-0.5f, finalBarLineXpos - xpos + self.rulerControl.staveLineSpacing*0.5f, 1);
            ypos += self.rulerControl.staveLineSpacing;
        }
    }
    else {
        // all other bars
        CGFloat ypos = self.touchableFrame.size.height * 0.5f - (self.numberOfStaveLines-1)*self.rulerControl.staveLineSpacing*0.5f + staveLineYOffset;
        for (UIView *line in staveLines.lines) {
            CGRect frame = CGRectMake(self.rulerControl.firstBeatXLoc + staveLines.barNumber * staveWidth, ypos-0.5f, staveWidth - testingOffset, 1);
            if (isfinite(frame.origin.x) && isfinite(frame.origin.y) && isfinite(frame.size.width) && isfinite(frame.size.height)) {
                line.frame = frame;
            }
            ypos += self.rulerControl.staveLineSpacing;
        }
    }
    
    for (UIView *line in staveLines.lines) {
        [self.parentScrollView.contentView sendSubviewToBack:line];
        CGRect frame = [(self.parentScrollView) convertRect:[line frame] fromStave:self];
        if (isfinite(frame.origin.x) && isfinite(frame.origin.y) && isfinite(frame.size.width) && isfinite(frame.size.height)) {
            [line setFrame:frame];
        }
    }
}

- (void)layoutStaveLines {
    const CGFloat staveWidth = self.rulerControl.distanceBetweenBars;
    const UInt16 barCount = [HWCollectionViewController currentlyOpenDocument].song.finalBarNumber;
    for (HWStaveLines *staveLines in self.staveLineViews) {
        [self layoutStaveLineObject:staveLines staveWidth:staveWidth barCount:barCount];
    }
}

- (void)layoutEventLabel:(HWEventLabel*)eventLabel {
    eventLabel.font = self.notesFont;
    CGFloat xpos = [self.rulerControl xposFromEvent:eventLabel.event] + eventLabel.xoffset;
    CGFloat ypos = self.rulerControl.staveLineSpacing * eventLabel.yoffset * 0.5f;
    CGSize sz = [eventLabel sizeThatFits:CGSizeZero];
    eventLabel.frame = CGRectMake(xpos - self.rulerControl.staveLineSpacing*0.5f, ypos, sz.width, self.touchableFrame.size.height);
//
//    if ([eventLabel.event isKindOfClass:[HarmonyWizNoteEvent class]]) {
//        ypos = [self noteYPos:(HarmonyWizNoteEvent*)eventLabel.event] + self.middleStaveLineOffset;
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xpos-2, ypos-2, 4, 4)];
//        view.backgroundColor = [UIColor redColor];
//        [self.parentScrollView.contentView addSubview:view];
//    }
}

- (HWTimeRulerScrollView*)rulerControl {
    return [(self.parentScrollView) rulerControl];
}

- (UIFont*)notesFont {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    return [UIFont fontWithName:@"AloisenNew" size:doc.metadata.notesPointSize];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.staveEvents = [NSMutableArray array];
        self.staveBarlines = [NSMutableArray array];
    }
    return self;
}

static SInt16 iround(float x) {
    return SInt16(x + 0.5f);
}

- (HarmonyWizTrack*)track {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if (self.trackNumber < doc.song.musicTracks.count)
        return [doc.song.musicTracks objectAtIndex:self.trackNumber];
    else if (self.trackNumber == doc.song.harmony.trackNumber)
        return doc.song.harmony;
    
    return nil;
}

- (UInt16)noteValueFromYLoc:(CGFloat)yloc {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    ScaleNote sn;
    
    yloc = self.middleStaveLineOffset - yloc;
    
    const CGFloat fnote = yloc / self.rulerControl.staveLineSpacing * 2.f + self.clefLabel.middleLine.note;
    sn.note = iround(fnote);
    if (doc.song.keySign.tonality == kTonality_Minor) {
        const ScaleNote snom = sn.octaveMod();
        if (snom == 5 || snom == 6) {
            if (fmodf(fnote, 1.f) < 0.5f)
                sn.alteration = ScaleNote::kAlteration_Sharpen;
        }
    }
    
    return sn.chromatic(doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
}

- (UInt16)noteValueFromYLoc:(CGFloat)yloc raised:(BOOL)raised {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    ScaleNote sn;
    
    yloc = self.middleStaveLineOffset - yloc;
    
    const CGFloat fnote = yloc / self.rulerControl.staveLineSpacing * 2.f + self.clefLabel.middleLine.note;
    sn.note = iround(fnote);
    if (doc.song.keySign.tonality == kTonality_Minor) {
        const ScaleNote snom = sn.octaveMod();
        if (snom == 5 || snom == 6) {
            if (raised)
                sn.alteration = ScaleNote::kAlteration_Sharpen;
        }
    }
    
    return sn.chromatic(doc.song.keySign.root, doc.song.keySign.tonality==kTonality_Minor);
}

- (HarmonyWizMusicEvent*)closestEventToLoc:(CGPoint)loc {
    HarmonyWizMusicEvent *result = nil;
    CGFloat closestDist = 1e6f, dist;
    HarmonyWizMusicEvent *closestEvent;
    for (HarmonyWizMusicEvent *ev in self.track.events) {
        const CGFloat eventXPos = [self.rulerControl xposFromEvent:ev];
        dist = fabsf(eventXPos - loc.x);
        if (dist < closestDist) {
            closestDist = dist;
            closestEvent = ev;
        }
    }
    //    if (closestDist < self.rulerControl.distanceBetweenBeats) {
    result = closestEvent;
    //    }
    return result;
}

- (CGFloat)middleStaveLineOffset {
    return self.touchableFrame.size.height*0.5f;
}

- (UIColor*)eventColour:(HarmonyWizMusicEvent*)event {
    UIColor *result = nil;
    
    if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
        HarmonyWizNoteEvent *note = (HarmonyWizNoteEvent*)event;
        if ([self.track isKindOfClass:[HarmonyWizMusicTrack class]]) {
            HarmonyWizMusicTrack *musicTrack = (HarmonyWizMusicTrack*)self.track;
            if (note.noteValue < musicTrack.lowestNoteAllowed ||
                note.noteValue > musicTrack.highestNoteAllowed) {
                result = [HWColourScheme outOfRangeColour];
            }
        }
    }
    
    if (result == nil) {
        if (event.selected) {
            result = [HWColourScheme selectedColour];
        }
        else if ([event isKindOfClass:[HarmonyWizNoteEvent class]]) {
            HarmonyWizNoteEvent *ne = (id)event;
            switch (ne.type) {
                case kNoteEventType_Harmony:
                    result = [HWColourScheme harmonyColour];
                    break;
                case kNoteEventType_Passing:
                    result = [HWColourScheme passingColour];
                    break;
                case kNoteEventType_Suspension:
                    result = [HWColourScheme suspensionColour];
                    break;
                case kNoteEventType_Auto:
                    if (self.track.locked)
                        result = [HWColourScheme lockedAutoColour];
                    else
                        result = [HWColourScheme autoColour];
                    break;
            }
        }
        else if ([event isKindOfClass:[HarmonyWizHarmonyEvent class]]) {
            NSString *leading = [[(HarmonyWizHarmonyEvent*)event constraints] objectForKey:HarmonyWizLeadingNoteTreatmentProperty];
            if ([leading isEqualToString:HarmonyWizLeadingNoteTreatmentMelodic]) {
                result = [HWColourScheme harmonyEventMelodicColour];
            }
            else if ([leading isEqualToString:HarmonyWizLeadingNoteTreatmentHarmonic]) {
                result = [HWColourScheme harmonyEventHarmonicColour];
            }
            else {
                result = [HWColourScheme harmonyEventColour];
            }
        }
        else if ([event isKindOfClass:[HarmonyWizRestEvent class]]) {
            result = [HWColourScheme restColour];
        }
        else {
            result = [UIColor blackColor];
        }
    }
    
    return (self.enabled ? result : [self lighterColorForColor:result]);
}

- (UIColor *)lighterColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:0.5f*(r+1.f)
                               green:0.5f*(g+1.f)
                                blue:0.5f*(b+1.f)
                               alpha:a];
    return c;
}

- (CGFloat)noteYPos:(HarmonyWizNoteEvent*)event {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    ScaleNote sn(event.noteValue,doc.song.keySign.root,doc.song.keySign.tonality==kTonality_Minor);
    if (self.track.clef == kClef_Neutral)
        return 0;
    return (self.rulerControl.staveLineSpacing * (self.clefLabel.middleLine.note - sn.note)*0.5f);
}

- (CGPoint)noteHeadLoc:(HarmonyWizMusicEvent*)event {
    if ([event isKindOfClass:[HarmonyWizNoteEvent class]])
        return CGPointMake([self.rulerControl xposFromEvent:event], [self noteYPos:(HarmonyWizNoteEvent*)event] + self.middleStaveLineOffset);
    else if ([event isKindOfClass:[HarmonyWizHarmonyEvent class]] && ![(HarmonyWizHarmonyEvent*)event noChord])
        return CGPointMake([self.rulerControl xposFromEvent:event], self.middleStaveLineOffset);
    else if ([event isKindOfClass:[HarmonyWizRestEvent class]]) {
        return CGPointMake([self.rulerControl xposFromEvent:event], self.middleStaveLineOffset);
    }
    else {
        @throw [NSException exceptionWithName:@"HarmonyWiz" reason:[@"invalid music event type " : NSStringFromClass([event class])] userInfo:nil];
    }
}

@end
