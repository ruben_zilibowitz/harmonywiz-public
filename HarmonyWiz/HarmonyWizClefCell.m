//
//  HarmonyWizClefCell.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizClefCell.h"
#import "NSString+Unichar.h"

@interface HarmonyWizClefCell ()
@property (nonatomic,strong) UILabel *staveLabel;
@property (nonatomic,strong) UILabel *clefLabel;
@end

@implementation HarmonyWizClefCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIFont *font = [UIFont fontWithName:@"AloisenNew" size:36.f];
        
        CGRect frame = self.bounds;
        frame.origin.x = 12.f;
        frame.origin.y = 18.f;
        frame.size.height = 84.f;
        self.staveLabel = [[UILabel alloc] initWithFrame:frame];
        self.staveLabel.backgroundColor = [UIColor clearColor];
        self.staveLabel.font = font;
        const unichar staveChar = 0xF03D;
        self.staveLabel.text = [NSString stringWithFormat:@"%C%C", staveChar, staveChar];
        [self.contentView addSubview:self.staveLabel];
        
        frame.origin.x = 24.f;
        self.clefLabel = [[UILabel alloc] initWithFrame:frame];
        self.clefLabel.backgroundColor = [UIColor clearColor];
        self.clefLabel.font = font;
        [self.contentView addSubview:self.clefLabel];
        
        self.clef = kClef_Treble;
    }
    return self;
}

- (void)setClef:(enum CellClefType)clef {
    _clef = clef;
    CGRect frame = self.clefLabel.frame;
    const CGFloat spaceSize = self.staveLabel.font.pointSize / 4.f;
    switch (clef) {
        case kClef_Treble:
            frame.origin.y = self.staveLabel.frame.origin.y - 1.f*spaceSize;
            self.clefLabel.frame = frame;
            self.clefLabel.text = [NSString stringWithUnichar:0xF080];
            break;
        case kClef_Alto:
            frame.origin.y = self.staveLabel.frame.origin.y - 2.f*spaceSize;
            self.clefLabel.frame = frame;
            self.clefLabel.text = [NSString stringWithUnichar:0xF082];
            break;
        case kClef_Tenor:
            frame.origin.y = self.staveLabel.frame.origin.y - 3.f*spaceSize;
            self.clefLabel.frame = frame;
            self.clefLabel.text = [NSString stringWithUnichar:0xF082];
            break;
        case kClef_Bass:
            frame.origin.y = self.staveLabel.frame.origin.y - 3.f*spaceSize;
            self.clefLabel.frame = frame;
            self.clefLabel.text = [NSString stringWithUnichar:0xF081];
            break;
    }
}

/*
 - (void)setSelected:(BOOL)selected animated:(BOOL)animated
 {
 [super setSelected:selected animated:animated];
 }*/

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted) {
        self.staveLabel.textColor = self.backgroundColor;
        self.clefLabel.textColor = self.backgroundColor;
    }
    else {
        [self.staveLabel performSelector:@selector(setTextColor:) withObject:[UIColor blackColor] afterDelay:0.25];
        [self.clefLabel performSelector:@selector(setTextColor:) withObject:[UIColor blackColor] afterDelay:0.25];
    }
}

@end
