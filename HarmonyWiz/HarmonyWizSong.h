//
//  HarmonyWizSong.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HarmonyWizTrack.h"
#import "HarmonyWizEvent.h"

enum TrackType {
    kTrackType_Soprano = 0,
    kTrackType_Mezzo,
    kTrackType_Contralto,
    kTrackType_Tenor,
    kTrackType_Baritone,
    kTrackType_Bass,
    kNumTrackTypes
};

@interface HarmonyWizSong : NSObject <NSCopying, NSCoding>

@property (atomic,strong) NSString *title;
//@property (strong) NSDate *lastModified;

@property (atomic,assign) UInt16 tempo; // bpm
@property (atomic,strong) HarmonyWizTrack *keySigns, *modes, *timeSigns, *specialBarLines;
@property (atomic,assign) enum TriadsType {kTertian, kQuartalPerfect, kQuartal} triadsType;
@property (atomic,strong) HarmonyWizTrack *harmony;
@property (atomic,assign) UInt16 beatSubdivisions;
@property (atomic,assign) float playbackPosition;
@property (atomic,strong,readonly) NSString *creatorVersion;
@property (atomic,strong) NSMutableDictionary *misc; // keys and objects must inherit NSCoding and NSCopying

struct NoteType {
    enum NoteHeadType {
        kNote_Breve = 0,
        kNote_Semibreve,
        kNote_Minim,
        kNote_Crotchet,
        kNote_Quaver,
        kNote_Semiquaver,
        kNote_Demisemiquaver,
        kNote_Hemidemisemiquaver,
        kNote_Unsupported
    } noteHead;
    unsigned char dots;
};

+ (HarmonyWizSong*)songWithData:(NSData*)data error:(NSError**)outError;
+ (HarmonyWizSong*)songWithURL:(NSURL*)url error:(NSError**)outError;

- (BOOL)isModal;
- (BOOL)validateForMode;
- (void)tieAdjacentAutoNotesPreservingSelection:(const HarmonyWizSong*)preservedSong;
- (void)replaceMusicEvent:(HarmonyWizMusicEvent*)ev with:(HarmonyWizMusicEvent*)replacement;
- (void)setEventDuration:(HarmonyWizMusicEvent*)ev to:(UInt16)subdivs;
- (void)setEventPosition:(HarmonyWizEvent*)ev to:(UInt16)subdivs;
+ (NSString*)oldestCompatibleSongVersion;
- (void)roundNotesToDiatonic;
- (void)roundNotesToMode;
- (void)transposeSongBySemitones:(SInt16)chromatic;
- (NSArray*)musicTracks;
- (HarmonyWizMusicTrack*)addMusicTrackOfType:(enum TrackType)type;
- (void)addMusicTrack:(HarmonyWizMusicTrack*)track;
- (void)removeMusicTrack:(HarmonyWizMusicTrack*)track;
- (struct NoteType) noteTypeFromMusicEvent:(HarmonyWizMusicEvent*)event;
- (UInt16) barNumberFromEvent:(HarmonyWizEvent*)event;
- (UInt16) barNumberFromEventEnd:(HarmonyWizMusicEvent*)event;
- (UInt16) barBeatFromEvent:(HarmonyWizEvent*)event;
- (BOOL) canPlaceEvent:(HarmonyWizMusicEvent*)nextEvent afterEvent:(HarmonyWizMusicEvent*)lastEvent;
- (BOOL) canPlaceEvent:(HarmonyWizMusicEvent*)nextEvent atEvent:(HarmonyWizMusicEvent*)thisEvent;
- (BOOL) event:(HarmonyWizMusicEvent*)eventA overlapsWith:(HarmonyWizMusicEvent*)eventB;
//- (BOOL) canPlaceTimeSignUpperNumeral:(UInt16)upper lowerNumeral:(UInt16)lower;
- (void)addFillerRestsTo:(NSMutableArray*)events;
- (NSIndexSet*) overlappingEvents:(NSArray*)events forEvent:(HarmonyWizMusicEvent*)event;
- (NSArray*) fillerRestsForEvents:(NSArray*)events replacedBy:(HarmonyWizMusicEvent*)newEvent;
- (UInt16)eventStartSubdivision:(HarmonyWizEvent*)event;
- (UInt16)eventDuration:(HarmonyWizMusicEvent*)event;
- (UInt16)eventEndSubdivision:(HarmonyWizMusicEvent*)event;
- (NSArray*) chordEvents;
- (void)roundUpFinalBar;
- (UInt16) finalBarSubdivision;
- (UInt16) finalBarNumber;
- (void) setFinalBarNumber:(UInt16)n;
- (NSMutableArray*)unifiedEvents:(NSArray*)events;
- (void)placePlaybackIndicatorAtEvent:(HarmonyWizEvent*)event;
- (void)removeOtherEventsOverlappingWith:(NSArray*)eventsArray;
- (void)truncateEventToEndOfSong:(HarmonyWizMusicEvent*)event;
- (HarmonyWizHarmonyEvent*)findLastHarmonyEventForEvent:(HarmonyWizMusicEvent*)event;

- (BOOL)canPerformAutoTie;
- (void)selectNone;
- (BOOL)anythingSelected;
- (void)autoTieSelectedNotes;
- (void) setupSATB;
- (void) clearBars:(UInt16)numBars;

- (void)addBarsToEnd:(NSUInteger)count;
- (void)removeBarsFromEnd:(NSUInteger)count;
- (BOOL)isEmpty;

- (NSSet*)harmonyEventsOverlappingSelection;
- (UInt16)findFirstHarmonyColumnForEvent:(HarmonyWizMusicEvent*)event;
- (UInt16)findLastHarmonyColumnForEvent:(HarmonyWizMusicEvent*)event;
- (HarmonyWizMusicEvent*)findFirstSelectedEvent;
- (HarmonyWizMusicEvent*)findLastSelectedEvent;
- (UInt16)countEmptyBeatsAtEnd;
- (void)clearArrangementFrom:(HarmonyWizHarmonyEvent*)he1 to:(HarmonyWizHarmonyEvent*)he2;
- (void)clearArrangement;
- (void)clearLeadingNoteConstraints;
- (void)clearHarmonyConstraints;
- (void)clearHarmonyProperties;
- (void)clearHarmonyRelaxConstraints;
- (UInt16)harmonyEventCount;
- (HarmonyWizKeySignEvent*)keySign;
- (void)setKeySign:(HarmonyWizKeySignEvent*)keySignEvent;
- (HarmonyWizModeEvent*)songMode;
- (void)setSongMode:(HarmonyWizModeEvent*)mode;
- (HarmonyWizTimeSignEvent*)timeSign;
- (void)setTimeSign:(HarmonyWizTimeSignEvent*)timeSignEvent;
- (HarmonyWizSpecialBarLineEvent*)finalBarLine;
- (void)setFinalBarLine:(HarmonyWizSpecialBarLineEvent*)finalBarLine;

+ (UInt16)sopranoHigh;
+ (UInt16)sopranoLow;
+ (UInt16)mezzoHigh;
+ (UInt16)mezzoLow;
+ (UInt16)altoHigh;
+ (UInt16)altoLow;
+ (UInt16)tenorHigh;
+ (UInt16)tenorLow;
+ (UInt16)baritoneHigh;
+ (UInt16)baritoneLow;
+ (UInt16)bassHigh;
+ (UInt16)bassLow;
+ (UInt16)highestNoteForTrackType:(enum TrackType)type;
+ (UInt16)lowestNoteForTrackType:(enum TrackType)type;

@end
