//
//  HWStar.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 31/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWStar.h"

@implementation HWStar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.color = [UIColor yellowColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [self.color set];
    [self drawStarAt:CGPointMake(CGRectGetWidth(self.bounds)*0.5f, CGRectGetHeight(self.bounds)*0.5f) radius:MIN(CGRectGetHeight(self.bounds),CGRectGetWidth(self.bounds))*0.5f];
}

- (void)drawStarAt:(CGPoint)pt radius:(CGFloat)r {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    float theta = 2.0 * M_PI * (2.0 / 5.0); // 144 degrees
    float flip = -1.0;
    
    CGContextMoveToPoint(ctx, pt.x, r*flip+pt.y);
    
    for (NSUInteger k=1; k<5; k++)
    {
        float x = r * sinf(k * theta);
        float y = r * cosf(k * theta);
        CGContextAddLineToPoint(ctx, x+pt.x, y*flip+pt.y);
    }
    
    CGContextFillPath(ctx);
}

@end
