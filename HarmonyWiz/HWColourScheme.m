//
//  HWColourScheme.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 28/10/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWColourScheme.h"

@implementation HWColourScheme

+ (UIColor*)harmonyColour {
    return UIColorFromRGB(0x0B5DB5);
}

+ (UIColor*)passingColour {
    return UIColorFromRGB(0xC50DC7);
}

+ (UIColor*)suspensionColour {
    return UIColorFromRGB(0x6FC110);
}

+ (UIColor*)autoColour {
    return UIColorFromRGB(0x27211F);
}

+ (UIColor*)restColour {
    return UIColorFromRGB(0x27211F);
}

+ (UIColor*)harmonyEventColour {
    return UIColorFromRGB(0x27211F);
}

+ (UIColor*)harmonyEventMelodicColour {
    return UIColorFromRGB(0xa7211F);
}

+ (UIColor*)harmonyEventHarmonicColour {
    return UIColorFromRGB(0x27a11F);
}

+ (UIColor*)selectedColour {
    return [UIColor orangeColor];
}

+ (UIColor*)outOfRangeColour {
    return [UIColor redColor];
}

+ (UIColor*)lockedAutoColour {
    return [UIColor colorWithRed:0.5 green:0.5 blue:1 alpha:1];
}

+ (UIColor*)paperColour {
    return UIColorFromRGB(0xffffff);
}

+ (UIColor*)rulerBackgroundColor {
    return UIColorFromRGB(0x878787);
}

+ (UIColor*)rulerLoopBackgroundColor {
//    return UIColorFromRGB(0x878700)
    return [HWColourScheme harmonyColour];
}

+ (UIColor*)mainBackgroundColour {
    return UIColorFromRGB(0xeeeeee);
}

+ (UIColor*)horizontalDividerColour {
    return UIColorFromRGB(0xDFE0E2);
}

+ (UIColor*)verticalDividerColour {
    return UIColorFromRGB(0x2F3C42);
}

+ (UIColor*)selectBoxBorderColour {
    return [UIColorFromRGB(0x0000dd) colorWithAlphaComponent:0.9f];
}

+ (UIColor*)selectBoxFillColour {
    return [UIColorFromRGB(0x0000ee) colorWithAlphaComponent:0.2f];
}

+ (UIColor*)eraserColour {
    return [UIColorFromRGB(0x0000dd) colorWithAlphaComponent:0.5f];
}

@end
