//
//  HWShareSongMenu.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/05/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HWCollectionViewController;

@interface HWShareSongMenu : UITableViewController
@property (nonatomic,weak) UIPopoverController *parentPopover;
@property (nonatomic,weak) HWCollectionViewController *songSelectView;
@end

enum ShareVia {
    kShareVia_Online = 0,
    kShareVia_Email,
    kShareVia_iTunes
};

// share menu
#define ShareMethodIsSoundCloud(indexPath)  ((indexPath).section==0&&(indexPath).row==0)
#define AudioShareAsAudio(indexPath)  ((indexPath).section==0&&(indexPath).row==1)
#define AudioShareAsMIDI(indexPath)  ((indexPath).section==0&&(indexPath).row==2)

#define EMailAudioFile(indexPath)  ((indexPath).section==1&&(indexPath).row==0)
#define EMailSongFile(indexPath)  ((indexPath).section==1&&(indexPath).row==1)
#define EMailLilypondFile(indexPath)  ((indexPath).section==1&&(indexPath).row==2)
#define EMailMusicXMLFile(indexPath)  ((indexPath).section==1&&(indexPath).row==3)
#define EMailMIDIFile(indexPath)  ((indexPath).section==1&&(indexPath).row==4)

#define SaveAudioFile(indexPath)  ((indexPath).section==2&&(indexPath).row==0)
#define SaveSongFile(indexPath)  ((indexPath).section==2&&(indexPath).row==1)
#define SaveLilypondFile(indexPath)  ((indexPath).section==2&&(indexPath).row==2)
#define SaveMusicXMLFile(indexPath)  ((indexPath).section==2&&(indexPath).row==3)
#define SaveMIDIFile(indexPath)  ((indexPath).section==2&&(indexPath).row==4)
