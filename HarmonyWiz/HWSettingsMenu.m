//
//  HWSettingsMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <LinguiSDK/Lingui.h>
#import "HWSettingsMenu.h"
#import "HWCollectionViewController.h"

extern NSString * const HWAllNotesOffNotification;
extern NSString * const HWKeyboardScrolls;
extern NSString * const HWExpertMode;
extern NSString * const HWAutoFix;
extern NSString * const HWAutoScroll;
extern NSString * const HWRecordFromMIDI;

@interface HWSettingsMenu ()

@end

@implementation HWSettingsMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    {
        BOOL expertMode = [[NSUserDefaults standardUserDefaults] boolForKey:HWExpertMode];
        BOOL autoFixEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:HWAutoFix];
        BOOL autoScrollEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:HWAutoScroll];
        BOOL keyboardScrolls = [[NSUserDefaults standardUserDefaults] boolForKey:HWKeyboardScrolls];
        BOOL recordFromMIDI = [[NSUserDefaults standardUserDefaults] boolForKey:@"recordFromMIDI"];
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        
        [self.expertMode setOn:expertMode];
        [self.autofix setOn:autoFixEnabled];
        [self.autoScroll setOn:autoScrollEnabled];
        [self.recordFromMIDI setOn:recordFromMIDI];
        //            [self.sw3 setOn:doc.metadata.scoreTexturesEnabled];
        //            [self.sw4 setOn:doc.metadata.scoreShadowsEnabled];
        [self.keyboardScrolls setOn:keyboardScrolls];
        //            [self.sw6 setOn:doc.metadata.varispeedBrush];
        
        self.verticalScale.value = doc.metadata.notesPointSize;
        self.horizontalScale.value = doc.metadata.noteSpacing;
        
        self.languageLabel.text = [Lingui getCurrentLanguage];
    }
}

- (BOOL)isMIDIPanic:(NSIndexPath*)indexPath {
    return (indexPath.section == 1 && indexPath.row == 3);
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isMIDIPanic:indexPath]) {
        cell.backgroundColor = [UIColor redColor];
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForHeaderInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view header");
    else
        return nil;
}

- (NSString*)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    NSString *result = [super tableView:tableView titleForFooterInSection:section];
    if (result)
        return LinguiLocalizedString(result, @"Table view footer");
    else
        return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL animateDeselect = NO;
    
    if ([self isMIDIPanic:indexPath]) {
        NSNotificationCenter *dnc = [NSNotificationCenter defaultCenter];
        [dnc postNotificationName:HWAllNotesOffNotification object:self];
        animateDeselect = YES;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:animateDeselect];
}

@end
