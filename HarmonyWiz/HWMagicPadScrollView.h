//
//  HWMagicPadScrollView.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 30/01/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWTimeRulerScrollView.h"
#import "HarmonyWizMainView.h"
#import "HarmonyWizStaveView.h"
#import "HWViewKeepingTop.h"

@interface HWMagicPadScrollView : UIScrollView
@property (nonatomic,strong) HWViewKeepingTop *contentView;
@property (nonatomic,weak) HWTimeRulerScrollView *rulerControl;
@property (nonatomic,weak) HarmonyWizMainView *mainView;
@property (nonatomic,weak) HarmonyWizStaveView *staveView;
- (void)clearAll;
- (void)updateLabel;
- (void)updateShapeViewsFromSong;
- (void)removeShapeViews;
@end

extern NSString *const HWPaintedNotification;
