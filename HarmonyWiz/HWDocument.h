//
//  HWDocument.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWMetadata.h"
#import "HarmonyWizSong.h"
#import "HarmonyWizUndoManager.h"
#import "PGMidi/PGMidi.h"
#import "MBProgressHUD/MBProgressHUD.h"

@interface HWDocument : UIDocument

// Data
- (HarmonyWizSong*)song;
- (void)setSong:(HarmonyWizSong *)song;
- (NSError*)openingSongError;

// Metadata
@property (nonatomic, strong) HWMetadata * metadata;
- (NSString *) description;

// undo manager
@property (nonatomic, strong) HarmonyWizUndoManager *hwUndoManager;

// MIDI
@property (nonatomic,strong) PGMidiDestination *midiDestination;
@property (nonatomic,strong) PGMidiDestination *lastMIDIDestinationAdded;

// Audio
- (void) bounceDocumentToPath:(NSString*)bouncePath withHUD:(MBProgressHUD *)progressHUD;
- (void) applyMuteAndSolo;
- (NSSet*)mutedTracks;
- (void) applyAllAudioSettings;

// Export
- (NSData*)dataForExport;

// Autosave
@property (atomic,assign) BOOL disableAutosave;

@end
