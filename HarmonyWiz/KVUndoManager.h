//
//  KVUndoManager.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 11/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KVUndoManager : NSUndoManager
@property (atomic,assign,readonly) BOOL kvCanUndo;
@property (atomic,assign,readonly) BOOL kvCanRedo;
@end
