//
//  HWZoomTransitionSegue.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 17/3/14.
//  Copyright (c) 2014 Ruben Zilibowitz. All rights reserved.
//  Based on ImageTransition by Joris Kluivers
//

#import "HWZoomTransitionSegue.h"

NSInteger const HWZoomTransitionImageViewTag = 'ztiv';

@interface HWZoomTransitionSegue ()
@property (atomic,strong,readonly) UIImageView *transitionImageView;
@end

@implementation HWZoomTransitionSegue {
	UIImageView *_transitionImageView;
}

- (id) initWithIdentifier:(NSString *)identifier source:(UIViewController *)source destination:(UIViewController *)destination
{
	self = [super initWithIdentifier:identifier source:source destination:destination];
	if (self) {
		_unwinding = NO;
		_destinationRect = CGRectZero;
	}
	return self;
}

- (UIImageView *) transitionImageView {
	if (!_transitionImageView) {
		_transitionImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _transitionImageView.tag = HWZoomTransitionImageViewTag;
	}
	
	return _transitionImageView;
}

static const NSTimeInterval HWZoomTransitionSpeed = 1.2;
static const NSTimeInterval HWFadeTransitionSpeed = 0.5;

- (void) perform {
    UIWindow *mainWindow = [[self.sourceViewController view] window];
    
    if (SYSTEM_VERSION_LESS_THAN(@"8")) {
        self.transitionImageView.image = [UIImage imageWithCGImage:self.transitionImage.CGImage scale:self.transitionImage.scale orientation:[[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight ? UIImageOrientationRight : UIImageOrientationLeft];
    }
    else {
        self.transitionImageView.image = self.transitionImage;
    }
    self.transitionImageView.frame = [mainWindow convertRect:self.sourceRect fromView:[self.sourceViewController view]];
    
    [mainWindow addSubview:self.transitionImageView];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:HWZoomTransitionSpeed
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         __strong typeof(self) strongSelf = weakSelf;
                         strongSelf.transitionImageView.frame = [mainWindow convertRect:strongSelf.destinationRect fromView:[strongSelf.sourceViewController view]];
                     }
                     completion:^(BOOL finished){
                         if (self.unwinding) {
                             [self fadeOutImageWithDuration:0.1f];
                         }
                     }];
    
    if (self.unwinding) {
        [self.destinationViewController dismissViewControllerAnimated:YES completion:nil];
    } else {
        // nb: this block is causing an abandoned memory issue
        [self.sourceViewController presentViewController:self.destinationViewController animated:YES completion:
         ^{
             [self fadeOutImageWithDuration:HWFadeTransitionSpeed];
         }];
    }
}

- (void)fadeOutImageWithDuration:(NSTimeInterval)dur {
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:dur
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         __strong typeof(self) strongSelf = weakSelf;
                         strongSelf.transitionImageView.alpha = 0.f;
                     } completion:^(BOOL finished){
                         __strong typeof(self) strongSelf = weakSelf;
                         [strongSelf.transitionImageView removeFromSuperview];
                     }];
}

@end
