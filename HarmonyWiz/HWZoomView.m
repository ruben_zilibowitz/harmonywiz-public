//
//  HWZoomView.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 21/03/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWZoomView.h"

@implementation HWZoomView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setScale:(CGFloat)scale {
    _scale = scale;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    if (! self.hidden) {
        UIBezierPath *background = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:16.f];
        [[[UIColor darkGrayColor] colorWithAlphaComponent:0.5f] setFill];
        [background fill];
        [background addClip];
        
        [[UIColor blackColor] setStroke];
        
        CGFloat xpos = self.spacing * self.scale;
        while (xpos < self.bounds.size.width) {
            UIBezierPath *line = [UIBezierPath bezierPath];
            [line moveToPoint:CGPointMake(xpos, 0)];
            [line addLineToPoint:CGPointMake(xpos, self.bounds.size.height)];
            [line setLineWidth:2];
            [line stroke];
            xpos += self.spacing * self.scale;
        }
    }
}

@end
