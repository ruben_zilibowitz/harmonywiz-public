//
//  HarmonyWizSong+ScaleNote.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 1/02/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong.h"
#include "ScaleNote.h"

@interface HarmonyWizSong (ScaleNote)
- (HarmonyWiz::ScaleNote)HWNoteToScaleNote:(HarmonyWizNoteEvent*)ne;
- (HarmonyWizNoteEvent*)ScaleNoteToHWNote:(HarmonyWiz::ScaleNote)sn;
@end

// This is a global shift to translate between Solver chromatic space
// and HarmonyWiz chromatic space.
// Solver chromatic note + kChromaticShift = HarmonyWiz chromatic note
const SInt16 kChromaticShift = 4*12;
