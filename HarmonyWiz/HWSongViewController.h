//
//  HWSongViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 10/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWDocument.h"
#import "HarmonyWizMainView.h"
#import "MBProgressHUD/MBProgressHUD.h"
#import "PGMidi/PGMidi.h"
#import "HarmonyWizToolboxController.h"
#import "HWStavesScrollView.h"
#import "HWTracksScrollView.h"
#import <MessageUI/MessageUI.h>

@class HWTimeRulerScrollView;
@class HWSongViewController;

typedef void (^TutorialBlock)();

@interface HWSongViewController : UIViewController <UIPopoverControllerDelegate, UIScrollViewDelegate, MBProgressHUDDelegate, NSURLConnectionDataDelegate, PGMidiDelegate, PGMidiSourceDelegate, UINavigationControllerDelegate, UIWebViewDelegate, MFMailComposeViewControllerDelegate/*, UIDocumentInteractionControllerDelegate*/>
@property (nonatomic,weak) IBOutlet HarmonyWizMainView *mainView;
@property (nonatomic,weak) IBOutlet UIBarButtonItem *btnUndo, *btnRedo, *btnExit, *btnFocus, *btnRewPlayRec, *btnArrange, *btnSong, *btnSettings, *btnHelp, *btnKeyboard, *btnMagic;

- (IBAction)exitToMainView:(id)sender;
- (IBAction)togglePiano:(id)sender;
- (IBAction)record:(id)sender;
- (IBAction)play:(id)sender;
- (IBAction)stopRewind:(id)sender;
- (IBAction)arrange:(id)sender;
- (IBAction)undo:(id)sender;
- (IBAction)redo:(id)sender;

- (IBAction)autoMelody:(UISlider*)sender;
- (IBAction)expertMode:(id)sender;
- (IBAction)autoFix:(id)sender;
- (IBAction)autoScroll:(id)sender;
- (IBAction)keyboardWizdomScrollEnabled:(UISwitch*)sender;
//- (IBAction)varispeedBrush:(UISwitch*)sender;
- (IBAction)recordFromMIDI:(id)sender;
- (IBAction)verticalScaleChanged:(UISlider*)sender;
- (IBAction)horizontalScaleChanged:(UISlider*)sender;
//- (IBAction)closeTutorial:(id)sender;

- (void)scrollToPlaybackPosition;
- (void)redrawStaves:(BOOL)flush;
- (BOOL)arrangeFailed:(NSString*)actionName;

- (HWStavesScrollView*)allStavesControl;
- (HWTimeRulerScrollView*)rulerControl;
- (HWTracksContentsView*)trackControlContentView;

- (UIImage *) scoreSnapshot;

- (void)shareSongWithMethod:(NSString*)method indexPath:(NSIndexPath*)indexPath;

@property (strong, nonatomic) HWDocument * doc;

@property (atomic,assign) UInt32 arrangeGuardColumn, arrangeGuardCoverage;
@property (atomic,assign) BOOL isRendering;
@property (atomic,strong) NSString *harmonyFailureReason;
@property (atomic,assign) BOOL didCancelArrange, didArrangeTimeOut;

@property (nonatomic,weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic,assign) enum HWState {kStateStopped = 0,kStatePlaying,kStateRecording} state;

@property (nonatomic,weak) IBOutlet HarmonyWizToolboxController *toolboxController;

@property (nonatomic,strong) NSMutableArray *fixesApplied;
@property (nonatomic,strong) MBProgressHUD *arrangeHUD;

// used by interactive tutorial. Not sure if this is going to be used in production.
//@property (nonatomic,strong) NSTimer *tutorialTimer;
//@property (nonatomic,assign) NSInteger tutorialStage, tutorialSequence;
//@property (nonatomic,copy) TutorialBlock arrangeCompleteHook;

@end

@interface HWSongTitleMenu : UITableViewController <UITextFieldDelegate>
@property (nonatomic,weak) IBOutlet UITextField *field;
@end
