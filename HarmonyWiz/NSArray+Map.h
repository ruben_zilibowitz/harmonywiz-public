//
//  NSArray+Map.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 19/01/2015.
//  Copyright (c) 2015 Wizdom Music. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Map)
- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block;
@end
