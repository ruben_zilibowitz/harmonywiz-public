//
//  HWWandMenu.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/04/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HWWandMenu.h"
#import "HWCollectionViewController.h"

NSString * const HWAutoTieNotification = @"HWAutoTieNotification";

@interface HWWandMenu ()

@end

@implementation HWWandMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Magic";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.textLabel.text = @"Auto-tie";
    
    {
        BOOL enabled;
        
        HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
        id track = [doc.song.musicTracks objectAtIndex:doc.metadata.currentlySelectedTrackNumber];
        [doc.song selectNone];
        [track selectAll];
        enabled = [doc.song canPerformAutoTie];
        [doc.song selectNone];
        
        cell.textLabel.textColor = (enabled ? [UIColor blackColor] : [UIColor lightGrayColor]);
        cell.userInteractionEnabled = enabled;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:HWAutoTieNotification object:self];
}

@end
