//
//  HarmonyWizSong+Accidentals.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 2/08/2014.
//  Copyright (c) 2014 Wizdom Music. All rights reserved.
//

#import "HarmonyWizSong+Accidentals.h"
#include "ScaleNote.h"

@implementation HarmonyWizSong (Accidentals)

using namespace HarmonyWiz;

static AccidentalType accidentalForRaised(SInt16 sn, UInt8 key, enum Tonality tonality) {
    AccidentalType accType = kAccidental_None;
    SInt16 snom = imod(sn, 7);
    UInt8 relKey = key;
    
    if (tonality == kTonality_Minor) {
        relKey = (relKey + 3) % 12;
        snom = (snom + 5) % 7;
    }
    
    switch (relKey) {
        case 0:
            accType = kAccidental_Sharp;
            break;
        case 1:
            accType = (snom==2||snom==6 ? kAccidental_Sharp : kAccidental_Natural);
            break;
        case 2:
            accType = (snom==2 ? kAccidental_DoubleSharp : kAccidental_Sharp);
            break;
        case 3:
            accType = (snom==1||snom==2||snom==5||snom==6 ? kAccidental_Sharp : kAccidental_Natural);
            break;
        case 4:
            accType = (snom==1||snom==2||snom==5||snom==6 ? kAccidental_DoubleSharp : kAccidental_Sharp);
            break;
        case 5:
            accType = (snom==3 ? kAccidental_Natural : kAccidental_Sharp);
            break;
        case 6:
            accType = (snom==6 ? kAccidental_Sharp : kAccidental_Natural);
            break;
        case 7:
            accType = (snom==6 ? kAccidental_DoubleSharp : kAccidental_Sharp);
            break;
        case 8:
            accType = (snom==2||snom==5||snom==6 ? kAccidental_Sharp : kAccidental_Natural);
            break;
        case 9:
            accType = (snom==2||snom==5||snom==6 ? kAccidental_DoubleSharp : kAccidental_Sharp);
            break;
        case 10:
            accType = (snom==0||snom==3 ? kAccidental_Natural : kAccidental_Sharp);
            break;
        case 11:
            accType = (snom==0||snom==3 ? kAccidental_Sharp : kAccidental_DoubleSharp);
            break;
    }
    return accType;
}

static AccidentalType shiftAccidentalDown(AccidentalType acc) {
    switch (acc) {
        case kAccidental_DoubleSharp:
            acc = kAccidental_Sharp;
            break;
        case kAccidental_Sharp:
            acc = kAccidental_Natural;
            break;
        case kAccidental_Natural:
            acc = kAccidental_Flat;
            break;
        case kAccidental_Flat:
            acc = kAccidental_DoubleFlat;
            break;
            
        default:
            acc = kAccidental_None;
            break;
    }
    return acc;
}

static AccidentalType accidentalForUnaltered(SInt16 sn, UInt8 key, enum Tonality tonality) {
    AccidentalType accType = accidentalForRaised(sn,key,tonality);
    return shiftAccidentalDown(accType);
}

static AccidentalType accidentalForFlattened(SInt16 sn, UInt8 key, enum Tonality tonality) {
    AccidentalType accType = accidentalForUnaltered(sn,key,tonality);
    return shiftAccidentalDown(accType);
}

- (AccidentalType)accidentalForScaleNote:(const ScaleNote)sn {
    AccidentalType accidental = kAccidental_None;
    
    UInt8 root = 0;
    enum Tonality tonality = kTonality_Major;
    
    if (! self.isModal) {
        root = self.keySign.root;
        tonality = self.keySign.tonality;
    }
    
    switch (sn.alteration) {
        case ScaleNote::kAlteration_None:
            accidental = accidentalForUnaltered(sn.note, root, tonality);
            break;
        case ScaleNote::kAlteration_Sharpen:
            accidental = accidentalForRaised(sn.note, root, tonality);
            break;
        case ScaleNote::kAlteration_Flatten:
            accidental = accidentalForFlattened(sn.note, root, tonality);
            break;
        case ScaleNote::kAlteration_SharpenDouble:
            accidental = kAccidental_DoubleSharp;
            break;
        case ScaleNote::kAlteration_FlattenDouble:
            accidental = kAccidental_DoubleFlat;
            break;
    }
    
    return accidental;
}

@end
