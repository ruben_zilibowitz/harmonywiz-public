//
//  HarmonyWizMainView.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 8/07/13.
//  Copyright (c) 2013 Zilibowitz Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HarmonyWizMainView : UIView
@property (nonatomic,weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic,strong) UIScrollView *tracksView, *rulerView, *stavesView, *magicView;
@property (nonatomic,strong) UIView *toolbox, *borderHorizontalView, *borderVerticalView;
@property (nonatomic,strong) UIView *piano, *zoomView;
@property (nonatomic,strong) UIView *leftPanel, *rightPanel;
@property (nonatomic,assign) BOOL drawerOpened;

- (void)updateAllFrames;
- (void)disableConstraintsForAllSubviews;

@end
