//
//  HWTransportButtons.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 4/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWTransportButtons : UIView
@property (nonatomic,strong) UIButton *leftbutton, *midButton, *rightButton;
@end
