//
//  HWStyleViewController.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 24/07/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const HWChangedStyleNotification;
extern NSString * const HWArrangeNotification;

@interface HWStyleViewController : UITableViewController
@property (nonatomic,weak) UIBarButtonItem *btnArrange;
@end
