//
//  HWDotsController.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 5/09/13.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import "HWDotsController.h"
#import "NSString+Concatenation.h"
#import "HWToolCustomCellBackground.h"
#import "HWCollectionViewController.h"

NSString * const HWNoteDotsNotification = @"HWNoteDots";

@interface HWDotsController ()

@end

@implementation HWDotsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    
    if (doc.metadata.noteDots != indexPath.row) {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:doc.metadata.noteDots inSection:0]];
        [[cell viewWithTag:3] setHidden:YES];
        [(UILabel*)[cell viewWithTag:2] setTextColor:[UIColor blackColor]];
        
        doc.metadata.noteDots = indexPath.row;
        
        cell = [collectionView cellForItemAtIndexPath:indexPath];
        [[cell viewWithTag:3] setHidden:NO];
        [(UILabel*)[cell viewWithTag:2] setTextColor:[UIColor orangeColor]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:HWNoteDotsNotification object:self];
    }
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


#pragma mark - UICollectionView Datasource

// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return 3;
}

// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID = [@"cell" : @(indexPath.row).stringValue];
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    if (indexPath.section == 0 && cell.selectedBackgroundView == nil) {
        cell.selectedBackgroundView = [[HWToolCustomCellBackground alloc] initWithFrame:CGRectZero];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    }

    HWDocument *doc = [HWCollectionViewController currentlyOpenDocument];
    if ((doc.metadata.notePlainDuration >= kDuration_Semiquaver && indexPath.row == 2) || (doc.metadata.notePlainDuration >= kDuration_Demisemiquaver && indexPath.row >= 1)) {
        cell.userInteractionEnabled = NO;
        UIImageView *iv = (id)[cell viewWithTag:1];
        iv.image = [UIImage imageNamed:(indexPath.row == 1 ? @"images/ToolboxIcons/SingleDotCircleDisabled" : @"images/ToolboxIcons/DoubleDotCircleDisabled")];
        UILabel *label = (id)[cell viewWithTag:2];
        label.enabled = NO;
    }
    
    BOOL highlighted = (doc.metadata.noteDots == indexPath.row);
    [[cell viewWithTag:3] setHidden:!highlighted];
    [(UILabel*)[cell viewWithTag:2] setTextColor:(highlighted ? [UIColor orangeColor] : [UIColor blackColor])];
    
    return cell;
}

@end
