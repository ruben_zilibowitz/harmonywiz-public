//
//  HWTutorialPage.h
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 16/12/2013.
//  Copyright (c) 2013 Wizdom Music. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HWTutorialPage : UIViewController

- (id)initWithName:(NSString*)name;

@property (nonatomic,assign) NSInteger index;
@property (nonatomic,strong) UIImageView *imageView;

@end
