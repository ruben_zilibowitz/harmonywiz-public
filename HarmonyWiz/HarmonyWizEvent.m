//
//  HarmonyWizEvent.m
//  HarmonyWiz
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizEvent.h"

static SInt8 sharpsFlatsCountForRoot(unsigned char root) {
    UInt8 numSharps = 0;
    UInt8 numFlats = 0;
    UInt16 i, j;
    for (i = root, j = i; i != 0 && j != 0; i = (i+7)%12, j = (j+5)%12) {
        numFlats++;
        numSharps++;
    }
    if (i == 0) {
        return -numFlats;
    }
    else if (j == 0) {
        return numSharps;
    }
    return 0;
}

@implementation HarmonyWizEvent
- (NSComparisonResult)compare:(HarmonyWizEvent*)event {
    if (self.beat < event.beat)
        return NSOrderedAscending;
    else if (self.beat > event.beat)
        return NSOrderedDescending;
    else if (self.beat == event.beat) {
        if (self.subdivision < event.subdivision)
            return NSOrderedAscending;
        else if (self.subdivision > event.subdivision)
            return NSOrderedDescending;
    }
    return NSOrderedSame;
}

- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizEvent *result = [[[self class] allocWithZone:zone] init];
    if (result) {
        result.beat = self.beat;
        result.subdivision = self.subdivision;
        result.selected = self.selected;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.beat = [aDecoder decodeInt32ForKey:@"beat"];
        self.subdivision = [aDecoder decodeIntForKey:@"subdivision"];
        self.selected = [aDecoder decodeBoolForKey:@"selected"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInt32:self.beat forKey:@"beat"];
    [aCoder encodeInt:self.subdivision forKey:@"subdivision"];
    [aCoder encodeBool:self.selected forKey:@"selected"];
}
@end

@implementation HarmonyWizKeySignEvent
- (UInt8)relativeMajorRoot {
    return ((self.root + (self.tonality == kTonality_Minor ? 3 : 0))%12);
}

- (SInt8)sharpsFlatsCount {
    return sharpsFlatsCountForRoot(self.relativeMajorRoot);
}

- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizKeySignEvent *result = [super copyWithZone:zone];
    if (result) {
        result.root = self.root;
        result.tonality = self.tonality;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.root = [aDecoder decodeIntForKey:@"root"];
        self.tonality = [aDecoder decodeIntForKey:@"tonality"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:self.root forKey:@"root"];
    [aCoder encodeInt:self.tonality forKey:@"tonality"];
}
@end

@implementation HarmonyWizModeEvent
@synthesize modeAccidentalTypes = _modeAccidentalTypes;
@synthesize spelling = _spelling;
@synthesize modePitches = _modePitches;
@synthesize transposition = _transposition;

- (NSArray *)spelling {
    @synchronized(self) {
        return _spelling;
    }
}

- (void)setSpelling:(NSArray *)spelling {
    @synchronized(self) {
        _modeAccidentalTypes = nil;
        _spelling = spelling;
    }
}

- (NSArray *)modePitches {
    @synchronized(self) {
        return _modePitches;
    }
}

- (void)setModePitches:(NSArray *)modePitches {
    @synchronized(self) {
        _modeAccidentalTypes = nil;
        _modePitches = modePitches;
    }
}

- (unsigned char)transposition {
    @synchronized(self) {
        return _transposition;
    }
}

- (void)setTransposition:(unsigned char)transposition {
    @synchronized(self) {
        _modeAccidentalTypes = nil;
        _transposition = transposition;
    }
}

- (NSArray*)modeAccidentalTypes {
    @synchronized(self) {
        if (_modeAccidentalTypes == nil) {
            _modeAccidentalTypes = [self createModeAccidentalTypes];
        }
        return _modeAccidentalTypes;
    }
}

- (NSArray*)createModeAccidentalTypes {
    const NSArray *modePitches = _modePitches;
    const unsigned char transposition = _transposition;
    const NSArray *modeSpelling = _spelling;
    const NSArray *CMajorScale = @[@(0),@(2),@(4),@(5),@(7),@(9),@(11)];
    NSMutableArray *majorScale = [NSMutableArray arrayWithCapacity:CMajorScale.count];
    
    // construct the correct spelling for the mode
    {
        int closestNote = -1;
        int closestIdx = -1;
        for (int i = 0; i < CMajorScale.count; i++) {
            if (0 <= [CMajorScale[i] intValue] - transposition && [CMajorScale[i] intValue] - transposition <= 1) {
                closestNote = [CMajorScale[i] intValue];
                closestIdx = i;
                break;
            }
        }
        NSAssert(closestNote >= 0 && closestIdx >= 0, @"Could not find closest note");
        for (int i = 0; i < CMajorScale.count; i++) {
            int n;
            if (closestIdx + i >= CMajorScale.count)
                n = [CMajorScale[(closestIdx + i) - CMajorScale.count] intValue] + 12;
            else
                n = [CMajorScale[closestIdx + i] intValue];
            [majorScale addObject:@(n)];
        }
    }
    
    NSMutableArray *shifts = @[@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0)].mutableCopy;
    for (int i = 0; i < modePitches.count; i++) {
        const int n = [majorScale[[modeSpelling[i] intValue]] intValue];
        const int shift = [modePitches[i] intValue] + transposition - n;
        const NSUInteger idx = ([modePitches[i] intValue] + transposition) % 12;
        shifts[idx] = @(shift);
    }
    return [NSArray arrayWithArray:shifts];
}

- (SInt8)sharpsFlatsCount {
    return sharpsFlatsCountForRoot(self.transposition);
}

- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizModeEvent *result = [super copyWithZone:zone];
    if (result) {
        result.modeID = self.modeID;
        result.modePitches = [self.modePitches copy];
        result.spelling = [self.spelling copy];
        result.transposition = self.transposition;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.modeID = [aDecoder decodeIntForKey:@"modeID"];
        self.modePitches = [aDecoder decodeObjectForKey:@"modePitches"];
        self.spelling = [aDecoder decodeObjectForKey:@"spelling"];
        self.transposition = [aDecoder decodeIntForKey:@"transposition"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:self.modeID forKey:@"modeID"];
    [aCoder encodeObject:self.modePitches forKey:@"modePitches"];
    [aCoder encodeObject:self.spelling forKey:@"spelling"];
    [aCoder encodeInt:self.transposition forKey:@"transposition"];
}
@end

@implementation HarmonyWizTimeSignEvent
- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizTimeSignEvent *result = [super copyWithZone:zone];
    if (result) {
        result.upperNumeral = self.upperNumeral;
        result.lowerNumeral = self.lowerNumeral;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.upperNumeral = [aDecoder decodeIntForKey:@"upperNumeral"];
        self.lowerNumeral = [aDecoder decodeIntForKey:@"lowerNumeral"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:self.upperNumeral forKey:@"upperNumeral"];
    [aCoder encodeInt:self.lowerNumeral forKey:@"lowerNumeral"];
}
@end

@implementation HarmonyWizSpecialBarLineEvent
- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizSpecialBarLineEvent *result = [super copyWithZone:zone];
    if (result) {
        result.barLine = self.barLine;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.barLine = [aDecoder decodeIntForKey:@"barLine"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:self.barLine forKey:@"barLine"];
}
@end

@implementation HarmonyWizMusicEvent
+ (UInt16)middleC {
    return 48;
}
- (void)carryForBeatSubdivision:(UInt16)beatSubdivision {
    if (self.subdivision >= beatSubdivision) {
        self.beat += self.subdivision / beatSubdivision;
        self.subdivision %= beatSubdivision;
    }
}
- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizMusicEvent *result = [super copyWithZone:zone];
    if (result) {
        result.durationBeats = self.durationBeats;
        result.durationSubdivisions = self.durationSubdivisions;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.durationBeats = [aDecoder decodeIntForKey:@"durationBeats"];
        self.durationSubdivisions = [aDecoder decodeIntForKey:@"durationSubdivisions"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:self.durationBeats forKey:@"durationBeats"];
    [aCoder encodeInt:self.durationSubdivisions forKey:@"durationSubdivisions"];
}

- (NSComparisonResult)compareDuration:(HarmonyWizMusicEvent*)event {
    if (self.durationBeats < event.durationBeats)
        return NSOrderedAscending;
    else if (self.durationBeats > event.durationBeats)
        return NSOrderedDescending;
    else if (self.durationBeats == event.durationBeats) {
        if (self.durationSubdivisions < event.durationSubdivisions)
            return NSOrderedAscending;
        else if (self.durationSubdivisions > event.durationSubdivisions)
            return NSOrderedDescending;
    }
    return NSOrderedSame;
}
@end

@implementation HarmonyWizRestEvent
- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizRestEvent *result = [super copyWithZone:zone];
    if (result) {
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
}
@end

@implementation HarmonyWizNoteEvent
- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizNoteEvent *result = [super copyWithZone:zone];
    if (result) {
        result.noteValue = self.noteValue;
        result.type = self.type;
        result.enharmonicShift = self.enharmonicShift;
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.type = [aDecoder decodeIntForKey:@"type"];
        self.noteValue = [aDecoder decodeIntForKey:@"noteValue"];
        self.enharmonicShift = [aDecoder decodeIntForKey:@"enharmonicShift"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeInt:self.type forKey:@"type"];
    [aCoder encodeInt:self.noteValue forKey:@"noteValue"];
    [aCoder encodeInt:self.enharmonicShift forKey:@"enharmonicShift"];
}

- (BOOL)wasCreatedByUser {
    return (self.type != kNoteEventType_Auto);
}

- (BOOL)isNonHarmonicTone {
    return (self.type == kNoteEventType_Passing || self.type == kNoteEventType_Suspension);
}

- (void)applyNoteShift:(int)shift {
    switch (shift) {
        case -2:
            self.enharmonicShift = kForceDoubleFlat;
            break;
        case -1:
            self.enharmonicShift = kSharpsToFlats;
            break;
        case 0:
            self.enharmonicShift = kDefaultAccidental;
            break;
        case 1:
            self.enharmonicShift = kFlatsToSharps;
            break;
        case 2:
            self.enharmonicShift = kForceDoubleSharp;
            break;
            
        default:
            NSLog(@"shift of %d not allowed", shift);
            break;
    }
}

@end

NSString * const HarmonyWizLeadingNoteTreatmentProperty = @"HarmonyWizLeadingNoteTreatmentProperty";
NSString * const HarmonyWizLeadingNoteTreatmentMelodic = @"HarmonyWizLeadingNoteTreatmentMelodic";
NSString * const HarmonyWizLeadingNoteTreatmentHarmonic = @"HarmonyWizLeadingNoteTreatmentHarmonic";
NSString * const HarmonyWizRelaxRulesProperty = @"HarmonyWizRelaxRulesProperty";
NSString * const HarmonyWizDegreeProperty = @"DegreeProperty";
NSString * const HarmonyWizThirdProperty = @"ThirdProperty";
NSString * const HarmonyWizFifthProperty = @"FifthProperty";
NSString * const HarmonyWizSevenProperty = @"SevenProperty";
NSString * const HarmonyWizNineProperty = @"NineProperty";
NSString * const HarmonyWizElevenProperty = @"ElevenProperty";
NSString * const HarmonyWizThirteenProperty = @"ThirteenProperty";
NSString * const HarmonyWizChordTypeProperty = @"ChordTypeProperty";
NSString * const HarmonyWizInversionProperty = @"InversionProperty";

@implementation HarmonyWizHarmonyEvent
- (id)copyWithZone:(NSZone *)zone {
    HarmonyWizHarmonyEvent *result = [super copyWithZone:zone];
    if (result) {
        result.noChord = self.noChord;
        result.pause = self.pause;
        result.constraints = [self.constraints mutableCopyWithZone:zone];
        result.temporaryConstraints = [self.temporaryConstraints mutableCopyWithZone:zone];
        result.properties = [self.properties mutableCopyWithZone:zone];
    }
    return result;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.noChord = [aDecoder decodeBoolForKey:@"noChord"];
        self.pause = [aDecoder decodeBoolForKey:@"pause"];
        self.properties = [aDecoder decodeObjectForKey:@"properties"];
        self.constraints = [aDecoder decodeObjectForKey:@"constraints"];
        self.temporaryConstraints = [aDecoder decodeObjectForKey:@"temporaryConstraints"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeBool:self.noChord forKey:@"noChord"];
    [aCoder encodeBool:self.pause forKey:@"pause"];
    [aCoder encodeObject:self.properties forKey:@"properties"];
    [aCoder encodeObject:self.constraints forKey:@"constraints"];
    [aCoder encodeObject:self.temporaryConstraints forKey:@"temporaryConstraints"];
}

@end
