//
// PPEmitterView.h
// Created by Particle Playground on 23/01/2014
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PPCollageEmitterView : UIView

-(void)start;

@end