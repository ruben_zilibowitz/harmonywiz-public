//
//  HarmonyWizTests.m
//  HarmonyWizTests
//
//  Created by Ruben Zilibowitz on 3/11/12.
//  Copyright (c) 2012 Zilibowitz Productions. All rights reserved.
//

#import "HarmonyWizTests.h"
#import "NSValue+HarmonyWizAdditions.h"
#include <iostream>
#include "Solver.h"

using namespace HarmonyWiz;
using namespace std;

@implementation HarmonyWizTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)testNSValue {
    ScaleNote sn(0);
    NSValue *val = [NSValue valueWithHWScaleNote:sn];
    STAssertNotNil(val, @"value is nil");
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testFigures {
    Figures figures;
    figures.reset();
    figures.at(1) = true;
    figures.at(3) = true;
    figures.at(5) = true;
    figures.at(7) = true;
    figures.check_chord();
    Figures::ChordType type;
    Figures::ChordInversion inv;
    tie(type,inv) = Figures::unpackChord(figures.what());
    STAssertTrue(type == Figures::kSeven && inv == Figures::kRootPosition, @"figures failed");
}

- (void)testVerify {
    Solver<4> solver(7, 0, false);
    
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-12,7));
    solver.setRange(3, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    solver.getFigures(0).disallowAll();
    solver.getFigures(0).allow(Figures::kTriad_Root);
    solver.getFigures(6).disallowAll();
    solver.getFigures(6).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(0, 0);
    solver.setAllowedRoot(6, 0);
    
    solver.setNote(0, 0, 0);
    solver.setNote(0, 1, 1);
    solver.setNote(0, 2, 3);
    solver.setNote(0, 3, 2);
    solver.setNote(0, 4, 1);
    solver.setNote(0, 5, 1);
    solver.setNote(0, 6, 0);
    
    solver.setNote(1, 0, -3);
    solver.setNote(1, 1, -3);
    solver.setNote(1, 2, -2);
    solver.setNote(1, 3, -2);
    solver.setNote(1, 4, -2);
    solver.setNote(1, 5, -3);
    solver.setNote(1, 6, -3);
    
    solver.setNote(2, 0, -5);
    solver.setNote(2, 1, -6);
    solver.setNote(2, 2, -6);
    solver.setNote(2, 3, -7);
    solver.setNote(2, 4, -4);
    solver.setNote(2, 5, -4);
    solver.setNote(2, 6, -5);
    
    solver.setNote(3, 0, -7);
    solver.setNote(3, 1, -8);
    solver.setNote(3, 2, -9);
    solver.setNote(3, 3, -9);
    solver.setNote(3, 4, -9);
    solver.setNote(3, 5, -8);
    solver.setNote(3, 6, -7);
    
    bool result = solver.verify();
    STAssertTrue(result, @"verify failed");
}

- (void)testConstraints {
    Solver<4> solver(3, 0, true);
    
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-12,7));
    solver.setRange(3, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    
    solver.getFigures(0).disallowAll();
    solver.getFigures(0).allow(Figures::kTriad_Root);
    solver.getFigures(2).disallowAll();
    solver.getFigures(2).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(0, 0);
    solver.setAllowedRoot(2, 0);
    
    solver.setAllowedThird(1, ScaleNote(6,ScaleNote::kAlteration_Sharpen));
    
    solver.setNote(0, 0, 7);
    solver.setNote(0, 1, 8);
    solver.setNote(0, 2, 9);
    
    bool result = solver.solveMaximisingScore();
    STAssertTrue(result, @"solver failed");
    
    cout << solver;
    cout << "score: " << solver.score() << endl;
    
    result = solver.verify();
    STAssertTrue(result, @"solver verify failed");
}

- (void)testCrossRelation {
    Solver<4> solver(5, 3, true);
    
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-12,7));
    solver.setRange(3, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    
    // set global properties
    solver.clearGlobalProperty(kBassAlwaysMoves);
    
    // set local properties
    solver.getFigures(0).disallowAll();
    solver.getFigures(0).allow(Figures::kTriad_Root);
    solver.getFigures(4).disallowAll();
    solver.getFigures(4).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(0, 0);
    solver.setAllowedRoot(4, 0);
    solver.getFigures(3).disallowAll();
    solver.getFigures(3).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(3, 4);
    solver.setAllowedThird(3, ScaleNote(6,ScaleNote::kAlteration_Sharpen));
    
    // set notes
    solver.setNote(0, 0, 7);
    solver.setNote(0, 1, 3);
    solver.setNote(0, 2, 4);
    solver.setNote(0, 3, 1);
    solver.setNote(0, 4, 2);
    
    bool result = solver.solveMaximisingScore();
    STAssertTrue(result, @"solve failed");
    
    result = solver.verify();
    STAssertTrue(result, @"verify failed");
    
    cout << solver;
    cout << "score: " << solver.score() << endl;
}

- (void)testThreeParts {
    // create solver
    Solver<3> solver(9, 0, false);
    
    // setup solver parameters
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 19);
    
    // set melody
    solver.setNote(0, 0, 4);
    solver.setNote(0, 1, 6);
    solver.setNote(0, 2, 5);
    solver.setNote(0, 3, 7);
    solver.setNote(0, 4, 6);
    solver.setNote(0, 5, 8);
    solver.setNote(0, 6, 7);
    solver.setNote(0, 7, 5);
    solver.setNote(0, 8, 4);
    
    // set global properties
    solver.clearGlobalProperty(kCommonTone);
    solver.clearGlobalProperty(kBassAlwaysMoves);
    
    // set local properties
    solver.getFigures(0).disallowAll();
    solver.getFigures(0).allow(Figures::kTriad_Root);
    solver.getFigures(8).disallowAll();
    solver.getFigures(8).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(0, 0);
    solver.setAllowedRoot(8, 0);
    
    // run solver
    bool result = solver.solve();
    
    // assert solver was successfull
    STAssertTrue(result, @"three part solver failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"three part solver verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;
    
    // run maximiser
    result = solver.solveMaximisingScore();
    
    // assert solver was successfull
    STAssertTrue(result, @"three part maximise score failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"three part maximise score verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;
}

- (void)testFourParts
{
    // create solver
    Solver<4> solver(7, 0, false);
    
    // setup solver parameters
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-12,7));
    solver.setRange(3, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    solver.getFigures(0).disallowAll();
    solver.getFigures(0).allow(Figures::kTriad_Root);
    solver.getFigures(6).disallowAll();
    solver.getFigures(6).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(0, 0);
    solver.setAllowedRoot(6, 0);
    
    // set melody
    solver.setNote(0, 0, 4);
    solver.setNote(0, 1, 5);
    solver.setNote(0, 2, 7);
    solver.setNote(0, 3, 6);
    solver.setNote(0, 4, 5);
    solver.setNote(0, 5, 4);
    solver.setNote(0, 6, 4);
    
    // set bass
    solver.setNote(3, 0, 0);
    solver.setNote(3, 1, -4);
    solver.setNote(3, 2, -5);
    solver.setNote(3, 3, -3);
    solver.setNote(3, 4, -2);
    solver.setNote(3, 5, -1);
//    solver.setNote(3, 6, 0);  // this last note should be inferred from the other constraints
    
    // run solver
    bool result = solver.solve();
    
    // assert solver was successfull
    STAssertTrue(result, @"four part solver failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"four part solver verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;
    
    // run maximiser
    result = solver.solveMaximisingScore();
    
    // assert solver was successfull
    STAssertTrue(result, @"four part maximise score failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"four part maximise score verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;
}

- (void)testScoreMajor {
    // create solver
    Solver<4> solver(7, 0, false);
    
    // setup solver parameters
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-12,7));
    solver.setRange(3, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    
    // set soprano
    solver.setNote(0, 0, 9);
    solver.setNote(0, 1, 9);
    solver.setNote(0, 2, 11);
    solver.setNote(0, 3, 10);
    solver.setNote(0, 4, 12);
    solver.setNote(0, 5, 11);
    solver.setNote(0, 6, 12);
    
    // set alto
    solver.setNote(1, 0, 2);
    solver.setNote(1, 1, 4);
    solver.setNote(1, 2, 6);
    solver.setNote(1, 3, 8);
    solver.setNote(1, 4, 7);
    solver.setNote(1, 5, 6);
    solver.setNote(1, 6, 7);
    
    // set tenor
    solver.setNote(2, 0, 0);
    solver.setNote(2, 1, 0);
    solver.setNote(2, 2, 1);
    solver.setNote(2, 3, 1);
    solver.setNote(2, 4, 2);
    solver.setNote(2, 5, 2);
    solver.setNote(2, 6, 2);
    
    // set bass
    solver.setNote(3, 0, -9);
    solver.setNote(3, 1, -10);
    solver.setNote(3, 2, -10);
    solver.setNote(3, 3, -9);
    solver.setNote(3, 4, -9);
    solver.setNote(3, 5, -5);
    solver.setNote(3, 6, -9);
    
    STAssertTrue(solver.verify(), @"solver verify");
    STAssertTrue(solver.score() == -5, @"solver score");
}

- (void)testConstraintsAgain {
    ScaleNote melarr[] = {4,3,5,6,4};
    uint8_t key = 5;
    uint8_t isMinor = 1;
    
    vector<ScaleNote> melody(melarr, melarr + sizeof(melarr)/sizeof(ScaleNote));
    Solver<4> solver(melody.size(), key, isMinor);
    int16_t middleC = 0;
    solver.setRange(0, std::make_tuple(middleC,middleC+24-3));
    solver.setRange(1, std::make_tuple(middleC-7,middleC+12+7));
    solver.setRange(2, std::make_tuple(middleC-12,middleC+12-3));
    solver.setRange(3, std::make_tuple(middleC-24+4,middleC+4));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    
    // set global properties
    bitset< kNumProperties > properties;
    properties.reset();
    properties.flip();
    properties[kBassAlwaysMoves] = false;
    properties[kAllowElevenths] = false;
    properties[kAllowThirteenths] = false;
    solver.setAllGlobalProperties(properties);
    
    // set melody
    for (uint16_t i = 0; i < melody.size(); i++) {
        solver.setNote(0, i, melody.at(i));
    }
    
    // set constraints
    ScaleNote back = melody.back().octaveMod();
    ScaleNote front = melody.front().octaveMod();
    if (front == 0 || front == 2 || front == 4) {
        solver.setAllowedRoot(0,0);
    }
    if (back == 0 || back == 2 || back == 4) {
        solver.setAllowedRoot(solver.getCols()-1,0);
        if (solver.getCols() >= 2) {
            ScaleNote beforeBack = melody.at(melody.size()-2).octaveMod();
            if (beforeBack == 4 || beforeBack == 6 || beforeBack == 1) {
                solver.setAllowedRoot(solver.getCols()-2,4);
            }
        }
    }
    
    bool result = solver.solveMaximisingScore();
    
    STAssertTrue(result, @"solver failed");
    
    cout << solver;
    cout << "score: " << solver.score() << endl;
    
    result = solver.verify();
    STAssertTrue(result, @"solver verify failed");
}

- (void)testScoreMinor {
    // create solver
    Solver<4> solver(7, 9, true);
    
    // setup solver parameters
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-12,7));
    solver.setRange(3, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    
    // set soprano
    solver.setNote(0, 0, 0);
    solver.setNote(0, 1, 1);
    solver.setNote(0, 2, 3);
    solver.setNote(0, 3, 6);
    solver.setNote(0, 4, 6);
    solver.setNote(0, 5, 4);
    solver.setNote(0, 6, 6);
    
    // set alto
    solver.setNote(1, 0, -3);
    solver.setNote(1, 1, -1);
    solver.setNote(1, 2, 1);
    solver.setNote(1, 3, 3);
    solver.setNote(1, 4, 2);
    solver.setNote(1, 5, 1);
    solver.setNote(1, 6, 2);
    
    // set tenor
    solver.setNote(2, 0, -3);
    solver.setNote(2, 1, -3);
    solver.setNote(2, 2, ScaleNote(-2, ScaleNote::kAlteration_Sharpen));
    solver.setNote(2, 3, ScaleNote(-2, ScaleNote::kAlteration_Sharpen));
    solver.setNote(2, 4, -3);
    solver.setNote(2, 5, -3);
    solver.setNote(2, 6, -3);
    
    // set bass
    solver.setNote(3, 0, -5);
    solver.setNote(3, 1, -6);
    solver.setNote(3, 2, -6);
    solver.setNote(3, 3, -13);
    solver.setNote(3, 4, -12);
    solver.setNote(3, 5, -8);
    solver.setNote(3, 6, -12);
    
    STAssertTrue(solver.verify(), @"solver verify");
    STAssertTrue(solver.score() == -5, @"solver score");
}

- (void)_testFiveParts {
    // create solver
    Solver<5> solver(7, 0, false);
    
    // setup solver parameters
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-7,12));
    solver.setRange(3, std::make_tuple(-12,7));
    solver.setRange(4, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 12);
    solver.setMaxPartDistance(3, 19);
    solver.getFigures(0).disallowAll();
    solver.getFigures(0).allow(Figures::kTriad_Root);
    solver.getFigures(6).disallowAll();
    solver.getFigures(6).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(0, 0);
    solver.setAllowedRoot(6, 0);
    
    solver.setNote(0, 0, 7);
    solver.setNote(0, 1, 11);
    solver.setNote(0, 2, 10);
    solver.setNote(0, 3, 9);
    solver.setNote(0, 4, 8);
    solver.setNote(0, 5, 8);
    solver.setNote(0, 6, 7);
    
    // run solver
    bool result = solver.solve();
    
    // assert solver was successfull
    STAssertTrue(result, @"five part solver failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"five part solver verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;
    
    /*
    // run maximiser
     // this is very slow
//    result = solver.solveMaximisingScore();
    solver.setMinimumAllowedScore(10);
    result = solver.solve();
    
    // assert solver was successfull
    STAssertTrue(result, @"five part maximise score failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"five part maximise score verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;
     */
}

- (void) testMinorKey {
    // create solver
    Solver<4> solver(8, 4, true);
    
    // setup solver parameters
    solver.setRange(0, std::make_tuple(0,19));
    solver.setRange(1, std::make_tuple(-7,12));
    solver.setRange(2, std::make_tuple(-12,7));
    solver.setRange(3, std::make_tuple(-19,0));
    solver.setMaxPartDistance(0, 12);
    solver.setMaxPartDistance(1, 12);
    solver.setMaxPartDistance(2, 19);
    solver.getFigures(0).disallowAll();
    solver.getFigures(0).allow(Figures::kTriad_Root);
    solver.getFigures(7).disallowAll();
    solver.getFigures(7).allow(Figures::kTriad_Root);
    solver.setAllowedRoot(0, 0);
    solver.setAllowedRoot(6, 4);
    solver.setAllowedThird(6, ScaleNote(6,ScaleNote::kAlteration_Sharpen));
    solver.setAllowedRoot(7, 0);
    
    // set melody
    solver.setNote(0, 0, 4);
    solver.setNote(0, 1, 7);
    solver.setNote(0, 2, 9);
    solver.setNote(0, 3, 8);
    solver.setNote(0, 4, 7);
    solver.setNote(0, 5, 5);
    solver.setNote(0, 6, 4);
    solver.setNote(0, 7, 4);
    
    // run solver
    bool result = solver.solve();
    
    // assert solver was successfull
    STAssertTrue(result, @"minor four part solver failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"minor four part solver verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;
    /*
    // run maximiser
    result = solver.solveMaximisingScore();
    
    // assert solver was successfull
    STAssertTrue(result, @"minor four part maximise score failed");
    
    // verify solver was successfull
    result = solver.verify();
    STAssertTrue(result, @"minor four part maximise score verify failed");
    
    // output solver
    cout << solver;
    cout << "score: " << solver.score() << endl;*/
}

@end
